# SITOLAUT v1.0.0

> Aplikasi pengelolaan kapal dan pemesanan barang komoditas melalui program Tol Laut Direktorat Jendrak Lalu Lintas dan Angkutan Laut
> Kementrian Perhubungan Republik Indonesia

## Requirement
> React Native Development Environment
> https://reactnative.dev/docs/environment-setup

## Instalasi

> ### Yarn
> ``` yarn install ```

> ### NPM
> ``` npm install ```

## Running

> ### Development
> ```cd android && gradlew clean && cd ..```
> ```npx react-native run-android```

> ### Bat File (for shortcut)
> #### develop.bat
> Development purpose
> #### release.bat
> Release purpose
