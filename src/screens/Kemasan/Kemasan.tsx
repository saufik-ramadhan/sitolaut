import React, { useState } from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  Button,
  List
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput, Space, TextInput } from "../../components";
import { DateFormat } from "../../services/utils";
import {
  packagingFilterClear,
  packagingFetchPage,
  packagingDelete,
  packagingFilterAdd,
} from "./../../redux/packagingReducer";
import { Formik } from "formik";

function FloatingAdd({ navigation }: RegulatorStackProps<"Kemasan">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateKemasan");
      }}
    />
  );
}

export default function Kemasan({
  navigation,
}: RegulatorStackProps<"Kemasan">) {
  const dispatch = useDispatch();
  const [nama, setNama] = useState();
  const packaging = useSelector(function (state: RootState) {
    return state.packaging.packagingData ? state.packaging.packagingData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.packaging.packagingGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.packaging.packagingFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.packaging.packagingGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.packaging.packagingAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.packaging.packagingDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.packaging.packagingViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(packagingFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(packagingFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(packagingDelete(id));
    dispatch(packagingFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(packagingFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <View style={{flex: 1}}>
      <View style={{paddingHorizontal: 20, backgroundColor: 'whitesmoke'}}>
        <TextInput
          label="Cari"
          onChangeText={(val) => setNama(val)}
          value={nama}
          style={{ width: '100%' }}
        />
        <Button onPress={() => dispatch(packagingFilterAdd({nama: nama}))}>Cari</Button>
      </View>
      <FlatList
        data={packaging}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Item
              title={item.kemasan}
              right={() => (
                <>
                  <IconButton
                    icon="square-edit-outline" 
                    onPress={() => navigation.navigate("UpdateKemasan", {item: item})}
                  />
                  <IconButton
                    icon="delete"
                    onPress={() => {
                      Alert.alert(
                        "Anda yakin",
                        "Menghapus Jenis Kemasan ?",
                        [
                          { text: "OK", onPress: () => handleDelete(item.id) },
                          { text: "Cancel", onPress: () => null }
                        ],
                        { cancelable: false }
                      );
                    }}
                  />
                </>
              )}
            />
          );
        }}
      />
      <FloatingAdd navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});
