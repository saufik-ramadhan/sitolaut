import React from "react";
import { Root, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { packagingAdd } from "../../redux/packagingReducer";
import { Button, HelperText } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import * as Yup from 'yup';

const Schema = Yup.object({
  kemasan: Yup.string().required().label('Nama Kemasan')
})


export default function CreateKemasan({
  navigation,
}: RegulatorStackProps<"CreateKemasan">) {
  const dispatch = useDispatch();
  function handleAdd(values) {
    dispatch(packagingAdd(values));
  }
  return (
    <Root style={{padding: 20}}>
      <Formik
        validationSchema={Schema}
        initialValues={{
          kemasan: "",
        }}
        onSubmit={(values) => {
          handleAdd(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors }) => (
          <>
            <TextInput
              label="Nama Kemasan"
              onChangeText={handleChange("kemasan")}
              onBlur={handleBlur("kemasan")}
              placeholder=""
              value={values.kemasan}
              error={errors.kemasan}
            />
            <HelperText type="error">{errors.kemasan}</HelperText>
            <Button onPress={handleSubmit}>Buat Baru</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}


