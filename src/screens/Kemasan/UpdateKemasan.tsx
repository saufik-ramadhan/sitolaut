import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  packagingAdd,
  packagingFetchOne,
  packagingUpdate,
  packagingFilterAdd,
} from "../../redux/packagingReducer";
import { Button, HelperText } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import * as Yup from 'yup';

const Schema = Yup.object({
  kemasan: Yup.string().required().label('Kemasan')
})


export default function UpdateKemasan({
  navigation,
  route,
}: RegulatorStackProps<"UpdateKemasan">) {
  const dispatch = useDispatch();
  const { id, kemasan } = route.params.item;
  function handleUpdate(values) {
    dispatch(packagingUpdate(values));
  }
  return (
    <Root style={{padding: 20}}>
      <Formik
        validationSchema={Schema}
        initialValues={{
          id: id,
          kemasan: kemasan,
        }}
        onSubmit={(values) => {
          handleUpdate(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors }) => (
          <>
            <TextInput
              label="Nama Barang"
              onChangeText={handleChange("kemasan")}
              onBlur={handleBlur("kemasan")}
              value={values.kemasan}
              error={errors.kemasan}
            />
            <HelperText>{errors.kemasan}</HelperText>
            <Button onPress={handleSubmit}>Update</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
