import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import { Root, SelectInput2, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { priceAdd } from "../../redux/priceReducer";
import { RegulatorStackProps } from "../Navigator";
import { routeFetchAll } from "../../redux/routeReducer";
import { Button } from "react-native-paper";
import * as Yup from 'yup';

const Schema = Yup.object({
  biaya_angkut: Yup.string().required().label('Biaya Angkut'),
  biaya_angkut_keterangan: Yup.string().required().label('Keterangan biaya angkut'),
  biaya_pelabuhan_bongkar: Yup.string().required().label('Biaya Pelabuhan Bongkar'),
  biaya_pelabuhan_bongkar_keterangan: Yup.string().required().label('Keterangan biaya bongkar'),
  biaya_pelabuhan_muat: Yup.string().required().label('Biaya Pelabuhan Muat'),
  biaya_pelabuhan_muat_keterangan: Yup.string().required().label('Keterangan biaya muat'),
  biaya_pengurusan: Yup.string().required().label('Biaya Pengurusan'),
  biaya_stripping: Yup.string().required().label('Biaya Stripping'),
  biaya_stripping_keterangan: Yup.string().required().label('Keterangan biaya stripping'),
  trayek_id: Yup.number().required().label('Trayek'),
})


export default function CreateMasterTarif({
  navigation,
  route,
}: RegulatorStackProps<"CreateMasterTarif">) {
  const dispatch = useDispatch();
  const { trayek } = route.params;
  function handleAdd(values) {
    dispatch(priceAdd(values));
    // console.log(values);
  }
  function fetchData() {dispatch(routeFetchAll())}
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <Root>
      <Formik
        validationSchema={Schema}
        initialValues={{
          biaya_angkut: "",
          biaya_angkut_keterangan: "",
          biaya_pelabuhan_bongkar: "",
          biaya_pelabuhan_bongkar_keterangan: "",
          biaya_pelabuhan_muat: "",
          biaya_pelabuhan_muat_keterangan: "",
          biaya_pengurusan: "",
          biaya_stripping: "",
          biaya_stripping_keterangan: "",
          trayek_id: "",
        }}
        onSubmit={(values) => {
          handleAdd(values);
          navigation.goBack();
          // console.log(values);
        }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          errors,
          setFieldValue,
        }) => (
          <>
            <SelectInput2
              label={"Trayek"}
              onValueChange={(itemValue, itemIndex) => setFieldValue("trayek_id", itemValue)}
              items={trayek}
              itemLabel="kode"
              itemValue="id"
              mode="dropdown"
              selectedValue={values.trayek_id}
              error={errors.trayek_id}
            />
            <TextInput
              label="Keterangan Biaya Pengurusan"
              onChangeText={handleChange("biaya_pengurusan")}
              onBlur={handleBlur("biaya_pengurusan")}
              placeholder=""
              value={values.biaya_pengurusan}
              error={errors.biaya_pengurusan}
            />
            <TextInput
              label="Biaya Pelabuhan Muat (Rp)"
              onChangeText={handleChange("biaya_pelabuhan_muat")}
              onBlur={handleBlur("biaya_pelabuhan_muat")}
              placeholder=""
              value={values.biaya_pelabuhan_muat}
              error={errors.biaya_pelabuhan_muat}
              keyboardType="decimal-pad"
            />
            <TextInput
              label="Keterangan"
              onChangeText={handleChange("biaya_pelabuhan_muat_keterangan")}
              onBlur={handleBlur("biaya_pelabuhan_muat_keterangan")}
              placeholder=""
              value={values.biaya_pelabuhan_muat_keterangan}
              error={errors.biaya_pelabuhan_muat_keterangan}
            />
            <TextInput
              label="Biaya Angkut (Rp)"
              onChangeText={handleChange("biaya_angkut")}
              onBlur={handleBlur("biaya_angkut")}
              placeholder=""
              value={values.biaya_angkut}
              error={errors.biaya_angkut}
              keyboardType="decimal-pad"
            />
            <TextInput
              label="Keterangan "
              onChangeText={handleChange("biaya_angkut_keterangan")}
              onBlur={handleBlur("biaya_angkut_keterangan")}
              placeholder=""
              value={values.biaya_angkut_keterangan}
              error={errors.biaya_angkut_keterangan}
            />
            <TextInput
              label="Biaya Pelabuhan Bongkar (Rp)"
              onChangeText={handleChange("biaya_pelabuhan_bongkar")}
              onBlur={handleBlur("biaya_pelabuhan_bongkar")}
              placeholder=""
              value={values.biaya_pelabuhan_bongkar}
              error={errors.biaya_pelabuhan_bongkar}
              keyboardType="decimal-pad"
            />
            <TextInput
              label="Keterangan"
              onChangeText={handleChange("biaya_pelabuhan_bongkar_keterangan")}
              onBlur={handleBlur("biaya_pelabuhan_bongkar_keterangan")}
              placeholder=""
              value={values.biaya_pelabuhan_bongkar_keterangan}
              error={errors.biaya_pelabuhan_bongkar_keterangan}
            />
            <TextInput
              label="Biaya Stripping (Rp)"
              onChangeText={handleChange("biaya_stripping")}
              onBlur={handleBlur("biaya_stripping")}
              placeholder=""
              value={values.biaya_stripping}
              error={errors.biaya_stripping}
              keyboardType="decimal-pad"
            />
            <TextInput
              label="Keterangan"
              onChangeText={handleChange("biaya_stripping_keterangan")}
              onBlur={handleBlur("biaya_stripping_keterangan")}
              placeholder=""
              value={values.biaya_stripping_keterangan}
              error={errors.biaya_stripping_keterangan}
            />
            <Button onPress={handleSubmit}>Create</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
