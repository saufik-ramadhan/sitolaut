import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  List,
  Button,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput, Space } from "../../components";
import { DateFormat, formatRupiah } from "../../services/utils";
import {
  priceFilterClear,
  priceFetchPage,
  priceDelete,
  priceFilterAdd,
} from "./../../redux/priceReducer";
import { Formik } from "formik";
import { routeFetchAll } from "../../redux/routeReducer";

function FloatingAdd({
  navigation,
  route,
}: RegulatorStackProps<"MasterTarif">) {
  const trayek = useSelector(function (state: RootState) {
    return state.route.routeGetSuccess ? state.route.routeGetSuccess.data : [];
  });
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateMasterTarif", {
          trayek: trayek,
        });
      }}
    />
  );
}

export default function MasterTarif({
  navigation,
  route,
}: RegulatorStackProps<"MasterTarif">) {
  const dispatch = useDispatch();
  const price = useSelector(function (state: RootState) {
    return state.price.priceData ? state.price.priceData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.price.priceGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.price.priceFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.price.priceGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.price.priceAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.price.priceDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.price.priceViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(priceFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(priceFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(priceDelete(id));
    dispatch(priceFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(priceFilterClear());
  };

  React.useEffect(() => {
    dispatch(routeFetchAll());
  }, []);
  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={price}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Accordion
              title={item.kode_trayek}
              description={`${item.pel_asal} ➤ ${item.pel_sampai}`}
              children={
                <View style={{backgroundColor: 'whitesmoke'}}>
                  <List.Item title={'Rp.' + formatRupiah(parseInt(item.biaya_pelabuhan_muat))} description={'Biaya Pelabuhan Muat'} style={styles.item} titleStyle={styles.title} descriptionStyle={styles.description}/>
                  <List.Item title={'Rp.' + formatRupiah(parseInt(item.biaya_angkut))} description={'Biaya Angkut'} style={styles.item} titleStyle={styles.title} descriptionStyle={styles.description}/>
                  <List.Item title={'Rp.' + formatRupiah(parseInt(item.biaya_pelabuhan_bongkar))} description={'Biaya Pelabuhan Bongkar'} style={styles.item} titleStyle={styles.title} descriptionStyle={styles.description}/>
                  <List.Item title={'Rp.' + formatRupiah(parseInt(item.biaya_stripping))} description={'Biaya Stripping'} style={styles.item} titleStyle={styles.title} descriptionStyle={styles.description}/>
                  <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                    <Button icon={'square-edit-outline'} onPress={function(){
                      navigation.navigate("UpdateMasterTarif", {item: item});
                    }}>Edit</Button>
                    <Button icon={'delete'} color={Colors.danger} onPress={function() {
                      Alert.alert(
                        "Hapus",
                        "Anda yakin akan menghapus master tarif ?",
                        [
                          { text: "Ya", onPress: () => handleDelete(item.mst_tarif_id) },
                          { text: "Tidak", onPress: () => null}
                        ],
                      );
                    }}>Delete</Button>
                  </View>
                </View>
              }
              style={{elevation: 1}}
            />
          );
        }}
      />
      <FloatingAdd navigation={navigation} route={route} />
      <FilterButton
        onPress={function () {
          navigation.navigate("FilterMasterTarif");
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
  item: {
    paddingVertical: 0,
    marginVertical: 0,
  },
  title: {
    fontSize: 14,
  },
  description: {
    fontSize: 12,
  }
});
