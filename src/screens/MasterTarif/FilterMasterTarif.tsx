import React, { useEffect } from "react";
import { StyleSheet, Text, View, Picker } from "react-native";
import { Formik } from "formik";
import { priceFilterAdd } from "../../redux/priceReducer";
import { IconButton, TextInput, Button } from "react-native-paper";
import { SelectInput } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { routeFetchAll } from "../../redux/routeReducer";

export default function FilterMasterTarif({ navigation }) {
  const dispatch = useDispatch();
  const route = useSelector(function (state: RootState) {
    return state.route.routeGetSuccess ? state.route.routeGetSuccess.data : [];
  });
  useEffect(() => {
    dispatch(routeFetchAll());
  }, []);
  return (
    <Formik
      initialValues={{
        trayek_id: "",
      }}
      onSubmit={(val) => {
        dispatch(priceFilterAdd(val));
        navigation.goBack();
      }}
    >
      {({ handleChange, handleBlur, handleSubmit, values, setFieldValue }) => (
        <>
          {/**
           * Supplier
           */}
          <SelectInput
            label={"Trayek"}
            onChangeValue={(val) => setFieldValue("trayek_id", val.id)}
            options={route}
            objectKey="kode_trayek"
            withSearch
          />

          <Button onPress={handleSubmit}>Filter</Button>
        </>
      )}
    </Formik>
  );
}

const styles = StyleSheet.create({});
