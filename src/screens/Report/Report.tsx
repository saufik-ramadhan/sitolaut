import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { List } from "react-native-paper";
import { Root } from "../../components";
import { RegulatorStackProps } from "../Navigator";
import ExportPdf from "../../services/ExportPdf";
import ExportXls from './../../services/ExportXls';
import * as Notifications from "expo-notifications";
import FileViewer from "react-native-file-viewer";
import {
  REPORT_JUMLAH_USER,
  REPORT_STATUS_ORDER,
  REPORT_ORDER_MASUK_PER_BULAN,
  REPORT_DATA_USER,
  REPORT_REALISASI_MUATAN,
  REPORT_DISPARITAS_HARGA,
  REPORT_DAFTAR_SISA_KUOTA,
  REPORT_TOTAL_MUATAN_PER_WILAYAH,
  REPORT_MUATAN_PER_WILAYAH_PER_PRIORITAS,
  REPORT_REALISASI_VOYAGE_PER_TRAYEK,
  REPORT_WAKTU_TEMPUH,
  REPORT_TOTAL_ORDER_PER_JENIS_PRIORITAS,
  REPORT_MUATAN_TERBANYAK_PER_PRIORITAS,
  REPORT_TOTAL_CONTAINER_PER_BULAN,
  REPORT_EXCEL_JUMLAH_USER,
  REPORT_EXCEL_DAFTAR_SISA_KUOTA,
  REPORT_EXCEL_DATA_USER,
  REPORT_EXCEL_DISPARITAS_HARGA,
  REPORT_EXCEL_MUATAN_PER_WILAYAH_PER_PRIORITAS,
  REPORT_EXCEL_MUATAN_TERBANYAK_PER_PRIORITAS,
  REPORT_EXCEL_ORDER_MASUK_PER_BULAN,
  REPORT_EXCEL_REALISASI_MUATAN,
  REPORT_EXCEL_REALISASI_VOYAGE_PER_TRAYEK,
  REPORT_EXCEL_STATUS_ORDER,
  REPORT_EXCEL_TOTAL_CONTAINER_PER_BULAN,
  REPORT_EXCEL_TOTAL_MUATAN_PER_WILAYAH,
  REPORT_EXCEL_TOTAL_ORDER_PER_JENIS_PRIORITAS,
  REPORT_EXCEL_WAKTU_TEMPUH,
} from "./../../config/constants";

export default function Report({ navigation }: RegulatorStackProps<"Report">) {
  useEffect(() => {
    const subscription = Notifications.addNotificationResponseReceivedListener(
      (response) => {
        const url = response.notification.request.content.body;
        FileViewer.open(url);
        // Linking.openUrl(url);
      }
    );
    return function () {
      subscription.remove();
    };
  }, []);
  return (
    <Root>
      <List.Item
        title="Jumlah User"
        onPress={function () {
          navigation.navigate("ReportJumlahUser");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_JUMLAH_USER}
              data={{}}
              filename="report_excel_jumlah_user"
            />
            <ExportPdf
              url={REPORT_JUMLAH_USER}
              data={{}}
              filename="report_jumlah_user"
            />
          </View>
        )}
      />
      <List.Item
        title="Status Order"
        onPress={function () {
          navigation.navigate("ReportStatusOrder");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_STATUS_ORDER}
              data={{}}
              filename="report_excel_status_order"
            />
            <ExportPdf
              url={REPORT_STATUS_ORDER}
              data={{}}
              filename="report_status_order"
            />
          </View>
        )}
      />
      <List.Item
        title="Order Masuk Per Bulan"
        onPress={function () {
          navigation.navigate("ReportOrderMasukPerBulan");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_ORDER_MASUK_PER_BULAN}
              data={{}}
              filename="report_excel_order_masuk_per_bulan"
            />
            <ExportPdf
              url={REPORT_ORDER_MASUK_PER_BULAN}
              data={{}}
              filename="report_order_masuk_per_bulan"
            />
          </View>
        )}
      />
      <List.Item
        title="Data User"
        onPress={function () {
          navigation.navigate("ReportDataUser");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_DATA_USER}
              data={{}}
              filename="report_excel_data_user"
            />
            <ExportPdf
              url={REPORT_DATA_USER}
              data={{}}
              filename="report_data_user"
            />
          </View>
        )}
      />
      <List.Item
        title="Realisasi Muatan"
        onPress={function () {
          navigation.navigate("ReportRealisasiMuatan");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_REALISASI_MUATAN}
              data={{}}
              filename="report_excel_realisasi_muatan"
            />
            <ExportPdf
              url={REPORT_REALISASI_MUATAN}
              data={{}}
              filename="report_realisasi_muatan"
            />
          </View>
        )}
      />
      <List.Item
        title="Disparitas Harga"
        onPress={function () {
          navigation.navigate("ReportDisparitasHarga");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_DISPARITAS_HARGA}
              data={{}}
              filename="report_excel_disparitas_harga"
            />
            <ExportPdf
              url={REPORT_DISPARITAS_HARGA}
              data={{}}
              filename="report_disparitas_harga"
            />
          </View>
        )}
      />
      <List.Item
        title="Daftar Sisa Kuota"
        onPress={function () {
          navigation.navigate("ReportDaftarSisaKuota");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_DAFTAR_SISA_KUOTA}
              data={{}}
              filename="report_excel_daftar_sisa_kuota"
            />
            <ExportPdf
              url={REPORT_DAFTAR_SISA_KUOTA}
              data={{}}
              filename="report_daftar_sisa_kuota"
            />
          </View>
        )}
      />
      <List.Item
        title="Total Muatan Per Wilayah"
        onPress={function () {
          navigation.navigate("ReportTotalMuatanPerWilayah");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_TOTAL_MUATAN_PER_WILAYAH}
              data={{}}
              filename="report_excel_total_muatan_per_wilayah"
            />
            <ExportPdf
              url={REPORT_TOTAL_MUATAN_PER_WILAYAH}
              data={{}}
              filename="report_total_muatan_per_wilayah"
            />
          </View>
        )}
      />
      <List.Item
        title="Muatan Per Wilayah Per Prioritas"
        onPress={function () {
          navigation.navigate("ReportMuatanPerWilayahPerPrioritas");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_MUATAN_PER_WILAYAH_PER_PRIORITAS}
              data={{}}
              filename="report_excel_muatan_per_wilayah_per_prioritas"
            />
            <ExportPdf
              url={REPORT_MUATAN_PER_WILAYAH_PER_PRIORITAS}
              data={{}}
              filename="report_muatan_per_wilayah_per_prioritas"
            />
          </View>
        )}
      />
      <List.Item
        title="Realisasi Voyage Per Trayek"
        onPress={function () {
          navigation.navigate("ReportRealisasiVoyagePerTrayek");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_REALISASI_VOYAGE_PER_TRAYEK}
              data={{}}
              filename="report_excel_realisasi_voyage_per_trayek"
            />
            <ExportPdf
              url={REPORT_REALISASI_VOYAGE_PER_TRAYEK}
              data={{}}
              filename="report_realisasi_voyage_per_trayek"
            />
          </View>
        )}
      />
      <List.Item
        title="Waktu Tempuh"
        onPress={function () {
          navigation.navigate("ReportWaktuTempuh");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_WAKTU_TEMPUH}
              data={{}}
              filename="report_excel_waktu_tempuh"
            />
            <ExportPdf
              url={REPORT_WAKTU_TEMPUH}
              data={{}}
              filename="report_waktu_tempuh"
            />
          </View>
        )}
      />
      <List.Item
        title="Total Order Per Jenis Prioritas"
        onPress={function () {
          navigation.navigate("ReportTotalOrderPerJenisPrioritas");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_TOTAL_ORDER_PER_JENIS_PRIORITAS}
              data={{}}
              filename="report_excel_total_order_per_jenis_prioritas"
            />
            <ExportPdf
              url={REPORT_TOTAL_ORDER_PER_JENIS_PRIORITAS}
              data={{}}
              filename="report_total_order_per_jenis_prioritas"
            />
          </View>
        )}
      />
      <List.Item
        title="Jenis Muatan Terbanyak Prioritas"
        onPress={function () {
          navigation.navigate("ReportMuatanTerbanyakPerPrioritas");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_MUATAN_TERBANYAK_PER_PRIORITAS}
              data={{}}
              filename="report_excel_muatan_terbanyak_per_prioritas"
            />
            <ExportPdf
              url={REPORT_MUATAN_TERBANYAK_PER_PRIORITAS}
              data={{}}
              filename="report_muatan_terbanyak_per_prioritas"
            />
          </View>
        )}
      />
      <List.Item
        title="Total Container Per Pelabuhan"
        onPress={function () {
          navigation.navigate("ReportTotalContainerPerPelabuhan");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_TOTAL_CONTAINER_PER_BULAN}
              data={{}}
              filename="report_excel_total_container_per_bulan"
            />
            <ExportPdf
              url={REPORT_TOTAL_CONTAINER_PER_BULAN}
              data={{}}
              filename="report_total_container_per_bulan"
            />
          </View>
        )}
      />
      <List.Item
        title="Muatan Tol Laut"
        onPress={function () {
          navigation.navigate("ReportMuatanTolLaut");
        }}
        right={(props) => (
          <View style={{ flexDirection: "row" }}>
            <ExportXls
              url={REPORT_EXCEL_TOTAL_CONTAINER_PER_BULAN}
              data={{}}
              filename="report_excel_total_container_per_bulan"
            />
            <ExportPdf
              url={REPORT_TOTAL_CONTAINER_PER_BULAN}
              data={{}}
              filename="report_total_container_per_bulan"
            />
          </View>
        )}
      />
    </Root>
  );
}

const styles = StyleSheet.create({});
