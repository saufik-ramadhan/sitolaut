import React, { useEffect, useState } from "react";
import { Card, Paragraph, Caption } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  consigneeStatisticAll,
  consigneeStatisticActive,
  supplierStatisticAll,
  supplierStatisticActive,
  shipperStatisticAll,
  shipperStatisticActive,
  resellerStatisticAll,
  resellerStatisticActive,
  operatorStatisticAll,
  operatorStatisticActive,
  regulatorStatisticAll,
  regulatorStatisticActive,
} from "../../redux/dashboardReducer";
import { Heading5 } from "../../components";
import HorizontalList from "../../components/HorizontalList";
/**
 * Report User
 * Menampilkan Ringkasan Jumlah
 * user yang terdaftar di aplikasi LCS
 */
interface UserCard {
  name: string;
  active: number;
  registered: number;
  backgroundColor?: string;
  width?: number;
  borderRadius?: number;
  margin?: number;
}
function UserCard({
  name,
  active,
  registered,
  backgroundColor = "white",
  width = 100,
  borderRadius = 5,
  margin = 3,
}: UserCard) {
  return (
    <Card
      style={{
        elevation: 3,
        padding: 5,
        backgroundColor: backgroundColor,
        borderRadius: borderRadius,
        margin: margin,
      }}
    >
      <Paragraph>{name}</Paragraph>
      <Caption>
        <Caption>{active}</Caption> active of {"\n"}
        <Caption>{registered}</Caption> registered
      </Caption>
    </Card>
  );
}
export default function ReportUser() {
  const dispatch = useDispatch();
  const consigneeAll = useSelector(function (state: RootState) {
    return state.dashboard.consigneeSttcAllSuccess
      ? state.dashboard.consigneeSttcAllSuccess.data[0].count
      : 0;
  });
  const consigneeAct = useSelector(function (state: RootState) {
    return state.dashboard.consigneeSttcActiveSuccess
      ? state.dashboard.consigneeSttcActiveSuccess.data[0].count
      : 0;
  });
  const supplierAll = useSelector(function (state: RootState) {
    return state.dashboard.supplierSttcAllSuccess
      ? state.dashboard.supplierSttcAllSuccess.data[0].count
      : 0;
  });
  const supplierAct = useSelector(function (state: RootState) {
    return state.dashboard.supplierSttcActiveSuccess
      ? state.dashboard.supplierSttcActiveSuccess.data[0].count
      : 0;
  });
  const shipperAll = useSelector(function (state: RootState) {
    return state.dashboard.shipperSttcAllSuccess
      ? state.dashboard.shipperSttcAllSuccess.data[0].count
      : 0;
  });
  const shipperAct = useSelector(function (state: RootState) {
    return state.dashboard.shipperSttcActiveSuccess
      ? state.dashboard.shipperSttcActiveSuccess.data[0].count
      : 0;
  });
  const resellerAll = useSelector(function (state: RootState) {
    return state.dashboard.resellerSttcAllSuccess
      ? state.dashboard.resellerSttcAllSuccess.data[0].count
      : 0;
  });
  const resellerAct = useSelector(function (state: RootState) {
    return state.dashboard.resellerSttcActiveSuccess
      ? state.dashboard.resellerSttcActiveSuccess.data[0].count
      : 0;
  });
  const operatorAll = useSelector(function (state: RootState) {
    return state.dashboard.operatorSttcAllSuccess
      ? state.dashboard.operatorSttcAllSuccess.data[0].count
      : 0;
  });
  const operatorAct = useSelector(function (state: RootState) {
    return state.dashboard.operatorSttcActiveSuccess
      ? state.dashboard.operatorSttcActiveSuccess.data[0].count
      : 0;
  });
  const regulatorAll = useSelector(function (state: RootState) {
    return state.dashboard.regulatorSttcAllSuccess
      ? state.dashboard.regulatorSttcAllSuccess.data[0].count
      : 0;
  });
  const regulatorAct = useSelector(function (state: RootState) {
    return state.dashboard.regulatorSttcActiveSuccess
      ? state.dashboard.regulatorSttcActiveSuccess.data[0].count
      : 0;
  });
  const [refreshing, setRefreshing] = useState(false);
  function fetchData() {
    if (refreshing) {
      dispatch(consigneeStatisticAll());
      dispatch(consigneeStatisticActive());
      dispatch(supplierStatisticAll());
      dispatch(supplierStatisticActive());
      dispatch(shipperStatisticAll());
      dispatch(shipperStatisticActive());
      dispatch(resellerStatisticAll());
      dispatch(resellerStatisticActive());
      dispatch(operatorStatisticAll());
      dispatch(operatorStatisticActive());
      dispatch(regulatorStatisticAll());
      dispatch(regulatorStatisticActive());
    } else {
      null;
    }
  }
  useEffect(() => {
    fetchData();
  }, [refreshing]);
  return (
    <>
      <UserCard
        name="Consignee"
        active={consigneeAct}
        registered={consigneeAll}
      />
      <UserCard name="Supplier" active={supplierAct} registered={supplierAll} />
      <UserCard name="Shipper" active={shipperAct} registered={shipperAll} />
      <UserCard name="Reseller" active={resellerAct} registered={resellerAll} />
      <UserCard name="Operator" active={operatorAct} registered={operatorAll} />
      <UserCard
        name="Regulator"
        active={regulatorAct}
        registered={regulatorAll}
      />
    </>
  );
}
