import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
} from "../../components";
import { jenisUser, statusUser, bulanId } from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll } from "../../redux/userReducer";
import { kotaFetchByProvinsi } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import {
  REPORT_DATA_USER,
  REPORT_EXCEL_REALISASI_MUATAN,
  REPORT_REALISASI_MUATAN,
} from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import { muatanPerOperator } from "../../redux/dashboardReducer";
import ExportXls from "../../services/ExportXls";

/**
 * Order Per Bulan
 */
export default function ReportRealisasiMuatan({
  navigation,
  route,
}: RegulatorStackProps<"ReportRealisasiMuatan">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    month: String(now.getMonth() + 1),
    operator: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const muatan_per_operator = useSelector(function (state: RootState) {
    return state.dashboard.muatanPerOperatorSuccess
      ? state.dashboard.muatanPerOperatorSuccess.data
      : [];
  });
  const { currentPage, pageTotal } = useSelector(function (state: RootState) {
    return state.dashboard.muatanPerOperatorSuccess;
  });

  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(muatanPerOperator(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
  }, [filter]);

  return (
    <>
      <FlatList
        data={muatan_per_operator}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection: 'row'}}>
            <ExportXls
              url={REPORT_EXCEL_REALISASI_MUATAN}
              data={filter}
              filename="report_realisasi_muatan"
            />
            <ExportPdf
              url={REPORT_REALISASI_MUATAN}
              data={filter}
              filename="report_realisasi_muatan"
            />
          </View>
        }
        renderItem={({ item }) => (
          <Card
            style={{
              borderRadius: 10,
              marginVertical: 2,
              padding: 10,
              marginHorizontal: 3,
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 3 }}>
                <Caption>
                  Trayek / Voyage
                  <Paragraph>
                    {" "}
                    {item.kode_trayek}/{item.voyage}
                  </Paragraph>
                </Caption>
                <Caption>
                  Operator
                  <Paragraph> {item.nama_perusahaan_operator}</Paragraph>
                </Caption>
                <Caption>
                  Rute
                  <Paragraph>
                    {" "}
                    {item.pelabuhan_asal} ➤ {item.pelabuhan_tujuan}
                  </Paragraph>
                </Caption>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Paragraph style={{ textAlign: "center" }}>
                  {parseInt(item.percentage)}%
                </Paragraph>
                <Badge style={{ backgroundColor: Colors.success }}>
                  Terpenuhi
                </Badge>
              </View>
            </View>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            month: String(now.getMonth() + 1),
            operator: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Nama Operator
               */}
              <TextInput
                value={values.operator}
                onChangeText={handleChange("operator")}
                onBlur={handleBlur("operator")}
                label="Nama Operator"
              />

              {/**
               * Bulan
               */}
              <SelectInput
                label="Bulan"
                onChangeValue={(val) => setFieldValue("month", val.id)}
                options={bulanId}
                objectKey="label"
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}
