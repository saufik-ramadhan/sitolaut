import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
  DateTime,
} from "../../components";
import {
  jenisUser,
  statusUser,
  bulanId,
  formatRupiah,
  DateFormat,
  DateFormatStrip,
} from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll } from "../../redux/userReducer";
import { kotaFetchByProvinsi, kotaFetchAll } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import {
  REPORT_DATA_USER,
  REPORT_REALISASI_MUATAN,
  REPORT_DISPARITAS_HARGA,
  REPORT_DAFTAR_SISA_KUOTA,
  REPORT_TOTAL_MUATAN_PER_WILAYAH,
  REPORT_EXCEL_TOTAL_MUATAN_PER_WILAYAH,
} from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import {
  muatanPerOperator,
  disparitasHarga,
  sisaQuotaTrayek,
  muatanPerWilayah,
} from "../../redux/dashboardReducer";
import { isNull } from "lodash";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { commodityFetchAll } from "../../redux/commodityReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { operatorFetchAll } from "../../redux/operatorReducer";
import { portFetchAll } from "../../redux/portReducer";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import ExportXls from "../../services/ExportXls";

function Row({ children }) {
  return <View style={{ flexDirection: "row" }}>{children}</View>;
}
/**
 * Order Per Bulan
 */
export default function ReportDisparitasHarga({
  navigation,
  route,
}: RegulatorStackProps<"ReportDisparitasHarga">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    date_end: "",
    date_start: "",
    jenis_barang: "",
    month: "",
    nama_barang: "",
    wilayah: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const muatan_per_wilayah = useSelector(function (state: RootState) {
    return state.dashboard.muatanPerWilayahSuccess
      ? state.dashboard.muatanPerWilayahSuccess.data
      : [];
  });
  const port_list = useSelector((state: RootState) =>
    state.port.portGetSuccess ? state.port.portGetSuccess.data : []
  );
  const jenisbarang_list = useSelector((state: RootState) =>
    state.jenisbarang.jenisbarangGetSuccess
      ? state.jenisbarang.jenisbarangGetSuccess.data
      : []
  );
  const { currentPage, pageTotal } = useSelector(function (state: RootState) {
    return state.dashboard.muatanPerWilayahSuccess;
  });

  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(muatanPerWilayah(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
    dispatch(portFetchAll());
    dispatch(jenisbarangFetchAll());
  }, [filter]);

  return (
    <>
      <FlatList
        data={muatan_per_wilayah}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection: 'row'}}>
            <ExportXls
              url={REPORT_EXCEL_TOTAL_MUATAN_PER_WILAYAH}
              data={filter}
              filename="report_excel_total_muatan_per_wilayah"
            />
            <ExportPdf
              url={REPORT_TOTAL_MUATAN_PER_WILAYAH}
              data={filter}
              filename="report_total_muatan_per_wilayah"
            />
          </View>
          
        }
        renderItem={({ item }) => (
          <Card style={{ padding: 10, borderRadius: 10, margin: 3 }}>
            <Caption>Wilayah: {item.pelabuhan}</Caption>
            <Caption>Jenis Barang: {item.jenis_barang}</Caption>
            <Caption>
              Nama Barang: {item.nama_barang} ({item.total_muatan})
            </Caption>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            date_end: "",
            date_start: "",
            jenis_barang: "",
            month: "",
            nama_barang: "",
            wilayah: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
            // alert(JSON.stringify(val));
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Jenis Barang
               */}
              <SelectInput
                label="Jenis Barang"
                onChangeValue={(val) => setFieldValue("jenis_barang", val.id)}
                options={jenisbarang_list}
                objectKey="label"
              />
              {/**
               * Bulan
               */}
              <SelectInput
                label="Bulan"
                onChangeValue={(val) => setFieldValue("month", val.id)}
                options={bulanId}
                objectKey="label"
              />

              {/**
               * Wilayah
               */}
              <SelectInput
                label="Wilayah"
                onChangeValue={(val) => setFieldValue("wilayah", val.id)}
                options={port_list}
                objectKey="label"
              />

              <TextInput
                value={values.nama_barang}
                onChangeText={handleChange("nama_barang")}
                onBlur={handleBlur("nama_barang")}
                label="Nama Barang"
              />

              {/**
               * Dari
               */}

              <DateTime
                label="Dari"
                onChangeValue={(date) =>
                  setFieldValue("date_start", DateFormatStrip(date))
                }
              />
              {/**
               * Sampai
               */}
              <DateTime
                label="Sampai"
                onChangeValue={(date) =>
                  setFieldValue("date_end", DateFormatStrip(date))
                }
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}

/** 



*/

// import React from "react";
// import { RegulatorStackProps } from "../Navigator";
// import { useSelector, useDispatch } from "react-redux";
// import { RootState } from "../../redux/rootReducer";
// import { muatanPerWilayah } from "../../redux/dashboardReducer";
// import { Heading5 } from "../../components";
// import HorizontalList from "../../components/HorizontalList";
// import { Card, Caption } from "react-native-paper";
// /**
//  * Total Muatan Per Wilayah
//  */
// const muatan_per_wilayah = [
//   {
//     wilayah: "Belawan",
//     jenis_barang: "Kebutuhan Pokok",
//     nama_barang: "Beras",
//     total_muatan: "2000 kg",
//   },
// ];
// export default function MuatanPerWilayah({
//   navigation,
//   route,
// }: RegulatorStackProps<"Dashboard">) {
//   const { id, usertype } = useSelector(function (state: RootState) {
//     return state.auth.authLoginSuccess
//       ? state.auth.authLoginSuccess.data.user[0]
//       : {
//           usertype: "Not Defined",
//         };
//   });
//   const dispatch = useDispatch();

//   React.useEffect(() => {
//     dispatch(
//       muatanPerWilayah({

//       })
//     );
//   }, []);
//   return (
//     <>
//       <Heading5>Total Muatan Per Wilayah</Heading5>
//       {muatan_per_wilayah.map((item, key) => (

//       ))}
//     </>
//   );
// }

/**
 * 
 * 
 * 
 * 
 
 
 */
