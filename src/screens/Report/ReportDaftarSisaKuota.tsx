import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
} from "../../components";
import {
  jenisUser,
  statusUser,
  bulanId,
  formatRupiah,
  DateFormat,
} from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll } from "../../redux/userReducer";
import { kotaFetchByProvinsi, kotaFetchAll } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import {
  REPORT_DATA_USER,
  REPORT_REALISASI_MUATAN,
  REPORT_DISPARITAS_HARGA,
  REPORT_DAFTAR_SISA_KUOTA,
  REPORT_EXCEL_DAFTAR_SISA_KUOTA,
} from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import {
  muatanPerOperator,
  disparitasHarga,
  sisaQuotaTrayek,
} from "../../redux/dashboardReducer";
import { isNull } from "lodash";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { commodityFetchAll } from "../../redux/commodityReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { operatorFetchAll } from "../../redux/operatorReducer";
import { portFetchAll } from "../../redux/portReducer";
import ExportXls from "../../services/ExportXls";

function Row({ children }) {
  return <View style={{ flexDirection: "row" }}>{children}</View>;
}
/**
 * Order Per Bulan
 */
export default function ReportDisparitasHarga({
  navigation,
  route,
}: RegulatorStackProps<"ReportDisparitasHarga">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    month: String(new Date().getMonth() + 1),
    operator_id: "",
    port_destination_id: "",
    port_origin_id: "",
    trayek_id: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const daftar_sisa_kuota = useSelector(function (state: RootState) {
    return state.dashboard.sisaQuotaTrayekSuccess
      ? state.dashboard.sisaQuotaTrayekSuccess.data
      : [];
  });
  const msttrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetSuccess
      ? state.msttrayek.msttrayekGetSuccess.data
      : [];
  });
  const port_list = useSelector((state: RootState) =>
    state.port.portGetSuccess ? state.port.portGetSuccess.data : []
  );
  const operator_list = useSelector((state: RootState) =>
    state.operator.operatorGetSuccess
      ? state.operator.operatorGetSuccess.data
      : []
  );
  const { currentPage, pageTotal } = useSelector(function (state: RootState) {
    return state.dashboard.sisaQuotaTrayekSuccess;
  });

  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(sisaQuotaTrayek(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
    dispatch(msttrayekFetchAll());
    dispatch(operatorFetchAll());
    dispatch(portFetchAll());
  }, [filter]);

  return (
    <>
      <FlatList
        data={daftar_sisa_kuota}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection:'row'}}>
            <ExportXls
              url={REPORT_EXCEL_DAFTAR_SISA_KUOTA}
              data={filter}
              filename="report_daftar_sisa_kuota"
            />
            <ExportPdf
              url={REPORT_DAFTAR_SISA_KUOTA}
              data={filter}
              filename="report_daftar_sisa_kuota"
            />
          </View>
        }
        renderItem={({ item }) => (
          <Card style={{ padding: 10, borderRadius: 10, margin: 3 }}>
            <Caption>Operator : {item.operator}</Caption>
            <Row>
              <View style={{ flex: 3 }}>
                <Caption>
                  Trayek/Voyage: {item.kode_trayek} / {item.voyage}
                </Caption>
                <Caption>
                  {item.pelabuhan_asal} ➤ {item.pelabuhan_tujuan}
                </Caption>
                <Caption>
                  {DateFormat(item.etd)} ➤ {DateFormat(item.eta)}
                </Caption>
              </View>
              <View style={{ flex: 1 }}>
                <Badge>{item.type_container_name}</Badge>
                <Caption style={{ textAlign: "right" }}>
                  Kuota:{"\n"}({item.total_sisa}/{item.total_alokasi_quota})
                </Caption>
              </View>
            </Row>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            month: "",
            operator_id: "",
            port_destination_id: "",
            port_origin_id: "",
            trayek_id: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
            // alert(JSON.stringify(val));
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Trayek
               */}
              <SelectInput
                label="Trayek"
                onChangeValue={(val) => setFieldValue("trayek_id", val.id)}
                options={msttrayek}
                objectKey="label"
              />
              {/**
               * Bulan
               */}
              <SelectInput
                label="Bulan"
                onChangeValue={(val) => setFieldValue("month", val.id)}
                options={bulanId}
                objectKey="label"
              />
              {/**
               * Operator
               */}
              <SelectInput
                label="Operator"
                onChangeValue={(val) => setFieldValue("operator_id", val.id)}
                options={operator_list}
                objectKey="label"
              />
              {/**
               * Pelabuhan Asal
               */}
              <SelectInput
                label="Pelabuhan Asal"
                onChangeValue={(val) => setFieldValue("port_origin_id", val.id)}
                options={port_list}
                objectKey="label"
              />
              {/**
               * Pelabuhan Tujuan
               */}
              <SelectInput
                label="Pelabuhan Tujuan"
                onChangeValue={(val) =>
                  setFieldValue("port_destination_id", val.id)
                }
                options={port_list}
                objectKey="label"
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}

/** 



*/
