import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
} from "../../components";
import { jenisUser, statusUser } from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll, userFetchPage } from "../../redux/userReducer";
import { kotaFetchByProvinsi } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import { REPORT_DATA_USER, REPORT_EXCEL_DATA_USER } from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import ExportXls from "../../services/ExportXls";

/**
 * Order Per Bulan
 */
export default function DataUser({
  navigation,
  route,
}: RegulatorStackProps<"DataUser">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    email: "",
    kota_id: "",
    name: "",
    provinsi_id: "",
    status_id: "",
    user_type_id: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const data_user = useSelector(function (state: RootState) {
    return state.user.userGetSuccess ? state.user.userGetSuccess.data : [];
  });
  const { currentPage, pageTotal } = useSelector(function (state: RootState) {
    return state.user.userGetSuccess;
  });
  const msttrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetSuccess
      ? state.msttrayek.msttrayekGetSuccess.data
      : [];
  });
  const pelabuhan = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  const provinsi = useSelector(function (state: RootState) {
    return state.provinsi.provinsiGetSuccess
      ? state.provinsi.provinsiGetSuccess.data
      : [];
  });
  const kota = useSelector(function (state: RootState) {
    return state.kota.kotaGetSuccess ? state.kota.kotaGetSuccess.data : [];
  });
  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    // alert(JSON.stringify(params));
    dispatch(userFetchPage(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
    dispatch(provinsiFetchAll());
  }, [filter]);

  return (
    <>
      <FlatList
        data={data_user}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection: 'row'}}>
            <ExportXls
              url={REPORT_EXCEL_DATA_USER}
              data={filter}
              filename="report_data_user"
            />
            <ExportPdf
              url={REPORT_DATA_USER}
              data={filter}
              filename="report_data_user"
            />
          </View>
        }
        renderItem={({ item }) => (
          <Card
            elevation={4}
            style={{ margin: 3, borderRadius: 3, padding: 10 }}
            onPress={function () {
              navigation.navigate("ReadAktivasiUser", {
                id: item.id,
                usery_type_id: item.usery_type_id,
              });
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <Badge
                style={{
                  backgroundColor:
                    item.status === 1 ? Colors.success : Colors.danger,
                  alignSelf: "flex-start",
                }}
              >
                {item.status === 1 ? "Aktif" : "Non-Aktif"}
              </Badge>
              <Badge
                style={{
                  backgroundColor: Colors.grayL,
                  alignSelf: "flex-start",
                }}
              >
                {
                  item.usery_type_id == 1 ? 'Consignee' : 
                  item.usery_type_id == 2 ? 'Supplier' : 
                  item.usery_type_id == 3 ? 'Shipper' :
                  item.usery_type_id == 4 ? 'Reseller' :
                  item.usery_type_id == 5 ? 'Operator' :
                  item.usery_type_id == 6 ? 'Regulator' :
                  null
                }
              </Badge>
            </View>
            <Caption style={{ flex: 3 }}>
              <Paragraph style={{ fontWeight: "bold", flex: 4 }}>
                {item.nama_consignee ||
                  item.nama_supplier ||
                  item.nama_shipper ||
                  item.nama_reseller ||
                  item.nama_operator ||
                  item.nama_regulator}
                {"\n"}
              </Paragraph>
              <Caption>
                {item.consignee_alamat ||
                  item.supplier_alamat ||
                  item.shipper_alamat ||
                  item.reseller_alamat ||
                  item.operator_alamat ||
                  item.regulator_alamat}
              </Caption>
              <Caption style={{ fontWeight: "bold", color: Colors.skyblue }}>
                {item.email}
              </Caption>
              {/* <Caption style={{ fontWeight: "bold" }}>
                Telp :{" "}
                <Paragraph>
                  {item.consignee_telp ||
                    item.supplier_telp ||
                    item.shipper_telp ||
                    item.reseller_telp ||
                    item.operator_telp ||
                    item.regulator_telp}
                  {"\n"}
                </Paragraph>
              </Caption>
              <Caption style={{ fontWeight: "bold" }}>
                {item.consignee_kota_name ||
                  item.supplier_kota_name ||
                  item.shipper_kota_name ||
                  item.reseller_kota_name ||
                  item.operator_kota_name ||
                  item.regulator_kota_name}
                ,
                {item.consignee_provinsi_name ||
                  item.supplier_provinsi_name ||
                  item.shipper_provinsi_name ||
                  item.reseller_provinsi_name ||
                  item.operator_provinsi_name ||
                  item.regulator_provinsi_name}
              </Caption> */}
            </Caption>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            email: "",
            kota_id: "",
            name: "",
            provinsi_id: "",
            status_id: "",
            user_type_id: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Nama Perusahaan
               */}
              <TextInput
                value={values.name}
                onChangeText={handleChange("name")}
                onBlur={handleBlur("name")}
                label="Nama Perusahaan"
              />
              {/**
               * Email
               */}
              <TextInput
                value={values.email}
                onChangeText={handleChange("email")}
                onBlur={handleBlur("email")}
                label="Email"
              />
              {/**
               * Jenis User
               */}
              <SelectInput
                label="Jenis User"
                onChangeValue={(val) => setFieldValue("user_type_id", val.id)}
                options={jenisUser}
                objectKey="label"
              />

              {/**
               * Status User
               */}
              <SelectInput
                label="Status User"
                onChangeValue={(val) => setFieldValue("status_id", val.id)}
                options={statusUser}
                objectKey="label"
              />

              {/**
               * Provinsi
               */}
              <SelectInput
                label="Provinsi"
                onChangeValue={(val) => {
                  setFieldValue("provinsi_id", val.id);
                  dispatch(kotaFetchByProvinsi(val.id));
                }}
                options={provinsi}
                objectKey="label"
              />

              {/**
               * Kota
               */}
              <SelectInput
                label="Kota"
                onChangeValue={(val) => setFieldValue("kota_id", val.id)}
                options={kota}
                objectKey="label"
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}
