import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
  DateTime,
} from "../../components";
import {
  jenisUser,
  statusUser,
  bulanId,
  formatRupiah,
  DateFormat,
  DateFormatStrip,
} from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll } from "../../redux/userReducer";
import { kotaFetchByProvinsi, kotaFetchAll } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import {
  REPORT_DATA_USER,
  REPORT_REALISASI_MUATAN,
  REPORT_DISPARITAS_HARGA,
  REPORT_DAFTAR_SISA_KUOTA,
  REPORT_REALISASI_VOYAGE_PER_TRAYEK,
  REPORT_WAKTU_TEMPUH,
  REPORT_TOTAL_ORDER_PER_JENIS_PRIORITAS,
  REPORT_MUATAN_TERBANYAK_PER_PRIORITAS,
  REPORT_TOTAL_CONTAINER_PER_BULAN,
  REPORT_EXCEL_TOTAL_CONTAINER_PER_BULAN,
} from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import {
  muatanPerOperator,
  disparitasHarga,
  sisaQuotaTrayek,
  realisasi,
  waktuTempuh,
  orderPerJenisPrioritas,
  muatanTerbanyak,
  containerPerTahun,
} from "../../redux/dashboardReducer";
import { isNull } from "lodash";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { commodityFetchAll } from "../../redux/commodityReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { operatorFetchAll } from "../../redux/operatorReducer";
import { portFetchAll } from "../../redux/portReducer";
import { unitFetchAll } from "../../redux/unitReducer";
import ExportXls from "../../services/ExportXls";

function Row({ children }) {
  return <View style={{ flexDirection: "row" }}>{children}</View>;
}
/**
 * Order Per Bulan
 */
export default function ReportTotalContainerPerPelabuhan({
  navigation,
  route,
}: RegulatorStackProps<"ReportTotalContainerPerPelabuhan">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    tahun: String(now.getFullYear()),
  });
  const range = 10;

  const dispatch = useDispatch();

  const total_container_per_pelabuhan = useSelector(function (
    state: RootState
  ) {
    return state.dashboard.containerPerTahunSuccess
      ? state.dashboard.containerPerTahunSuccess.data
      : [];
  });
  const { currentPage, pageTotal, tahun } = useSelector(function (
    state: RootState
  ) {
    return state.dashboard.containerPerTahunSuccess;
  });
  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(containerPerTahun(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
  }, [filter]);

  return (
    <>
      <FlatList
        data={total_container_per_pelabuhan}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection: 'row'}}>
            <ExportXls
              url={REPORT_EXCEL_TOTAL_CONTAINER_PER_BULAN}
              data={filter}
              filename="report_excel_total_container_per_bulan"
            />
            <ExportPdf
              url={REPORT_TOTAL_CONTAINER_PER_BULAN}
              data={filter}
              filename="report_total_container_per_bulan"
            />
          </View>
        }
        renderItem={({ item }) => (
          <Card style={{ padding: 10, margin: 5, borderRadius: 5 }}>
            <Caption>{item.pelabuhan}</Caption>
            <Caption>Total Container : {item.total}</Caption>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            tahun: String(now.getFullYear()),
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
            // alert(JSON.stringify(val));
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Tahun
               */}
              <SelectInput
                label="Tahun"
                onChangeValue={(val) => setFieldValue("tahun", val.label)}
                options={tahun}
                objectKey="label"
              />
              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}

/** 



*/
