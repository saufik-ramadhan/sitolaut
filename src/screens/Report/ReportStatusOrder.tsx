import React, { useEffect } from "react";
import { Caption, Card, Paragraph } from "react-native-paper";
import { View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  allOrder,
  orderPaid,
  orderUnpaid,
  orderDenied,
} from "../../redux/dashboardReducer";
import { Heading5, Icon } from "../../components";
import HorizontalList from "../../components/HorizontalList";
import Colors from "../../config/Colors";

/**
 * Status Order
 */
interface Order {
  name?: string;
  count?: number;
  icon?: string;
  backgroundColor?: string;
  iconColor?: string;
  width?: number;
}
function Order({
  name,
  count,
  icon,
  backgroundColor,
  iconColor,
  width = 100,
}: Order) {
  return (
    <Card style={{ elevation: 3, margin: 3, padding: 5, borderRadius: 5 }}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          padding: 3,
        }}
      >
        <Icon
          name={icon}
          style={{
            fontSize: 25,
            position: "absolute",
            color: iconColor || Colors.gray1,
            bottom: 0,
            right: 0,
            opacity: 0.7,
          }}
        />
        <View style={{ flex: 2 }}>
          <Paragraph style={{ fontSize: 20 }}>
            <Caption>{name}</Caption>

            {"\n"}
            {count}
          </Paragraph>
        </View>
      </View>
    </Card>
  );
}
export default function StatusOrder() {
  const dispatch = useDispatch();

  const allOrderData = useSelector(function (state: RootState) {
    return state.dashboard.allOrderSuccess
      ? state.dashboard.allOrderSuccess.data[0].count
      : 0;
  });
  const orderPaidData = useSelector(function (state: RootState) {
    return state.dashboard.orderPaidSuccess
      ? state.dashboard.orderPaidSuccess.data[0].count
      : 0;
  });
  const orderUnpaidData = useSelector(function (state: RootState) {
    return state.dashboard.orderUnpaidSuccess
      ? state.dashboard.orderUnpaidSuccess.data[0].count
      : 0;
  });
  const orderDeniedData = useSelector(function (state: RootState) {
    return state.dashboard.orderDeniedSuccess
      ? state.dashboard.orderDeniedSuccess.data[0].count
      : 0;
  });

  useEffect(() => {
    dispatch(allOrder());
    dispatch(orderPaid());
    dispatch(orderUnpaid());
    dispatch(orderDenied());
  }, []);
  return (
    <>
      <Order
        name="Total Order"
        count={allOrderData}
        icon="cart"
        iconColor={Colors.slateblue}
      />
      <Order
        name="Lunas"
        count={orderPaidData}
        icon="cash"
        iconColor={Colors.seagreen}
      />
      <Order
        name="Belum Lunas"
        count={orderUnpaidData}
        icon="alert-circle"
        iconColor={Colors.warning}
      />
      <Order
        name="Ditolak"
        count={orderDeniedData}
        icon="close-circle"
        iconColor={Colors.danger}
      />
    </>
  );
}
