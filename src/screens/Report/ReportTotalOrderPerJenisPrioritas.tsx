import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
  DateTime,
} from "../../components";
import {
  jenisUser,
  statusUser,
  bulanId,
  formatRupiah,
  DateFormat,
  DateFormatStrip,
} from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll } from "../../redux/userReducer";
import { kotaFetchByProvinsi, kotaFetchAll } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import {
  REPORT_DATA_USER,
  REPORT_REALISASI_MUATAN,
  REPORT_DISPARITAS_HARGA,
  REPORT_DAFTAR_SISA_KUOTA,
  REPORT_REALISASI_VOYAGE_PER_TRAYEK,
  REPORT_WAKTU_TEMPUH,
  REPORT_TOTAL_ORDER_PER_JENIS_PRIORITAS,
  REPORT_EXCEL_TOTAL_ORDER_PER_JENIS_PRIORITAS,
} from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import {
  muatanPerOperator,
  disparitasHarga,
  sisaQuotaTrayek,
  realisasi,
  waktuTempuh,
  orderPerJenisPrioritas,
} from "../../redux/dashboardReducer";
import { isNull } from "lodash";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { commodityFetchAll } from "../../redux/commodityReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { operatorFetchAll } from "../../redux/operatorReducer";
import { portFetchAll } from "../../redux/portReducer";
import ExportXls from "../../services/ExportXls";

function Row({ children }) {
  return <View style={{ flexDirection: "row" }}>{children}</View>;
}
/**
 * Order Per Bulan
 */
export default function ReportTotalOrderPerJenisPrioritas({
  navigation,
  route,
}: RegulatorStackProps<"ReportTotalOrderPerJenisPrioritas">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    date_end: "",
    date_start: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const total_order_per_jenis_prioritas = useSelector(function (
    state: RootState
  ) {
    return state.dashboard.orderPerJenisPrioritasSuccess
      ? state.dashboard.orderPerJenisPrioritasSuccess.data
      : [];
  });
  const { currentPage, pageTotal } = useSelector(function (state: RootState) {
    return state.dashboard.orderPerJenisPrioritasSuccess;
  });
  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(orderPerJenisPrioritas(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
  }, [filter]);

  return (
    <>
      <FlatList
        data={total_order_per_jenis_prioritas}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection: 'row'}}>
            <ExportXls
              url={REPORT_EXCEL_TOTAL_ORDER_PER_JENIS_PRIORITAS}
              data={filter}
              filename="report_excel_total_order_per_jenis_prioritas"
            />
            <ExportPdf
              url={REPORT_TOTAL_ORDER_PER_JENIS_PRIORITAS}
              data={filter}
              filename="report_total_order_per_jenis_prioritas"
            />
          </View>
          
        }
        renderItem={({ item }) => (
          <Card style={{ margin: 3, borderRadius: 5, padding: 10 }}>
            <Caption>
              {item.jenis_barang} {"\n"}{" "}
              <Paragraph style={{ color: Colors.def, fontSize: 18 }}>
                {item.jml_order}
              </Paragraph>
            </Caption>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            date_end: "",
            date_start: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
            // alert(JSON.stringify(val));
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Dari
               */}

              <DateTime
                label="Dari"
                onChangeValue={(date) =>
                  setFieldValue("date_start", DateFormatStrip(date))
                }
              />
              {/**
               * Sampai
               */}
              <DateTime
                label="Sampai"
                onChangeValue={(date) =>
                  setFieldValue("date_end", DateFormatStrip(date))
                }
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}

/** 



*/
