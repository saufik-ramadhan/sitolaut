import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
} from "../../components";
import {
  jenisUser,
  statusUser,
  bulanId,
  formatRupiah,
} from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll } from "../../redux/userReducer";
import { kotaFetchByProvinsi, kotaFetchAll } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import {
  REPORT_DATA_USER,
  REPORT_REALISASI_MUATAN,
  REPORT_DISPARITAS_HARGA,
  REPORT_EXCEL_DISPARITAS_HARGA,
} from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import {
  muatanPerOperator,
  disparitasHarga,
} from "../../redux/dashboardReducer";
import { isNull } from "lodash";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { commodityFetchAll } from "../../redux/commodityReducer";
import ExportXls from "../../services/ExportXls";

function Row({ children }) {
  return <View style={{ flexDirection: "row" }}>{children}</View>;
}
/**
 * Order Per Bulan
 */
export default function ReportDisparitasHarga({
  navigation,
  route,
}: RegulatorStackProps<"ReportDisparitasHarga">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    barang_id: "",
    consignee_id: "",
    kota_asal: "",
    kota_tujuan: "",
    reseller_id: "",
    supplier_id: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const disparitas_harga = useSelector(function (state: RootState) {
    return state.dashboard.disparitasHargaSuccess
      ? state.dashboard.disparitasHargaSuccess.data
      : [];
  });
  const commodity_list = useSelector((state: RootState) =>
    state.commodity.commodityGetSuccess
      ? state.commodity.commodityGetSuccess.data
      : []
  );
  const consignee_list = useSelector((state: RootState) =>
    state.consignee.consigneeGetSuccess
      ? state.consignee.consigneeGetSuccess.data
      : []
  );
  const supplier_list = useSelector((state: RootState) =>
    state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : []
  );
  const reseller_list = useSelector((state: RootState) =>
    state.reseller.resellerGetSuccess
      ? state.reseller.resellerGetSuccess.data
      : []
  );
  const kota_list = useSelector((state: RootState) =>
    state.kota.kotaGetSuccess ? state.kota.kotaGetSuccess.data : []
  );
  const { currentPage, pageTotal } = useSelector(function (state: RootState) {
    return state.dashboard.disparitasHargaSuccess;
  });

  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(disparitasHarga(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
    dispatch(commodityFetchAll());
    dispatch(consigneeFetchAll());
    dispatch(supplierFetchAll());
    dispatch(resellerFetchAll());
    dispatch(kotaFetchAll());
  }, [filter]);

  return (
    <>
      <FlatList
        data={disparitas_harga}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection: 'row'}}>
            <ExportXls
              url={REPORT_EXCEL_DISPARITAS_HARGA}
              data={filter}
              filename="report_disparitas_harga"
            />
            <ExportPdf
              url={REPORT_DISPARITAS_HARGA}
              data={filter}
              filename="report_disparitas_harga"
            />
          </View>
          
        }
        renderItem={({ item }) => (
          <Card
            style={{
              padding: 10,
              borderRadius: 10,
              margin: 3,
            }}
          >
            <Row>
              <Paragraph style={{ flex: 1 }}>{item.nama_barang}</Paragraph>
            </Row>

            <Row>
              <Caption style={{ flex: 2 }}>
                {item.supplier_name}
                {"\n"}({item.supplier_kota})
              </Caption>
              <Caption
                style={{ color: "orangered", flex: 1, textAlign: "right" }}
              >
                Rp.{formatRupiah(parseInt(item.harga_satuan))} / {item.satuan}
              </Caption>
            </Row>

            {!isNull(item.harga_satuan_consignee) && (
              <Row>
                <Caption style={{ flex: 2 }}>
                  {item.consignee_name}
                  {"\n"}({item.consignee_kota})
                </Caption>
                <Caption
                  style={{ color: "orangered", flex: 1, textAlign: "right" }}
                >
                  Rp.{formatRupiah(parseInt(item.harga_satuan_consignee))}
                </Caption>
              </Row>
            )}

            {!isNull(item.harga_satuan_jual) && (
              <Row>
                <Caption style={{ flex: 2 }}>
                  {item.reseller_name}
                  {"\n"}({item.reseller_kota})
                </Caption>
                <Caption
                  style={{ color: "orangered", flex: 1, textAlign: "right" }}
                >
                  Rp.{formatRupiah(parseInt(item.harga_satuan_jual))}
                </Caption>
              </Row>
            )}
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            barang_id: "",
            consignee_id: "",
            kota_asal: "",
            kota_tujuan: "",
            reseller_id: "",
            supplier_id: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
            // alert(JSON.stringify(val));
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               *
               */}

              {/**
               * Nama Operator
               */}
              {/* <TextInput
                value={values.operator}
                onChangeText={handleChange("operator")}
                onBlur={handleBlur("operator")}
                label="Nama Operator"
              /> */}

              {/**
               * Nama Barang
               */}
              <SelectInput
                label="Nama Barang"
                onChangeValue={(val) => setFieldValue("barang_id", val.id)}
                options={commodity_list}
                objectKey="nama_barang"
                withSearch
              />

              {/**
               * Consignee
               */}
              <SelectInput
                label="Consignee"
                onChangeValue={(val) => setFieldValue("consignee_id", val.id)}
                options={consignee_list}
                objectKey="label"
                withSearch
              />

              {/**
               * Supplier
               */}
              <SelectInput
                label="Supplier"
                onChangeValue={(val) => setFieldValue("supplier_id", val.id)}
                options={supplier_list}
                objectKey="label"
                withSearch
              />

              {/**
               * Reseller
               */}
              <SelectInput
                label="Reseller"
                onChangeValue={(val) => setFieldValue("reseller_id", val.id)}
                options={reseller_list}
                objectKey="label"
                withSearch
              />

              {/**
               * Kota Asal
               */}
              <SelectInput
                label="Kota Asal"
                onChangeValue={(val) => setFieldValue("kota_asal", val.id)}
                options={kota_list}
                objectKey="label"
                withSearch
              />

              {/**
               * Kota Tujuan
               */}
              <SelectInput
                label="Kota Tujuan"
                onChangeValue={(val) => setFieldValue("kota_tujuan", val.id)}
                options={kota_list}
                objectKey="label"
                withSearch
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}

{
  /**



*/
}
