import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
  DateTime,
} from "../../components";
import {
  jenisUser,
  statusUser,
  bulanId,
  formatRupiah,
  DateFormat,
  DateFormatStrip,
} from "../../services/utils";
import {
  Card,
  Caption,
  Button,
  TextInput,
  Badge,
  Paragraph,
} from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { userAll } from "../../redux/userReducer";
import { kotaFetchByProvinsi, kotaFetchAll } from "./../../redux/kotaReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { View } from "react-native";
import Colors from "./../../config/Colors";
import {
  REPORT_DATA_USER,
  REPORT_REALISASI_MUATAN,
  REPORT_DISPARITAS_HARGA,
  REPORT_DAFTAR_SISA_KUOTA,
  REPORT_TOTAL_MUATAN_PER_WILAYAH,
  REPORT_MUATAN_PER_WILAYAH_PER_PRIORITAS,
  REPORT_EXCEL_MUATAN_PER_WILAYAH_PER_PRIORITAS,
} from "../../config/constants";
import ExportPdf from "../../services/ExportPdf";
import {
  muatanPerOperator,
  disparitasHarga,
  sisaQuotaTrayek,
  muatanPerWilayah,
  muatanPerWilayahPerPrioritas,
} from "../../redux/dashboardReducer";
import { isNull } from "lodash";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { commodityFetchAll } from "../../redux/commodityReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { operatorFetchAll } from "../../redux/operatorReducer";
import { portFetchAll } from "../../redux/portReducer";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import ExportXls from "../../services/ExportXls";

function Row({ children }) {
  return <View style={{ flexDirection: "row" }}>{children}</View>;
}
/**
 * Order Per Bulan
 */
export default function ReportMuatanPerWilayahPerPrioritas({
  navigation,
  route,
}: RegulatorStackProps<"ReportMuatanPerWilayahPerPrioritas">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    date_end: "",
    date_start: "",
    month: now.getMonth() + 1,
    wilayah: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const muatan_per_wilayah_per_prioritas = useSelector(function (
    state: RootState
  ) {
    return state.dashboard.muatanPerWilayahPerPrioritasSuccess
      ? state.dashboard.muatanPerWilayahPerPrioritasSuccess.data
      : [];
  });
  const port_list = useSelector((state: RootState) =>
    state.port.portGetSuccess ? state.port.portGetSuccess.data : []
  );
  const { currentPage, pageTotal } = useSelector(function (state: RootState) {
    return state.dashboard.muatanPerWilayahPerPrioritasSuccess;
  });

  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(muatanPerWilayahPerPrioritas(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
    dispatch(portFetchAll());
  }, [filter]);

  return (
    <>
      <FlatList
        data={muatan_per_wilayah_per_prioritas}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection: 'row'}}>
            <ExportXls
              url={REPORT_EXCEL_MUATAN_PER_WILAYAH_PER_PRIORITAS}
              data={filter}
              filename="report_excel_muatan_per_wilayah_per_prioritas"
            />  
            <ExportPdf
              url={REPORT_MUATAN_PER_WILAYAH_PER_PRIORITAS}
              data={filter}
              filename="report_muatan_per_wilayah_per_prioritas"
            />  
          </View>
          
        }
        renderItem={({ item }) => (
          <Card
            style={{ padding: 0, borderRadius: 10, margin: 3, padding: 10 }}
          >
            <Caption>
              Wilayah :{" "}
              <Paragraph style={{ color: Colors.def }}>
                {item.pelabuhan}
              </Paragraph>
              {"\n"}
              Brg. Pokok:{" "}
              <Paragraph style={{ color: Colors.def }}>
                {item.brg_pokok}
              </Paragraph>
              {"\n"}Brg. Penting:{" "}
              <Paragraph style={{ color: Colors.def }}>
                {item.brg_penting}
              </Paragraph>
              {"\n"}
              Brg. Penting Lain:{" "}
              <Paragraph style={{ color: Colors.def }}>
                {item.brg_penting_lainnya}
              </Paragraph>
              {"\n"}
              Brg. Penting Lain (rekomendasi):{" "}
              <Paragraph style={{ color: Colors.def }}>
                {item.brg_penting_lainnya_rekomendasi}
              </Paragraph>
              {"\n"}
              Brg. Unggulan Daerah:{" "}
              <Paragraph style={{ color: Colors.def }}>
                {item.brg_unggulan_daerah}
              </Paragraph>
            </Caption>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            date_end: "",
            date_start: "",
            month: now.getMonth() + 1,
            wilayah: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
            // alert(JSON.stringify(val));
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Bulan
               */}
              <SelectInput
                label="Bulan"
                onChangeValue={(val) => setFieldValue("month", val.id)}
                options={bulanId}
                objectKey="label"
              />

              {/**
               * Wilayah
               */}
              <SelectInput
                label="Wilayah"
                onChangeValue={(val) => setFieldValue("wilayah", val.id)}
                options={port_list}
                objectKey="label"
              />

              {/**
               * Dari
               */}

              <DateTime
                label="Dari"
                onChangeValue={(date) =>
                  setFieldValue("date_start", DateFormatStrip(date))
                }
              />
              {/**
               * Sampai
               */}
              <DateTime
                label="Sampai"
                onChangeValue={(date) =>
                  setFieldValue("date_end", DateFormatStrip(date))
                }
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}

// import React from "react";
// import { RegulatorStackProps } from "../Navigator";
// import { useSelector, useDispatch } from "react-redux";
// import { RootState } from "../../redux/rootReducer";
// import { muatanPerWilayahPerPrioritas } from "../../redux/dashboardReducer";
// import { Heading5 } from "../../components";
// import HorizontalList from "../../components/HorizontalList";
// import { Card, List, Paragraph, Caption } from "react-native-paper";

// export default function MuatanPerWilayahPerPrioritas({
//   navigation,
//   route,
// }: RegulatorStackProps<"Dashboard">) {
//   const { id, usertype } = useSelector(function (state: RootState) {
//     return state.auth.authLoginSuccess
//       ? state.auth.authLoginSuccess.data.user[0]
//       : {
//           usertype: "Not Defined",
//         };
//   });
//   const dispatch = useDispatch();
//   const muatan_per_wilayah_per_prioritas = useSelector(function (
//     state: RootState
//   ) {
//     return state.dashboard.muatanPerWilayahPerPrioritasSuccess
//       ? state.dashboard.muatanPerWilayahPerPrioritasSuccess.data
//       : [];
//   });
//   React.useEffect(() => {
//     dispatch(
//       muatanPerWilayahPerPrioritas({
//         date_end: "",
//         date_start: "",
//         jenis_barang: "",
//         length: 5,
//         month: "",
//         nama_barang: "",
//         wilayah: "",
//       })
//     );
//   }, []);
//   return (
//     <>
//       <Heading5>Muatan Per Wilayah Per Prioritas</Heading5>
//       {muatan_per_wilayah_per_prioritas.map((item, key) => (
// <Card
//   key={key}
//   style={{ padding: 0, borderRadius: 10, width: 300, margin: 3 }}
// >
//           <Caption>
//             Brg. Pokok: {item.barang_pokok}
//             {"\n"}Brg. Penting: {item.barang_penting}
//             {"\n"}
//             Brg. Penting Lain: {item.barang_penting_lainnya}
//             {"\n"}
//             Brg. Penting Lain (rekomendasi):{" "}
//             {item.barang_penting_lainnya_rekomendasi}
//             {"\n"}
//             Brg. Unggulan Daerah: {item.barang_unggulan_daerah}
//           </Caption>
// </Card>
//       ))}
//     </>
//   );
// }
