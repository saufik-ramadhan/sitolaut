import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from "../../redux/rootReducer";
import { dataDetailTrayekBalik, dataTrayekBalik, masterKodeTrayekFetchAll } from "../../redux/reporttblReducer";
import { Root, SelectInput } from "../../components";
import { IconButton, List, Surface } from "react-native-paper";
import {Formik} from "formik";
import Colors from '../../config/Colors';
import { FlatList } from 'react-native-gesture-handler';
import { RegulatorStackProps } from '../Navigator';
import ExportPdf from '../../services/ExportPdf';
import ExportXls from '../../services/ExportXls';
import { BASE_URL } from '../../config/constants';

export default function ReportMuatanTolLaut({navigation}: RegulatorStackProps<'ReportMuatanTolLaut'>) {
	const dispatch = useDispatch();
	const { reporttbl } = useSelector((state: RootState) => state);
	const [year, setYear] = React.useState('');
	React.useEffect(() => {
		
		dispatch(masterKodeTrayekFetchAll());
	}, [])
  return (
  	<>
			<Formik 
			initialValues={{
				date:"",
			}}
			onSubmit={function(vals) {dispatch(dataTrayekBalik(vals))}}
			>
			{
				({handleSubmit, handleChange, handleBlur, setFieldValue, values, touched, errors}) => (
					<View style={{flexDirection: 'row'}}>
						<View style={{backgroundColor: Colors.white, flexDirection: 'row'}}>
							{/* <IconButton onPress={function(){}} icon="file-pdf" color={Colors.danger}/>
							<IconButton onPress={function(){}} icon="file-excel" color={Colors.ter}/> */}
							<ExportPdf
								url={`${BASE_URL}trayek_balik/header?year=${year}&kode=`}
								data={{}}
								filename="report_muatan_tollaut"
							/>
							<ExportXls
								url={`${BASE_URL}trayek_balik/excel_header?year=${year}&kode=`}
								data={{}}
								filename="report_excel_muatan_tollaut"
							/>
						</View>
						<View style={{flex: 8}}>
							<SelectInput
								label="Tahun"
								onChangeValue={(val) => {setFieldValue('date', val.value); setYear(val.value)}}
								options={reporttbl.masterKodeTrayekYearGetSuccess}
								objectKey={'label'}
							/>
						</View>
						<View style={{backgroundColor: Colors.white, flexDirection: 'row'}}>
							<IconButton onPress={handleSubmit} icon="table-search" color={Colors.pri}/>
						</View>
					</View>
				)
			}
			</Formik>
	    <FlatList
				data={reporttbl.trayekBalikGetSuccess.data}
				renderItem={({item}) => <List.Item title={item.kode} 
					onPress={function() {
						dispatch(dataDetailTrayekBalik({kode: item.kode, date: year}))
						navigation.navigate('DetailTrayekBalik', {
							kodetrayek: item.kode,
							year: year
						});
					}}
					description={
					`${item.operator}\nDRY: ${item.dry} | REEFER: ${item.reefer} | CURAH: ${item.curah} | BOX: ${item.box}`
					}
					descriptionNumberOfLines={2}
					right={() => <List.Icon icon="chevron-right-box"/>}
					/>}
				keyExtractor={(item, key) => String(key)}
			/>
    </>
  )
}

const styles = StyleSheet.create({
  surface: {
		padding: 8,
		width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 3,
  },
});
