import React, { useState, ReactNode } from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { orderPerBulan } from "../../redux/dashboardReducer";
import {
  Heading5,
  FilterButton,
  Pagination,
  CustomDialog,
  SelectInput,
  Space,
} from "../../components";
import { getBulan, monthId } from "../../services/utils";
import { Card, Caption, Button, IconButton } from "react-native-paper";
import { FlatList } from "react-native-gesture-handler";
import { Formik } from "formik";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { portFetchAll } from "../../redux/portReducer";
import { bulanId } from "./../../services/utils";
import { roleEdit } from "./../../redux/js/roleAction";
import ExportPdf from "../../services/ExportPdf";
import { REPORT_EXCEL_ORDER_MASUK_PER_BULAN, REPORT_ORDER_MASUK_PER_BULAN } from "../../config/constants";
import ExportXls from "../../services/ExportXls";
import { View } from "react-native";

/**
 * Order Per Bulan
 */
export default function OrderMasuk({
  navigation,
  route,
}: RegulatorStackProps<"OrderMasuk">) {
  const now = new Date();
  const [visible, setVisible] = useState(false);
  const [filter, setFilter] = useState({
    month: String(now.getMonth() + 1),
    port_destination_id: "",
    port_origin_id: "",
    trayek_id: "",
  });
  const range = 10;

  const dispatch = useDispatch();

  const order_per_bulan = useSelector(function (state: RootState) {
    return state.dashboard.orderPerBulanSuccess
      ? state.dashboard.orderPerBulanSuccess.data
      : [];
  });
  const { hasNext, currentPage, pageTotal, hasPrev } = useSelector(function (
    state: RootState
  ) {
    return state.dashboard.orderPerBulanSuccess;
  });
  const msttrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetSuccess
      ? state.msttrayek.msttrayekGetSuccess.data
      : [];
  });
  const pelabuhan = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });

  function fetchData({ filter, current }) {
    const params = {
      length: range,
      start: current == 1 ? 0 : (current - 1) * range,
      ...filter,
    };
    dispatch(orderPerBulan(params));
  }
  function onNext() {
    if (currentPage < pageTotal) {
      fetchData({
        filter: filter,
        current: currentPage + 1,
      });
    }
  }
  function onPrev() {
    if (currentPage > 1) {
      fetchData({
        filter: filter,
        current: currentPage - 1,
      });
    }
  }
  React.useEffect(() => {
    fetchData({
      filter: filter,
      current: 1,
    });
    dispatch(msttrayekFetchAll());
    dispatch(portFetchAll());
  }, [filter]);

  return (
    <>
      <FlatList
        data={order_per_bulan}
        ListFooterComponent={<Space height={80} />}
        ListHeaderComponent={
          <View style={{flexDirection:'row'}}>
            <ExportXls
              url={REPORT_EXCEL_ORDER_MASUK_PER_BULAN}
              data={filter}
              filename="report_order_masuk_per_bulan"
            />
            <ExportPdf
              url={REPORT_ORDER_MASUK_PER_BULAN}
              data={filter}
              filename="report_order_masuk_per_bulan"
            />
          </View>
        }
        renderItem={({ item }) => (
          <Card
            style={{ margin: 5, padding: 10, borderRadius: 5, elevation: 3 }}
          >
            <Caption>
              Kode / Voyage : {item.kode_trayek} / {item.voyage} {"\n"}
              {item.origin}➤{item.destination}
              {"\n"}
              Order masuk : {item.count}
            </Caption>
          </Card>
        )}
        keyExtractor={(item, key) => String(key)}
      />
      <FilterButton onPress={() => setVisible(true)} />
      <Pagination
        current={currentPage}
        count={pageTotal}
        onNext={() => onNext()}
        onPrev={() => onPrev()}
      />
      <CustomDialog
        visible={visible}
        title="Filter"
        onDismiss={() => setVisible(false)}
      >
        <Formik
          initialValues={{
            month: String(now.getMonth() + 1),
            port_destination_id: "",
            port_origin_id: "",
            trayek_id: "",
          }}
          onSubmit={(val) => {
            setVisible(false);
            setFilter(val);
          }}
        >
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            values,
          }) => (
            <>
              {/**
               * Bulan
               */}
              <SelectInput
                label="Bulan"
                onChangeValue={(val) => setFieldValue("month", val.id)}
                options={bulanId}
                objectKey="label"
              />
              {/**
               * Trayek
               */}
              <SelectInput
                label="Trayek"
                onChangeValue={(val) => setFieldValue("trayek_id", val.id)}
                options={msttrayek}
                objectKey="label"
                withSearch
              />

              {/**
               * Origin
               */}
              <SelectInput
                label="Origin"
                onChangeValue={(val) => setFieldValue("port_origin_id", val.id)}
                options={pelabuhan}
                objectKey="label"
                withSearch
              />

              {/**
               * Destination
               */}
              <SelectInput
                label="Destination"
                onChangeValue={(val) =>
                  setFieldValue("port_destination_id", val.id)
                }
                options={pelabuhan}
                objectKey="label"
                withSearch
              />

              <Button onPress={handleSubmit}>Filter</Button>
            </>
          )}
        </Formik>
      </CustomDialog>
    </>
  );
}
