import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler';
import { Badge, Caption, Dialog, List, Portal, Surface } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { Root } from '../../components';
import Colors from '../../config/Colors';
import { dataBarangPerRuas } from '../../redux/reporttblReducer';
import { RootState } from '../../redux/rootReducer';
import { DateFormat, formatRupiah } from '../../services/utils';
import { Table, TableWrapper, Row } from 'react-native-table-component';
import ExportXls from '../../services/ExportXls';
import ExportPdf from '../../services/ExportPdf';
import { RegulatorStackProps } from '../Navigator';
import { BASE_URL } from '../../config/constants';

export default function DetailTrayekBalik({route}: RegulatorStackProps<'DetailTrayekBalik'>) {
  const dispatch = useDispatch();
  const {kodetrayek, year} = route.params || {kodetrayek: '', year: new Date().getFullYear()};
  const { reporttbl } = useSelector((state: RootState) => state);
  const [visible, setVisible] = React.useState({visible: false, data: {}});
  const [tableHead]= React.useState(['Shipper', 'BL Number', 'Kode Booking', 'Subjek', 'Total'])
  const [widthArr]= React.useState([230, 80, 80, 150, 100]);
  if(!reporttbl.trayekBalikViewSuccess.data) <Caption>Loading</Caption>
  return (
    <>
    <FlatList
      data={reporttbl.trayekBalikViewSuccess.data}
      ListHeaderComponent={<View style={{flexDirection:'row'}}>
        <ExportPdf
          url={`${BASE_URL}trayek_balik/details?kodeTrayek=${kodetrayek}&year=${year}`}
          data={{}}
          filename="report_detail_trayek"
        />
        <ExportXls
          url={`${BASE_URL}trayek_balik/excel_details?kodeTrayek=${kodetrayek}&year=${year}`}
          data={{}}
          filename="report_excel_detail_trayek"
        />
      </View>}
      renderItem={({item}) => (
        <List.Item 
          title={item.kode}
          descriptionNumberOfLines={6}
          onPress={() => {
            setVisible({visible: true, data: item})
            dispatch(dataBarangPerRuas(item));
          }}
          description={
            `${item.nama_perusahaan}\n${item.pol} -> ${item.pod}\n${DateFormat(item.tanggal_berangkat)} -> ${DateFormat(item.tanggal_tiba)}\nVoyage ${item.voyage}\nDRY: ${item.dry} | REEFER: ${item.reefer} | CURAH: ${item.curah} | BOX: ${item.box}`
          }
          right={() => <Badge style={{backgroundColor: Colors.pri}}>{item.jenis_trayek}</Badge>}
        />
      )}
      keyExtractor={(item, key) => String(key)}
    />
    <Portal>
      <Dialog visible={visible.visible} onDismiss={() => setVisible({visible: false, data: {}})}>
        {
          reporttbl.dataBarangPerRuasGetSuccess ? 
            (
              <Dialog.Content>
                <ScrollView horizontal>
                  <View>
                  <View style={{flexDirection:'row'}}>
                    <ExportPdf
                      url={`${BASE_URL}trayek_balik/muatan/${visible.data.kode}`}
                      data={{}}
                      filename="report_detail_trayek"
                    />
                    <ExportXls
                      url={`${BASE_URL}trayek_balik/excel_muatan/${visible.data.kode}`}
                      data={{}}
                      filename="report_excel_detail_trayek"
                    />
                  </View>
                  <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                    <Row data={tableHead} widthArr={widthArr} style={styles.header} textStyle={styles.text}/>
                  </Table>
                  <ScrollView style={styles.dataWrapper}>
                    <Table borderStyle={{borderWidth: 1, borderColor: '#C1C0B9'}}>
                      {
                        reporttbl.dataBarangPerRuasGetSuccess.map((rowData, index) => (
                          <Row
                            key={index}
                            data={[
                              rowData.shipper,
                              rowData.bill_lading_number,
                              rowData.kode_booking,
                              rowData.subjek,
                              'Rp. ' + formatRupiah(parseInt(rowData.total_harga))
                            ]}
                            widthArr={widthArr}
                            style={[styles.row, index%2 && {backgroundColor: '#F7F6E7'}]}
                            textStyle={styles.text}
                          />
                        ))
                      }
                    </Table>
                  </ScrollView>
                  </View>
                </ScrollView>
              </Dialog.Content>
            ) :
            (<Caption>Loading</Caption>)
        }
      </Dialog>
    </Portal>
    </>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  header: { height: 50, backgroundColor: '#537791' },
  text: { textAlign: 'center', fontWeight: '100' },
  dataWrapper: { marginTop: -1 },
  row: { height: 40, backgroundColor: '#E7E6E1' }
});
