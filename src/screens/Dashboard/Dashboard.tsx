import React, { memo, useCallback, useEffect } from "react";
import {
  Root,
  Heading5,
  Space,
  FloatingMenu,
  Icon,
  GojekMenu,
  SelectInput,
  SlideUpMenu,
  BottomTab,
} from "../../components";
import {
  Paragraph,
  Caption,
  IconButton,
  Card,
  Badge,
  Subheading,
  Button,
  List,
  Title,
} from "react-native-paper";
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Image,
  FlatList,
  RefreshControl,
  Dimensions,
  Linking,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import Colors from "./../../config/Colors";
import { Heading4, Heading1, Heading3 } from "./../../components/Heading";
import { useNavigation } from "@react-navigation/native";
import {
  purchaseOrder,
  distribusiBarang,
  dashboardCommodity,
  sisaQuotaTrayek,
  bookingVessel,
  consigneeStatisticAll,
  consigneeStatisticActive,
  supplierStatisticAll,
  supplierStatisticActive,
  shipperStatisticAll,
  shipperStatisticActive,
  resellerStatisticAll,
  resellerStatisticActive,
  operatorStatisticAll,
  operatorStatisticActive,
  regulatorStatisticAll,
  regulatorStatisticActive,
  allOrder,
  orderPaid,
  orderUnpaid,
  orderDenied,
  muatanPerOperator,
  disparitasHarga,
  muatanPerWilayah,
  muatanPerWilayahPerPrioritas,
  realisasi,
  waktuTempuh,
  orderPerBulan,
  orderPerJenisPrioritas,
  muatanTerbanyak,
  voyagePerTrayek,
  containerPerTahun,
} from "../../redux/dashboardReducer";
import MapboxGL from "@react-native-mapbox-gl/maps";
import { MonthsID } from "../../config/Months";
import { ConsigneeStack, RegulatorStackProps } from "./../Navigator";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from "react-native-chart-kit";
import { ScrollView } from "react-native-gesture-handler";
import { Formik } from "formik";
import {
  DateFormat,
  formatRupiah,
  DateFormat4,
  getBulan,
} from "./../../services/utils";
import { isEmpty, isNull } from "lodash";
import { ChartConfig } from "react-native-chart-kit/dist/HelperTypes";
import { CONSIGNEE_PANDUAN_URL, OPERATOR_PANDUAN_URL, REGULATOR_PANDUAN_URL, RESELLER_PANDUAN_URL, SHIPPER_PANDUAN_URL, SUPPLIER_PANDUAN_URL } from "../../config/constants";

const m = MonthsID;
/**
 * Set map access token retrieved from
 * https://account.mapbox.com/
 */
// MapboxGL.setAccessToken("<YOUR_ACCESSTOKEN>");
MapboxGL.setAccessToken(
  "pk.eyJ1Ijoid2FsbGFieTIxIiwiYSI6ImNrY2lsdWt6NDBsOHgyeGsyMmdtNmkwb3EifQ.9EpcSZJif_0iEhfUlCl1Ng"
);

/**
 *
 * @param param0
 */
type HorizontalList = {
  children: any;
  snapToInterval: number;
};
const HorizontalList = memo(({ children, snapToInterval }: HorizontalList) => {
  return (
    <ScrollView
      horizontal={true}
      pagingEnabled={true}
      snapToInterval={snapToInterval}
      decelerationRate={"fast"}
      showsHorizontalScrollIndicator={false}
    >
      {children}
    </ScrollView>
  );
});

/**
 * Lihat Semua (Lihat Semua)
 */
function LihatSemua({ onPress, style }) {
  return (
    <Caption
      style={{ fontWeight: "bold", color: Colors.pri, ...style }}
      onPress={onPress}
    >
      Lihat Semua
    </Caption>
  );
}

/**
 * Logo LCS
 */
function Logo() {
  return (
    <Image
      source={require("../../assets/img/lcslogo.png")}
      style={{ height: 100, maxWidth: "80%", alignSelf: "center" }}
      resizeMode="center"
    />
  );
}

/**
 * Brief Profile Information
 */
function Profile() {
  const userType = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0].usertype
      : "Not Known";
  });
  const userEmail = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0].email
      : "User Email Not Available";
  });
  return (
    <View style={styles.profileContainer}>
      <View style={styles.profileName}>
        <Heading5 style={{ color: Colors.def }}>{userEmail}</Heading5>
        <Caption>{userType}</Caption>
      </View>
      <View style={styles.profileImage}>
        <IconButton
          icon="account"
          color={Colors.gray5}
          onPress={function () {
            null
          }}
        />
      </View>
    </View>
  );
}

/**
 * Panduan Penggunaan Aplikasi
 */
function PanduanPenggunaan() {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  let URL_PANDUAN = () => {
    switch(usertype) {
      case "Consignee": return CONSIGNEE_PANDUAN_URL;
      case "Supplier": return SUPPLIER_PANDUAN_URL;
      case "Shipper": return SHIPPER_PANDUAN_URL;
      case "Reseller": return RESELLER_PANDUAN_URL;
      case "Vessel Operator": return OPERATOR_PANDUAN_URL;
      case "REGULATOR": return REGULATOR_PANDUAN_URL;
      default: return REGULATOR_PANDUAN_URL;
    }
  }
  return (
    <Card 
      style={{borderRadius: 5, padding: 10, backgroundColor: Colors.lightblue, borderLeftWidth: 2, borderLeftColor: Colors.pri}}
      onPress={()=> { Linking.openURL(String(URL_PANDUAN())) }} 
    >
      <Paragraph style={{color: Colors.pri}}>Klik di sini untuk unduh Panduan Penggunaan user <Paragraph style={{fontWeight:'bold', color: Colors.pri}}>{usertype}</Paragraph></Paragraph>
    </Card>
  )
}

/**
 * Greet People in Dashboard
 */
function Greeting() {
  return (
    <Card
      style={{
        padding: 10,
        backgroundColor: Colors.def,
        borderRadius: 5,
      }}
    >
      <Title style={{ color: Colors.white }}>Halo,</Title>
      <Subheading style={{ color: Colors.white }}>
        Selamat datang di aplikasi Sistem Informasi Tol Laut.
      </Subheading>
    </Card>
  );
}

/**
 * Total Voyage per Trayek
 */
function TotalContainerPerPort({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const dispatch = useDispatch();
  const containerPerTahunSuccess = useSelector((state: RootState) =>
    state.dashboard.containerPerTahunSuccess
      ? state.dashboard.containerPerTahunSuccess.data
      : []
  );
  const current = new Date();
  const fetchData = () => {
    const params = {
      tahun: String(current.getFullYear()),
    };
    dispatch(containerPerTahun(params));
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <Heading5>Total Container per Port</Heading5>
      {containerPerTahunSuccess.map((item, key) => (
        <Card key={key} style={{ padding: 10, margin: 5, borderRadius: 5 }}>
          <Caption>{item.pelabuhan}</Caption>
          <Caption>Total Container : {item.total}</Caption>
        </Card>
      ))}
      {/* <LineChart
        data={{
          labels: [
            "Tj. Perak",
            "Kalabahi",
            "Morotai",
            "Tj. Priok",
            "Bengawan",
            "Tarakan",
          ],
          datasets: [
            {
              data: [
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
              ],
            },
          ],
        }}
        width={Dimensions.get("window").width - 40} // from react-native
        height={220}
        yAxisLabel=""
        yAxisSuffix=""
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          backgroundColor: Colors.def,
          backgroundGradientFrom: Colors.grayL,
          backgroundGradientTo: Colors.grayL,
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(40, 22,111 , ${opacity})`,
          labelColor: (opacity = 1) => `rgba(40, 22, 111, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: "5",
            strokeWidth: "1",
            stroke: Colors.white,
          },
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
      /> */}
    </>
  );
}

/**
 * Total Voyage per Trayek
 */
function TotalVoyagePerTrayek({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const dispatch = useDispatch();
  const current = new Date();
  const totalVoyagePerTrayek = useSelector((state: RootState) =>
    state.dashboard.voyagePerTrayekSuccess
      ? state.dashboard.voyagePerTrayekSuccess.data
      : []
  );
  const fetchData = () => {
    const params = {
      tahun: String(current.getFullYear()),
    };
    dispatch(voyagePerTrayek(params));
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <Heading5>Total Voyage per Trayek</Heading5>
      {totalVoyagePerTrayek.map((item, key) => (
        <Card key={key} style={{ padding: 10, margin: 5, borderRadius: 5 }}>
          <Caption>{item.kode}</Caption>
          <Caption>
            Jumlah Voyage : {item.voyage} dari {item.target}
          </Caption>
        </Card>
      ))}
      {/* <LineChart
        data={{
          labels: ["P27", "P28", "P29", "P30", "P31", "P32"],
          datasets: [
            {
              data: [
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
              ],
            },
          ],
        }}
        width={Dimensions.get("window").width - 40} // from react-native
        height={220}
        yAxisLabel=""
        yAxisSuffix=""
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          backgroundColor: Colors.def,
          backgroundGradientFrom: Colors.grayL,
          backgroundGradientTo: Colors.grayL,
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(40, 22,111 , ${opacity})`,
          labelColor: (opacity = 1) => `rgba(40, 22, 111, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: "5",
            strokeWidth: "1",
            stroke: Colors.white,
          },
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
      /> */}
    </>
  );
}

/**
 * 5 Jenis Muatan Terbanyak Per Prioritas
 */
function MuatanTerbanyakPrioritas() {
  const dispatch = useDispatch();
  const muatanTerbanyakSuccess = useSelector((state: RootState) =>
    state.dashboard.muatanTerbanyakSuccess
      ? state.dashboard.muatanTerbanyakSuccess.data
      : []
  );
  function fetchData() {
    const params = {
      date_end: "",
      date_start: "",
      length: 5,
    };
    dispatch(muatanTerbanyak(params));
  }
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <Heading5> 5 Muatan Terbanyak </Heading5>
      {muatanTerbanyakSuccess.map((item, key) => (
        <Card key={key} style={{ padding: 10, margin: 5, borderRadius: 5 }}>
          <Caption>
            {item.nama_barang}
            {"\n"}
            {item.total} {item.satuan}
          </Caption>
        </Card>
      ))}
      {/* <LineChart
        data={{
          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
          datasets: [
            {
              data: [
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
                Math.random() * 10,
              ],
            },
          ],
        }}
        width={Dimensions.get("window").width - 40} // from react-native
        height={220}
        yAxisLabel=""
        yAxisSuffix=""
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          backgroundColor: Colors.def,
          backgroundGradientFrom: Colors.grayL,
          backgroundGradientTo: Colors.grayL,
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(40, 22,111 , ${opacity})`,
          labelColor: (opacity = 1) => `rgba(40, 22, 111, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: "5",
            strokeWidth: "1",
            stroke: Colors.white,
          },
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
      /> */}
    </>
  );
}

/**
 * Total Order per Jenis Prioritas
 */
function TotalOrderPrioritas({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const dispatch = useDispatch();

  const orderPerPrioritas = useSelector((state: RootState) =>
    state.dashboard.orderPerJenisPrioritasSuccess
      ? state.dashboard.orderPerJenisPrioritasSuccess.data
      : []
  );
  const fetchData = () => {
    dispatch(
      orderPerJenisPrioritas({
        date_end: "",
        date_start: "",
      })
    );
  };
  const chartConfig: ChartConfig = {
    backgroundColor: Colors.def,
    backgroundGradientFrom: Colors.grayL,
    backgroundGradientTo: Colors.grayL,
    decimalPlaces: 0, // optional, defaults to 2dp
    color: (opacity = 1) => `rgba(40, 22,111 , ${opacity})`,
    labelColor: (opacity = 1) => `rgba(40, 22, 111, ${opacity})`,
    style: {
      borderRadius: 16,
    },
    propsForDots: {
      r: "5",
      strokeWidth: "1",
      stroke: Colors.white,
    },
  };

  let data = [
    ...orderPerPrioritas.map((item, key) => ({
      name: item.jenis_barang,
      population: 20,
      color: "rgba(131, 0, 234, 1)",
      legendFontColor: "black",
      legendFontSize: 15,
    })),
  ];
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <Heading5> Total Order Per Jenis Prioritas </Heading5>
      {/* <HorizontalList snapToInterval={253}> */}
      {orderPerPrioritas.map((item, key) => (
        <Card key={key} style={{ margin: 3, borderRadius: 5, padding: 10 }}>
          <Caption>
            {item.jenis_barang} {">"} {item.jml_order}
          </Caption>
        </Card>
      ))}
      {/* </HorizontalList> */}
      {/* <PieChart
        data={data}
        width={Dimensions.get("window").width - 40}
        height={220}
        chartConfig={chartConfig}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="15"
        absolute
        hasLegend={false}
      /> */}
    </>
  );
}

/**
 * Waktu Tempuh
 */
function WaktuTempuh({ navigation, route }: RegulatorStackProps<"Dashboard">) {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const waktu_tempuh = useSelector(function (state: RootState) {
    return state.dashboard.waktuTempuhSuccess
      ? state.dashboard.waktuTempuhSuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      waktuTempuh({
        kode_trayek: "",
        length: 5,
        operator_id: "",
        tahun: "",
      })
    );
  }, []);
  // const waktu_tempuh = [
  //   {
  //     operator: "PT Operator",
  //     trayek: "P27",
  //     voyage: "2",
  //     origin: "Belawan",
  //     destination: "Tanjung Perak",
  //     eta: new Date(),
  //     etd: new Date(),
  //     waktu: 5,
  //   },
  //   {
  //     operator: "PT Operator",
  //     trayek: "P27",
  //     voyage: "2",
  //     origin: "Belawan",
  //     destination: "Tanjung Perak",
  //     eta: new Date(),
  //     etd: new Date(),
  //     waktu: 5,
  //   },
  // ];
  return (
    <>
      <Heading5>Waktu Tempuh</Heading5>
      <HorizontalList snapToInterval={300}>
        {waktu_tempuh.map((item, key) => (
          <Card
            key={key}
            style={{ padding: 10, borderRadius: 10, margin: 3, width: 300 }}
          >
            <Caption>Operator : {item.operator_name}</Caption>
            <Row>
              <View style={{ flex: 3 }}>
                <Caption>
                  Trayek/Voyage: {item.kode_trayek} / {item.voyage}
                </Caption>
                <Caption>
                  {item.pelabuhan_asal} ➤ {item.pelabuhan_tujuan}
                </Caption>
                <Caption>
                  {DateFormat(item.etd)} ➤ {DateFormat(item.eta)}
                </Caption>
              </View>
              <View style={{ flex: 1 }}>
                <Badge>{item.waktu_tempuh.days} hari</Badge>
              </View>
            </Row>
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("WaktuTempuh");
        }}
        style={{ textAlign: "right" }}
      />
    </>
  );
}

/**
 * Realisasi Voyage Per Trayek
 */
function RealisasiVoyage({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const realisasi_voyage = useSelector(function (state: RootState) {
    return state.dashboard.realisasiSuccess
      ? state.dashboard.realisasiSuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      realisasi({
        kode_trayek: "",
        length: 5,
        operator_id: "",
        tahun: "",
      })
    );
  }, []);
  return (
    <>
      <Heading5>Realisasi Voyage</Heading5>
      <View style={{ padding: 5, borderRadius: 10 }}>
        <Row>
          <Caption style={{ flex: 1 }}>Trayek</Caption>
          <Caption style={{ flex: 3 }}>Operator</Caption>
          <Caption style={{ flex: 1 }}>Target</Caption>
          <Caption style={{ flex: 1 }}>Tahun</Caption>
        </Row>
      </View>
      <HorizontalList snapToInterval={303}>
        {realisasi_voyage.map((item, key) => (
          <Card
            key={key}
            style={{ padding: 5, borderRadius: 10, margin: 3, width: 300 }}
          >
            <Row>
              <Caption style={{ flex: 1 }}>{item.kode_trayek}</Caption>
              <Caption style={{ flex: 3 }}>{item.operator}</Caption>
              <Caption style={{ flex: 1 }}>
                {item.voyage}/{item.target}
              </Caption>
              <Caption style={{ flex: 1 }}>{item.tahun}</Caption>
            </Row>
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("RealisasiVoyage");
        }}
        style={{ textAlign: "right" }}
      />
    </>
  );
}

/**
 * Total Muatan Per Wilayah Per Prioritas
 */
// const muatan_per_wilayah_per_prioritas = [
//   {
//     wilayah: "Belawan",
//     muatan: {
//       barang_pokok: "2000 kg",
//       barang_penting: "0",
//       barang_penting_lainnya: "0",
//       barang_penting_lainnya_rekomendasi: "0",
//       barang_komersil: "0",
//       barang_unggulan_daerah: "0",
//     },
//   },
// ];
function MuatanPerWilayahPerPrioritas({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const muatan_per_wilayah_per_prioritas = useSelector(function (
    state: RootState
  ) {
    return state.dashboard.muatanPerWilayahPerPrioritasSuccess
      ? state.dashboard.muatanPerWilayahPerPrioritasSuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      muatanPerWilayahPerPrioritas({
        date_end: "",
        date_start: "",
        jenis_barang: "",
        length: 5,
        month: "",
        nama_barang: "",
        wilayah: "",
      })
    );
  }, []);
  return (
    <>
      <Heading5>Muatan Per Wilayah Per Prioritas</Heading5>
      <HorizontalList snapToInterval={303}>
        {muatan_per_wilayah_per_prioritas.map((item, key) => (
          <Card
            key={key}
            style={{ padding: 0, borderRadius: 10, width: 300, margin: 3 }}
          >
            <List.Accordion
              titleNumberOfLines={2}
              title={<Paragraph>Wilayah : {item.pelabuhan}</Paragraph>}
            >
              <List.Item
                onPress={function () {
                  null;
                }}
                titleNumberOfLines={10}
                title={
                  <>
                    <Caption>
                      Brg. Pokok: {item.barang_pokok}
                      {"\n"}Brg. Penting: {item.barang_penting}
                      {"\n"}
                      Brg. Penting Lain: {item.barang_penting_lainnya}
                      {"\n"}
                      Brg. Penting Lain (rekomendasi):{" "}
                      {item.barang_penting_lainnya_rekomendasi}
                      {"\n"}
                      Brg. Unggulan Daerah: {item.barang_unggulan_daerah}
                    </Caption>
                  </>
                }
              />
            </List.Accordion>
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("TotalOrderPerJenisPrioritas");
        }}
        style={{ textAlign: "right" }}
      />
    </>
  );
}

/**
 * Total Muatan Per Wilayah
 */
const muatan_per_wilayah = [
  {
    wilayah: "Belawan",
    jenis_barang: "Kebutuhan Pokok",
    nama_barang: "Beras",
    total_muatan: "2000 kg",
  },
];
function MuatanPerWilayah({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const muatan_per_wilayah = useSelector(function (state: RootState) {
    return state.dashboard.muatanPerWilayahSuccess
      ? state.dashboard.muatanPerWilayahSuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      muatanPerWilayah({
        date_end: "",
        date_start: "",
        jenis_barang: "",
        length: 5,
        month: "",
        nama_barang: "",
        wilayah: "",
      })
    );
  }, []);
  return (
    <>
      <Heading5>Total Muatan Per Wilayah</Heading5>
      <HorizontalList snapToInterval={253}>
        {muatan_per_wilayah.map((item, key) => (
          <Card
            style={{ padding: 10, borderRadius: 10, width: 250, margin: 3 }}
            key={key}
          >
            <Caption>Wilayah: {item.pelabuhan}</Caption>
            <Caption>Jenis Barang: {item.jenis_barang}</Caption>
            <Caption>
              Nama Barang: {item.nama_barang} ({item.total_muatan})
            </Caption>
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("TotalMuatanPerWilayah");
        }}
        style={{ textAlign: "right" }}
      />
    </>
  );
}

/**
 * Daftar Sisa Kuota Trayek
 */

function SisaKuotaTrayek({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const trayek = useSelector(function (state: RootState) {
    return state.dashboard.sisaQuotaTrayekSuccess
      ? state.dashboard.sisaQuotaTrayekSuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      sisaQuotaTrayek({
        length: 5,
        month: "",
        operator_id: "",
        port_destination_id: "",
        port_origin_id: "",
        trayek_id: "",
      })
    );
  }, []);
  // const trayek = [
  //   {
  //     operator: "PT Operator",
  //     trayek: "P27",
  //     voyage: "2",
  //     jenis_container: "20 REFEER",
  //     sisa_kuota: 21,
  //     alokasi_kuota: 21,
  //     origin: "Belawan",
  //     destination: "Tanjung Perak",
  //     eta: new Date(),
  //     etd: new Date(),
  //   },
  // ];
  return (
    <>
      <Heading5>Daftar Sisa Kuota Trayek</Heading5>
      <HorizontalList snapToInterval={303}>
        {trayek.map((item, key) => (
          <Card
            key={key}
            style={{ padding: 10, borderRadius: 10, width: 300, margin: 3 }}
          >
            <Caption>Operator : {item.operator}</Caption>
            <Row>
              <View style={{ flex: 3 }}>
                <Caption>
                  Trayek/Voyage: {item.kode_trayek} / {item.voyage}
                </Caption>
                <Caption>
                  {item.pelabuhan_asal} ➤ {item.pelabuhan_tujuan}
                </Caption>
                <Caption>
                  {DateFormat(item.etd)} ➤ {DateFormat(item.eta)}
                </Caption>
              </View>
              <View style={{ flex: 1 }}>
                <Badge>{item.type_container_name}</Badge>
                <Caption style={{ textAlign: "right" }}>
                  Kuota:{"\n"}({item.total_sisa}/{item.total_alokasi_quota})
                </Caption>
              </View>
            </Row>
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("DaftarSisaKuotaTrayek");
        }}
        style={{ textAlign: "right" }}
      />
    </>
  );
}

/**
 * Disparitas Harga
 */
function Row({ children }) {
  return <View style={{ flexDirection: "row" }}>{children}</View>;
}
function DisparitasHarga({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const dispatch = useDispatch();
  const disparitas_harga = useSelector(function (state: RootState) {
    return state.dashboard.disparitasHargaSuccess
      ? state.dashboard.disparitasHargaSuccess.data
      : [];
  });
  const current = new Date();
  useEffect(() => {
    dispatch(
      disparitasHarga({
        barang_id: "",
        consignee_id: "",
        kota_asal: "",
        kota_tujuan: "",
        length: 5,
        reseller_id: "",
        supplier_id: "",
      })
    );
  }, []);
  const disparitas = [
    {
      consignee_kota: "Kota Jakarta Selatan",
      consignee_name: "PT Consignee",
      harga_satuan: "11000.00",
      harga_satuan_consignee: null,
      harga_satuan_jual: null,
      id: 1,
      nama_barang: "Beras Merah",
      reseller_kota: null,
      reseller_name: null,
      satuan: "Kg",
      supplier_kota: "Kota Jakarta Selatan",
      supplier_name: "PT Supplier",
    },
  ];
  return (
    <>
      <Heading5>Disparitas Harga</Heading5>
      <HorizontalList snapToInterval={303}>
        {disparitas_harga.map((item, key) => (
          <Card
            style={{
              padding: 10,
              borderRadius: 10,
              width: 300,
              margin: 3,
            }}
            key={key}
          >
            <Row>
              <Paragraph style={{ flex: 1 }}>{item.nama_barang}</Paragraph>
              <Caption style={{ flex: 1, textAlign: "right" }}>
                {item.origin} ➤ {item.destination}
              </Caption>
            </Row>

            <Row>
              <Caption style={{ flex: 3 }}>
                {item.supplier_name}
                {"\n"}({item.supplier_kota})
              </Caption>
              <Caption
                style={{ color: "orangered", flex: 1, textAlign: "right" }}
              >
                Rp.{formatRupiah(parseInt(item.harga_satuan))}
              </Caption>
            </Row>

            {!isNull(item.harga_satuan_consignee) && (
              <Row>
                <Caption style={{ flex: 3 }}>
                  {item.consignee_name}
                  {"\n"}({item.consignee_kota})
                </Caption>
                <Caption
                  style={{ color: "orangered", flex: 1, textAlign: "right" }}
                >
                  Rp.{formatRupiah(parseInt(item.harga_satuan_consignee))}
                </Caption>
              </Row>
            )}

            {!isNull(item.harga_satuan_jual) && (
              <Row>
                <Caption style={{ flex: 3 }}>
                  {item.reseller_name}
                  {"\n"}({item.reseller_kota})
                </Caption>
                <Caption
                  style={{ color: "orangered", flex: 1, textAlign: "right" }}
                >
                  Rp.{formatRupiah(parseInt(item.harga_satuan_jual))}
                </Caption>
              </Row>
            )}
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("DisparitasHarga");
        }}
        style={{ textAlign: "right" }}
      />
    </>
  );
}

/**
 * Total Realisasi Muatan per Operator
 */
function RealisasiMuatan({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const dispatch = useDispatch();
  const muatan_per_operator = useSelector(function (state: RootState) {
    return state.dashboard.muatanPerOperatorSuccess
      ? state.dashboard.muatanPerOperatorSuccess.data
      : [];
  });
  const current = new Date();
  useEffect(() => {
    dispatch(
      muatanPerOperator({
        length: 5,
        month: String(current.getMonth() + 1),
        operator_id: "",
      })
    );
  }, []);
  if(muatan_per_operator === undefined || muatan_per_operator.length === 0) return null;
  return (
    <>
      <Heading5>Realisasi Muatan (Agustus)</Heading5>
      <HorizontalList snapToInterval={253}>
        {muatan_per_operator.map((item, key) => (
          <Card
            key={key}
            style={{
              borderRadius: 10,
              marginVertical: 2,
              width: 250,
              padding: 10,
              marginHorizontal: 3,
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 3 }}>
                <Caption>
                  Trayek / Voyage
                  <Paragraph>
                    {" "}
                    {item.kode_trayek}/{item.voyage}
                  </Paragraph>
                </Caption>
                <Caption>
                  Operator
                  <Paragraph> {item.nama_perusahaan_operator}</Paragraph>
                </Caption>
                <Caption>
                  Rute
                  <Paragraph>
                    {" "}
                    {item.pelabuhan_asal} ➤ {item.pelabuhan_tujuan}
                  </Paragraph>
                </Caption>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Paragraph style={{ textAlign: "center" }}>100%</Paragraph>
                <Badge style={{ backgroundColor: Colors.success }}>
                  Terpenuhi
                </Badge>
              </View>
            </View>
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("RealisasiMuatan");
        }}
        style={{ textAlign: "right" }}
      />
      {/* <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: Colors.white,
          borderRadius: 10,
        }}
      >
        <View style={{ flex: 6 }}>
          <Formik
            initialValues={{
              bulan: "",
              operator_id: "",
            }}
            onSubmit={(val) => {
              console.log(JSON.stringify(val));
            }}
          >
            {({
              values,
              handleChange,
              handleBlur,
              handleSubmit,
              setFieldValue,
            }) => (
              <>
                <SelectInput
                  label={"Operator"}
                  onChangeValue={(val) => setFieldValue("operator_id", val.id)}
                  options={[{ id: 0, label: "PT Operator" }]}
                  objectKey="label"
                  withSearch
                />
                <SelectInput
                  label={"Bulan"}
                  onChangeValue={(val) => setFieldValue("bulan", val.id)}
                  options={[
                    { id: 0, label: "Januari" },
                    { id: 1, label: "Februari" },
                    { id: 2, label: "Maret" },
                    { id: 3, label: "April" },
                    { id: 4, label: "Mei" },
                    { id: 5, label: "Juni" },
                    { id: 6, label: "Juli" },
                    { id: 7, label: "Agustus" },
                    { id: 8, label: "September" },
                    { id: 9, label: "Oktober" },
                    { id: 10, label: "November" },
                    { id: 11, label: "Desember" },
                  ]}
                  objectKey="label"
                  withSearch
                />
              </>
            )}
          </Formik>
        </View>

        <IconButton
          icon={"magnify"}
          style={{ flex: 1, backgroundColor: Colors.def }}
          color={Colors.white}
        >
          Cari
        </IconButton>
      </View> */}
    </>
  );
}

/**
 * Order Per Bulan
 */
function OrderMasuk({ navigation, route }: RegulatorStackProps<"OrderMasuk">) {
  const current = new Date();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const order_per_bulan = useSelector(function (state: RootState) {
    return state.dashboard.orderPerBulanSuccess
      ? state.dashboard.orderPerBulanSuccess.data
      : [];
  });
  const dataset = order_per_bulan.map((item) => item.count);
  React.useEffect(() => {
    dispatch(
      orderPerBulan({
        month: current.getMonth() + 1,
        // port_destination_id: "",
        // port_origin_id: "",
        // trayek_id: "",
      })
    );
  }, []);
  if(dataset === undefined || dataset.length === 0) return null;
  return (
    <>
      {/* <Paragraph>{JSON.stringify(dataset)}</Paragraph> */}
      <Heading5>Order Masuk Bulan {getBulan(current)}</Heading5>
      {/* <LineChart
        data={{
          labels: order_per_bulan.map((item) => item.kode_trayek),
          datasets: [
            {
              data: dataset,
            },
          ],
        }}
        width={Dimensions.get("window").width - 40} // from react-native
        height={220}
        yAxisLabel=""
        yAxisSuffix=""
        yAxisInterval={1} // optional, defaults to 1
        chartConfig={{
          backgroundColor: Colors.def,
          backgroundGradientFrom: Colors.grayL,
          backgroundGradientTo: Colors.grayL,
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(40, 22,111 , ${opacity})`,
          labelColor: (opacity = 1) => `rgba(40, 22, 111, ${opacity})`,
          style: {
            borderRadius: 16,
          },
          propsForDots: {
            r: "5",
            strokeWidth: "1",
            stroke: Colors.white,
          },
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 16,
        }}
      /> */}
      
      <HorizontalList snapToInterval={255}>
        {order_per_bulan.map((item, key) => (
          <Card
            key={key}
            style={{ width: 250, margin: 5, padding: 5, borderRadius: 10 }}
          >
            <Caption>
              Kode / Voyage : {item.kode_trayek} / {item.voyage} {"\n"}
              {item.origin}➤{item.destination}
              {"\n"}
              Order masuk : {item.count}
            </Caption>
          </Card>
        ))}
      </HorizontalList>
      <LihatSemua
        onPress={function () {
          navigation.navigate("PurchaseOrder");
        }}
        style={{ textAlign: "right" }}
      />
    </>
  );
}

/**
 * Status Order
 */
interface Order {
  name?: string;
  count?: number;
  icon?: string;
  backgroundColor?: string;
  iconColor?: string;
  width?: number;
}
function Order({
  name,
  count,
  icon,
  backgroundColor,
  iconColor,
  width = 100,
}: Order) {
  return (
    <Card style={{ elevation: 3, margin: 3, borderRadius: 5, width }}>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
          padding: 3,
        }}
      >
        <Icon
          name={icon}
          style={{
            fontSize: 25,
            position: "absolute",
            color: iconColor || Colors.gray1,
            bottom: 0,
            right: 0,
            opacity: 0.7,
          }}
        />
        <View style={{ flex: 2 }}>
          <Paragraph>
            {name}
            {"\n"}
            {count}
          </Paragraph>
        </View>
      </View>
    </Card>
  );
}
function StatusOrder() {
  const dispatch = useDispatch();

  const allOrderData = useSelector(function (state: RootState) {
    return state.dashboard.allOrderSuccess
      ? state.dashboard.allOrderSuccess.data[0].count
      : 0;
  });
  const orderPaidData = useSelector(function (state: RootState) {
    return state.dashboard.orderPaidSuccess
      ? state.dashboard.orderPaidSuccess.data[0].count
      : 0;
  });
  const orderUnpaidData = useSelector(function (state: RootState) {
    return state.dashboard.orderUnpaidSuccess
      ? state.dashboard.orderUnpaidSuccess.data[0].count
      : 0;
  });
  const orderDeniedData = useSelector(function (state: RootState) {
    return state.dashboard.orderDeniedSuccess
      ? state.dashboard.orderDeniedSuccess.data[0].count
      : 0;
  });

  useEffect(() => {
    dispatch(allOrder());
    dispatch(orderPaid());
    dispatch(orderUnpaid());
    dispatch(orderDenied());
  }, []);
  return (
    <>
      <Heading5>Status Order</Heading5>
      <HorizontalList snapToInterval={150}>
        <Order
          name="Total Order"
          count={allOrderData}
          icon="cart"
          iconColor={Colors.slateblue}
        />
        <Order
          name="Lunas"
          count={orderPaidData}
          icon="cash"
          iconColor={Colors.seagreen}
        />
        <Order
          name="Belum Lunas"
          count={orderUnpaidData}
          icon="alert-circle"
          iconColor={Colors.warning}
        />
        <Order
          name="Ditolak"
          count={orderDeniedData}
          icon="close-circle"
          iconColor={Colors.danger}
        />
      </HorizontalList>
    </>
  );
}

/**
 * Report User
 * Menampilkan Ringkasan Jumlah
 * user yang terdaftar di aplikasi LCS
 */
interface UserCard {
  name: string;
  active: number;
  registered: number;
  backgroundColor?: string;
  width?: number;
  borderRadius?: number;
  margin?: number;
}
const UserCard = memo(
  ({
    name,
    active,
    registered,
    backgroundColor = "white",
    width = 100,
    borderRadius,
    margin,
  }: UserCard) => {
    return (
      <Card
        style={{
          elevation: 2,
          padding: 2,
          backgroundColor: backgroundColor,
          borderRadius: borderRadius,
          width: width,
          margin: margin,
        }}
      >
        <Paragraph style={{ textAlign: "center", color: "white" }}>{name}</Paragraph>
        <Caption style={{ textAlign: "center" }}>
          <Caption>{active}</Caption> active of {"\n"}
          <Caption>{registered}</Caption> registered
        </Caption>
      </Card>
    );
  }
);
function ReportUser() {
  const dispatch = useDispatch();
  const default_user = 0;
  const consigneeAll = useSelector(function (state: RootState) {
    return state.dashboard.consigneeSttcAllSuccess
      ? state.dashboard.consigneeSttcAllSuccess.data[0].count
      : default_user;
  });
  const consigneeAct = useSelector(function (state: RootState) {
    return state.dashboard.consigneeSttcActiveSuccess
      ? state.dashboard.consigneeSttcActiveSuccess.data[0].count
      : 0;
  });
  const supplierAll = useSelector(function (state: RootState) {
    return state.dashboard.supplierSttcAllSuccess
      ? state.dashboard.supplierSttcAllSuccess.data[0].count
      : 0;
  });
  const supplierAct = useSelector(function (state: RootState) {
    return state.dashboard.supplierSttcActiveSuccess
      ? state.dashboard.supplierSttcActiveSuccess.data[0].count
      : 0;
  });
  const shipperAll = useSelector(function (state: RootState) {
    return state.dashboard.shipperSttcAllSuccess
      ? state.dashboard.shipperSttcAllSuccess.data[0].count
      : 0;
  });
  const shipperAct = useSelector(function (state: RootState) {
    return state.dashboard.shipperSttcActiveSuccess
      ? state.dashboard.shipperSttcActiveSuccess.data[0].count
      : 0;
  });
  const resellerAll = useSelector(function (state: RootState) {
    return state.dashboard.resellerSttcAllSuccess
      ? state.dashboard.resellerSttcAllSuccess.data[0].count
      : 0;
  });
  const resellerAct = useSelector(function (state: RootState) {
    return state.dashboard.resellerSttcActiveSuccess
      ? state.dashboard.resellerSttcActiveSuccess.data[0].count
      : 0;
  });
  const operatorAll = useSelector(function (state: RootState) {
    return state.dashboard.operatorSttcAllSuccess
      ? state.dashboard.operatorSttcAllSuccess.data[0].count
      : 0;
  });
  const operatorAct = useSelector(function (state: RootState) {
    return state.dashboard.operatorSttcActiveSuccess
      ? state.dashboard.operatorSttcActiveSuccess.data[0].count
      : 0;
  });
  const regulatorAll = useSelector(function (state: RootState) {
    return state.dashboard.regulatorSttcAllSuccess
      ? state.dashboard.regulatorSttcAllSuccess.data[0].count
      : 0;
  });
  const regulatorAct = useSelector(function (state: RootState) {
    return state.dashboard.regulatorSttcActiveSuccess
      ? state.dashboard.regulatorSttcActiveSuccess.data[0].count
      : 0;
  });
  useEffect(() => {
    dispatch(consigneeStatisticAll());
    dispatch(consigneeStatisticActive());

    dispatch(supplierStatisticAll());
    dispatch(supplierStatisticActive());

    dispatch(shipperStatisticAll());
    dispatch(shipperStatisticActive());

    dispatch(resellerStatisticAll());
    dispatch(resellerStatisticActive());

    dispatch(operatorStatisticAll());
    dispatch(operatorStatisticActive());

    dispatch(regulatorStatisticAll());
    dispatch(regulatorStatisticActive());
  }, []);
  return (
    <>
      <Subheading style={{paddingHorizontal: 20, marginBottom: 10}}>Laporan Jumlah Pengguna</Subheading>
      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        <UserCard
          borderRadius={5}
          name="Consignee"
          active={consigneeAct}
          registered={consigneeAll}
          width={"48%"}
          margin={"1%"}
          backgroundColor={Colors.cadetblue}
        />
        <UserCard
          borderRadius={5}
          name="Supplier"
          active={supplierAct}
          registered={supplierAll}
          width={"48%"}
          margin={"1%"}
          backgroundColor={Colors.forestgreen}
        />
        <UserCard
          borderRadius={5} name="Shipper" active={shipperAct} registered={shipperAll} width={"48%"} margin={"1%"} backgroundColor={Colors.orange}/>
        <UserCard
          borderRadius={5}
          name="Reseller"
          active={resellerAct}
          registered={resellerAll}
          width={"48%"}
          margin={"1%"}
          backgroundColor={Colors.palevioletred}
        />
        <UserCard
          borderRadius={5}
          name="Operator"
          active={operatorAct}
          registered={operatorAll}
          width={"48%"}
          margin={"1%"}
          backgroundColor={Colors.peru}
        />
        <UserCard
          borderRadius={5}
          name="Regulator"
          active={regulatorAct}
          registered={regulatorAll}
          width={"48%"}
          margin={"1%"}
          backgroundColor={Colors.tomato}
        />
      </View>
    </>
  );
}

/**
 * Vessel Tracking
 * Show currently active vessel
 * plotted into Google Maps
 */
function VesselTracking() {
  return (
    <View style={styles.page}>
      <View style={styles.mapContainer}>
        {/* <MapboxGL.MapView style={styles.map} /> */}
        <MapboxGL.MapView style={styles.map}>
          <MapboxGL.Camera
            zoomLevel={16}
            followUserMode={"normal"}
            followUserLocation
            centerCoordinate={[
              [-73.99155, 40.73581],
              [-73.99155, 40.73681],
            ]}
          />
        </MapboxGL.MapView>
      </View>
    </View>
  );
}

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

/**
 * Vessel Booking Terbaru
 */
function VesselBookingTerbaru({ navigation }) {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const bookingvessel = useSelector(function (state: RootState) {
    return state.dashboard.bookingVesselSuccess
      ? state.dashboard.bookingVesselSuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      bookingVessel({
        limit: 5,
        operator_id: id,
      })
    );
  }, []);
  return (
    <>
      <View style={{ flexDirection: "row" }}>
        <Heading5 style={{ flex: 1 }}>Vessel Booking Terbaru</Heading5>
        <Caption
          onPress={function () {
            navigation.navigate("PurchaseOrder");
          }}
          style={{ textAlign: "right" }}
        >
          Lihat Semua
        </Caption>
      </View>
      <Space height={10} />

      {bookingvessel.map(function (item, key) {
        const depart = new Date(item.tanggal_berangkat);
        const arrive = new Date(item.tanggal_tiba);

        return (
          <Card
            key={key}
            elevation={4}
            style={{
              backgroundColor: "white",
              elevation: 3,
              borderLeftColor: Colors.pri,
              borderLeftWidth: 8,
              padding: 10,
              borderRadius: 0,
              marginBottom: 10,
            }}
            onPress={function () {
              navigation.navigate("ReadVesselBooking", { itemId: item.id });
            }}
          >
            <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
              No : {item.kode_booking}
            </Paragraph>

            {/** Rute */}
            <View style={{ flexDirection: "row", flex: 2 }}>
              <View style={{ flex: 1 }}>
                <B>
                  {item.nama_pelabuhan_asal}
                  {"\n"}
                  <Caption>
                    {`${depart.getDate()} ${m[depart.getMonth()]}`}
                  </Caption>
                </B>
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
              </View>
              <View style={{ flex: 1, alignItems: "flex-end" }}>
                <B>
                  {item.nama_pelabuhan_tujuan}
                  {"\n"}
                  <Caption>
                    {`${arrive.getDate()} ${m[arrive.getMonth()]}`}
                  </Caption>
                </B>
              </View>
            </View>

            <Caption>
              Consignee : <Paragraph>{item.consignee_name}</Paragraph>
            </Caption>
            <Caption>
              Trayek :{" "}
              <Paragraph>
                {item.kode_trayek} Voyage {item.voyage}
              </Paragraph>
            </Caption>
            <Badge style={{ backgroundColor: "skyblue" }}>
              {item.status === 1 ? "Baru" : null}
            </Badge>
          </Card>
        );
      })}
    </>
  );
}

/**
 * Daftar Sisa Kuota Trayek
 */
function SisaQuotaTrayek({
  navigation,
  route,
}: RegulatorStackProps<"Dashboard">) {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dispatch = useDispatch();
  const sisaquota = useSelector(function (state: RootState) {
    return state.dashboard.sisaQuotaTrayekSuccess
      ? state.dashboard.sisaQuotaTrayekSuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      sisaQuotaTrayek({
        length: 5,
        month: "",
        operator_id: "",
        port_destination_id: "",
        port_origin_id: "",
        trayek_id: "",
      })
    );
  }, []);
  return (
    <>
      <View style={{ flexDirection: "row" }}>
        <Heading5 style={{ flex: 1 }}>Sisa Quota Trayek</Heading5>
      </View>
      <Space height={10} />
      {sisaquota.map(function (item, key) {
        const etd = new Date(item.etd);
        const eta = new Date(item.eta);
        return (
          <Card
            key={key}
            elevation={4}
            style={{
              backgroundColor: "white",
              elevation: 3,
              borderLeftColor: Colors.pri,
              borderLeftWidth: 8,
              padding: 10,
              borderRadius: 0,
              marginBottom: 10,
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 2 }}>
                <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                  Kode Trayek : {item.kode_trayek}
                </Paragraph>
                <Caption>
                  Quota : <Paragraph>{item.total_alokasi_quota}</Paragraph>
                </Caption>
                <Caption>
                  Sisa Quota : <Paragraph>{item.total_sisa}</Paragraph>
                </Caption>
              </View>
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Caption>
                  Container : {"\n"}
                  <Paragraph>{item.type_container_name}</Paragraph>
                </Caption>
              </View>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Caption style={{ flex: 1 }}>
                ETA :{" "}
                <Paragraph>{`${eta.getDate()} ${m[eta.getMonth()]
                  } ${eta.getFullYear()}`}</Paragraph>
              </Caption>
              <Caption style={{ flex: 1, textAlign: "right" }}>
                ETD :{" "}
                <Paragraph>{`${etd.getDate()} ${m[etd.getMonth()]
                  } ${etd.getFullYear()}`}</Paragraph>
              </Caption>
            </View>
          </Card>
        );
      })}
    </>
  );
}

/**
 * Latest Commodity for Suppliers
 */
function LatestCommodity({ navigation }) {
  const dispatch = useDispatch();
  const { id } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : "";
  });
  const latestCommodities = useSelector(function (state: RootState) {
    return state.dashboard.dashboardCommoditySuccess
      ? state.dashboard.dashboardCommoditySuccess.data
      : [];
  });
  React.useEffect(() => {
    dispatch(
      dashboardCommodity({
        supplier_id: id,
      })
    );
  }, []);
  return (
    <>
      <View style={{ flexDirection: "row" }}>
        <Heading5 style={{ flex: 1 }}>Komoditas Terbaru</Heading5>
        <Caption
          onPress={function () {
            navigation.navigate("Commodity");
          }}
          style={{ textAlign: "right" }}
        >
          Lihat Semua
        </Caption>
      </View>
      <Space height={10} />

      <HorizontalList snapToInterval={203}>
        {latestCommodities.map(function (item, key) {
          return (
            <Card
              key={key}
              style={{
                backgroundColor: "white",
                elevation: 3,
                padding: 10,
                width: 200,
                borderRadius: 5,
                margin: 3,
              }}
              onPress={function () {
                navigation.navigate("ReadCommodity", { id: item.id });
              }}
            >
              <Paragraph style={{ position: "absolute", bottom: 0, right: 0 }}>
                {item.kode_barang}
              </Paragraph>
              <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                {item.nama_barang}
                {"\n"}
                <Caption>{item.jenis_barang}</Caption>
                {"\n"}
                <Caption style={{ fontSize: 13 }}>
                  Rp. {formatRupiah(parseInt(item.harga_satuan))} /{" "}
                  {item.satuan}
                  {"\n"}
                  min. order {item.minimum_order}
                </Caption>
              </Paragraph>
            </Card>
          );
        })}
      </HorizontalList>
    </>
  );
}

/**
 * Latest Goods (Reseller)
 */
function LatestGoods({ navigation }) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const distribusi_barang = useSelector(function (state: RootState) {
    return state.dashboard.distribusiBarangSuccess
      ? state.dashboard.distribusiBarangSuccess.data.dstrb_reseller
      : [];
  });
  function fetchData() {
    const params = {
      limit: 5,
      reseller_id: String(id),
    };
    dispatch(distribusiBarang(params));
  }
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <Heading5>
        Barang Terbaru{" "}
        <Caption
          style={styles.reload}
          onPress={function () {
            fetchData();
          }}
        >
          Perbarui
        </Caption>
      </Heading5>
      {distribusi_barang.map((item, key) => (
        <Card style={{ padding: 10, borderRadius: 5 }} key={key}>
          <Caption>{DateFormat(item.tgl_distribusi)}</Caption>
          <Paragraph>
            {item.nama_barang} (<Caption>{item.kode_barang}</Caption>)
          </Paragraph>
          <Caption>
            Qty <Paragraph>{item.qty}</Paragraph>
          </Caption>
          <Caption>
            Harga Beli :{" "}
            <Paragraph>Rp.{formatRupiah(item.harga_satuan_beli)}</Paragraph>
          </Caption>
          <Caption>
            Harga Jual :{" "}
            <Paragraph>Rp.{formatRupiah(item.harga_satuan_jual)}</Paragraph>
          </Caption>
        </Card>
      ))}
    </>
  );
}

/**
 * Latest Purchase Order bound to user authenticated
 */
function LatestPurchaseOrder({ navigation }) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dashboardPurchaseOrder = useSelector(function (state: RootState) {
    return state.dashboard.purchaseOrderSuccess
      ? state.dashboard.purchaseOrderSuccess.data
      : [];
  });
  const fetchData = useCallback(() => {
    dispatch(
      purchaseOrder({
        consignee_id: usertype == "Consignee" ? id : "",
        supplier_id: usertype == "Supplier" ? id : "",
        shipper_id: usertype == "Shipper" ? id : "",
        reseller_id: usertype == "Reseller" ? id : "",
        limit: 3,
      })
    );
  }, [dashboardPurchaseOrder]);

  React.useEffect(() => {
    fetchData();
  }, []);
  return (
    <View>
      {/* <Paragraph>{JSON.stringify(dashboardPurchaseOrder)}</Paragraph> */}
      <View style={{ flexDirection: "row" }}>
        <Heading5 style={{ flex: 1 }}>
          Pembelian Terakhir{" "}
          <Caption
            style={styles.reload}
            onPress={function () {
              fetchData();
            }}
          >
            Perbarui
          </Caption>
        </Heading5>
        <Caption
          onPress={function () {
            navigation.navigate("PurchaseOrder");
          }}
          style={{ textAlign: "right" }}
        >
          Lihat Semua
        </Caption>
      </View>
      <Space height={10} />

      <HorizontalList snapToInterval={255}>
        {dashboardPurchaseOrder.map(function (item, key) {
          return (
            <Card
              key={key}
              elevation={4}
              style={{
                backgroundColor: Colors.white,
                elevation: 3,
                paddingHorizontal: 10,
                paddingVertical: 5,
                borderRadius: 10,
                width: 250,
                margin: 5,
              }}
              onPress={function () {
                navigation.navigate("ReadPurchaseOrder", {
                  id: item.id,
                });
              }}
            >
              <Paragraph style={{ fontWeight: "bold", fontSize: 14 }}>
                No : {item.no_po}
                {"\n"}
                <Caption
                  style={{
                    color: item.is_approve_by_shipper
                      ? Colors.success
                      : item.is_approve
                        ? Colors.gray7
                        : Colors.gray7,
                  }}
                >
                  {item.is_approve_by_shipper
                    ? "Pesanan Diterima"
                    : item.is_approve
                      ? "Menunggu Konfirmasi Shipper"
                      : "Menunggu Konfirmasi Supplier"}
                </Caption>
                <Caption>
                  {"\n"}
                  Supplier: {item.supplier_name}
                  {"\n"}
                  JPT: {item.shipper_name}
                  {"\n"}
                  <Caption>
                    {item.origin} ➤ {item.destination} ({item.kode_trayek})
                  </Caption>
                </Caption>
              </Paragraph>

              <Caption style={{ textAlign: "right" }}>
                {DateFormat4(item.date_po)}
              </Caption>
            </Card>
          );
        })}
      </HorizontalList>
    </View>
  );
}

/**
 * Latest distribution bound to the user
 */
function LatestDistribution({ navigation }) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        usertype: "Not Defined",
      };
  });
  const dashboardDistribusiBarang = useSelector(function (state: RootState) {
    return state.dashboard.distribusiBarangSuccess
      ? state.dashboard.distribusiBarangSuccess.data.dstrb_consignee
      : [];
  });

  function fetchData() {
    dispatch(
      distribusiBarang({
        consignee_id: usertype == "Consignee" ? id : "",
        limit: 5,
      })
    );
  }

  React.useEffect(() => {
    fetchData();
  }, []);
  return (
    <>
      <Heading5>
        Distribusi Terakhir{" "}
        <Caption
          style={styles.reload}
          onPress={function () {
            fetchData();
          }}
        >
          Perbarui
        </Caption>
        {/* <Icon
          name="refresh"
          style={{ fontSize: 20 }}
          
        /> */}
      </Heading5>
      <Space height={10} />
      {dashboardDistribusiBarang.map(function (item, key) {
        return (
          <Card
            key={key}
            style={{
              backgroundColor: "white",
              padding: 10,
              borderRadius: 5,
              marginBottom: 10,
            }}
            onPress={function () {
              navigation.navigate("DistributePurchaseOrder");
            }}
          >
            <View>
              <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                No : {item.no_po}
              </Paragraph>
              <Caption style={{ fontSize: 13 }}>
                Reseller: {item.reseller_name}
                {"\n"}
                Tanggal Distribusi:{" "}
                {`${new Date(item.tgl_distribusi).getDate()}-`}
                {`${new Date(item.tgl_distribusi).getMonth()}-`}
                {`${new Date(item.tgl_distribusi).getFullYear()}`}
                {"\n"}
                Kuantitas:{item.qty}
              </Caption>
            </View>
          </Card>
        );
      })}
    </>
  );
}

/**
 * Consignee Menu
 * @param param0
 */
function ConsigneeMenu({ navigation }) {
  return (
    <FloatingMenu
      navigation={navigation}
      data={[
        {
          // name: "Purchases",
          name: "Pembelian",
          icon: "clipboard-text",
          route: "PurchaseOrder",
        },
        {
          name: "Jadwal",
          icon: "calendar",
          route: "Jadwal",
        },
        {
          name: "Lacak Pembelian",
          icon: "map-marker-distance",
          route: "TrackPurchaseOrder",
        },
        {
          name: "Penerimaan Barang",
          icon: "arrow-down-box",
          route: "ClaimPurchaseOrder",
        },
        {
          name: "Distribusi Barang",
          icon: "truck",
          route: "DistributePurchaseOrder",
        },
        {
          name: "Harga Barang",
          icon: "tag",
          route: "HargaJualBarang",
        },
        {
          name: "Lacak Kapal",
          icon: "map",
          route: "VesselGeoLocation",
        },
      ]}
    />
  );
}

/**
 * Supplier Menu
 * @param param0
 */
function SupplierMenu({ navigation }) {
  return (
    <FloatingMenu
      navigation={navigation}
      data={[
        {
          name: "Pembelian",
          icon: "clipboard-text",
          route: "PurchaseOrder",
        },
        {
          name: "Jadwal",
          icon: "calendar",
          route: "Jadwal",
        },
        {
          name: "Lacak Pembelian",
          icon: "map-marker-distance",
          route: "TrackPurchaseOrder",
        },
        {
          name: "Komoditas",
          icon: "cards-variant",
          route: "Commodity",
        },
        {
          name: "Lacak Kapal",
          icon: "map",
          route: "VesselGeoLocation",
        },
      ]}
    />
  );
}

/**
 * Shipper Menu
 * @param param0
 */
function ShipperMenu({ navigation }) {
  return (
    <FloatingMenu
      navigation={navigation}
      data={[
        {
          name: "Purchase",
          icon: "clipboard-text",
          route: "PurchaseOrder",
        },
        {
          name: "Tracking",
          icon: "map-marker-distance",
          route: "TrackPurchaseOrder",
        },
        {
          name: "Booking",
          icon: "arrow-down-box",
          route: "VesselBooking",
        },
        {
          name: "Biaya",
          icon: "nas",
          route: "BiayaPengurusan",
        },
        {
          name: "Vessel Map",
          icon: "map",
          route: "VesselGeoLocation",
        },
      ]}
    />
  );
}

/**
 * Reseller Menu
 * @param param0
 */
function ResellerMenu({ navigation }) {
  return (
    <FloatingMenu
      navigation={navigation}
      data={[
        {
          name: "Daftar Barang",
          icon: "package",
          route: "Goods",
        },
        {
          name: "Jadwal",
          icon: "calendar",
          route: "Jadwal",
        },
        {
          name: "Vessel Map",
          icon: "map",
          route: "VesselGeoLocation",
        },
      ]}
    />
  );
}

/**
 * Operator Menu
 * @param param0
 */
function OperatorMenu({ navigation }) {
  return (
    <FloatingMenu
      navigation={navigation}
      data={[
        {
          name: "Vessel Booking",
          icon: "book",
          route: "VesselBooking",
        },
        {
          name: "Trayek",
          icon: "map-marker-path",
          route: "Trayek",
        },
        {
          name: "Pelabuhan",
          icon: "anchor",
          route: "Pelabuhan",
        },
        {
          name: "Depot",
          icon: "cube",
          route: "Depot",
        },
        {
          name: "Jadwal",
          icon: "calendar",
          route: "JadwalOperator",
        },
        {
          name: "Kapal",
          icon: "ferry",
          route: "Kapal",
        },
        {
          name: "Manifest",
          icon: "file-pdf",
          route: "Manifest",
        },
        {
          name: "Tracking Data",
          icon: "ferry",
          route: "TrackingData",
        },
        {
          name: "Vessel Map",
          icon: "map",
          route: "VesselGeoLocation",
        },
      ]}
    />
  );
}

/**
 * Regulator Menu
 * @param param0
 */
function RegulatorMenu({ navigation }) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'space-around'}}>
    <FloatingMenu
      navigation={navigation}
      data={[
        {
          name: "Order Barang",
          icon: "clipboard-text",
          route: "PurchaseOrder",
        },
        {
          name: "Lacak Order",
          icon: "map-marker-distance",
          route: "TrackPurchaseOrder",
        },
        {
          name: "Vessel Booking",
          icon: "book",
          route: "VesselBooking",
        },
        {
          name: "Daftar Pelabuhan",
          icon: "anchor",
          route: "Pelabuhan",
        },
        {
          name: "Jadwal Kapal",
          icon: "calendar",
          route: "Jadwal",
        },
        {
          name: "Daftar Kapal",
          icon: "ferry",
          route: "Kapal",
        },
        {
          name: "Jenis Barang",
          icon: "package",
          route: "JenisBarang",
        },
        {
          name: "Jenis Komoditas",
          icon: "package",
          route: "JenisCommodity",
        },
        {
          name: "Barang Penting",
          icon: "package",
          route: "BarangPenting",
        },
        {
          name: "Kemasan",
          icon: "package-variant",
          route: "Kemasan",
        },
        {
          name: "Satuan",
          icon: "package",
          route: "Satuan",
        },
        {
          name: "Vessel Map",
          icon: "map",
          route: "VesselGeoLocation",
        },
        {
          name: "Master Tarif",
          icon: "tag",
          route: "MasterTarif",
        },
        {
          name: "Tipe Container",
          icon: "contain",
          route: "TipeContainer",
        },
        {
          name: "User LCS",
          icon: "account-multiple",
          route: "UserLCS",
        },
        {
          name: "Regulator",
          icon: "shield-account",
          route: "Regulator",
        },
        {
          name: "Manifest",
          icon: "file-table",
          route: "Manifest",
        },
        {
          name: "Report",
          icon: "file-table",
          route: "Report",
        },
        {
          name: "Riwayat Aktifitas",
          icon: "file-table",
          route: "LogAktivitas",
        },
        {
          name: "Master",
          icon: "file-table",
          route: "Master",
        },
        {
          name: "Daftar Tamu",
          icon: "account-multiple",
          route: "Guest",
        },
      ]}
    />
    </View>
  );
}

/**
 * Menu Selector
 * @param param0
 */
const Menu = memo(({ navigation }) => {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        id: 0,
        usertype: false,
      };
  });
  switch (usertype) {
    case "Consignee":
      return <ConsigneeMenu navigation={navigation} />;
    case "Supplier":
      return <SupplierMenu navigation={navigation} />;
    case "Shipper":
      return <ShipperMenu navigation={navigation} />;
    case "Reseller":
      return <ResellerMenu navigation={navigation} />;
    case "Vessel Operator":
      return <OperatorMenu navigation={navigation} />;
    default:
      return null;
  }
});

const DashboardConsignee = () => (
  <>
    <ReportUser />
  </>
)

/**
 * Dashboard Screen
 * @param param0
 */
function Dashboard(props) {
  const { usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        id: 0,
        usertype: false,
      };
  });
  return (
    <View style={{ flex: 1, backgroundColor: Colors.whitesmoke }}>
      <ScrollView>
        <View style={{paddingHorizontal: 20, marginBottom: 10}}>
          <Logo />
          <Space height={10} />
          <Profile />
          <Space height={10} />
          <Greeting />
          <Space height={10} />
          <PanduanPenggunaan />
        </View>
        {usertype === "Regulator" && (
          <>
            <RegulatorMenu {...props}/>
            {/* <DashboardConsignee /> */}
          </>
        )}
        {usertype === "Visitors" && (
          <RegulatorMenu {...props}/>
        )}
        <Menu {...props} />

        <View style={{paddingHorizontal: 20}}>
        {usertype == "Consignee" ? (
          <>
            <LatestPurchaseOrder {...props} />
            <Space height={20} />
            <LatestDistribution {...props} />
          </>
        ) : usertype == "Supplier" ? (
          <>
            <LatestPurchaseOrder {...props} />
            <LatestCommodity {...props} />
          </>
        ) : usertype == "Shipper" ? (
          <>
            <LatestPurchaseOrder {...props} />
          </>
        ) : usertype == "Reseller" ? (
          <>
            <LatestGoods {...props} />
          </>
        ) : usertype == "Vessel Operator" ? (
          <>
            <SisaQuotaTrayek {...props} />
            <VesselBookingTerbaru {...props} />
          </>
        ) : usertype == "Regulator" ? (
          <>
            <ReportUser />
            <Space height={20} />
            <StatusOrder />
            <Space height={20} />
            <OrderMasuk {...props} />
            <Space height={20} />
            <RealisasiMuatan {...props} />
            <Space height={20} />
            <DisparitasHarga {...props} />
            <Space height={20} />
            {/* <SisaKuotaTrayek {...props} /> */}
            {/* <MuatanPerWilayah {...props} /> */}
            {/* <MuatanPerWilayahPerPrioritas {...props} /> */}
            <RealisasiVoyage {...props} />
            <Space height={20} />
            <WaktuTempuh {...props} />
            <Space height={20} />
            {/* <TotalOrderPrioritas {...props} /> */}
            <MuatanTerbanyakPrioritas {...props} />
            {/* <TotalVoyagePerTrayek {...props} /> */}
            {/* <TotalContainerPerPort {...props} /> */}
          </>
        ) : usertype == "Visitors" ? (
          <>
            <ReportUser />
            <Space height={10} />
            <StatusOrder />
            <OrderMasuk {...props} />
            <RealisasiMuatan {...props} />
            <DisparitasHarga {...props} />
            {/* <SisaKuotaTrayek {...props} /> */}
            {/* <MuatanPerWilayah {...props} /> */}
            {/* <MuatanPerWilayahPerPrioritas {...props} /> */}
            <RealisasiVoyage {...props} />
            <WaktuTempuh {...props} />
            {/* <TotalOrderPrioritas {...props} /> */}
            <MuatanTerbanyakPrioritas {...props} />
            {/* <TotalVoyagePerTrayek {...props} /> */}
            {/* <TotalContainerPerPort {...props} /> */}
          </>
        ) : null}
        </View>
      </ScrollView>
      {/* <Image
        source={require("../../assets/img/tollaut.png")}
        style={{
          width: "100%",
          height: 200,
          opacity: 0.5,
          position: "absolute",
          zIndex: -1,
          bottom: 0,
        }}
      /> */}
    </View>
  );
}

export default React.memo(Dashboard);

const styles = StyleSheet.create({
  profileContainer: { flexDirection: "row", alignItems: "center" },
  profileName: { flex: 3 },
  profileImage: { flex: 1, alignItems: "flex-end", justifyContent: "flex-end" },
  gridMenuContainer: {},
  page: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  mapContainer: {
    height: 300,
    width: "100%",
    backgroundColor: "tomato",
  },
  map: {
    flex: 1,
  },
  reload: {
    color: Colors.def,
  },
});


// <Space height={10} />
        // {usertype == "Consignee" ? (
        //   <>
        //     <LatestPurchaseOrder {...props} />
        //     <Space height={20} />
        //     <LatestDistribution {...props} />
        //   </>
        // ) : usertype == "Supplier" ? (
        //   <>
        //     <LatestPurchaseOrder {...props} />
        //     <LatestCommodity {...props} />
        //   </>
        // ) : usertype == "Shipper" ? (
        //   <>
        //     <LatestPurchaseOrder {...props} />
        //   </>
        // ) : usertype == "Reseller" ? (
        //   <>
        //     <LatestGoods {...props} />
        //   </>
        // ) : usertype == "Vessel Operator" ? (
        //   <>
        //     <SisaQuotaTrayek {...props} />
        //     <VesselBookingTerbaru {...props} />
        //   </>
        // ) : usertype == "Regulator" ? (
        //   <>
        //     <ReportUser />
        //     <Space height={10} />
        //     <StatusOrder />
        //     <OrderMasuk {...props} />
        //     <RealisasiMuatan {...props} />
        //     <DisparitasHarga {...props} />
        //     {/* <SisaKuotaTrayek {...props} /> */}
        //     {/* <MuatanPerWilayah {...props} /> */}
        //     {/* <MuatanPerWilayahPerPrioritas {...props} /> */}
        //     <RealisasiVoyage {...props} />
        //     <WaktuTempuh {...props} />
        //     {/* <TotalOrderPrioritas {...props} /> */}
        //     <MuatanTerbanyakPrioritas {...props} />
        //     {/* <TotalVoyagePerTrayek {...props} /> */}
        //     {/* <TotalContainerPerPort {...props} /> */}
        //   </>
        // ) : usertype == "Visitors" ? (
        //   <>
        //     <ReportUser />
        //     <Space height={10} />
        //     <StatusOrder />
        //     <OrderMasuk {...props} />
        //     <RealisasiMuatan {...props} />
        //     <DisparitasHarga {...props} />
        //     {/* <SisaKuotaTrayek {...props} /> */}
        //     {/* <MuatanPerWilayah {...props} /> */}
        //     {/* <MuatanPerWilayahPerPrioritas {...props} /> */}
        //     <RealisasiVoyage {...props} />
        //     <WaktuTempuh {...props} />
        //     {/* <TotalOrderPrioritas {...props} /> */}
        //     <MuatanTerbanyakPrioritas {...props} />
        //     {/* <TotalVoyagePerTrayek {...props} /> */}
        //     {/* <TotalContainerPerPort {...props} /> */}
        //   </>
        // ) : null}
//         <Space height={100} />