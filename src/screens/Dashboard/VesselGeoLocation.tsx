import React, { useState, useRef, useEffect } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import MapboxGL, { ShapeSourceProps } from "@react-native-mapbox-gl/maps";
import vesselStop from "../../assets/img/vessel-stop.png";
import vesselStart from "../../assets/img/vessel-start.png";
import exampleIcon from "../../assets/example.png";
import { useDispatch, useSelector } from "react-redux";
import { vesselTrackingGet } from "../../redux/dashboardReducer";
import { RootState } from "../../redux/rootReducer";
import { Button, Dialog, Paragraph, Portal, Caption } from "react-native-paper";
import { Root, SelectInput } from "../../components";
import { ScrollView } from "react-native-gesture-handler";
import { UPLOAD_URL } from "../../config/constants";
import { isArray } from "lodash";
import { bulanId, DateFormat4 } from "../../services/utils";
import { Formik } from "formik";

const featureCollection = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      id: "9d10456e-bdda-4aa9-9269-04c1667d4552",
      properties: {
        icon: "vesselStop",
      },
      geometry: {
        type: "Point",
        coordinates: [117.43117300739021, -4.649743873292219],
      },
    },
    {
      type: "Feature",
      id: "9d10456e-bdda-4aa9-9269-04c1667d4552",
      properties: {
        icon: "example",
      },
      geometry: {
        type: "Point",
        coordinates: [116.99557036558582, -2.1664354593499864],
      },
    },
    {
      type: "Feature",
      id: "9d10456e-bdda-4aa9-9269-04c1667d4552",
      properties: {
        icon: "example",
      },
      geometry: {
        type: "Point",
        coordinates: [121.46557107601272, -4.772367576794015],
      },
    },
  ],
};

function ShowVessel({ onYes, onNo, visible, onDismiss, children, title }) {
  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Content>
          <ScrollView>{children}</ScrollView>
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={onYes}>OK</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
}

export default function VesselGeoLocation() {
  const [centers, setCenters] = useState([]);
  const [images, setImages] = useState({
    example: exampleIcon,
    vesselStart: vesselStart,
    vesselStop: vesselStop,
  });
  const [visible, setVisible] = useState(false);
  const [vessel, setVessel] = useState({});
  let _timeout = null;
  let mapRef = useRef();
  const dispatch = useDispatch();
  const coordQuad = [
    [-80.425, 46.437], // top left
    [-71.516, 46.437], // top right
    [-71.516, 37.936], // bottom right
    [-80.425, 37.936], // bottom left
  ];
  async function onRegionDidChange() {
    const center = await mapRef.getCenter();
    // console.log(JSON.stringify(center));
    setCenters(center);
  }
  const vesselTrackingGetSuccess = useSelector((state: RootState) =>
    state.dashboard.vesselTrackingGetSuccess
      ? state.dashboard.vesselTrackingGetSuccess.data
      : []
  );
  const year = useSelector((state: RootState) =>
    state.dashboard.vesselTrackingGetSuccess
      ? state.dashboard.vesselTrackingGetSuccess.year
      : []
  );

  let featureCollection = {
    type: "FeatureCollection",
    features: [
      ...vesselTrackingGetSuccess.map((item, key) => ({
        type: "Feature",
        id: String(item.id),
        properties: {
          icon: parseInt(item.speed) > 0 ? "vesselStart" : "vesselStop",
          ...item,
        },
        geometry: {
          type: "Point",
          coordinates: [parseInt(item.lon), parseInt(item.lat)],
        },
      })),
    ],
  };

  function onSourceLayerPress({ features }) {
    const feature = features[0];
    // alert(JSON.stringify(feature.properties));
    setVessel(feature.properties);
    setVisible(true);
    // alert(JSON.stringify(features[0]));
  }
  function onDismiss() {
    setVisible(false);
  }
  // function getLng() {
  //   const center = centers;
  //   return center.length === 2 ? `Lng: ${center[0]}` : "Not available";
  // }

  // function getLat() {
  //   const center = centers;
  //   return center.length === 2 ? `Lat: ${center[1]}` : "Not available";
  // }

  const current = new Date();
  useEffect(() => {
    dispatch(
      vesselTrackingGet({
        month: String(current.getMonth() + 1),
        year: String(current.getFullYear()),
      })
    );
    return () => {
      if (_timeout) clearTimeout(_timeout);
    };
  }, []);
  return (
    <View style={{ flex: 1 }}>
      <MapboxGL.MapView
        onRegionDidChange={() => onRegionDidChange()}
        ref={(c) => (mapRef = c)}
        style={{ flex: 1 }}
      >
        <MapboxGL.Camera
          zoomLevel={3.3}
          centerCoordinate={[116.20462350371821, -3.4481890002535702]}
        />
        <MapboxGL.Images
          nativeAssetImages={["pin"]}
          images={images}
          onImageMissing={(imageKey) =>
            setImages({ ...images, [imageKey]: pinIcon })
          }
        />
        <MapboxGL.ShapeSource
          id="exampleShapeSource"
          shape={featureCollection}
          onPress={onSourceLayerPress}
        >
          <MapboxGL.SymbolLayer id="exampleIconName" style={styles2.icon} />
        </MapboxGL.ShapeSource>
      </MapboxGL.MapView>
      <ShowVessel
        visible={visible}
        onYes={() => setVisible(false)}
        onNo={() => setVisible(false)}
        onDismiss={() => setVisible(false)}
      >
        <>
          <Image
            source={{ uri: UPLOAD_URL + vessel.photo }}
            style={{ height: 100 }}
            resizeMode="contain"
          />
          <Caption>
            {vessel.nama_kapal + "\n"}
            Speed : {vessel.speed} Knot
          </Caption>
          {isArray(vessel.schedules)
            ? vessel.schedules.map((item, key) => (
                <Caption key={key}>
                  {item.kode_trayek}
                  {"\n"}
                  {item.dari_pelabuhan}➤{item.ke_pelabuhan}
                  {"\n"}
                  {DateFormat4(item.tanggal_berangkat)}➤
                  {DateFormat4(item.tanggal_tiba)}
                </Caption>
              ))
            : null}
        </>
      </ShowVessel>
      <View style={{position: 'absolute', bottom: 0, width: '100%', padding: 20}}>
        <Formik
          initialValues={{
            month: "",
            year: "",
          }}
          onSubmit={(val) => dispatch(vesselTrackingGet(val))}
        >
          {({handleSubmit, handleChange, setFieldValue, values}) => (
            <View style={{flex: 1, width: '100%'}}>
                <SelectInput
                  options={bulanId}
                  label="Bulan"
                  objectKey="label"
                  onChangeValue={(val) => setFieldValue('month', val.id)}
                />
                <SelectInput
                  options={year}
                  label="Tahun"
                  objectKey="year"
                  onChangeValue={(val) => setFieldValue('year', val.year)}
                />
              <Button style={{borderBottomEndRadius: 5, borderBottomStartRadius: 5}} onPress={handleSubmit} mode="contained">Proses</Button>
            </View>
          )}
        </Formik>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    color: "white",
  },
});

const styles2 = {
  icon: {
    iconImage: ["get", "icon"],

    iconSize: [
      "match",
      ["get", "icon"],
      "example",
      0.4,
      "vesselStop",
      0.15,
      "vesselStart",
      0.15,
      "airport-15",
      0.15,
      /* default */ 1,
    ],
  },
};
