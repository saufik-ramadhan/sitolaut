import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  List,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput, Space } from "../../components";
import { DateFormat } from "../../services/utils";
import {
  typecontainerFilterClear,
  typecontainerFetchPage,
  typecontainerDelete,
  typecontainerFilterAdd,
} from "./../../redux/typecontainerReducer";
import { Formik } from "formik";

function FloatingAdd({ navigation }: RegulatorStackProps<"TipeContainer">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateTipeContainer");
      }}
    />
  );
}

export default function TipeContainer({
  navigation,
}: RegulatorStackProps<"TipeContainer">) {
  const dispatch = useDispatch();
  const typecontainer = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerData
      ? state.typecontainer.typecontainerData
      : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(typecontainerFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(typecontainerFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(typecontainerDelete(id));
    dispatch(typecontainerFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(typecontainerFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      {/* <Formik
        initialValues={{
          nama: "",
        }}
        onSubmit={(val) => dispatch(typecontainerFilterAdd(val))}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <TextInput
              label="Cari"
              onChangeText={handleChange("nama")}
              onBlur={handleBlur("nama")}
              value={values.nama}
              style={{ flex: 5 }}
            />
            <IconButton
              onPress={handleSubmit}
              icon="magnify"
              style={{ flex: 1 }}
            />
          </View>
        )}
      </Formik> */}
      <FlatList
        data={typecontainer}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Item
              title={item.type_container_name}
              style={{borderBottomWidth: 0.3}}
              right={() => (
                <>
                  <IconButton icon="square-edit-outline" onPress={() => navigation.navigate("UpdateTipeContainer", {item: item})}/>
                  <IconButton icon="delete" color={Colors.danger} onPress={() => {
                    Alert.alert(
                      "Hapus",
                      "Anda yakin akan menghapus jenis container ini ?",
                      [
                        { text: "Ya", onPress: () => handleDelete(item.id)},
                        { text: "Tidak", onPress: () => null }
                      ]
                    );
                  }}/>
                </>
              )}
            />
          );
        }}
      />
      <FloatingAdd navigation={navigation} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});


{/* <Card
  elevation={4}
  style={styles.card}
  onPress={function () {
    null;
  }}
>
  <View style={{ flexDirection: "row", alignItems: "center" }}>
    <Paragraph style={{ flex: 3 }}>
      {item.type_container_name}
    </Paragraph>
    <View style={{ flexDirection: "row" }}>
      <IconButton
        icon="circle-edit-outline"
        color={Colors.def}
        onPress={function () {
          navigation.navigate("UpdateTipeContainer", {
            item: item,
          });
        }}
      />
      <IconButton
        icon="delete"
        color={Colors.danger}
        onPress={function () {
          Alert.alert(
            "Anda yakin",
            "Menghapus Jenis Barang ?",
            [
              {
                text: "OK",
                onPress: () => handleDelete(item.id),
              },
              {
                text: "Cancel",
                onPress: () => null,
              },
            ],
            { cancelable: false }
          );
        }}
      />
    </View>
  </View>
</Card> */}