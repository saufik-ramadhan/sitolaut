import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  typecontainerAdd,
  typecontainerFetchOne,
  typecontainerFilterAdd,
} from "../../redux/typecontainerReducer";
import { Button } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import * as Yup from 'yup';

const Schema = Yup.object({
  type_container_name: Yup.string().required().label("Tipe Kontainer")
})


export default function CreateTipeContainer({
  navigation,
  route,
}: RegulatorStackProps<"CreateTipeContainer">) {
  const dispatch = useDispatch();
  function handleAdd(values) {
    dispatch(typecontainerAdd(values));
  }
  return (
    <Root>
      <Formik
        validationSchema={Schema}
        initialValues={{
          type_container_name: "",
        }}
        onSubmit={(values) => {
          handleAdd(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors }) => (
          <>
            <TextInput
              label="Tipe Container"
              onChangeText={handleChange("type_container_name")}
              onBlur={handleBlur("type_container_name")}
              value={values.type_container_name}
              error={errors.type_container_name}
            />
            <Button onPress={handleSubmit}>Create</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
