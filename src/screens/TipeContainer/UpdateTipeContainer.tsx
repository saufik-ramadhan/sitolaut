import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  typecontainerAdd,
  typecontainerFetchOne,
  typecontainerUpdate,
  typecontainerFilterAdd,
} from "../../redux/typecontainerReducer";
import { Button } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import * as Yup from 'yup';

const Schema = Yup.object({
  type_container_name: Yup.string().required().label("Tipe Kontainer")
})


export default function UpdateTipeContainer({
  navigation,
  route,
}: RegulatorStackProps<"UpdateTipeContainer">) {
  const dispatch = useDispatch();
  const { id, type_container_name } = route.params.item;
  function handleUpdate(values) {
    dispatch(typecontainerUpdate(values));
  }
  return (
    <Root>
      <Formik
        validationSchema={Schema}
        initialValues={{
          id: id,
          type_container_name: type_container_name,
        }}
        onSubmit={(values) => {
          handleUpdate(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values }) => (
          <>
            <TextInput
              label="Tipe Container"
              onChangeText={handleChange("type_container_name")}
              onBlur={handleBlur("type_container_name")}
              placeholder=""
              value={values.type_container_name}
            />
            <Button onPress={handleSubmit}>Update</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
