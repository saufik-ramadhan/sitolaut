import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  List,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput } from "../../components";
import { DateFormat } from "../../services/utils";
import {
  jeniscommodityFilterClear,
  jeniscommodityFetchPage,
  jeniscommodityDelete,
  jeniscommodityFilterAdd,
} from "./../../redux/jeniscommodityReducer";

function FloatingAdd({ navigation }: RegulatorStackProps<"JenisCommodity">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateJenisCommodity");
      }}
    />
  );
}

export default function JenisCommodity({
  navigation,
}: RegulatorStackProps<"JenisCommodity">) {
  const dispatch = useDispatch();
  const jeniscommodity = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityData
      ? state.jeniscommodity.jeniscommodityData
      : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(jeniscommodityFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(jeniscommodityFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(
      jeniscommodityDelete({
        id: id,
      })
    );
    dispatch(jeniscommodityFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(jeniscommodityFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={jeniscommodity}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Item
              title={item.nama_barang}
              description={item.jenis_barang}
              right={() => (
                <>
                  <IconButton icon="square-edit-outline" onPress={function(){navigation.navigate("UpdateJenisCommodity", {item: item});}}/>
                  <IconButton icon="delete" onPress={function(){
                    Alert.alert(
                      "Hapus",
                      "Anda yakin ingin menghapus komoditas ini ?",
                      [
                        {
                          text: "Ya",
                          onPress: () =>
                            dispatch(jeniscommodityDelete(item.id)),
                        },
                        {
                          text: "Tidak",
                          onPress: () => null,
                        },
                      ],
                      { cancelable: false }
                    )
                  }}/>
                </>
              )}
            />
          );
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("JenisCommodityFilter");
        }}
      />
      <FloatingAdd navigation={navigation} />
    </>
  );
}

{/* <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                null;
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Paragraph style={{ flex: 3 }}>
                  {item.nama_barang}
                  {"\n"}
                  <Caption>{item.jenis_barang}</Caption>
                </Paragraph>
                <View style={{ flexDirection: "row" }}>
                  <IconButton
                    icon="circle-edit-outline"
                    color={Colors.def}
                    onPress={function () {
                      navigation.navigate("UpdateJenisCommodity", {
                        item: item
                      });
                    }}
                  />
                  <IconButton
                    icon="delete"
                    color={Colors.danger}
                    onPress={function () {
                      Alert.alert(
                        "Hapus",
                        "Anda yakin ingin menghapus komoditas ini ?",
                        [
                          {
                            text: "Ya",
                            onPress: () =>
                              dispatch(jeniscommodityDelete(item.id)),
                          },
                          {
                            text: "Tidak",
                            onPress: () => null,
                          },
                        ],
                        { cancelable: false }
                      );
                    }}
                  />
                </View>
              </View>
            </Card> */}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});
