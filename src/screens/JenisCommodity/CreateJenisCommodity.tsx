import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root, SelectInput, TextInput, SelectInput2 } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  jeniscommodityAdd,
  jeniscommodityFetchOne,
  jeniscommodityFilterAdd,
} from "../../redux/jeniscommodityReducer";
import { Button } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import * as Yup from 'yup';

const JenisCommoditySchema = Yup.object({
  nama_barang: Yup.string().required().label('Nama Barang'),
  id_jenis_barang: Yup.string().required().label('Jenis Barang')
})

export default function CreateJenisCommodity({
  navigation,
  route,
}: RegulatorStackProps<"CreateJenisCommodity">) {
  const dispatch = useDispatch();
  const jenisbarang = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangGetSuccess
      ? state.jenisbarang.jenisbarangGetSuccess.data
      : [];
  });
  function handleAdd(values) {
    dispatch(jeniscommodityAdd(values));
  }
  useEffect(() => {
    dispatch(jenisbarangFetchAll());
  }, []);
  return (
    <Root style={{padding: 20}}>
      <Formik
        validationSchema={JenisCommoditySchema}
        initialValues={{
          nama_barang: "",
          id_jenis_barang: "",
        }}
        onSubmit={(values) => {
          handleAdd(values);
          navigation.goBack();
        }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          setFieldValue,
          values,
          errors,
          touched,
        }) => (
          <>
            <TextInput
              label="Nama Barang"
              onChangeText={handleChange("nama_barang")}
              onBlur={handleBlur("nama_barang")}
              placeholder=""
              value={values.nama_barang}
              error={errors.nama_barang && touched.nama_barang}
            />
            {/**
             * Jenis Commodity
             */}
            <SelectInput2
              label="Jenis Commodity"
              items={jenisbarang}
              mode="dropdown"
              selectedValue={values.id_jenis_barang}
              onValueChange={(itemValue, itemIndex) => setFieldValue('id_jenis_barang', itemValue)}
              itemLabel="jenis_barang"
              itemValue="id"
              error={errors.id_jenis_barang}
            />
            <Button onPress={handleSubmit}>Create</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
