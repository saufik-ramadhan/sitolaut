import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Center, Root, SelectInput, SelectInput2, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  jeniscommodityAdd,
  jeniscommodityFetchOne,
  jeniscommodityUpdate,
  jeniscommodityFilterAdd,
} from "../../redux/jeniscommodityReducer";
import { ActivityIndicator, Button, Caption } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import * as Yup from 'yup';
import Colors from "../../config/Colors";

const JenisCommoditySchema = Yup.object({
  nama_barang: Yup.string().required().label('Nama Barang'),
  id_jenis_barang: Yup.string().required().label('Jenis Barang')
})

export default function UpdateJenisCommodity({
  navigation,
  route,
}: RegulatorStackProps<"UpdateJenisCommodity">) {
  const dispatch = useDispatch();
  const jeniscommodity = route.params.item;
  const { jenisbarang } = useSelector((state: RootState) => state);
  function handleUpdate(values) {
    dispatch(jeniscommodityUpdate(values));
  }
  useEffect(() => {
    // dispatch(jeniscommodityFetchOne(id));
    dispatch(jenisbarangFetchAll());
  }, []);
  if(!jenisbarang.jenisbarangGetSuccess.data) {
    return <Center><ActivityIndicator color={Colors.pri}/></Center>
  }
  return (
    <Root>
      <Formik
        validationSchema={JenisCommoditySchema}
        initialValues={{
          id: jeniscommodity.id,
          id_jenis_barang: jeniscommodity.id_jenis_barang,
          nama_barang: jeniscommodity.nama_barang,
        }}
        onSubmit={(values) => {
          handleUpdate(values);
          navigation.goBack();
        }}
      >
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          setFieldValue,
          values,
          errors,
          touched
        }) => (
          <View style={{padding: 20}}>
            <TextInput
              label="Nama Barang"
              onChangeText={handleChange("nama_barang")}
              onBlur={handleBlur("nama_barang")}
              value={values.nama_barang}
              error={errors.nama_barang && touched.nama_barang}
            />
            {/**
             * Jenis Commodity
             */}
            <SelectInput2
              label="Jenis Commodity"
              items={jenisbarang.jenisbarangGetSuccess.data}
              mode="dropdown"
              selectedValue={values.id_jenis_barang}
              onValueChange={(itemValue, itemIndex) => setFieldValue('id_jenis_barang', itemValue)}
              itemLabel="label"
              itemValue="value"
            />
            <Button onPress={handleSubmit}>Perbarui</Button>
          </View>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
