import React from "react";
import { StyleSheet, Text, View, Picker } from "react-native";
import { Formik } from "formik";
import { jeniscommodityFilterAdd } from "../../redux/jeniscommodityReducer";
import { IconButton, Button } from "react-native-paper";
import { SelectInput2, TextInput } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";

export default function JenisCommodityFilter({ navigation }) {
  const dispatch = useDispatch();
  const jenisbarang = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangGetSuccess
      ? state.jenisbarang.jenisbarangGetSuccess.data
      : [];
  });
  return (
    <Formik
      initialValues={{
        jenis_barang_id: "",
        nama_barang: "",
      }}
      onSubmit={(val) => {
        dispatch(jeniscommodityFilterAdd(val));
        navigation.goBack();
      }}
    >
      {({ handleChange, handleBlur, handleSubmit, values, setFieldValue }) => (
        <View style={{padding: 20}}>
          <TextInput
            label="Nama Komoditas"
            onChangeText={handleChange("nama_barang")}
            onBlur={handleBlur("nama_barang")}
            value={values.nama_barang}
          />
          {/**
           * Supplier
           */}
          <SelectInput2
            mode="dialog"
            label={"Jenis Komoditas"}
            selectedValue={values.jenis_barang_id}
            onValueChange={(itemValue, itemIndex) => setFieldValue("jenis_barang_id", itemValue)}
            items={jenisbarang}
            itemLabel="label"
            itemValue="id"
          />

          <Button onPress={handleSubmit}>Filter</Button>
        </View>
      )}
    </Formik>
  );
}

const styles = StyleSheet.create({});
