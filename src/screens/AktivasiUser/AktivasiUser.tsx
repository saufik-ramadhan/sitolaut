import React, { useState } from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  Button,
  Portal,
  Dialog,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import {
  EmptyState,
  FilterButton,
  SelectInput,
  Space,
  ErrorText,
} from "../../components";
import { DateFormat, formatRupiah } from "../../services/utils";
import {
  userFilterClear,
  userFetchPage,
  userDelete,
  userFilterAdd,
} from "./../../redux/userReducer";
import { Formik } from "formik";
import * as Yup from "yup";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}

Yup.addMethod(Yup.string, "equalTo", equalTo);
const ValidationSchema = Yup.object().shape({
  id: Yup.number().required(),
  password: Yup.string().required("Password Empty").min(6),
  confirm_password: Yup.string()
    .equalTo(Yup.ref("password"), "Passwords must match")
    .required("Required"),
});

export default function AktivasiUser({
  navigation,
  route,
}: RegulatorStackProps<"AktivasiUser">) {
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState({});
  const user = useSelector(function (state: RootState) {
    return state.user.userGetSuccess ? state.user.userGetSuccess.data : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.user.userGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.user.userFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.user.userGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.user.userAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.user.userDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.user.userViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      status: "0",
      ...filter,
    };
    dispatch(userFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      status: "0",
      ...filter,
    };
    dispatch(userFetchPage(params));
  }
  function hideDialog() {
    setVisible(false);
  }
  function handleDelete(id) {
    dispatch(userDelete(id));
    dispatch(userFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(userFilterClear());
  };
  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={user}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                navigation.navigate("ReadAktivasiUser", {
                  item: item,
                  usery_type_id: item.usery_type_id,
                });
              }}
            >
              <Badge
                style={{
                  backgroundColor:
                    item.status === 1 ? Colors.success : Colors.danger,
                  alignSelf: "flex-start",
                }}
              >
                {item.status === 1 ? "Aktif" : "Non-Aktif"}
              </Badge>
              <Caption style={{ flex: 3 }}>
                <Paragraph style={{ fontWeight: "bold" }}>
                  {item.nama_consignee ||
                    item.nama_supplier ||
                    item.nama_shipper ||
                    item.nama_reseller ||
                    item.nama_operator ||
                    item.nama_regulator}
                </Paragraph>
                {"\n"}
                <Caption style={{ fontWeight: "bold", color: Colors.skyblue }}>
                  {item.email}
                </Caption>
              </Caption>
            </Card>
          );
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("FilterAktivasiUser");
        }}
      />
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Password</Dialog.Title>
          <Dialog.Content>
            <Formik
              validationSchema={ValidationSchema}
              initialValues={{
                confirm_password: "",
                id: data.id,
                password: "",
              }}
              onSubmit={function (val) {
                // dispatch(userResetPassword(val));
                console.log(val);
              }}
            >
              {({
                handleChange,
                handleBlur,
                handleSubmit,
                values,
                errors,
                touched,
              }) => (
                <>
                  <TextInput
                    style={[styles.formItem, styles.textInput]}
                    value={values.password}
                    secureTextEntry={true}
                    onChangeText={handleChange("password")}
                    onBlur={handleBlur("password")}
                    label="Password"
                    keyboardType="ascii-capable"
                  />
                  {errors.password && touched.password && (
                    <ErrorText>{errors.password}</ErrorText>
                  )}
                  <Space height={10} />
                  <TextInput
                    style={[styles.formItem, styles.textInput]}
                    value={values.confirm_password}
                    secureTextEntry={true}
                    onChangeText={handleChange("confirm_password")}
                    onBlur={handleBlur("confirm_password")}
                    label="Konfirmasi Password"
                    keyboardType="ascii-capable"
                  />
                  {errors.confirm_password && touched.confirm_password && (
                    <ErrorText>{errors.confirm_password}</ErrorText>
                  )}
                  <Button onPress={handleSubmit}>Reset</Button>
                </>
              )}
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
  },
  button: {
    position: "absolute",
    right: 0,
  },
});
