import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button, Caption, Dialog, IconButton, List, Paragraph, Portal } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { Root, TextInput } from '../../components';
import Colors from '../../config/Colors';
import { UPLOAD_URL } from '../../config/constants';
import { consigneeFetchOne } from '../../redux/consigneeReducer';
import { operatorFetchOne } from '../../redux/operatorReducer';
import { resellerFetchOne } from '../../redux/resellerReducer';
import { RootState } from '../../redux/rootReducer';
import { shipperFetchOne } from '../../redux/shipperReducer';
import { supplierFetchOne } from '../../redux/supplierReducer';
import { userFetchOne, userFetchUser } from '../../redux/userReducer';
import { RegulatorStackProps } from '../Navigator'
import { handleStatus as handleStatusConsignee } from "../../redux/consigneeReducer";
import { handleStatus as handleStatusSupplier } from "../../redux/supplierReducer";
import { handleStatus as handleStatusShipper } from "../../redux/shipperReducer";
import { handleStatus as handleStatusReseller } from "../../redux/resellerReducer";
import { handleStatus as handleStatusOperator } from "../../redux/operatorReducer";
import { handleStatus as handleStatusUser } from "../../redux/userReducer";

interface ConfirmationDialog {
  onYes: any;
  onNo: any;
  visible: boolean;
  onDismiss: any;
  onChangeText: any;
  content: string;
  title: string;
  value: string;
};

function FormPenolakan({
  onYes,
  onNo,
  visible,
  onDismiss,
  onChangeText,
  content,
  title,
  value,
}: ConfirmationDialog) {
  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Content>
          <TextInput onChangeText={onChangeText} label="Alasan" value={value} />
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={onYes}>Ya</Button>
          <Button onPress={onNo}>Tidak</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
}

export default function ReadAktivasiUser({navigation, route}: RegulatorStackProps<'ReadAktivasiUser'>) {
  let data;
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [alasan, setAlasan] = useState("");
  const pengguna = route.params.item;
  const usertype = route.params.usertype;
  const {consignee, supplier, shipper, reseller, operator, user} = useSelector((state: RootState) => state);
  data = () => {
    switch(usertype) {
      case "Consignee": return consignee.consigneeViewSuccess;
      case "Supplier": return supplier.supplierViewSuccess;
      case "Shipper": return shipper.shipperViewSuccess;
      case "Reseller": return reseller.resellerViewSuccess;
      case "Operator": return operator.operatorViewSuccess;
      case "Regulator": return user.userViewSuccess;
      default: return user.userViewSuccess;
    }
  }
          
  const onHandleStatus = () => {
    const params = {id: pengguna.id, status: "approve"};
    switch(usertype) {
      case "Consignee": dispatch(handleStatusConsignee(params)); break;
      case "Supplier": dispatch(handleStatusSupplier(params)); break;
      case "Shipper": dispatch(handleStatusShipper(params)); break;
      case "Reseller": dispatch(handleStatusReseller(params)); break;
      case "Operator": dispatch(handleStatusOperator(params)); break;
      case "Regulator": dispatch(handleStatusUser(params)); break;
      default: dispatch(handleStatusUser(params));
    }
  }
  const onRejectStatus = (alasan: string) => {
    const params = {id: pengguna.id, status: "reject", alasan: alasan};
    switch(usertype) {
      case "Consignee": dispatch(handleStatusConsignee(params)); break;
      case "Supplier": dispatch(handleStatusSupplier(params)); break;
      case "Shipper": dispatch(handleStatusShipper(params)); break;
      case "Reseller": dispatch(handleStatusReseller(params)); break;
      case "Operator": dispatch(handleStatusOperator(params)); break;
      case "Regulator": dispatch(handleStatusUser(params)); break;
      default: dispatch(handleStatusUser(params));
    }
  }
  useEffect(() => {
    switch(usertype) {
      case "Consignee": dispatch(consigneeFetchOne(pengguna.id)); break;
      case "Supplier": dispatch(supplierFetchOne(pengguna.id)); break;
      case "Shipper": dispatch(shipperFetchOne(pengguna.id)); break;
      case "Reseller": dispatch(resellerFetchOne(pengguna.id)); break;
      case "Operator": dispatch(operatorFetchOne(pengguna.id)); break;
      case "Regulator": dispatch(userFetchOne(pengguna.id)); break;
      default: dispatch(userFetchOne(pengguna.id));
    }
  }, [
    consignee.consigneeAddLoading,
    supplier.supplierAddLoading,
    shipper.shipperAddLoading,
    reseller.resellerAddLoading,
    operator.operatorAddLoading,
    user.userAddLoading
  ]);

  
  if(!data().data) return null;
  return (
    <Root>
      <List.Item
        title={data().data[0].nama_perusahaan}
        description={data().data[0].email}
        left={() => <List.Icon icon="flag"/>}
      />
      {data().data[0].status == 0 ? (
        <>
          <Button mode="contained" onPress={() => onHandleStatus()} color={Colors.pri} icon="account-check">
            Aktifkan
          </Button>
          <Button mode="contained" onPress={() => setVisible(true)} color={Colors.danger} icon="delete" >
            Tolak
          </Button>
        </>
      ) : data().data[0].status == 1 ? 
        <Button compact mode="contained" color={Colors.danger} onPress={() => setVisible(true)}>Deactivate</Button> : 
        <Button compact mode="contained" onPress={() => onHandleStatus()}>Activate</Button>
      }

      <List.Section title="Data Pengguna" style={{backgroundColor: Colors.whitesmoke}}>
        <List.Item title={data().data[0].telp} description={"No. Telp"} />
        <List.Item title={data().data[0].fax} description={"No. Fax"} />
        <List.Item title={data().data[0].alamat} description={"Alamat"} />
        <List.Item title={data().data[0].kota + ", " + data().data[0].provinsi} description={"Kota, Provinsi"} />
      </List.Section>

      <List.Section title="Data PIC" style={{backgroundColor: Colors.whitesmoke}}>
        <List.Item title={data().data[0].telp_pic} description={"No. Telp"} />
        <List.Item title={data().data[0].hp_pic} description={"Handphone"} />
        <List.Item title={data().data[0].fax_pic} description={"No. Fax"} />
      </List.Section>

      <List.Section title="Dokumen" style={{backgroundColor: Colors.whitesmoke}}>
      <List.Item
        title={data().data[0].siup}
        description="SIUP"
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].siup_doc,
                },
              });
            }}
          />
        )}
      />
      <List.Item
        title={data().data[0].npwp}
        description="NPWP"
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].npwp_doc,
                },
              });
            }}
          />
        )}
      />
      <List.Item
        title={"Pakta Integritas"}
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].pakta_integritas,
                },
              });
            }}
          />
        )}
      />
      <List.Item
        title={"Form Penjualan"}
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].form_penjualan,
                },
              });
            }}
          />
        )}
      />
      </List.Section>
      <FormPenolakan
        value={alasan}
        onChangeText={(val) => setAlasan(val)}
        visible={visible}
        onYes={function () {
          setVisible(false);
          onRejectStatus(alasan);
        }}
        onNo={function () {
          setVisible(false);
        }}
        content="Alasan Penolakan ?"
        title="Non Aktif"
        onDismiss={function () {
          setVisible(false);
        }}
      />

    </Root>
  )
}

const styles = StyleSheet.create({})


// import React from "react";
// import { Root, Heading5, Space, Icon } from "../../components";
// import {
//   Paragraph,
//   List,
//   Caption,
//   Button,
//   Dialog,
//   Portal,
//   TextInput,
// } from "react-native-paper";
// import Colors from "./../../config/Colors";
// import { StyleSheet, View, Image, RefreshControl } from "react-native";
// import { useSelector, useDispatch } from "react-redux";
// import { RootState } from "../../redux/rootReducer";
// import { doLogout } from "../../redux/authReducers";
// import {
//   Placeholder,
//   PlaceholderMedia,
//   PlaceholderLine,
//   Fade,
// } from "rn-placeholder";
// import { consigneeFetchOne, consigneeFetchUser } from "../../redux/consigneeReducer";
// import { supplierFetchOne, supplierFetchUser } from "../../redux/supplierReducer";
// import { shipperFetchOne, shipperFetchUser } from "../../redux/shipperReducer";
// import { resellerFetchOne, resellerFetchUser } from "./../../redux/resellerReducer";
// import { operatorFetchOne, operatorFetchUser } from "./../../redux/operatorReducer";
// import { userFetchOne, userFetchUser } from "../../redux/userReducer";
// import { UPLOAD_URL } from "../../config/constants";
// import { handleStatus as handleStatusConsignee } from "../../redux/consigneeReducer";
// import { handleStatus as handleStatusSupplier } from "../../redux/supplierReducer";
// import { handleStatus as handleStatusShipper } from "../../redux/shipperReducer";
// import { handleStatus as handleStatusReseller } from "../../redux/resellerReducer";
// import { handleStatus as handleStatusOperator } from "../../redux/operatorReducer";
// import { handleStatus as handleStatusUser } from "../../redux/userReducer";

// type ConfirmationDialog = {
//   onYes: any;
//   onNo: any;
//   visible: boolean;
//   onDismiss: any;
//   onChangeText: any;
//   content: string;
//   title: string;
//   value: string;
// };

// function LogoutConfirmation({
//   onYes,
//   onNo,
//   visible,
//   onDismiss,
//   onChangeText,
//   content,
//   title,
//   value,
// }: ConfirmationDialog) {
//   return (
//     <Portal>
//       <Dialog visible={visible} onDismiss={onDismiss}>
//         <Dialog.Title>{title}</Dialog.Title>
//         <Dialog.Content>
//           <Paragraph>{content}</Paragraph>
//           <TextInput onChangeText={onChangeText} label="Alasan" value={value} />
//         </Dialog.Content>
//         <Dialog.Actions>
//           <Button onPress={onYes}>Ya</Button>
//           <Button onPress={onNo}>Tidak</Button>
//         </Dialog.Actions>
//       </Dialog>
//     </Portal>
//   );
// }

// function ReadAktivasiUserPhoto({ nama, email, usertype }) {
//   return (
//     <View style={styles.profileContainer}>
//       <Image
//         style={styles.profileImage}
//         source={require("../../assets/img/download.png")}
//         resizeMode="center"
//       />
//       <Heading5>{nama}</Heading5>
//       <Caption>{email}</Caption>
//       <Paragraph>
//         <Icon name="shield-check" />{" "}
//         {usertype == 1
//           ? "Consignee"
//           : usertype == 2
//           ? "Supplier"
//           : usertype == 3
//           ? "Shipper"
//           : usertype == 4
//           ? "Reseller"
//           : usertype == 5
//           ? "Vessel Operator"
//           : "Regulator"}
//       </Paragraph>
//     </View>
//   );
// }

// export default function ReadAktivasiUser({ navigation, route }) {
//   const [refreshing, setRefreshing] = React.useState(false);
//   const [logout, setLogout] = React.useState(false);
//   const [visible, setVisible] = React.useState(false);
//   const [alasan, setAlasan] = React.useState("");
//   const user_id = route.params.id;
//   const usery_type_id = route.params.usery_type_id;
//   const dispatch = useDispatch();
//   const { id, usertype } = useSelector(function (state: RootState) {
//     return state.auth.authLoginSuccess
//       ? state.auth.authLoginSuccess.data.user[0]
//       : {
//           usertype: "Not Defined",
//         };
//   });
//   const loading = useSelector(function (state: RootState) {
//     switch (usery_type_id) {
//       case "Consignee":
//         return state.consignee.consigneeViewLoading;
//       case "Supplier":
//         return state.supplier.supplierViewLoading;
//       case "Shipper":
//         return state.shipper.shipperViewLoading;
//       case "Reseller":
//         return state.reseller.resellerViewLoading;
//       case "Vessel Operator":
//         return state.operator.operatorViewLoading;
//       case "Regulator":
//         return state.user.userViewLoading;
//       default:
//         return false;
//     }
//   });
//   const consigneeLoading = useSelector(
//     (state: RootState) => state.consignee.consigneeAddLoading
//   );
//   const supplierLoading = useSelector(
//     (state: RootState) => state.supplier.supplierAddLoading
//   );
//   const shipperLoading = useSelector(
//     (state: RootState) => state.shipper.shipperAddLoading
//   );
//   const resellerLoading = useSelector(
//     (state: RootState) => state.reseller.resellerAddLoading
//   );
//   const operatorLoading = useSelector(
//     (state: RootState) => state.operator.operatorAddLoading
//   );
//   const userLoading = useSelector(
//     (state: RootState) => state.user.userAddLoading
//   );
//   const onRefresh = React.useCallback(() => {
//     handleFetchUser();
//   }, [refreshing]);
//   const {
//     nama_perusahaan,
//     alamat,
//     nama_regulator,
//     email_perusahaan,
//     telp,
//     fax,
//     siup,
//     siup_doc,
//     npwp,
//     npwp_doc,
//     nama_pic,
//     email_pic,
//     hp_pic,
//     telp_pic,
//     fax_pic,
//     kota,
//     provinsi,
//     pakta_integritas,
//     form_penjualan,
//     status,
//     alasan_ditolak,
//   } = useSelector(function (state: RootState) {
//     switch (usery_type_id) {
//       case 1:
//         return state.consignee.consigneeViewSuccess
//           ? state.consignee.consigneeViewSuccess.data[0]
//           : false;
//       case 2:
//         return state.supplier.supplierViewSuccess
//           ? state.supplier.supplierViewSuccess.data[0]
//           : false;
//       case 3:
//         return state.shipper.shipperViewSuccess
//           ? state.shipper.shipperViewSuccess.data[0]
//           : false;
//       case 4:
//         return state.reseller.resellerViewSuccess
//           ? state.reseller.resellerViewSuccess.data[0]
//           : false;
//       case 5:
//         return state.operator.operatorViewSuccess
//           ? state.operator.operatorViewSuccess.data[0]
//           : false;
//       case 6:
//         return state.consignee.consigneeViewSuccess
//           ? state.consignee.consigneeViewSuccess.data[0]
//           : false;
//       default:
//         return false;
//     }
//   });
//   function onHandleStatus() {
//     switch (usery_type_id) {
//       case 1:
//         dispatch(
//           handleStatusConsignee({
//             id: user_id,
//             status: "approve",
//           })
//         );
//         break;
//       case 2:
//         dispatch(
//           handleStatusSupplier({
//             id: user_id,
//             status: "approve",
//           })
//         );
//         break;
//       case 3:
//         dispatch(
//           handleStatusShipper({
//             id: user_id,
//             status: "approve",
//           })
//         );
//         break;
//       case 4:
//         dispatch(
//           handleStatusReseller({
//             id: user_id,
//             status: "approve",
//           })
//         );
//         break;
//       case 5:
//         dispatch(
//           handleStatusOperator({
//             id: user_id,
//             status: "approve",
//           })
//         );
//         break;
//       case 6:
//         dispatch(
//           handleStatusUser({
//             id: user_id,
//             status: "approve",
//           })
//         );
//         break;
//       default:
//         return false;
//     }
//   }
//   function onRejectStatus(alasan: String) {
//     switch (usery_type_id) {
//       case 1:
//         dispatch(
//           handleStatusConsignee({
//             alasan: alasan,
//             id: user_id,
//             status: "reject",
//           })
//         );
//         break;
//       case 2:
//         dispatch(
//           handleStatusSupplier({
//             alasan: alasan,
//             id: user_id,
//             status: "reject",
//           })
//         );
//         break;
//       case 3:
//         dispatch(
//           handleStatusShipper({
//             alasan: alasan,
//             id: user_id,
//             status: "reject",
//           })
//         );
//         break;
//       case 4:
//         dispatch(
//           handleStatusReseller({
//             alasan: alasan,
//             id: user_id,
//             status: "reject",
//           })
//         );
//         break;
//       case 5:
//         dispatch(
//           handleStatusOperator({
//             alasan: alasan,
//             id: user_id,
//             status: "reject",
//           })
//         );
//         break;
//       case 6:
//         dispatch(
//           handleStatusUser({
//             alasan: alasan,
//             id: user_id,
//             status: "reject",
//           })
//         );
//         break;
//       default:
//         return false;
//     }
//   }
//   function handleFetchUser() {
//     setRefreshing(true);
//     switch (usery_type_id) {
//       case 1:
//         dispatch(consigneeFetchOne(user_id));
//         break;
//       case 2:
//         dispatch(supplierFetchOne(user_id));
//         break;
//       case 3:
//         dispatch(shipperFetchOne(user_id));
//         break;
//       case 4:
//         dispatch(resellerFetchOne(user_id));
//         break;
//       case 5:
//         dispatch(operatorFetchOne(user_id));
//         break;
//       case 6:
//         dispatch(userFetchOne(user_id));
//         break;
//       default:
//         return false;
//     }
//     setRefreshing(false);
//   }
//   React.useEffect(() => {
//     handleFetchUser();
//   }, [
//     consigneeLoading,
//     supplierLoading,
//     shipperLoading,
//     resellerLoading,
//     operatorLoading,
//     userLoading,
//   ]);
//   return (
//     <Root
//       style={styles.container}
//       refreshControl={
//         <RefreshControl refreshing={refreshing} onRefresh={() => onRefresh()} />
//       }
//     >
//       <>
//         <ReadAktivasiUserPhoto
//           nama={nama_perusahaan || nama_regulator}
//           email={email_perusahaan}
//           usertype={usery_type_id}
//         />
//         <View style={{ flexDirection: "row", justifyContent: "center" }}>
//           {status == 0 && (
//             <>
//               <Button
//                 mode="contained"
//                 onPress={function () {
//                   onHandleStatus();
//                 }}
//                 color={Colors.pri}
//                 icon="account-check"
//               >
//                 Aktifkan
//               </Button>
//               <Button
//                 mode="contained"
//                 onPress={function () {
//                   setVisible(true);
//                 }}
//                 color={Colors.danger}
//                 icon="delete"
//               >
//                 Tolak
//               </Button>
//             </>
//           )}
//           {status == 2 && (
//             <Button
//               mode="contained"
//               onPress={function () {
//                 onHandleStatus();
//               }}
//               color={Colors.pri}
//               icon="account-check"
//             >
//               Aktifkan
//             </Button>
//           )}
//           {status == 1 && (
//             <Button
//               mode="contained"
//               onPress={function () {
//                 setVisible(true);
//               }}
//               color={Colors.danger}
//               icon="delete"
//             >
//               Non Aktifkan
//             </Button>
//           )}
//         </View>
//         <List.Section>
//           <List.Subheader>Status</List.Subheader>
//           <List.Item
//             title={
//               status == 0 ? "Non Aktif" : status == 1 ? "Aktif" : "Ditolak"
//             }
//             description={status == 2 && "Alasan : " + alasan_ditolak}
//             titleNumberOfLines={4}
//             titleStyle={{
//               fontSize: 13,
//             }}
//             left={() => <List.Icon icon="shield" />}
//           />
//           {/**
//            * Details User
//            */}
//           <List.Subheader>Details</List.Subheader>
//           {loading ? (
//             <View style={{ paddingHorizontal: 20 }}>
//               <Placeholder Animation={Fade} Left={PlaceholderMedia}>
//                 <PlaceholderLine width={80} />
//                 <PlaceholderLine width={30} />
//               </Placeholder>
//             </View>
//           ) : (
//             <>
//               <List.Item
//                 title={`${alamat},${kota},${provinsi}`}
//                 description="Alamat"
//                 titleNumberOfLines={4}
//                 titleStyle={{
//                   fontSize: 13,
//                 }}
//                 left={() => <List.Icon icon="office-building" />}
//               />
//               <List.Item
//                 title={telp}
//                 description="No. Telp"
//                 left={() => <List.Icon color="#000" icon="phone-classic" />}
//               />
//               <List.Item
//                 title={fax}
//                 description="No. Fax"
//                 left={() => <List.Icon color="#000" icon="fax" />}
//               />
//             </>
//           )}

//           {/**
//            * Dokumen
//            */}
//           <List.Subheader>Dokumen</List.Subheader>
//           {loading ? (
//             <View style={{ paddingHorizontal: 20 }}>
//               <Placeholder Animation={Fade} Left={PlaceholderMedia}>
//                 <PlaceholderLine width={80} />
//                 <PlaceholderLine width={30} />
//               </Placeholder>
//             </View>
//           ) : (
//             <>
//               <List.Item
//                 title={siup}
//                 description="SIUP"
//                 left={() => <List.Icon color="#000" icon="file-document" />}
//                 right={() => (
//                   <Button
//                     mode="outlined"
//                     color={Colors.pri}
//                     style={{ borderWidth: 0 }}
//                     labelStyle={{ fontSize: 12 }}
//                     onPress={function () {
//                       // alert("pressed");
//                       navigation.navigate("Modals", {
//                         screen: "ImageModal",
//                         params: {
//                           url: UPLOAD_URL + siup_doc,
//                         },
//                       });
//                     }}
//                   >
//                     Lihat
//                   </Button>
//                 )}
//               />
//               <List.Item
//                 title={npwp}
//                 description="NPWP"
//                 left={() => <List.Icon color="#000" icon="file-document" />}
//                 right={() => (
//                   <Button
//                     mode="outlined"
//                     color={Colors.pri}
//                     style={{ borderWidth: 0 }}
//                     labelStyle={{ fontSize: 12 }}
//                     onPress={function () {
//                       navigation.navigate("Modals", {
//                         screen: "ImageModal",
//                         params: {
//                           url: UPLOAD_URL + npwp_doc,
//                         },
//                       });
//                     }}
//                   >
//                     Lihat
//                   </Button>
//                 )}
//               />
//               {usery_type_id === 1 ? (
//                 <>
//                   <List.Item
//                     title={"Pakta Integritas"}
//                     left={() => <List.Icon color="#000" icon="file-document" />}
//                     right={() => (
//                       <Button
//                         mode="outlined"
//                         color={Colors.pri}
//                         style={{ borderWidth: 0 }}
//                         labelStyle={{ fontSize: 12 }}
//                         onPress={function () {
//                           navigation.navigate("Modals", {
//                             screen: "ImageModal",
//                             params: {
//                               url: UPLOAD_URL + pakta_integritas,
//                             },
//                           });
//                         }}
//                       >
//                         Lihat
//                       </Button>
//                     )}
//                   />
//                   <List.Item
//                     title={"Form Penjualan"}
//                     left={() => <List.Icon color="#000" icon="file-document" />}
//                     right={() => (
//                       <Button
//                         mode="outlined"
//                         color={Colors.pri}
//                         style={{ borderWidth: 0 }}
//                         labelStyle={{ fontSize: 12 }}
//                         onPress={function () {
//                           navigation.navigate("Modals", {
//                             screen: "ImageModal",
//                             params: {
//                               url: UPLOAD_URL + form_penjualan,
//                             },
//                           });
//                         }}
//                       >
//                         Lihat
//                       </Button>
//                     )}
//                   />
//                 </>
//               ) : null}
//             </>
//           )}

//           {/**
//            * Details User
//            */}
//           <List.Subheader>Data PIC</List.Subheader>
//           {loading ? (
//             <View style={{ paddingHorizontal: 20 }}>
//               <Placeholder Animation={Fade} Left={PlaceholderMedia}>
//                 <PlaceholderLine width={80} />
//                 <PlaceholderLine width={30} />
//               </Placeholder>
//             </View>
//           ) : (
//             <>
//               <List.Item
//                 title={nama_pic}
//                 description="Nama PIC"
//                 left={() => <List.Icon color="#000" icon="account" />}
//               />
//               <List.Item
//                 title={email_pic}
//                 description="Email"
//                 left={() => <List.Icon color="#000" icon="email" />}
//               />
//               <List.Item
//                 title={hp_pic}
//                 description="Phone"
//                 left={() => <List.Icon color="#000" icon="cellphone" />}
//               />
//               <List.Item
//                 title={telp_pic}
//                 description="No. Telp"
//                 left={() => <List.Icon color="#000" icon="phone-classic" />}
//               />
//               <List.Item
//                 title={fax_pic}
//                 description="No. Fax"
//                 left={() => <List.Icon color="#000" icon="fax" />}
//               />
//             </>
//           )}

//           {/* <Button
//             color={Colors.danger}
//             onPress={function () {
//               setLogout(true);
//             }}
//           >
//             LOGOUT
//           </Button> */}
//         </List.Section>
//         <LogoutConfirmation
//           value={alasan}
//           onChangeText={(val) => setAlasan(val)}
//           visible={visible}
//           onYes={function () {
//             setVisible(false);
//             onRejectStatus(alasan);
//           }}
//           onNo={function () {
//             setVisible(false);
//           }}
//           content="Alasan Penolakan ?"
//           title="Non Aktif"
//           onDismiss={function () {
//             setVisible(false);
//           }}
//         />
//       </>
//       {/* )} */}
//     </Root>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: Colors.white,
//   },
//   profileContainer: {
//     alignItems: "center",
//     paddingVertical: 20,
//     borderBottomColor: Colors.gray1,
//     borderBottomWidth: 1,
//   },
//   profileImage: { width: 50, height: 50, borderRadius: 5 },
// });
