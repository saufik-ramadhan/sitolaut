import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  Button,
  Portal,
  Dialog,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput } from "../../components";
import { DateFormat, FormatTanggal, formatRupiah } from "../../services/utils";
import {
  manifestFilterClear,
  manifestFetchAll,
  manifestFilterAdd,
  manifestFetchOne,
} from "./../../redux/manifestReducer";
import { getBookingList } from "../../redux/bookingReducer";
import * as Print from "expo-print";
import Confirmation from "../../components/Confirmation";
import HeaderPage from "./HeaderPage";
import { ManifestPdf } from "./ManifestPdf";
import { WebView } from "react-native-webview";
import { UPLOAD_URL } from "../../config/constants";
import { isArray } from "lodash";
import { Formik } from "formik";

export default function Manifest({
  navigation,
}: RegulatorStackProps<"Manifest">) {
  const dispatch = useDispatch();
  const [visible, setVisible] = React.useState(false);
  const [filterShow, setFilterShow] = React.useState(false);
  const manifest = useSelector(function (state: RootState) {
    return state.manifest.manifestData ? state.manifest.manifestData : [];
  });
  const { hasNext, currentPage, trayek, year } = useSelector(function (state: RootState) {
    return state.manifest.manifestGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.manifest.manifestFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.manifest.manifestGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.manifest.manifestAddLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.manifest.manifestViewLoading;
  });
  const manifestGet = useSelector(function (state: RootState) {
    return state.booking.bookingListSuccess
      ? state.booking.bookingListSuccess.data
      : [];
  });
  const manifestView = useSelector(function (state: RootState) {
    return state.manifest.manifestViewSuccess
      ? state.manifest.manifestViewSuccess.data
      : {};
  });
  const schedule = useSelector(function (state: RootState) {
    return state.manifest.manifestViewSuccess
      ? state.manifest.manifestViewSuccess.data.schedule
      : {};
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      user_id: usertype == "Vessel Operator" ? id : "",
      ...filter,
    };
    dispatch(manifestFetchAll(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(manifestFetchAll(params));
  }

  function cetakManifest() {
    const options = {
      html: `
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

        <html>
        <head>
          
          <meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
          <title></title>
          <meta name="generator" content="LibreOffice 6.4.3.2 (Windows)"/>
          <meta name="created" content="2020-08-31T17:29:21.437000000"/>
          <meta name="changed" content="2020-08-31T18:02:39.463000000"/>
          
          <style type="text/css">
            body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Liberation Sans"; font-size:x-small }
            a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
            a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
            comment { display:none;  } 
          </style>
          
        </head>
        
        <body>
        <table cellspacing="0" border="0">
          <colgroup width="73"></colgroup>
          <colgroup width="102"></colgroup>
          <colgroup width="113"></colgroup>
          <colgroup width="78"></colgroup>
          <colgroup width="99"></colgroup>
          <colgroup width="71"></colgroup>
          <colgroup width="94"></colgroup>
          <colgroup width="85"></colgroup>
          <colgroup width="104"></colgroup>
          <colgroup width="73"></colgroup>
          <colgroup width="56"></colgroup>
          <colgroup width="101"></colgroup>
          <colgroup width="106"></colgroup>
          <tr>
            <td colspan=2 rowspan=6 height="102" align="left"><font size=1><br><img src="${
              UPLOAD_URL + schedule.logo_perusahaan
            }" width=80 height=77 hspace=48 vspace=15>
            </font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
          </tr>
          <tr>
            <td colspan=9 rowspan=5 align="center" valign=middle><b><font size=5>${
              schedule.nama_perusahaan
            }</font></b></td>
            </tr>
          <tr>
            </tr>
          <tr>
            </tr>
          <tr>
            </tr>
          <tr>
            </tr>
          <tr>
            <td colspan=2 height="17" align="center" valign=middle><b><font face="Arial" size=1>FREIGHT MANIFEST</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=middle><b><font face="Arial" size=1>NAMA KAPAL : ${
              schedule.nama_kapal
            }</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=middle><b><font face="Arial" size=1>RUTE/VOYAGE : ${
              schedule.kode
            }/${schedule.voyage}</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 align="left" valign=middle><b><font face="Arial" size=1>TANGGAL ETD : ${DateFormat(
              schedule.tanggal_berangkat
            )}</font></b></td>
            <td colspan=2 align="center" valign=middle><font face="Arial" size=1>Cabang</font></td>
          </tr>
          <tr>
            <td colspan=2 height="17" align="center" valign=middle><b><font face="Arial" size=1>NO. MNF44</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=4 align="left" valign=middle><b><font face="Arial" size=1>PELABUHAN MUAT : ${
              schedule.dari_pelabuhan
            }</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=5 align="left" valign=middle><b><font face="Arial" size=1>PELABUHAN BONGKAR : ${
              schedule.ke_pelabuhan
            }</font></b></td>
            <td colspan=2 align="center" valign=middle><font face="Arial" size=1>${
              schedule.kota
            }</font></td>
          </tr>
          <tr>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 height="52" align="center" valign=middle><font face="Arial" size=1>No. Konosemen<br>No. of B/L </font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>PENGIRIM<br>SHIPPER</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>PENERIMA<br>CONSIGNEE</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>Banyaknya<br>koli/ekor<br>Quantity of<br>Packages</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>MEREK &amp; NOMOR<br>MARK</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>Jenis<br>Bungkusan<br>Kind of<br>Packages</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>ISINYA<br>CONTENTS</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>UKURAN<br>Dalam M3<br>MEASUREMENT</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle><font face="Arial" size=1>BERAT<br>Dalam KG<br>WEIGHT</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="center" valign=middle><font face="Arial" size=1>Tambang Satuan<br>Freight per Load</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="center" valign=middle><font face="Arial" size=1>UANG TAMBANG FREIGHT</font></td>
            </tr>
          <tr>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Total Prepaid</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Per</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Total Prepaid</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="Arial" size=1>Total payable at destination</font></td>
          </tr>
          <tr>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="17" align="center"><font face="Arial" size=1>(1)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(2)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(3)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(4)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(5)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(6)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(7)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(8)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(9)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(10)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(11)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(12)</font></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>(13)</font></td>
          </tr>
          ${
            isArray(manifestView.manifest) &&
            manifestView.manifest.map((item, key) =>
              item.containers.map(
                (i, key2) =>
                  `
                <tr>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="17" align="center"><font face="Arial" size=1>${
                    item.bill_lading_number
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    item.perusahaan_shipper
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    item.perusahaan_consignee
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    item.total_container
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    i.no_container
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>empty</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    i.nama_barang
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    i.berat_total
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    i.berat_total
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${formatRupiah(
                    parseInt(i.harga)
                  )}</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    item.type_container_name
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1>${
                    item.total_harga_container
                  }</font></td>
                  <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><font face="Arial" size=1></font></td>
                </tr>
                `
              )
            )
          }
          <tr>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="17" align="left"><b><font size=1><br></font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b><font size=1>JUMLAH</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b><font size=1><br></font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" sdval="0" sdnum="9216;"><b><font size=1>${
              manifestView.total_container
            }</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b><font size=1><br></font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b><font size=1><br></font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b><font size=1><br></font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdval="0" sdnum="9216;"><b><font size=1>${
              manifestView.total_kg
            }</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdval="0" sdnum="9216;"><b><font size=1>${
              manifestView.total_kg
            }</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b><font size=1><br></font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b><font size=1><br></font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" sdval="0" sdnum="9216;"><b><font size=1>${
              manifestView.total_muatan
            }</font></b></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center"><b><font size=1><br></font></b></td>
          </tr>
          <tr>
            <td height="17" align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
          </tr>
          <tr>
            <td height="17" align="left"><font size=1>REKAPITULASI</font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
          </tr>
          <tr>
            <td colspan=2 height="17" align="left" valign=middle><font size=1>CONTAINER (unit)</font></td>
            <td align="left"><font size=1>:</font></td>
            <td align="right" sdval="0" sdnum="9216;"><font size=1>${
              manifestView.total_container
            }</font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td colspan=4 align="left" valign=middle><font size=1>${
              schedule.kota
            }, ${FormatTanggal(new Date())}</font></td>
            </tr>
          <tr>
            <td colspan=2 height="17" align="left" valign=middle><font size=1>MOBIL (unit)</font></td>
            <td align="left"><font size=1>:</font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td colspan=4 rowspan=4 align="left" valign=middle><font size=1>
            <img src="${
              UPLOAD_URL + schedule.foto_ttd
            }" width=80 height=77 hspace=0 vspace=0></font></td>
            </tr>
          <tr>
            <td colspan=2 height="17" align="left" valign=middle><font size=1>JUMLAH GENCAR (koly</font></td>
            <td align="left"><font size=1>:</font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            </tr>
          <tr>
            <td colspan=2 height="17" align="left" valign=middle><font size=1>JUMLAH (kgs)</font></td>
            <td align="left"><font size=1>:</font></td>
            <td align="right" sdval="0" sdnum="9216;"><font size=1>${
              manifestView.total_kg
            }</font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            </tr>
          <tr>
            <td colspan=2 height="17" align="left" valign=middle><font size=1>JUMLAH TONAGE (T/M3)</font></td>
            <td align="left"><font size=1>:</font></td>
            <td align="right" sdval="0" sdnum="9216;"><font size=1>${
              manifestView.total_kg
            }</font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            </tr>
          <tr>
            <td colspan=2 height="17" align="left" valign=middle><font size=1>JUMLAH UT.MUATAN (Rp)</font></td>
            <td align="left"><font size=1>:</font></td>
            <td align="right" sdval="0" sdnum="9216;"><font size=1>${
              manifestView.total_muatan
            }</font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td align="left"><font size=1><br></font></td>
            <td colspan=4 align="left" valign=middle><font size=1>Asep</font></td>
            </tr>
        </table>
        <!-- ************************************************************************** -->
        </body>
        
        </html>
      `,
      width: 1008,
      height: 612,
    };
    Print.printAsync(options);
  }

  const onRefresh = () => {
    dispatch(manifestFilterClear());
  };
  const token = useSelector(function (state: RootState) {
    return state.auth.token;
  });
  const form = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess.data.form;
  });
  const { id, usertype, user_type_id, email } = useSelector(function (
    state?: RootState | any
  ) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  // const runFirst = `
  //   dispatch(doLogin({

  //   }));
  // `;
  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess]);

  return (
    <>
      <FlatList
        data={manifest}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                null;
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Paragraph style={{ flex: 3 }}>
                  {item.kode}
                  {"\n"}
                  <Caption>
                    Voyage {item.voyage} ({item.tahun})
                  </Caption>
                </Paragraph>
                <Caption style={{ flex: 3 }}>
                  {item.dari_pelabuhan}
                  {"\n"}
                  {item.ke_pelabuhan}
                </Caption>
                <View style={{ flex: 1 }}>
                  <IconButton
                    icon="file-pdf"
                    onPress={function () {
                      dispatch(manifestFetchOne(item.id));
                      setVisible(true);
                    }}
                  />
                </View>
              </View>
            </Card>
          );
        }}
      />

      <FilterButton
        onPress={function () {
          setFilterShow(true);
        }}
      />

      {/** Filter */}
      <Portal>
        <Dialog visible={filterShow}>
          <Dialog.Content>
            <Formik
              initialValues={{
                trayek: "",
                year: "",
              }}
              onSubmit={(val) => {
                // alert(JSON.stringify(val))
                dispatch(manifestFilterAdd(val));
                setFilterShow(false);
              }}
            >
              {({handleSubmit, handleChange, handleBlur, setFieldValue, values}) => (
                <View>
                  <SelectInput
                    options={trayek}
                    onChangeValue={(val) => setFieldValue('trayek', val.value)}
                    label="Trayek"
                    objectKey="label"
                  />
                  <SelectInput
                    options={year}
                    onChangeValue={(val) => setFieldValue('year', val.value)}
                    label="Trayek"
                    objectKey="label"
                  />
                  <Button onPress={handleSubmit} mode="contained">Filter</Button>
                </View>
              )}
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>
      {/** -- */}

      <Confirmation
        visible={visible}
        onYes={function () {
          setVisible(false);
          cetakManifest();
        }}
        onNo={function () {
          setVisible(false);
        }}
        content="Print Manifest ?"
        title="Manifest"
        onDismiss={function () {
          setVisible(false);
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});
