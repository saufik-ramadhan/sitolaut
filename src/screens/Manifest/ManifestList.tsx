import React, { Component } from 'react'
import { connect } from 'react-redux'
import { manifestFetchAll } from '../../actions/manifestAction'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import moment from 'moment'
import {
  Card, CardHeader, CardBody, CardFooter,
  Col, Row, Table, Button,
  FormGroup, InputGroup, InputGroupText, InputGroupAddon,
  Pagination, PaginationItem, PaginationLink
} from 'reactstrap'
import { AvForm, AvInput } from 'availity-reactstrap-validation'

let userId = localStorage.getItem('tolaut@userId') || null
let userType = localStorage.getItem('tolaut@typeUser') || null

class ManifestList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      curPage: 1,
      start: 0,
      perPage: 10,
      trayek: '',
      year: 0
    }

    this.handleSearch = this.handleSearch.bind(this)
  }

  componentDidMount() {
    const data = {
       length: this.state.perPage
    }
    console.log(userType)
    if (userType != 6) data.user_id = userId

    if (this.props.match.params.pageNum === undefined) {
      data.start = 0
    } else {
      let offset = (this.props.match.params.pageNum * 10) - 10
      data.start = offset
    }

    this.props.manifestFetchAll(data)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.pageNum !== this.state.curPage) {
      this.setState({ curPage: nextProps.match.params.pageNum })

      const data = {
        length: this.state.perPage,
        trayek: this.state.trayek,
        year: this.state.year
      }

      if (userType != 6) data.user_id = userId

      if (nextProps.match.params.pageNum === undefined) {
        data.start = 0
      } else {
        let offset = (nextProps.match.params.pageNum * 10) - 10
        data.start = offset
      }

      console.log(data)

      this.props.manifestFetchAll(data)
    }
  }

  handleSearch(event, values) {
    this.setState({
      trayek: values.trayek,
      year: values.year,
    })

    const data = {
      length: this.state.perPage,
      trayek: this.state.trayek,
      year: this.state.year,
      start: 0
    }
    if (userType != 6) data.user_id = userId

    this.props.manifestFetchAll(data)
    this.props.history.replace('/manifest/page/1')
  }

  renderData() {
    if (this.props.isSuccess) {
      const defaultValues = {
        trayek: this.state.trayek,
        year: this.state.year,
      }

      const prevPage = parseInt(this.props.manifests.currentPage) - 1
      const nextPage = parseInt(this.props.manifests.currentPage) + 1

      let curPage = this.props.manifests.currentPage
      let minPage = Number(curPage) - 2
      let maxPage = Number(curPage) + 2

      const pages = []
      const totalPages = this.props.manifests.pageTotal

      for (let index = 1; index <= totalPages; index++) {
        if (pages.indexOf(index) == -1) {
          pages.push(index)
        }
      }

      return (
        <div className="animated fadeIn">
          <Row>
            <Col xs="12" lg="12">
              <Card>
                <CardHeader className="bg-white">
                  <AvForm onValidSubmit={this.handleSearch} model={defaultValues}>
                    <Row>
                      <Col md="3" >
                        <FormGroup >
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>Trayek</InputGroupText>
                            </InputGroupAddon>
                            <AvInput type="select" name="trayek">
                              <option value="">ALL</option>
                              {this.props.manifests.trayek.map((item) => (
                                <option key={item.id} value={item.kode}>{item.kode}</option>
                              ))}
                            </AvInput>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col md="3" >
                        <FormGroup >
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>Tahun</InputGroupText>
                            </InputGroupAddon>
                            <AvInput type="select" name="year">
                              <option value="">ALL</option>
                              {this.props.manifests.year.map((item, key) => (
                                <option key={key} value={item.tahun}>{item.tahun}</option>
                              ))}
                            </AvInput>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col md="2" >
                        <FormGroup className="form-actions">
                          <Button type="submit" size="md" color="success" className=" btn-brand mr-1 mb-1"><i className="fa fa-search"></i><span>Cari </span></Button>
                        </FormGroup>
                      </Col>
                    </Row>
                  </AvForm>
                </CardHeader>
                <CardBody>
                  <Table responsive bordered>
                    <thead>
                      <tr>
                        <th>Schedule</th>
                        <th>Trayek</th>
                        <th>Voyage</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Tahun</th>
                        <th width="130px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.props.manifests.data.map((item) => (
                        <tr key={item.id}>
                          <td>
                            <strong>
                              <Link to={"/schedule/view/" + item.id}>
                                {item.kode}<br /> 
                                <small> Voyage {item.voyage}</small>
                              </Link>
                            </strong>
                          </td>
                          <td>{item.kode}</td>
                          <td>{item.voyage}</td>
                          <td>
                            <strong>{item.dari_pelabuhan}</strong><br />
                            <span>{moment(item.tanggal_berangkat).format('DD-MM-YYYY')}</span>
                          </td>
                          <td>
                            <strong>{item.ke_pelabuhan}</strong><br />
                            <span>{moment(item.tanggal_tiba).format('DD-MM-YYYY')}</span>
                          </td>
                          <td>{item.tahun}</td>
                          <td>
                            <a href={"/manifest/print/" + item.id}>
                              <Button size="sm" color="primary" className="btn-danger btn-brand mr-1 mb-1"><i className="fa fa-file-pdf-o"></i><span>Manifest</span></Button>
                            </a>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </CardBody>
                <CardFooter className="bg-white">
                  <Pagination>
                    {this.props.manifests.hasPrev ?
                      <Link to={'/manifest/page/' + 1}>
                        <PaginationItem><PaginationLink previous tag="button">First Page</PaginationLink></PaginationItem>
                      </Link>
                      : null}

                    {this.props.manifests.hasPrev ?
                      <Link to={'/manifest/page/' + prevPage}>
                        <PaginationItem><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
                      </Link>
                      : null}

                    {pages.map((item, index) => {
                      let actived = false
                      if (curPage === undefined) {
                        curPage = 1
                      }
                      if (item == curPage) {
                        actived = true
                      }

                      if (item >= minPage && item <= maxPage) {
                        return <Link to={'/manifest/page/' + item} key={index}>
                          <PaginationItem active={actived}><PaginationLink tag="button">{item}</PaginationLink></PaginationItem>
                        </Link>
                      }
                    })}

                    {this.props.manifests.hasNext ?
                      <Link to={'/manifest/page/' + nextPage}>
                        <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
                      </Link>
                      : null}

                    {this.props.manifests.hasNext ?
                      <Link to={'/manifest/page/' + totalPages}>
                        <PaginationItem><PaginationLink next tag="button">Last Page</PaginationLink></PaginationItem>
                      </Link>
                      : null}

                  </Pagination>
                  {this.props.manifests.pageTotal > 0 &&
                    <p>Page: {this.props.manifests.currentPage} of {this.props.manifests.pageTotal}</p>
                  }
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }

  render() {
    if (this.props.hasError) {
      return (
        <div>
          <p>Sorry, There was an error loading the data</p>
        </div>
      )
    }

    if (this.props.isLoading) {
      return (
        <div>
          <p>Loading ..... </p>
        </div>
      )
    }

    return (
      <div>
        {this.renderData()}
      </div>
    )
  }
}

ManifestList.propTypes = {
  isSuccess: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired
}

const mapStateToProps = (state) => {
  return {
    manifests: state.manifest.manifests,
    isSuccess: state.manifest.manifestGetSuccess,
    hasError: state.manifest.manifestGetError,
    isLoading: state.manifest.manifestGetLoading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    manifestFetchAll: (data) => dispatch(manifestFetchAll(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManifestList)