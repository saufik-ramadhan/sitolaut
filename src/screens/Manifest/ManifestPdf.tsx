type Schedule = {
  dari_pelabuhan: String;
  foto_ttd: String;
  id: String;
  ke_pelabuhan: String;
  kode: String;
  kota: String;
  logo_perusahaan: String;
  nama_kapal: String;
  nama_perusahaan: String;
  nama_ttd: String;
  tanggal_berangkat: String;
  tanggal_manifest: String;
  voyage: String;
};
type ManifestPdf = {
  manifest: Array<object>;
  schedule: Schedule;
  total_container: Number;
  total_kg: Number;
  total_muatan: Number;
};
export function ManifestPdf(data) {}
