import React from 'react'
import { Text, View } from '@react-pdf/renderer'

const EmptyRow = (props) => {
  return (
    <View style={props.styles.rowTopBorder}>
      <View style={props.styles.col1}>
        <Text style={props.styles.textcenter}> </Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.text}></Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.text}></Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.text}></Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.text}></Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.colSub5}>
        <Text style={props.styles.textRight}></Text>
      </View>
      <View style={props.styles.colSub5}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.colSub6}>
        <Text style={props.styles.textRight}></Text>
      </View>
      <View style={props.styles.colSub61}>
        <Text style={props.styles.textcenter}></Text>
      </View>
    </View>
  )
}

export default EmptyRow