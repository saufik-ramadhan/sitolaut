import React from 'react'
import { Text, View } from '@react-pdf/renderer'

const ManifestRow = (props) => {
  return (
    <View style={props.styles.rowTopBorder}>
      <View style={props.styles.col1}>
        <Text style={props.styles.textcenter}>{props.data.bill_lading_number}</Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.text}>{props.data.perusahaan_shipper && props.data.perusahaan_shipper.toUpperCase()}</Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.text}>{props.data.perusahaan_consignee && props.data.perusahaan_consignee.toUpperCase()}</Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}>{props.data.containers.length}</Text>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.text}>{props.data.containers[0].no_container}</Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}>UNIT</Text>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.text}>{
          props.data.containers[0].nama_barang_arr.map((val, idx) => {
            if (idx === 0) {
              return (val)
            }else if (idx === 1){
              return (', ' + val)
            }else if (idx === 2){
              return (', dll.')
            }
          })
        }</Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}>{props.data.containers[0].berat_total}</Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}>{props.data.containers[0].berat_total}</Text>
      </View>
      <View style={props.styles.colSub5}>
        <Text style={props.styles.textRight}>{props.data.containers[0].harga && props.data.containers[0].harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
      </View>
      <View style={props.styles.colSub5}>
        <Text style={props.styles.textcenter}>{props.data.type_container_name}</Text>
      </View>
      <View style={props.styles.colSub6}>
        <Text style={props.styles.textRight}>{props.data.total_harga_container && props.data.total_harga_container.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
      </View>
      <View style={props.styles.colSub61}>
        <Text style={props.styles.textcenter}> - </Text>
      </View>
    </View>
  )
}

export default ManifestRow