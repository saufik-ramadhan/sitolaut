import React from 'react'
import { Text, View } from '@react-pdf/renderer'

const ManifestChild = (props) => {
  return (
    <View style={props.styles.rowTopBorder}>
      <View style={props.styles.col1}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.text}></Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.text}></Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.text}>{props.data.no_container}</Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.text}>{
          props.data.nama_barang_arr.map((val, idx) => {
            if (idx === 0) {
              return (val)
            }else if (idx === 1){
              return (', ' + val)
            }else if (idx === 2){
              return (', dll.')
            }
          })
          }</Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}>{props.data.berat_total}</Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}>{props.data.berat_total}</Text>
      </View>
      <View style={props.styles.colSub5}>
        <Text style={props.styles.textRight}>{props.data.harga && props.data.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
      </View>
      <View style={props.styles.colSub5}>
        <Text style={props.styles.textcenter}>{props.container_type}</Text>
      </View>
      <View style={props.styles.colSub6}>
        <Text style={props.styles.textRight}> - </Text>
      </View>
      <View style={props.styles.colSub61}>
        <Text style={props.styles.textcenter}> - </Text>
      </View>
    </View>
  )
}

export default ManifestChild