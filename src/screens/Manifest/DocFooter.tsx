import React from 'react'
import { Text, View } from '@react-pdf/renderer'
import moment from 'moment'
import styled from '@react-pdf/styled-components'
import {IP_URL} from '../../actions/constant';
const Ttd = styled.Image`
width: 200px;
height: auto;
margin-left: 70px;
position: relative;
vertical-align: middle;
align: left;
`
const DocFooter = (props) => {
  return (
    <View style={props.styles.footer}>
      <View style={props.styles.row}>
        {props.last_page ?
          <View style={props.styles.colFooter1}>
            <View style={props.styles.rekap}>
              <Text style={props.styles.text}>REKAPITULASI</Text>
              <View style={props.styles.row}>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.text}>CONTAINER (unit)</Text>
                </View>
                <View style={props.styles.colRekap2}>
                  <Text style={props.styles.text}>:</Text>
                </View>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.textBoldRight}>{props.data.total_container}</Text>
                </View>
              </View>
              <View style={props.styles.row}>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.text}>MOBIL (unit)</Text>
                </View>
                <View style={props.styles.colRekap2}>
                  <Text style={props.styles.text}>:</Text>
                </View>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.textBoldRight}>-</Text>
                </View>
              </View>
              <View style={props.styles.row}>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.text}>JUMLAH GENCAR (koly)</Text>
                </View>
                <View style={props.styles.colRekap2}>
                  <Text style={props.styles.text}>:</Text>
                </View>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.textBoldRight}>-</Text>
                </View>
              </View>
              <View style={props.styles.row}>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.text}>JUMLAH (kgs)</Text>
                </View>
                <View style={props.styles.colRekap2}>
                  <Text style={props.styles.text}>:</Text>
                </View>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.textBoldRight}>{props.data.total_kg && props.data.total_kg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
                </View>
              </View>
              <View style={props.styles.row}>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.text}>JUMLAH TONAGE (T/M3)</Text>
                </View>
                <View style={props.styles.colRekap2}>
                  <Text style={props.styles.text}>:</Text>
                </View>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.textBoldRight}>{props.data.total_kg && props.data.total_kg.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
                </View>
              </View>
              <View style={props.styles.row}>
                <View style={props.styles.colRekap1}>
                  <Text style={props.styles.text}>JUMLAH UT.MUATAN (Rp)</Text>
                </View>
                <View style={props.styles.colRekap2}>
                  <Text style={props.styles.text}>:</Text>
                </View>
                <View style={props.styles.colRekap1}>
                <Text style={props.styles.textBoldRight}>{props.data.total_muatan && props.data.total_muatan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
                </View>
              </View>
            </View>
          </View>
          :
          <View style={props.styles.colFooter2} />
        }

        <View style={props.styles.colFooter2} />

        <View style={props.styles.colFooter3}>
          <View style={props.styles.ttd}>
            <Text style={props.styles.textcenter}>{props.data.schedule.kota}, {moment().format('DD MMMM YYYY')}</Text>
            <Ttd
            
            src={IP_URL+props.data.schedule.foto_ttd}
          />
          </View>
          <Text style={props.styles.namaTtd}>{props.data.schedule.nama_ttd}</Text>
        </View>

      </View>
    </View>
  )
}

export default DocFooter