import React from 'react'
import { Text, View } from '@react-pdf/renderer'
import moment from 'moment'
import styled from '@react-pdf/styled-components'
import {IP_URL} from '../../actions/constant';
const Logo = styled.Image`
width: 10px;
margin-left: 70px;
position: relative;
vertical-align: middle;
align: left;
`

const HeaderPage = (props) => {
  return (
    <View break={props.break}>
      <View style={props.styles.container}>
        <View style={props.styles.logoColumn}>
          <Logo
            style={props.styles.image}
            src={IP_URL+props.data.logo_perusahaan}
          />
        </View>
        <View style={props.styles.textColumn}>
          <Text style={props.styles.title}>{props.data.nama_perusahaan}</Text>

        </View>
        <View style={props.styles.logoColumn}>
          <Text style={props.styles.number}>CABANG  </Text>
          <Text style={props.styles.subtitle}>{props.data.kota}</Text>
        </View>
      </View>
      <View style={props.styles.rowNoBorder}>
        <View style={props.styles.column1}>
          <Text style={props.styles.textBoldCenter}>FREIGHT MANIFEST</Text>
          <Text style={props.styles.textBoldCenter}>NO. MNF{props.data.id}{props.data.voyage}</Text>
        </View>
        <View style={props.styles.column2}>
          <View style={props.styles.rowTopBorder}>
            <View style={props.styles.column2sub1}>
              <Text style={props.styles.textBold}>NAMA KAPAL</Text>
            </View>
            <View style={props.styles.column2sub2}>
              <Text style={props.styles.textBold}>: {props.data.nama_kapal} ({props.data.kode})</Text>
            </View>
            <View style={props.styles.column2sub3}>
              <Text style={props.styles.textBold}>RUTE/VOYAGE : </Text>
              <View>
                <Text style={props.styles.textBold}>{props.data.voyage}</Text>
              </View>
            </View>
            <View style={props.styles.column2sub4}>
              <Text style={props.styles.textBold}>TANGGAL ETD : </Text>
              <Text style={props.styles.textBold}>
                {moment(props.data.tanggal_berangkat).format('DD MMMM YYYY')}
              </Text>
            </View>
          </View>
          <View style={props.styles.rowTopBorder}>
            <View style={props.styles.column2sub1}>
              <Text style={props.styles.textBold}>PELABUHAN MUAT</Text>
            </View>
            <View style={props.styles.column2sub2}>
              <Text style={props.styles.textBold}>: {props.data.dari_pelabuhan && props.data.dari_pelabuhan.toUpperCase()}</Text>
            </View>
            <View style={props.styles.column2sub5}>
              <Text style={props.styles.textBold}>PELABUHAN BONGKAR : {props.data.dari_pelabuhan && props.data.ke_pelabuhan.toUpperCase()}</Text>
            </View>
          </View>
        </View>
        <View style={props.styles.column1}>
          <View style={props.styles.row}>
            <Text style={props.styles.textBoldCenter}>HALAMAN </Text>
            <Text render={({ pageNumber }) => (
              `${pageNumber}`
            )} fixed style={props.styles.textBoldCenter} />
          </View>
        </View>
      </View>

      <View style={props.styles.rowTopBorder}>
        <View style={props.styles.col1}>
          <Text style={props.styles.textunderline}>No. Konosemen</Text>
          <Text style={props.styles.textcenter}>No. of B/L </Text>
        </View>
        <View style={props.styles.col2}>
          <Text style={props.styles.textunderline}>PENGIRIM </Text>
          <Text style={props.styles.textcenter}>SHIPPER </Text>
        </View>
        <View style={props.styles.col2}>
          <Text style={props.styles.textunderline}>PENERIMA </Text>
          <Text style={props.styles.textcenter}>CONSIGNEE </Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>Banyaknya</Text>
          <Text style={props.styles.textunderline}>koli/ekor </Text>
          <Text style={props.styles.textcenter}>Quantity of packages </Text>
          <Text style={props.styles.text}> </Text>
        </View>
        <View style={props.styles.col3}>
          <Text style={props.styles.textunderline}>MEREK & NOMOR </Text>
          <Text style={props.styles.textcenter}>MARK </Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>Jenis</Text>
          <Text style={props.styles.textunderline}>Bungkusan </Text>
          <Text style={props.styles.textcenter}>Kind of packages </Text>
          <Text style={props.styles.text}> </Text>
        </View>
        <View style={props.styles.col3}>
          <Text style={props.styles.textunderline}>ISINYA</Text>
          <Text style={props.styles.textcenter}>CONTENTS</Text>
          <Text style={props.styles.text}> </Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>UKURAN</Text>
          <Text style={props.styles.textunderline}>dalam M3</Text>
          <Text style={props.styles.textcenter}>MEASUREMENT</Text>
          <Text style={props.styles.text}> </Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>BERAT dalam KG</Text>
          <Text style={props.styles.textcenter}>WEIGHT</Text>
          <Text style={props.styles.text}> </Text>
        </View>
        <View style={props.styles.col5}>
          <Text style={props.styles.textcenter}>Tambang Satuan</Text>
          <Text style={props.styles.textH}>Freight per Load</Text>
          <View style={props.styles.rowTopBorder}>
            <View style={props.styles.col7}>
              <Text style={props.styles.textcenter}>Total Prepaid</Text>
            </View>
            <View style={props.styles.col8}>
              <Text style={props.styles.textcenter}>Per</Text>
            </View>
          </View>
        </View>
        <View style={props.styles.col6}>
          <Text style={props.styles.textcenter}>UANG TAMBANG</Text>
          <Text style={props.styles.textH}>FREIGHT</Text>
          <View style={props.styles.rowTopBorder}>
            <View style={props.styles.col7}>
              <Text style={props.styles.textcenter}>Total Prepaid</Text>
            </View>
            <View style={props.styles.col8}>
              <Text style={props.styles.textcenter}>Total payable at destination</Text>
            </View>
          </View>
        </View>
      </View>

      <View style={props.styles.rowTopBorder}>
        <View style={props.styles.col1}>
          <Text style={props.styles.textcenter}>(1)</Text>
        </View>
        <View style={props.styles.col2}>
          <Text style={props.styles.textcenter}>(2) </Text>
        </View>
        <View style={props.styles.col2}>
          <Text style={props.styles.textcenter}>(3) </Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>(4)</Text>
        </View>
        <View style={props.styles.col3}>
          <Text style={props.styles.textcenter}>(5)</Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>(6)</Text>
        </View>
        <View style={props.styles.col3}>
          <Text style={props.styles.textcenter}>(7)</Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>(8)</Text>
        </View>
        <View style={props.styles.col4}>
          <Text style={props.styles.textcenter}>(9)</Text>
        </View>
        <View style={props.styles.colSub5}>
          <Text style={props.styles.textcenter}>(10)</Text>
        </View>
        <View style={props.styles.colSub5}>
          <Text style={props.styles.textcenter}>(11)</Text>
        </View>
        <View style={props.styles.colSub6}>
          <Text style={props.styles.textcenter}>(12)</Text>
        </View>
        <View style={props.styles.colSub61}>
          <Text style={props.styles.textcenter}>(13)</Text>
        </View>
      </View>
    </View>
  )
}

export default HeaderPage