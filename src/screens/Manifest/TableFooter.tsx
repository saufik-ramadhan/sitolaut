import React from 'react'
import { Text, View } from '@react-pdf/renderer'
import styled from '@react-pdf/styled-components'

const Bold = styled.Text`
font-size: 12px;
font-weight 800;
text-align: center;
font-family: Helvetica-Bold;
padding-top: 5px;
`

const TableFooter = (props) => {
  return (
    <View style={props.styles.rowBorderDouble}>
      <View style={props.styles.col1}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.textBold}>JUMLAH</Text>
      </View>
      <View style={props.styles.col2}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col4}>
        <Bold>{props.total_container}</Bold>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col4}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col3}>
        <Text style={props.styles.textcenter}></Text>
      </View>
      <View style={props.styles.col4}>
        <Bold>{props.total_muatan_container && props.total_muatan_container.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Bold>
      </View>
      <View style={props.styles.col4}>
        <Bold>{props.total_muatan_container && props.total_muatan_container.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Bold>
      </View>

      <View style={props.styles.colSub5}>
        <Text style={props.styles.textBoldRight}></Text>
      </View>
      <View style={props.styles.colSub5}>
        <Text style={props.styles.textBoldCenter}></Text>
      </View>
      <View style={props.styles.colSub6}>
        <Text style={props.styles.textBoldRight}>{props.total_harga_container && props.total_harga_container.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</Text>
      </View>
      <View style={props.styles.colSub61}>
        <Text style={props.styles.textcenter}> - </Text>
      </View>
    </View>
  )
}

export default TableFooter