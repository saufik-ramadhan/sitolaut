import React from "react";
import { StyleSheet, Image, Alert } from "react-native";
import { Root, Space, ErrorText, DateTime, FileInput } from "../../components";
import { Button, TextInput, Paragraph } from "react-native-paper";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { operatorUpdate } from "../../redux/operatorReducer";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  nama_perusahaan: Yup.string().required("Required"),
  id: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
  alamat: Yup.string().required("Required"),
  fax: Yup.number().required("Required"),
  telp: Yup.number().required("Required"),
  siup: Yup.number().required("Required"),
  npwp: Yup.number().required("Required"),
  nama_pic: Yup.string().required("Required"),
  email_pic: Yup.string().email("Invalid email").required("Email Empty"),
  hp_pic: Yup.number().required("Required"),
  telp_pic: Yup.number().required("Required"),
  fax_pic: Yup.number().required("Required"),
  nama_ttd: Yup.string().required("Required"),
  no_pmku: Yup.number().required("Required"),
  tgl_pmku: Yup.string().required("Required"),
  nama_bank: Yup.string().required("Required"),
  nomor_rekening: Yup.number().required("Required"),
  atas_nama: Yup.string().required("Required"),
});
/**
 * MAIN SCREEN
 */
export default function UpdateOperatorProfile({ profile, navigation }) {
  const dispatch = useDispatch();
  const loading = useSelector(function (state: RootState) {
    return state.operator.operatorAddLoading;
  });
  return (
    <Root style={styles.container}>
      <Formik
        validationSchema={RegisterSchema}
        initialValues={{
          nama_perusahaan: profile.nama_perusahaan,
          id: profile.id,
          email: profile.email,
          alamat: profile.alamat,
          fax: profile.fax,
          telp: profile.telp,
          siup: profile.siup,
          npwp: profile.npwp,
          nama_pic: profile.nama_pic,
          email_pic: profile.email_pic,
          hp_pic: profile.hp_pic,
          telp_pic: profile.telp_pic,
          fax_pic: profile.fax_pic,
          nama_ttd: profile.nama_ttd,
          no_pmku: profile.no_pmku,
          tgl_pmku: profile.tgl_pmku,
          nama_bank: profile.nama_bank,
          nomor_rekening: profile.nomor_rekening,
          atas_nama: profile.atas_nama,

          siup_doc: "",
          npwp_doc: "",
          logo_perusahaan: "",
          foto_ttd: "",
        }}
        onSubmit={function (values) {
          Alert.alert("Ubah", "Anda yakin ingin memperbarui profil?", [
            {text:"Ya", onPress:() => {
              dispatch(operatorUpdate(values));
              navigation.goBack();
            }},
            {text: "Tdk", onPress:() => null}
          ]);
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Perusahaan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_perusahaan}
              onChangeText={handleChange("nama_perusahaan")}
              onBlur={handleBlur("nama_perusahaan")}
              label="Nama Perusahaan"
            />
            {errors.nama_perusahaan && touched.nama_perusahaan && (
              <ErrorText>{errors.nama_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              label="Alamat"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor Fax
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax}
              onChangeText={handleChange("fax")}
              onBlur={handleBlur("fax")}
              label="No. Fax"
              keyboardType="number-pad"
            />
            {errors.fax && touched.fax && <ErrorText>{errors.fax}</ErrorText>}
            <Space height={10} />

            {/**
             * Nomor Telepon
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp}
              onChangeText={handleChange("telp")}
              onBlur={handleBlur("telp")}
              label="No. Telp"
              keyboardType="number-pad"
            />
            {errors.telp && touched.telp && (
              <ErrorText>{errors.telp}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor SIUP
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.siup}
              onChangeText={handleChange("siup")}
              onBlur={handleBlur("siup")}
              label="No. SIUP"
              keyboardType="number-pad"
            />
            {errors.siup && touched.siup && (
              <ErrorText>{errors.siup}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor NPWP
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.npwp}
              onChangeText={handleChange("npwp")}
              onBlur={handleBlur("npwp")}
              label="No. NPWP"
              keyboardType="number-pad"
            />
            {errors.npwp && touched.npwp && (
              <ErrorText>{errors.npwp}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== PENANDA TANGAN ========== */}
            {/**
             * Nama Penanda Tangan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_ttd}
              onChangeText={handleChange("nama_ttd")}
              onBlur={handleBlur("nama_ttd")}
              label="Nama Penanda Tangan"
            />
            {errors.nama_ttd && touched.nama_ttd && (
              <ErrorText>{errors.nama_ttd}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== PMKU ========== */}
            <Paragraph>Data PMKU : </Paragraph>
            {/**
             * Nomor PMKU
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.no_pmku}
              onChangeText={handleChange("no_pmku")}
              onBlur={handleBlur("no_pmku")}
              label="No. PMKU"
              keyboardType="number-pad"
            />
            {errors.no_pmku && touched.no_pmku && (
              <ErrorText>{errors.no_pmku}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Tanggal PMKU
             */}
            <DateTime
              onChangeValue={function (value: Date) {
                setFieldValue(
                  "tgl_pmku",
                  `${value.getFullYear()}-${value.getMonth()}-${value.getDate()}`
                );
              }}
              label="Tanggal PMKU"
            />
            {errors.tgl_pmku && touched.tgl_pmku && (
              <ErrorText>{errors.tgl_pmku}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nama Bank
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_bank}
              onChangeText={handleChange("nama_bank")}
              onBlur={handleBlur("nama_bank")}
              label="Nama Bank"
            />
            {errors.nama_bank && touched.nama_bank && (
              <ErrorText>{errors.nama_bank}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor Rekening
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nomor_rekening}
              onChangeText={handleChange("nomor_rekening")}
              onBlur={handleBlur("nomor_rekening")}
              label="No. Rekening"
              keyboardType="number-pad"
            />
            {errors.nomor_rekening && touched.nomor_rekening && (
              <ErrorText>{errors.nomor_rekening}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Atas Nama
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.atas_nama}
              onChangeText={handleChange("atas_nama")}
              onBlur={handleBlur("atas_nama")}
              label="Atas Nama"
            />
            {errors.atas_nama && touched.atas_nama && (
              <ErrorText>{errors.atas_nama}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== PIC ========== */}
            <Paragraph>Data PIC : </Paragraph>
            {/**
             * Nama PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_pic}
              onChangeText={handleChange("nama_pic")}
              onBlur={handleBlur("nama_pic")}
              label="Nama PIC"
            />
            {errors.nama_pic && touched.nama_pic && (
              <ErrorText>{errors.nama_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Email PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.email_pic}
              onChangeText={handleChange("email_pic")}
              onBlur={handleBlur("email_pic")}
              label="Email PIC"
              keyboardType="email-address"
            />
            {errors.email_pic && touched.email_pic && (
              <ErrorText>{errors.email_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * HP PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.hp_pic}
              onChangeText={handleChange("hp_pic")}
              onBlur={handleBlur("hp_pic")}
              label="No. HP PIC"
              keyboardType="number-pad"
            />
            {errors.hp_pic && touched.hp_pic && (
              <ErrorText>{errors.hp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Telp PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_pic}
              onChangeText={handleChange("telp_pic")}
              onBlur={handleBlur("telp_pic")}
              label="No. Telp. PIC"
              keyboardType="number-pad"
            />
            {errors.telp_pic && touched.telp_pic && (
              <ErrorText>{errors.telp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Fax PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax_pic}
              onChangeText={handleChange("fax_pic")}
              onBlur={handleBlur("fax_pic")}
              label="No. Fax. PIC"
              keyboardType="numeric"
            />
            {errors.fax_pic && touched.fax_pic && (
              <ErrorText>{errors.fax_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen SIUP
             */}
            {values.siup_doc ? (
              <>
                <Image
                  source={{ uri: values.siup_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Dokumen SIUP"
              placeholder={
                values.siup_doc ? values.siup_doc.name : "Upload File siup"
              }
              error={errors.siup_doc_size}
              getValue={function (value) {
                setFieldValue("siup_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("siup_doc_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Dokumen NPWP
             */}
            {values.npwp_doc ? (
              <>
                <Image
                  source={{ uri: values.npwp_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Dokumen NPWP"
              placeholder={
                values.npwp_doc ? values.npwp_doc.name : "Upload File npwp"
              }
              error={errors.npwp_doc_size}
              getValue={function (value) {
                setFieldValue("npwp_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("npwp_doc_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Logo Perusahaan
             */}
            {values.logo_perusahaan ? (
              <>
                <Image
                  source={{ uri: values.logo_perusahaan.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Logo Perusahaan"
              placeholder={
                values.logo_perusahaan ? values.logo_perusahaan.name : null
              }
              error={errors.logo_perusahaan_size}
              getValue={function (value) {
                setFieldValue("logo_perusahaan", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("logo_perusahaan_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Foto Tanda Tangan
             */}
            {values.foto_ttd ? (
              <>
                <Image
                  source={{ uri: values.foto_ttd.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="File Tanda Tangan"
              placeholder={values.foto_ttd ? values.foto_ttd.name : "..."}
              error={errors.foto_ttd_size}
              getValue={function (value) {
                setFieldValue("foto_ttd", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("foto_ttd_size", value.fileSize);
              }}
            />
            <Space height={5} />

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.gray1 }}
              onPress={handleSubmit}
              loading={loading}
            >
              Simpan
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    padding: 30,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    color: "black",
    backgroundColor: Colors.grayL,
  },
  textInput: {
    paddingHorizontal: 10,
  },
  pickerContainer: {
    backgroundColor: Colors.grayL,
    paddingVertical: 5,
  },
});
