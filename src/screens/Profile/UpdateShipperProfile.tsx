import React from "react";
import { StyleSheet, View, Image, Alert } from "react-native";
import { Root, Space, ErrorText, FileInput } from "../../components";
import { Button, TextInput, Caption } from "react-native-paper";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { shipperUpdate } from "../../redux/shipperReducer";
import { Picker } from "@react-native-community/picker";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  nama_perusahaan: Yup.string().required("Required"),
  id: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
  alamat: Yup.string().required("Required"),
  fax: Yup.number().required("Required"),
  telp: Yup.number().required("Required"),
  siup: Yup.number().required("Required"),
  npwp: Yup.number().required("Required"),
  nama_pic: Yup.string().required("Required"),
  email_pic: Yup.string().email("Invalid email").required("Email Empty"),
  hp_pic: Yup.number().required("Required"),
  telp_pic: Yup.number().required("Required"),
  fax_pic: Yup.number().required("Required"),
  nama_ttd: Yup.string().required("Required"),
  nama_officer1: Yup.string().required("Required"),
  telp_officer1: Yup.number().required("Required"),
  nama_officer2: Yup.string().required("Required"),
  telp_officer2: Yup.number().required("Required"),
  nama_officer3: Yup.string().required("Required"),
  telp_officer3: Yup.number().required("Required"),
  status_perusahaan: Yup.string().required("Required"),
  jenis_user: Yup.string().required("Required"),
  identify_officer1: Yup.string().required("Required"),
  identify_officer2: Yup.string().required("Required"),
  identify_officer3: Yup.string().required("Required"),
});
/**
 * MAIN SCREEN
 */
export default function UpdateShipperProfile({ profile, navigation }) {
  const dispatch = useDispatch();
  const loading = useSelector(function (state: RootState) {
    return state.shipper.shipperAddLoading;
  });
  return (
    <Root style={styles.container}>
      <Formik
        validationSchema={RegisterSchema}
        initialValues={{
          nama_perusahaan: profile.nama_perusahaan,
          id: profile.id,
          email: profile.email,
          alamat: profile.alamat,
          fax: profile.fax,
          telp: profile.telp,
          siup: profile.siup,
          npwp: profile.npwp,
          nama_pic: profile.nama_pic,
          email_pic: profile.email_pic,
          hp_pic: profile.hp_pic,
          telp_pic: profile.telp_pic,
          fax_pic: profile.fax_pic,
          nama_ttd: profile.nama_ttd,
          nama_officer1: profile.nama_officer1,
          telp_officer1: profile.telp_officer1,
          nama_officer2: profile.nama_officer2,
          telp_officer2: profile.telp_officer2,
          nama_officer3: profile.nama_officer3,
          telp_officer3: profile.telp_officer3,
          status_perusahaan: profile.status_perusahaan,
          jenis_user: profile.jenis_user,
          identify_officer1: profile.identify_officer1,
          identify_officer2: profile.identify_officer2,
          identify_officer3: profile.identify_officer3,

          siup_doc: "",
          npwp_doc: "",
          surat_rekomendasi: "",
          logo_perusahaan: "",
          foto_ttd: "",
          identify_officerdoc1: "",
          identify_officerdoc2: "",
          identify_officerdoc3: "",
        }}
        onSubmit={function (values) {
          Alert.alert("Ubah", "Anda yakin ingin memperbarui profil?", [
            {text:"Ya", onPress:() => {
              dispatch(shipperUpdate(values));
              navigation.goBack();
            }},
            {text: "Tdk", onPress:() => null}
          ]);
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Perusahaan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_perusahaan}
              onChangeText={handleChange("nama_perusahaan")}
              onBlur={handleBlur("nama_perusahaan")}
              label="Nama Perusahaan"
            />
            {errors.nama_perusahaan && touched.nama_perusahaan && (
              <ErrorText>{errors.nama_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              label="Alamat"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor Fax
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax}
              onChangeText={handleChange("fax")}
              onBlur={handleBlur("fax")}
              label="No. Fax"
              keyboardType="number-pad"
            />
            {errors.fax && touched.fax && <ErrorText>{errors.fax}</ErrorText>}
            <Space height={10} />

            {/**
             * Nomor Telepon
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp}
              onChangeText={handleChange("telp")}
              onBlur={handleBlur("telp")}
              label="No. Telp"
              keyboardType="number-pad"
            />
            {errors.telp && touched.telp && (
              <ErrorText>{errors.telp}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor SIUP
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.siup}
              onChangeText={handleChange("siup")}
              onBlur={handleBlur("siup")}
              label="No. SIUP"
              keyboardType="number-pad"
            />
            {errors.siup && touched.siup && (
              <ErrorText>{errors.siup}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor NPWP
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.npwp}
              onChangeText={handleChange("npwp")}
              onBlur={handleBlur("npwp")}
              label="No. NPWP"
              keyboardType="number-pad"
            />
            {errors.npwp && touched.npwp && (
              <ErrorText>{errors.npwp}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== PENANDA TANGAN ========== */}
            {/**
             * Nama Penanda Tangan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_ttd}
              onChangeText={handleChange("nama_ttd")}
              onBlur={handleBlur("nama_ttd")}
              label="Nama Penanda Tangan"
            />
            {errors.nama_ttd && touched.nama_ttd && (
              <ErrorText>{errors.nama_ttd}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Jenis Perusahaan
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.jenis_user}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("jenis_user", itemValue);
                }}
              >
                <Picker.Item label="Jenis Perusahaan" value={0} />
                <Picker.Item label="BUMN" value="BUMN" />
                <Picker.Item label="Swasta" value="Swasta" />
              </Picker>
            </View>
            {errors.jenis_user && touched.jenis_user && (
              <ErrorText>{errors.jenis_user}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Status Perusahaan
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.status_perusahaan}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("status_perusahaan", itemValue);
                }}
              >
                <Picker.Item label="Status Perusahaan" value={0} />
                <Picker.Item label="Pusat" value="Pusat" />
                <Picker.Item label="Cabang" value="Cabang" />
              </Picker>
            </View>
            {errors.status_perusahaan && touched.status_perusahaan && (
              <ErrorText>{errors.status_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== OFFICER 1 ========== */}
            <Caption style={styles.section}>Officer 1 : </Caption>
            {/**
             * Nama Officer 1
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_officer1}
              onChangeText={handleChange("nama_officer1")}
              onBlur={handleBlur("nama_officer1")}
              label="Nama Officer 1"
            />
            {errors.nama_officer1 && touched.nama_officer1 && (
              <ErrorText>{errors.nama_officer1}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * No. Telp Officer 1
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_officer1}
              onChangeText={handleChange("telp_officer1")}
              onBlur={handleBlur("telp_officer1")}
              label="No. Telp Officer 1"
              keyboardType="number-pad"
            />
            {errors.telp_officer1 && touched.telp_officer1 && (
              <ErrorText>{errors.telp_officer1}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Kartu Identitas Officer 1
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.identify_officer1}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("identify_officer1", itemValue);
                }}
              >
                <Picker.Item label="Pilih Identitas" value={0} />
                <Picker.Item label="KTP" value="KTP" />
                <Picker.Item label="SIM" value="SIM" />
                <Picker.Item
                  label="Kartu Identitas Lain"
                  value="Kartu Identitas Lain"
                />
              </Picker>
            </View>
            {errors.identify_officer1 && touched.identify_officer1 && (
              <ErrorText>{errors.identify_officer1}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== OFFICER 2 ========== */}
            <Caption style={styles.section}>Officer 2 : </Caption>
            {/**
             * Nama Officer 2
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_officer2}
              onChangeText={handleChange("nama_officer2")}
              onBlur={handleBlur("nama_officer2")}
              label="Nama Officer 2"
            />
            {errors.nama_officer2 && touched.nama_officer2 && (
              <ErrorText>{errors.nama_officer2}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * No. Telp Officer 2
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_officer2}
              onChangeText={handleChange("telp_officer2")}
              onBlur={handleBlur("telp_officer2")}
              label="No. Telp Officer 2"
              keyboardType="number-pad"
            />
            {errors.telp_officer2 && touched.telp_officer2 && (
              <ErrorText>{errors.telp_officer2}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Kartu Identitas Officer 2
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.identify_officer2}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("identify_officer2", itemValue);
                }}
              >
                <Picker.Item label="Pilih Identitas" value={0} />
                <Picker.Item label="KTP" value="KTP" />
                <Picker.Item label="SIM" value="SIM" />
                <Picker.Item
                  label="Kartu Identitas Lain"
                  value="Kartu Identitas Lain"
                />
              </Picker>
            </View>
            {errors.identify_officer2 && touched.identify_officer2 && (
              <ErrorText>{errors.identify_officer2}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== OFFICER 3 ========== */}
            <Caption style={styles.section}>Officer 3 : </Caption>
            {/**
             * Nama Officer 3
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_officer3}
              onChangeText={handleChange("nama_officer3")}
              onBlur={handleBlur("nama_officer3")}
              label="Nama Officer 3"
            />
            {errors.nama_officer3 && touched.nama_officer3 && (
              <ErrorText>{errors.nama_officer3}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * No. Telp Officer 3
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_officer3}
              onChangeText={handleChange("telp_officer3")}
              onBlur={handleBlur("telp_officer3")}
              label="No. Telp Officer 3"
              keyboardType="number-pad"
            />
            {errors.telp_officer3 && touched.telp_officer3 && (
              <ErrorText>{errors.telp_officer3}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Kartu Identitas Officer 3
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.identify_officer3}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("identify_officer3", itemValue);
                }}
              >
                <Picker.Item label="Pilih Identitas" value={0} />
                <Picker.Item label="KTP" value="KTP" />
                <Picker.Item label="SIM" value="SIM" />
                <Picker.Item
                  label="Kartu Identitas Lain"
                  value="Kartu Identitas Lain"
                />
              </Picker>
            </View>
            {errors.identify_officer3 && touched.identify_officer3 && (
              <ErrorText>{errors.identify_officer3}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== PIC ========== */}
            <Caption style={styles.section}>Data PIC : </Caption>
            {/**
             * Nama PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_pic}
              onChangeText={handleChange("nama_pic")}
              onBlur={handleBlur("nama_pic")}
              label="Nama PIC"
            />
            {errors.nama_pic && touched.nama_pic && (
              <ErrorText>{errors.nama_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Email PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.email_pic}
              onChangeText={handleChange("email_pic")}
              onBlur={handleBlur("email_pic")}
              label="Email PIC"
              keyboardType="email-address"
            />
            {errors.email_pic && touched.email_pic && (
              <ErrorText>{errors.email_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * HP PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.hp_pic}
              onChangeText={handleChange("hp_pic")}
              onBlur={handleBlur("hp_pic")}
              label="No. HP PIC"
              keyboardType="number-pad"
            />
            {errors.hp_pic && touched.hp_pic && (
              <ErrorText>{errors.hp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Telp PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_pic}
              onChangeText={handleChange("telp_pic")}
              onBlur={handleBlur("telp_pic")}
              label="No. Telp. PIC"
              keyboardType="number-pad"
            />
            {errors.telp_pic && touched.telp_pic && (
              <ErrorText>{errors.telp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Fax PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax_pic}
              onChangeText={handleChange("fax_pic")}
              onBlur={handleBlur("fax_pic")}
              label="No. Fax. PIC"
              keyboardType="numeric"
            />
            {errors.fax_pic && touched.fax_pic && (
              <ErrorText>{errors.fax_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen SIUP
             */}
            {values.siup_doc ? (
              <>
                <Image
                  source={{ uri: values.siup_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Dokumen SIUP"
              placeholder={
                values.siup_doc ? values.siup_doc.name : "Upload File siup"
              }
              error={errors.siup_doc_size}
              getValue={function (value) {
                setFieldValue("siup_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("siup_doc_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Dokumen NPWP
             */}
            {values.npwp_doc ? (
              <>
                <Image
                  source={{ uri: values.npwp_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Dokumen NPWP"
              placeholder={
                values.npwp_doc ? values.npwp_doc.name : "Upload File npwp"
              }
              error={errors.npwp_doc_size}
              getValue={function (value) {
                setFieldValue("npwp_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("npwp_doc_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Foto Tanda Tangan
             */}
            {values.foto_ttd ? (
              <>
                <Image
                  source={{ uri: values.foto_ttd.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="File Tanda Tangan"
              placeholder={values.foto_ttd ? values.foto_ttd.name : "..."}
              error={errors.foto_ttd_size}
              getValue={function (value) {
                setFieldValue("foto_ttd", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("foto_ttd_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Surat Rekomendasi
             */}
            {values.surat_rekomendasi ? (
              <>
                <Image
                  source={{ uri: values.surat_rekomendasi.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Surat Rekomendasi"
              placeholder={
                values.surat_rekomendasi ? values.surat_rekomendasi.name : null
              }
              error={errors.surat_rekomendasi_size}
              getValue={function (value) {
                setFieldValue("surat_rekomendasi", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("surat_rekomendasi_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Logo Perusahaan
             */}
            {values.logo_perusahaan ? (
              <>
                <Image
                  source={{ uri: values.logo_perusahaan.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Logo Perusahaan"
              placeholder={
                values.logo_perusahaan ? values.logo_perusahaan.name : null
              }
              error={errors.logo_perusahaan_size}
              getValue={function (value) {
                setFieldValue("logo_perusahaan", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("logo_perusahaan_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Upload Kartu Identitas Officer 1
             */}
            {values.identify_officerdoc1 ? (
              <>
                <Image
                  source={{ uri: values.identify_officerdoc1.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Upload Identitas"
              placeholder={
                values.identify_officerdoc1
                  ? values.identify_officerdoc1.name
                  : null
              }
              error={errors.identify_officerdoc1_size}
              getValue={function (value) {
                setFieldValue("identify_officerdoc1", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("identify_officerdoc1_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Upload Kartu Identitas Officer 2
             */}
            {values.identify_officerdoc2 ? (
              <>
                <Image
                  source={{ uri: values.identify_officerdoc2.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Upload Identitas"
              placeholder={
                values.identify_officerdoc2
                  ? values.identify_officerdoc2.name
                  : null
              }
              error={errors.identify_officerdoc2_size}
              getValue={function (value) {
                setFieldValue("identify_officerdoc2", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("identify_officerdoc2_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Upload Kartu Identitas Officer 3
             */}
            {values.identify_officerdoc3 ? (
              <>
                <Image
                  source={{ uri: values.identify_officerdoc3.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Upload Identitas"
              placeholder={
                values.identify_officerdoc3
                  ? values.identify_officerdoc3.name
                  : null
              }
              error={errors.identify_officerdoc3_size}
              getValue={function (value) {
                setFieldValue("identify_officerdoc3", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("identify_officerdoc3_size", value.fileSize);
              }}
            />
            <Space height={5} />

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.gray1 }}
              onPress={handleSubmit}
              loading={loading}
            >
              Simpan
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    padding: 30,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    color: "black",
    backgroundColor: Colors.grayL,
  },
  textInput: {
    paddingHorizontal: 10,
  },
  pickerContainer: {
    backgroundColor: Colors.grayL,
    paddingVertical: 5,
  },
});
