import React from "react";
import { StyleSheet, Image, Alert } from "react-native";
import { Root, Space, ErrorText, FileInput, TextInput } from "../../components";
import { Button } from "react-native-paper";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { consigneeUpdate } from "../../redux/consigneeReducer";
import { UPLOAD_URL } from "../../config/constants";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  nama_perusahaan: Yup.string().required("tidak boleh kosong").label('Nama perusahaan'),
  id: Yup.string().required("tidak boleh kosong").label('Id'),
  email: Yup.string().email("Invalid email").required("tidak boleh kosong").label('Email'),
  alamat: Yup.string().required("tidak boleh kosong").label('Alamat'),
  fax: Yup.number().required("tidak boleh kosong").label('Fax'),
  telp: Yup.number().required("tidak boleh kosong").label('Telp'),
  siup: Yup.number().required("tidak boleh kosong").label('Siup'),
  npwp: Yup.string().required("tidak boleh kosong").label('Npwp'),
  nama_pic: Yup.string().required("tidak boleh kosong").label('Nama pic'),
  email_pic: Yup.string().email("Invalid email").required("tidak boleh kosong").label('Email pic'),
  hp_pic: Yup.number().required("tidak boleh kosong").label('Hp pic'),
  telp_pic: Yup.number().required("tidak boleh kosong").label('Telp pic'),
  fax_pic: Yup.number().required("tidak boleh kosong").label('Fax pic'),
});
/**
 * MAIN SCREEN
 */
export default function UpdateConsigneeProfile({ profile, navigation }) {
  const dispatch = useDispatch();
  const loading = useSelector(function (state: RootState) {
    return state.consignee.consigneeAddLoading;
  });
  return (
    <Root style={styles.container}>
      <Formik
        validationSchema={RegisterSchema}
        initialValues={{
          nama_perusahaan: profile.nama_perusahaan,
          id: profile.id,
          email: profile.email,
          alamat: profile.alamat,
          fax: profile.fax,
          telp: profile.telp,
          siup: profile.siup,
          npwp: profile.npwp,
          nama_pic: profile.nama_pic,
          email_pic: profile.email_pic,
          hp_pic: profile.hp_pic,
          telp_pic: profile.telp_pic,
          fax_pic: profile.fax_pic,

          siup_doc: UPLOAD_URL + profile.siup_doc,
          npwp_doc: UPLOAD_URL + profile.npwp_doc,
          pakta_integritas: UPLOAD_URL + profile.pakta_integritas,
          form_penjualan: UPLOAD_URL + profile.form_penjualan,
        }}
        onSubmit={function (values) {
          Alert.alert("Ubah", "Anda yakin ingin memperbarui profil?", [
            {text:"Ya", onPress:() => {
              dispatch(consigneeUpdate(values));
              navigation.goBack();
            }},
            {text: "Tdk", onPress:() => null}
          ]);
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
          isValid,
        }) => (
          <>
            {/**
             * Nama Perusahaan
             */}
            <TextInput
              value={values.nama_perusahaan}
              onChangeText={handleChange("nama_perusahaan")}
              onBlur={handleBlur("nama_perusahaan")}
              label="Nama Perusahaan"
            />
            {errors.nama_perusahaan && touched.nama_perusahaan && (
              <ErrorText>{errors.nama_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat
             */}
            <TextInput
              value={values.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              label="Alamat"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor Fax
             */}
            <TextInput
              value={values.fax}
              onChangeText={handleChange("fax")}
              onBlur={handleBlur("fax")}
              label="No. Fax"
              keyboardType="number-pad"
            />
            {errors.fax && touched.fax && <ErrorText>{errors.fax}</ErrorText>}
            <Space height={10} />

            {/**
             * Nomor Telepon
             */}
            <TextInput
              value={values.telp}
              onChangeText={handleChange("telp")}
              onBlur={handleBlur("telp")}
              label="No. Telp"
              keyboardType="number-pad"
            />
            {errors.telp && touched.telp && (
              <ErrorText>{errors.telp}</ErrorText>
            )}
            <Space height={40} />

            {/**
             * Nomor SIUP
             */}
            <TextInput
              value={values.siup}
              onChangeText={handleChange("siup")}
              onBlur={handleBlur("siup")}
              label="No. SIUP"
              keyboardType="number-pad"
            />
            {errors.siup && touched.siup && (
              <ErrorText>{errors.siup}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor NPWP
             */}
            <TextInput
              value={values.npwp}
              onChangeText={handleChange("npwp")}
              onBlur={handleBlur("npwp")}
              label="No. NPWP"
              keyboardType="number-pad"
            />
            {errors.npwp && touched.npwp && (
              <ErrorText>{errors.npwp}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== PIC ========== */}
            {/**
             * Nama PIC
             */}
            <TextInput
              value={values.nama_pic}
              onChangeText={handleChange("nama_pic")}
              onBlur={handleBlur("nama_pic")}
              label="Nama PIC"
            />
            {errors.nama_pic && touched.nama_pic && (
              <ErrorText>{errors.nama_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Email PIC
             */}
            <TextInput
              value={values.email_pic}
              onChangeText={handleChange("email_pic")}
              onBlur={handleBlur("email_pic")}
              label="Email PIC"
              keyboardType="email-address"
            />
            {errors.email_pic && touched.email_pic && (
              <ErrorText>{errors.email_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * HP PIC
             */}
            <TextInput
              value={values.hp_pic}
              onChangeText={handleChange("hp_pic")}
              onBlur={handleBlur("hp_pic")}
              label="No. HP PIC"
              keyboardType="number-pad"
            />
            {errors.hp_pic && touched.hp_pic && (
              <ErrorText>{errors.hp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Telp PIC
             */}
            <TextInput
              value={values.telp_pic}
              onChangeText={handleChange("telp_pic")}
              onBlur={handleBlur("telp_pic")}
              label="No. Telp. PIC"
              keyboardType="number-pad"
            />
            {errors.telp_pic && touched.telp_pic && (
              <ErrorText>{errors.telp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Fax PIC
             */}
            <TextInput
              value={values.fax_pic}
              onChangeText={handleChange("fax_pic")}
              onBlur={handleBlur("fax_pic")}
              label="No. Fax. PIC"
              keyboardType="numeric"
            />
            {errors.fax_pic && touched.fax_pic && (
              <ErrorText>{errors.fax_pic}</ErrorText>
            )}
            <Space height={30} />

            {/**
             * Dokumen SIUP
             */}
            {values.siup_doc ? (
              <>
                <Image
                  source={{ uri: values.siup_doc.uri || values.siup_doc }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Dokumen SIUP"
              placeholder={
                values.siup_doc ? values.siup_doc.name : "Upload File siup"
              }
              error={errors.siup_doc_size}
              getValue={function (value) {
                setFieldValue("siup_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("siup_doc_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Dokumen NPWP
             */}
            {values.npwp_doc ? (
              <>
                <Image
                  source={{ uri: values.npwp_doc.uri || values.npwp_doc }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Dokumen NPWP"
              placeholder={
                values.npwp_doc ? values.npwp_doc.name : "Upload File npwp"
              }
              error={errors.npwp_doc_size}
              getValue={function (value) {
                setFieldValue("npwp_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("npwp_doc_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Pakta Integritas
             */}
            {values.pakta_integritas ? (
              <>
                <Image
                  source={{ uri: values.pakta_integritas.uri || values.pakta_integritas }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Pakta Integritas"
              placeholder={
                values.pakta_integritas
                  ? values.pakta_integritas.name
                  : "Upload File Pakta Integritas"
              }
              error={errors.pakta_integritas_size}
              getValue={function (value) {
                setFieldValue("pakta_integritas", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("pakta_integritas_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Form Penjualan
             */}
            {values.form_penjualan ? (
              <>
                <Image
                  source={{ uri: values.form_penjualan.uri || values.form_penjualan }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Form Penjualan"
              placeholder={
                values.form_penjualan
                  ? values.form_penjualan.name
                  : "Upload File npwp"
              }
              error={errors.form_penjualan_size}
              getValue={function (value) {
                setFieldValue("form_penjualan", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("form_penjualan_size", value.fileSize);
              }}
            />
            <Space height={40} />

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.gray1 }}
              onPress={function() {isValid ? handleSubmit() : alert("Harap isi semua field dengan benar")}}
              loading={loading}
            >
              Update
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    color: "black",
  },
  textInput: {
    paddingHorizontal: 10,
  },
  pickerContainer: {
    backgroundColor: Colors.grayL,
    paddingVertical: 5,
  },
});
