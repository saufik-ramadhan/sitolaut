import React, { useEffect } from "react";
import { Root } from "../../components";
import Colors from "./../../config/Colors";
import { StyleSheet, RefreshControl } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "src/redux/rootReducer";
import { consigneeFetchUser } from "../../redux/consigneeReducer";
import { supplierFetchUser } from "../../redux/supplierReducer";
import { shipperFetchUser } from "../../redux/shipperReducer";
import { resellerFetchUser } from "./../../redux/resellerReducer";
import { operatorFetchUser } from "./../../redux/operatorReducer";
import UpdateConsigneeProfile from "./UpdateConsigneeProfile";
import UpdateSupplierProfile from "./UpdateSupplierProfile";
import UpdateShipperProfile from "./UpdateShipperProfile";
import UpdateResellerProfile from "./UpdateResellerProfile";
import UpdateOperatorProfile from "./UpdateOperatorProfile";
import UpdateRegulatorProfile from "./UpdateRegulatorProfile";
import { userFetchUser } from "../../redux/userReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { kotaFetchByProvinsi } from "../../redux/kotaReducer";

export default function UpdateProfile({navigation, route}) {
  const profile = route.params.item
  const [refreshing, setRefreshing] = React.useState(false);
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  // const loading = useSelector(function (state: RootState) {
  //   switch (usertype) {
  //     case "Consignee":
  //       return state.consignee.consigneeViewLoading;
  //     case "Supplier":
  //       return state.supplier.supplierViewLoading;
  //     case "Shipper":
  //       return state.shipper.shipperViewLoading;
  //     case "Reseller":
  //       return state.reseller.resellerViewLoading;
  //     case "Vessel Operator":
  //       return state.operator.operatorViewLoading;
  //     case "Regulator":
  //       return state.user.userViewLoading;
  //     default:
  //       return false;
  //   }
  // });
  // const onRefresh = React.useCallback(() => {
  //   setRefreshing(true);
  //   switch (usertype) {
  //     case "Consignee":
  //       dispatch(consigneeFetchUser(id));
  //       break;
  //     case "Supplier":
  //       dispatch(supplierFetchUser(id));
  //       break;
  //     case "Shipper":
  //       dispatch(shipperFetchUser(id));
  //       break;
  //     case "Reseller":
  //       dispatch(resellerFetchUser(id));
  //       break;
  //     case "Vessel Operator":
  //       dispatch(operatorFetchUser(id));
  //       break;
  //     case "Regulator":
  //       dispatch(userFetchUser(id));
  //       break;
  //     default:
  //       return false;
  //   }
  //   setRefreshing(false);
  // }, [refreshing]);
  // const profile = useSelector(function (state: RootState) {
  //   switch (usertype) {
  //     case "Consignee":
  //       return state.consignee.consigneeViewSuccess
  //         ? state.consignee.consigneeViewSuccess.data[0]
  //         : false;
  //     case "Supplier":
  //       return state.supplier.supplierViewSuccess
  //         ? state.supplier.supplierViewSuccess.data[0]
  //         : false;
  //     case "Shipper":
  //       return state.shipper.shipperViewSuccess
  //         ? state.shipper.shipperViewSuccess.data[0]
  //         : false;
  //     case "Reseller":
  //       return state.reseller.resellerViewSuccess
  //         ? state.reseller.resellerViewSuccess.data[0]
  //         : false;
  //     case "Vessel Operator":
  //       return state.operator.operatorViewSuccess
  //         ? state.operator.operatorViewSuccess.data[0]
  //         : false;
  //     case "Regulator":
  //       return state.user.userViewSuccess
  //         ? state.user.userViewSuccess.data[0]
  //         : false;
  //     default:
  //       return false;
  //   }
  // });
  useEffect(() => {
    dispatch(provinsiFetchAll());
    dispatch(kotaFetchByProvinsi(profile.provinsi_id));
  }, [])
  return (
    <Root
      style={styles.container}
    >
      {usertype == "Consignee" ? (
        <UpdateConsigneeProfile profile={profile} navigation={navigation}/>
      ) : usertype == "Supplier" ? (
        <UpdateSupplierProfile profile={profile} navigation={navigation}/>
      ) : usertype == "Shipper" ? (
        <UpdateShipperProfile profile={profile} navigation={navigation}/>
      ) : usertype == "Reseller" ? (
        <UpdateResellerProfile profile={profile} navigation={navigation}/>
      ) : usertype == "Vessel Operator" ? (
        <UpdateOperatorProfile profile={profile} navigation={navigation}/>
      ) : <UpdateRegulatorProfile profile={profile}navigation={navigation}/>}
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
  },
  profileContainer: {
    alignItems: "center",
    paddingTop: 20,
    borderBottomColor: Colors.gray1,
    borderBottomWidth: 1,
  },
  profileImage: { width: 50, height: 50, borderRadius: 5 },
});
