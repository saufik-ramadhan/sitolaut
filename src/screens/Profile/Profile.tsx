import React, { useEffect } from 'react'
import { Alert, StyleSheet, Text, View } from 'react-native'
import { Caption, IconButton, List, Button } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux'
import { Root } from '../../components';
import Colors from '../../config/Colors';
import { UPLOAD_URL } from '../../config/constants';
import { doLogout } from '../../redux/authReducers';
import { consigneeFetchOne } from '../../redux/consigneeReducer';
import { operatorFetchOne } from '../../redux/operatorReducer';
import { resellerFetchOne } from '../../redux/resellerReducer';
import { RootState } from '../../redux/rootReducer';
import { shipperFetchOne } from '../../redux/shipperReducer';
import { supplierFetchOne } from '../../redux/supplierReducer';
import { userFetchOne } from '../../redux/userReducer';
import { DateFormat, FormatTanggal } from '../../services/utils';

export default function Profile({navigation}) {
  let data;
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const {consignee, supplier, shipper, reseller, operator, user} = useSelector((state: RootState) => state);
  data = () => {
    switch(usertype) {
      case "Consignee": return consignee.consigneeViewSuccess;
      case "Supplier": return supplier.supplierViewSuccess;
      case "Shipper": return shipper.shipperViewSuccess;
      case "Reseller": return reseller.resellerViewSuccess;
      case "Vessel Operator": return operator.operatorViewSuccess;
      case "Regulator": return user.userViewSuccess;
      default: return user.userViewSuccess;
    }
  }
  useEffect(() => {
    switch(usertype) {
      case "Consignee": dispatch(consigneeFetchOne(id)); break;
      case "Supplier": dispatch(supplierFetchOne(id)); break;
      case "Shipper": dispatch(shipperFetchOne(id)); break;
      case "Reseller": dispatch(resellerFetchOne(id)); break;
      case "Vessel Operator": dispatch(operatorFetchOne(id)); break;
      case "Regulator": dispatch(userFetchOne(id)); break;
      default: dispatch(userFetchOne(id));
    }
  }, [
    consignee.consigneeAddLoading,
    supplier.supplierAddLoading,
    shipper.shipperAddLoading,
    reseller.resellerAddLoading,
    operator.operatorAddLoading,
    user.userAddLoading
  ]);
  if(!data().data) return null;

  const UpdateProfile = () => (
    <Button
      mode="contained"
      onPress={() => navigation.navigate("UpdateProfile", {
        item: usertype == 'Regulator' ? data().data : data().data[0]
      })}
      color={Colors.pri}
      icon="account-edit"
    >
      Perbarui Profil
    </Button>
  )

  const ResetPassword = () => (
    <Button
      mode="contained"
      onPress={function () {
        navigation.navigate("ResetPassword", {
          item: usertype == 'Regulator' ? data().data : data().data[0]
        });
      }}
      color={Colors.grayL}
      icon="lock-reset"
    >
      Reset Password
    </Button>
  )

  const RegulatorInfo = () => (
    <List.Section title="Data Regulator" style={{backgroundColor: Colors.whitesmoke}}>
      <List.Item title={data().data.telp_regulator} description={"No. Telp"} />
      <List.Item title={data().data.tmpt_lahir + ", " + FormatTanggal(data().data.tgl_lahir)} description={"Tempat dan Tanggal Lahir"} />
      <List.Item title={data().data.alamat_regulator} description={"Alamat"} />
      <List.Item title={data().data.kota + ", " + data().data.provinsi} description={"Kota, Provinsi"} />
    </List.Section>
  )

  const UserInfo = () => (
    <>
      <List.Section title="Data Pengguna" style={{backgroundColor: Colors.whitesmoke}}>
        <List.Item title={data().data[0].alamat} description={"Alamat"} titleNumberOfLines={3}/>
        <List.Item title={data().data[0].kota + ", " + data().data[0].provinsi} description={"Kota, Provinsi"} titleNumberOfLines={3}/>
        <List.Item title={data().data[0].telp} description={"No. Telp"} titleNumberOfLines={3} />
        <List.Item title={data().data[0].fax} description={"No. Fax"} titleNumberOfLines={3} />
      </List.Section>

      <List.Section title="Dokumen" style={{backgroundColor: Colors.whitesmoke}}>
      <List.Item
        title={data().data[0].siup}
        description="SIUP"
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].siup_doc,
                },
              });
            }}
          />
        )}
        titleNumberOfLines={3}
      />
      <List.Item
        title={data().data[0].npwp}
        description="NPWP"
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].npwp_doc,
                },
              });
            }}
          />
        )}
        titleNumberOfLines={3}
      />
      <List.Item
        title={"Pakta Integritas"}
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].pakta_integritas,
                },
              });
            }}
          />
        )}
      />
      <List.Item
        title={"Form Penjualan"}
        right={() => (
          <IconButton
            icon="eye"
            onPress={function () {
              navigation.navigate("Modals", {
                screen: "ImageModal",
                params: {
                  url: UPLOAD_URL + data().data[0].form_penjualan,
                },
              });
            }}
          />
        )}
      />
      </List.Section>

      <List.Section title="Data PIC" style={{backgroundColor: Colors.whitesmoke}}>
        <List.Item title={data().data[0].telp_pic} description={"No. Telp"} titleNumberOfLines={3} />
        <List.Item title={data().data[0].hp_pic} description={"Handphone"} titleNumberOfLines={3} />
        <List.Item title={data().data[0].fax_pic} description={"No. Fax"} titleNumberOfLines={3} />
      </List.Section>
    </>
  )

  return (
    <Root style={{backgroundColor: 'white'}}>
      {/* <Caption>{JSON.stringify(data().data)}</Caption> */}
      <List.Item
        title={usertype == "Regulator" ? data().data.nama_regulator : data().data[0].nama_perusahaan}
        description={`${usertype == "Regulator" ? data().data.email : data().data[0].email}\n${usertype}`}
        left={() => <List.Icon icon="flag"/>}
      />
        <UpdateProfile/>
        <ResetPassword/>
        {usertype == "Regulator" ? <RegulatorInfo /> : <UserInfo />}
        <Button color={Colors.danger} mode="contained" onPress={() => {
          Alert.alert("Logout", "Anda yakin akan keluar?", [
            {text:"Ya", onPress:() => dispatch(doLogout())},
            {text:"Tidak", onPress:() => null}
          ])
        }}>Logout</Button>
    </Root>
  )
}

const styles = StyleSheet.create({})



// import React from "react";
// import { Root, Heading5, Space, Icon } from "../../components";
// import {
//   Paragraph,
//   List,
//   Caption,
//   Button,
//   Dialog,
//   Portal,
//   IconButton,
// } from "react-native-paper";
// import Colors from "./../../config/Colors";
// import { StyleSheet, View, Image, RefreshControl } from "react-native";
// import { useSelector, useDispatch } from "react-redux";
// import { RootState } from "../../redux/rootReducer";
// import { doLogout } from "../../redux/authReducers";
// import {
//   Placeholder,
//   PlaceholderMedia,
//   PlaceholderLine,
//   Fade,
// } from "rn-placeholder";
// import { consigneeFetchUser } from "../../redux/consigneeReducer";
// import { supplierFetchUser } from "../../redux/supplierReducer";
// import { shipperFetchUser } from "../../redux/shipperReducer";
// import { resellerFetchUser } from "./../../redux/resellerReducer";
// import { operatorFetchUser } from "./../../redux/operatorReducer";
// import { userFetchUser } from "../../redux/userReducer";
// import { UPLOAD_URL } from "../../config/constants";

// type ConfirmationDialog = {
//   onYes: any;
//   onNo: any;
//   visible: boolean;
//   onDismiss: any;
//   content: string;
//   title: string;
// };

// function LogoutConfirmation({
//   onYes,
//   onNo,
//   visible,
//   onDismiss,
//   content,
//   title,
// }: ConfirmationDialog) {
//   return (
//     <Portal>
//       <Dialog visible={visible} onDismiss={onDismiss}>
//         <Dialog.Title>{title}</Dialog.Title>
//         <Dialog.Content>
//           <Paragraph>{content}</Paragraph>
//         </Dialog.Content>
//         <Dialog.Actions>
//           <Button onPress={onYes}>Ya</Button>
//           <Button onPress={onNo}>Tidak</Button>
//         </Dialog.Actions>
//       </Dialog>
//     </Portal>
//   );
// }

// function ProfilePhoto({ nama }) {
//   const { usertype, email } = useSelector(function (state: RootState) {
//     return state.auth.authLoginSuccess
//       ? state.auth.authLoginSuccess.data.user[0]
//       : {
//           usertype: "Not Defined",
//           email: "Now Known",
//         };
//   });
//   return (
//     <View style={styles.profileContainer}>
//       <Image
//         style={styles.profileImage}
//         source={require("../../assets/img/download.png")}
//         resizeMode="center"
//       />
//       <Heading5>{nama}</Heading5>
//       <Caption>{email}</Caption>
//       <Paragraph>
//         <Icon name="shield-check" /> {usertype}
//       </Paragraph>
//     </View>
//   );
// }

// export default function Profile({ navigation }) {
//   const [refreshing, setRefreshing] = React.useState(false);
//   const [logout, setLogout] = React.useState(false);
//   const dispatch = useDispatch();
//   const { id, usertype } = useSelector(function (state: RootState) {
//     return state.auth.authLoginSuccess
//       ? state.auth.authLoginSuccess.data.user[0]
//       : {
//           usertype: "Not Defined",
//         };
//   });

//   const consigneeAddLoading = useSelector((state: RootState) => state.consignee.consigneeAddLoading);
//   const supplierAddLoading = useSelector((state: RootState) => state.supplier.supplierAddLoading);
//   const resellerAddLoading = useSelector((state: RootState) => state.reseller.resellerAddLoading);
//   const operatorAddLoading = useSelector((state: RootState) => state.operator.operatorAddLoading);
//   const shipperAddLoading = useSelector((state: RootState) => state.shipper.shipperAddLoading);
//   const userAddLoading = useSelector((state: RootState) => state.user.userAddLoading);

//   const loading = useSelector(function (state: RootState) {
//     switch (usertype) {
//       case "Consignee":
//         return state.consignee.consigneeViewLoading;
//       case "Supplier":
//         return state.supplier.supplierViewLoading;
//       case "Shipper":
//         return state.shipper.shipperViewLoading;
//       case "Reseller":
//         return state.reseller.resellerViewLoading;
//       case "Vessel Operator":
//         return state.operator.operatorViewLoading;
//       case "Regulator":
//         return state.user.userViewLoading;
//       default:
//         return false;
//     }
//   });
//   const onRefresh = React.useCallback(() => {
//     handleFetchUser();
//   }, [refreshing]);
//   const {
//     nama_perusahaan,
//     alamat,
//     nama_regulator,
//     email_perusahaan,
//     telp,
//     fax,
//     siup,
//     siup_doc,
//     npwp,
//     npwp_doc,
//     nama_pic,
//     email_pic,
//     hp_pic,
//     telp_pic,
//     fax_pic,
//     kota,
//     provinsi,
//     pakta_integritas,
//     form_penjualan,
//   } = useSelector(function (state: RootState) {
//     switch (usertype) {
//       case "Consignee":
//         return state.consignee.consigneeViewSuccess
//           ? state.consignee.consigneeViewSuccess.data[0]
//           : false;
//       case "Supplier":
//         return state.supplier.supplierViewSuccess
//           ? state.supplier.supplierViewSuccess.data[0]
//           : false;
//       case "Shipper":
//         return state.shipper.shipperViewSuccess
//           ? state.shipper.shipperViewSuccess.data[0]
//           : false;
//       case "Reseller":
//         return state.reseller.resellerViewSuccess
//           ? state.reseller.resellerViewSuccess.data[0]
//           : false;
//       case "Vessel Operator":
//         return state.operator.operatorViewSuccess
//           ? state.operator.operatorViewSuccess.data[0]
//           : false;
//       case "Regulator":
//         return state.user.userViewSuccess
//           ? state.user.userViewSuccess.data[0]
//           : false;
//       default:
//         return false;
//     }
//   });
//   function handleFetchUser() {
//     setRefreshing(true);
//     switch (usertype) {
//       case "Consignee":
//         dispatch(consigneeFetchUser(id));
//         break;
//       case "Supplier":
//         dispatch(supplierFetchUser(id));
//         break;
//       case "Shipper":
//         dispatch(shipperFetchUser(id));
//         break;
//       case "Reseller":
//         dispatch(resellerFetchUser(id));
//         break;
//       case "Vessel Operator":
//         dispatch(operatorFetchUser(id));
//         break;
//       case "Regulator":
//         dispatch(userFetchUser(id));
//         break;
//       default:
//         return false;
//     }
//     setRefreshing(false);
//   }
//   React.useEffect(() => {
//     handleFetchUser();
//   }, [
//     consigneeAddLoading,
//     supplierAddLoading, 
//     shipperAddLoading, 
//     resellerAddLoading, 
//     operatorAddLoading, 
//     userAddLoading
//   ]);
//   return (
//     <Root
//       style={styles.container}
//       refreshControl={
//         <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
//       }
//     >
//       <>
//         <ProfilePhoto nama={nama_perusahaan || nama_regulator} />
//         <Button
//           mode="contained"
//           onPress={function () {
//             navigation.navigate("UpdateProfile");
//           }}
//           style={{marginHorizontal: 50, marginVertical: 5, borderRadius: 5}}
//           color={Colors.pri}
//           icon="account-edit"
//         >
//           Perbarui Profil
//         </Button>
//         <Button
//           mode="contained"
//           onPress={function () {
//             navigation.navigate("ResetPassword");
//           }}
//           style={{marginHorizontal: 50, marginVertical: 5, borderRadius: 5}}
//           color={Colors.grayL}
//           icon="lock-reset"
//         >
//           Reset Password
//         </Button>
//         <List.Section>
//           {/**
//            * Details User
//            */}
//           <List.Subheader>Informasi Kontak</List.Subheader>
//           {loading ? (
//             <View style={{ paddingHorizontal: 20 }}>
//               <Placeholder Animation={Fade} Left={PlaceholderMedia}>
//                 <PlaceholderLine width={80} />
//                 <PlaceholderLine width={30} />
//               </Placeholder>
//             </View>
//           ) : (
//             <>
//               <List.Item
//                 title={`${alamat},${kota},${provinsi}`}
//                 description="Alamat"
//                 titleNumberOfLines={4}
//                 titleStyle={{
//                   fontSize: 13,
//                 }}
//                 left={() => <List.Icon icon="office-building" />}
//               />
//               <List.Item
//                 title={telp}
//                 description="No. Telp"
//                 left={() => <List.Icon color="#000" icon="phone-classic" />}
//               />
//               {usertype === "Regulator" ? null : <List.Item
//                 title={fax}
//                 description="No. Fax"
//                 left={() => <List.Icon color="#000" icon="fax" />}
//               />}
//             </>
//           )}

//           {/**
//            * Dokumen
//            */}
//           {
//             usertype === "Regulator" ? null :
//             (
//             <>
//               <List.Subheader>Dokumen Kelengkapan</List.Subheader>
//               {loading ? (
//                 <View style={{ paddingHorizontal: 20 }}>
//                   <Placeholder Animation={Fade} Left={PlaceholderMedia}>
//                     <PlaceholderLine width={80} />
//                     <PlaceholderLine width={30} />
//                   </Placeholder>
//                 </View>
//               ) : (
//                 <>
//                   <List.Item
//                     title={siup}
//                     description="SIUP"
//                     left={() => <List.Icon color="#000" icon="file-document" />}
//                     right={() => (
//                       <IconButton
//                         color={Colors.pri}
//                         icon="eye"
//                         onPress={function () {
//                           // alert("pressed");
//                           let uploader = '';
//                           switch(usertype) {
//                             case 'Consignee': uploader = 'consignee_uploads'; break;
//                             case 'Supplier': uploader = 'supplier_uploads'; break;
//                             case 'Shipper': uploader = 'shipper_uploads'; break;
//                             case 'Reseller': uploader = 'reseller_uploads'; break;
//                             case 'Vessel Operator': uploader = 'operator_uploads'; break;
//                             case 'Regulator': uploader = 'regulator_uploads'; break;
//                           }
//                           navigation.navigate("Modals", {
//                             screen: "ImageModal",
//                             params: {
//                               url: UPLOAD_URL + siup_doc,
//                             },
//                           });
//                           // alert(UPLOAD_URL + siup_doc);
//                         }}
//                       />
//                     )}
//                   />
//                   <List.Item
//                     title={npwp}
//                     description="NPWP"
//                     left={() => <List.Icon color="#000" icon="file-document" />}
//                     right={() => (
//                       <IconButton
//                         color={Colors.pri}
//                         icon="eye"
//                         onPress={function () {
//                           navigation.navigate("Modals", {
//                             screen: "ImageModal",
//                             params: {
//                               url: UPLOAD_URL + npwp_doc,
//                             },
//                           });
//                         }}
//                       />
//                     )}
//                   />
//                   {usertype === "Consignee" ? (
//                     <>
//                       <List.Item
//                         title={"Pakta Integritas"}
//                         left={() => <List.Icon color="#000" icon="file-document" />}
//                         right={() => (
//                           <IconButton
//                             color={Colors.pri}
//                             icon="eye"
//                             onPress={function () {
//                               navigation.navigate("Modals", {
//                                 screen: "ImageModal",
//                                 params: {
//                                   url: UPLOAD_URL + pakta_integritas,
//                                 },
//                               });
//                             }}
//                           />
//                         )}
//                       />
//                       <List.Item
//                         title={"Form Penjualan"}
//                         left={() => <List.Icon color="#000" icon="file-document" />}
//                         right={() => (
//                           <IconButton
//                             color={Colors.pri}
//                             icon="eye"
//                             onPress={function () {
//                               navigation.navigate("Modals", {
//                                 screen: "ImageModal",
//                                 params: {
//                                   url: UPLOAD_URL + form_penjualan,
//                                 },
//                               });
//                             }}
//                           />
//                         )}
//                       />
//                     </>
//                   ) : null}
//                 </>
//               )}

//               {/**
//                * Details User
//                */}
//               <List.Subheader>Data PIC</List.Subheader>
//               {loading ? (
//                 <View style={{ paddingHorizontal: 20 }}>
//                   <Placeholder Animation={Fade} Left={PlaceholderMedia}>
//                     <PlaceholderLine width={80} />
//                     <PlaceholderLine width={30} />
//                   </Placeholder>
//                 </View>
//               ) : (
//                 <>
//                   <List.Item
//                     title={nama_pic}
//                     description="Nama PIC"
//                     left={() => <List.Icon color="#000" icon="account" />}
//                   />
//                   <List.Item
//                     title={email_pic}
//                     description="Email"
//                     left={() => <List.Icon color="#000" icon="email" />}
//                   />
//                   <List.Item
//                     title={hp_pic}
//                     description="Phone"
//                     left={() => <List.Icon color="#000" icon="cellphone" />}
//                   />
//                   <List.Item
//                     title={telp_pic}
//                     description="No. Telp"
//                     left={() => <List.Icon color="#000" icon="phone-classic" />}
//                   />
//                   <List.Item
//                     title={fax_pic}
//                     description="No. Fax"
//                     left={() => <List.Icon color="#000" icon="fax" />}
//                   />
//                 </>
//               )}
//             </>    
//             )
//           }
          

//           <Button
//             color={Colors.danger}
//             onPress={function () {
//               setLogout(true);
//             }}
//           >
//             LOGOUT
//           </Button>
//         </List.Section>
//         <LogoutConfirmation
//           visible={logout}
//           onYes={function () {
//             setLogout(false);
//             dispatch(doLogout());
//           }}
//           onNo={function () {
//             setLogout(false);
//           }}
//           content="Apakah anda yakin untuk keluar ?"
//           title="Logout"
//           onDismiss={function () {
//             setLogout(false);
//           }}
//         />
//       </>
//       {/* )} */}
//     </Root>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: Colors.white,
//   },
//   profileContainer: {
//     alignItems: "center",
//     paddingVertical: 20,
//     borderBottomColor: Colors.gray1,
//     borderBottomWidth: 1,
//   },
//   profileImage: { width: 50, height: 50, borderRadius: 5 },
// });
