import React from "react";
import { StyleSheet, Image, Alert } from "react-native";
import { Root, Space, ErrorText, FileInput, SelectInput2, DateTime, TextInput } from "../../components";
import { Button, Caption, HelperText } from "react-native-paper";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { userUpdate } from "../../redux/userReducer";
import { DateFormatStrip } from "../../services/utils";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { kotaFetchByProvinsi } from "../../redux/kotaReducer";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  nama_regulator: Yup.string().required("Required"),
  alamat: Yup.string().required("Required"),
  id: Yup.string().required("Required"),
  jabatan: Yup.string().required("Required"),
  telp: Yup.string().required("Required"),
  tgl_lahir: Yup.string().required("Required"),
  tmpt_lahir: Yup.string().required("Required"),
  provinsi_id: Yup.string().required("Required"),
  kota_id: Yup.string().required("Required"),
});
/**
 * MAIN SCREEN
 */
export default function UpdateRegulatorProfile({ profile, navigation }) {
  const dispatch = useDispatch();
  const loading = useSelector(function (state: RootState) {
    return state.user.userAddLoading;
  });
  const {provinsi, kota} = useSelector((state: RootState) => state);
  // const provinsi = useSelector((state: RootState) => state.provinsi.provinsiGetSuccess ? state.provinsi.provinsiGetSuccess.data : []);
  // const kota = useSelector((state: RootState) => state.kota.kotaGetSuccess ? state.kota.kotaGetSuccess.data : []);

  if(!provinsi.provinsiGetSuccess.data && !kota.kotaGetSuccess.data) return null;
  return (
    <Root style={styles.container}>
      <Formik
        // validationSchema={RegisterSchema}
        initialValues={{
          alamat: profile.alamat_regulator,
          id: profile.id,
          jabatan: profile.jabatan,
          nama_regulator: profile.nama_regulator,
          telp: profile.telp_regulator,
          tgl_lahir: DateFormatStrip(profile.tgl_lahir),
          tmpt_lahir: profile.tmpt_lahir,
          kota_id: profile.kota_id,
          provinsi_id: profile.provinsi_id,
        }}
        validationSchema={RegisterSchema}
        onSubmit={function (values) {
          Alert.alert("Ubah", "Anda yakin ingin memperbarui profil?", [
            {text:"Ya", onPress:() => {
              dispatch(userUpdate(values))
              navigation.goBack();
            }},
            {text: "Tidak", onPress:() => null}
          ]);
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Perusahaan
             */}
            <TextInput
              value={values.nama_regulator}
              onChangeText={handleChange("nama_regulator")}
              onBlur={handleBlur("nama_regulator")}
              label="Nama Perusahaan"
            />
            {errors.nama_perusahaan && touched.nama_perusahaan && (
              <ErrorText>{errors.nama_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/** Jabatan */}
            <TextInput
              value={values.jabatan}
              onChangeText={handleChange('jabatan')}
              onBlur={handleBlur('jabatan')}
              label="Jabatan"
            />
            {/**
             * Alamat
             */}
            <TextInput
              value={values.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              label="Alamat"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />
            <SelectInput2
              items={provinsi.provinsiGetSuccess.data}
              label="Provinsi"
              itemLabel="label"
              itemValue="id"
              mode="dialog"
              selectedValue={profile.provinsi_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('provinsi_id', itemValue)
              }}
            />
            <SelectInput2
              items={kota.kotaGetSuccess.data}
              label="Kota"
              itemLabel="label"
              itemValue="kota_id"
              mode="dropdown"
              selectedValue={profile.kota_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('kota_id', itemValue)
              }}
            />
            {/**
             * Nomor Telepon
             */}
            <TextInput
              dense
              value={values.telp}
              onChangeText={handleChange("telp")}
              onBlur={handleBlur("telp")}
              label="No. Telp"
              keyboardType="number-pad"
            />
            {errors.telp && touched.telp && (
              <ErrorText>{errors.telp}</ErrorText>
            )}
            <Space height={20} />
            {/** Tanggal Lahir */}
            <DateTime type="date" label="Tanggal Lahir" onChangeValue={(val) => setFieldValue('tgl_lahir', val.date)} initialValue={values.tgl_lahir}/>
            <Space height={40} />          

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.gray1 }}
              onPress={handleSubmit}
              loading={loading}
            >
              Update
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    color: "black",
  },
  textInput: {
    paddingHorizontal: 10,
  },
  pickerContainer: {
    backgroundColor: Colors.grayL,
    paddingVertical: 5,
  },
});
