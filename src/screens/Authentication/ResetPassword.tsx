import React from "react";
import { StyleSheet } from "react-native";
import { Button } from "react-native-paper";
import { Root, Space, ErrorText, TextInput } from "../../components";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import { resetPassword } from "../../redux/forgotpasswordReducer";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { userChangePassword } from "../../redux/userReducer";

/**
 * Validation Schema
 * Reset Password
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const ResetPasswordSchema = Yup.object().shape({
  password: Yup.string().required("Password Empty").min(6),
  confirm_password: Yup.string()
    .equalTo(Yup.ref("password"), "Passwords must match")
    .required("Required"),
});

/**
 * Reset Password Screen
 * @param navigation navigation prop
 * @param route route prop
 */
export default function ResetPassword({ navigation, route }) {
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const item = route.params.item;
  const loading = useSelector(function (state: RootState) {
    return state.forgotpassword.resetPasswordLoading;
  });
  const dispatch = useDispatch();
  return (
    <Root style={styles.container}>
      <Space height={20} />
      <Formik
        initialValues={{ 
          confirm_password: "",
          email: item.email,
          id: String(item.id),
          password: "",
        }}
        onSubmit={(values) => {
          dispatch(userChangePassword(values))
          navigation.goBack();
        }}
        validationSchema={ResetPasswordSchema}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.password}
              secureTextEntry={true}
              onChangeText={handleChange("password")}
              onBlur={handleBlur("password")}
              label="Password"
              keyboardType="ascii-capable"
            />
            {errors.password && touched.password && (
              <ErrorText>{errors.password}</ErrorText>
            )}
            <Space height={10} />
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.confirm_password}
              secureTextEntry={true}
              onChangeText={handleChange("confirm_password")}
              onBlur={handleBlur("confirm_password")}
              label="Konfirmasi Password"
              keyboardType="ascii-capable"
            />
            {errors.confirm_password && touched.confirm_password && (
              <ErrorText>{errors.confirm_password}</ErrorText>
            )}
            <Space height={20} />
            <Button
              mode="contained"
              color={Colors.sec}
              labelStyle={{ color: Colors.black }}
              onPress={handleSubmit}
              loading={loading}
            >
              Reset Password
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  welcome: {
    color: "white",
  },
  headline: {
    color: "white",
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    opacity: 0.8,
    borderRadius: 3,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
