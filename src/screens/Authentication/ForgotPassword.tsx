import React from "react";
import { View, StyleSheet, TextInput } from "react-native";
import { Button, Headline, Paragraph, Text } from "react-native-paper";
import { Picker } from "@react-native-community/picker";
import { Root, Space, ErrorText, Icon } from "../../components";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import { forgotPassword } from "../../redux/forgotpasswordReducer";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";

/**
 * Validation Schema
 * Forgot Password
 */
const ForgotPasswordSchema = Yup.object().shape({
  usery_type_id: Yup.number()
    .moreThan(0, "Pilih User !")
    .lessThan(7, "User not found"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
});

/**
 * Forgot Password Screen
 * @param navigation Navigation Props
 */
export default function ForgotPassword({ navigation }) {
  const loading = useSelector(function (state: RootState) {
    return state.forgotpassword.forgotPasswordLoading;
  });
  const dispatch = useDispatch();
  return (
    <Root style={styles.container}>
      <Icon
        name={"arrow-left"}
        style={{ fontSize: 30, color: Colors.sec }}
        onPress={function () {
          navigation.goBack();
        }}
      />
      <Space height={30} />
      <Paragraph style={[styles.welcome]}>
        Whoaa ✋,{"\n"}
        <Headline style={styles.headline}>Forgot Password</Headline>
      </Paragraph>
      <Text style={{ color: Colors.sec }}>Send password to my email</Text>
      <Space height={20} />
      <Formik
        initialValues={{ email: "", usery_type_id: 0 }}
        onSubmit={(values) => dispatch(forgotPassword(values))}
        validationSchema={ForgotPasswordSchema}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.usery_type_id}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number, itemIndex: number) {
                  setFieldValue("usery_type_id", itemValue);
                }}
              >
                <Picker.Item label="-Pilih User-" value={0} />
                <Picker.Item label="Consignee" value={1} />
                <Picker.Item label="Supplier" value={2} />
                <Picker.Item label="Shipper" value={3} />
                <Picker.Item label="Reseller" value={4} />
                <Picker.Item label="Operator" value={5} />
                <Picker.Item label="Regulator" value={6} />
              </Picker>
            </View>
            {errors.usery_type_id && touched.usery_type_id && (
              <ErrorText>{errors.usery_type_id}</ErrorText>
            )}
            <Space height={10} />
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.email}
              onChangeText={handleChange("email")}
              onBlur={handleBlur("email")}
              placeholder="Email Address"
              keyboardType="email-address"
            />
            {errors.email && touched.email && (
              <ErrorText>{errors.email}</ErrorText>
            )}
            <Space height={10} />
            <Button
              mode="contained"
              color={Colors.sec}
              labelStyle={{ color: Colors.black }}
              onPress={handleSubmit}
              loading={loading}
            >
              Submit
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.ter,
    padding: 30,
  },
  welcome: {
    color: "white",
  },
  headline: {
    color: "white",
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    opacity: 0.8,
    borderRadius: 3,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
