import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Root, Space, ErrorText, FileInput, TextInput, SelectInput2 } from "../../components";
import { Button, Paragraph, Caption } from "react-native-paper";
import { Picker } from "@react-native-community/picker";
import Colors from "../../config/Colors";
import { RegisterStackProps } from "../Navigator";
import { Formik } from "formik";
import * as Yup from "yup";
import { LIST_PROVINSI, BASE_URL } from "./../../config/constants";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { kotaFetchByProvinsi } from "../../redux/kotaReducer";
import { consigneeAdd } from "../../redux/consigneeReducer";
import TextInputMask from "react-native-text-input-mask";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  usery_type_id: Yup.number()
    .moreThan(0, "Pilih User !")
    .lessThan(7, "User not found"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
  password: Yup.string().required("Password Empty").min(6),
  nama_perusahaan: Yup.string().required("Required").min(6),
  alamat: Yup.string().required("Required"),
  fax: Yup.number().required("Required"),
  telp: Yup.number().required("Required"),
  siup: Yup.number().required("Required"),
  siup_doc: Yup.string().required("Required"),
  npwp: Yup.number().required("Required"),
  npwp_doc: Yup.string().required("Required"),
  pakta_integritas: Yup.string().required("Required"),
  form_penjualan: Yup.string().required("Required"),
  nama_pic: Yup.string().required("Required"),
  email_pic: Yup.string().email("Invalid email").required("Email Empty"),
  hp_pic: Yup.number().required("Required"),
  telp_pic: Yup.number().required("Required"),
  fax_pic: Yup.number().required("Required"),
  provinsi_id: Yup.string().required(),
  kota_id: Yup.string().required(),
});
/**
 * MAIN SCREEN
 */
export default function RegisterConsignee({
  route,
}: RegisterStackProps<"Register">) {
  const dispatch = useDispatch();
  const usertype = route.params ? route.params.usery_type_id : 0;
  const loading = useSelector(function (state: RootState) {
    return state.consignee.consigneeAddLoading;
  });
  const listProvinsi = useSelector(function (state: RootState) {
    return state.provinsi.provinsiGetSuccess
      ? state.provinsi.provinsiGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  const listKota = useSelector(function (state: RootState) {
    return state.kota.kotaGetSuccess
      ? state.kota.kotaGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  React.useEffect(() => {
    dispatch(provinsiFetchAll());
  }, []);

  return (
    <Root style={styles.container}>
      <Formik
        validationSchema={RegisterSchema}
        initialValues={{
          nama_perusahaan: "",
          email: route.params ? route.params.email : "",
          alamat: "",
          fax: "",
          telp: "",
          siup: "",
          siup_doc: "",
          siup_doc_size: "",
          npwp: "",
          npwp_doc: "",
          npwp_doc_size: "",
          pakta_integritas: "",
          pakta_integritas_size: "",
          form_penjualan: "",
          form_penjualan_size: "",
          nama_pic: "",
          email_pic: "",
          hp_pic: "",
          telp_pic: "",
          fax_pic: "",
          provinsi_id: "",
          kota_id: "",
          usery_type_id: route.params ? route.params.usery_type_id : 0,
          password: route.params ? route.params.password : "",
        }}
        onSubmit={function (values) {
          // alert(JSON.stringify(values));
          dispatch(consigneeAdd(values));
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Perusahaan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_perusahaan}
              error={errors.nama_perusahaan}
              onChangeText={handleChange("nama_perusahaan")}
              onBlur={handleBlur("nama_perusahaan")}
              label="Nama Perusahaan"
            />
            {errors.nama_perusahaan && touched.nama_perusahaan && (
              <ErrorText>{errors.nama_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.alamat}
              error={errors.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              label="Alamat"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Provinsi ID
             */}
            <SelectInput2
              items={listProvinsi}
              mode="dropdown"
              itemLabel="label"
              itemValue="id"
              selectedValue={values.provinsi_id}
              error={errors.provinsi_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('provinsi_id', itemValue)
                dispatch(kotaFetchByProvinsi(itemValue))
              }}
              label="Provinsi"
            />
            <Space height={10} />

            {/**
             * Kota ID
             */}
            <SelectInput2
              items={listKota}
              mode="dropdown"
              itemLabel="label"
              itemValue="kota_id"
              selectedValue={values.kota_id}
              error={errors.kota_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('kota_id', itemValue)
              }}
              label="Kota"
            />
            <Space height={10} />

            {/**
             * Nomor Fax
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax}
              error={errors.fax}
              onChangeText={handleChange("fax")}
              onBlur={handleBlur("fax")}
              label="No. Fax"
              keyboardType="number-pad"
            />
            {errors.fax && touched.fax && <ErrorText>{errors.fax}</ErrorText>}
            <Space height={10} />

            {/**
             * Nomor Telepon
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp}
              error={errors.telp}
              onChangeText={handleChange("telp")}
              onBlur={handleBlur("telp")}
              label="No. Telp"
              keyboardType="number-pad"
            />
            {errors.telp && touched.telp && (
              <ErrorText>{errors.telp}</ErrorText>
            )}
            <Space height={40} />

            {/**
             * Nomor SIUP
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.siup}
              error={errors.siup}
              onChangeText={handleChange("siup")}
              onBlur={handleBlur("siup")}
              label="No. SIUP"
              keyboardType="number-pad"
            />
            {errors.siup && touched.siup && (
              <ErrorText>{errors.siup}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen SIUP
             */}
            {values.siup_doc ? (
              <>
                <Image
                  source={{ uri: values.siup_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Dokumen SIUP"
              label={
                values.siup_doc ? values.siup_doc.name : "Upload File siup"
              }
              error={errors.siup_doc_size}
              getValue={function (value) {
                setFieldValue("siup_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("siup_doc_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Nomor NPWP
             */}
            <Caption>No. NPWP</Caption>
            <View
              style={{
                borderRadius: 3,
                paddingHorizontal: 5,
                borderWidth: 1.5
              }}
            >
              <TextInputMask
                onChangeText={(formatted, extracted) => {
                  setFieldValue("npwp", formatted); // +1 (123) 456-78-90
                }}
                mask={"[00].[000].[000].[0]-[000].[000]"}
                label={"No. NPWP"}
              />
            </View>
            {errors.npwp && touched.npwp && (
              <ErrorText>{errors.npwp}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen NPWP
             */}
            {values.npwp_doc ? (
              <>
                <Image
                  source={{ uri: values.npwp_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Dokumen NPWP"
              label={
                values.npwp_doc ? values.npwp_doc.name : "Upload File npwp"
              }
              error={errors.npwp_doc_size}
              getValue={function (value) {
                setFieldValue("npwp_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("npwp_doc_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Pakta Integritas
             */}
            {values.pakta_integritas ? (
              <>
                <Image
                  source={{ uri: values.pakta_integritas.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Pakta Integritas"
              label={
                values.pakta_integritas
                  ? values.pakta_integritas.name
                  : "Upload File Pakta Integritas"
              }
              error={errors.pakta_integritas_size}
              getValue={function (value) {
                setFieldValue("pakta_integritas", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("pakta_integritas_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Form Penjualan
             */}
            {values.form_penjualan ? (
              <>
                <Image
                  source={{ uri: values.form_penjualan.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Form Penjualan"
              label={
                values.form_penjualan
                  ? values.form_penjualan.name
                  : "Upload File npwp"
              }
              error={errors.form_penjualan_size}
              getValue={function (value) {
                setFieldValue("form_penjualan", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("form_penjualan_size", value.fileSize);
              }}
            />
            <Space height={40} />

            {/** ========== PIC ========== */}
            {/**
             * Nama PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_pic}
              error={errors.nama_pic}
              onChangeText={handleChange("nama_pic")}
              onBlur={handleBlur("nama_pic")}
              label="Nama PIC"
            />
            {errors.nama_pic && touched.nama_pic && (
              <ErrorText>{errors.nama_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Email PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.email_pic}
              error={errors.email_pic}
              onChangeText={handleChange("email_pic")}
              onBlur={handleBlur("email_pic")}
              label="Email PIC"
              keyboardType="email-address"
            />
            {errors.email_pic && touched.email_pic && (
              <ErrorText>{errors.email_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * HP PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.hp_pic}
              error={errors.hp_pic}
              onChangeText={handleChange("hp_pic")}
              onBlur={handleBlur("hp_pic")}
              label="No. HP PIC"
              keyboardType="number-pad"
            />
            {errors.hp_pic && touched.hp_pic && (
              <ErrorText>{errors.hp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Telp PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_pic}
              error={errors.telp_pic}
              onChangeText={handleChange("telp_pic")}
              onBlur={handleBlur("telp_pic")}
              label="No. Telp. PIC"
              keyboardType="number-pad"
            />
            {errors.telp_pic && touched.telp_pic && (
              <ErrorText>{errors.telp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Fax PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax_pic}
              error={errors.fax_pic}
              onChangeText={handleChange("fax_pic")}
              onBlur={handleBlur("fax_pic")}
              label="No. Fax. PIC"
              keyboardType="numeric"
            />
            {errors.fax_pic && touched.fax_pic && (
              <ErrorText>{errors.fax_pic}</ErrorText>
            )}
            <Space height={30} />

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.gray1 }}
              onPress={handleSubmit}
              loading={loading}
            >
              Register
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    padding: 30,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
