import React from "react";
import { StyleSheet, Image } from "react-native";
import { Root, Space, Heading3 } from "../../components";
import Colors from "./../../config/Colors";
import { Paragraph, Button } from "react-native-paper";
import { AuthStackProps } from "../Navigator";
import { useDispatch } from "react-redux";
import { setConsigneeAddSuccess } from "../../redux/consigneeReducer";
import { setSupplierAddSuccess } from "../../redux/supplierReducer";
import { setShipperAddSuccess } from "../../redux/shipperReducer";
import { setResellerAddSuccess } from "../../redux/resellerReducer";
import { setOperatorAddSuccess } from "../../redux/operatorReducer";

export default function RegisterSuccess({
  navigation,
}: AuthStackProps<"Register">) {
  const dispatch = useDispatch();
  return (
    <Root style={styles.container}>
      <Space height={50} />
      <Image
        source={require("../../assets/img/tick.png")}
        style={styles.image}
        resizeMode="center"
      />
      <Space height={50} />
      <Heading3 style={styles.text1}>Selamat Registrasi Berhasil </Heading3>
      <Space height={20} />
      <Paragraph style={styles.text2}>
        Link verifikasi akun akan dikirimkan melalui email dalam (1 x 24 Jam).
      </Paragraph>
      <Space height={50} />
      <Button
        mode="contained"
        color={Colors.gray1}
        style={{ width: "100%" }}
        onPress={function () {
          dispatch(setConsigneeAddSuccess(false));
          dispatch(setSupplierAddSuccess(false));
          dispatch(setShipperAddSuccess(false));
          dispatch(setResellerAddSuccess(false));
          dispatch(setOperatorAddSuccess(false));
          navigation.navigate("Login");
        }}
      >
        Kembali
      </Button>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.pri,
    alignItems: "center",
    padding: 30,
  },
  image: {
    height: 200,
  },
  text1: {
    color: Colors.white,
    textAlign: "center",
  },
  text2: {
    color: Colors.sec,
    textAlign: "center",
  },
});
