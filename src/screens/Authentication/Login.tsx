import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Button, Headline, TextInput, Paragraph, HelperText, Title } from "react-native-paper";
import { Picker } from "@react-native-community/picker";
import { Root, Space, ErrorText } from "../../components";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as Yup from "yup";
import { AuthStackProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import { doLogin } from "../../redux/authReducers";
import { RootState } from "src/redux/rootReducer";

/**
 * Validation Schema
 */
const LoginSchema = Yup.object().shape({
  usery_type_id: Yup.number()
    .moreThan(0, "Pilih User !")
    .lessThan(8, "User not found"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
  password: Yup.string().required("Password Empty").min(6),
});

/**
 * Main Screen
 * @param navigation
 */
function Login({ navigation }: AuthStackProps<"Login">) {
  const [userRole, setUserRole] = React.useState("none");
  const dispatch = useDispatch();
  const isLoading = useSelector(function (state: RootState) {
    return state.auth.authLoginLoading;
  });
  return (
    <View style={{ flex: 1, backgroundColor: Colors.white }}>
      <Root style={styles.container}>
        <Image
          source={require("../../assets/img/lcslogo.png")}
          resizeMethod="resize"
          resizeMode="contain"
          style={{ height: 75, maxWidth: 200, alignSelf: 'center' }}
        />
        <Title style={{textAlign: 'center', color: Colors.pri}}>Sistem Informasi Tol Laut</Title>
        <Space height={30} />
        <Formik
          initialValues={{ email: "", password: "", usery_type_id: 0 }}
          onSubmit={(values) => {
            dispatch(doLogin(values));
          }}
          validationSchema={LoginSchema}
        >
          {({
            handleChange,
            setFieldValue,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,

          }) => (
              <>
                <View
                  style={{ flexDirection: "row", justifyContent: "center", backgroundColor: Colors.grayL }}
                >
                  <Picker
                    selectedValue={userRole}
                    style={{ flex: 10 }}
                    itemStyle={{ fontSize: 8 }}
                    mode="dropdown"
                    onValueChange={function (
                      itemValue: string,
                      itemIndex: number
                    ) {
                      setFieldValue("usery_type_id", itemIndex);
                      setUserRole(itemValue);
                    }}
                  >
                    <Picker.Item label="-Pilih User-" value="none" />
                    <Picker.Item label="Consignee" value="consignee" />
                    <Picker.Item label="Supplier" value="supplier" />
                    <Picker.Item label="Shipper" value="shipper" />
                    <Picker.Item label="Reseller" value="reseller" />
                    <Picker.Item label="Operator" value="operator" />
                    <Picker.Item label="Regulator" value="regulator" />
                    <Picker.Item label="Visitor" value="visitor" />
                  </Picker>
                </View>
                {errors.usery_type_id && touched.usery_type_id && (
                  <HelperText type="error" visible={errors.usery_type_id}>{errors.usery_type_id}</HelperText>
                )}
                <Space height={10} />
                <TextInput
                  style={[styles.formItem, styles.textInput]}
                  value={values.email}
                  onChangeText={handleChange("email")}
                  onEndEditing={function(val){setFieldValue('email', val.nativeEvent.text.toLowerCase())}}
                  autoCapitalize="none"
                  onBlur={handleBlur("email")}
                  label="Alamat Email"
                  keyboardType="email-address"
                  error={errors.email}
                />
                {errors.email && touched.email && (
                  <HelperText type="error" visible={errors.email}>{errors.email}</HelperText>
                )}
                <Space height={10} />
                <TextInput
                  style={[styles.formItem, styles.textInput]}
                  value={values.password}
                  secureTextEntry={true}
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  label="Password"
                  passwordRules="required:digit;required:upper;minlength:8;"
                  keyboardType="ascii-capable"
                  error={errors.password}
                />
                {errors.password && touched.password && (
                  <HelperText type="error" visible={errors.password}>{errors.password}</HelperText>
                )}
                <Space height={20} />
                <Button
                  mode="contained"
                  color={Colors.pri}
                  labelStyle={{ color: Colors.white }}
                  onPress={handleSubmit}
                  loading={isLoading}
                  style={{ borderRadius: 3 }}
                  contentStyle={{ padding: 5 }}
                >
                  Masuk
              </Button>
              </>
            )}
        </Formik>

        <Space height={5} />
        <Paragraph
          style={{ color: Colors.pri, textAlign: "right", fontWeight: "bold" }}
          onPress={function () {
            navigation.navigate("ForgotPassword");
          }}
        >
          Lupa Password ?
        </Paragraph>
        <Space height={20} />
        <Paragraph style={{ textAlign: "center" }}>
          Belum memiliki akun?{" "}
          <Paragraph
            onPress={function () {
              navigation.navigate("Register");
            }}
            style={{ color: Colors.pri, fontWeight: "bold" }}
          >
            Daftar
          </Paragraph>
        </Paragraph>
      </Root>
      <Image
        source={require("../../assets/img/tollaut.png")}
        style={{
          width: "100%",
          height: 200,
          opacity: 0.3,
          position: "absolute",
          zIndex: -3,
          bottom: 0,
        }}
      />
    </View>
  );
}

export default React.memo(Login);

const styles = StyleSheet.create({
  container: {
    padding: 30,
    justifyContent: 'center',
  },
  formItem: {
    backgroundColor: Colors.white,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
});
