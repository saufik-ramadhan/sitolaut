import React from "react";
import { View, TextInput, StyleSheet, Image } from "react-native";
import { Root, Space, ErrorText, FileInput, DateTime } from "../../components";
import { Button, Paragraph } from "react-native-paper";
import { Picker } from "@react-native-community/picker";
import Colors from "../../config/Colors";
import { RegisterStackProps } from "../Navigator";
import { Formik } from "formik";
import * as Yup from "yup";
import { LIST_PROVINSI, BASE_URL } from "./../../config/constants";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { kotaFetchByProvinsi } from "../../redux/kotaReducer";
import { operatorAdd } from "../../redux/operatorReducer";
import TextInputMask from "react-native-text-input-mask";
import { DateFormatStrip } from "../../services/utils";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  usery_type_id: Yup.number()
    .moreThan(0, "Pilih User !")
    .lessThan(7, "User not found"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
  password: Yup.string().required("Password Empty").min(6),
  nama_perusahaan: Yup.string().required("Required").min(6),
  alamat: Yup.string().required("Required"),
  fax: Yup.number().required("Required"),
  telp: Yup.number().required("Required"),
  siup: Yup.number().required("Required"),
  siup_doc: Yup.string().required("Required"),
  npwp: Yup.number().required("Required"),
  npwp_doc: Yup.string().required("Required"),
  pakta_integritas: Yup.string().required("Required"),
  form_penjualan: Yup.string().required("Required"),
  nama_pic: Yup.string().required("Required"),
  email_pic: Yup.string().email("Invalid email").required("Email Empty"),
  hp_pic: Yup.number().required("Required"),
  telp_pic: Yup.number().required("Required"),
  fax_pic: Yup.number().required("Required"),
  provinsi_id: Yup.number().moreThan(0, "Pilih Provinsi !"),
  kota_id: Yup.number().moreThan(0, "Pilih Kota !"),
});
/**
 * MAIN SCREEN
 */
export default function RegisterOperator({
  route,
}: RegisterStackProps<"Register">) {
  const dispatch = useDispatch();
  const usertype = route.params ? route.params.usery_type_id : 0;
  const loading = useSelector(function (state: RootState) {
    return state.operator.operatorAddLoading;
  });
  const listProvinsi = useSelector(function (state: RootState) {
    return state.provinsi.provinsiGetSuccess
      ? state.provinsi.provinsiGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  const listKota = useSelector(function (state: RootState) {
    return state.kota.kotaGetSuccess
      ? state.kota.kotaGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  React.useEffect(() => {
    dispatch(provinsiFetchAll());
  }, []);
  const currentDate = new Date();
  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          nama_perusahaan: "",
          email: route.params ? route.params.email : "",
          usery_type_id: route.params ? route.params.usery_type_id : 0,
          password: route.params ? route.params.password : "",
          provinsi_id: 11,
          kota_id: "",
          alamat: "",
          fax: "",
          telp: "",
          siup: "",
          siup_doc: "",
          siup_doc_size: "",
          npwp: "",
          npwp_doc: "",
          npwp_doc_size: "",

          // Penanda Tangan
          nama_ttd: "",
          foto_ttd: "",
          foto_ttd_size: "",
          logo_perusahaan: "",
          logo_perusahaan_size: "",

          // PMKU
          no_pmku: "",
          tgl_pmku: `${currentDate.getFullYear()}-${currentDate.getMonth()}-${currentDate.getDate()}`,
          nama_bank: "",
          nomor_rekening: "",
          atas_nama: "",

          // Kontrak
          nomor_kontrak: "",
          tanggal_awal_kontrak: DateFormatStrip(currentDate),
          tanggal_habis_kontrak: DateFormatStrip(currentDate),
          addendum_kontrak: "",

          // PIC
          nama_pic: "",
          email_pic: "",
          hp_pic: "",
          telp_pic: "",
          fax_pic: "",
        }}
        onSubmit={function (values) {
          dispatch(operatorAdd(values));
          // alert(JSON.stringify(values));
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Perusahaan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_perusahaan}
              onChangeText={handleChange("nama_perusahaan")}
              onBlur={handleBlur("nama_perusahaan")}
              placeholder="Nama Perusahaan"
            />
            {errors.nama_perusahaan && touched.nama_perusahaan && (
              <ErrorText>{errors.nama_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              placeholder="Alamat"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Provinsi ID
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.provinsi_id}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("provinsi_id", itemValue);
                  dispatch(kotaFetchByProvinsi(itemValue));
                }}
              >
                <Picker.Item label="Pilih Provinsi" value={0} />
                {listProvinsi.map(function (item, key) {
                  return (
                    <Picker.Item label={item.label} value={item.id} key={key} />
                  );
                })}
              </Picker>
            </View>
            {errors.provinsi_id && touched.provinsi_id && (
              <ErrorText>{errors.provinsi_id}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Kota ID
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.kota_id}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("kota_id", itemValue);
                }}
              >
                <Picker.Item label="Pilih Kota" value={0} />
                {listKota.map(function (item, key) {
                  return (
                    <Picker.Item
                      label={item.label}
                      value={item.kota_id}
                      key={key}
                    />
                  );
                })}
              </Picker>
            </View>
            {errors.kota_id && touched.kota_id && (
              <ErrorText>{errors.kota_id}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor Fax
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax}
              onChangeText={handleChange("fax")}
              onBlur={handleBlur("fax")}
              placeholder="No. Fax"
              keyboardType="number-pad"
            />
            {errors.fax && touched.fax && <ErrorText>{errors.fax}</ErrorText>}
            <Space height={10} />

            {/**
             * Nomor Telepon
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp}
              onChangeText={handleChange("telp")}
              onBlur={handleBlur("telp")}
              placeholder="No. Telp"
              keyboardType="number-pad"
            />
            {errors.telp && touched.telp && (
              <ErrorText>{errors.telp}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor SIUP
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.siup}
              onChangeText={handleChange("siup")}
              onBlur={handleBlur("siup")}
              placeholder="No. SIUP"
              keyboardType="number-pad"
            />
            {errors.siup && touched.siup && (
              <ErrorText>{errors.siup}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen SIUP
             */}
            {values.siup_doc ? (
              <>
                <Image
                  source={{ uri: values.siup_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Dokumen SIUP"
              placeholder={values.siup_doc ? values.siup_doc.name : null}
              error={errors.siup_doc_size}
              getValue={function (value) {
                setFieldValue("siup_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("siup_doc_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Nomor NPWP
             */}
            <View
              style={{
                backgroundColor: Colors.grayL,
                borderRadius: 3,
                paddingHorizontal: 5,
              }}
            >
              <TextInputMask
                onChangeText={(formatted, extracted) => {
                  setFieldValue("npwp", formatted); // +1 (123) 456-78-90
                }}
                mask={"[00].[000].[000].[0]-[000].[000]"}
                placeholder={"No. NPWP"}
              />
            </View>
            {errors.npwp && touched.npwp && (
              <ErrorText>{errors.npwp}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen NPWP
             */}
            {values.npwp_doc ? (
              <>
                <Image
                  source={{ uri: values.npwp_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Dokumen NPWP"
              placeholder={values.npwp_doc ? values.npwp_doc.name : null}
              error={errors.npwp_doc_size}
              getValue={function (value) {
                setFieldValue("npwp_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("npwp_doc_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/** ========== PENANDA TANGAN ========== */}
            {/**
             * Nama Penanda Tangan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_ttd}
              onChangeText={handleChange("nama_ttd")}
              onBlur={handleBlur("nama_ttd")}
              placeholder="Nama Penanda Tangan"
            />
            {errors.nama_ttd && touched.nama_ttd && (
              <ErrorText>{errors.nama_ttd}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Foto Tanda Tangan
             */}
            {values.foto_ttd ? (
              <>
                <Image
                  source={{ uri: values.foto_ttd.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="File Tanda Tangan"
              placeholder={values.foto_ttd ? values.foto_ttd.name : null}
              error={errors.foto_ttd_size}
              getValue={function (value) {
                setFieldValue("foto_ttd", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("foto_ttd_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/**
             * Logo Perusahaan
             */}
            {values.logo_perusahaan ? (
              <>
                <Image
                  source={{ uri: values.logo_perusahaan.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Logo Perusahaan"
              placeholder={
                values.logo_perusahaan ? values.logo_perusahaan.name : null
              }
              error={errors.logo_perusahaan_size}
              getValue={function (value) {
                setFieldValue("logo_perusahaan", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("logo_perusahaan_size", value.fileSize);
              }}
            />
            <Space height={10} />

            {/** ========== PMKU ========== */}
            <Paragraph>Data PMKU : </Paragraph>
            {/**
             * Nomor PMKU
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.no_pmku}
              onChangeText={handleChange("no_pmku")}
              onBlur={handleBlur("no_pmku")}
              placeholder="No. PMKU"
              keyboardType="number-pad"
            />
            {errors.no_pmku && touched.no_pmku && (
              <ErrorText>{errors.no_pmku}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Tanggal PMKU
             */}
            <DateTime
              onChangeValue={function (value: Date) {
                setFieldValue(
                  "tgl_pmku",
                  `${value.getFullYear()}-${value.getMonth()}-${value.getDate()}`
                );
              }}
              label="Tanggal PMKU"
            />
            {errors.tgl_pmku && touched.tgl_pmku && (
              <ErrorText>{errors.tgl_pmku}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nama Bank
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_bank}
              onChangeText={handleChange("nama_bank")}
              onBlur={handleBlur("nama_bank")}
              placeholder="Nama Bank"
            />
            {errors.nama_bank && touched.nama_bank && (
              <ErrorText>{errors.nama_bank}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor Rekening
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nomor_rekening}
              onChangeText={handleChange("nomor_rekening")}
              onBlur={handleBlur("nomor_rekening")}
              placeholder="No. Rekening"
              keyboardType="number-pad"
            />
            {errors.nomor_rekening && touched.nomor_rekening && (
              <ErrorText>{errors.nomor_rekening}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Atas Nama
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.atas_nama}
              onChangeText={handleChange("atas_nama")}
              onBlur={handleBlur("atas_nama")}
              placeholder="Atas Nama"
            />
            {errors.atas_nama && touched.atas_nama && (
              <ErrorText>{errors.atas_nama}</ErrorText>
            )}
            <Space height={10} />

            {/** ============== Kontrak ============ */}
            <Paragraph>Kontrak : </Paragraph>
            {/**
             * Nomor Kontrak
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nomor_kontrak}
              onChangeText={handleChange("nomor_kontrak")}
              onBlur={handleBlur("nomor_kontrak")}
              placeholder="No. Kontrak"
              keyboardType="number-pad"
            />
            {errors.nomor_kontrak && touched.nomor_kontrak && (
              <ErrorText>{errors.nomor_kontrak}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Tanggal Awal Kontrak
             */}
            <DateTime
              onChangeValue={function (value: Date) {
                setFieldValue(
                  "tanggal_awal_kontrak",
                  `${value.getFullYear()}-${value.getMonth()}-${value.getDate()}`
                );
              }}
              label="Tanggal Awal Kontrak"
            />
            {errors.tanggal_awal_kontrak && touched.tanggal_awal_kontrak && (
              <ErrorText>{errors.tanggal_awal_kontrak}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Tanggal Habis Kontrak
             */}
            <DateTime
              onChangeValue={function (value: Date) {
                setFieldValue(
                  "tanggal_habis_kontrak",
                  `${value.getFullYear()}-${value.getMonth()}-${value.getDate()}`
                );
              }}
              label="Tanggal Habis Kontrak"
            />
            {errors.tanggal_habis_kontrak && touched.tanggal_habis_kontrak && (
              <ErrorText>{errors.tanggal_habis_kontrak}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Addendum Kontrak
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.addendum_kontrak}
              onChangeText={handleChange("addendum_kontrak")}
              onBlur={handleBlur("addendum_kontrak")}
              placeholder="Addendum Kontrak"
              keyboardType="number-pad"
            />
            {errors.addendum_kontrak && touched.addendum_kontrak && (
              <ErrorText>{errors.addendum_kontrak}</ErrorText>
            )}
            <Space height={10} />

            {/** ========== PIC ========== */}
            <Paragraph>Data PIC : </Paragraph>
            {/**
             * Nama PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_pic}
              onChangeText={handleChange("nama_pic")}
              onBlur={handleBlur("nama_pic")}
              placeholder="Nama PIC"
            />
            {errors.nama_pic && touched.nama_pic && (
              <ErrorText>{errors.nama_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Email PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.email_pic}
              onChangeText={handleChange("email_pic")}
              onBlur={handleBlur("email_pic")}
              placeholder="Email PIC"
              keyboardType="email-address"
            />
            {errors.email_pic && touched.email_pic && (
              <ErrorText>{errors.email_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * HP PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.hp_pic}
              onChangeText={handleChange("hp_pic")}
              onBlur={handleBlur("hp_pic")}
              placeholder="No. HP PIC"
              keyboardType="number-pad"
            />
            {errors.hp_pic && touched.hp_pic && (
              <ErrorText>{errors.hp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Telp PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_pic}
              onChangeText={handleChange("telp_pic")}
              onBlur={handleBlur("telp_pic")}
              placeholder="No. Telp. PIC"
              keyboardType="number-pad"
            />
            {errors.telp_pic && touched.telp_pic && (
              <ErrorText>{errors.telp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Fax PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax_pic}
              onChangeText={handleChange("fax_pic")}
              onBlur={handleBlur("fax_pic")}
              placeholder="No. Fax. PIC"
              keyboardType="numeric"
            />
            {errors.fax_pic && touched.fax_pic && (
              <ErrorText>{errors.fax_pic}</ErrorText>
            )}
            <Space height={10} />

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.gray1 }}
              onPress={handleSubmit}
              loading={loading}
            >
              Register
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.gray1,
    padding: 30,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    opacity: 0.8,
    borderRadius: 3,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
