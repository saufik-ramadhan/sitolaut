import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Root, Space, ErrorText, FileInput, TextInput, SelectInput2 } from "../../components";
import { Button, Paragraph, Caption } from "react-native-paper";
import { Picker } from "@react-native-community/picker";
import Colors from "../../config/Colors";
import { RegisterStackProps } from "../Navigator";
import { Formik } from "formik";
import * as Yup from "yup";
import { LIST_PROVINSI, BASE_URL } from "./../../config/constants";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { kotaFetchByProvinsi } from "../../redux/kotaReducer";
import { shipperAdd } from "../../redux/shipperReducer";
import TextInputMask from "react-native-text-input-mask";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  usery_type_id: Yup.number()
    .moreThan(0, "Pilih User !")
    .lessThan(7, "User not found"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
  password: Yup.string().required("Password Empty").min(6),
  nama_perusahaan: Yup.string().required("Required").min(6),
  alamat: Yup.string().required("Required"),
  fax: Yup.number().required("Required"),
  telp: Yup.number().required("Required"),
  siup: Yup.number().required("Required"),
  siup_doc: Yup.string().required("Required"),
  npwp: Yup.number().required("Required"),
  npwp_doc: Yup.string().required("Required"),
  nama_pic: Yup.string().required("Required"),
  email_pic: Yup.string().email("Invalid email").required("Email Empty"),
  hp_pic: Yup.number().required("Required"),
  telp_pic: Yup.number().required("Required"),
  fax_pic: Yup.number().required("Required"),
  provinsi_id: Yup.string().required(),
  kota_id: Yup.string().required(),

  nama_ttd: Yup.string().required(),
  nama_officer1: Yup.string().required(),
  telp_officer1: Yup.string().required(),
  nama_officer2: Yup.string().required(),
  telp_officer2: Yup.string().required(),
  nama_officer3: Yup.string().required(),
  telp_officer3: Yup.string().required(),
});
/**
 * MAIN SCREEN
 */
export default function RegisterShipper({
  route,
}: RegisterStackProps<"Register">) {
  const dispatch = useDispatch();
  const usertype = route.params ? route.params.usery_type_id : 0;
  const loading = useSelector(function (state: RootState) {
    return state.shipper.shipperAddLoading;
  });
  const listProvinsi = useSelector(function (state: RootState) {
    return state.provinsi.provinsiGetSuccess
      ? state.provinsi.provinsiGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  const listKota = useSelector(function (state: RootState) {
    return state.kota.kotaGetSuccess
      ? state.kota.kotaGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  React.useEffect(() => {
    dispatch(provinsiFetchAll());
  }, []);

  return (
    <Root style={styles.container}>
      <Formik
        validationSchema={RegisterSchema}
        initialValues={{
          nama_perusahaan: "",
          email: route.params ? route.params.email : "",
          usery_type_id: route.params ? route.params.usery_type_id : 0,
          password: route.params ? route.params.password : "",
          alamat: "",
          provinsi_id: 11,
          kota_id: "",
          fax: "",
          telp: "",
          siup: "",
          siup_doc: "",
          siup_doc_size: "",
          npwp: "",
          npwp_doc: "",
          npwp_doc_size: "",

          nama_ttd: "",
          foto_ttd: "",
          foto_ttd_size: "",
          surat_rekomendasi: "",
          surat_rekomendasi_size: "",
          logo_perusahaan: "",
          logo_perusahaan_size: "",
          jenis_user: "",
          status_perusahaan: "",

          nama_officer1: "",
          telp_officer1: "",
          identify_officer1: "",
          identify_officerdoc1: "",
          identify_officerdoc1_size: "",

          nama_officer2: "",
          telp_officer2: "",
          identify_officer2: "",
          identify_officerdoc2: "",
          identify_officerdoc2_size: "",

          nama_officer3: "",
          telp_officer3: "",
          identify_officer3: "",
          identify_officerdoc3: "",
          identify_officerdoc3_size: "",

          nama_pic: "",
          email_pic: "",
          hp_pic: "",
          telp_pic: "",
          fax_pic: "",
        }}
        onSubmit={function (values) {
          dispatch(shipperAdd(values));
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Perusahaan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_perusahaan}
              error={errors.nama_perusahaan}
              onChangeText={handleChange("nama_perusahaan")}
              onBlur={handleBlur("nama_perusahaan")}
              label="Nama Perusahaan"
            />
            {errors.nama_perusahaan && touched.nama_perusahaan && (
              <ErrorText>{errors.nama_perusahaan}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.alamat}
              error={errors.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              label="Alamat"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Provinsi ID
             */}

            <SelectInput2
              items={listProvinsi}
              mode="dropdown"
              itemLabel="label"
              itemValue="id"
              selectedValue={values.provinsi_id}
              error={errors.provinsi_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('provinsi_id', itemValue)
                dispatch(kotaFetchByProvinsi(itemValue))
              }}
              label="Provinsi"
            />
            <Space height={10} />

            {/**
             * Kota ID
             */}
            <SelectInput2
              items={listKota}
              mode="dropdown"
              itemLabel="label"
              itemValue="kota_id"
              selectedValue={values.kota_id}
              error={errors.kota_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('kota_id', itemValue)
              }}
              label="Kota"
            />
            <Space height={10} />

            {/**
             * Nomor Fax
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax}
              error={errors.fax}
              onChangeText={handleChange("fax")}
              onBlur={handleBlur("fax")}
              label="No. Fax"
              keyboardType="number-pad"
            />
            {errors.fax && touched.fax && <ErrorText>{errors.fax}</ErrorText>}
            <Space height={10} />

            {/**
             * Nomor Telepon
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp}
              error={errors.telp}
              onChangeText={handleChange("telp")}
              onBlur={handleBlur("telp")}
              label="No. Telp"
              keyboardType="number-pad"
            />
            {errors.telp && touched.telp && (
              <ErrorText>{errors.telp}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nomor SIUP
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.siup}
              error={errors.siup}
              onChangeText={handleChange("siup")}
              onBlur={handleBlur("siup")}
              label="No. SIUP"
              keyboardType="number-pad"
            />
            {errors.siup && touched.siup && (
              <ErrorText>{errors.siup}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen SIUP
             */}
            {values.siup_doc ? (
              <>
                <Image
                  source={{ uri: values.siup_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Dokumen SIUP"
              placeholder={
                values.siup_doc ? values.siup_doc.name : "Upload File siup"
              }
              error={errors.siup_doc_size}
              getValue={function (value) {
                setFieldValue("siup_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("siup_doc_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Nomor NPWP
             */}
            <View
              style={{
                backgroundColor: Colors.grayL,
                borderRadius: 3,
                paddingHorizontal: 5,
              }}
            >
              <TextInputMask
                onChangeText={(formatted, extracted) => {
                  setFieldValue("npwp", formatted); // +1 (123) 456-78-90
                }}
                mask={"[00].[000].[000].[0]-[000].[000]"}
                placeholder={"No. NPWP"}
              />
            </View>
            {errors.npwp && touched.npwp && (
              <ErrorText>{errors.npwp}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dokumen NPWP
             */}
            {values.npwp_doc ? (
              <>
                <Image
                  source={{ uri: values.npwp_doc.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Dokumen NPWP"
              placeholder={
                values.npwp_doc ? values.npwp_doc.name : "Upload File npwp"
              }
              error={errors.npwp_doc_size}
              getValue={function (value) {
                setFieldValue("npwp_doc", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("npwp_doc_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/** ========== PENANDA TANGAN ========== */}
            {/**
             * Nama Penanda Tangan
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_ttd}
              error={errors.nama_ttd}
              onChangeText={handleChange("nama_ttd")}
              onBlur={handleBlur("nama_ttd")}
              label="Nama Penanda Tangan"
            />
            {errors.nama_ttd && touched.nama_ttd && (
              <ErrorText>{errors.nama_ttd}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Foto Tanda Tangan
             */}
            <FileInput
              title="File Tanda Tangan"
              placeholder={values.foto_ttd ? values.foto_ttd.name : "..."}
              error={errors.foto_ttd_size}
              getValue={function (value) {
                setFieldValue("foto_ttd", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("foto_ttd_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Jenis Perusahaan
             */}
            <SelectInput2
              items={[
                {label: "BUMN", value: "BUMN"},
                {label: "Swasta", value: "Swasta"}
              ]}
              mode="dropdown"
              itemLabel="label"
              itemValue="value"
              selectedValue={values.jenis_user}
              error={errors.jenis_user}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('jenis_user', itemValue)
              }}
              label="Jenis Perusahaan"
            />
            {/* <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.jenis_user}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("jenis_user", itemValue);
                }}
              >
                <Picker.Item label="Jenis Perusahaan" value={0} />
                <Picker.Item label="BUMN" value="BUMN" />
                <Picker.Item label="Swasta" value="Swasta" />
              </Picker>
            </View> */}
            {/* {errors.jenis_user && touched.jenis_user && (
              <ErrorText>{errors.jenis_user}</ErrorText>
            )} */}

            {/**
             * Status Perusahaan
             */}
             <SelectInput2
              items={[
                {label: "Pusat", value: "Pusat"},
                {label: "Cabang", value: "Cabang"}
              ]}
              mode="dropdown"
              itemLabel="label"
              itemValue="value"
              selectedValue={values.status_perusahaan}
              error={errors.status_perusahaan}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('status_perusahaan', itemValue)
              }}
              label="Status Perusahaan"
            />
            {/* <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.status_perusahaan}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("status_perusahaan", itemValue);
                }}
              >
                <Picker.Item label="Status Perusahaan" value={0} />
                <Picker.Item label="Pusat" value="Pusat" />
                <Picker.Item label="Cabang" value="Cabang" />
              </Picker>
            </View>
            {errors.status_perusahaan && touched.status_perusahaan && (
              <ErrorText>{errors.status_perusahaan}</ErrorText>
            )}
            <Space height={10} /> */}

            {/**
             * Surat Rekomendasi
             */}
            {values.surat_rekomendasi ? (
              <>
                <Image
                  source={{ uri: values.surat_rekomendasi.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}

            <FileInput
              title="Surat Rekomendasi"
              placeholder={
                values.surat_rekomendasi ? values.surat_rekomendasi.name : null
              }
              error={errors.surat_rekomendasi_size}
              getValue={function (value) {
                setFieldValue("surat_rekomendasi", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("surat_rekomendasi_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/**
             * Logo Perusahaan
             */}
            {values.logo_perusahaan ? (
              <>
                <Image
                  source={{ uri: values.logo_perusahaan.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Logo Perusahaan"
              placeholder={
                values.logo_perusahaan ? values.logo_perusahaan.name : null
              }
              error={errors.logo_perusahaan_size}
              getValue={function (value) {
                setFieldValue("logo_perusahaan", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("logo_perusahaan_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/** ========== OFFICER 1 ========== */}
            <Caption style={styles.section}>Officer 1 : </Caption>
            {/**
             * Nama Officer 1
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_officer1}
              error={errors.nama_officer1}
              onChangeText={handleChange("nama_officer1")}
              onBlur={handleBlur("nama_officer1")}
              label="Nama Officer 1"
            />
            {errors.nama_officer1 && touched.nama_officer1 && (
              <ErrorText>{errors.nama_officer1}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * No. Telp Officer 1
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_officer1}
              onChangeText={handleChange("telp_officer1")}
              onBlur={handleBlur("telp_officer1")}
              label="No. Telp Officer 1"
              keyboardType="number-pad"
            />
            {errors.telp_officer1 && touched.telp_officer1 && (
              <ErrorText>{errors.telp_officer1}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Kartu Identitas Officer 1
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.identify_officer1}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("identify_officer1", itemValue);
                }}
              >
                <Picker.Item label="Pilih Identitas" value={0} />
                <Picker.Item label="KTP" value="KTP" />
                <Picker.Item label="SIM" value="SIM" />
                <Picker.Item
                  label="Kartu Identitas Lain"
                  value="Kartu Identitas Lain"
                />
              </Picker>
            </View>
            {errors.identify_officer1 && touched.identify_officer1 && (
              <ErrorText>{errors.identify_officer1}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Upload Kartu Identitas Officer 1
             */}
            {values.identify_officerdoc1 ? (
              <>
                <Image
                  source={{ uri: values.identify_officerdoc1.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Upload Identitas"
              placeholder={
                values.identify_officerdoc1
                  ? values.identify_officerdoc1.name
                  : null
              }
              error={errors.identify_officerdoc1_size}
              getValue={function (value) {
                setFieldValue("identify_officerdoc1", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("identify_officerdoc1_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/** ========== OFFICER 2 ========== */}
            <Caption style={styles.section}>Officer 2 : </Caption>
            {/**
             * Nama Officer 2
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_officer2}
              onChangeText={handleChange("nama_officer2")}
              onBlur={handleBlur("nama_officer2")}
              label="Nama Officer 2"
            />
            {errors.nama_officer2 && touched.nama_officer2 && (
              <ErrorText>{errors.nama_officer2}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * No. Telp Officer 2
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_officer2}
              onChangeText={handleChange("telp_officer2")}
              onBlur={handleBlur("telp_officer2")}
              label="No. Telp Officer 2"
              keyboardType="number-pad"
            />
            {errors.telp_officer2 && touched.telp_officer2 && (
              <ErrorText>{errors.telp_officer2}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Kartu Identitas Officer 2
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.identify_officer2}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("identify_officer2", itemValue);
                }}
              >
                <Picker.Item label="Pilih Identitas" value={0} />
                <Picker.Item label="KTP" value="KTP" />
                <Picker.Item label="SIM" value="SIM" />
                <Picker.Item
                  label="Kartu Identitas Lain"
                  value="Kartu Identitas Lain"
                />
              </Picker>
            </View>
            {errors.identify_officer2 && touched.identify_officer2 && (
              <ErrorText>{errors.identify_officer2}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Upload Kartu Identitas Officer 2
             */}
            {values.identify_officerdoc2 ? (
              <>
                <Image
                  source={{ uri: values.identify_officerdoc2.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Upload Identitas"
              placeholder={
                values.identify_officerdoc2
                  ? values.identify_officerdoc2.name
                  : null
              }
              error={errors.identify_officerdoc2_size}
              getValue={function (value) {
                setFieldValue("identify_officerdoc2", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("identify_officerdoc2_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/** ========== OFFICER 3 ========== */}
            <Caption style={styles.section}>Officer 3 : </Caption>
            {/**
             * Nama Officer 3
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_officer3}
              onChangeText={handleChange("nama_officer3")}
              onBlur={handleBlur("nama_officer3")}
              label="Nama Officer 3"
            />
            {errors.nama_officer3 && touched.nama_officer3 && (
              <ErrorText>{errors.nama_officer3}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * No. Telp Officer 3
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_officer3}
              onChangeText={handleChange("telp_officer3")}
              onBlur={handleBlur("telp_officer3")}
              label="No. Telp Officer 3"
              keyboardType="number-pad"
            />
            {errors.telp_officer3 && touched.telp_officer3 && (
              <ErrorText>{errors.telp_officer3}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Kartu Identitas Officer 3
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.identify_officer3}
                style={{ flex: 10 }}
                itemStyle={{ borderRadius: 5 }}
                mode="dropdown"
                onValueChange={function (itemValue: number) {
                  setFieldValue("identify_officer3", itemValue);
                }}
              >
                <Picker.Item label="Pilih Identitas" value={0} />
                <Picker.Item label="KTP" value="KTP" />
                <Picker.Item label="SIM" value="SIM" />
                <Picker.Item
                  label="Kartu Identitas Lain"
                  value="Kartu Identitas Lain"
                />
              </Picker>
            </View>
            {errors.identify_officer3 && touched.identify_officer3 && (
              <ErrorText>{errors.identify_officer3}</ErrorText>
            )}
            <Space height={10} />
            {/**
             * Upload Kartu Identitas Officer 3
             */}
            {values.identify_officerdoc3 ? (
              <>
                <Image
                  source={{ uri: values.identify_officerdoc3.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Upload Identitas"
              placeholder={
                values.identify_officerdoc3
                  ? values.identify_officerdoc3.name
                  : null
              }
              error={errors.identify_officerdoc3_size}
              getValue={function (value) {
                setFieldValue("identify_officerdoc3", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("identify_officerdoc3_size", value.fileSize);
              }}
            />
            <Space height={5} />

            {/** ========== PIC ========== */}
            <Caption style={styles.section}>Data PIC : </Caption>
            {/**
             * Nama PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_pic}
              onChangeText={handleChange("nama_pic")}
              onBlur={handleBlur("nama_pic")}
              label="Nama PIC"
            />
            {errors.nama_pic && touched.nama_pic && (
              <ErrorText>{errors.nama_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Email PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.email_pic}
              onChangeText={handleChange("email_pic")}
              onBlur={handleBlur("email_pic")}
              label="Email PIC"
              keyboardType="email-address"
            />
            {errors.email_pic && touched.email_pic && (
              <ErrorText>{errors.email_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * HP PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.hp_pic}
              onChangeText={handleChange("hp_pic")}
              onBlur={handleBlur("hp_pic")}
              label="No. HP PIC"
              keyboardType="number-pad"
            />
            {errors.hp_pic && touched.hp_pic && (
              <ErrorText>{errors.hp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Telp PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.telp_pic}
              onChangeText={handleChange("telp_pic")}
              onBlur={handleBlur("telp_pic")}
              label="No. Telp. PIC"
              keyboardType="number-pad"
            />
            {errors.telp_pic && touched.telp_pic && (
              <ErrorText>{errors.telp_pic}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Fax PIC
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.fax_pic}
              onChangeText={handleChange("fax_pic")}
              onBlur={handleBlur("fax_pic")}
              label="No. Fax. PIC"
              keyboardType="numeric"
            />
            {errors.fax_pic && touched.fax_pic && (
              <ErrorText>{errors.fax_pic}</ErrorText>
            )}
            <Space height={10} />

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.gray1 }}
              onPress={handleSubmit}
              loading={loading}
            >
              Register
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.gray1,
    padding: 30,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    opacity: 0.8,
    borderRadius: 3,
  },
  textInput: {
    paddingHorizontal: 10,
  },
  section: {
    fontSize: 15,
    marginVertical: 8,
  },
});
