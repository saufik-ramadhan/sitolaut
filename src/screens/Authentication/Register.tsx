import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Root, Space, ErrorText, Icon } from "../../components";
import {
  Paragraph,
  TextInput,
  Headline,
  Button,
  Caption,
} from "react-native-paper";
import { Picker } from "@react-native-community/picker";
import Colors from "../../config/Colors";
import { RegisterStackProps } from "../Navigator";
import { Formik } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "src/redux/rootReducer";
import { registerClearData, doRegisterCheck } from "./../../redux/authReducers";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} must be the same as ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}
Yup.addMethod(Yup.string, "equalTo", equalTo);
const RegisterSchema = Yup.object().shape({
  usery_type_id: Yup.number()
    .moreThan(0, "Pilih User !")
    .lessThan(7, "User not found"),
  email: Yup.string().email("Invalid email").required("Email Empty"),
  password: Yup.string().required("Password Empty").min(6),
  confirm_password: Yup.string()
    .equalTo(Yup.ref("password"), "Passwords must match")
    .required("Required"),
});

/**
 * MAIN SCREEN
 */
export default function Register({
  navigation,
}: RegisterStackProps<"Register">) {
  const [userRole, setUserRole] = React.useState("none");
  const loading = useSelector(function (state: RootState) {
    return state.auth.authRegisterCheckLoading;
  });
  const registerCheck = useSelector(function (state: RootState) {
    return {
      error: state.auth.authRegisterCheckError,
      data: state.auth.authRegisterCheckSuccess,
    };
  });
  const registerCheckError = useSelector(function (state: RootState) {
    return state.auth.authRegisterCheckError;
  });
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(registerClearData());
  }, []);
  return (
    <View style={{ flex: 1, backgroundColor: Colors.white }}>
      <Root style={styles.container}>
        <Icon
          name={"arrow-left"}
          style={{ fontSize: 30, color: Colors.for }}
          onPress={function () {
            navigation.goBack();
          }}
        />
        <Space height={30} />
        <Paragraph>
          Dengan membuat akun di aplikasi LCS ini anda dianggap telah menyetujui
          semua peraturan penggunaan yang telah ditetapkan oleh Kementrian
          Perhubungan Republik Indonesia
        </Paragraph>
        <Space height={30} />
        <Formik
          initialValues={{
            email: "",
            password: "",
            confirm_password: "",
            usery_type_id: 0,
          }}
          onSubmit={function (values) {
            dispatch(doRegisterCheck(values));
          }}
          validationSchema={RegisterSchema}
        >
          {({
            handleChange,
            setFieldValue,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  enabled={!(!registerCheck.error && registerCheck.data)}
                  selectedValue={userRole}
                  style={{ flex: 10 }}
                  itemStyle={{ borderRadius: 5 }}
                  mode="dropdown"
                  onValueChange={function (
                    itemValue: string,
                    itemIndex: number
                  ) {
                    setFieldValue("usery_type_id", itemIndex);
                    setUserRole(itemValue);
                  }}
                >
                  <Picker.Item label="-Pilih User-" value="none" />
                  <Picker.Item label="Consignee" value="consignee" />
                  <Picker.Item label="Supplier" value="supplier" />
                  <Picker.Item label="Shipper" value="shipper" />
                  <Picker.Item label="Reseller" value="reseller" />
                  <Picker.Item label="Operator" value="operator" />
                </Picker>
              </View>
              {errors.usery_type_id && touched.usery_type_id && (
                <ErrorText>{errors.usery_type_id}</ErrorText>
              )}
              <Space height={10} />
              <TextInput
                editable={!(!registerCheck.error && registerCheck.data)}
                style={[styles.formItem, styles.textInput]}
                value={values.email}
                onChangeText={handleChange('email')}
                onEndEditing={function(val){setFieldValue('email', val.nativeEvent.text.toLowerCase())}}
                onBlur={handleBlur("email")}
                label="Alamat Email"
                keyboardType="email-address"
              />
              {errors.email && touched.email && (
                <ErrorText>{errors.email}</ErrorText>
              )}
              <Space height={10} />
              <TextInput
                editable={!(!registerCheck.error && registerCheck.data)}
                style={[styles.formItem, styles.textInput]}
                value={values.password}
                secureTextEntry={true}
                onChangeText={handleChange("password")}
                onBlur={handleBlur("password")}
                label="Password"
                passwordRules="required:digit;required:upper;minlength:8;"
                keyboardType="ascii-capable"
              />
              {errors.password && touched.password && (
                <ErrorText>{errors.password}</ErrorText>
              )}
              <Space height={10} />
              <TextInput
                editable={!(!registerCheck.error && registerCheck.data)}
                style={[styles.formItem, styles.textInput]}
                value={values.confirm_password}
                secureTextEntry={true}
                onChangeText={handleChange("confirm_password")}
                onBlur={handleBlur("confirm_password")}
                label="Konfirmasi Password"
                passwordRules="required:digit;required:upper;minlength:8;"
                keyboardType="ascii-capable"
              />
              {errors.confirm_password && touched.confirm_password && (
                <ErrorText>{errors.confirm_password}</ErrorText>
              )}
              <Space height={20} />
              {!registerCheck.error && registerCheck.data.success ? (
                <>
                  <Paragraph style={{ textAlign: "center" }}>
                    Email tersedia klik untuk melanjutkan!
                  </Paragraph>
                  <Button
                    mode="contained"
                    color={Colors.pri}
                    labelStyle={{ color: Colors.white }}
                    style={{ borderRadius: 3 }}
                    contentStyle={{ padding: 5 }}
                    onPress={function () {
                      const form = registerCheck.data.data.usertype;
                      switch (form) {
                        case "Consignee":
                          navigation.navigate("RegisterConsignee", values);
                          break;
                        case "Supplier":
                          navigation.navigate("RegisterSupplier", values);
                          break;
                        case "Shipper":
                          navigation.navigate("RegisterShipper", values);
                          break;
                        case "Reseller":
                          navigation.navigate("RegisterReseller", values);
                          break;
                        case "Vessel Operator":
                          navigation.navigate("RegisterOperator", values);
                          break;
                        default:
                          null;
                      }
                    }}
                  >
                    Lanjutkan
                  </Button>
                  <Button
                    mode="text"
                    color={Colors.danger}
                    onPress={function () {
                      dispatch(registerClearData());
                    }}
                  >
                    Reset
                  </Button>
                </>
              ) : (
                <Button
                  mode="contained"
                  color={Colors.sec}
                  labelStyle={{ color: Colors.for }}
                  onPress={handleSubmit}
                  loading={loading}
                  style={{ borderRadius: 3 }}
                  contentStyle={{ padding: 5 }}
                >
                  DAFTARKAN
                </Button>
              )}
            </>
          )}
        </Formik>
        {registerCheckError ? (
          <Caption style={{ textAlign: "center", color: "orangered" }}>
            {registerCheckError.message}
          </Caption>
        ) : null}
      </Root>
      <Image
        source={require("../../assets/img/tollaut.png")}
        style={{
          width: "100%",
          height: 200,
          opacity: 0.3,
          position: "absolute",
          zIndex: -3,
          bottom: 0,
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  welcome: {
    color: Colors.pri,
  },
  headline: {
    color: Colors.pri,
  },
  formItem: {
    borderRadius: 3,
    backgroundColor: Colors.grayL,
    elevation: 2,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
