import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  jenisbarangAdd,
  jenisbarangFetchOne,
  jenisbarangEdit,
  jenisbarangFilterAdd,
} from "../../redux/jenisbarangReducer";
import { TextInput, Button, HelperText } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import * as Yup from 'yup';

const UpdateJenisBarangSchema = Yup.object().shape({
  id: Yup.number().required(),
  jenis_barang: Yup.string().required("Nama Barang Harus diisi").min(6),
});


export default function UpdateJenisBarang({
  navigation,
  route,
}: RegulatorStackProps<"UpdateJenisBarang">) {
  const dispatch = useDispatch();
  const { id, jenis_barang } = route.params;
  function handleUpdate(values) {
    dispatch(jenisbarangEdit(values));
  }
  return (
    <Root>
      <Formik
        validationSchema={UpdateJenisBarangSchema}
        initialValues={{
          id: id,
          jenis_barang: jenis_barang,
        }}
        onSubmit={(values) => {
          handleUpdate(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors, touched }) => (
          <>
            <TextInput
              label="Nama Barang"
              onChangeText={handleChange("jenis_barang")}
              onBlur={handleBlur("jenis_barang")}
              placeholder=""
              value={values.jenis_barang}
            />
            {errors.jenis_barang && touched.jenis_barang && (
              <HelperText type="error">{errors.jenis_barang}</HelperText>
            )}
            <Button onPress={handleSubmit}>Update</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
