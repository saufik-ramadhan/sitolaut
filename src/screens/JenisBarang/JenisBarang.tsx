import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton } from "../../components";
import { DateFormat } from "../../services/utils";
import {
  jenisbarangFilterClear,
  jenisbarangFetchPage,
  jenisbarangDelete,
  jenisbarangFilterAdd,
} from "./../../redux/jenisbarangReducer";
import { Formik } from "formik";

function FloatingAdd({ navigation }: RegulatorStackProps<"JenisBarang">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateJenisBarang");
      }}
    />
  );
}

export default function JenisBarang({
  navigation,
}: RegulatorStackProps<"JenisBarang">) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const jenisbarang = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangData
      ? state.jenisbarang.jenisbarangData
      : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangEditLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(jenisbarangFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(jenisbarangFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(
      jenisbarangDelete({
        id: id,
      })
    );
    dispatch(jenisbarangFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(jenisbarangFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={jenisbarang}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        ListHeaderComponent={
          <Formik
            initialValues={{
              jenis_barang: "",
            }}
            onSubmit={(val) => dispatch(jenisbarangFilterAdd(val))}
          >
            {({ handleChange, handleBlur, handleSubmit, values }) => (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <TextInput
                  label="Cari"
                  onChangeText={handleChange("jenis_barang")}
                  onBlur={handleBlur("jenis_barang")}
                  value={values.jenis_barang}
                  style={{ flex: 5 }}
                />
                <IconButton
                  onPress={handleSubmit}
                  icon="magnify"
                  style={{ flex: 1 }}
                />
              </View>
            )}
          </Formik>
        }
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                null;
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Paragraph style={{ flex: 3 }}>{item.jenis_barang}</Paragraph>
                <View style={{ flexDirection: "row" }}>
                  <IconButton
                    icon="circle-edit-outline"
                    color={Colors.def}
                    onPress={function () {
                      navigation.navigate("UpdateJenisBarang", {
                        jenis_barang: item.jenis_barang,
                        id: item.id,
                      });
                    }}
                  />
                  <IconButton
                    icon="delete"
                    color={Colors.danger}
                    onPress={function () {
                      Alert.alert(
                        "Anda yakin",
                        "Menghapus Jenis Barang ?",
                        [
                          {
                            text: "OK",
                            onPress: () => handleDelete(item.id),
                          },
                          {
                            text: "Cancel",
                            onPress: () => null,
                          },
                        ],
                        { cancelable: false }
                      );
                    }}
                  />
                </View>
              </View>
            </Card>
          );
        }}
      />
      <FloatingAdd navigation={navigation} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
});
