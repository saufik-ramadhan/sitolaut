import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  jenisbarangAdd,
  jenisbarangFetchOne,
  jenisbarangEdit,
  jenisbarangFilterAdd,
} from "../../redux/jenisbarangReducer";
import { TextInput, Button, HelperText } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import * as Yup from 'yup';

const CreateJenisBarangSchema = Yup.object().shape({
  jenis_barang: Yup.string().required("Nama Barang Harus diisi").min(6),
});


export default function CreateJenisBarang({
  navigation,
  route,
}: RegulatorStackProps<"CreateJenisBarang">) {
  const dispatch = useDispatch();
  function handleAdd(values) {
    dispatch(jenisbarangAdd(values));
  }
  return (
    <Root>
      <Formik
        validationSchema={CreateJenisBarangSchema}
        initialValues={{
          jenis_barang: "",
        }}
        onSubmit={(values) => {
          handleAdd(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors, touched }) => (
          <>
            <TextInput
              label="Jenis Barang"
              onChangeText={handleChange("jenis_barang")}
              onBlur={handleBlur("jenis_barang")}
              placeholder=""
              value={values.jenis_barang}
            />
            {errors.jenis_barang && touched.jenis_barang && (
              <HelperText type="error">{errors.jenis_barang}</HelperText>
            )}
            <Button onPress={handleSubmit}>Create</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
