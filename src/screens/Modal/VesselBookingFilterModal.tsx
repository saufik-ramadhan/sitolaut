import React from "react";
import { Formik } from "formik";
import { Button, TextInput } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { SelectInput, DateTime } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { DateFormatStrip } from "../../services/utils";
import { bookingFilterAdd } from "../../redux/bookingReducer";

export default function VesselBookingFilterModal({ navigation, route }) {
  const { supplier, shipper, port, msttrayek } = route.params;
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : [{}];
  });

  return (
    <Formik
      initialValues={{
        consignee_id: "",
        destination: "",
        kode_booking: "",
        operator_id: `${id}`,
        origin: "",
        shipper_id: "",
        status: "",
        supplier_id: "",
        trayek_id: "",
        voyage: "",
      }}
      onSubmit={function (values) {
        dispatch(bookingFilterAdd(values));
        navigation.goBack();
      }}
    >
      {({ setFieldValue, handleSubmit, handleBlur, handleChange, values }) => (
        <>
          {/**
           * Kode Booking
           */}
          <TextInput
            value={values.kode_booking}
            onChangeText={handleChange("kode_booking")}
            onBlur={handleBlur("kode_booking")}
            placeholder="Kode Booking"
          />

          {/**
           * Voyage
           */}
          <TextInput
            value={values.voyage}
            onChangeText={handleChange("voyage")}
            onBlur={handleBlur("voyage")}
            placeholder="Voyage"
          />

          {/**
           * Status
           */}
          <SelectInput
            label={"Status"}
            onChangeValue={(val) => setFieldValue("status", val.id)}
            options={[
              { id: 1, label: "Menunggu validasi operator" },
              { id: 2, label: "Menunggu pembayaran" },
              { id: 3, label: "Pembayaran diterima" },
              { id: 4, label: "Dibatalkan" },
              { id: 5, label: "Selesai" },
              { id: 6, label: "Sudah sampai tujuan" },
            ]}
            objectKey="label"
            withSearch
          />

          {/**
           * Trayek
           */}
          <SelectInput
            label={"Trayek"}
            onChangeValue={(val) => setFieldValue("trayek_id", val.id)}
            options={msttrayek}
            objectKey="label"
            withSearch
          />

          {/**
           * Shipper
           */}
          <SelectInput
            label={"Shipper"}
            onChangeValue={(val) => setFieldValue("shipper_id", val.id)}
            options={shipper}
            objectKey="label"
            withSearch
          />

          {/**
           * Origin
           */}
          <TextInput
            value={values.origin}
            onChangeText={handleChange("origin")}
            onBlur={handleBlur("origin")}
            placeholder="Origin"
          />

          {/**
           * Destination
           */}
          <TextInput
            value={values.destination}
            onChangeText={handleChange("destination")}
            onBlur={handleBlur("destination")}
            placeholder="Destination"
          />

          <Button mode="contained" onPress={handleSubmit}>
            Submit
          </Button>
        </>
      )}
    </Formik>
  );
}
