import React from 'react'
import { View, FlatList } from 'react-native'
import { List, Paragraph, Caption, Divider, Card } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import { detailcontainerFetchSub } from '../../redux/detailContainerReducer';
import { EmptyState, PlaceholderLoading } from '../../components';

export default function DetailContainerModal({ route, navigation }:any) {
    const { id_booking, id,title }=route.params;
  const dispatch = useDispatch();
       const detailcontainer = useSelector(function (state: RootState) {
         return state.detailContainer.detailContainerGetSuccess
           ? state.detailContainer.detailContainerGetSuccess.data
           : [{}];
       });
     const detailcontainerLoading = useSelector(function (state: RootState) {
       return state.detailContainer.detailContainerGetLoading
     });
    const fetchData = () => {
      dispatch(detailcontainerFetchSub("get_booking/" + id_booking));
    //   console.log("Detail Container==>", detailcontainer, id_booking,id);
    };

    React.useEffect(() => {
        fetchData();
        console.log(detailcontainer)
        navigation.setOptions({
          title: title,
        });
    }, []);

  
    return (
        <View style={{ backgroundColor: '#eee', flex: 1 }}>
           {!detailcontainerLoading && <FlatList data={detailcontainer.filter((item)=>item.id_container==id)}
                keyExtractor={(item, index) => String(index)}
                renderItem={({ item }) => {
                       return (
                         <Card style={{ padding: 10, margin:10 }}>
                           <List.Item
                             title={<Caption>Jenis</Caption>}
                             right={() => (
                               <Paragraph>{item.jenis_barang}</Paragraph>
                             )}
                           />
                           <Divider />
                           <List.Item
                             title={<Caption>Item</Caption>}
                             right={() => (
                               <Paragraph>{item.nama_barang}</Paragraph>
                             )}
                           />
                           <Divider />
                           <List.Item
                             title={<Caption>Deskripsi</Caption>}
                             right={() => (
                               <Paragraph>{item.deskripsi}</Paragraph>
                             )}
                           />
                           <Divider />
                           <List.Item
                             title={<Caption>Kemasan</Caption>}
                             right={() => <Paragraph>{item.kemasan}</Paragraph>}
                           />
                           <Divider />
                           <List.Item
                             title={<Caption>Jumlah Kemasan</Caption>}
                             right={() => (
                               <Paragraph>{item.jumlah_barang}</Paragraph>
                             )}
                           />
                           <Divider />
                           <List.Item
                             title={<Caption>Berat</Caption>}
                             right={() => (
                               <Paragraph>{item.berat_total}</Paragraph>
                             )}
                           />
                           <Divider />
                         </Card>
                       );
                }}
                ListEmptyComponent={EmptyState}
            />}
       <PlaceholderLoading loading={detailcontainerLoading}/>
      </View>
    );
}
