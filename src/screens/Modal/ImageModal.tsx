import React from "react";
import { StyleSheet, Text, View, Image, Dimensions } from "react-native";
import ImageZoom from "react-native-image-pan-zoom";

export default function ImageModal({ navigation, route }) {
  const url = route.params.url;
  return (
    <View style={styles.container}>
      <ImageZoom
        cropHeight={Dimensions.get("window").height}
        cropWidth={Dimensions.get("window").width}
        imageWidth={Dimensions.get("window").width}
        imageHeight={Dimensions.get("window").height}
      >
        <Image
          style={styles.image}
          source={{ uri: url }}
          resizeMode="contain"
        />
      </ImageZoom>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: "100%",
    height: "75%",
  },
});
