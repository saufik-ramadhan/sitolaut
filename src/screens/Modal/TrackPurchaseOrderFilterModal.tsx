import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import { Formik } from "formik";
import { Button, TextInput, Card } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import Colors from "../../config/Colors";
import { SelectInput, Space } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { purchaseFetchPage } from "../../redux/purchaseReducer";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { portFetchAll } from "../../redux/portReducer";
import { scheduleFetchWill } from "../../redux/scheduleReducer";
import { trackFetchPage, trackFilterAdd } from "../../redux/trackReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";

export default function TrackPurchaseOrderFilterModal({ navigation }) {
  const { consignee, supplier } = useSelector((state: RootState) => state);
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(supplierFetchAll());
    dispatch(consigneeFetchAll());
  }, []);
  if(!(consignee.consigneeGetSuccess.data || supplier.supplierGetSuccess.data)) return null;
  return (
    <Formik
      initialValues={{
        consignee_id: "",
        destination: "",
        jpt: "",
        no_po: "",
        origin: "",
        supplier_id: 0,
      }}
      onSubmit={function (values) {
        // alert(JSON.stringify(values));
        // dispatch(trackFetchPage(values));
        dispatch(trackFilterAdd(values));
        navigation.goBack();
      }}
    >
      {({ setFieldValue, handleSubmit, handleBlur, handleChange, values }) => (
        <Card style={{ padding: 10, margin: 10, borderRadius: 5 }}>
          {/**
           * No PO
           */}
          <TextInput
            dense
            value={values.no_po}
            onChangeText={handleChange("no_po")}
            onBlur={handleBlur("no_po")}
            label="No PO"
            mode="outlined"
          />
          <Space height={10} />
          {/**
           * Supplier / Consignee
           */}
           {
             usertype == "Consignee" ? 
              <SelectInput
                label="Supplier"
                onChangeValue={(val) => setFieldValue("supplier_id", val.id)}
                options={supplier.supplierGetSuccess.data}
                objectKey="label"
                withSearch
              /> :
              <SelectInput
                label="Consignee"
                onChangeValue={(val) => setFieldValue("consignee_id", val.id)}
                options={consignee.consigneeGetSuccess.data}
                objectKey="label"
                withSearch
              />
           }
          <Space height={10} />

          {/**
           * JPT
           */}
          <TextInput
            dense
            value={values.jpt}
            onChangeText={handleChange("jpt")}
            onBlur={handleBlur("jpt")}
            label="JPT"
            mode="outlined"
          />
          <Space height={10} />

          {/**
           * Pelabuhan Asal
           */}
          <TextInput
            dense
            value={values.origin}
            onChangeText={handleChange("origin")}
            onBlur={handleBlur("origin")}
            label="Pelabuhan Asal"
            mode="outlined"
          />
          <Space height={10} />

          {/**
           * Pelabuhan Asal
           */}
          <TextInput
            dense
            value={values.destination}
            onChangeText={handleChange("destination")}
            onBlur={handleBlur("destination")}
            label="Pelabuhan Asal"
            mode="outlined"
          />
          <Space height={10} />

          <Button mode="contained" onPress={handleSubmit}>
            Filter
          </Button>
        </Card>
      )}
    </Formik>
  );
}
