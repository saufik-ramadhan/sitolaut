import React, { Fragment } from 'react'
import { Card, TextInput, Button } from 'react-native-paper';
import { Space, PickerCostum, SelectInput } from '../../components';
import * as yup from 'yup'
import { Formik } from 'formik'
import { msttrayekFetchAll } from '../../redux/msttrayekReducer';
import { useDispatch, useSelector } from 'react-redux';
import { getUserType } from '../../services/utils';
import { bookingFetchPage } from '../../redux/bookingReducer';
import { RootState } from '../../redux/rootReducer';
import { shipperFetchAll } from '../../redux/shipperReducer';


export default function SearchBookingModal(props: any) {
    const { navigation, route } = props
    const dispatch = useDispatch()
    const onClose = ()=> {navigation.goBack()}
    const { id ,usertype} = useSelector(function (state: RootState) {
      return state.auth.authLoginSuccess
        ? state.auth.authLoginSuccess.data.user[0]
        : {
            id: 0,
            usertype: "Not Defined",
          };
    });
    const { shipper } = useSelector((state: RootState) => state);
    const msttrayek = useSelector(function (state: RootState) {
      return state.msttrayek.msttrayekGetSuccess
        ? state.msttrayek.msttrayekGetSuccess.data
        : [
            {
              data: [
                {
                  id: "",
                  kode: "",
                  mst_trayek_id: "",
                  pel_asal: "",
                  pel_sampai: "",
                  port_destination_id: "",
                  port_origin_id: "",
                  users_id: "",
                },
              ],
            },
          ];
    });

    const getLoading = useSelector(function (state: RootState) {
      return state.msttrayek.msttrayekGetLoading;
    });

    const fetchData = () => {
      dispatch(msttrayekFetchAll());
      dispatch(shipperFetchAll());

  };
  const reset = () => {
     const searchParams = {
       destination: '',
       kode_booking: '',
       length: 10,
       origin: '',
       shipper_id: isShipper ? id : "",
       start: 0,
       status: '',
       operator_id: isOperator ? id : "",
       trayek_id: '',
       voyage: '',
    };
    dispatch(bookingFetchPage(searchParams));
    onClose()
  }
    const {isShipper,isOperator} = getUserType(usertype)
    React.useEffect(() => {
        fetchData();
    },[])
    return (
            <Formik
                initialValues={{
                    kode_booking: '',
                    trayek: '',
                    voyage: '',
                    status: '',
                    origin: '',
                    destination: '',
                    shipper_id: isShipper ? id : "",
                    operator_id: isOperator ? id : "",
                }}
                onSubmit={(values:any)=>{
                    const searchParams = {
                      destination: values.destination,
                      kode_booking: values.kode_booking,
                      length: 10,
                      origin: values.origin,
                      start: 0,
                      status: values.status,
                      trayek_id: values.trayek,
                      voyage: values.voyage,
                      shipper_id: values.shipper_id,
                      operator_id: values.operator_id,
                    };
                    console.log(searchParams)
                    dispatch(bookingFetchPage(searchParams))
                    onClose();
                }}
            >
              { (formikProps:any) => {return(<Card>
                <Card.Content>
                    
                <TextInput 
                onChangeText={formikProps.handleChange('kode_booking')}
                label="Kode Booking" dense />
                <Space height={5} />
                <PickerCostum initLabel="--Trayek--" 
                onValueChange={(value:any)=>{formikProps.setFieldValue('trayek',value.id)
                }}
                data={msttrayek} objectKey='label'/>
                <Space height={5} />
                <TextInput 
                    label="Voyage"
                     onChangeText={formikProps.handleChange('voyage')}
                    dense />
                <Space height={5} />
                <PickerCostum initLabel="--Status--" 
                    onValueChange={(value:any)=>{formikProps.setFieldValue('status',value.id)
                }}
                 data={dataStatus} objectKey='label' />
                <Space height={5} />
                <TextInput label="Origin" dense
                onChangeText={formikProps.handleChange('origin')} />
                <Space height={5} />
                <TextInput label="Destination"
                onChangeText={formikProps.handleChange('destination')}
                dense />
                <Space height={5} />
                { 
                  usertype == "Vessel Operator" || usertype == "Regulator" && (
                    <SelectInput options={shipper.shipperGetSuccess.data} objectKey="label" onChangeValue={function(val){formikProps.setFieldValue('shipper_id', val.id)}} label="Shipper"/>
                  )
                }
                <Space height={5} />
                <Button onPress={formikProps.handleSubmit} icon="magnify" mode="contained">
                  Cari
                </Button>
                <Button mode='outlined'
                onPress={reset}
                icon="close">Reset</Button>
            </Card.Content>
            </Card>)
            }}
            
    </Formik>
    );
}


const dataStatus = [
  {
    id: 1,
    label: "Menunggu validasi operator",
  },
  {
    id: 2,
    label: "Menunggu Pembayaran",
  },
  {
    id: 3,
    label: "Pembayaran diterima",
  },
  {
    id: 4,
    label: "Dibatalkan"  },
  {
    id: 5,
    label: "Selesai",
  },
  {
    id: 6,
    label: "Sudah sampai tujuan",
  },
];