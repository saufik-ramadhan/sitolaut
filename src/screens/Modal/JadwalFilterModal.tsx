import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import { Formik } from "formik";
import { Button, Card } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import Colors from "../../config/Colors";
import { SelectInput, Space } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { purchaseFetchPage } from "../../redux/purchaseReducer";
import { operatorFetchAll } from "../../redux/operatorReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { portFetchAll } from "../../redux/portReducer";
import {
  scheduleFetchWill,
  scheduleFilterAdd,
} from "../../redux/scheduleReducer";

export default function JadwalFilterModal({ navigation }) {
  const operator = useSelector(function (state: RootState) {
    return state.operator.operatorGetSuccess
      ? state.operator.operatorGetSuccess.data
      : [];
  });
  const msttrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetSuccess
      ? state.msttrayek.msttrayekGetSuccess.data
      : [];
  });
  const pelabuhan = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(operatorFetchAll());
    dispatch(msttrayekFetchAll());
    dispatch(portFetchAll());
  }, []);
  return (
    <Formik
      initialValues={{
        length: "",
        operator_id: "",
        port_destination_id: "",
        port_origin_id: "",
        trayek: "",
      }}
      onSubmit={function (values) {
        // alert(JSON.stringify(values));
        // dispatch(scheduleFetchWill(values));
        // console.log(values);
        dispatch(scheduleFilterAdd(values));
        navigation.goBack();
      }}
    >
      {({ setFieldValue, handleSubmit }) => (
        <Card style={{ padding: 10, margin: 10, borderRadius: 5 }}>
          {/**
           * Operator
           */}
          <SelectInput
            label="Operator"
            onChangeValue={(val) => setFieldValue("operator_id", val.id)}
            options={operator}
            objectKey="label"
            withSearch
          />

          {/**
           * Trayek
           */}
          <SelectInput
            label="Trayek"
            onChangeValue={(val) => setFieldValue("trayek", val.id)}
            options={msttrayek}
            objectKey="label"
            withSearch
          />

          {/**
           * Origin
           */}
          <SelectInput
            label="Origin"
            onChangeValue={(val) => setFieldValue("port_origin_id", val.id)}
            options={pelabuhan}
            objectKey="label"
            withSearch
          />

          {/**
           * Destination
           */}
          <SelectInput
            label="Destination"
            onChangeValue={(val) =>
              setFieldValue("port_destination_id", val.id)
            }
            options={pelabuhan}
            objectKey="label"
            withSearch
          />
          <Space height={10} />
          <Button mode="contained" onPress={handleSubmit}>
            Submit
          </Button>
        </Card>
      )}
    </Formik>
  );
}
