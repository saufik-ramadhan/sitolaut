import React from "react";
import { Formik } from "formik";
import { Button, TextInput, Card } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { SelectInput, DateTime, Space } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { purchaseFilterAdd } from "../../redux/purchaseReducer";
import { getUserType, DateFormatStrip } from "../../services/utils";

export default function FilterPurchaseOrder({ navigation, route }) {
  const supplier = route.params.supplier;
  const consignee = route.params.consignee;
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : [{}];
  });

  const { isShipper, isSupplier, isConsignee, isReseller } = getUserType(
    usertype
  );
  const pendings = isShipper ? "1" : "0";
  const isApprove = isShipper ? "1" : "2";
  const isApproveByShipper = isShipper ? "2" : "0";
  const PickerDefaultValue = isConsignee ? "Supplier" : "Consignee";
  const pickerItem = isConsignee ? supplier : consignee;

  const dispatch = useDispatch();

  return (
    <Formik
      initialValues={{
        dari: "",
        is_approve: "",
        is_approve_by_shipper: "",
        length: 10,
        no_po: "",
        sampai: "",
        consignee_id: isConsignee ? id : "",
        supplier_id: isSupplier ? id : "",
        shipper_id: isShipper ? id : "",
        reseller_id: isReseller ? id : "",
        start: 0,
      }}
      onSubmit={function (values) {
        // dispatch(trackFilterAdd(values));
        // alert(JSON.stringify(values));
        dispatch(purchaseFilterAdd(values));
        navigation.goBack();
      }}
    >
      {({ setFieldValue, handleSubmit, handleBlur, handleChange, values }) => (
        <Card
          style={{ elevation: 3, margin: 10, padding: 10, borderRadius: 5 }}
        >
          {/**
           * No PO
           */}
          <TextInput
            value={values.no_po}
            mode="outlined"
            label="Nomor Po"
            onChangeText={handleChange("no_po")}
            onBlur={handleBlur("no_po")}
            dense
          />
          <Space height={10}/>
          {/**
           * Status
           */}
          <SelectInput
            label="Status"
            onChangeValue={(val) => {
              switch (val.id) {
                case 0:
                  setFieldValue("is_approve", pendings);
                  setFieldValue("is_approve_by_shipper", "0");
                  break;
                case 1:
                  setFieldValue("is_approve", "1");
                  setFieldValue("is_approve_by_shipper", "1");
                  break;
                case 2:
                  setFieldValue("is_approve", isApprove);
                  setFieldValue("is_approve_by_shipper", isApproveByShipper);
                  break;
                default:
                  setFieldValue("is_approve", "");
                  setFieldValue("is_approve_by_shipper", "");
              }
            }}
            options={[
              { id: 0, label: "Menunggu" },
              { id: 1, label: "Diterima" },
              { id: 2, label: "Ditolak" },
            ]}
            objectKey="label"
          />

          {/**
           * Supplier / Consignee
           */}
          <SelectInput
            label={PickerDefaultValue}
            onChangeValue={(val) => setFieldValue("supplier_id", val.id)}
            options={pickerItem}
            objectKey="label"
            withSearch
          />
          <Space height={10} />
          {/**
           * Dari
           */}

          <DateTime
            label="Dari Tanggal"
            onChangeValue={(date) =>
              setFieldValue("dari", DateFormatStrip(date))
            }
          />
          <Space height={10} />
          {/**
           * Sampai
           */}
          <DateTime
            label="Sampai Tanggal"
            onChangeValue={(date) =>
              setFieldValue("sampai", DateFormatStrip(date))
            }
          />
          <Space height={10} />
          <Button mode="contained" onPress={handleSubmit}>
            Submit
          </Button>
        </Card>
      )}
    </Formik>
  );
}
