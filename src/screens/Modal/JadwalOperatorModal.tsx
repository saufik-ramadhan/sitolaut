import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
} from "react-native";
import {
  Card,
  Paragraph,
  Caption,
  Button,
  IconButton,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { useDispatch, useSelector } from "react-redux";
import {
  scheduleFetchWill,
  scheduleFetchOne,
  scheduleUpdate,
} from "../../redux/scheduleReducer";
import { RootState } from "../../redux/rootReducer";
import { MonthsID } from "../../config/Months";
import { Icon, Space, DateTime, Root } from "../../components";
import { OperatorStackProps } from "../Navigator";
import { DateFormat, TimeFormat, DateFormatStrip } from "../../services/utils";
import { Formik } from "formik";

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

export default function JadwalOperatorModal({ navigation, route }: any) {
  const dispatch = useDispatch();
  const jadwal_id = route.params.id;
  const [show, setShow] = useState(false);
  const { id } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : 0;
  });
  const loading = useSelector(function (state: RootState) {
    return state.schedule.scheduleViewLoading;
  });
  const updateSuccess = useSelector(function (state: RootState) {
    return state.schedule.scheduleAddLoading;
  });
  const item = useSelector(function (state: RootState) {
    return state.schedule.scheduleViewSuccess
      ? state.schedule.scheduleViewSuccess.data
      : {};
  });
  const fetchJadwalOperatorModal = (data) => {
    const params = data.id;
    dispatch(scheduleFetchOne(params));
  };
  const depart = new Date(item.tanggal_berangkat);
  const arrive = new Date(item.tanggal_tiba);
  const closebumn = new Date(item.tutup_jadwal_shipper_bumn);
  const closeswasta = new Date(item.tutup_jadwal_shipper_swasta);
  const m = MonthsID;
  useEffect(() => {
    fetchJadwalOperatorModal({ id: jadwal_id });
  }, [updateSuccess]);
  return (
    <Root>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={Colors.pri}/>
        </View>
      ) : (
        <Card style={styles.card}>
          <View style={{ flexDirection: "row" }}>
            {/** Rute */}
            <View style={{ flexDirection: "row", flex: 2 }}>
              <View style={{ flex: 1 }}>
                <B>
                  {item.pel_asal}
                  {"\n"}
                  <Caption>
                    {`${DateFormat(depart)}`}
                    {"\n"}
                    {`(${TimeFormat(depart)})`}{" "}
                  </Caption>
                </B>
              </View>
              <View
                style={{
                  flex: 1,
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
              </View>
              <View style={{ flex: 1, alignItems: "flex-end" }}>
                <B>
                  {item.pel_sampai}
                  {"\n"}
                  <Caption>
                    {`${DateFormat(arrive)}`}
                    {"\n"}
                    {`(${TimeFormat(arrive)})`}{" "}
                  </Caption>
                </B>
              </View>
            </View>

            {/** Trayek & Voyage */}
            <View
              style={{
                flex: 1,
                alignItems: "flex-end",
                justifyContent: "center",
              }}
            >
              <Paragraph>
                <B>Trayek</B> {item.kode_trayek} {"\n"}
                <B>Voyage</B> {item.voyage}
              </Paragraph>
            </View>
          </View>

          {/** Closing BUMN */}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <Icon
              name={"calendar-remove"}
              style={{ fontSize: 20, color: Colors.def }}
            />
            <Paragraph style={{ alignItems: "center" }}>
              {" "}
              Closing BUMN :{" "}
              {`${closebumn.getDate()} ${
                m[closebumn.getMonth()]
              } ${closebumn.getFullYear()}`}
            </Paragraph>
          </View>

          {/** Closing SWASTA */}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <Icon
              name={"calendar-remove"}
              style={{ fontSize: 20, color: Colors.sec }}
            />
            <Paragraph style={{ alignItems: "center" }}>
              {" "}
              Closing SWASTA :{" "}
              {`${closeswasta.getDate()} ${
                m[closeswasta.getMonth()]
              } ${closeswasta.getFullYear()}`}
            </Paragraph>
          </View>

          {/** Operator */}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <Icon name={"account"} style={{ fontSize: 20 }} />
            <Paragraph style={{ alignItems: "center" }}>
              {" "}
              {item.operator_kapal_name}
            </Paragraph>
          </View>

          {/** Telepon */}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <Icon name={"phone"} style={{ fontSize: 20 }} />
            <Paragraph style={{ alignItems: "center" }}>
              {" "}
              {item.operator_telp}
            </Paragraph>
          </View>

          {/** Telepon */}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <Icon name={"mail"} style={{ fontSize: 20 }} />
            <Paragraph style={{ alignItems: "center" }}>
              {" "}
              {item.operator_email}
            </Paragraph>
          </View>

          {/** Telepon */}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 5,
            }}
          >
            <Icon name={"office-building"} style={{ fontSize: 20 }} />
            <Paragraph style={{ alignItems: "center" }}>
              {" "}
              {item.operator_alamat}
            </Paragraph>
          </View>

          <IconButton
            icon="calendar-edit"
            style={{ alignSelf:'flex-end' }}
            onPress={function () {
              setShow(!show);
            }}
          />
        </Card>
      )}
      <Space height={5} />

      {show ? (
        <Card style={styles.card}>
          <Formik
            initialValues={{
              id: item.id,
              is_ready_to_book: true,
              tanggal_berangkat:
                DateFormatStrip(item.tanggal_berangkat) +
                " " +
                TimeFormat(item.tanggal_berangkat),
              tanggal_tiba:
                DateFormatStrip(item.tanggal_tiba) +
                " " +
                TimeFormat(item.tanggal_tiba),
              tutup_jadwal_shipper_bumn:
                DateFormatStrip(item.tutup_jadwal_shipper_bumn) +
                " " +
                TimeFormat(item.tutup_jadwal_shipper_bumn),
              tutup_jadwal_shipper_swasta:
                DateFormatStrip(item.tutup_jadwal_shipper_swasta) +
                " " +
                TimeFormat(item.tanggal_berangkat),
              user_id: String(id),
            }}
            onSubmit={function (val) {
              // console.log(val);
              dispatch(scheduleUpdate(val));
            }}
          >
            {({
              handleChange,
              handleSubmit,
              handleBlur,
              values,
              setFieldValue,
            }) => (
              <>
                {/** Tanggal Berangkat */}
                <DateTime
                  initialValue={new Date(item.tanggal_berangkat)}
                  label="Departure"
                  onChangeValue={(date: string) =>
                    setFieldValue("tanggal_berangkat", date)
                  }
                  type="datetime"
                />
                <Space height={10} />

                {/** Tanggal Sampai */}
                <DateTime
                  initialValue={new Date(item.tanggal_tiba)}
                  label="Arrival"
                  onChangeValue={(date: string) =>
                    setFieldValue("tanggal_tiba", date)
                  }
                  type="datetime"
                />
                <Space height={10} />

                {/** Tanggal Closing BUMN */}
                <DateTime
                  initialValue={new Date(item.tutup_jadwal_shipper_bumn)}
                  label="Closing BUMN"
                  onChangeValue={(date: string) =>
                    setFieldValue("tutup_jadwal_shipper_bumn", date)
                  }
                  type="datetime"
                />
                <Space height={10} />

                {/** Tanggal Closing SWASTA */}
                <DateTime
                  initialValue={new Date(item.tutup_jadwal_shipper_swasta)}
                  label="Closing Swasta"
                  onChangeValue={(date: string) =>
                    setFieldValue("tutup_jadwal_shipper_swasta", date)
                  }
                  type="datetime"
                />
                <Space height={10} />

                <Button onPress={handleSubmit}>Update Jadwal</Button>
              </>
            )}
          </Formik>
        </Card>
      ) : null}
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  modalContainer: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 3,
    elevation: 3,
    margin: 10,
  },
  bold: {
    fontWeight: "bold",
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  ctrlButton: {
    flex: 1,
    borderRadius: 3,
  },
});
