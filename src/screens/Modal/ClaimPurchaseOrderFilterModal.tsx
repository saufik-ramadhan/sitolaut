import React from "react";
import { Formik } from "formik";
import { Button, TextInput } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { SelectInput, DateTime } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { DateFormatStrip } from "../../services/utils";
import { bookingFilterAdd } from "../../redux/bookingReducer";

export default function ClaimPurchaseOrderFilterModal({ navigation, route }) {
  const { supplier, shipper, port } = route.params;
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : [{}];
  });

  return (
    <Formik
      initialValues={{
        date_po_end: "",
        date_po_start: "",
        id: `${id}`,
        kode_booking: "",
        length: 10,
        no_po: "",
        port_destination_id: "",
        port_origin_id: "",
        shipper_id: "",
        supplier_id: "",
      }}
      onSubmit={function (values) {
        dispatch(bookingFilterAdd(values));
        navigation.goBack();
      }}
    >
      {({ setFieldValue, handleSubmit, handleBlur, handleChange, values }) => (
        <>
          {/**
           * No PO
           */}
          <TextInput
            value={values.no_po}
            onChangeText={handleChange("no_po")}
            onBlur={handleBlur("no_po")}
            placeholder="No PO"
          />

          {/**
           * Kode Booking
           */}
          <TextInput
            value={values.kode_booking}
            onChangeText={handleChange("kode_booking")}
            onBlur={handleBlur("kode_booking")}
            placeholder="Kode Booking"
          />

          {/**
           * Dari Tanggal
           */}
          <DateTime
            label="Dari Tanggal"
            onChangeValue={(date) =>
              setFieldValue("date_po_start", DateFormatStrip(date))
            }
          />

          {/**
           * Sampai Tanggal
           */}
          <DateTime
            label="Sampai Tanggal"
            onChangeValue={(date) =>
              setFieldValue("date_po_end", DateFormatStrip(date))
            }
          />

          {/**
           * Supplier
           */}
          <SelectInput
            label={"Supplier"}
            onChangeValue={(val) => setFieldValue("supplier_id", val.id)}
            options={supplier}
            objectKey="label"
            withSearch
          />

          {/**
           * Shipper
           */}
          <SelectInput
            label={"Shipper"}
            onChangeValue={(val) => setFieldValue("shipper_id", val.id)}
            options={shipper}
            objectKey="label"
            withSearch
          />

          {/**
           * Origin
           */}
          <SelectInput
            label={"Origin"}
            onChangeValue={(val) => setFieldValue("port_origin_id", val.id)}
            options={port}
            objectKey="label"
            withSearch
          />

          {/**
           * Destination
           */}
          <SelectInput
            label={"Destination"}
            onChangeValue={(val) =>
              setFieldValue("port_destination_id", val.id)
            }
            options={port}
            objectKey="label"
            withSearch
          />

          <Button mode="contained" onPress={handleSubmit}>
            Submit
          </Button>
        </>
      )}
    </Formik>
  );
}
