import React from "react";
import { Formik } from "formik";
import { Button, TextInput } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { SelectInput, DateTime } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { DateFormatStrip } from "../../services/utils";
import { priceconsigneeFilterAdd } from "../../redux/priceconsigneeReducer";

export default function HargaJualBarangFilterModal({ navigation, route }) {
  const { supplier, shipper, port } = route.params;
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : [{}];
  });

  return (
    <Formik
      initialValues={{
        sampai: "",
        dari: "",
        status: "",
        no_po: "",
        port_destination_id: "",
        port_origin_id: "",
        shipper_id: "",
        supplier_id: "",
      }}
      onSubmit={function (values) {
        dispatch(priceconsigneeFilterAdd(values));
        navigation.goBack();
      }}
    >
      {({ setFieldValue, handleSubmit, handleBlur, handleChange, values }) => (
        <>
          {/**
           * No PO
           */}
          <TextInput
            value={values.no_po}
            onChangeText={handleChange("no_po")}
            onBlur={handleBlur("no_po")}
            placeholder="No PO"
          />

          {/**
           * Kode Booking
           */}
          {/* <TextInput
            value={values.kode_booking}
            onChangeText={handleChange("kode_booking")}
            onBlur={handleBlur("kode_booking")}
            placeholder="Kode Booking"
          /> */}

          {/**
           * Dari Tanggal
           */}
          <DateTime
            label="Dari Tanggal"
            onChangeValue={(date) =>
              setFieldValue("dari", DateFormatStrip(date))
            }
          />

          {/**
           * Sampai Tanggal
           */}
          <DateTime
            label="Sampai Tanggal"
            onChangeValue={(date) =>
              setFieldValue("sampai", DateFormatStrip(date))
            }
          />

          {/**
           * Status
           */}
          <SelectInput
            label={"Status"}
            onChangeValue={(val) => setFieldValue("status", val.state)}
            options={[
              { id: 0, label: "Belum Diset", state: false },
              { id: 1, label: "Sudah Diset", state: true },
            ]}
            objectKey="label"
          />

          {/**
           * Supplier
           */}
          <SelectInput
            label={"Supplier"}
            onChangeValue={(val) => setFieldValue("supplier_id", val.id)}
            options={supplier}
            objectKey="label"
            withSearch
          />

          {/**
           * Shipper
           */}
          <SelectInput
            label={"Shipper"}
            onChangeValue={(val) => setFieldValue("shipper_id", val.id)}
            options={shipper}
            objectKey="label"
            withSearch
          />

          {/**
           * Origin
           */}
          <SelectInput
            label={"Origin"}
            onChangeValue={(val) => setFieldValue("port_origin_id", val.id)}
            options={port}
            objectKey="label"
            withSearch
          />

          {/**
           * Destination
           */}
          <SelectInput
            label={"Destination"}
            onChangeValue={(val) =>
              setFieldValue("port_destination_id", val.id)
            }
            options={port}
            objectKey="label"
            withSearch
          />

          <Button mode="contained" onPress={handleSubmit}>
            Submit
          </Button>
        </>
      )}
    </Formik>
  );
}
