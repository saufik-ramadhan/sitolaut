import React from "react";
import { Formik } from "formik";
import { Button, TextInput } from "react-native-paper";
import { useSelector, useDispatch } from "react-redux";
import { SelectInput, DateTime, Space } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { DateFormatStrip } from "../../services/utils";
import { bookingFilterAdd } from "../../redux/bookingReducer";
import { portFilterAdd } from "../../redux/portReducer";
import { View } from "react-native";

export default function PelabuhanFilterModal({ navigation, route }) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : [{}];
  });

  return (
    <View style={{flex: 1, padding: 10}}>
    <Formik
      initialValues={{
        kode: "",
        nama: "",
      }}
      onSubmit={function (values) {
        dispatch(portFilterAdd(values));
        navigation.goBack();
      }}
    >
      {({ setFieldValue, handleSubmit, handleBlur, handleChange, values }) => (
        <View style={{flex: 1}}>
          {/**
           * Kode Pelabuhan
           */}
          <TextInput
            label="Kode Pelabuhan"
            dense
            value={values.kode}
            onChangeText={handleChange("kode")}
            onBlur={handleBlur("kode")}
          />
          <Space height={10}/>
          {/**
           * Nama Pelabuhan
           */}
          <TextInput
            label="Nama Pelabuhan"
            value={values.nama}
            onChangeText={handleChange("nama")}
            onBlur={handleBlur("nama")}
          />

          <Button mode="contained" onPress={handleSubmit} style={{position: 'absolute', bottom: 0, width: '100%'}}>
            Submit
          </Button>
        </View>
      )}
    </Formik>
    </View>
  );
}
