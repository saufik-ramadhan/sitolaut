import React from 'react';
import {StyleSheet, Image, Text} from 'react-native';
import {Button, Title, Paragraph} from 'react-native-paper';
import {Root, Space} from '../components';
import {Navigation} from 'react-native-navigation';
import Screens from './Screens';
import {useConnect} from 'remx';
import {store} from './../stores/authentication/store';
import * as actions from './../stores/authentication/actions';

function Welcome(props) {
  function login() {
    Navigation.push(props.componentId, {
      component: {
        id: 'LOGIN_SCREEN',
        name: Screens.LoginScreen,
      },
    });
  }
  function register() {
    Navigation.push(props.componentId, {
      component: {
        id: 'REGISTER_SCREEN',
        name: Screens.RegisterScreen,
      },
    });
  }

  return (
    <Root style={styles.container}>
      <Logo />
      <Title>Selamat Datang</Title>
      <Paragraph>di Aplikasi Logistic Management System</Paragraph>
      <Paragraph>Kementrian Perhubungan Republik Indonesia</Paragraph>
      <Illustration />
      <Space height={20} />
      <Button mode="contained" onPress={login} style={styles.accessBtn}>
        Masuk
      </Button>
      <Space height={10} />
      <Button mode="outlined" onPress={register} style={styles.accessBtn}>
        daftar baru
      </Button>
    </Root>
  );
}

function Logo() {
  return (
    <Image
      source={require('../assets/medias/images/lcslogo.png')}
      style={{transform: [{scale: 0.5}]}}
    />
  );
}

function Illustration() {
  return (
    <Image
      source={require('../assets/medias/images/tollaut.png')}
      style={{height: 180, width: 360, opacity: 0.3}}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  accessBtn: {
    width: '100%',
  },
});

export default WelcomeScreen;
