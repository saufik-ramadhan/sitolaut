import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  List,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput } from "../../components";
import { DateFormat } from "../../services/utils";
import {
  jeniscommodityFilterClear,
  jeniscommodityFetchPage,
  jeniscommodityDelete,
  jeniscommodityFilterAdd,
} from "./../../redux/jeniscommodityReducer";

function FloatingAdd({ navigation }: RegulatorStackProps<"BarangPenting">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateBarangPenting");
      }}
    />
  );
}

export default function BarangPenting({
  navigation,
}: RegulatorStackProps<"BarangPenting">) {
  const dispatch = useDispatch();
  const jeniscommodity = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityData
      ? state.jeniscommodity.jeniscommodityData
      : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.jeniscommodity.jeniscommodityViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      jenis_barang_id: 4,
      ...filter,
    };
    dispatch(jeniscommodityFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      jenis_barang_id: 4,
      ...filter,
    };
    dispatch(jeniscommodityFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(
      jeniscommodityDelete(id)
    );
    dispatch(jeniscommodityFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(jeniscommodityFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={jeniscommodity}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Item title={item.nama_barang} description={item.jenis_barang}
              right={() => (
                <>
                  <IconButton
                    icon="square-edit-outline"
                    onPress={function () {
                      navigation.navigate("UpdateBarangPenting", {item: item})
                    }}
                  />
                  <IconButton
                    icon="delete"
                    onPress={function () {
                      Alert.alert(
                        "Hapus",
                        "Anda yakin akan menghapus jenis barang ini?",
                        [{text: "OK", onPress: () => handleDelete(item.id)}, {text: "Cancel",onPress: () => null}],
                        { cancelable: false }
                      );
                    }}
                  />
                </>
              )}
            />
          );
        }}
      />
      <FloatingAdd navigation={navigation} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});
