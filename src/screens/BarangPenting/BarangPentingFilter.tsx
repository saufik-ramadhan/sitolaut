import React from "react";
import { StyleSheet, Text, View, TextInput, Picker } from "react-native";
import { Formik } from "formik";
import { jeniscommodityFilterAdd } from "../../redux/jeniscommodityReducer";
import { IconButton } from "react-native-paper";

export default function BarangPentingFilter() {
  return (
    <Formik
      initialValues={{
        jenis_barang_id: "",
        nama_barang: "",
      }}
      onSubmit={(val) => dispatch(jeniscommodityFilterAdd(val))}
    >
      {({ handleChange, handleBlur, handleSubmit, values, setFieldValue }) => (
        <>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <TextInput
              label="Cari"
              onChangeText={handleChange("nama_barang")}
              onBlur={handleBlur("nama_barang")}
              value={values.nama_barang}
              style={{ flex: 5 }}
            />
            <IconButton
              onPress={handleSubmit}
              icon="magnify"
              style={{ flex: 1 }}
            />
          </View>
          {/**
           * Jenis Barang
           */}
          <View
            style={[
              styles.formItem,
              { flexDirection: "row", justifyContent: "center" },
            ]}
          >
            <Picker
              selectedValue={values.jenis_barang_id}
              style={{ flex: 10 }}
              itemStyle={{ borderRadius: 5 }}
              mode="dropdown"
              onValueChange={function (itemValue: number) {
                setFieldValue("jenis_barang_id", itemValue);
              }}
            >
              <Picker.Item label="Jenis Barang" value={0} />
              {jenisbarang.map(function (item, key) {
                return (
                  <Picker.Item label={item.label} value={item.id} key={key} />
                );
              })}
            </Picker>
          </View>
        </>
      )}
    </Formik>
  );
}

const styles = StyleSheet.create({});
