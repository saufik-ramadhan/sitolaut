import React, { useEffect } from 'react'
import {Image, View } from 'react-native'
import {Formik} from 'formik';
import * as Yup from 'yup';
import { Center, DateTime, FileInput, Root, SelectInput2, TextInput } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/rootReducer';
import { ActivityIndicator, Button, HelperText } from 'react-native-paper';
import { msttrayekFetchAll } from '../../redux/msttrayekReducer';
import { jeniscommodityAdd } from '../../redux/jeniscommodityReducer';
import { RegulatorStackProps } from '../Navigator';

const GambarSchema = Yup.object({

})
const CreateBarangPentingSchema = Yup.object({
  nama_barang: Yup.string().required().label('Nama Barang'),
  tgl_mulai: Yup.string().required().label('Tanggal Mulai'),
  tgl_akhir: Yup.string().required().label('Tanggal Mulai'),
  gambar: GambarSchema,
  id_jenis_barang: Yup.string().required().label('Jenis Barang'),
  id_mst_trayek: Yup.string().required().label('Trayek')
})

export default function CreateBarangPenting({navigation}: RegulatorStackProps<'CreateBarangPenting'>) {
  const dispatch = useDispatch();
  const { msttrayek } = useSelector((state: RootState) => state);
  useEffect(() => {
    dispatch(msttrayekFetchAll());
  }, [])
  if(!msttrayek.msttrayekGetSuccess.options) {return <Center><ActivityIndicator size="large"/></Center>}
  return (
    <Root>
    <Formik
      validationSchema={CreateBarangPentingSchema}
      initialValues={{
        nama_barang: "",
        tgl_mulai: "",
        tgl_akhir: "",
        gambar: "",
        id_jenis_barang: "4",
        id_mst_trayek: ""
      }}    
      onSubmit={(val) => {
        dispatch(jeniscommodityAdd(val));
        navigation.goBack();
      }}
    >
      {({handleBlur,handleChange,handleSubmit, setFieldValue, touched, values, errors}) => (
        <View style={{padding: 20}}>
          <TextInput
            label="Nama Barang"
            value={values.nama_barang}
            onChangeText={handleChange('nama_barang')}
            onBlur={handleBlur('nama_barang')}
            error={errors.nama_barang && touched.nama_barang}
          />
          <HelperText type="error">{errors.nama_barang}</HelperText>
          <SelectInput2
            items={msttrayek.msttrayekGetSuccess.options}
            label="Trayek"
            onValueChange={(itemValue, itemIndex) => setFieldValue('id_mst_trayek', itemValue)}
            selectedValue={values.id_mst_trayek}
            itemLabel="label"
            itemValue="value"
          />
          <HelperText type="error">{errors.id_mst_trayek}</HelperText>
          <DateTime
            label="Tanggal Mulai"
            onChangeValue={(date: string) => setFieldValue("tgl_mulai", date.date)}
            type="date"
          />
          <HelperText type="error">{errors.tgl_mulai}</HelperText>
          <DateTime
            label="Tanggal Akhir"
            onChangeValue={(date: string) => setFieldValue("tgl_akhir", date.date)}
            type="date"
          />
          <HelperText type="error">{errors.tgl_akhir}</HelperText>
          <FileInput
            title="Surat Rekomendasi"
            placeholder={
              values.gambar ? values.gambar.name : "Upload File siup"
            }
            getValue={function (value) {
              setFieldValue("gambar", {
                uri: value.uri,
                type: value.type,
                name: value.fileName,
              });
            }}
          />
          <HelperText type="error">{errors.gambar}</HelperText>
          {values.gambar ? <Image source={{ uri: values.gambar.uri }} style={{ height: 100, width: 100 }} /> : null}

          <Button onPress={handleSubmit}>SIMPAN</Button>
        </View>
      )}
    </Formik>
    </Root>
  )
}
