import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  Caption,
  Card,
  Paragraph,
  TextInput,
  Button,
} from "react-native-paper";
import { formatRupiah, DateFormat } from "../../services/utils";
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { distributionUpdate } from "../../redux/distributionReducer";

export default function ReadGoods({ navigation, route }) {
  const { item } = route.params;
  const dispatch = useDispatch();
  return (
    <Card style={{ padding: 10 }}>
      <Paragraph>
        <Paragraph
          selectable
          style={{ fontWeight: "bold", fontSize: 16, flex: 3 }}
        >
          No : {item.kode_booking}
        </Paragraph>
        {"\n"}
        {item.nama_barang}
        {"\n"}
        Qty. {item.qty}
        {item.satuan}
      </Paragraph>
      <Caption style={{ flex: 1, position: "absolute", right: 0, top: 0 }}>
        {DateFormat(item.date_po)}{" "}
      </Caption>
      <Formik
        initialValues={{
          harga_satuan_beli: item.harga_satuan_beli,
          harga_satuan_jual: item.harga_satuan_jual,
          id: item.id,
        }}
        onSubmit={function (val) {
          dispatch(distributionUpdate(val));
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleBlur, handleChange, values }) => (
          <>
            <TextInput
              value={String(parseInt(values.harga_satuan_beli))}
              label={"Harga Beli (Rp.)"}
              style={styles.form}
              mode={"flat"}
              onChangeText={handleChange("harga_satuan_beli")}
              onBlur={handleBlur("harga_satuan_beli")}
            />
            <TextInput
              value={String(parseInt(values.harga_satuan_jual))}
              label={"Harga Jual (Rp.)"}
              style={styles.form}
              mode={"flat"}
              onChangeText={handleChange("harga_satuan_jual")}
              onBlur={handleBlur("harga_satuan_jual")}
            />
            <Button onPress={handleSubmit}>Update</Button>
          </>
        )}
      </Formik>
    </Card>
  );
}

const styles = StyleSheet.create({
  form: {
    backgroundColor: "white",
  },
});
