import React, { useState } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Space, FilterButton, EmptyState } from "../../components";
import {
  Card,
  Paragraph,
  Caption,
  IconButton,
  Badge,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { ConsigneeStackProps } from "../Navigator";
import { purchaseFetchPage } from "../../redux/purchaseReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import { distributionFetchReseller } from "../../redux/distributionReducer";
import { distributionFilterClear } from "./../../redux/distributionReducer";
import { DateFormat, formatRupiah } from "../../services/utils";
import { shipperFetchAll } from "../../redux/shipperReducer";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { portFetchAll } from "../../redux/portReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";

function GoodsList({ navigation }: ConsigneeStackProps<"Goods">) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const distribution = useSelector(function (state: RootState) {
    return state.distribution.distributionData
      ? state.distribution.distributionData
      : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.distribution.distributionGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.distribution.distributionFilter;
  });
  const listSupplier = useSelector(function (state: RootState) {
    return state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : [];
  });
  const listShipper = useSelector(function (state: RootState) {
    return state.shipper.shipperGetSuccess
      ? state.shipper.shipperGetSuccess.data
      : [];
  });
  const listReseller = useSelector(function (state: RootState) {
    return state.reseller.resellerGetSuccess
      ? state.reseller.resellerGetSuccess.data
      : [];
  });
  const updateSuccess = useSelector(function (state: RootState) {
    return state.distribution.distributionAddLoading;
  });
  const listPort = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  const loading = useSelector(function (state: RootState) {
    return state.distribution.distributionGetLoading;
  });

  function fetchData(filter) {
    const params = {
      length: 10,
      user_id: String(id),
      ...filter,
    };
    dispatch(distributionFetchReseller(params));
    dispatch(shipperFetchAll());
    dispatch(supplierFetchAll());
    dispatch(resellerFetchAll());
    dispatch(portFetchAll());
  }

  function handleReachEnd(filter) {
    const params = {
      length: 10,
      user_id: String(id),
      start: currentPage * 10,
      ...filter,
    };
    dispatch(distributionFetchReseller(params));
  }

  const onRefresh = () => {
    dispatch(distributionFilterClear());
  };

  React.useEffect(() => {
    filter === {} ? null : fetchData(filter);
  }, [filter, updateSuccess]);

  return (
    <>
      <FlatList
        data={distribution}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                navigation.navigate("ReadGoods", {
                  item: item,
                });
              }}
            >
              <Paragraph>
                <Paragraph
                  selectable
                  style={{ fontWeight: "bold", fontSize: 12, flex: 3 }}
                >
                  No : {item.kode_booking}
                </Paragraph>
                {"\n"}
                {item.nama_barang}
                {"\n"}
                Qty. {item.qty}
                {item.satuan}
                {"\n"}
                Beli. Rp.{formatRupiah(parseInt(item.harga_satuan_beli))}
                {"\n"}
                Jual. Rp.{formatRupiah(parseInt(item.harga_satuan_jual))}
              </Paragraph>
              <Caption
                style={{ flex: 1, position: "absolute", right: 0, bottom: 0 }}
              >
                {DateFormat(item.date_po)}{" "}
              </Caption>
            </Card>
          );
        }}
      />

      <FilterButton
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "GoodsFilterModal",
            params: {
              shipper: listShipper,
              port: listPort,
              supplier: listSupplier,
            },
          });
        }}
      />
    </>
  );
}

const Goods = ({ navigation, route }: ConsigneeStackProps<"Goods">) => {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });

  return (
    <View style={styles.container}>
      <GoodsList navigation={navigation} route={route} />
    </View>
  );
};

export default Goods;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 2,
  },
});
