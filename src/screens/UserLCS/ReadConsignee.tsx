import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Paragraph, Card } from "react-native-paper";
import { Root } from "../../components";

export default function ReadConsignee({ navigation, route }) {
  const item = route.params.item;
  return (
    <Root>
      <Card style={{}}>
        <Paragraph>{JSON.stringify(item)}</Paragraph>
      </Card>
    </Root>
  );
}

const styles = StyleSheet.create({});
