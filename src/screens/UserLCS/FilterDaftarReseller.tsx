import React, { useEffect } from "react";
import { StyleSheet, Text, View, Picker } from "react-native";
import { Formik } from "formik";
import { resellerFilterAdd } from "../../redux/resellerReducer";
import { IconButton, Button } from "react-native-paper";
import { Root, SelectInput2, TextInput } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { routeFetchAll } from "../../redux/routeReducer";

export default function FilterDaftarReseller({ navigation }) {
  const dispatch = useDispatch();
  const provinsi = useSelector(function (state: RootState) {
    return state.provinsi.provinsiGetSuccess
      ? state.provinsi.provinsiGetSuccess.data
      : [];
  });
  return (
    <Root>
    <Formik
      initialValues={{
        email: "",
        kota_id: "",
        nama_perusahaan: "",
        provinsi_id: "",
        status: "",
      }}
      onSubmit={(val) => {
        dispatch(resellerFilterAdd(val));
        navigation.goBack();
        console.log(val);
      }}
    >
      {({ handleChange, handleBlur, handleSubmit, values, setFieldValue }) => (
        <>
          {/**
           * Nama Perusahaan
           */}
          <TextInput
            style={[styles.formItem, styles.textInput]}
            value={values.nama_perusahaan}
            onChangeText={handleChange("nama_perusahaan")}
            onBlur={handleBlur("nama_perusahaan")}
            label="Nama Perusahaan"
          />
          {/**
           * Email
           */}
          <TextInput
            style={[styles.formItem, styles.textInput]}
            value={values.email}
            onChangeText={handleChange("email")}
            onBlur={handleBlur("email")}
            label="Email PIC"
            keyboardType="email-address"
          />

          {/**
           * Provinsi
           */}
          <SelectInput2
            label={"Provinsi"}
            onValueChange={(itemValue, itemIndex) => setFieldValue("provinsi_id", itemValue)}
            items={provinsi}
            itemLabel="label"
            itemValue="id"
            selectedValue={values.provinsi_id}
          />

          {/**
           * Status
           */}
          <SelectInput2
            label={"Status"}
            onValueChange={(itemValue, itemIndex) => setFieldValue("status", itemValue)}
            items={[
              {
                value: "0",
                label: "Tidak Aktif",
              },
              {
                value: "1",
                label: "Aktif",
              },
              {
                value: "2",
                label: "Ditolak",
              },
            ]}
            itemLabel="label"
            itemValue="value"
            selectedValue={values.status}
          />

          <Button onPress={handleSubmit}>Filter</Button>
        </>
      )}
    </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
