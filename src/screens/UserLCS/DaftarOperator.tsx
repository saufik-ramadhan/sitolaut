import React, { useState } from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  Button,
  Portal,
  Dialog,
  List,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import {
  EmptyState,
  FilterButton,
  SelectInput,
  Space,
  ErrorText,
} from "../../components";
import { DateFormat, formatRupiah } from "../../services/utils";
import {
  operatorFilterClear,
  operatorFetchPage,
  operatorDelete,
  operatorFilterAdd,
} from "./../../redux/operatorReducer";
import { Formik } from "formik";
import * as Yup from "yup";
import { userResetPassword } from "../../redux/userReducer";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} harus sama dengan ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}

Yup.addMethod(Yup.string, "equalTo", equalTo);
const ValidationSchema = Yup.object().shape({
  id: Yup.number().required(),
  password: Yup.string().required("Tidak boleh kosong").min(6, "tidak boleh kurang dari 6 karakter"),
  confirm_password: Yup.string()
    .equalTo(Yup.ref("password"), "Passwords harus sama")
    .required("Tidak boleh kosong"),
});

export default function DaftarOperator({
  navigation,
  route,
}: RegulatorStackProps<"DaftarOperator">) {
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState({});
  const { provinsi } = route.params;
  const operator = useSelector(function (state: RootState) {
    return state.operator.operatorData ? state.operator.operatorData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.operator.operatorGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.operator.operatorFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.operator.operatorGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.operator.operatorAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.operator.operatorDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.operator.operatorViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(operatorFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(operatorFetchPage(params));
  }
  function hideDialog() {
    setVisible(false);
  }
  function handleDelete(id) {
    dispatch(operatorDelete(id));
    dispatch(operatorFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(operatorFilterClear());
  };
  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={operator}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Accordion
              title={item.nama_perusahaan}
              description={`${item.email} | ${item.status === 1 ? "Aktif" : "Non-Aktif"}`}
            >
              <View style={{backgroundColor: 'whitesmoke'}}>
                <List.Item title={item.telp} left={() => <List.Icon icon="phone"/>} style={styles.item} titleStyle={styles.title}
                descriptionStyle={styles.description}/>
                <List.Item title={item.npwp} description="NPWP" style={styles.item} titleStyle={styles.title}
                descriptionStyle={styles.description}/>
                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                  <Button icon={'account'} onPress={function() {
                    navigation.navigate("ReadAktivasiUser", {
                      item: item,
                      usertype: "Operator"
                    });
                  }}>Detail</Button>
                  <Button icon={'account-key'} onPress={function(){
                    setVisible(true);
                    setData(item);
                  }}>Pass</Button>
                  <Button icon={'delete'} color={Colors.danger} onPress={function() {
                    Alert.alert(
                      "Hapus",
                      "Anda yakin akan menghapus operator ini ?",
                      [
                        { text: "Ya", onPress: () => dispatch(operatorDelete(item.id)) },
                        { text: "Tidak", onPress: () => null}
                      ],
                    );
                  }}>Delete</Button>
                </View>
              </View>
            </List.Accordion>
          );
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("FilterDaftarOperator", {
            provinsi: provinsi,
          });
        }}
      />
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Password</Dialog.Title>
          <Dialog.Content>
            <Formik
              validationSchema={ValidationSchema}
              initialValues={{
                confirm_password: "",
                id: data.id,
                password: "",
              }}
              onSubmit={function (val) {
                dispatch(userResetPassword(val));
                setVisible(false);
                // console.log(val);
              }}
            >
              {({
                handleChange,
                handleBlur,
                handleSubmit,
                values,
                errors,
                touched,
              }) => (
                <>
                  <TextInput
                    style={[styles.formItem, styles.textInput]}
                    value={values.password}
                    secureTextEntry={true}
                    onChangeText={handleChange("password")}
                    onBlur={handleBlur("password")}
                    label="Password"
                    keyboardType="ascii-capable"
                  />
                  {errors.password && touched.password && (
                    <ErrorText>{errors.password}</ErrorText>
                  )}
                  <Space height={10} />
                  <TextInput
                    style={[styles.formItem, styles.textInput]}
                    value={values.confirm_password}
                    secureTextEntry={true}
                    onChangeText={handleChange("confirm_password")}
                    onBlur={handleBlur("confirm_password")}
                    label="Konfirmasi Password"
                    keyboardType="ascii-capable"
                  />
                  {errors.confirm_password && touched.confirm_password && (
                    <ErrorText>{errors.confirm_password}</ErrorText>
                  )}
                  <Button onPress={handleSubmit}>Reset</Button>
                </>
              )}
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
  },
  button: {
    position: "absolute",
    right: 0,
  },
  item: {
    paddingVertical: 0,
    marginVertical: 0,
  },
  title: {
    fontSize: 14,
  },
  description: {
    fontSize: 12,
  }
});