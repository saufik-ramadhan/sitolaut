import React, { useState } from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  Button,
  Portal,
  Dialog,
  List,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import {
  EmptyState,
  FilterButton,
  SelectInput,
  Space,
  ErrorText,
} from "../../components";
import { DateFormat, formatRupiah } from "../../services/utils";
import {
  shipperFilterClear,
  shipperFetchPage,
  shipperDelete,
  shipperFilterAdd,
} from "./../../redux/shipperReducer";
import { Formik } from "formik";
import * as Yup from "yup";
import { userResetPassword } from "../../redux/userReducer";

/**
 * Validation Schema
 */
function equalTo(ref: any, msg: any) {
  return Yup.mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || "${path} harus sama dengan ${reference}",
    params: {
      reference: ref.path,
    },
    test: function (value: any) {
      return value === this.resolve(ref);
    },
  });
}

Yup.addMethod(Yup.string, "equalTo", equalTo);
const ValidationSchema = Yup.object().shape({
  id: Yup.number().required(),
  password: Yup.string().required("Tidak boleh kosong").min(6, "tidak boleh kurang dari 6 karakter"),
  confirm_password: Yup.string()
    .equalTo(Yup.ref("password"), "Passwords harus sama")
    .required("Tidak boleh kosong"),
});

export default function DaftarShipper({
  navigation,
  route,
}: RegulatorStackProps<"DaftarShipper">) {
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState({});
  const { provinsi } = route.params;
  const shipper = useSelector(function (state: RootState) {
    return state.shipper.shipperData ? state.shipper.shipperData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.shipper.shipperGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.shipper.shipperFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.shipper.shipperGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.shipper.shipperAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.shipper.shipperDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.shipper.shipperViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(shipperFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(shipperFetchPage(params));
  }
  function hideDialog() {
    setVisible(false);
  }
  function handleDelete(id) {
    dispatch(shipperDelete(id));
    dispatch(shipperFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(shipperFilterClear());
  };
  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={shipper}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Accordion
              title={item.nama_perusahaan}
              description={`${item.email} | ${item.status === 1 ? "Aktif" : "Non-Aktif"}`}
            >
              <View style={{backgroundColor: 'whitesmoke'}}>
                <List.Item title={item.telp} left={() => <List.Icon icon="phone"/>} style={styles.item} titleStyle={styles.title}
                descriptionStyle={styles.description}/>
                <List.Item title={item.npwp} description="NPWP" style={styles.item} titleStyle={styles.title}
                descriptionStyle={styles.description}/>
                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                  <Button icon={'account'} onPress={function() {
                    navigation.navigate("ReadAktivasiUser", {
                      item: item,
                      usertype: "Shipper"
                    });
                  }}>Detail</Button>
                  <Button icon={'account-key'} onPress={function(){
                    setVisible(true);
                    setData(item);
                  }}>Pass</Button>
                  <Button icon={'delete'} color={Colors.danger} onPress={function() {
                    Alert.alert(
                      "Hapus",
                      "Anda yakin akan menghapus shipper ini ?",
                      [
                        { text: "Ya", onPress: () => dispatch(shipperDelete(item.id)) },
                        { text: "Tidak", onPress: () => null}
                      ],
                    );
                  }}>Delete</Button>
                </View>
              </View>
            </List.Accordion>
          );
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("FilterDaftarShipper", {
            provinsi: provinsi,
          });
        }}
      />
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Password</Dialog.Title>
          <Dialog.Content>
            <Formik
              validationSchema={ValidationSchema}
              initialValues={{
                confirm_password: "",
                id: data.id,
                password: "",
              }}
              onSubmit={function (val) {
                dispatch(userResetPassword(val));
                setVisible(false);
                // console.log(val);
              }}
            >
              {({
                handleChange,
                handleBlur,
                handleSubmit,
                values,
                errors,
                touched,
              }) => (
                <>
                  <TextInput
                    style={[styles.formItem, styles.textInput]}
                    value={values.password}
                    secureTextEntry={true}
                    onChangeText={handleChange("password")}
                    onBlur={handleBlur("password")}
                    label="Password"
                    keyboardType="ascii-capable"
                  />
                  {errors.password && touched.password && (
                    <ErrorText>{errors.password}</ErrorText>
                  )}
                  <Space height={10} />
                  <TextInput
                    style={[styles.formItem, styles.textInput]}
                    value={values.confirm_password}
                    secureTextEntry={true}
                    onChangeText={handleChange("confirm_password")}
                    onBlur={handleBlur("confirm_password")}
                    label="Konfirmasi Password"
                    keyboardType="ascii-capable"
                  />
                  {errors.confirm_password && touched.confirm_password && (
                    <ErrorText>{errors.confirm_password}</ErrorText>
                  )}
                  <Button onPress={handleSubmit}>Reset</Button>
                </>
              )}
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
  },
  button: {
    position: "absolute",
    right: 0,
  },
  item: {
    paddingVertical: 0,
    marginVertical: 0,
  },
  title: {
    fontSize: 14,
  },
  description: {
    fontSize: 12,
  }
});