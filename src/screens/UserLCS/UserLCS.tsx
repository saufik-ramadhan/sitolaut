import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { TouchableRipple, Card, Paragraph, List } from "react-native-paper";
import { Icon, Root } from "../../components";
import Colors from "../../config/Colors";
import { Heading3 } from "./../../components/Heading";
import { RegulatorStackProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import { provinsiFetchAll } from "../../redux/provinsiReducer";
import { RootState } from "../../redux/rootReducer";
import { Item } from 'react-native-paper/lib/typescript/src/components/List/List';

export default function UserLCS({
  navigation,
}: RegulatorStackProps<"UserLCS">) {
  const dispatch = useDispatch();
  const provinsi = useSelector(function (state: RootState) {
    return state.provinsi.provinsiGetSuccess
      ? state.provinsi.provinsiGetSuccess.data
      : [];
  });
  useEffect(() => {
    dispatch(provinsiFetchAll());
  }, []);

  return (
    <Root>
      <List.Item
        title="Consignee"
        description="Daftar user consignee"
        onPress={() => navigation.navigate('DaftarConsignee', {provinsi: provinsi})}
        right={() => <List.Icon icon="chevron-right"/>}
      />
      <List.Item
        title="Supplier"
        description="Daftar user supplier"
        onPress={() => navigation.navigate('DaftarSupplier', {provinsi: provinsi})}
        right={() => <List.Icon icon="chevron-right"/>}
      />
      <List.Item
        title="Shipper"
        description="Daftar user shipper"
        onPress={() => navigation.navigate('DaftarShipper', {provinsi: provinsi})}
        right={() => <List.Icon icon="chevron-right"/>}
      />
      <List.Item
        title="Reseller"
        description="Daftar user reseller"
        onPress={() => navigation.navigate('DaftarReseller', {provinsi: provinsi})}
        right={() => <List.Icon icon="chevron-right"/>}
      />
      <List.Item
        title="Vessel Operator"
        description="Daftar user operator"
        onPress={() => navigation.navigate('DaftarOperator', {provinsi: provinsi})}
        right={() => <List.Icon icon="chevron-right"/>}
      />
    </Root>
  );
}

const styles = StyleSheet.create({
  menu: {
    padding: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    fontSize: 100,
    flex: 1,
    color: Colors.grayL,
  },
  content: {
    flex: 2,
  },
  heading: {
    color: "rgba(0,0,0,0.4)",
  },
});
