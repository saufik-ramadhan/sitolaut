import React from "react";
import {
  StyleSheet,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, SelectInput2, TextInput } from "../../components";
import Colors from "../../config/Colors";
import {
  Button,
  HelperText,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import { vesselAdd, vesselFilterClear } from "./../../redux/vesselReducer";
import { Formik } from "formik";
import * as Yup from "yup";
import { masterKontrakFetchAll } from "../../redux/masterkontrakReducer";
import { listOperatorFetchAll } from "../../redux/masterkapalReducer";
import { onlyNumber } from "../../services/utils";

const CreateKapalSchema = Yup.object().shape({
  nama_kapal: Yup.string().required().label('Nama Kapal'),
  device_id: Yup.string().required().label('Device Id'),
  status_operasional: Yup.number().required().label('Status Operasional'),
  status_kapal: Yup.number().required().label('Status Kapal'),
  tahun_kontrak: Yup.number().required().label('Tahun Kontrak'),
  nomor_kontrak: Yup.string().required().label('Nomor Kontrak'),
  operator_id: Yup.number().required().label('Operator Id'),
});

export default function FilterKapal({
  navigation,
}: OperatorStackProps<"FilterKapal">) {
  const dispatch = useDispatch();
  const { masterkapal, masterkontrak, vessel } = useSelector((state: RootState) => state);

  const fetchData = () => {
    dispatch(listOperatorFetchAll());
    dispatch(masterKontrakFetchAll());
  };

  /** Render Tahun Kontrak Selector*/
  const tahunKontrak = () => {
    let tahun = [];
    let currentYear = new Date().getFullYear();
    for(let i=currentYear; i<currentYear + 15; i++) {
      tahun.push({id: i, label: String(i)});
    }
    return tahun;
  }

  /** Constructor */
  React.useEffect(() => {
    fetchData();
  }, [vessel.vesselAddLoading]);

  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          nama_kapal: "",
          device_id: "",
          status_operasional: "",
          status_kapal: "",
          tahun_kontrak: "",
          nomor_kontrak: "",
          operator_id: "",
        }}
        onSubmit={(values) => {
          dispatch(vesselAdd(values));
          dispatch(vesselFilterClear());
          navigation.goBack();
          // alert(values);
          // console.log(values);
        }}
        validationSchema={CreateKapalSchema}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            <TextInput
              label="Nama Kapal"
              value={values.nama_kapal}
              onChangeText={handleChange("nama_kapal")}
              onBlur={handleBlur("nama_kapal")}
            />
            {errors.nama_kapal && touched.nama_kapal && (
              <HelperText type="error">{errors.nama_kapal}</HelperText>
            )}

            <TextInput
              label="Device ID"
              value={values.device_id}
              onChangeText={(val) => setFieldValue('device_id', onlyNumber(val))}
              onBlur={handleBlur("device_id")}
            />
            {errors.device_id && touched.device_id && (
              <HelperText type="error">{errors.device_id}</HelperText>
            )}
            <Space height={10} />

            <SelectInput2
              label="Status Operasional"
              items={[{label: "Kapal Utama", value: "0"}, {label: "Kapal Pengganti", value: "1"}]}
              mode="dropdown"
              selectedValue={values.status_operasional}
              onValueChange={(itemValue, itemIndex) => setFieldValue('status_operasional', itemValue)}
              itemLabel="label"
              itemValue="value"
            />

            <SelectInput2
              label="Tahun Kontrak"
              items={tahunKontrak()}
              mode="dropdown"
              selectedValue={values.tahun_kontrak}
              onValueChange={(itemValue, itemIndex) => setFieldValue('tahun_kontrak', itemValue)}
              itemLabel="label"
              itemValue="id"
            />

            <SelectInput2
              label="Nomor Kontrak"
              items={masterkontrak.kontrakGetSuccess.data}
              mode="dropdown"
              selectedValue={values.nomor_kontrak}
              onValueChange={(itemValue, itemIndex) => setFieldValue('nomor_kontrak', itemValue)}
              itemLabel="nomor_kontrak"
              itemValue="nomor_kontrak"
            />

            <SelectInput2
              label="Operator"
              items={masterkapal.listOperatorGetSuccess.data}
              mode="dropdown"
              selectedValue={values.operator_id}
              onValueChange={(itemValue, itemIndex) => setFieldValue('operator_id', itemValue)}
              itemLabel="nama_perusahaan"
              itemValue="id"
            />
            
            <SelectInput2
              label="Status Kapal"
              items={[{id: 1, label: 'Aktif'}, {id: 0, label: 'Tidak Aktif'}]}
              mode="dropdown"
              selectedValue={values.status_kapal}
              onValueChange={(itemValue, itemIndex) => setFieldValue('status_kapal', itemValue)}
              itemLabel="label"
              itemValue="id"
            />

            {/**
             * Photo Kapal
             */}
            {/* {values.photo ? (
              <>
                <Image
                  source={{ uri: values.photo.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Photo"
              placeholder={
                values.photo ? values.photo.name : "Upload File siup"
              }
              error={errors.photo_size}
              getValue={function (value) {
                setFieldValue("photo", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("photo_size", value.fileSize);
              }}
            />
            <Space height={10} /> */}

            <Button icon={"plus"} onPress={handleSubmit} mode={"contained"}>
              Simpan
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
