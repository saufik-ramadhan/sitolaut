import React from "react";
import { StyleSheet, Text, View, Alert, Image } from "react-native";
import { OperatorStackProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import {
  vesselFetchOne,
  vesselDelete,
  vesselFetchPage,
} from "../../redux/vesselReducer";
import { RootState } from "../../redux/rootReducer";
import { Card, Caption, Paragraph, Button, List } from "react-native-paper";
import Colors from "./../../config/Colors";
import { UPLOAD_URL } from "../../config/constants";

export default function ReadKapal({
  navigation,
  route,
}: OperatorStackProps<"ReadKapal">) {
  const vesselId = route.params.id;
  const dispatch = useDispatch();
  const vessel = useSelector(function (state: RootState) {
    return state.vessel.vesselViewSuccess
      ? state.vessel.vesselViewSuccess.data[0]
      : {};
  });
  React.useEffect(() => {
    dispatch(vesselFetchOne(vesselId));
  }, []);

  return (
    <List.Section>
      <List.Item
        title={vessel.nama_kapal}
        description="Nama Kapal"
        left={props => <List.Icon {...props} icon="ferry" />}
      />
      <List.Item
        title={vessel.device_id}
        description="Device ID"
        left={props => <List.Icon {...props} icon="barcode" />}
      />
      <List.Item
        title={vessel.status_operasional == 0 ? "Kapal Utama" : "Kapal Pengganti"}
        description="Status Operasional"
        left={props => <List.Icon {...props} icon="ferry" color={vessel.status_operasional == 0 ? Colors.pri : Colors.sec} />}
      />
      <List.Item
        title={vessel.status_kapal == 0 ? "Tidak Aktif" : "Aktif"}
        description="Status Kapal"
        left={props => <List.Icon {...props} icon="information" color={vessel.status_kapal == 0 ? Colors.danger : Colors.ter}/>}
      />
      <List.Item
        title={vessel.nama_operator}
        description="Nama Operator"
        left={props => <List.Icon {...props} icon="account" />}
      />
      <Image
        source={{ uri: UPLOAD_URL + vessel.photo }}
        style={{ height: 200, width: '100%' }}
        resizeMode="contain"
      />
      <Button icon="delete" onPress={function(){
        Alert.alert("Hapus", "Anda yakin akan menghapus kapal ini?", [
          {text: "Ya", onPress:() => {
            dispatch(vesselDelete(vesselId))
            navigation.goBack()
          }},
          {text:"Tdk", onPress:() => null}
        ])
      }}>Hapus</Button>
    </List.Section>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  action: {
    flexDirection: "row",
  },
  button: {
    flex: 1,
  },
});
