import React from "react";
import { StyleSheet, Text, View, FlatList, TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, ErrorText, SelectInput } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  Button,
  HelperText,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import { vesselFetchPage, vesselFilterAdd } from "./../../redux/vesselReducer";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { portFetchAll } from "../../redux/portReducer";
import * as Yup from "yup";
import { listOperatorFetchAll } from "../../redux/masterkapalReducer";

export default function FilterKapal({
  navigation,
  route,
}: OperatorStackProps<"FilterKapal">) {
  const dispatch = useDispatch();
  /**
   * Selector
   */
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const { masterkapal } = useSelector((state: RootState) => state);

  React.useEffect(() => {
    dispatch(listOperatorFetchAll());
  }, [])

  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          nama_kapal: "",
          operator_id: usertype === "Vessel Operator" ? String(id) : "",
        }}
        onSubmit={(values) => {
          // fetchData(values);
          dispatch(vesselFilterAdd(values));
          navigation.goBack();
          // alert(values);
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Kapal
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_kapal}
              onChangeText={handleChange("nama_kapal")}
              onBlur={handleBlur("nama_kapal")}
              placeholder="Nama Kapal"
            />
            {errors.nama_kapal && touched.nama_kapal && (
              <ErrorText>{errors.nama_kapal}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Operator
             */}
            {
              usertype === "Regulator" && (
                <View>
                  <SelectInput
                    label="Operator"
                    onChangeValue={(val) => setFieldValue('operator_id', val.id)}
                    options={masterkapal.listOperatorGetSuccess.data}
                    objectKey="nama_perusahaan"
                  />
                  {errors.operator_id && touched.operator_id && (
                    <HelperText type="error">{errors.operator_id}</HelperText>
                  )}
                  <Space height={10} />
                </View>
              )
            }

            <Button icon={"filter"} onPress={handleSubmit} mode="contained">
              Filter
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
