import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root } from "../../components";
import { Formik } from "formik";
import { useDispatch } from "react-redux";
import { trackingdataFilterAdd } from "../../redux/trackingdataReducer";
import { IconButton, TextInput, Button } from "react-native-paper";
import { OperatorStackProps } from "../Navigator";

export default function TrackingDataFilter({
  navigation,
}: OperatorStackProps<"TrackingDataFilter">) {
  const dispatch = useDispatch();
  return (
    <>
      <Formik
        initialValues={{
          device_id: "",
          nama_kapal: "",
          view_type: "daily",
        }}
        onSubmit={(val) => {
          dispatch(trackingdataFilterAdd(val));
          navigation.goBack();
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <>
            <TextInput
              label="Device ID"
              onChangeText={handleChange("device_id")}
              onBlur={handleBlur("device_id")}
              value={values.device_id}
            />
            <TextInput
              label="Nama Kapal"
              onChangeText={handleChange("nama_kapal")}
              onBlur={handleBlur("nama_kapal")}
              value={values.nama_kapal}
            />
            <Button onPress={handleSubmit}>Filter</Button>
          </>
        )}
      </Formik>
    </>
  );
}

const styles = StyleSheet.create({});
