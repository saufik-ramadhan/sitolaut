import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps, OperatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput, Space } from "../../components";
import { DateFormat } from "../../services/utils";
import {
  trackingdataFilterClear,
  trackingdataFilterAdd,
  trackingDataGet,
} from "./../../redux/trackingdataReducer";
import { Formik } from "formik";

export default function TrackingData({
  navigation,
}: OperatorStackProps<"TrackingData">) {
  const dispatch = useDispatch();
  const trackingdata = useSelector(function (state: RootState) {
    return state.trackingdata.trackingdataData
      ? state.trackingdata.trackingdataData
      : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.trackingdata.trackingdataGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.trackingdata.trackingdataFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.trackingdata.trackingdataGetLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(trackingDataGet(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(trackingDataGet(params));
  }

  const onRefresh = () => {
    dispatch(trackingdataFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter]);

  return (
    <>
      <FlatList
        data={trackingdata}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                null;
              }}
            >
              <Caption>
                Device ID :{" "}
                <Paragraph style={{ flex: 3 }}>{item.txid}</Paragraph>
              </Caption>
              <Caption>
                Nama Kapal :{" "}
                <Paragraph style={{ flex: 3 }}>{item.name}</Paragraph>
              </Caption>
            </Card>
          );
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("TrackingDataFilter");
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});
