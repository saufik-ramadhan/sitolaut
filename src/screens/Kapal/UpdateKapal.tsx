import React from "react";
import {
  StyleSheet, View
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, SelectInput, SelectInput2, TextInput, Center} from "../../components";
import Colors from "../../config/Colors";
import {
  Caption,
  Button,
  HelperText,
  ActivityIndicator,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import { vesselUpdate, vesselFilterClear, vesselFetchOne } from "./../../redux/vesselReducer";
import { Formik } from "formik";
import * as Yup from "yup";
import { masterKontrakFetchAll } from "../../redux/masterkontrakReducer";
import { listOperatorFetchAll } from "../../redux/masterkapalReducer";
import { onlyNumber } from "../../services/utils";
import { Picker } from "@react-native-community/picker";

const UpdateKapalSchema = Yup.object().shape({
  nama_kapal: Yup.string().required().label('Nama Kapal'),
  device_id: Yup.string().required().label('Device Id'),
  status_operasional: Yup.number().required().label('Status Operasional'),
  status_kapal: Yup.number().required().label('Status Kapal'),
  tahun_kontrak: Yup.number().required().label('Tahun Kontrak'),
  nomor_kontrak: Yup.string().required().label('Nomor Kontrak'),
  operator_id: Yup.number().required().label('Operator Id'),
});

export default function FilterKapal({
  navigation,
  route
}: OperatorStackProps<"FilterKapal">) {
  const dispatch = useDispatch();
  const {id_kapal} = route.params;
  const { masterkapal, masterkontrak, vessel } = useSelector((state: RootState) => state);

  const fetchData = () => {
    dispatch(vesselFetchOne(id_kapal))
    dispatch(listOperatorFetchAll());
    dispatch(masterKontrakFetchAll());
  };

  /** Render Tahun Kontrak Selector*/
  const tahunKontrak = () => {
    let tahun = [];
    let currentYear = new Date().getFullYear();
    for(let i=currentYear; i<currentYear + 15; i++) {
      tahun.push({id: i, label: String(i)});
    }
    return tahun;
  }



  /** Constructor */
  React.useEffect(() => {
    fetchData();
  }, []);
  
  if(!vessel.vesselViewSuccess.data) return <Center><ActivityIndicator color={Colors.pri}/></Center>;
  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          nama_kapal: vessel.vesselViewSuccess.data[0].nama_kapal,
          device_id: vessel.vesselViewSuccess.data[0].device_id,
          status_operasional: vessel.vesselViewSuccess.data[0].status_operasional,
          status_kapal: vessel.vesselViewSuccess.data[0].status_kapal,
          tahun_kontrak: vessel.vesselViewSuccess.data[0].tahun_kontrak,
          nomor_kontrak: vessel.vesselViewSuccess.data[0].nomor_kontrak,
          operator_id: vessel.vesselViewSuccess.data[0].operator_id,
          photo: "",
          id: vessel.vesselViewSuccess.data[0].id,
        }}
        onSubmit={(values) => {
          dispatch(vesselUpdate(values));
          dispatch(vesselFilterClear());
          // navigation.goBack();
          // alert(values);
          // console.log(values);
        }}
        validationSchema={UpdateKapalSchema}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            <TextInput
              label="Nama Kapal"
              value={values.nama_kapal}
              onChangeText={handleChange("nama_kapal")}
              onBlur={handleBlur("nama_kapal")}
            />
            {errors.nama_kapal && touched.nama_kapal && (
              <HelperText type="error">{errors.nama_kapal}</HelperText>
            )}

            <TextInput
              label="Device ID"
              value={values.device_id}
              onChangeText={(val) => setFieldValue('device_id', onlyNumber(val))}
              onBlur={handleBlur("device_id")}
            />
            {errors.device_id && touched.device_id && (
              <HelperText type="error">{errors.device_id}</HelperText>
            )}
            <Space height={10} />

            <SelectInput2
              label="Status Operasional"
              items={[{label: "Kapal Utama", value: "0"}, {label: "Kapal Utama", value: "1"}]}
              mode="dropdown"
              selectedValue={values.status_operasional}
              onValueChange={(itemValue, itemIndex) => setFieldValue('status_operasional', itemValue)}
              itemLabel="label"
              itemValue="value"
            />

            <SelectInput2
              label="Tahun Kontrak"
              items={tahunKontrak()}
              mode="dropdown"
              selectedValue={values.tahun_kontrak}
              onValueChange={(itemValue, itemIndex) => setFieldValue('tahun_kontrak', itemValue)}
              itemLabel="label"
              itemValue="id"
            />

            <SelectInput2
              label="Nomor Kontrak"
              items={masterkontrak.kontrakGetSuccess.data}
              mode="dropdown"
              selectedValue={values.nomor_kontrak}
              onValueChange={(itemValue, itemIndex) => setFieldValue('nomor_kontrak', itemValue)}
              itemLabel="nomor_kontrak"
              itemValue="nomor_kontrak"
            />

            <SelectInput2
              label="Operator"
              items={masterkapal.listOperatorGetSuccess.data}
              mode="dropdown"
              selectedValue={values.operator_id}
              onValueChange={(itemValue, itemIndex) => setFieldValue('operator_id', itemValue)}
              itemLabel="nama_perusahaan"
              itemValue="id"
            />
            
            <SelectInput2
              label="Status Kapal"
              items={[{id: 1, label: 'Aktif'}, {id: 0, label: 'Tidak Aktif'}]}
              mode="dropdown"
              selectedValue={values.status_kapal}
              onValueChange={(itemValue, itemIndex) => setFieldValue('status_kapal', itemValue)}
              itemLabel="label"
              itemValue="id"
            />

            {/**
             * Photo Kapal
             */}
            {/* {values.photo ? (
              <>
                <Image
                  source={{ uri: values.photo.uri }}
                  style={{ height: 100, width: 100 }}
                />
                <Space height={10} />
              </>
            ) : null}
            <FileInput
              title="Photo"
              placeholder={
                values.photo ? values.photo.name : "Upload File siup"
              }
              error={errors.photo_size}
              getValue={function (value) {
                setFieldValue("photo", {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
                setFieldValue("photo_size", value.fileSize);
              }}
            />
            <Space height={10} /> */}

            <Button icon={"plus"} onPress={handleSubmit} mode={"contained"} style={styles.button}>
              Simpan
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  button: {
    borderRadius: 5,
    marginTop: 20,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
  picker: {
    borderWidth: 0.3,
    borderColor: 'gray',
    borderRadius: 5,
    width: '100%',
    alignSelf: 'center'
  }
});
