import React from "react";
import { StyleSheet, Text, View, FlatList, Alert, Image } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, Icon, FilterButton, EmptyState, FileInput } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  List,
  Dialog,
  Portal,
  TextInput,
  Button,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import { msttrayekFetchPage } from "./../../redux/msttrayekReducer";
import { portFetchPage } from "../../redux/portReducer";
import {
  vesselFetchPage,
  vesselDelete,
  vesselFilterClear,
  vesselFetchAll,
  vesselFetchOne,
  vesselUpdate,
} from "../../redux/vesselReducer";
import { Formik } from "formik";

function FloatingAdd({ navigation }: OperatorStackProps<"Kapal">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateKapal");
      }}
    />
  );
}

export default function Kapal({
  navigation,
  route,
}: OperatorStackProps<"Kapal">) {
  const dispatch = useDispatch();
  const [update, setUpdate] = React.useState({visible:false, data: {}});
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.vessel.vesselGetSuccess;
  });
  const vessels = useSelector(function (state: RootState) {
    return state.vessel.vesselData ? state.vessel.vesselData : [];
  });
  const filter = useSelector(function (state: RootState) {
    return state.vessel.vesselFilter;
  });
  const vesselLoading = useSelector(function(state: RootState) {
    return state.vessel.vesselAddLoading;
  })
  const vesselDelLoading = useSelector(function(state: RootState) {
    return state.vessel.vesselDeleteLoading;
  })
  const refresh = useSelector(function (state: RootState) {
    return state.vessel.vesselGetLoading;
  });
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const fetchData = (filter) => {
    const params = {
      length: 10,
      operator_id: usertype === "Vessel Operator" ? id : "",
      start: 0,
      ...filter,
    };
    dispatch(vesselFetchPage(params));
  };
  const handleReachEnd = (filter) => {
    const params = {
      length: 10,
      operator_id: usertype === "Vessel Operator" ? id : "",
      start: currentPage * 10,
      ...filter,
    };
    dispatch(vesselFetchPage(params));
  };
  const onRefresh = () => {
    dispatch(vesselFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, vesselLoading, vesselDelLoading]);

  return (
    <View style={{flex: 1}}>
      <FlatList
        data={vessels}
        ListHeaderComponent={() => <Space height={10} />}
        onRefresh={onRefresh}
        refreshing={refresh}
        ListEmptyComponent={<EmptyState/>}
        onEndReached={() => (hasNext ? handleReachEnd(filter) : null)}
        onEndReachedThreshold={0.5}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        renderItem={function ({item}) {
          return (
            <List.Item title={item.nama_kapal} titleStyle={{fontSize: 14}} descriptionStyle={{fontSize: 12}} titleNumberOfLines={3} description={'ID: ' + item.device_id} 
              left={() => <List.Icon icon="ferry"/>} 
              right={() => <IconButton icon="square-edit-outline" onPress={function(){
                usertype == "Regulator" ? navigation.navigate('UpdateKapal', {id_kapal: item.id}) : setUpdate({visible: true, data: item})
              }}/>}
              onPress={function(){
                navigation.navigate("ReadKapal", {
                  id: item.id,
                });
              }}/>
          );
        }}
      />

      <Portal>
        <Dialog visible={update.visible} onDismiss={function(){setUpdate({visible: false, data: {}})}}>
          <Dialog.Content>
            <Formik
              initialValues={{
                nama_kapal: update.data.nama_kapal,
                device_id: 	update.data.device_id,
                operator_id: update.data.operator_id,
                nomor_kontrak: update.data.nomor_kontak,
                status_operasional: update.data.status_operasional,
                status_kapal: update.data.status_kapal,
                tahun_kontrak: update.data.tahun_kontrak,
                photo: "",
                photo_size: "",
                id: update.data.id,
              }}
              onSubmit={(val) => {
                dispatch(vesselUpdate(val))
                setUpdate({visible: false, data: {}});
              }}
            >
              {({handleSubmit, handleChange, handleBlur, setFieldValue, errors,values}) => (
                <View>
                  {values.photo ? (
                    <>
                      <Image
                        source={{ uri: values.photo.uri }}
                        style={{ height: 100, width: 100 }}
                      />
                      <Space height={10} />
                    </>
                  ) : null}
                  <FileInput
                    title="Foto Kapal"
                    placeholder={
                      values.photo ? values.photo.name : "Upload File siup"
                    }
                    error={errors.photo_size}
                    getValue={function (value) {
                      setFieldValue("photo", {
                        uri: value.uri,
                        type: value.type,
                        name: value.fileName,
                      });
                      setFieldValue("photo_size", value.fileSize);
                    }}
                  />
                  <Button onPress={handleSubmit}>Upload</Button>
                </View>
              )}
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>

      {usertype == "Regulator" && <FloatingAdd navigation={navigation} route={route} />}
      <FilterButton
        onPress={function () {
          navigation.navigate("FilterKapal");
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    padding: 10,
    marginBottom: 3,
  },
});
