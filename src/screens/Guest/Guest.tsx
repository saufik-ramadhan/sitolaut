import React from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  Badge,
  Button,
  Caption,
  Card,
  IconButton,
  Paragraph,
  TextInput,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { Icon, Root } from "../../components";
import { RootState } from "../../redux/rootReducer";
import { RegulatorStackProps } from "../Navigator";
import {
  changePassword,
  deleteGuest,
  guestFetchAll,
  addGuest,
} from "./../../redux/guestReducer";
import Colors from "./../../config/Colors";
import Confirmation from "../../components/Confirmation";
import { Formik } from "formik";

function FloatingAdd({
  navigation,
  onPress,
}: RegulatorStackProps<"MasterKodeTrayek">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={onPress}
    />
  );
}

export default function Guest(props: RegulatorStackProps<"Guest">) {
  const dispatch = useDispatch();
  const { guest } = useSelector((state: RootState) => state);
  const [add, setAdd] = React.useState({ visible: false, data: {} });
  const [edit, setEdit] = React.useState({ visible: false, id: 0 });
  const [del, setDel] = React.useState({ visible: false, id: 0 });

  const fetchData = () => {
    dispatch(guestFetchAll());
  };
  React.useEffect(() => {
    fetchData();
  }, [guest.loading]);
  if (!guest.guestGetSuccess.data)
    return <Text>Data Guest tidak ditemukan</Text>;
  return (
    <>
      <Confirmation
        visible={del.visible}
        onYes={function () {
          dispatch(deleteGuest({ id: del.id }));
          setDel({ visible: false, id: 0 });
          // navigation.goBack();
        }}
        onNo={function () {
          setDel({ visible: false, id: 0 });
        }}
        content="Delete Guest"
        title="Delete"
        onDismiss={function () {
          setDel({ visible: false, id: 0 });
        }}
      />
      <Confirmation
        visible={edit.visible}
        component={
          <Formik
            initialValues={{
              id: edit.id,
              password: "",
              confirm_password: "",
            }}
            onSubmit={function (vals) {
              if (vals.password !== vals.confirm_password)
                alert("Konfirmasi Password Tidak sama");
              else {
                setEdit({ visible: false, id: 0 });
                dispatch(changePassword(vals));
              }
            }}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              setFieldValue,
              values,
            }) => (
              <>
                <TextInput
                  label="Password"
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  value={values.password}
                  secureTextEntry
                />
                <TextInput
                  label="Confirm Password"
                  onChangeText={handleChange("confirm_password")}
                  onBlur={handleBlur("confirm_password")}
                  value={values.confirm_password}
                  secureTextEntry
                />
                <Button onPress={handleSubmit}>Change Password</Button>
              </>
            )}
          </Formik>
        }
        title="Chage Password"
        onDismiss={function () {
          setEdit({ visible: false, id: 0 });
        }}
      />
      <Confirmation
        visible={add.visible}
        component={
          <Formik
            initialValues={{
              email: "",
              password: "",
              usery_type_id: 7,
            }}
            onSubmit={function (vals) {
              setAdd({ visible: false, data: {} });
              dispatch(addGuest(vals));
            }}
          >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              setFieldValue,
              values,
            }) => (
              <>
                <TextInput
                  label="Email"
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  value={values.email}
                />
                <TextInput
                  label="Password"
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  value={values.password}
                  secureTextEntry
                />
                <Button onPress={handleSubmit}>Tambah Guest</Button>
              </>
            )}
          </Formik>
        }
        title="Add Guest"
        onDismiss={function () {
          setAdd({ visible: false, id: 0 });
        }}
      />
      <Root>
        {guest.guestGetSuccess.data.map((item, key) => (
          <Card
            key={key}
            style={styles.card}
            onPress={function () {
              null;
            }}
          >
            <Caption>
              Email : <Paragraph>{item.email}</Paragraph>
            </Caption>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                justifyContent: "space-around",
              }}
            >
              <Icon
                name="account-key"
                style={{ fontSize: 20 }}
                onPress={function () {
                  setEdit({ visible: true, id: item.id });
                }}
              />
              <Icon
                name="delete"
                style={{ fontSize: 20 }}
                onPress={function () {
                  setDel({ visible: true, id: item.id });
                }}
              />
            </View>
          </Card>
        ))}
      </Root>
      <FloatingAdd
        {...props}
        onPress={function () {
          setAdd({ visible: true, data: {} });
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    margin: 5,
  },
});
