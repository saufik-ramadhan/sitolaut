import React from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Root, ErrorText, Space } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";
import Colors from "./../../config/Colors";
import { Picker } from "@react-native-community/picker";
import { TextInput, Button } from "react-native-paper";
import { RootState } from "../../redux/rootReducer";
import { userFetchPage } from "../../redux/userReducer";
import { operatorFetchPage } from "../../redux/operatorReducer";
import { complaintAdd, complaintFetchPage } from "../../redux/complaintReducer";

/**
 * Validation Schema
 */
const PengaduanSchema = Yup.object().shape({
  subject: Yup.string().required("Required"),
  description: Yup.string().required("Required"),
  user_id: Yup.number().required("Required"),
  // operator_id: Yup.number().required("Required"),
  // regulator_id: Yup.number().required("Required"),
});

export default function CreatePengaduan({ navigation, route }) {
  const dispatch = useDispatch();
  const [tujuan, setTujuan] = React.useState(0);
  const isLoading = useSelector(function (state: RootState) {
    return state.complaint.complaintAddLoading;
  });
  const createSuccess = useSelector(function (state: RootState) {
    return state.complaint.complaintAddSuccess
      ? state.complaint.complaintAddSuccess.success
      : false;
  });
  const regulatorList = useSelector(function (state: RootState) {
    return state.user.userGetSuccess
      ? state.user.userGetSuccess.data
      : [{ id: 0, label: "" }];
  });
  const operatorList = useSelector(function (state: RootState) {
    return state.operator.operatorGetSuccess
      ? state.operator.operatorGetSuccess.data
      : [{ id: 0, label: "" }];
  });
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  React.useEffect(() => {
    dispatch(
      complaintFetchPage({
        user_id: `${id}`,
      })
    );
    createSuccess ? navigation.goBack() : null;
  }, [createSuccess]);
  React.useEffect(() => {
    dispatch(
      userFetchPage({
        typeUser: 6,
        length: 1000,
      })
    );
    dispatch(
      operatorFetchPage({
        length: 1000,
      })
    );
  }, []);
  return (
    <Root style={styles.container}>
      {/**
       * Tujuan
       */}
      <View
        style={[
          styles.formItem,
          { flexDirection: "row", justifyContent: "center" },
        ]}
      >
        <Picker
          selectedValue={tujuan}
          style={{ flex: 10 }}
          itemStyle={{ fontSize: 8 }}
          mode="dropdown"
          onValueChange={function (itemValue: number) {
            setTujuan(itemValue);
          }}
        >
          <Picker.Item label="Pilih Tujuan" value={0} />
          <Picker.Item label="Regulator" value={6} />
          <Picker.Item label="Vessel Operator" value={5} />
        </Picker>
      </View>
      <Space height={10} />
      <Formik
        initialValues={{
          subject: "",
          description: "",
          user_id: `${id}`,
          operator_id: "",
          regulator_id: 1,
        }}
        validationSchema={PengaduanSchema}
        onSubmit={(values) => {
          // alert(
          //   JSON.stringify({
          //     subject: values.subject,
          //     description: values.description,
          //     user_id: values.user_id,
          //     operator_id: tujuan == 5 ? values.operator_id : null,
          //     regulator_id: tujuan == 6 ? values.regulator_id : null,
          //   })
          // );
          dispatch(
            complaintAdd({
              subject: values.subject,
              description: values.description,
              user_id: values.user_id,
              operator_id: tujuan == 5 ? values.operator_id : null,
              regulator_id: tujuan == 6 ? values.regulator_id : null,
            })
          );
        }}
        // validationSchema={PengaduanSchema}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Pilih Operator
             */}
            {tujuan == 5 && (
              <>
                {/**
                 * Pilih Operator
                 */}
                <View
                  style={[
                    styles.formItem,
                    { flexDirection: "row", justifyContent: "center" },
                  ]}
                >
                  <Picker
                    selectedValue={values.operator_id}
                    style={{ flex: 10 }}
                    itemStyle={{ fontSize: 8 }}
                    mode="dropdown"
                    onValueChange={function (itemValue: string) {
                      setFieldValue("operator_id", itemValue);
                    }}
                  >
                    <Picker.Item label="Pilih Operator" value={0} />
                    {operatorList.map((item, key) => (
                      <Picker.Item
                        label={item.label}
                        value={item.id}
                        key={key}
                      />
                    ))}
                  </Picker>
                </View>
                {errors.operator_id && touched.operator_id && (
                  <ErrorText>{errors.operator_id}</ErrorText>
                )}
                <Space height={10} />
              </>
            )}

            {/**
             * Subject
             */}
            <TextInput
              style={[styles.formItem]}
              value={values.subject}
              onChangeText={handleChange("subject")}
              autoCapitalize="none"
              onBlur={handleBlur("subject")}
              label="Subject"
            />
            {errors.subject && touched.subject && (
              <ErrorText>{errors.subject}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Description
             */}
            <TextInput
              style={[styles.formItem]}
              value={values.description}
              onChangeText={handleChange("description")}
              onBlur={handleBlur("description")}
              label="Description"
            />
            {errors.description && touched.description && (
              <ErrorText>{errors.description}</ErrorText>
            )}
            <Space height={20} />

            <Button
              mode="contained"
              color={Colors.pri}
              labelStyle={{ color: Colors.white }}
              onPress={handleSubmit}
              loading={isLoading}
              style={{ borderRadius: 3 }}
              contentStyle={{ padding: 5 }}
            >
              Input Aduan
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
});
