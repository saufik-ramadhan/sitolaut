import React from "react";
import { Root } from "../../components";
import { Paragraph, List, IconButton } from "react-native-paper";
import { StyleSheet, FlatList, View } from "react-native";
import Colors from "./../../config/Colors";
import { AppBottomTabProps, ConsigneeStackProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  complaintFetchPage,
  complaintFetchOne,
} from "../../redux/complaintReducer";

/**
 * Floating Add for Adding Pengaduan
 * @param param0
 */
function FloatingAdd({ navigation }: ConsigneeStackProps<"PurchaseOrder">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreatePengaduan");
      }}
    />
  );
}

export default function Pengaduan({
  navigation,
  route,
}: ConsigneeStackProps<"Pengaduan">) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: "",
          usertype: "Not Defined",
        };
  });
  const complaints = useSelector(function (state: RootState) {
    return state.complaint.complaintGetSuccess
      ? state.complaint.complaintGetSuccess.data
      : [];
  });
  const closing = useSelector(function (state: RootState) {
    return state.complaint.complaintCloseLoading;
  });
  React.useEffect(() => {
    if (usertype == "Vessel Operator" || usertype == "Regulator") {
      dispatch(
        complaintFetchPage({
          start: 0,
          length: 10,
          operator_id: usertype == "Vessel Operator" ? `${id}` : "",
          regulator_id: usertype == "Regulator" ? `${id}` : "",
        })
      );
    } else {
      dispatch(
        complaintFetchPage({
          user_id: `${id}`,
        })
      );
    }
  }, [closing]);
  return (
    <>
      <FlatList
        data={complaints}
        renderItem={function (item) {
          return (
            <List.Item
              title={item.item.subject}
              description={item.item.description}
              left={(props) => (
                <List.Icon
                  {...props}
                  icon={item.item.status_is_open ? "lock-open" : "lock"}
                  color={item.item.status_is_open ? "orangered" : "green"}
                />
              )}
              descriptionNumberOfLines={1}
              onPress={function () {
                navigation.navigate("ReadPengaduan", {
                  pengaduan_id: item.item.id,
                });
              }}
            />
          );
        }}
        keyExtractor={function (item, key) {
          return String(key);
        }}
      />
      {usertype !== "Regulator" && usertype !== "Vessel Operator" && (
        <FloatingAdd navigation={navigation} route={route} />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
  },
});
