import React from "react";
import { StyleSheet, Text, View, TextInput, Image, Alert } from "react-native";
import Colors from "./../../config/Colors";
import { Root } from "../../components";
import {
  IconButton,
  Caption,
  Card,
  Button,
  Paragraph,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  complaintFetchOne,
  complaintReply,
  complaintClose,
} from "../../redux/complaintReducer";
import { Formik } from "formik";
import * as Yup from "yup";
import { DateTimeFormat } from "../../services/utils";

type Chat = {
  message: string;
};

const PengaduanSchema = Yup.object().shape({
  description: Yup.string().required("Fill Message"),
  user_id: Yup.number().required(),
  pengaduan_id: Yup.number().required(),
});

function Padding({ size, children }) {
  return <View style={{ padding: size }}>{children}</View>;
}
function Left({ message }: Chat) {
  return (
    <View
      style={{
        marginBottom: 0,
        alignItems: "flex-start",
      }}
    >
      <View
        style={{
          maxWidth: "70%",
          backgroundColor: Colors.white,
          padding: 8,
          borderRadius: 10,
        }}
      >
        <Text>{message}</Text>
      </View>
      <Caption>10.00</Caption>
    </View>
  );
}
function Right({ message }: Chat) {
  return (
    <View
      style={{
        marginBottom: 0,
        alignItems: "flex-end",
      }}
    >
      <View
        style={{
          maxWidth: "70%",
          backgroundColor: Colors.sec,
          padding: 8,
          borderRadius: 10,
        }}
      >
        <Text>{message}</Text>
      </View>
      <Caption>10.00</Caption>
    </View>
  );
}
function Send({ navigation, pengaduanId, open }) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const replySuccess = useSelector(function (state: RootState) {
    return state.complaint.complaintReplySuccess
      ? state.complaint.complaintReplySuccess.success
      : false;
  });
  const closeSuccess = useSelector(function (state: RootState) {
    return state.complaint.complaintCloseSuccess
      ? state.complaint.complaintReplySuccess.success
      : false;
  });
  function onCloseChat() {
    const params = {
      id: `${pengaduanId}`,
      user_id: `${id}`,
    };
    Alert.alert(
      "Attention !",
      "Are you sure to close this conversation",
      [
        {
          text: "Yes",
          onPress: () => {
            dispatch(complaintClose(params));
            navigation.goBack();
          },
        },
        { text: "No", onPress: () => console.log("chat closed") },
      ],
      {
        cancelable: false,
      }
    );
  }
  React.useEffect(() => {
    replySuccess ? dispatch(complaintFetchOne(pengaduanId)) : null;
    closeSuccess ? dispatch(complaintFetchOne(pengaduanId)) : null;
  }, [replySuccess, closeSuccess]);
  return (
    <>
      {open ? (
        <Formik
          initialValues={{
            description: "",
            user_id: id,
            pengaduan_id: pengaduanId,
          }}
          validationSchema={PengaduanSchema}
          onSubmit={function (values) {
            dispatch(complaintReply(values));
          }}
        >
          {({
            handleChange,
            setFieldValue,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => {
            return (
              <View
                style={{
                  backgroundColor: Colors.gray1,
                  width: "100%",
                  position: "absolute",
                  bottom: 0,
                }}
              >
                <TextInput
                  placeholder="Kirim"
                  style={{ flex: 1, padding: 10, fontSize: 20 }}
                  autoFocus={false}
                  blurOnSubmit={true}
                  value={values.description}
                  autoCapitalize="none"
                  onChangeText={handleChange("description")}
                  onBlur={handleBlur("description")}
                />

                {usertype === "Regulator" || usertype === "Vessel Operator" ? (
                  <View
                    style={{
                      flexDirection: "row",
                    }}
                  >
                    <Button
                      mode="contained"
                      color={Colors.danger}
                      style={{ flex: 1 }}
                      onPress={function () {
                        onCloseChat();
                      }}
                    >
                      Close Chat
                    </Button>
                  </View>
                ) : null}

                <Button
                  mode="contained"
                  color={Colors.pri}
                  style={{ flex: 1 }}
                  onPress={handleSubmit}
                >
                  Kirim
                </Button>
              </View>
            );
          }}
        </Formik>
      ) : (
        <Caption style={{ textAlign: "center" }}>Chat closed</Caption>
      )}
    </>
  );
}
function ReadPengaduan({ navigation, route }) {
  const { pengaduan_id } = route.params;
  const dispatch = useDispatch();
  const pengaduan = useSelector(function (state: RootState) {
    return state.complaint.complaintViewSuccess
      ? state.complaint.complaintViewSuccess.data
      : { reply: [{}] };
  });
  const open = useSelector(function (state: RootState) {
    return state.complaint.complaintViewSuccess
      ? state.complaint.complaintViewSuccess.data.status_is_open
      : false;
  });
  React.useEffect(() => {
    // alert(pengaduan_id);
    dispatch(complaintFetchOne(pengaduan_id));
  }, []);
  return (
    <Root style={styles.container}>
      <Card style={styles.card}>
        <Caption>
          Dari:{" "}
          <Paragraph>
            {pengaduan.nama_perusahaan_consignee ||
              pengaduan.nama_perusahaan_supplier ||
              pengaduan.nama_perusahaan_shipper ||
              pengaduan.nama_perusahaan_reseller}
          </Paragraph>
        </Caption>
        <Caption>
          Ke:{" "}
          <Paragraph>
            {pengaduan.nama_perusahaan_vessel_operator ||
              pengaduan.nama_regulator}
          </Paragraph>
        </Caption>
        <Caption>
          Judul: <Paragraph>{pengaduan.subject}</Paragraph>
        </Caption>
        <Caption>
          Tanggal: <Paragraph>{DateTimeFormat(pengaduan.timestamp)}</Paragraph>
        </Caption>
        <Caption>
          Deskripsi: <Paragraph>{pengaduan.description}</Paragraph>
        </Caption>
      </Card>
      <Padding size={10}>
        {pengaduan.reply.map(function (item, key) {
          return item.position == "sender" ? (
            <Right key={key} message={item.description} />
          ) : (
            <Left key={key} message={item.description} />
          );
        })}
      </Padding>
      <Send navigation={navigation} pengaduanId={pengaduan_id} open={open} />
    </Root>
  );
}

export default ReadPengaduan;

const styles = StyleSheet.create({
  container: {
    padding: 0,
    backgroundColor: Colors.grayL,
  },
  card: {
    padding: 10,
  },
});
