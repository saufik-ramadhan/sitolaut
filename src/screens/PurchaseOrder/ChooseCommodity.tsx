import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Dimensions,
  Image,
} from "react-native";
import {
  Card,
  Paragraph,
  Caption,
  Button,
  IconButton,
  Badge,
  TextInput,
} from "react-native-paper";
import Colors from "../../config/Colors";
import { CreatePurchaseOrderProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import { commodityFetchPage } from "../../redux/commodityReducer";
import { RootState } from "../../redux/rootReducer";
import { addCartItem, deleteCartItem } from "../../redux/cartReducer";
import { formatRupiah } from "../../services/utils";
import { FilterButton, SelectInput, Space } from "../../components";
import {Formik} from 'formik';
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";

function CommodityList({
  navigation,
  route,
}: CreatePurchaseOrderProps<"ChooseCommodity">) {
  const dispatch = useDispatch();
  const { supplier_id } = route.params;
  const commodityList = useSelector(function (state: RootState) {
    return state.commodity.commodityGetSuccess
      ? state.commodity.commodityGetSuccess.data
      : [];
  });
  const cartItems = useSelector(function (state: RootState) {
    return state.cart.items;
  });
  /**
   * Search element inside array of object
   * @param nameKey
   * @param myArray
   */
  function search(id, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].id === id) {
        return myArray[i];
      }
    }
  }
  React.useEffect(() => {
    dispatch(commodityFetchPage({ user_id_supplier: supplier_id }));
  }, []);
  const dummyData = [
    {
      id: 1,
      name: "Gula Pasir",
      url: require("../../assets/img/gula.jpg"),
    },
    {
      id: 2,
      name: "Beras Putih",
      url: require("../../assets/img/beras-wangi.jpg"),
    },
    {
      id: 3,
      name: "Minyak Goreng",
      url: require("../../assets/img/minyak.jpg"),
    },
    {
      id: 4,
      name: "Beras Merah",
      url: require("../../assets/img/beras-merah.jpg"),
    },
    {
      id: 5,
      name: "Tepung Terigu",
      url: require("../../assets/img/tepung.jpg"),
    },
  ];
  return (
    <FlatList
      data={commodityList}
      ListEmptyComponent={
        <Caption style={{ textAlign: "center" }}>No Data</Caption>
      }
      numColumns={2}
      columnWrapperStyle={{}}
      renderItem={function (item) {
        return (
          <Card
            style={{
              width: Dimensions.get("window").width / 2 - 6,
              paddingHorizontal: 5,
              paddingVertical: 8,
              marginHorizontal: 3,
              marginVertical: 3,
              elevation: 3,
            }}
          >
            {/* <Image
              source={item.item.url}
              style={{ width: "100%", height: 150 }}
            /> */}
            <Paragraph
              style={{
                color: Colors.pri,
                fontWeight: "bold",
                fontSize: 16,
                fontFamily: "Verdana",
              }}
            >
              {item.item.nama_barang}
            </Paragraph>
            <Caption>
              <Caption
                style={{ color: "orange", fontWeight: "bold", fontSize: 15 }}
              >
                Rp. {formatRupiah(parseInt(item.item.harga_satuan))} /{" "}
                {item.item.satuan} {"\n"}
              </Caption>
              Min. Order {item.item.minimum_order}
            </Caption>

            {cartItems.find((o) => o.id === item.item.id) === undefined ? (
              <Button
                mode="contained"
                onPress={function () {
                  dispatch(addCartItem({ ...item.item, count: 0 }));
                }}
              >
                Order
              </Button>
            ) : (
              <Button
                mode="contained"
                onPress={function () {
                  dispatch(deleteCartItem(item.item.id));
                }}
                color={Colors.danger}
              >
                Hapus
              </Button>
            )}
          </Card>
        );
      }}
      keyExtractor={function (item) {
        return item.id;
      }}
    />
  );
}

function FloatingCheckoutButton({
  navigation,
}: CreatePurchaseOrderProps<"ChooseCommodity">) {
  const cartItems = useSelector(function (state: RootState) {
    return state.cart.items;
  });
  return (
    <View style={{ position: "absolute", bottom: 10, right: 10 }}>
      <Badge style={{ position: "absolute" }}>{cartItems.length}</Badge>
      <IconButton
        icon="cart"
        size={40}
        style={{
          backgroundColor: Colors.sec,
          elevation: 3,
        }}
        onPress={function () {
          navigation.navigate("CommoditySummary");
        }}
      />
    </View>
  );
}

export default function ChooseCommodity(
  props: CreatePurchaseOrderProps<"ChooseCommodity">
) {
  const cartItems = useSelector(function (state: RootState) {
    return state.cart.items;
  });
  const dispatch = useDispatch();
  const { supplier_id } = props.route.params;
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const { jenisbarang } = useSelector((state: RootState) => state);
  React.useEffect(() => {
    dispatch(jenisbarangFetchAll());
  }, [])
  return (
    <View style={styles.container}>
      <Formik
        initialValues={{
          id_jenis_barang: "",
          nama_barang: "",
          user_id_supplier: supplier_id,
        }}
        onSubmit={(values) => dispatch(commodityFetchPage(values))}
      >
        {
          ({handleSubmit, handleBlur, handleChange, setFieldValue,values}) => (
            <View style={{margin: 10}}>
              <SelectInput
                options={jenisbarang.jenisbarangGetSuccess.data}
                onChangeValue={function(val){setFieldValue('id_jenis_barang', val.id)}}
                objectKey="label"
                label="Jenis barang"
              />
              <Space height={5}/>
              <TextInput
                dense
                label="Nama barang"
                onChangeText={handleChange('nama_barang')}
                value={values.nama_barang}
              />
              <Space height={5}/>
              <Button onPress={handleSubmit}>Cari</Button>
            </View>
          )
        }
      </Formik>
      <CommodityList {...props} />
      {cartItems == false ? null : <FloatingCheckoutButton {...props} />}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
});
