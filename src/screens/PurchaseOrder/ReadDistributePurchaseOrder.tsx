import React, { useState } from "react";
import { StyleSheet, Text, View, Image, Dimensions } from "react-native";
import {
  Root,
  DateTime,
  Space,
  FileInput,
  ErrorText,
  Icon,
  SelectInput,
} from "../../components";
import {
  Card,
  Caption,
  Badge,
  Paragraph,
  TextInput,
  Button,
  IconButton,
  Modal,
} from "react-native-paper";
import Colors from "../../config/Colors";
import { DateFormat, DateFormat4 } from "../../services/utils";
import { Formik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import { detailpoFetchDistribution } from "../../redux/detailpoReducer";
import {
  bookingFetchConsigneeReceived,
  bookingFilterClear,
  bookingFilterAdd,
  bookingHasDoView,
} from "../../redux/bookingReducer";
import { RootState } from "../../redux/rootReducer";
import {
  distributionFetchSub,
  distributionAdd,
} from "../../redux/distributionReducer";

function KirimBarang({ show, onDismiss, data, reseller, id_booking }) {
  const dispatch = useDispatch();
  return (
    <Modal
      visible={show}
      onDismiss={onDismiss}
      contentContainerStyle={{
        backgroundColor: "white",
        margin: 10,
        padding: 10,
      }}
    >
      <Caption>Nama Barang</Caption>
      <Paragraph>{data.nama_barang}</Paragraph>
      <Formik
        initialValues={{
          detail_po_id: data.id,
          id_booking: id_booking,
          nama_barang: data.nama_barang,
          qty: "",
          reseller_id: "",

          // detail_po_id: 19403
          // id_booking: 6166
          // nama_barang: "Gula Sweet"
          // qty: "1"
          // reseller_id: "3989"
        }}
        onSubmit={function (val) {
          dispatch(distributionAdd(val));
          onDismiss();
        }}
      >
        {({
          values,
          handleBlur,
          handleChange,
          handleSubmit,
          setFieldValue,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Kuantitas
             */}
            <TextInput
              value={values.qty}
              onChangeText={handleChange("qty")}
              onBlur={handleBlur("qty")}
              placeholder="Kuantitas"
            />
            {errors.qty && touched.qty && <ErrorText>{errors.qty}</ErrorText>}

            {/**
             * Reseller
             */}
            <SelectInput
              label={"Reseller"}
              onChangeValue={(val) =>
                setFieldValue("reseller_id", String(val.id))
              }
              options={reseller}
              objectKey="nama_perusahaan"
              withSearch
            />

            <Button onPress={handleSubmit}>Kirim</Button>
          </>
        )}
      </Formik>
    </Modal>
  );
}

export default function ReadDistributePurchaseOrder({ navigation, route }) {
  const { item, reseller } = route.params;
  const dispatch = useDispatch();
  const [modal, setModal] = useState({});
  const [show, setShow] = useState(false);
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const detailpo = useSelector(function (state: RootState) {
    return state.detailpo.detailpoGetSuccess
      ? state.detailpo.detailpoGetSuccess.data
      : [];
  });
  const distribution = useSelector(function (state: RootState) {
    return state.distribution.distributionGetSuccess
      ? state.distribution.distributionGetSuccess.data
      : [];
  });
  const bookingview = useSelector(function (state: RootState) {
    return state.booking.bookingHasDoViewSuccess
      ? state.booking.bookingHasDoViewSuccess.data
      : {};
  });
  const distributionAddSuccess = useSelector(function (state: RootState) {
    return state.distribution.distributionAddSuccess
      ? state.distribution.distributionAddSuccess
      : false;
  });
  const fetchData = function fetchData() {
    const params = {
      consignee_id: `${id}`,
      id: `${item.booking_has_po_id}`,
      booking_id: item.booking_id,
    };
    const params2 = {
      consignee_id: `${id}`,
      booking_id: item.booking_id,
    }
    dispatch(detailpoFetchDistribution(params));
    dispatch(distributionFetchSub(params2));
    dispatch(bookingHasDoView({ id: item.booking_id }));
  };
  React.useEffect(() => {
    fetchData();
  }, [distributionAddSuccess]);
  return (
    <>
      <Root style={{ backgroundColor: Colors.grayL }}>
        <Card style={styles.card}>
          <Caption>Nomor PO</Caption>
          <Paragraph>{item.no_po}</Paragraph>
          <Caption>Status</Caption>
          <Badge
            style={{
              alignSelf: "flex-start",
              backgroundColor: item.is_received ? Colors.success : Colors.grayL,
            }}
          >
            {item.is_received
              ? "Sudah diterima consignee"
              : "Belum diterima consignee"}
          </Badge>
          <Caption>Tanggal PO</Caption>
          <Paragraph>{DateFormat(item.date_po)}</Paragraph>
          <Caption>Supplier</Caption>
          <Paragraph>{item.nama_perusahaan}</Paragraph>
          <Caption>Shipper</Caption>
          <Paragraph>{item.shipper_name}</Paragraph>
          <Caption>Rute</Caption>
          <Paragraph>
            {item.origin} ➤ {item.destination}
          </Paragraph>
        </Card>

        {/**
         * DAFTAR BARANG
         */}
        <Card style={styles.card}>
          <Paragraph>Daftar Barang</Paragraph>
          {/**
           * Field Name
           */}
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Caption style={styles.fieldname}>Kode</Caption>
            </View>
            <View style={{ flex: 2 }}>
              <Caption style={styles.fieldname}>Nama Barang</Caption>
            </View>
            <View style={{ flex: 1 }}>
              <Caption style={styles.fieldname}>Sisa</Caption>
            </View>
            <View style={{ flex: 1 }}></View>
          </View>

          {/**
           * ROWS
           */}
          {detailpo.map(function (item, key) {
            return (
              <View style={{ flexDirection: "row" }} key={key}>
                <View style={[{ flex: 1 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>
                    {item.kode_barang}
                  </Paragraph>
                </View>
                <View style={[{ flex: 2 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>
                    {item.nama_barang}
                  </Paragraph>
                </View>
                <View style={[{ flex: 1 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>
                    {item.sisa_barang}
                  </Paragraph>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {item.sisa_barang > 0 ? (
                    <IconButton
                      color={"green"}
                      icon="cube-send"
                      size={20}
                      onPress={function () {
                        setShow(true);
                        setModal({ ...item, ...bookingview });
                      }}
                    />
                  ) : null}
                </View>
              </View>
            );
          })}
        </Card>

        {/**
         * DAFTAR DISTRIBUSI
         */}
        <Card style={styles.card}>
          <Paragraph>Daftar Pendistribusian Barang</Paragraph>
          {/**
           * Field Name
           */}
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <Caption style={styles.fieldname}>
                <Icon name={"calendar"} />
              </Caption>
            </View>
            <View style={{ flex: 1 }}>
              <Caption style={styles.fieldname}>Kode</Caption>
            </View>
            <View style={{ flex: 2 }}>
              <Caption style={styles.fieldname}>Barang</Caption>
            </View>
            <View style={{ flex: 1 }}>
              <Caption style={styles.fieldname}>Qty</Caption>
            </View>
            <View style={{ flex: 2 }}>
              <Caption style={styles.fieldname}>Reseller</Caption>
            </View>
          </View>

          {/**
           * ROWS
           */}
          {distribution.map(function (item, key) {
            return (
              <View style={{ flexDirection: "row" }} key={key}>
                <View style={[{ flex: 1 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>
                    {DateFormat4(item.tgl_distribusi)}
                  </Paragraph>
                </View>
                <View style={[{ flex: 1 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>
                    {item.kode_barang}
                  </Paragraph>
                </View>
                <View style={[{ flex: 2 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>
                    {item.nama_barang}
                  </Paragraph>
                </View>
                <View style={[{ flex: 1 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>{item.qty}</Paragraph>
                </View>
                <View style={[{ flex: 2 }, styles.item]}>
                  <Paragraph style={styles.rowitem}>
                    {item.nama_perusahaan_reseller}
                  </Paragraph>
                </View>
              </View>
            );
          })}
        </Card>
      </Root>
      <KirimBarang
        data={modal}
        onDismiss={function () {
          setShow(false);
        }}
        show={show}
        reseller={reseller}
        id_booking={item.booking_id}
      />
    </>
  );
}

const styles = StyleSheet.create({
  fieldname: {
    textAlign: "center",
  },
  rowitem: {
    textAlign: "center",
  },
  item: {
    justifyContent: "center",
    alignItems: "center",
  },
  card: {
    padding: 10,
    marginBottom: 3,
  },
});
