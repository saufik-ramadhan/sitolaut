import React, { useState } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Space, FilterButton, EmptyState } from "../../components";
import {
  Card,
  Paragraph,
  Caption,
  IconButton,
  Badge,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { ConsigneeStackProps } from "../Navigator";
import { purchaseFetchPage } from "../../redux/purchaseReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import { distributionFetchReseller } from "../../redux/distributionReducer";
import { bookingFetchConsigneeReceived, bookingFilterClear } from "./../../redux/bookingReducer";
import { DateFormat } from "../../services/utils";
import { shipperFetchAll } from "../../redux/shipperReducer";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { portFetchAll } from "../../redux/portReducer";
import { resellerFetchAll } from "../../redux/resellerReducer";

function DistributePurchaseOrderList({
  navigation,
}: ConsigneeStackProps<"DistributePurchaseOrder">) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const booking = useSelector(function (state: RootState) {
    return state.booking.bookingData ? state.booking.bookingData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.booking.bookingGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.booking.bookingFilter;
  });
  const listSupplier = useSelector(function (state: RootState) {
    return state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : [];
  });
  const listShipper = useSelector(function (state: RootState) {
    return state.shipper.shipperGetSuccess
      ? state.shipper.shipperGetSuccess.data
      : [];
  });
  const listReseller = useSelector(function (state: RootState) {
    return state.reseller.resellerGetSuccess
      ? state.reseller.resellerGetSuccess.data
      : [];
  });
  const listPort = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  const loading = useSelector(function (state: RootState) {
    return state.booking.bookingGetLoading;
  });

  function fetchData(filter) {
    const params = {
      length: 10,
      id: `${id}`,
      ...filter,
    };
    dispatch(distributionFetchReseller(params));
    dispatch(bookingFetchConsigneeReceived(params));
    dispatch(shipperFetchAll());
    dispatch(supplierFetchAll());
    dispatch(resellerFetchAll());
    dispatch(portFetchAll());
  }

  function handleReachEnd(filter) {
    const params = {
      length: 10,
      id: `${id}`,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(distributionFetchReseller(params));
  }

  const onRefresh = () => {
    dispatch(bookingFilterClear());
  };

  React.useEffect(() => {
    filter === {} ? null : fetchData(filter);
  }, [filter]);

  return (
    <>
      <FlatList
        data={booking}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                navigation.navigate("ReadDistributePurchaseOrder", {
                  item: item,
                  reseller: listReseller,
                });
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Paragraph
                  selectable
                  style={{ fontWeight: "bold", fontSize: 16, flex: 3 }}
                >
                  No : {item.no_po}
                </Paragraph>
                <Caption style={{ flex: 1 }}>
                  {DateFormat(item.date_po)}{" "}
                </Caption>
              </View>
              <Badge
                style={{
                  alignSelf: "flex-start",
                  backgroundColor: item.is_received
                    ? Colors.success
                    : Colors.grayL,
                }}
              >
                {item.is_received
                  ? "Sudah diterima consignee"
                  : "Belum diterima consignee"}
              </Badge>
              <Caption style={{ fontSize: 13 }}>
                Supplier: {item.nama_perusahaan}
                {"\n"}
                JPT: {item.shipper_name}
                {"\n"}
                <Caption>
                  {item.origin} ➤ {item.destination}
                </Caption>
              </Caption>
            </Card>
          );
        }}
      />

      <FilterButton
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "DistributePurchaseOrderFilterModal",
            params: {
              shipper: listShipper,
              port: listPort,
              supplier: listSupplier,
            },
          });
        }}
      />
    </>
  );
}

const DistributePurchaseOrder = ({
  navigation,
  route,
}: ConsigneeStackProps<"DistributePurchaseOrder">) => {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });

  return (
    <View style={styles.container}>
      <DistributePurchaseOrderList navigation={navigation} route={route} />
    </View>
  );
};

export default DistributePurchaseOrder;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    padding: 10,
    borderRadius: 5,
    margin: 5,
  },
});
