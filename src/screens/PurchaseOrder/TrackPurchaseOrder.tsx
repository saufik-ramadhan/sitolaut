import React, { useState, useCallback } from "react";
import { View, FlatList, ActivityIndicator } from "react-native";
import { Card, Paragraph, Button, Caption } from "react-native-paper";
import Colors from "../../config/Colors";
import { Space, FilterButton } from "../../components";
import { useDispatch, useSelector } from "react-redux";
import { trackFetchPage, trackFilterDel } from "../../redux/trackReducer";
import { RootState } from "../../redux/rootReducer";
import { trackFilterClear } from "./../../redux/trackReducer";

export default function TrackPurchaseOrder({ navigation, route }) {
  const dispatch = useDispatch();
  const loading = useSelector(function (state: RootState) {
    return state.track.trackGetLoading;
  });
  const tracklList = useSelector(function (state: RootState) {
    return state.track.trackData ? state.track.trackData : [];
  });
  const filter = useSelector(function (state: RootState) {
    return state.track.trackFilter ? state.track.trackFilter : {};
  });
  const refresh = useSelector(function (state: RootState) {
    return state.track.trackGetLoading;
  });

  const {
    currentPage,
    hasNext,
    hasPrev,
    pageTotal,
    recordsTotal,
  } = useSelector(function (state: RootState) {
    return state.track.trackGetSuccess ? state.track.trackGetSuccess : false;
  });

  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : [{}];
  });

  /** Fetching data */
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      user_id_consignee: usertype == "Consignee" ? `${id}` : "",
      user_id_supplier: usertype == "Supplier" ? `${id}` : "",
      ...filter,
    };
    dispatch(trackFetchPage(params));
  }
  function handleReachEnd(filter) {
    dispatch(
      trackFetchPage({
        length: 10,
        start: currentPage * 10,
        user_id_consignee: usertype == "Consignee" ? `${id}` : "",
        user_id_supplier: usertype == "Supplier" ? `${id}` : "",
        ...filter,
      })
    );
  }

  function onRefresh() {
    dispatch(trackFilterDel());
  }

  React.useEffect(() => {
    fetchData(filter);
  }, [filter]);

  return (
    <>
      {/* <Caption>{JSON.stringify(tracklList)}</Caption> */}
      <FlatList
        data={tracklList}
        refreshing={refresh}
        onRefresh={onRefresh}
        maxToRenderPerBatch={6}
        onEndReachedThreshold={0.5}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        ListFooterComponent={
          <>
            {loading ? (
              <>
                <Space height={20} />
                <ActivityIndicator />
              </>
            ) : null}

            <Space height={100} />
          </>
        }
        renderItem={function (item) {
          return (
            <Card
              elevation={4}
              style={{
                backgroundColor: "white",
                elevation: 3,
                padding: 10,
                borderRadius: 5,
                margin: 5,
              }}
              onPress={function () {
                navigation.navigate("TrackViewPurchaseOrder", {
                  id: item.item.id,
                });
              }}
            >
              <View>
                <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                  No : {item.item.no_po}
                </Paragraph>
                <Caption style={{ fontSize: 13 }}>
                  {
                    usertype == "Consignee" ? `Supplier: ${item.item.supplier_name}` : `Consignee: ${item.item.consignee_name}`
                  }
                  {"\n"}
                  JPT: {item.item.shipper_name ? item.item.shipper_name : "-"}
                  {"\n"}
                  <Caption>
                    {item.item.origin}
                    {item.item.port_origin_code} ➡️ {item.item.destination}
                    {item.item.port_destination_code}
                  </Caption>
                </Caption>
              </View>
            </Card>
          );
        }}
        keyExtractor={function (item, key) {
          return String(key);
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "TrackPurchaseOrderFilterModal",
          });
        }}
      />
    </>
  );
}
