import React from "react";
import { StyleSheet, Text, View, FlatList, Image } from "react-native";
import { CreatePurchaseOrderProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Caption, Card, Paragraph, IconButton } from "react-native-paper";
import { scheduleFetchWill } from "../../redux/scheduleReducer";
import Colors from "./../../config/Colors";

export default function ChooseTrayek({
  navigation,
  route,
}: CreatePurchaseOrderProps<"ChooseTrayek">) {
  // const {port_origin_id, port_destination_id} = route.params;
  const dispatch = useDispatch();
  const schedule = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetSuccess.data
      ? state.schedule.scheduleGetSuccess.data
      : false;
  });
  React.useEffect(() => {
    dispatch(scheduleFetchWill(route.params));
  }, []);
  return (
    <FlatList
      ListEmptyComponent={
        <Caption style={{ textAlign: "center" }}>
          No Data "Search to find data"
        </Caption>
      }
      data={schedule}
      style={{ height: "100%" }}
      showsVerticalScrollIndicator={false}
      ListFooterComponent={<View style={{ height: 50 }} />}
      renderItem={function ({ item }) {
        return (
          <Card
            onPress={function () {
              null;
            }}
            style={{
              borderRadius: 0,
              marginBottom: 5,
              elevation: 3,
              borderLeftColor: Colors.pri,
              borderLeftWidth: 8,
              padding: 5,
            }}
          >
            <View
              style={{
                flexDirection: "row",
              }}
            >
              {/** Logo Perusahaan */}
              <View style={{ flex: 2, justifyContent: "center" }}>
                <Image
                  source={require("../../assets/img/download.png")}
                  style={{
                    height: 50,
                    width: 50,
                  }}
                />
              </View>
              {/** Detail Supplier */}
              <View style={{ flex: 7 }}>
                <Paragraph style={{ fontSize: 16 }}>
                  {item.nama_perusahaan}
                </Paragraph>

                <Caption style={{ color: "#888", fontSize: 13 }}>
                  {item.kota}
                  {"\n"}
                  {item.provinsi}
                </Caption>
              </View>
              {/** Choose Button */}
              <View style={{ justifyContent: "center" }}>
                <IconButton icon="chevron-right" onPress={function () {}} />
              </View>
              {/** --------- */}
            </View>
          </Card>
        );
      }}
      keyExtractor={function (item, key) {
        return String(key);
      }}
    />
  );
}

const styles = StyleSheet.create({});
