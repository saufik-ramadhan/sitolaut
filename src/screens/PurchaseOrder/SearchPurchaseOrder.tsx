import React, { useState, ComponentProps } from "react";
import { View, StyleSheet, Keyboard } from "react-native";
import {
  Card,
  TextInput,
  RadioButton,
  Paragraph,
  Button,
  List,
  Divider,
} from "react-native-paper";
import {
  DateTime,
  Space,
  AutoCompletePicker,
  SearchModal as PickerCompany,
  SelectInput,
} from "../../components";
import { useSelector, connect } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import Colors from "../../config/Colors";
import moment from "moment";
import { getUserType } from "../../services/utils";
import { Picker } from "@react-native-community/picker";

function SearchPurchaseOrder(props: any) {
  const {
    id,
    usertype,
    visible,
    onSearch,
    onClose,
    consignee,
    supplier,
  } = props;
  const { isShipper, isSupplier, isConsignee } = getUserType(usertype);
  const isAprove: any = isShipper ? 1 : 2;
  const isApproveByShipper: any = isShipper ? 2 : 0;
  const [searchForm, setSearchForm] = useState({
    dari: "",
    is_approve: "",
    is_approve_by_shipper: "",
    length: 10,
    no_po: "",
    sampai: "",
    consignee_id: getUserType(usertype).isConsignee ? id : "",
    supplier_id: getUserType(usertype).isSupplier ? id : "",
    shipper_id: getUserType(usertype).isShipper ? id : "",
    reseller_id: getUserType(usertype).isReseller ? id : "",
    start: 0,
  });
  const [status, setStatus] = useState({
    pending: false,
    accepted: false,
    rejected: false,
  });
  const [pickUser, setPickUser] = useState(0);
  const { pending, accepted, rejected } = status;
  const { no_po } = searchForm;

  const pendings = isShipper ? "1" : "0";
  const onPressPending = () => {
    setStatus({ ...status, pending: true, accepted: false, rejected: false });
    setSearchForm({
      ...searchForm,
      ...{ is_approve: pendings, is_approve_by_shipper: "0" },
    });
  };
  const onPressAccepted = () => {
    setStatus({ ...status, accepted: true, pending: false, rejected: false });
    setSearchForm({
      ...searchForm,
      ...{ is_approve: "1", is_approve_by_shipper: "1" },
    });
  };
  const onPressRejected = () => {
    setStatus({ ...status, rejected: true, accepted: false, pending: false });
    setSearchForm({
      ...searchForm,
      ...{ is_approve: isAprove, is_approve_by_shipper: isApproveByShipper },
    });
  };
  const RBColor = accepted
    ? Colors.accepted
    : rejected
    ? Colors.danger
    : Colors.gray10;
  const setDateFormat = (date: any) => moment(date).format("yyyy-MM-DD");

  const resetForm = () => {
    setStatus({
      pending: false,
      accepted: false,
      rejected: false,
    });
    setSearchForm({
      ...searchForm,
      ...{
        no_po: "",
        dari: setDateFormat(new Date()),
        sampai: setDateFormat(new Date()),
      },
    });
  };
  const onPressClose = () => {
    resetForm();
    onClose();
  };

  const PickerDefaultValue = isConsignee ? "Supplier" : "Consignee";
  const showPicker = isConsignee || isSupplier;
  const onChangePicker = (_id: any) => {
    isConsignee
      ? setSearchForm({
          ...searchForm,
          ...{ supplier_id: _id },
        })
      : setSearchForm({
          ...searchForm,
          ...{ consignee_id: _id },
        });
  };
  const pickerItem = isConsignee ? supplier : consignee;
  // console.log(consignee)

  return visible ? (
    <View style={{ margin: 5 }}>
      <Card>
        <Card.Content>
          <TextInput
            label="No. PO"
            dense
            value={no_po}
            onChangeText={(text: string) => {
              setSearchForm({ ...searchForm, no_po: text });
            }}
          />
          <View style={styles.statusWrapper}>
            <CostumRadioButton
              value="Menunggu"
              status={pending ? "checked" : "unchecked"}
              onPress={onPressPending}
              color={RBColor}
            />
            <CostumRadioButton
              value="Diterima"
              status={accepted ? "checked" : "unchecked"}
              onPress={onPressAccepted}
              color={RBColor}
            />
            <CostumRadioButton
              value="Ditolak"
              status={rejected ? "checked" : "unchecked"}
              onPress={onPressRejected}
              color={RBColor}
            />
          </View>
          {showPicker && (
            <SelectInput
              onChangeValue={(item) => {
                onChangePicker(item.id);
                console.log(item.id);
              }}
              options={pickerItem}
              label={PickerDefaultValue}
              withSearch
              objectKey="nama_perusahaan"
            />
          )}
          <Space height={10} />
          <DateTime
            label="Dari Tanggal"
            onChangeValue={(date: any) =>
              setSearchForm({ ...searchForm, dari: setDateFormat(date) })
            }
          />
          <Space height={10} />
          <DateTime
            label="Sampai Tanggal"
            onChangeValue={(date: any) =>
              setSearchForm({ ...searchForm, sampai: setDateFormat(date) })
            }
          />

          <Space height={10} />
          <View style={styles.row}>
            <Button
              style={styles.btn}
              mode="outlined"
              icon="close"
              onPress={onPressClose}
            >
              Tutup
            </Button>
            <Button
              style={styles.btn}
              onPress={function () {
                onSearch(searchForm);
                Keyboard.dismiss();
              }}
              icon="magnify"
              mode="contained"
            >
              Cari
            </Button>
          </View>
        </Card.Content>
      </Card>
    </View>
  ) : null;
}

type radioButton = ComponentProps<typeof RadioButton>;
const CostumRadioButton = ({
  value,
  onPress,
  color,
  ...props
}: radioButton) => {
  return (
    <View style={styles.RBCostum}>
      <RadioButton value="first" {...props} onPress={onPress} color={color} />
      <Paragraph onPress={onPress}>{value}</Paragraph>
    </View>
  );
};

const styles = StyleSheet.create({
  statusWrapper: {
    padding: 5,
    flexDirection: "row",
    flexWrap: "wrap",
  },
  RBCostum: { flexDirection: "row", alignItems: "center", flex: 1 },
  row: {
    flexDirection: "row",
  },
  btn: { flex: 1, marginHorizontal: 2 },
});

const mapState = (state: any) => ({
  id: state?.auth?.authLoginSuccess?.data.user[0].id,
  usertype: state?.auth?.authLoginSuccess?.data.user[0].usertype,
});
const mapDispatch = (dispatch: any) => ({});
export default connect(mapState, mapDispatch)(SearchPurchaseOrder);
