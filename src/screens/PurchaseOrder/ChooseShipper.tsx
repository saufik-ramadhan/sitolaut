import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Image,
  ScrollView,
  TextInput,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { portFetchAll } from "../../redux/portReducer";
import { RootState } from "../../redux/rootReducer";
import {
  Card,
  Button,
  Paragraph,
  Caption,
  IconButton,
  DataTable,
} from "react-native-paper";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import Colors from "./../../config/Colors";
import { ErrorText, Space, Icon, Root, SelectInput } from "../../components";
import { CreatePurchaseOrderProps } from "../Navigator";
import {
  shipperFetchAll,
  shipperPriceFetchAll,
  shipperClear,
} from "./../../redux/shipperReducer";
import {
  formatRupiah,
  DateFormat,
  TimeFormat,
  DateTimeFormat,
} from "../../services/utils";
import { addJpt, deleteJpt } from "./../../redux/jptReducer";
import { scheduleFetchWill, scheduleClear } from "../../redux/scheduleReducer";
import { deleteJadwal, addJadwal, addQuota } from "../../redux/jadwalReducer";
import { KeyboardAwareInsetsView } from "react-native-keyboard-tracking-view";
import { max } from "lodash";
import { consigneeFetchUser } from "../../redux/consigneeReducer";

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

function ShipperFilter() {
  const [portOrigin, setPortOrigin] = React.useState(0);
  const [portDestination, setPortDestination] = React.useState(0);
  /**
   * Get Port
   */
  const dispatch = useDispatch();
  const ports = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  const { choosesupplier, consignee } = useSelector((state: RootState) => state);
  const portOfLoading = () => {
    return ports.filter((item) => item.provinsi_id == choosesupplier.data.provinsi_id);
  }
  const portOfDischarge = () => {
    return ports.filter((item) => item.provinsi_id == consignee.consigneeViewSuccess.data[0].provinsi_id);
  }
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  React.useEffect(() => {
    dispatch(portFetchAll());
    dispatch(consigneeFetchUser(id));
    dispatch(shipperClear());
    dispatch(scheduleClear());
  }, []);
  if(!consignee.consigneeViewSuccess.data) return null;
  return (
    <Card style={{ padding: 10, elevation: 3 }}>
      <Formik
        initialValues={{ port_destination_id: 0, port_origin_id: 0 }}
        onSubmit={function (values) {
          if (values.port_destination_id == 0 || values.port_origin_id == 0)
            null;
          else {
            dispatch(shipperPriceFetchAll(values));
            dispatch(scheduleFetchWill(values));
          }
        }}
      >
        {function ({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) {
          return (
            <>
              {/**
               * Port Origin
               */}
              <SelectInput
                options={portOfLoading() || []}
                onChangeValue={function(val){setFieldValue('port_origin_id', val.id)}}
                label="Port of Loading (PoL)"
                objectKey="label"
                withSearch
              />
              <SelectInput
                options={portOfDischarge() || []}
                onChangeValue={function(val){setFieldValue('port_destination_id', val.id)}}
                label="Port of Discharge (PoD)"
                objectKey="label"
                withSearch
              />
              {/* <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  selectedValue={portOrigin}
                  style={{ flex: 10 }}
                  itemStyle={{ borderRadius: 5 }}
                  mode="dropdown"
                  onValueChange={function (itemValue: number) {
                    setFieldValue("port_origin_id", itemValue);
                    setPortOrigin(itemValue);
                  }}
                >
                  <Picker.Item label="POL" value={0} />
                  {portOfLoading().map(function (item, key) {
                    return (
                      <Picker.Item
                        label={item.label}
                        value={item.id}
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </View>
              {errors.port_origin_id && touched.port_origin_id && (
                <ErrorText>{errors.port_origin_id}</ErrorText>
              )}
              <Space height={10} /> */}

              {/**
               * Port Destination
               */}
              {/* <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  selectedValue={portDestination}
                  style={{ flex: 10 }}
                  itemStyle={{ borderRadius: 5 }}
                  mode="dropdown"
                  onValueChange={function (itemValue: number) {
                    setFieldValue("port_destination_id", itemValue);
                    setPortDestination(itemValue);
                  }}
                >
                  <Picker.Item label="POD" value={0} />
                  {portOfDischarge().map(function (item, key) {
                    return (
                      <Picker.Item
                        label={item.label}
                        value={item.id}
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </View>
              {errors.port_destination_id && touched.port_destination_id && (
                <ErrorText>{errors.port_destination_id}</ErrorText>
              )} */}
              <Space height={10} />
              {values.port_destination_id == 0 || values.port_origin_id == 0 ? (
                <Button onPress={handleSubmit} mode="contained" disabled={true}>
                  Search
                </Button>
              ) : (
                <Button onPress={handleSubmit} mode="contained">
                  Search
                </Button>
              )}
            </>
          );
        }}
      </Formik>
    </Card>
  );
}

function ShipperList({
  navigation,
  route,
}: CreatePurchaseOrderProps<"ChooseSupplier">) {
  const dispatch = useDispatch();
  const shippers = useSelector(function (state: RootState) {
    return state.shipper.shipperGetSuccess
      ? state.shipper.shipperGetSuccess.data
      : [];
  });
  const jptlist = useSelector(function (state: RootState) {
    return state.jpt.data ? state.jpt.data : false;
  });
  React.useEffect(() => {
    // dispatch(shipperFetchAll());
  }, []);
  return (
    <>
      {shippers == false ? (
        <Caption style={{ textAlign: "center" }}>JPT tidak ditemukan</Caption>
      ) : (
        <DataTable>
          <DataTable.Header>
            <DataTable.Title>Nama Shipper</DataTable.Title>
            <DataTable.Title numeric>Tarif</DataTable.Title>
            <DataTable.Title numeric>Action</DataTable.Title>
          </DataTable.Header>

          {shippers.map((item, key) => {
            return (
              <DataTable.Row key={key}>
                <DataTable.Cell>{item.nama_perusahaan}</DataTable.Cell>
                <DataTable.Cell numeric>
                  Rp. {formatRupiah(parseInt(item.mst_bp_biaya))}
                </DataTable.Cell>
                <DataTable.Cell numeric>
                  {jptlist === false ? (
                    <Paragraph
                      style={{
                        color: Colors.def,
                      }}
                      onPress={function () {
                        dispatch(addJpt(item));
                      }}
                    >
                      Pilih
                    </Paragraph>
                  ) : (
                    <Paragraph
                      style={{
                        color: Colors.danger,
                      }}
                      onPress={function () {
                        dispatch(deleteJpt());
                      }}
                    >
                      Hapus
                    </Paragraph>
                  )}
                </DataTable.Cell>
              </DataTable.Row>
            );
          })}

          <DataTable.Pagination
            page={1}
            numberOfPages={3}
            onPageChange={(page) => {
              console.log(page);
            }}
            label="1-2 of 6"
          />
        </DataTable>
      )}
    </>
  );
}

function JadwalList({
  navigation,
  route,
}: CreatePurchaseOrderProps<"ChooseSupplier">) {
  const dispatch = useDispatch();
  const schedules = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetSuccess
      ? state.schedule.scheduleGetSuccess.data
      : [];
  });
  const jadwalList = useSelector(function (state: RootState) {
    return state.jadwal.data;
  });
  const jptlist = useSelector(function (state: RootState) {
    return state.jpt.data;
  });
  const [visible, setVisible] = useState(false);
  const quota = useSelector(function (state: RootState) {
    return state.jadwal.quota;
  });
  return (
    <>
      <FlatList
        data={schedules}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListHeaderComponent={
          <>
            <ShipperFilter />
            <ShipperList navigation={navigation} route={route} />
          </>
        }
        renderItem={function ({ item }) {
          return (
            <Card style={{ padding: 20, elevation: 3 }}>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 3 }}>
                  <Caption>
                    Trayek :{" "}
                    <Paragraph>
                      {item.kode_trayek} Voyage {item.voyage}
                    </Paragraph>
                  </Caption>
                  <Caption>
                    Origin : <Paragraph>{item.pel_asal}</Paragraph>
                  </Caption>
                  <Caption>
                    Destination : <Paragraph>{item.pel_sampai}</Paragraph>
                  </Caption>
                  <Caption>
                    ETD :{" "}
                    <Paragraph>
                      {DateTimeFormat(item.tanggal_berangkat)}
                    </Paragraph>
                  </Caption>
                  <Caption>
                    ETA :{" "}
                    <Paragraph>{DateTimeFormat(item.tanggal_tiba)}</Paragraph>
                  </Caption>
                  <Caption>
                    Operator : <Paragraph>{item.operator_kapal_name}</Paragraph>
                  </Caption>
                  <Caption>
                    Harga :{" "}
                    {item.containers.map(function (c, ckey) {
                      return (
                        <Paragraph key={ckey}>
                          {"\n"}
                          {c.type_container_name} - Rp.{" "}
                          {formatRupiah(parseInt(c.price))}
                        </Paragraph>
                      );
                    })}
                  </Caption>
                </View>
                <View
                  style={{
                    flex: 1,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  {jadwalList.id !== item.id ? (
                    <Button
                      onPress={function () {
                        dispatch(addJadwal(item));
                      }}
                    >
                      Pilih
                    </Button>
                  ) : (
                    <>
                      <Caption>Masukkan kuota dipesan</Caption>
                      <TextInput
                        style={{
                          backgroundColor: Colors.gray1,
                          padding: 0,
                          width: 30,
                        }}
                        onChangeText={function (val) {
                          let jadwalFiltered: Array<object> = jadwalList.containers.filter(
                            (x) => parseInt(val) <= x.sisa_quota
                          );
                          let kuotaJadwal = jadwalFiltered.map(
                            (x) => x.sisa_quota
                          );
                          let kuotaTerbanyak = Math.max.apply({}, kuotaJadwal);
                          if (
                            kuotaTerbanyak === -Infinity ||
                            parseInt(val) === 0
                          ) {
                            console.log("Out of range");
                            setVisible(false);
                          } else {
                            dispatch(addQuota(val));
                            setVisible(true);
                          }
                        }}
                      />
                      <Paragraph
                        style={{ color: Colors.danger }}
                        onPress={function () {
                          dispatch(deleteJadwal());
                        }}
                      >
                        Hapus
                      </Paragraph>
                    </>
                  )}
                </View>
              </View>
            </Card>
          );
        }}
      />
      {visible && jptlist && (
        <Button
          style={{ width: "100%" }}
          mode="contained"
          color={Colors.success}
          onPress={function () {
            navigation.navigate("Checkout");
          }}
        >
          lanjutkan
        </Button>
      )}
    </>
  );
}

function SubmitShipper({
  navigation,
  route,
}: CreatePurchaseOrderProps<"ChooseSupplier">) {
  const jptlist = useSelector(function (state: RootState) {
    return state.jpt.data;
  });
  const jadwalList = useSelector(function (state: RootState) {
    return state.jadwal.data;
  });
  const quota = useSelector(function (state: RootState) {
    return state.jadwal.quota;
  });
  return (
    <>
      <Button
        style={{ width: "100%" }}
        mode="contained"
        color={Colors.success}
        onPress={function () {
          navigation.navigate("Checkout");
        }}
      >
        lanjutkan
      </Button>
    </>
  );
}

export default function ChooseShipper(
  props: CreatePurchaseOrderProps<"ChooseSupplier">
) {
  return (
    <>
      <JadwalList {...props} />
    </>
  );
}

const styles = StyleSheet.create({
  formItem: {
    backgroundColor: Colors.gray1,
    color: "black",
    width: "100%",
    opacity: 0.8,
    borderRadius: 3,
  },
});
