import React from "react";
import { StyleSheet, Text, View, FlatList, Image } from "react-native";
import {
  Card,
  Paragraph,
  Caption,
  IconButton,
  Button,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { Space, Icon } from "../../components";
import { CreatePurchaseOrderProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { TextInput } from "react-native-gesture-handler";
import { increaseCartItem, decreaseCartItem } from "../../redux/cartReducer";
import { formatRupiah } from "../../services/utils";
import { deleteJpt } from "../../redux/jptReducer";
import { deleteJadwal } from "../../redux/jadwalReducer";

function Counter({ value, onAdd, onSub, style }) {
  return (
    <View style={[style, { flexDirection: "row", alignItems: "center" }]}>
      <Icon name="minus-circle" style={{ fontSize: 24 }} onPress={onSub} />
      {/* <Text>{value}</Text> */}
      <TextInput
        caretHidden={false}
        style={{
          padding: 0,
          fontSize: 13,
          marginHorizontal: 5,
        }}
        enabled={false}
        value={String(value)}
      />
      <Icon name="plus-circle" style={{ fontSize: 24 }} onPress={onAdd} />
    </View>
  );
}

function CommodityList({
  navigation,
}: CreatePurchaseOrderProps<"CommoditySummary">) {
  const cartItems = useSelector(function (state: RootState) {
    return state.cart.items;
  });

  const [total, setTotal] = React.useState(0);
  const dispatch = useDispatch();
  function handleAdd(id: number) {
    dispatch(increaseCartItem(id));
    // alert(`Increase ${id}`);
  }
  function handleSub(id: number) {
    dispatch(decreaseCartItem(id));
    // alert(`Decrease ${id}`);
  }
  function totalCart() {
    let totalHarga = 0;
    cartItems.forEach((item) => {
      totalHarga =
        totalHarga +
        (parseInt(item.count) + parseInt(item.minimum_order)) *
          parseInt(item.harga_satuan);
    });
    setTotal(totalHarga);
  }
  React.useEffect(() => {
    totalCart();
  }, [cartItems]);
  return (
    <View style={{flex: 1}}>
    <FlatList
      data={cartItems}
      style={{backgroundColor:'whitesmoke'}}
      ListEmptyComponent={
        <Caption style={{ textAlign: "center" }}>No Data</Caption>
      }
      renderItem={function (item) {
        const orderItem =
          parseInt(item.item.minimum_order) + parseInt(item.item.count);
        return (
          <Card style={{ elevation: 2, padding: 10, marginBottom: 5 }}>
            <View style={{ flexDirection: "row" }}>
              {/* <Image
                source={item.item.url}
                style={{ width: "100%", height: 50, flex: 1 }}
              /> */}
              <View style={{ flex: 6, marginLeft: 10 }}>
                <Paragraph
                  style={{
                    color: Colors.pri,
                    fontSize: 16,
                  }}
                >
                  {item.item.nama_barang}
                </Paragraph>
                <View style={{ flexDirection: "row" }}>
                  <Caption style={{ flex: 1 }}>
                    Rp. {formatRupiah(parseInt(item.item.harga_satuan))} /{" "}
                    {item.item.satuan}
                  </Caption>
                  <Counter
                    style={{ justifyContent: "center", flex: 1 }}
                    value={orderItem}
                    onAdd={() => handleAdd(item.item.id)}
                    onSub={() => {
                      if (orderItem > item.item.minimum_order)
                        handleSub(item.item.id);
                    }}
                  />
                </View>
              </View>
              {/* <View style={{ flex: 1 }}>
                <IconButton
                  icon="delete"
                  color={Colors.gray1}
                  onPress={function () {
                    null;
                  }}
                />
              </View> */}
            </View>
            <Caption
              style={{
                fontSize: 16,
                flex: 1,
                color: "orange",
                fontWeight: "bold",
                textAlign: "right",
              }}
            >
              Rp. {formatRupiah(parseInt(item.item.harga_satuan) * orderItem)}
            </Caption>
          </Card>
        );
      }}
      keyExtractor={function (item) {
        return JSON.stringify(item.id);
      }}
    />
    <View style={{position: 'absolute', bottom: 0, width: '100%'}}>
      <Card style={{ paddingHorizontal: 10, paddingVertical: 15 }}>
        <View style={{ flexDirection: "row" }}>
          <Paragraph
            style={{
              color: Colors.pri,
              fontWeight: "bold",
              fontSize: 20,
              flex: 1,
            }}
          >
            Total harga
          </Paragraph>
          <Paragraph
            style={{
              color: "orange",
              fontWeight: "bold",
              fontSize: 20,
              flex: 1,
              textAlign: "right",
            }}
          >
            {`Rp. ${formatRupiah(parseInt(total))}`}
          </Paragraph>
        </View>
      </Card>
      <Button
        mode="contained"
        onPress={function () {
          dispatch(deleteJpt());
          dispatch(deleteJadwal());
          navigation.navigate("ChooseShipper");
        }}
      >
        <Text>Pilih Pengiriman</Text>
      </Button>
    </View>
    </View>
  );
}
export default function CommoditySummary({
  navigation,
  route,
}: CreatePurchaseOrderProps<"CommoditySummary">) {
  return (
    <View style={styles.container}>
      <CommodityList navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
