import React, { useState } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Space, FilterButton, EmptyState } from "../../components";
import {
  Card,
  Paragraph,
  Caption,
  IconButton,
  Badge,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { ConsigneeStackProps } from "../Navigator";
import { purchaseFetchPage } from "../../redux/purchaseReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import { bookingFetchConsigneeReceived } from "../../redux/bookingReducer";
import { bookingFilterClear } from "./../../redux/bookingReducer";
import { DateFormat } from "../../services/utils";
import { shipperFetchAll } from "../../redux/shipperReducer";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { portFetchAll } from "../../redux/portReducer";

function ClaimPurchaseOrderList({
  navigation,
}: ConsigneeStackProps<"ClaimPurchaseOrder">) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const booking = useSelector(function (state: RootState) {
    return state.booking.bookingData ? state.booking.bookingData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.booking.bookingGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.booking.bookingFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.booking.bookingGetLoading;
  });

  const listSupplier = useSelector(function (state: RootState) {
    return state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : [];
  });
  const listShipper = useSelector(function (state: RootState) {
    return state.shipper.shipperGetSuccess
      ? state.shipper.shipperGetSuccess.data
      : [];
  });
  const listPort = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });

  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      id: `${id}`,
      ...filter,
    };
    dispatch(bookingFetchConsigneeReceived(params));
    dispatch(shipperFetchAll());
    dispatch(supplierFetchAll());
    dispatch(portFetchAll());
  }

  function handleReachEnd(filter) {
    const params = {
      length: 10,
      id: `${id}`,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(bookingFetchConsigneeReceived(params));
  }

  const onRefresh = () => {
    dispatch(bookingFilterClear());
  };

  React.useEffect(() => {
    filter === {} ? null : fetchData(filter);
  }, [filter]);

  return (
    <>
      <FlatList
        data={booking}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                navigation.navigate("ReadClaimPurchaseOrder", {
                  item: item,
                });
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Paragraph
                  selectable
                  style={{ fontWeight: "bold", fontSize: 16, flex: 3 }}
                >
                  No : {item.no_po}
                </Paragraph>
                <Caption style={{ flex: 1 }}>
                  {DateFormat(item.date_po)}{" "}
                </Caption>
              </View>
              <Badge
                style={{
                  alignSelf: "flex-start",
                  backgroundColor: item.is_received
                    ? Colors.success
                    : Colors.grayL,
                }}
              >
                {item.is_received
                  ? "Sudah diterima consignee"
                  : "Belum diterima consignee"}
              </Badge>
              <Caption style={{ fontSize: 13 }}>
                Supplier: {item.nama_perusahaan}
                {"\n"}
                JPT: {item.shipper_name}
                {"\n"}
                <Caption>
                  {item.origin} ➤ {item.destination}
                </Caption>
              </Caption>
            </Card>
          );
        }}
      />

      <FilterButton
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "ClaimPurchaseOrderFilterModal",
            params: {
              shipper: listShipper,
              port: listPort,
              supplier: listSupplier,
            },
          });
        }}
      />
    </>
  );
}

const ClaimPurchaseOrder = ({
  navigation,
  route,
}: ConsigneeStackProps<"ClaimPurchaseOrder">) => {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });

  return (
    <View style={styles.container}>
      <ClaimPurchaseOrderList navigation={navigation} route={route} />
    </View>
  );
};

export default ClaimPurchaseOrder;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    padding: 10,
    borderRadius: 5,
    margin: 5,
  },
});
