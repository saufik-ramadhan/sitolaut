import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { Root, DateTime, Space, FileInput, ErrorText } from "../../components";
import {
  Card,
  Caption,
  Badge,
  Paragraph,
  TextInput,
  Button,
  Title,
  List,
  Dialog,
  Portal,
} from "react-native-paper";
import Colors from "../../config/Colors";
import { DateFormat, DateFormat3 } from "../../services/utils";
import { Formik } from "formik";
import { useSelector, useDispatch } from "react-redux";
import * as Yup from "yup";
import {
  bookingHasDOUpdate,
  bookingFetchConsigneeReceived,
  bookingFilterClear,
  bookingFilterAdd,
} from "../../redux/bookingReducer";
import { UPLOAD_URL } from "../../config/constants";
import { RootState } from "../../redux/rootReducer";

/**
 * Validation Schema
 */
const ValidationSchema = Yup.object().shape({
  recipient_consignee_name: Yup.string().required("Required"),
  recipient_consignee_date: Yup.string().required("Required"),
  recipient_consignee_ktp: Yup.string().required("Required"),
});

function DetailPengambilanBarang({nama, tanggal, identitas, navigation}) {
  const [visible, setVisible] = React.useState(false);
  return (
    <View style={{margin: 10}}>
      <Title>Detail pengambilan barang</Title>
      <List.Item title={nama} description="Nama pengambil barang"/>
      <List.Item title={DateFormat(tanggal)} description="Tanggal pengambilan"/>
      <TouchableOpacity onPress={function(){
        navigation.navigate("Modals", {
          screen: "ImageModal",
          params: {
            url: identitas,
          },
        });
      }}>
        <Image resizeMode="contain" source={{uri:identitas}} style={{width: '100%', height: 100}}/>
      </TouchableOpacity>
    </View>
  )
}

export default function ReadClaimPurchaseOrder({ navigation, route }) {
  const { item } = route.params;
  const dispatch = useDispatch();

  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  return (
    <Root>
      <Card style={styles.card}>
        <Caption>Nomor PO</Caption>
        <Paragraph>{item.no_po}</Paragraph>
        <Caption>Status</Caption>
        <Badge
          style={{
            alignSelf: "flex-start",
            backgroundColor: item.is_received ? Colors.success : Colors.grayL,
          }}
        >
          {item.is_received == 1
            ? "Sudah diterima consignee"
            : "Belum diterima consignee"}
        </Badge>
        <Caption>Tanggal PO</Caption>
        <Paragraph>{DateFormat(item.date_po)}</Paragraph>
        <Caption>Supplier</Caption>
        <Paragraph>{item.nama_perusahaan}</Paragraph>
        <Caption>Shipper</Caption>
        <Paragraph>{item.shipper_name}</Paragraph>
        <Caption>Rute</Caption>
        <Paragraph>
          {item.origin} ➤ {item.destination}
        </Paragraph>
      </Card>

      {item.is_received == 1 ? <DetailPengambilanBarang navigation={navigation} nama={item.recipient_consignee_name} tanggal={item.recipient_consignee_date} identitas={UPLOAD_URL + item.recipient_consignee_ktp}/> : (
        <Card>
          <Formik
            initialValues={{
              recipient_consignee_name: "",
              id: item.booking_has_po_id,
              booking_id: item.booking_id,
              recipient_consignee_date: "",
              recipient_consignee_ktp: "",
              recipient_consignee_ktp_size: "",
            }}
            validationSchema={ValidationSchema}
            onSubmit={function (values) {
              const params = {
                length: 10,
                id: `${id}`,
              };
              dispatch(bookingHasDOUpdate(values));
              dispatch(bookingFetchConsigneeReceived(params));
              navigation.goBack();
            }}
          >
            {({
              setFieldValue,
              handleSubmit,
              handleBlur,
              handleChange,
              values,
              errors,
              touched,
            }) => (
              <>
                {/**
                 * Nama Penerima
                 */}
                <TextInput
                  value={values.recipient_consignee_name}
                  onChangeText={handleChange("recipient_consignee_name")}
                  onBlur={handleBlur("recipient_consignee_name")}
                  placeholder="Nama Penerima"
                />
                {errors.recipient_consignee_name &&
                  touched.recipient_consignee_name && (
                    <ErrorText>{errors.recipient_consignee_name}</ErrorText>
                  )}

                {/**
                 * Tanggal Diterima
                 */}
                <DateTime
                  label="Tanggal Diterima"
                  onChangeValue={(date) =>
                    setFieldValue("recipient_consignee_date", DateFormat3(date))
                  }
                />
                {errors.recipient_consignee_date &&
                  touched.recipient_consignee_date && (
                    <ErrorText>{errors.recipient_consignee_date}</ErrorText>
                  )}

                {/**
                 * Dokumen KTP
                 */}
                {values.recipient_consignee_ktp ? (
                  <>
                    <Image
                      source={{ uri: values.recipient_consignee_ktp.uri }}
                      style={{ height: 100, width: 100 }}
                    />
                    <Space height={10} />
                  </>
                ) : null}
                <FileInput
                  title="KTP"
                  placeholder={
                    values.recipient_consignee_ktp
                      ? values.recipient_consignee_ktp.name
                      : "Upload File siup"
                  }
                  error={errors.recipient_consignee_ktp_size}
                  getValue={function (value) {
                    setFieldValue("recipient_consignee_ktp", {
                      uri: value.uri,
                      type: value.type,
                      name: value.fileName,
                    });
                    setFieldValue(
                      "recipient_consignee_ktp_size",
                      value.fileSize
                    );
                  }}
                />
                {errors.recipient_consignee_ktp &&
                  touched.recipient_consignee_ktp && (
                    <ErrorText>{errors.recipient_consignee_ktp}</ErrorText>
                  )}

                <Button mode="contained" onPress={handleSubmit}>
                  Terima Barang
                </Button>
              </>
            )}
          </Formik>
        </Card>
      )}
    </Root>
  );
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
  },
});
