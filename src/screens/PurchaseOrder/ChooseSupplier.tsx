import React, { useEffect } from "react";
import { View, FlatList, Image, StyleSheet } from "react-native";
import { Space, ErrorText } from "../../components";
import {
  Card,
  Button,
  Paragraph,
  Caption,
  IconButton,
  TextInput,
} from "react-native-paper";
import { CreatePurchaseOrderProps } from "../Navigator";
import Colors from "./../../config/Colors";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { provinsiFetchAll } from "./../../redux/provinsiReducer";
import { kotaFetchByProvinsi } from "../../redux/kotaReducer";
import { supplierFetchPage } from "../../redux/supplierReducer";
import { clearCartItems } from "../../redux/cartReducer";
import { addChoosesupplier } from "../../redux/choosesupplierReducer";
import { supplierClear } from "./../../redux/supplierReducer";

function SupplierFilter() {
  const dispatch = useDispatch();
  const listProvinsi = useSelector(function (state: RootState) {
    return state.provinsi.provinsiGetSuccess
      ? state.provinsi.provinsiGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  const listKota = useSelector(function (state: RootState) {
    return state.kota.kotaGetSuccess
      ? state.kota.kotaGetSuccess.data
      : [{ id: 0, label: "Loading.." }];
  });
  React.useEffect(() => {
    dispatch(provinsiFetchAll());
  }, []);
  return (
    <Card style={{ padding: 10, elevation: 3 }}>
      <Formik
        initialValues={{
          provinsi_id: 0,
          kota_id: 0,
          nama_perusahaan: '',
        }}
        onSubmit={function (values) {
          // alert(JSON.stringify(values));
          dispatch(supplierFetchPage(values));
        }}
      >
        {function ({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) {
          return (
            <>
              <TextInput
                label="Nama Supplier"
                dense
                mode="outlined"
                onChangeText={handleChange('nama_perusahaan')}
                onBlur={handleBlur('nama_perusahaan')}
              />
              <Space height={10}/>
              {/**
               * Provinsi ID
               */}
              <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  selectedValue={values.provinsi_id}
                  style={{ flex: 10 }}
                  itemStyle={{ borderRadius: 5 }}
                  mode="dropdown"
                  onValueChange={function (itemValue: number) {
                    setFieldValue("provinsi_id", itemValue);
                    dispatch(kotaFetchByProvinsi(itemValue));
                  }}
                >
                  <Picker.Item label="Pilih Provinsi" value={0} />
                  {listProvinsi.map(function (item, key) {
                    return (
                      <Picker.Item
                        label={item.label}
                        value={item.id}
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </View>
              {errors.provinsi_id && touched.provinsi_id && (
                <ErrorText>{errors.provinsi_id}</ErrorText>
              )}
              <Space height={10} />

              {/**
               * Kota ID
               */}
              <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  selectedValue={values.kota_id}
                  style={{ flex: 10 }}
                  itemStyle={{ borderRadius: 5 }}
                  mode="dropdown"
                  onValueChange={function (itemValue: number) {
                    setFieldValue("kota_id", itemValue);
                  }}
                >
                  <Picker.Item label="Pilih Kota" value={0} />
                  {listKota.map(function (item, key) {
                    return (
                      <Picker.Item
                        label={item.label}
                        value={item.kota_id}
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </View>
              {errors.kota_id && touched.kota_id && (
                <ErrorText>{errors.kota_id}</ErrorText>
              )}
              {/* <Space height={10} /> */}

              <Button onPress={handleSubmit} mode="contained">
                Cari
              </Button>
            </>
          );
        }}
      </Formik>
    </Card>
  );
}

function SupplierList({
  navigation,
  route,
}: CreatePurchaseOrderProps<"ChooseSupplier">) {
  const listSupplier = useSelector(function (state: RootState) {
    return state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : [];
  });
  const dispatch = useDispatch();
  return (
    <FlatList
      ListEmptyComponent={
        <Caption style={{ textAlign: "center" }}>
          Data tidak ditemukan klik "Cari" untuk mencari data
          berdasarkan Nama atau Provinsi
        </Caption>
      }
      data={listSupplier}
      style={{ height: "100%" }}
      showsVerticalScrollIndicator={false}
      ListFooterComponent={<View style={{ height: 50 }} />}
      renderItem={function ({ item }) {
        return (
          <Card
            onPress={function () {
              dispatch(clearCartItems());
              dispatch(addChoosesupplier(item));
              navigation.navigate("ChooseCommodity", {
                supplier_id: item.id,
              });
            }}
            style={{
              borderRadius: 0,
              marginBottom: 5,
              elevation: 3,
              borderLeftColor: Colors.pri,
              borderLeftWidth: 8,
              padding: 5,
            }}
          >
            <View
              style={{
                flexDirection: "row",
              }}
            >
              {/** Logo Perusahaan */}
              <View style={{ flex: 2, justifyContent: "center" }}>
                <Image
                  source={require("../../assets/img/download.png")}
                  style={{
                    height: 50,
                    width: 50,
                  }}
                />
              </View>
              {/** Detail Supplier */}
              <View style={{ flex: 7 }}>
                <Paragraph style={{ fontSize: 16 }}>
                  {item.nama_perusahaan}
                </Paragraph>

                <Caption style={{ color: "#888", fontSize: 13 }}>
                  {item.kota}
                  {"\n"}
                  {item.provinsi}
                </Caption>
              </View>
              {/** Choose Button */}
              <View style={{ justifyContent: "center" }}>
                <IconButton icon="chevron-right" onPress={function () {}} />
              </View>
              {/** --------- */}
            </View>
          </Card>
        );
      }}
      keyExtractor={function (item, key) {
        return String(key);
      }}
    />
  );
}

export default function ChooseSupplier(
  props: CreatePurchaseOrderProps<"ChooseSupplier">
) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(supplierClear());
  }, []);
  return (
    <View style={styles.container}>
      <SupplierFilter />
      <SupplierList {...props} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formItem: {
    backgroundColor: Colors.gray1,
    color: "black",
    width: "100%",
    opacity: 0.8,
    borderRadius: 3,
  },
  textInput: {
    paddingHorizontal: 10,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  viewPager: {
    flex: 1,
  },
});
