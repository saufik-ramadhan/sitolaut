import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TextInput,
  Alert,
} from "react-native";
import {
  Caption,
  Card,
  Paragraph,
  IconButton,
  Button,
} from "react-native-paper";
import {
  formatRupiah,
  DateFormat,
  TimeFormat,
  getUserType,
} from "../../services/utils";
import { Space, Icon, Root } from "../../components";
import Colors from "./../../config/Colors";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { CreatePurchaseOrderProps } from "../Navigator";
import { increaseCartItem, decreaseCartItem } from "../../redux/cartReducer";
import { addQuota, deleteJadwal, addJadwal } from "../../redux/jadwalReducer";
import { scheduleFetchOne } from "../../redux/scheduleReducer";
import { priceFetchOne } from "./../../redux/priceReducer";
import { purchaseAdd, purchaseFetchPage } from "../../redux/purchaseReducer";
import { Item } from "react-native-paper/lib/typescript/src/components/List/List";

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

function Counter({ value, onAdd, onSub, style }) {
  return (
    <View style={[style, { flexDirection: "row", alignItems: "center" }]}>
      <Icon name="minus-circle" style={{ fontSize: 24 }} onPress={onSub} />
      {/* <Text>{value}</Text> */}
      <TextInput
        caretHidden={false}
        style={{
          padding: 0,
          fontSize: 13,
          marginHorizontal: 5,
        }}
        enabled={false}
        value={String(value)}
      />
      <Icon name="plus-circle" style={{ fontSize: 24 }} onPress={onAdd} />
    </View>
  );
}

function CommodityList({
  navigation,
}: CreatePurchaseOrderProps<"CommoditySummary">) {
  const cartItems = useSelector(function (state: RootState) {
    return state.cart.items;
  });

  const [total, setTotal] = React.useState(0);
  const dispatch = useDispatch();
  function handleAdd(id: number) {
    dispatch(increaseCartItem(id));
    // alert(`Increase ${id}`);
  }
  function handleSub(id: number) {
    dispatch(decreaseCartItem(id));
    // alert(`Decrease ${id}`);
  }
  function totalCart() {
    let totalHarga = 0;
    cartItems.forEach((item) => {
      totalHarga =
        totalHarga +
        (parseInt(item.count) + parseInt(item.minimum_order)) *
          parseInt(item.harga_satuan);
    });
    setTotal(totalHarga);
  }
  React.useEffect(() => {
    totalCart();
  }, [cartItems]);
  return (
    <>
      {cartItems.map((item, key) => {
        const orderItem = parseInt(item.minimum_order) + parseInt(item.count);
        return (
          <Card style={{ elevation: 2, padding: 10 }} key={key}>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={item.url}
                style={{ width: "100%", height: 50, flex: 1 }}
              />
              <View style={{ flex: 6, marginLeft: 10 }}>
                <Paragraph
                  style={{
                    color: Colors.pri,
                    fontSize: 16,
                  }}
                >
                  {item.nama_barang}
                </Paragraph>
                <View style={{ flexDirection: "row" }}>
                  <Caption style={{ flex: 1 }}>
                    Rp. {formatRupiah(parseInt(item.harga_satuan))}
                  </Caption>
                  <Counter
                    style={{ justifyContent: "center", flex: 1 }}
                    value={orderItem}
                    onAdd={() => handleAdd(item.id)}
                    onSub={() => {
                      if (orderItem > item.minimum_order) handleSub(item.id);
                    }}
                  />
                </View>
              </View>
              <View style={{ flex: 1 }}>
                <IconButton
                  icon="delete"
                  color={Colors.gray1}
                  onPress={function () {
                    null;
                  }}
                />
              </View>
            </View>
            <Caption
              style={{
                fontSize: 16,
                flex: 1,
                color: "orange",
                fontWeight: "bold",
                textAlign: "right",
              }}
            >
              Rp. {formatRupiah(parseInt(item.harga_satuan) * orderItem)}
            </Caption>
          </Card>
        );
      })}
      <Card style={{ paddingHorizontal: 10, paddingVertical: 15 }}>
        <View style={{ flexDirection: "row" }}>
          <Paragraph
            style={{
              color: Colors.black,
              fontWeight: "bold",
              fontSize: 16,
              flex: 1,
            }}
          >
            Harga Barang
          </Paragraph>
          <Paragraph
            style={{
              color: "orange",
              fontWeight: "bold",
              fontSize: 16,
              flex: 1,
              textAlign: "right",
            }}
          >
            {`Rp. ${formatRupiah(parseInt(total))}`}
          </Paragraph>
        </View>
      </Card>
    </>
  );
}

function JadwalKapal() {
  const dispatch = useDispatch();
  const schedules = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetSuccess
      ? state.schedule.scheduleGetSuccess.data
      : false;
  });
  const jadwalList = useSelector(function (state: RootState) {
    return state.jadwal.data;
  });
  const quota = useSelector(function (state: RootState) {
    return state.jadwal.quota;
  });
  const depart = jadwalList.tanggal_berangkat;
  const arrive = jadwalList.tanggal_tiba;
  return (
    <>
      <Card style={{ padding: 20 }}>
        {/** Rute */}
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <B>
              {jadwalList.pel_asal}
              {"\n"}
              <Caption>
                {`${DateFormat(depart)}`}
                {"\n"}
                {`(${TimeFormat(depart)})`}{" "}
              </Caption>
            </B>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
          </View>
          <View style={{ flex: 1, alignItems: "flex-end" }}>
            <B>
              {jadwalList.pel_sampai}
              {"\n"}
              <Caption>
                {`${DateFormat(arrive)}`}
                {"\n"}
                {`(${TimeFormat(arrive)})`}{" "}
              </Caption>
            </B>
          </View>
        </View>

        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 2 }}>
            <Caption>
              Trayek :
              <Paragraph>
                {jadwalList.kode_trayek}(Voyage {jadwalList.voyage})
              </Paragraph>
            </Caption>
            <View>
              <Caption>
                Operator :{" "}
                <Paragraph>{jadwalList.operator_kapal_name}</Paragraph>
              </Caption>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
            }}
          >
            {jadwalList === false ? (
              <Button
                color={Colors.pri}
                mode="contained"
                onPress={function () {
                  dispatch(addJadwal(item));
                }}
              >
                Pilih
              </Button>
            ) : (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <View style={{ flex: 4, borderWidth: 1 }}>
                  <TextInput
                    style={{ padding: 0 }}
                    onChangeText={function (val) {
                      if (val <= jadwalList.containers[1].sisa_quota)
                        dispatch(addQuota(val));
                      else null;
                    }}
                    value={quota}
                  />
                </View>
                <IconButton
                  icon="delete"
                  color={Colors.danger}
                  onPress={function () {
                    dispatch(deleteJadwal());
                  }}
                  style={{ flex: 1 }}
                />
              </View>
            )}
          </View>
        </View>
      </Card>
    </>
  );
}

function JasaPengurusanTransportasi() {
  const jpt = useSelector(function (state: RootState) {
    return state.jpt.data;
  });
  const biaya_pengiriman = formatRupiah(
    parseInt(jpt.biaya_pelabuhan_muat) +
      parseInt(jpt.biaya_angkut) +
      parseInt(jpt.biaya_pelabuhan_bongkar) +
      parseInt(jpt.biaya_stripping) +
      parseInt(jpt.mst_bp_biaya)
  );
  return (
    <>
      <Card style={{ padding: 20 }}>
        <Caption>
          Nama JPT {"\n"}
          <Paragraph>{jpt.nama_perusahaan}</Paragraph>
        </Caption>
      </Card>
      <Card style={{ paddingHorizontal: 10 }}>
        <View style={{ flexDirection: "row" }}>
          <Paragraph
            style={{
              color: Colors.black,
              fontWeight: "bold",
              fontSize: 16,
              flex: 1,
            }}
          >
            Biaya Pengiriman
          </Paragraph>
          <Paragraph
            style={{
              color: "orange",
              fontWeight: "bold",
              fontSize: 16,
              flex: 1,
              textAlign: "right",
            }}
          >
            Rp. {biaya_pengiriman}
          </Paragraph>
        </View>
      </Card>
    </>
  );
}

function TotalHarga() {
  const cartItems = useSelector(function (state: RootState) {
    return state.cart.items;
  });
  const jpt = useSelector(function (state: RootState) {
    return state.jpt.data;
  });
  const biaya_pengiriman =
    parseInt(jpt.biaya_pelabuhan_muat) +
    parseInt(jpt.biaya_angkut) +
    parseInt(jpt.biaya_pelabuhan_bongkar) +
    parseInt(jpt.biaya_stripping) +
    parseInt(jpt.mst_bp_biaya);
  const [total, setTotal] = React.useState(0);
  const dispatch = useDispatch();
  const totalHarga = formatRupiah(total + biaya_pengiriman);
  function totalCart() {
    let totalHarga = 0;
    cartItems.forEach((item) => {
      totalHarga =
        totalHarga +
        (parseInt(item.count) + parseInt(item.minimum_order)) *
          parseInt(item.harga_satuan);
    });
    setTotal(totalHarga);
  }
  React.useEffect(() => {
    totalCart();
  }, [cartItems]);
  return (
    <Card style={{ padding: 10 }}>
      <View style={{ flexDirection: "row" }}>
        <Paragraph
          style={{
            color: Colors.success,
            fontWeight: "bold",
            fontSize: 20,
            flex: 1,
          }}
        >
          Total Harga
        </Paragraph>
        <Paragraph
          style={{
            color: "orange",
            fontWeight: "bold",
            fontSize: 20,
            flex: 1,
            textAlign: "right",
          }}
        >
          Rp. {totalHarga}
        </Paragraph>
      </View>
    </Card>
  );
}

function SupplierDetail() {
  const choosesupplier = useSelector(function (state: RootState) {
    return state.choosesupplier.data;
  });
  return <Paragraph>{JSON.stringify(choosesupplier)}</Paragraph>;
}

function SubmitCheckout({ loading }) {
  const [total, setTotal] = React.useState(0);
  const dispatch = useDispatch();
  const commodities = useSelector(function (state: RootState) {
    return state.cart.items;
  });
  const detail_po = commodities.map((item, key) => ({
    ...item,
    commodity_id: item.id,
    harga_total:
      (parseInt(item.minimum_order) + parseInt(item.count)) *
      parseInt(item.harga_satuan),
    qty: `${parseInt(item.minimum_order) + parseInt(item.count)}`,
  }));
  const shipper = useSelector(function (state: RootState) {
    return state.jpt.data;
  });
  const supplier = useSelector(function (state: RootState) {
    return state.choosesupplier.data;
  });
  const jadwal = useSelector(function (state: RootState) {
    return state.jadwal.data;
  });
  const quota = useSelector(function (state: RootState) {
    return state.jadwal.quota;
  });
  function totalCart() {
    let totalHarga = 0;
    commodities.forEach((item) => {
      totalHarga =
        totalHarga +
        (parseInt(item.count) + parseInt(item.minimum_order)) *
          parseInt(item.harga_satuan);
    });
    setTotal(totalHarga);
  }
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  React.useEffect(() => {
    totalCart();
  }, [commodities]);

  function addPurchaseOrder() {
    const params = {
      biaya_angkut: shipper.biaya_angkut,
      biaya_pelabuhan_bongkar: shipper.biaya_pelabuhan_bongkar,
      biaya_pelabuhan_muat: shipper.biaya_pelabuhan_muat,
      biaya_pengurusan: shipper.mst_bp_biaya,
      biaya_stripping: shipper.biaya_stripping,
      consignee_id: `${id}`,
      destination: jadwal.pel_sampai,
      detail_po: detail_po,
      jadwal_id: jadwal.id,
      origin: jadwal.pel_asal,
      port_destination_id: jadwal.port_destination_id,
      port_origin_id: jadwal.port_origin_id,
      quota: `${quota}`,
      shipper_id: shipper.shipper_id,
      supplier_id: supplier.id,
      term_coond: "",
      total_harga: String(
        parseInt(total) +
          parseInt(shipper.biaya_angkut) +
          parseInt(shipper.biaya_pelabuhan_bongkar) +
          parseInt(shipper.biaya_pelabuhan_muat) +
          parseInt(shipper.biaya_stripping) +
          parseInt(shipper.mst_bp_biaya)
      ),
      trayek_id: jadwal.trayek_id,
    };
    // console.warn(JSON.stringify(params));
    dispatch(purchaseAdd(params));
  }

  useEffect(() => {}, []);
  return (
    <>
      <Button
        onPress={function () {
          addPurchaseOrder();
        }}
        loading={loading}
      >
        Checkout
      </Button>
    </>
  );
}

export default function Checkout({ navigation }) {
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const checkoutSuccess = useSelector(function (state: RootState) {
    return state.purchase.purchaseAddSuccess
      ? state.purchase.purchaseAddSuccess.data
      : false;
  });
  const loading = useSelector(function (state: RootState) {
    return state.purchase.purchaseAddLoading;
  });
  const dispatch = useDispatch();
  function fetchPurchaseOrder() {
    const isAprove = getUserType(usertype).isShipper ? 1 : "";
    const fetchParams: any = {
      length: "",
      no_po: "",
      is_approve: isAprove,
      is_approve_by_shipper: "",
      consignee_id: getUserType(usertype).isConsignee ? id : "",
      supplier_id: getUserType(usertype).isSupplier ? id : "",
      shipper_id: getUserType(usertype).isShipper ? id : "",
      reseller_id: getUserType(usertype).isReseller ? id : "",
      dari: "",
      sampai: "",
      start: 0,
    };

    dispatch(purchaseFetchPage(fetchParams));
  }
  React.useEffect(() => {
    if (checkoutSuccess) {
      fetchPurchaseOrder();
      Alert.alert(
        "Berhasil",
        "Purchase Order Baru berhasil dibuat",
        [{ text: "OK", onPress: () => navigation.navigate("PurchaseOrder") }],
        { cancelable: false }
      );
    }
  }, [checkoutSuccess]);
  return (
    <Root>
      <CommodityList />
      <JadwalKapal />
      <JasaPengurusanTransportasi />
      {/* <SupplierDetail /> */}
      <TotalHarga />
      <SubmitCheckout loading={loading} />
    </Root>
  );
}

const styles = StyleSheet.create({});
