import React, { useState } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Space, FilterButton, EmptyState } from "../../components";
import {
  Card,
  Paragraph,
  Caption,
  IconButton,
  Badge,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { ConsigneeStackProps } from "../Navigator";
import { purchaseFetchPage } from "../../redux/purchaseReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import { priceconsigneeFetchPage } from "../../redux/priceconsigneeReducer";
import { priceconsigneeFilterClear } from "./../../redux/priceconsigneeReducer";
import { DateFormat, FormatTanggal } from "../../services/utils";
import { shipperFetchAll } from "../../redux/shipperReducer";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { portFetchAll } from "../../redux/portReducer";

function HargaJualBarangList({
  navigation,
}: ConsigneeStackProps<"HargaJualBarang">) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const priceconsignee = useSelector(function (state: RootState) {
    return state.priceconsignee.priceconsigneeData
      ? state.priceconsignee.priceconsigneeData
      : [];
  });
  const priceconsigneeAddSuccess = useSelector(function (state: RootState) {
    return state.priceconsignee.priceconsigneeAddLoading;
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.priceconsignee.priceconsigneeGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.priceconsignee.priceconsigneeFilter;
  });
  const listSupplier = useSelector(function (state: RootState) {
    return state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : [];
  });
  const listShipper = useSelector(function (state: RootState) {
    return state.shipper.shipperGetSuccess
      ? state.shipper.shipperGetSuccess.data
      : [];
  });
  const listPort = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  const loading = useSelector(function (state: RootState) {
    return state.priceconsignee.priceconsigneeGetLoading;
  });

  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(priceconsigneeFetchPage(params));
    dispatch(shipperFetchAll());
    dispatch(supplierFetchAll());
    dispatch(portFetchAll());
  }

  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(priceconsigneeFetchPage(params));
  }

  const onRefresh = () => {
    dispatch(priceconsigneeFilterClear());
  };

  React.useEffect(() => {
    filter === {} ? null : fetchData(filter);
  }, [filter, priceconsigneeAddSuccess]);

  return (
    <>
      <FlatList
        data={priceconsignee}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              style={styles.card}
              onPress={function () {
                navigation.navigate("ReadHargaJualBarang", {
                  price_id: item.id,
                });
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <Paragraph
                  selectable
                  style={{ fontWeight: "bold", fontSize: 16, flex: 3 }}
                >
                  No : {item.no_po}
                </Paragraph>
              </View>
              <Badge
                style={{
                  alignSelf: "flex-start",
                  backgroundColor: item.already_set_harga
                    ? Colors.success
                    : Colors.grayL,
                }}
              >
                {item.already_set_harga
                  ? "Harga Sudah Diset"
                  : "Harga Belum Diset"}
              </Badge>
              <Caption style={{ fontSize: 13 }}>
                Supplier: {item.supplier_name}
                {"\n"}
                JPT: {item.shipper_nama_perusahaan}
                {"\n"}
                <Caption>
                  {item.origin} ➤ {item.destination}
                </Caption>{"\n"}
                <Caption>
                  {DateFormat(item.date_po)}{" "}
                </Caption>
              </Caption>
            </Card>
          );
        }}
      />

      <FilterButton
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "HargaJualBarangFilterModal",
            params: {
              shipper: listShipper,
              port: listPort,
              supplier: listSupplier,
            },
          });
        }}
      />
    </>
  );
}

const HargaJualBarang = ({
  navigation,
  route,
}: ConsigneeStackProps<"HargaJualBarang">) => {
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });

  return (
    <View style={styles.container}>
      <HargaJualBarangList navigation={navigation} route={route} />
    </View>
  );
};

export default HargaJualBarang;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    margin: 5,
    borderRadius: 5,
    elevation: 3,
  },
});
