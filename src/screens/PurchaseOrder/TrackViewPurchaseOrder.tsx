import React from "react";
import { View, Text, FlatList, StyleSheet } from "react-native";
import { default as colors } from "../../config/Colors";
import { Icon, Root, Space } from "../../components";
import { useDispatch, useSelector } from "react-redux";
import { trackFetchOne } from "../../redux/trackReducer";
import { RootState } from "../../redux/rootReducer";
import { Paragraph, Caption, Card } from "react-native-paper";
import { DateTimeFormat } from "../../services/utils";

function TrackPoint({ date, name }) {
  return (
    <View
      style={{
        borderColor: "black",
        marginBottom: 2,
      }}
    >
      <View
        style={{
          backgroundColor: date ? colors.pri : colors.white,
          flexDirection: "row",
          borderWidth: 1,
          borderColor: date ? colors.pri : colors.gray1,
          alignItems: "center",
        }}
      >
        <Icon
          name="map-marker"
          style={{
            color: date ? colors.sec : colors.gray1,
            fontSize: 60,
            padding: 4,
          }}
        />
        <View>
          <Paragraph style={{ color: colors.gray1 }}>{name}</Paragraph>
          <Paragraph style={{ color: colors.gray1, fontSize: 20 }}>
            {date ? DateTimeFormat(date) : null}
          </Paragraph>
        </View>
      </View>
    </View>
  );
}

export default function ({ navigation, route }) {
  const { id } = route.params;
  const dispatch = useDispatch();
  const track = useSelector(function (state: RootState) {
    return state.track.trackViewSuccess
      ? state.track.trackViewSuccess.data[0]
      : false;
  });
  React.useEffect(() => {
    dispatch(trackFetchOne(id));
  }, []);
  return (
    <Root style={styles.container}>
      <Card style={{ padding: 10 }}>
        <Caption>
          No. PO : <Paragraph>{track.no_po}</Paragraph>
        </Caption>
        <Caption>
          JPT : <Paragraph>{track.shipper_name}</Paragraph>
        </Caption>
        <Caption>
          Supplier : <Paragraph>{track.supplier_name}</Paragraph>
        </Caption>
        <Caption>
          Consignee : <Paragraph>{track.consignee_name}</Paragraph>
        </Caption>
        <Caption>
          Rute :{" "}
          <Paragraph>
            {track.origin} ➤ {track.destination}
          </Paragraph>
        </Caption>
      </Card>
      <TrackPoint date={track.ts_consignee} name={"Consignee"} />
      <TrackPoint date={track.ts_supplier} name={"Supplier"} />
      <TrackPoint date={track.ts_shipper} name={"Shipper Pengirim"} />
      <TrackPoint date={track.ts_operator} name={"Operator"} />
      <TrackPoint date={track.ts_receive_shipper} name={"Shipper Penerima"} />
      <TrackPoint date={track.ts_receive_consignee} name={"Consignee"} />
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 0,
  },
  eventTitle: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
