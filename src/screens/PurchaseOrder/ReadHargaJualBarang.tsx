import React, { useEffect, useState, useCallback } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {
  Root,
  DateTime,
  Space,
  FileInput,
  ErrorText,
  Heading5,
} from "../../components";
import {
  Card,
  Caption,
  Badge,
  Paragraph,
  TextInput,
  Button,
  IconButton,
  Portal,
  Dialog,
} from "react-native-paper";
import Colors from "../../config/Colors";
import { DateFormat, DateFormat3, onlyNumber } from "../../services/utils";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  priceconsigneeFetchOne,
  priceconsigneeUpdate,
} from "../../redux/priceconsigneeReducer";
import { formatRupiah } from "./../../services/utils";
import { Formik } from "formik";
import * as Yup from "yup";

const Schema = Yup.object().shape({
  harga: Yup.string().required("Isi Harga"),
  id: Yup.number().required("Required"),
});

export default function ReadHargaJualBarang({ navigation, route }) {
  const { price_id } = route.params;
  const dispatch = useDispatch();
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState({});
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const { purchase_order, po_detail_barang } = useSelector(function (
    state: RootState
  ) {
    return state.priceconsignee.priceconsigneeViewSuccess
      ? state.priceconsignee.priceconsigneeViewSuccess.data
      : { purchase_order: {}, po_detail_barang: [] };
  });
  const update = useSelector(function (state: RootState) {
    return state.priceconsignee.priceconsigneeAddLoading;
  });
  const fetchData = () => {
    dispatch(priceconsigneeFetchOne(price_id));
  };
  const onRefresh = () => {
    fetchData();
  };
  const showDialog = useCallback(
    (item) => {
      setVisible(true);
      setData(item);
    },
    [visible]
  );
  const hideDialog = useCallback(() => {
    setVisible(false);
  }, [visible]);
  useEffect(() => {
    fetchData();
  }, [update]);

  return (
    <>
      <Root>
        <Card style={styles.card}>
          <Caption>Nomor PO</Caption>
          <Paragraph>{purchase_order.no_po}</Paragraph>
          <Caption>Status</Caption>
          <Badge
            style={{
              alignSelf: "flex-start",
              backgroundColor: purchase_order.already_set_harga
                ? Colors.success
                : Colors.grayL,
            }}
          >
            {purchase_order.already_set_harga
              ? "Harga Sudah Diset"
              : "Harga Belum Diset"}
          </Badge>
          <Caption>Tanggal PO</Caption>
          <Paragraph>{DateFormat(purchase_order.date_po)}</Paragraph>
          <Caption>Supplier</Caption>
          <Paragraph>{purchase_order.supplier_name}</Paragraph>
          <Caption>Shipper</Caption>
          <Paragraph>{purchase_order.shipper_name}</Paragraph>
          <Caption>Rute</Caption>
          <Paragraph>
            {purchase_order.origin} ➤ {purchase_order.destination}
          </Paragraph>
        </Card>
        <Space height={5} />
        <Card style={[styles.card, { elevation: 0 }]}>
          <Heading5>Detail Barang</Heading5>
          <Space height={10} />
          {po_detail_barang.map((item, key) => (
            <Card
              key={key}
              elevation={0}
              style={{ borderBottomWidth: 1, borderColor: Colors.grayL }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 4 }}>
                  <Paragraph>
                    {item.nama_barang}
                    <Caption> ({item.kode_barang})</Caption>
                  </Paragraph>
                  <Caption>
                    Harga Barang :{" "}
                    <Paragraph style={styles.money}>
                      Rp.{formatRupiah(parseInt(item.harga_satuan))}
                    </Paragraph>
                  </Caption>
                  <Caption>
                    Harga Jual :{" "}
                    <Paragraph style={styles.money}>
                      Rp.
                      {formatRupiah(parseInt(item.harga_satuan_consignee || 0))}
                    </Paragraph>
                  </Caption>
                </View>
                <View style={styles.center}>
                  <Caption>{item.satuan}</Caption>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "flex-end",
                  }}
                >
                  <IconButton
                    icon="menu-down"
                    color={Colors.gray5}
                    onPress={function () {
                      showDialog(item);
                    }}
                  />
                </View>
              </View>
            </Card>
          ))}
        </Card>
      </Root>
      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>{data.nama_barang}</Dialog.Title>
          <Dialog.Content>
            <Formik
              validationSchema={Schema}
              initialValues={{
                id: data.id,
                harga: "",
              }}
              onSubmit={function (val) {
                // alert(JSON.stringify(val));
                hideDialog();
                dispatch(priceconsigneeUpdate(val));
              }}
            >
              {({
                handleBlur,
                handleChange,
                handleSubmit,
                setFieldValue,
                values,
                errors,
                touched,
              }) => (
                <>
                  <TextInput
                    label="Harga (Rp)"
                    mode="outlined"
                    keyboardType="decimal-pad"
                    onChangeText={function (text) {
                      setFieldValue("harga", onlyNumber(text));
                    }}
                    onBlur={handleBlur("harga")}
                    value={values.harga}
                  />
                  {errors.harga && touched.harga && (
                    <ErrorText>{errors.harga}</ErrorText>
                  )}
                  <Button onPress={handleSubmit}>Done</Button>
                </>
              )}
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>
    </>
  );
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
  },
  money: {
    color: "orangered",
  },
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
