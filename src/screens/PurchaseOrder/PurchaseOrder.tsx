import React, { useState, useCallback } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import { Space, EmptyState, FilterButton } from "../../components";
import {
  Card,
  Paragraph,
  Caption,
  IconButton,
  Badge,
  List,
  Button,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { ConsigneeStackProps } from "../Navigator";
import {
  purchaseFetchPage,
  purchaseFilterClear,
  purchaseFilterDel,
} from "../../redux/purchaseReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import SearchPurchaseOrder from "./SearchPurchaseOrder";
import { getUserType, seStatusProps, DateFormat } from "../../services/utils";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { isEmptyArray } from "formik";
function PurchaseOrderList({
  navigation,
}: ConsigneeStackProps<"PurchaseOrder">) {
  const dispatch = useDispatch();
  const [refresh, setRefresh] = useState(false);
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const {
    isConsignee,
    isShipper,
    isSupplier,
    isReseller,
    isOperator,
    isRegulator,
  } = getUserType(usertype);
  const listConsignee = useSelector(function (state: RootState) {
    return state.consignee.consigneeGetSuccess
      ? state.consignee.consigneeGetSuccess.data
      : {};
  });
  const listSupplier = useSelector(function (state: RootState) {
    return state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : {};
  });
  const polist = useSelector(function (state: RootState) {
    return state.purchase.purchaseData ? state.purchase.purchaseData : [];
  });
  const filter = useSelector(function (state: RootState) {
    return state.purchase.purchaseFilter ? state.purchase.purchaseFilter : {};
  });
  const {
    currentPage,
    hasNext,
    hasPrev,
    pageTotal,
    recordsTotal,
  } = useSelector(function (state: RootState) {
    return state.purchase.purchaseGetSuccess
      ? state.purchase.purchaseGetSuccess
      : false;
  });
  const loading = useSelector(function (state: RootState | any) {
    return state.purchase.purchaseGetLoading;
  });
  const fetchData = useCallback(
    (filter = {}) => {
      const isAprove = isShipper ? 1 : "";
      const fetchParams: any = {
        length: "",
        no_po: "",
        is_approve: isAprove,
        is_approve_by_shipper: "",
        consignee_id: isConsignee ? id : "",
        supplier_id: isSupplier ? id : "",
        shipper_id: isShipper ? id : "",
        reseller_id: isReseller ? id : "",
        dari: "",
        sampai: "",
        start: 0,
        ...filter,
      };

      dispatch(purchaseFetchPage(fetchParams));
      dispatch(consigneeFetchAll());
      dispatch(supplierFetchAll());
    },
    [polist]
  );

  function handleReachEnd(filter = {}) {
    dispatch(
      purchaseFetchPage({
        length: 10,
        start: currentPage * 10,
        consignee_id: isConsignee ? `${id}` : "",
        supplier_id: isSupplier ? `${id}` : "",
        ...filter,
      })
    );
  }

  const onRefresh = () => {
    dispatch(purchaseFilterClear());
  };

  React.useEffect(() => {
    filter === {} ? null : fetchData(filter);
  }, [filter]);
  // if(isEmptyArray(polist)) {return <EmptyState/>}
  return (
    <View style={{flex:1, }}>
      <FlatList
        data={polist}
        onEndReachedThreshold={0.1}
        maxToRenderPerBatch={6}
        alwaysBounceVertical={true}
        removeClippedSubviews={true}
        ListFooterComponent={<Space height={100} />}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        ListEmptyComponent={() => <EmptyState/>}
        renderItem={function ({ item }) {
          return (
            <List.Item
              title={item.no_po}
              onPress={() => navigation.navigate('ReadPurchaseOrder', {id: item.id})}
              right={() => <List.Icon icon="chevron-right"/>}
              style={{borderBottomWidth: 0.3}}
              description={
                seStatusProps(
                  item.is_approve,
                  item.is_approve_by_shipper,
                  usertype
                ).label
              }
              descriptionStyle={seStatusProps(
                item.is_approve,
                item.is_approve_by_shipper,
                usertype
              ).color}
            />
          );
        }}
        onRefresh={onRefresh}
        refreshing={loading}
        keyExtractor={function (item, key) {
          return String(key);
        }}
      />

      <FilterButton
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "FilterPurchaseOrder",
            params: {
              supplier: listSupplier,
              consignee: listConsignee,
            },
          });
        }}
      />
    </View>
  );
}

function FloatingAdd({ navigation }: ConsigneeStackProps<"PurchaseOrder">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("ChooseSupplier");
      }}
    />
  );
}

function PurchaseOrder({
  navigation,
  route,
}: ConsigneeStackProps<"PurchaseOrder">) {
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  return (
    <View style={styles.container}>
      <PurchaseOrderList navigation={navigation} route={route} />
      {usertype !== "Consignee" ? null : (
        <FloatingAdd navigation={navigation} route={route} />
      )}
    </View>
  );
}

export default PurchaseOrder;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    margin: 5,
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  boxSearch: {
    backgroundColor: Colors.gray1,
    flex: 1,
    marginRight: 10,
    borderRadius: 4,
    padding: 8,
    justifyContent: "center",
  },
  cardSearch: {
    margin: 0,
    paddingVertical: 10,
  },
  emptyWrapper: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  headerComponent: { justifyContent: "center", alignSelf: "center" },
});


{/* <Card
  style={styles.card}
  onPress={function () {
    navigation.navigate("ReadPurchaseOrder", {
      id: item.id,
    });
  }}
>
  <Paragraph
    selectable
    style={{ fontWeight: "bold", fontSize: 16, flex: 3 }}
  >
    No : {item.no_po}
  </Paragraph>
  <View style={{ flexDirection: "row" }}>
    <Caption style={{ flex: 2 }}>
      {DateFormat(item.date_po)}{" "}
    </Caption>
    <Badge
      style={{
        flex: 1,
        alignSelf: "flex-start",
        backgroundColor: seStatusProps(
          item.is_approve,
          item.is_approve_by_shipper,
          usertype
        ).color,
        paddingHorizontal: 10,
      }}
    >
      {
        seStatusProps(
          item.is_approve,
          item.is_approve_by_shipper,
          usertype
        ).label
      }
    </Badge>
  </View>
  <Caption style={{ fontSize: 13 }}>
    Supplier: {item.supplier_name}
    {"\n"}
    JPT: {item.shipper_nama_perusahaan}
    {"\n"}
    <Caption>
      {item.origin}({item.port_origin_code}) ➤ {item.destination}(
      {item.port_destination_code})
    </Caption>
  </Caption>
</Card> */}