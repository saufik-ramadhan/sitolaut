import React, { useState, Fragment } from "react";
import { StyleSheet, View, FlatList, Alert } from "react-native";
import { Icon, PickerCostum, Space, LoadingComponent } from "../../components";
import {
  Card,
  Caption,
  Button,
  Paragraph,
  IconButton,
  Modal,
  TextInput,
  Portal,
  Badge,
  Divider,
  Subheading,
  HelperText,
  Avatar,
  List,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import {
  purchaseFetchOne,
  purchaseSupplierApproval,
  purchaseShipperApproval,
  purchaseFetchPage,
  purchaseSetFcl,
} from "../../redux/purchaseReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  formatRupiah,
  DateTimeFormat,
  DateFormat,
  seStatusProps,
  getUserType,
  getConfirmationStatus,
} from "../../services/utils";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import { UPLOAD_URL } from "../../config/constants";
function Row({
  title = "untitled",
  content = "undefined",
  bill = "20000",
  total = "20",
  contentAlign = "right",
}) {
  return (
    <View
      style={{
        flexDirection: "row",
        paddingVertical: 2,
        borderBottomWidth: 1,
        borderBottomColor: Colors.gray1,
      }}
    >
      <View style={{ flex: 1 }}>
        <Caption
          style={{
            fontSize: 13,
            color: Colors.for,
            fontWeight: "bold",
          }}
        >
          {title ? title : "Consignee"}
        </Caption>
      </View>
      <View
        style={{
          alignItems: "flex-start",
          flex: 1,
          marginLeft: 5,
        }}
      >
        <Caption
          style={{
            fontSize: bill ? 14 : total ? 16 : 13,
            color: Colors.gray6,
            textAlign: contentAlign ? contentAlign : "left",
          }}
        >
          {content ? content : "PT Consignee Sejahtera"}
        </Caption>
      </View>
    </View>
  );
}

function GeneralInformation({ data }: any) {
  const user_type = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state?.auth?.authLoginSuccess?.data.user[0].usertype
      : {
          usertype: "Not Defined",
        };
  });
  const { is_approve, date_po, no_po, is_approve_by_shipper, quota } = data;
  return (
    <Card style={{ padding: 10, elevation: 5 }}>
      <View style={[styles.row]}>
        <Caption>Status</Caption>
        <Badge
          style={[
            styles.description,
            {
              backgroundColor: seStatusProps(
                is_approve,
                is_approve_by_shipper,
                user_type
              ).color,
            },
          ]}
        >
          {seStatusProps(is_approve, is_approve_by_shipper, user_type).label}
        </Badge>
      </View>
      <View style={[styles.row]}>
        <Caption>
          Tanggal Pesanan{"\n"}
          <Caption style={styles.description}>{DateFormat(date_po)}</Caption>
        </Caption>
      </View>
      <View style={[styles.row]}>
        <Caption>
          Nomor Purchase Order{"\n"}
          <Caption style={styles.description}>{no_po}</Caption>
        </Caption>
      </View>
      <View style={[styles.row]}>
        <Caption>
          Justifikasi Quota Container{"\n"}
          <Caption style={styles.description}>{quota}</Caption>
        </Caption>
      </View>
    </Card>
  );
}
function DetailPengiriman({ data }: any) {
  const {
    consignee_name,
    supplier_name,
    shipper_nama_perusahaan,
    trayek_id,
    operator_kapal_name,
    port_origin,
    port_destination,
    tanggal_berangkat,
    tanggal_tiba,
  } = data;
  return (
    <Card style={styles.detailPengiriman}>
      <Row title={"Consignee"} content={consignee_name} />
      <Row title={"Supplier"} content={supplier_name} />
      <Row title={"JPT"} content={shipper_nama_perusahaan} />
      <Row title={"Nomor Trayek"} content={trayek_id} />
      <Row title={"Operator"} content={operator_kapal_name} />
      <Row title={"Pelabuhan Asal"} content={port_origin} />
      <Row title={"Pelabuhan Tujuan"} content={port_destination} />
      <Row
        title={"ATD (Departure)"}
        content={DateTimeFormat(tanggal_berangkat)}
      />
      <Row title={"ATA (Arrival)"} content={DateTimeFormat(tanggal_tiba)} />
    </Card>
  );
}


function DaftarBarang({ data }: any) {
  const {
    detail_barang,
    total_harga,
    biaya_pengurusan,
    biaya_pelabuhan_muat,
    biaya_angkut,
    biaya_pelabuhan_bongkar,
    biaya_stripping,
  } = data;
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  let totalHargaBarang = detail_barang ? detail_barang.reduce((a, b) => (a + parseInt(b.harga_total)), 0) : 0;
  // React.useEffect(() => {
  //   alert(JSON.stringify(data));
  // }, [])
  return (
    <FlatList
      data={detail_barang}
      showsVerticalScrollIndicator={false}
      ListEmptyComponent={
        <Caption style={{ textAlign: "center" }}>No Data</Caption>
      }
      ListHeaderComponent={<GeneralInformation data={data} />}
      ListFooterComponent={
        <>
          <List.Item 
            title={"Rp. " + formatRupiah(totalHargaBarang)} 
            description={'Total Harga Barang'} 
            titleStyle={{textAlign: 'right', color: 'orange', fontWeight: 'bold', fontSize: 16}}
            descriptionStyle={{textAlign: 'right'}}/>
          <DetailPengiriman data={data} />
          <View style={{ padding: 10 }}>
            <Paragraph
              style={{
                color: Colors.pri,
                fontWeight: "bold",
                fontSize: 15,
                flex: 1,
              }}
            >
              BIAYA KIRIM
            </Paragraph>
            {usertype === "Shipper" ? (
              <>
                <Row
                  title={"Pengurusan"}
                  content={"Rp. " + formatRupiah(parseInt(biaya_pengurusan))}
                />
                <Row
                  title={"Pelabuhan muat"}
                  content={
                    "Rp. " + formatRupiah(parseInt(biaya_pelabuhan_muat))
                  }
                />
                <Row
                  title={"Angkut"}
                  content={"Rp. " + formatRupiah(parseInt(biaya_angkut))}
                />
                <Row
                  title={"Pelabuhan bongkar"}
                  content={
                    "Rp. " + formatRupiah(parseInt(biaya_pelabuhan_bongkar))
                  }
                />
                <Row
                  title={"Stripping"}
                  content={"Rp. " + formatRupiah(parseInt(biaya_stripping))}
                />
              </>
            ) : null}

            <List.Item
              title={"Rp. " + formatRupiah(
                parseInt(biaya_pengurusan) +
                parseInt(biaya_pelabuhan_muat) +
                parseInt(biaya_angkut) +
                parseInt(biaya_pelabuhan_bongkar) +
                parseInt(biaya_stripping)
              )}
              description={"Biaya Pengiriman"}
              titleStyle={{textAlign: 'right', color: 'orange', fontWeight: 'bold', fontSize: 16}}
              descriptionStyle={{textAlign: 'right'}}
            />
          </View>
          <List.Item
            title={"Rp. "+ formatRupiah(
              totalHargaBarang+
              parseInt(biaya_pengurusan) +
              parseInt(biaya_pelabuhan_muat) +
              parseInt(biaya_angkut) +
              parseInt(biaya_pelabuhan_bongkar) +
              parseInt(biaya_stripping)
            )}
            description={"Total Harga"}
            centered={true}
            titleStyle={{textAlign: 'left', color: 'orangered', fontWeight: 'bold', fontSize: 20}}
            descriptionStyle={{textAlign: 'left'}}
          />
        </>
      }
      renderItem={function ({ item }) {
        const {
          harga_satuan,
          harga_total,
          nama_barang,
          satuan,
          qty,
          gambar,
        } = item;
        return (
          <Card
            style={{
              padding: 10,
              margin: 5,
              marginTop: 5,
              marginBottom: 0,

              elevation: 3,
              borderRadius: 5,
            }}
          >
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <View style={{ flex: 5 }}>
                <Paragraph
                  style={{
                    color: Colors.pri,
                    fontSize: 12,
                  }}
                >
                  {nama_barang}
                  {"\n"}
                  <Caption style={{ flex: 2 }}>
                    Rp.{formatRupiah(parseInt(harga_satuan))} / {satuan}
                  </Caption>
                </Paragraph>
              </View>
              <Caption
                style={{
                  flex: 4,
                  textAlign: "right",
                }}
              >
                {"x "}
                {qty}
                {"\n"}
                <Caption
                  style={{
                    color: "orange",
                    fontSize: 13,
                    fontWeight: "bold",
                  }}
                >
                  Rp. {formatRupiah(parseInt(harga_total))}
                </Caption>
              </Caption>
            </View>
          </Card>
        );
      }}
      keyExtractor={function (item) {
        return JSON.stringify(item.id);
      }}
    />
  );
}
export default function ReadPurchaseOrder(props: any) {
  const id_po = props.route.params.id;
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const poView = useSelector(function (state: RootState | any) {
    return state.purchase.purchaseViewSuccess
      ? state.purchase.purchaseViewSuccess.data
      : [{}];
  });
  const loading = useSelector(function (state: RootState) {
    return state.purchase.purchaseViewLoading;
  });
  const purchaseAddSuccess = useSelector(function (state: RootState) {
    return state.purchase.purchaseAddLoading;
  });
  const dispatch = useDispatch();
  const [isSuccess, setSuccess] = useState(false);
  const isShowActionButton = (is_approve: any, is_approve_by_shipper: any) => {
    const {
      supplierPending,
      shipperPending,
      supplierAccepted,
    } = getConfirmationStatus(is_approve, is_approve_by_shipper);
    const { isShipper, isSupplier } = getUserType(usertype);
    if ((isSupplier && supplierPending) || (isShipper && shipperPending))
      return true;
    else return false;
  };
  const { isShipper } = getUserType(usertype);
  const { is_approve, is_approve_by_shipper, is_fcl } = poView;
  const showFormType =
    isShipper &&
    is_approve == 1 &&
    is_approve_by_shipper == 1 &&
    is_fcl == null;
  React.useEffect(() => {
    dispatch(purchaseFetchOne(id_po));
  }, [purchaseAddSuccess]);

  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      {!loading && (
        <>
          <DaftarBarang data={poView} />
          <ActionButton
            visible={isShowActionButton(is_approve, is_approve_by_shipper)}
            data={poView}
            usertype={usertype}
          />
          <FormOrderType visible={showFormType} id={id_po} />
        </>
      )}
      <PlaceholderLoading loading={loading} />
    </View>
  );
}

const ActionButton = ({ visible, data, usertype }: any) => {
  const dispatch = useDispatch();
  const [modal, showModal] = useState(false);
  const [reason, setReason] = useState("");
  var {
    id,
    biaya_pelabuhan_muat,
    biaya_angkut,
    biaya_pelabuhan_bongkar,
    total_harga,
  } = data;

  const SupplierAcceptForm = {
    is_approve: 1,
    total_harga,
    biaya_pelabuhan_muat,
    biaya_angkut,
    biaya_pelabuhan_bongkar,
    id,
    reject_reason_supplier: reason,
  };
  const SupplierRejectForm = {
    is_approve: 2,
    total_harga,
    biaya_pelabuhan_muat,
    biaya_angkut,
    biaya_pelabuhan_bongkar,
    id,
    reject_reason_supplier: reason,
  };

  const shipperAcceptForm = {
    id,
    is_approve_by_shipper: 1,
    reject_reason_shipper: reason,
  };

  const shipperRejectForm = {
    id,
    is_approve_by_shipper: 2,
    reject_reason_shipper: reason,
  };

  const onApprovePo = async () => {
    const { isSupplier } = getUserType(usertype);
    if (isSupplier) {
      dispatch(purchaseSupplierApproval(SupplierAcceptForm));
    } else {
      dispatch(purchaseShipperApproval(shipperAcceptForm));
    }
  };

  const onRejectPo = async () => {
    const { isSupplier } = getUserType(usertype);
    if (isEmpty) {
      return;
    }
    if (isSupplier) {
      dispatch(purchaseSupplierApproval(SupplierRejectForm));
    } else {
      dispatch(purchaseShipperApproval(shipperRejectForm));
    }
    showModal(!modal);
  };

  const approve = () => {
    Alert.alert(
      "Perhatian",
      "Apakah anda yakin akan menerima purchase order ini?",
      [
        {
          text: "Batal",
          style: "cancel",
        },
        {
          text: "Terima",
          onPress: onApprovePo,
        },
      ]
    );
  };
  const isEmpty = reason === "";
  return visible ? (
    <View>
      <Caption style={styles.statusCaption}>
        Sedang menunggu konfirmasi anda
      </Caption>
      <View style={styles.actionContainer}>
        <Button
          style={styles.btnAction}
          mode="outlined"
          color={Colors.danger}
          icon="close"
          onPress={() => showModal(!modal)}
        >
          Tolak
        </Button>
        <Button
          style={styles.btnAction}
          mode="contained"
          color={Colors.pri}
          icon="check"
          onPress={approve}
        >
          Terima
        </Button>
        <Portal>
          <Modal
            onDismiss={() => showModal(!modal)}
            visible={modal}
            contentContainerStyle={{ padding: 10 }}
          >
            <Card>
              <Card.Title
                title="Yakin Untuk Menolak PO ini?"
                subtitle="Alasan Penolakan : "
              />
              <Card.Content>
                <TextInput
                  placeholder="Masukan alasan penolakan"
                  multiline={true}
                  numberOfLines={3}
                  mode="outlined"
                  error={isEmpty}
                  onChangeText={(text) => setReason(text)}
                />
              </Card.Content>
              <Card.Actions style={styles.cardAction}>
                <Button onPress={() => showModal(!modal)} color={Colors.gray10}>
                  Batal
                </Button>
                <Button
                  color={Colors.danger}
                  style={{ marginLeft: 10, width: 100 }}
                  mode="contained"
                  onPress={onRejectPo}
                >
                  Ok
                </Button>
              </Card.Actions>
            </Card>
          </Modal>
        </Portal>
      </View>
    </View>
  ) : null;
};

const FormOrderType = ({ visible, id }: boolean | any) => {
  const dispatch = useDispatch();
  const fclOption = [
    { value: 0, label: "LCL (Less Container Loaded)" },
    { value: 1, label: "FCL (Full Container Loaded)" },
  ];
  const loading = useSelector(function (state: RootState) {
    return state.purchase.purchaseAddLoading;
  });
  const [is_fcl, setFcl]: any = useState("");
  const [biayapengurusan, setBiayaPengurusan] = useState(false);
  const onSetFcl = (value: any) => {
    setFcl(value.value);
  };

  const handleSetFcl = () => {
    let values: any = {};
    if (is_fcl === "") {
      alert("Form type order wajib diisi.");
    } else {
      if (is_fcl === 0 && biayapengurusan) {
        values.biayapengurusan = biayapengurusan;
      } else {
        setBiayaPengurusan(0);
        delete values.biayapengurusan;
      }
      values.id = id;
      values.is_fcl = is_fcl;
      console.log(values);
      dispatch(purchaseSetFcl(values));
    }
  };

  return visible ? (
    <Fragment>
      <Card>
        <Card.Content>
          <Subheading>Tentukan Tipe Order : </Subheading>
          <Divider />
          <PickerCostum
            data={fclOption}
            objectKey={"label"}
            initLabel="Pilih Tipe"
            onValueChange={onSetFcl}
          />
          <Space height={10} />
          {is_fcl === 0 && (
            <TextInput
              onChangeText={(text) => setBiayaPengurusan(text)}
              label="Biaya Pengurusan"
              keyboardType="decimal-pad"
            />
          )}
          <Button icon="check" mode="contained" onPress={handleSetFcl}>
            Simpan
          </Button>
        </Card.Content>
      </Card>
      <LoadingComponent visible={loading} />
    </Fragment>
  ) : null;
};

const styles = StyleSheet.create({
  actionContainer: { flexDirection: "row", height: 50 },
  cardAction: { justifyContent: "flex-end", paddingHorizontal: 20 },
  btnAction: { flex: 1, justifyContent: "center", margin: 5 },
  description: {
    fontSize: 12,
    fontWeight: "bold",
    paddingHorizontal: 10,
    alignSelf: "flex-start",
    borderRadius: 10,
  },
  detailPengiriman: {
    padding: 10,
    elevation: 3,
    margin: 5,
    borderRadius: 5,
  },
  title: {
    fontSize: 15,
  },
  statusCaption: {
    borderWidth: 1,
    padding: 5,
    borderColor: Colors.sec,
    marginHorizontal: 10,
    textAlign: "center",
  },
});
