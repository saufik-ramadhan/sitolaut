import React from "react";
import { Root, Heading3, Heading4, Space, Heading5, Bold } from "../components";
import { Image, View, Text, StyleSheet, ImageBackground } from "react-native";
import {
  Paragraph,
  Subheading,
  TouchableRipple,
  Caption,
  Card,
  IconButton,
} from "react-native-paper";
import recentpo from "../dummy/recentpo";
import Colors from "../commons/Colors";

function Profile() {
  return (
    <View
      style={{
        flexDirection: "row",
        margin: 5,
        backgroundColor: "rgba(255,255,255,0.9)",
        padding: 10,
        borderRadius: 5,
      }}
    >
      <View style={{ flex: 5 }}>
        <Heading5>PT Nutech Integrasi</Heading5>
        <Caption>Consignee</Caption>
      </View>
      <Image
        source={require("../assets/medias/images/download.png")}
        style={{ height: 40, width: 40 }}
      />
    </View>
  );
}

/**
 * Contain brief information of whats
 * below this component
 * or what to do next
 */
function Jumbotron({ children, color }) {
  return (
    <Card
      style={{
        backgroundColor: color,
        padding: 5,
        marginHorizontal: 10,
        elevation: 5,
      }}
    >
      {children}
    </Card>
  );
}

function CloseButton({ ...props }) {
  return (
    <IconButton
      {...props}
      icon="close"
      style={{ position: "absolute", top: 0, right: 0, margin: 0, padding: 0 }}
    />
  );
}

function LatestPurchase() {
  return (
    <>
      <View
        style={{
          borderLeftColor: "midnightblue",
          borderLeftWidth: 3,
          paddingLeft: 10,
        }}
      >
        <Heading4 style={{ color: "midnightblue" }}>
          Latest Purchase Order
        </Heading4>
      </View>
      <Space height={10} />
      {recentpo.map(function (item, key) {
        return (
          <TouchableRipple
            key={key}
            onPress={() => console.log("Pressed")}
            style={{
              backgroundColor: "white",
              width: "100%",
              padding: 1,
              marginVertical: 5,
              borderRadius: 5,
              elevation: 5,
            }}
          >
            <View
              style={{
                marginVertical: 8,
                marginHorizontal: 5,
                borderRadius: 5,
                borderLeftColor:
                  item.Status === "Diterima"
                    ? Colors.accent
                    : item.Status === "Ditolak"
                    ? "red"
                    : item.Status === "Menunggu Konfirmasi"
                    ? "#888"
                    : "#888",
              }}
            >
              <View
                style={{
                  backgroundColor:
                    item.Status === "Diterima"
                      ? "#C0F0D4"
                      : item.Status === "Ditolak"
                      ? "#F9D0CD"
                      : item.Status === "Menunggu Konfirmasi"
                      ? "#E3E3E3"
                      : "#E3E3E3",
                  borderRadius: 5,
                  paddingVertical: 5,
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 14,
                    color: "#555",
                  }}
                >
                  {item.Status === "Diterima"
                    ? "Pesanan Diterima"
                    : item.Status === "Ditolak"
                    ? "Pesanan Ditolak"
                    : item.Status === "Menunggu Konfirmasi"
                    ? "Menunggu Konfirmasi"
                    : "#E3E3E3"}
                </Text>
              </View>
              <View>
                <View
                  style={{
                    paddingHorizontal: 10,
                    borderBottomWidth: 1,
                    borderBottomColor: "#ccc",
                    paddingVertical: 5,
                    flexDirection: "row",
                  }}
                >
                  <View style={{ flex: 1, justifyContent: "center" }}>
                    <Text style={{ color: "#999" }}>{item.Date} 2019</Text>
                    <Text
                      style={{
                        fontWeight: "bold",
                        color: "#888",
                      }}
                    >
                      {item.NoPO}
                    </Text>
                  </View>
                  {/** TRAYEK */}
                  <View style={{ flex: 1, alignItems: "flex-end" }}>
                    <View
                      style={{
                        backgroundColor: "#eee",
                        padding: 3,
                        borderRadius: 3,
                        marginBottom: 3,
                      }}
                    >
                      <Text style={{ fontSize: 12, color: "#555" }}>TR15</Text>
                    </View>
                    {/** JALUR */}
                    <View
                      style={{
                        backgroundColor: "#eee",
                        padding: 3,
                        borderRadius: 3,
                      }}
                    >
                      <Text style={{ fontSize: 12, color: "#555" }}>
                        Surabaya ➡ Morotai
                      </Text>
                    </View>
                    {/** -- */}
                  </View>
                </View>
                <View style={{ padding: 10 }}>
                  <Text style={{ marginBottom: 5 }}>
                    <Text
                      style={{
                        fontSize: 13,
                        color: "#999",
                        fontWeight: "bold",
                      }}
                    >
                      Supplier :{" "}
                    </Text>
                    {"\n"}PT SUPPLIER APLUS
                  </Text>
                  <Text>
                    <Text
                      style={{
                        fontSize: 13,
                        color: "#999",
                        fontWeight: "bold",
                      }}
                    >
                      Jasa Pengurusan Transportasi :{" "}
                    </Text>
                    {"\n"}
                    {item.Supplier}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableRipple>
        );
      })}
    </>
  );
}

export default function HomeScreen() {
  return (
    <Root>
      <ImageBackground
        source={require("./../assets/medias/images/tollaut.png")}
        style={{ padding: 5 }}
      >
        <Profile />
      </ImageBackground>
      <Jumbotron color="skyblue">
        <Heading5>Halo,</Heading5>
        <Paragraph>
          Selamat datang di aplikasi LCS, sebagai <Bold>Consignee</Bold> anda
          dapat memulai dengan membuat Purchase Order pada halaman Purchase
          Order
        </Paragraph>
        <CloseButton
          onPress={function () {
            console.log("pressed");
          }}
        />
      </Jumbotron>
      <Space height={15} />
      <View
        style={{
          paddingHorizontal: 10,
          elevation: 10,
          backgroundColor: "white",
        }}
      >
        <Space height={15} />
        <LatestPurchase />
      </View>
    </Root>
  );
}

const styles = StyleSheet.create({});
