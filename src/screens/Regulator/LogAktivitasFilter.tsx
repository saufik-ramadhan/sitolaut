import React, { useEffect } from "react";
import { StyleSheet, Text, View, FlatList, TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  Root,
  Space,
  ErrorText,
  DateTime,
  SelectInput,
} from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  Button,
} from "react-native-paper";
import { OperatorStackProps, RegulatorStackProps } from "../Navigator";
import {
  activityFetchPage,
  activityFilterAdd,
} from "./../../redux/activityReducer";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { portFetchAll } from "../../redux/portReducer";
import * as Yup from "yup";
import { DateFormatStrip } from "../../services/utils";
import { operatorFetchAll } from "../../redux/operatorReducer";

export default function LogAktivitasFilter({
  navigation,
  route,
}: RegulatorStackProps<"LogAktivitasFilter">) {
  const dispatch = useDispatch();
  const operator = useSelector(function (state: RootState) {
    return state.operator.operatorGetSuccess
      ? state.operator.operatorGetSuccess.data
      : [];
  });
  /**
   * Dispatch
   */
  const fetchData = (data) => {
    dispatch(activityFilterAdd(data));
    navigation.goBack();
  };

  useEffect(() => {
    dispatch(operatorFetchAll());
  }, []);

  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          activity_date_end: "",
          activity_date_start: "",
          activity_desc: "",
          operator_id: "",
        }}
        onSubmit={(values) => {
          fetchData(values);
          // alert(JSON.stringify(values));
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Operator
             */}
            <SelectInput
              label="Operator"
              onChangeValue={(val) => setFieldValue("operator_id", val.id)}
              options={operator}
              objectKey="label"
              withSearch
            />
            {/**
             * Nama LogAktivitas
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.activity_desc}
              onChangeText={handleChange("activity_desc")}
              onBlur={handleBlur("activity_desc")}
              placeholder="Deskripsi"
            />
            {errors.activity_desc && touched.activity_desc && (
              <ErrorText>{errors.activity_desc}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Dari
             */}
            <DateTime
              label="Dari Tanggal"
              onChangeValue={(date) =>
                setFieldValue("activity_date_start", DateFormatStrip(date))
              }
            />
            <Space height={10} />
            {/**
             * Sampai
             */}
            <DateTime
              label="Sampai Tanggal"
              onChangeValue={(date) =>
                setFieldValue("activity_date_end", DateFormatStrip(date))
              }
            />
            <Space height={10} />

            <Button icon={"filter"} onPress={handleSubmit} mode="contained">
              Filter
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
