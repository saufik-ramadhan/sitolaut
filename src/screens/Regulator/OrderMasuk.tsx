import React from "react";
import { RegulatorStackProps } from "../Navigator";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { orderPerBulan } from "../../redux/dashboardReducer";
import { Heading5 } from "../../components";
import { LineChart } from "react-native-chart-kit";
import { Dimensions } from "react-native";
import Colors from "../../config/Colors";
import { Card, Paragraph } from "react-native-paper";

export default function OrderMasuk({
  navigation,
  route,
}: RegulatorStackProps<"OrderMasuk">) {
  const current = new Date();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const dispatch = useDispatch();
  const order_per_bulan = useSelector(function (state: RootState) {
    return state.dashboard.orderPerBulanSuccess
      ? state.dashboard.orderPerBulanSuccess.data
      : [];
  });
  React.useEffect(() => {
    console.log(current.getMonth() + 1);
    dispatch(
      orderPerBulan({
        month: current.getMonth() + 2,
        port_destination_id: "",
        port_origin_id: "",
        trayek_id: "",
      })
    );
  }, []);
  return (
    <>
      {order_per_bulan.map(function (item, key) {
        return (
          <Card key={key}>
            <Paragraph>Hi</Paragraph>
          </Card>
        );
      })}
    </>
  );
}
