import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, Icon, FilterButton } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
} from "react-native-paper";
import { OperatorStackProps, RegulatorStackProps } from "../Navigator";
import { msttrayekFetchPage } from "./../../redux/msttrayekReducer";
import { portFetchPage } from "../../redux/portReducer";
import {
  activityFetchPage,
  activityDelete,
  activityFilterClear,
  activityFetchAll,
} from "../../redux/activityReducer";
import { DateTimeFormat } from "../../services/utils";

function FloatingAdd({ navigation }: OperatorStackProps<"Activity">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateActivity");
      }}
    />
  );
}

export default function LogAktivitas({
  navigation,
  route,
}: RegulatorStackProps<"LogAktivitas">) {
  const dispatch = useDispatch();
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.activity.activityGetSuccess;
  });
  const activitys = useSelector(function (state: RootState) {
    return state.activity.activityData ? state.activity.activityData : [];
  });
  const filter = useSelector(function (state: RootState) {
    return state.activity.activityFilter;
  });
  const refresh = useSelector(function (state: RootState) {
    return state.activity.activityGetLoading;
  });
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const fetchData = (filter) => {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(activityFetchPage(params));
  };
  const handleReachEnd = (filter) => {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(activityFetchPage(params));
  };
  const onRefresh = () => {
    dispatch(activityFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter]);

  return (
    <>
      <FlatList
        data={activitys}
        ListHeaderComponent={() => <Space height={10} />}
        onRefresh={onRefresh}
        refreshing={refresh}
        ListEmptyComponent={() => <PlaceholderLoading loading={false} />}
        onEndReached={() => (hasNext ? handleReachEnd(filter) : null)}
        onEndReachedThreshold={0.5}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={[styles.card, { marginHorizontal: 10 }]}
              onPress={function () {
                null;
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 8 }}>
                  <Caption>{DateTimeFormat(item.activity_date)}</Caption>
                  <Paragraph style={{ fontSize: 16 }}>
                    {item.activity_desc}
                  </Paragraph>
                  <Caption>
                    User :{" "}
                    {item.nama_consignee ||
                      item.nama_supplier ||
                      item.nama_shipper ||
                      item.nama_reseller ||
                      item.nama_vessel_operator ||
                      item.nama_regulator}
                  </Caption>
                </View>
              </View>
            </Card>
          );
        }}
      />

      <FilterButton
        onPress={function () {
          navigation.navigate("LogAktivitasFilter");
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    padding: 10,
    marginBottom: 3,
  },
});
