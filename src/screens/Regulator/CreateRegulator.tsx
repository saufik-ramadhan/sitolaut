import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { DateTime, Root, SelectInput2, Space, TextInput } from '../../components'
import {Formik} from 'formik'
import { Button, Caption, HelperText, Paragraph, Subheading } from 'react-native-paper'
import { useDispatch, useSelector } from 'react-redux'
import { provinsiFetchAll } from '../../redux/provinsiReducer'
import { RootState } from '../../redux/rootReducer'
import { RegulatorStackProps } from '../Navigator'
import { kotaFetchByProvinsi } from '../../redux/kotaReducer'
import { userAdd } from '../../redux/userReducer'
import * as Yup from 'yup';
import { resolvePlugin } from '@babel/core'

/**
 * Create Regulator Validation Schema
 */
const CreateRegulatorValidation = Yup.object().shape({
  email_regulator: Yup.string().email('Email invalid').required().label('Email'),
  password_regulator: Yup.string().required('No password provided.').min(8, 'Password is too short - should be 8 chars minimum.').label('Password'),
  alamat: Yup.string().required().label('Alamat'),
  jabatan: Yup.string().required().label('Jabatan'),
  kota_id: Yup.number().required().label('Kota'),
  nama_regulator: Yup.string().required().label('Nama Lengkap'),
  provinsi_id: Yup.number().required().label('Provinsi'),
  telp: Yup.string().required().label('Nomor Telp.'),
  tempt_lahir: Yup.string().required().label('Tempat Lahir'),
  tgl_lahir: Yup.string().required().label('Tanggal Lahir'),
});


export default function CreateRegulator({navigation}: RegulatorStackProps<'CreateRegulator'>) {
  const dispatch = useDispatch();
  const { provinsi, kota } = useSelector((state: RootState) => state);
  const fetchData = () => {
    if(!provinsi.data) dispatch(provinsiFetchAll());
  }
  React.useEffect(() => {
    fetchData();
  },[])
  return (
    <>
      <Root>
        <View style={{padding: 20}}>
          <Formik 
            validationSchema={CreateRegulatorValidation}
            initialValues={{
              alamat: "",
              email_regulator: "",
              jabatan: "",
              kota_id: "",
              nama_regulator: "",
              password_regulator: "",
              provinsi_id: "",
              telp: "",
              tempt_lahir: "",
              tgl_lahir: "",
            }} 
            onSubmit={function(vals){
              console.log(vals);
              dispatch(userAdd(vals));
              // navigation.goBack();
            }}>
            {
              ({handleSubmit, handleChange, handleBlur, values, setFieldValue, errors}) => (
                <>
                  <Subheading>Data Login</Subheading>

                  <TextInput label="Email" onChangeText={handleChange('email_regulator')} onBlur={handleBlur('email_regulator')} mode="outlined" error={errors.email_regulator} />
                  <HelperText type="error" visible={errors.email_regulator}>{errors.email_regulator}</HelperText>
                  <TextInput label="Password" onChangeText={handleChange('password_regulator')} onBlur={handleBlur('password_regulator')} mode="outlined" error={errors.password_regulator} secureTextEntry/>
                  <HelperText type="error" visible={errors.password_regulator}>{errors.password_regulator}</HelperText>

                  <Space height={10}/>

                  <Subheading>Data Regulator</Subheading>

                  <TextInput label="Nama Lengkap" onChangeText={handleChange('nama_regulator')} onBlur={handleBlur('nama_regulator')} mode="outlined" error={errors.nama_regulator}/>
                  <HelperText type="error" visible={errors.nama_regulator}>{errors.nama_regulator}</HelperText>
                  <TextInput label="Nomor Telp." onChangeText={handleChange('telp')} onBlur={handleBlur('telp')} mode="outlined" keyboardType="number-pad" error={errors.telp}/>
                  <HelperText type="error" visible={errors.telp}>{errors.telp}</HelperText>
                  <TextInput label="Tempat Lahir" onChangeText={handleChange('tempt_lahir')} onBlur={handleBlur('tempt_lahir')} mode="outlined" error={errors.tempt_lahir}/>
                  <HelperText type="error" visible={errors.tempt_lahir}>{errors.tempt_lahir}</HelperText>
                  <Space height={5}/>
                  <DateTime
                    label="Tanggal Lahir"
                    onChangeValue={(date: string) =>
                      setFieldValue("tgl_lahir", date.date)
                    }
                    type="date"
                  />
                  <HelperText type="error" visible={errors.tgl_lahir}>{errors.tgl_lahir}</HelperText>
                  <TextInput label="Jabatan" onChangeText={handleChange('jabatan')} onBlur={handleBlur('jabatan')} mode="outlined" error={errors.jabatan}/>
                  <HelperText type="error" visible={errors.jabatan}>{errors.jabatan}</HelperText>
                  <Space height={5}/>
                  <SelectInput2
                    items={provinsi.provinsiGetSuccess.data || []} 
                    itemLabel="provinsi" 
                    itemValue="id"
                    label="Provinsi" 
                    onValueChange={(itemValue, itemIndex) => { setFieldValue('provinsi_id', itemValue); dispatch(kotaFetchByProvinsi(itemValue)); }} 
                    selectedValue={values.provinsi_id}
                    error={errors.provinsi_id}
                  />
                  <HelperText type="error" visible={errors.provinsi_id}>{errors.provinsi_id}</HelperText>
                  <Space height={5}/>
                  <SelectInput2 
                    items={kota.kotaGetSuccess.data || []} 
                    itemLabel="kota" 
                    itemValue="kota_id"
                    selectedValue={values.kota_id}
                    label="Kota / Kabupaten" 
                    onValueChange={(itemValue, itemIndex) => setFieldValue('kota_id', itemValue)}
                    error={errors.kota_id}
                  />
                  <HelperText type="error" visible={errors.kota_id}>{errors.kota_id}</HelperText>
                  <TextInput label="Alamat" onChangeText={handleChange('alamat')} onBlur={handleBlur('alamat')} mode="outlined" error={errors.alamat}/>
                  <HelperText type="error" visible={errors.alamat}>{errors.alamat}</HelperText>
                  <Space height={20}/>
                  <Button onPress={handleSubmit} mode="contained">Create Regulator</Button>
                  <Space height={20}/>
                </>
              )
            }
          </Formik>
        </View>
      </Root>
    </>
  )
}

const styles = StyleSheet.create({})
