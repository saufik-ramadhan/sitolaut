import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Alert,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, ErrorText } from "../../components";
import Colors from "../../config/Colors";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  Button,
  TextInput,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import {
  depotFetchPage,
  depotUpdate,
  depotFetchOne,
} from "./../../redux/depotReducer";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { portFetchAll } from "../../redux/portReducer";
import * as Yup from "yup";
import { useForm } from "react-hook-form";

export default function UpdateDepot({
  navigation,
  route,
}: OperatorStackProps<"UpdateDepot">) {
  const dispatch = useDispatch();
  const depot = route.params;
  /**
   * Selector
   */
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const port = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [{}];
  });
  const depotLoading = useSelector(function (state: RootState) {
    return state.depot.depotAddLoading;
  });
  const depotFetch = useSelector(function (state: RootState) {
    return state.depot.depotViewSuccess
      ? state.depot.depotViewSuccess.data[0]
      : {};
  });
  
  /**
   * Fetch Data
   */
  React.useEffect(() => {
    dispatch(portFetchAll());
  }, [depotLoading]);

  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          id: String(depot.id),
          user_id: String(id),
          nama_depot: String(depot.nama_depot),
          alamat: String(depot.alamat),
        }}
        onSubmit={(values) => {
          Alert.alert("Update", "Anda yakin akan memperbarui depot ini ?", [
            {text:"Ya", onPress:() => {
              dispatch(depotUpdate(values));
              navigation.goBack();
            }},
            {
              text: "Tdk", onPress:() => {null}
            }
          ])
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Port
             */}
            {/* <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={0}
                style={{ flex: 10 }}
                itemStyle={{ fontSize: 8 }}
                mode="dropdown"
                onValueChange={function (itemValue: string) {
                  setFieldValue("port_id", String(itemValue));
                }}
              >
                <Picker.Item label="Pelabuhan" value={0} />
                {port.map((item, key) => {
                  return (
                    <Picker.Item label={item.label} value={item.id} key={key} />
                  );
                })}
              </Picker>
            </View> */}
            {errors.port_id && touched.port_id && (
              <ErrorText>{errors.port_id}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Nama Depot
             */}
            <TextInput
              value={values.nama_depot}
              onChangeText={handleChange("nama_depot")}
              onBlur={handleBlur("nama_depot")}
              label="Nama Depot"
              dense
            />
            {errors.nama_depot && touched.nama_depot && (
              <ErrorText>{errors.nama_depot}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat Depot
             */}
            <TextInput
              value={values.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              label="Alamat Depot"
              dense
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            <Button
              icon={"square-edit-outline"}
              onPress={handleSubmit}
              mode="contained"
              loading={depotLoading}
            >
              Update
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
