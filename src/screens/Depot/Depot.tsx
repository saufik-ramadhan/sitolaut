import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, Icon, EmptyState, FilterButton } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  List,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import { msttrayekFetchPage } from "./../../redux/msttrayekReducer";
import { portFetchPage, portFilterClear } from "../../redux/portReducer";
import {
  depotFetchPage,
  depotFetchAll,
  depotDelete,
  depotFilterClear,
  depotFetchOne,
} from "../../redux/depotReducer";

function FloatingAdd({ navigation }: OperatorStackProps<"Trayek">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateDepot");
      }}
    />
  );
}

export default function Depot({
  navigation,
  route,
}: OperatorStackProps<"Depot">) {
  const dispatch = useDispatch();
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.depot.depotGetSuccess;
  });
  const listDepot = useSelector(function (state: RootState) {
    return state.depot.depotData ? state.depot.depotData : [];
  });
  const refresh = useSelector(function (state: RootState) {
    return state.depot.depotGetLoading;
  });
  const addLoading = useSelector(function(state: RootState) {
    return state.depot.depotAddLoading;
  })
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const filter = useSelector(function (state: RootState) {
    return state.depot.depotFilter;
  });
  const fetchData = (filter) => {
    const params = {
      length: 10,
      start: 0,
      user_operator_id: `${id}`,
      ...filter,
    };

    dispatch(depotFetchPage(params));
  };
  const handleReachEnd = (filter) => {
    const params = {
      length: 10,
      start: currentPage * 10,
      user_operator_id: `${id}`,
      ...filter,
    };
    dispatch(depotFetchPage(params));
  };

  const onRefresh = () => {
    dispatch(depotFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addLoading]);

  return (
    <>
      <FlatList
        data={listDepot}
        onRefresh={onRefresh}
        ListHeaderComponent={<Space height={10} />}
        refreshing={refresh}
        onEndReachedThreshold={0.5}
        onEndReached={() => (hasNext ? handleReachEnd(filter) : null)}
        ListEmptyComponent={EmptyState}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        renderItem={function ({item}) {
          return (
            <List.Item title={item.nama_depot} description={item.pelabuhan + " | " + item.alamat} 
              right={() => (
                <>
                  <IconButton icon="square-edit-outline" onPress={function(){
                    dispatch(depotFetchOne(item.id));
                    navigation.navigate("UpdateDepot", item);
                  }}/>
                  <IconButton icon="delete" onPress={function(){
                    Alert.alert(
                      "Hapus",
                      "Anda yakin akan menghapus depot ini?",
                      [
                        {
                          text: "OK",
                          onPress: () => {
                            dispatch(depotDelete(item.id));
                            dispatch(depotFilterClear());
                          },
                        },
                        {
                          text: "Cancel",
                          onPress: function () {
                            console.log("Cancel");
                          },
                        },
                      ],
                      { cancelable: true }
                    );
                  }}/>
                </>
              )}
              onPress={function () {
                navigation.navigate("ReadDepot", {
                  id: item.id,
                });
              }}
            />
            
          );
        }}
      />
      {usertype == "Regulator" && <FloatingAdd navigation={navigation} route={route} />}
      <FilterButton
        navigation={navigation}
        onPress={function () {
          navigation.navigate("FilterDepot");
        }}
      />
    </>
  );
}


{/* <Card
              elevation={4}
              style={[styles.card, { marginHorizontal: 10 }]}
              onPress={function () {
                navigation.navigate("ReadDepot", {
                  id: item.id,
                });
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  minHeight: 100,
                }}
              >
                <View style={{ flex: 8 }}>
                  <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                    Nama Depot : {item.nama_depot}
                  </Paragraph>
                  <Caption>Pelabuhan : {item.pelabuhan}</Caption>
                  <Caption>Alamat : {item.alamat}</Caption>
                </View>
                <View
                  style={{
                    flex: 1,
                    justifyContent: "space-between",
                    alignItems: "flex-end",
                  }}
                >
                  <Icon
                    name="square-edit-outline"
                    size={20}
                    onPress={function () {
                      navigation.navigate("UpdateDepot", item.item);
                    }}
                  />
                  <Icon
                    name="delete-outline"
                    size={20}
                    color={Colors.danger}
                    onPress={function () {
                      Alert.alert(
                        "Anda Yakin",
                        "Anda yakin akan menghapus trayek ini?",
                        [
                          {
                            text: "OK",
                            onPress: () => {
                              dispatch(depotDelete(item.id));
                              dispatch(depotFilterClear());
                            },
                          },
                          {
                            text: "Cancel",
                            onPress: function () {
                              console.log("Cancel");
                            },
                          },
                        ],
                        { cancelable: false }
                      );
                    }}
                  />
                </View>
              </View>
            </Card> */}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    padding: 10,
    marginBottom: 3,
  },
});
