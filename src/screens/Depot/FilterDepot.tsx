import React from "react";
import { StyleSheet, Text, View, FlatList, TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, ErrorText } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  Button,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import { depotFetchPage } from "./../../redux/depotReducer";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { portFetchAll } from "../../redux/portReducer";
import * as Yup from "yup";

export default function FilterDepot({
  navigation,
  route,
}: OperatorStackProps<"FilterDepot">) {
  const dispatch = useDispatch();
  /**
   * Selector
   */
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const port = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [{}];
  });

  /**
   * Dispatch
   */
  const fetchPelabuhan = () => {
    dispatch(portFetchAll());
  };
  const fetchData = (data) => {
    const fetchParams = {
      port_id: data.port_id,
      nama_depot: data.nama_depot,
      alamat: data.alamat,
      user_operator_id: `${id}`,
    };
    dispatch(depotFetchPage(fetchParams));
    navigation.goBack();
  };

  React.useEffect(() => {
    fetchPelabuhan();
  }, []);

  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          port_id: "",
          nama_depot: "",
          alamat: "",
        }}
        onSubmit={(values) => {
          fetchData(values);
          // alert(values);
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Nama Depot
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.nama_depot}
              onChangeText={handleChange("nama_depot")}
              onBlur={handleBlur("nama_depot")}
              placeholder="Nama Depot"
            />
            {errors.nama_depot && touched.nama_depot && (
              <ErrorText>{errors.nama_depot}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Alamat Depot
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.alamat}
              onChangeText={handleChange("alamat")}
              onBlur={handleBlur("alamat")}
              placeholder="Alamat Depot"
            />
            {errors.alamat && touched.alamat && (
              <ErrorText>{errors.alamat}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Port
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.port_id}
                style={{ flex: 10 }}
                itemStyle={{ fontSize: 8 }}
                mode="dropdown"
                onValueChange={function (itemValue: string) {
                  setFieldValue("port_id", itemValue);
                }}
              >
                <Picker.Item label="Pelabuhan" value={0} />
                {port.map((item, key) => {
                  return (
                    <Picker.Item label={item.label} value={item.id} key={key} />
                  );
                })}
              </Picker>
            </View>
            {errors.port_id && touched.port_id && (
              <ErrorText>{errors.port_id}</ErrorText>
            )}
            <Space height={10} />

            <Button icon={"filter"} onPress={handleSubmit} mode="contained">
              Filter
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
