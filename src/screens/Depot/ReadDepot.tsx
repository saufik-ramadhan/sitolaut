import React from "react";
import { StyleSheet, Text, View, Alert } from "react-native";
import { OperatorStackProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import {
  depotFetchOne,
  depotDelete,
  depotFetchPage,
} from "../../redux/depotReducer";
import { RootState } from "../../redux/rootReducer";
import { Card, Caption, Paragraph, Button, List } from "react-native-paper";
import Colors from "./../../config/Colors";
import { Root } from "../../components";

export default function ReadDepot({
  navigation,
  route,
}: OperatorStackProps<"ReadDepot">) {
  const depotId = route.params.id;
  const dispatch = useDispatch();
  const depot = useSelector(function (state: RootState) {
    return state.depot.depotViewSuccess
      ? state.depot.depotViewSuccess.data[0]
      : {};
  });
  React.useEffect(() => {
    dispatch(depotFetchOne(depotId));
  }, []);

  return (
    <Root>
      <List.Item title={depot.pelabuhan} description={'Pelabuhan'}/>
      <List.Item title={depot.nama_depot} description={'Nama Depot'}/>
      <List.Item title={depot.alamat} description={'Alamat Depot'}/>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  action: {
    flexDirection: "row",
  },
  button: {
    flex: 1,
  },
});
