import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, FilterButton } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  List,
  Dialog,
  Portal,
  TextInput,
} from "react-native-paper";
import { OperatorStackProps, RegulatorStackProps } from "../Navigator";
import { msttrayekFetchPage } from "./../../redux/msttrayekReducer";
import {
  portFetchPage,
  portFetchAll,
  portFilterClear,
  portDelete,
} from "../../redux/portReducer";
import { Formik } from "formik";

export default function Pelabuhan({
  navigation,
  route,
}: OperatorStackProps<"Pelabuhan">) {
  const dispatch = useDispatch();
  const [showAdd, setShowAdd] = React.useState(false);
  const listPort = useSelector(function (state: RootState) {
    return state.port.portData ? state.port.portData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.port.portGetSuccess;
  });
  const refresh = useSelector(function (state: RootState) {
    return state.port.portGetLoading;
  });
  const portAddLoading = useSelector(function(state: RootState) {return state.port.portAddLoading});
  const portDelLoading = useSelector(function(state: RootState) {return state.port.portDeleteLoading});
  const filter = useSelector(function (state: RootState) {
    return state.port.portFilter;
  });
  const fetchData = (filter) => {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(portFetchPage(params));
  };
  const handleReachEnd = (filter) => {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(portFetchPage(params));
  };
  const onRefresh = () => {
    dispatch(portFilterClear());
  };
  React.useEffect(() => {
    fetchData(filter);
  }, [filter, portAddLoading, portDelLoading]);

  return (
    <>
      <FlatList
        data={listPort}
        onEndReached={() => (hasNext ? handleReachEnd() : null)}
        onEndReachedThreshold={0.5}
        onRefresh={onRefresh}
        refreshing={refresh}
        ListEmptyComponent={() => <PlaceholderLoading loading={false} />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        renderItem={function (item) {
          return (
            <List.Item title={item.item.pelabuhan} description={item.item.kode} left={() => <List.Icon icon="anchor" color={Colors.pri}/>}
              onPress={function () {
                navigation.navigate("ReadPelabuhan", {
                  id: item.item.id,
                });
              }}
              right={
                function() {
                  return (
                    <IconButton icon="delete" onPress={() => 
                      Alert.alert("Hapus", "Anda yakin akan menghapus pelabuhan ini?", [
                        {text: "Ya", onPress:function(){
                          dispatch(portDelete(item.item.id));
                        }},
                        {text: "Tdk", onPress: function(){null}}
                      ])   
                    }/>
                  )
                }
              }
            />
          );
        }}
      />
      <FilterButton
        navigation={navigation}
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "PelabuhanFilterModal",
          });
        }}
      />

      <IconButton
        style={{
          position: "absolute",
          backgroundColor: Colors.sec,
          bottom: 10,
          right: 10,
        }}
        size={40}
        color={Colors.black}
        icon="plus"
        onPress={function () {
          // setShowAdd(true);
          navigation.navigate('CreatePelabuhan');
        }}
      />

      <Portal>
        <Dialog visible={showAdd} onDismiss={() => setShowAdd(false)}>
          <Dialog.Content>
            <Formik
              initialValues={{
                kode: "",
                pelabuhan: "",
                koordinat: "",
                user_id: "",
                provinsi_id: "",
                kota_id: "",
              }}
              onSubmit={() => null}
            >
              {({handleSubmit, handleChange, handleBlur, values, setFieldValue}) => (
                <View>
                  <TextInput label="Kode Pelabuhan" onChangeText={handleChange('kode')} onBlur={handleBlur('kode')} dense/>
                  <TextInput label="Nama Pelabuhan" onChangeText={handleChange('pelabuhan')} onBlur={handleBlur('pelabuhan')} dense/>
                  <TextInput label="Koordinat" onChangeText={handleChange('koordinat')} onBlur={handleBlur('koordinat')} dense/>
                </View>
              )}
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    padding: 10,
    marginBottom: 2,
  },
});
