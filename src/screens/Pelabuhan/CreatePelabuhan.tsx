import { Formik } from 'formik'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from 'react-native-paper';
import { Root, TextInput, SelectInput2 } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/rootReducer';
import { provinsiFetchAll } from '../../redux/provinsiReducer';
import { kotaFetchByProvinsi } from '../../redux/kotaReducer';
import { portAdd } from '../../redux/portReducer';
import { RegulatorStackProps } from '../Navigator';
import * as Yup from 'yup';

const CreatePelabuhanSchema = Yup.object({
  kode: Yup.string().required().label('Kode Pelabuhan'),
  pelabuhan:Yup.string().required().label('Nama Pelabuhan'),
  koordinat:Yup.string().required().label('Koordinat'),
  user_id:Yup.string().required().label('User ID'),
  provinsi_id:Yup.string().required().label('Provinsi'),
  kota_id:Yup.string().required().label('Kota')
})

export default function CreatePelabuhan({navigation}: RegulatorStackProps<'CreatePelabuhan'>) {
  const dispatch = useDispatch();
  const provinsi = useSelector((state: RootState) => state.provinsi.provinsiGetSuccess ? state.provinsi.provinsiGetSuccess.data : []);
  const kota = useSelector((state: RootState) => state.kota.kotaGetSuccess ? state.kota.kotaGetSuccess.data : []);
  React.useEffect(() => {
    dispatch(provinsiFetchAll());
  }, [])
  return (
    <Root style={{padding: 20}}>
    <Formik
      validationSchema={CreatePelabuhanSchema}
      initialValues={{
        kode: "",
        pelabuhan: "",
        koordinat: "",
        user_id: 1,
        provinsi_id: 11,
        kota_id: 1101,
      }}
      onSubmit={(values) => {
        // alert(JSON.stringify(values))
        dispatch(portAdd(values));
        navigation.goBack();
      }}
    >
      {
        ({handleSubmit, handleChange, handleBlur, setFieldValue, values, errors}) => (
          <>
            <TextInput
              label="Kode Pelabuhan"
              value={values.kode}
              onChangeText={handleChange("kode")}
              onBlur={handleBlur("kode")}
              error={errors.kode}
            />
            <TextInput
              label="Nama Pelabuhan"
              value={values.pelabuhan}
              onChangeText={handleChange("pelabuhan")}
              onBlur={handleBlur("pelabuhan")}
              error={errors.pelabuhan}
            />
            <TextInput
              label="Koordinat"
              value={values.koordinat}
              onChangeText={handleChange("koordinat")}
              onBlur={handleBlur("koordinat")}
              error={errors.koordinat}
            />
            <SelectInput2 
              mode="dropdown" 
              items={provinsi} 
              label="Provinsi" 
              itemLabel="label" 
              itemValue="id"
              selectedValue={values.provinsi_id}
              error={errors.provinsi_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('provinsi_id', itemValue)
                dispatch(kotaFetchByProvinsi(itemValue));
              }}/>
            <SelectInput2 
              mode="dropdown" 
              items={kota} 
              label="Kota" 
              itemLabel="label" 
              itemValue="kota_id"
              selectedValue={values.kota_id}
              error={errors.provinsi_id}
              onValueChange={(itemValue, itemIndex) => 
                setFieldValue('kota_id', itemValue)
              }/>
            <Button mode="contained" onPress={handleSubmit}>Simpan</Button>
          </>
        )
      }
    </Formik>
    </Root>
  )
}

const styles = StyleSheet.create({})
