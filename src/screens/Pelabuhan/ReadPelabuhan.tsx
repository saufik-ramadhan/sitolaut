import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { portFetchOne } from "../../redux/portReducer";
import { RootState } from "../../redux/rootReducer";
import { Root } from "../../components";
import { Paragraph, Card, Caption, List, Button } from "react-native-paper";

export default function ReadPelabuhan({ navigation, route }) {
  const dispatch = useDispatch();
  const { id } = route.params;
  const fetchData = () => {
    dispatch(portFetchOne(id));
  };
  const {port} = useSelector((state: RootState) => state);
  const loading = useSelector((state: RootState) => state.port.portAddLoading);
  useEffect(() => {
    fetchData();
  }, [loading]);
  if(!port.portViewSuccess.data) return null;
  const pelabuhan = port.portViewSuccess.data[0];
  return (
    <Root>
      <List.Item title={pelabuhan.pelabuhan} description="Nama Pelabuhan"/>
      <List.Item title={pelabuhan.kode} description="Kode Pelabuhan"/>
      <List.Item title={pelabuhan.kota} description="Kota"/>
      <List.Item title={pelabuhan.provinsi} description="Provinsi"/>
      <List.Item title={pelabuhan.koordinat} description="Koordinat"/>
      <Button onPress={function(){navigation.navigate('UpdatePelabuhan', {
        pelabuhan: pelabuhan
      })}}>Perbarui</Button>
    </Root>
  );
}

const styles = StyleSheet.create({});
