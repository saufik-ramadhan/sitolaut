import { Formik } from 'formik'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Button } from 'react-native-paper';
import { Root, SelectInput2, TextInput } from '../../components';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/rootReducer';
import { provinsiFetchAll } from '../../redux/provinsiReducer';
import { kotaFetchByProvinsi } from '../../redux/kotaReducer';
import { portAdd, portUpdate } from '../../redux/portReducer';
import { RegulatorStackProps } from '../Navigator';
import * as Yup from 'yup';

const CreatePelabuhanSchema = Yup.object({
  id: Yup.string().required().label("Id"),
  kode: Yup.string().required().label('Kode Pelabuhan'),
  pelabuhan:Yup.string().required().label('Nama Pelabuhan'),
  koordinat:Yup.string().required().label('Koordinat'),
  user_id:Yup.string().required().label('User ID'),
  provinsi_id:Yup.string().required().label('Provinsi'),
  kota_id:Yup.string().required().label('Kota')
})

export default function UpdatePelabuhan({navigation, route}: RegulatorStackProps<'UpdatePelabuhan'>) {
  const dispatch = useDispatch();
  const {pelabuhan} = route.params;
  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const {provinsi, kota} = useSelector((state: RootState) => state);
  React.useEffect(() => {
    dispatch(provinsiFetchAll());
    dispatch(kotaFetchByProvinsi(pelabuhan.provinsi_id));
  }, [])
  if(!provinsi.provinsiGetSuccess.data && !kota.kotaGetSuccess.data) return null;
  return (
    <Root style={{padding: 20}}>
    <Formik
      validationSchema={CreatePelabuhanSchema}
      initialValues={{
        id: pelabuhan.id,
        kode: pelabuhan.kode,
        pelabuhan: pelabuhan.pelabuhan,
        koordinat: pelabuhan.koordinat,
        user_id: id,
        provinsi_id: pelabuhan.provinsi_id,
        kota_id: pelabuhan.kota_id,
      }}
      onSubmit={(values) => {
        // alert(JSON.stringify(values))
        dispatch(portUpdate(values));
        navigation.goBack();
      }}
    >
      {
        ({handleSubmit, handleChange, handleBlur, setFieldValue, values, errors}) => (
          <>
            <TextInput
              label="Kode Pelabuhan"
              value={values.kode}
              onChangeText={handleChange("kode")}
              onBlur={handleBlur("kode")}
              error={errors.kode}
            />
            <TextInput
              label="Nama Pelabuhan"
              value={values.pelabuhan}
              onChangeText={handleChange("pelabuhan")}
              onBlur={handleBlur("pelabuhan")}
              error={errors.pelabuhan}
            />
            <TextInput
              label="Koordinat"
              value={values.koordinat}
              onChangeText={handleChange("koordinat")}
              onBlur={handleBlur("koordinat")}
              error={errors.koordinat}
            />
            <SelectInput2 
              mode="dropdown" 
              items={provinsi.provinsiGetSuccess.data} 
              label="Provinsi" 
              itemLabel="label" 
              itemValue="id"
              selectedValue={pelabuhan.provinsi_id}
              error={errors.provinsi_id}
              onValueChange={(itemValue, itemIndex) => {
                setFieldValue('provinsi_id', itemValue)
                dispatch(kotaFetchByProvinsi(itemValue));
              }}/>
            <SelectInput2 
              mode="dropdown" 
              items={kota.kotaGetSuccess.data} 
              label="Kota" 
              itemLabel="label" 
              itemValue="kota_id"
              selectedValue={pelabuhan.kota_id}
              error={errors.provinsi_id}
              onValueChange={(itemValue, itemIndex) => 
                setFieldValue('kota_id', itemValue)
              }/>
            <Button mode="contained" onPress={handleSubmit}>Simpan</Button>
          </>
        )
      }
    </Formik>
    </Root>
  )
}

const styles = StyleSheet.create({})
