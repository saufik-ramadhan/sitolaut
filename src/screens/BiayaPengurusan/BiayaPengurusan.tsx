import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, Icon, SelectInputWithSubtitle, EmptyState } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  Button,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import { portFetchPage } from "../../redux/portReducer";
import { biayapengurusanFetchPage,biayapengurusanDelete } from "../../redux/biayapengurusanReducer";
import { routeFetchAll } from "../../redux/routeReducer";
import { formatRupiah } from "../../services/utils";
function FloatingAdd({ navigation }: OperatorStackProps<"Trayek">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("AddBiayaPengurusan");
      }}
    />
  );
}
export default function BiayaPengurusan({
  navigation,
  route,
}: OperatorStackProps<"biayapengurusan"> |any) {
  const dispatch = useDispatch();
  const [refresh, setRefresh] = React.useState(false);
  const [trayek_id, setTrayekId] = React.useState('');
  const biayapengurusan = useSelector(function (state: RootState|any) {
    return state.biayapengurusan.biayapengurusanGetSuccess
      ? state.biayapengurusan.biayapengurusanGetSuccess.data
      : [{}];
  });
  const biayapengurusanDel = useSelector((state: RootState) => state.biayapengurusan.biayapengurusanDeleteLoading);
  const listTrayek = useSelector(function (state: RootState | any) {
    return state.route.routeGetSuccess
      ? state.route.routeGetSuccess.data
      : [{}];
  });
  const getLoading = useSelector(function (state: RootState) {
    return state.biayapengurusan.biayapengurusanGetLoading;
  });
  const { id } = useSelector(function (state: RootState|any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const fetchData = () => {
    const fetchParams = {
      shipper_id: `${id}`,
    };
    dispatch(biayapengurusanFetchPage(fetchParams));
    dispatch(routeFetchAll());
    setRefresh(false);
  };

  const onRefresh = () => {
    setRefresh(true);
    fetchData();
  };


 const handleFilter = () =>{
   const data = {
     trayek_id: trayek_id.id,
     shipper_id: id,
     length: 10,
     start: 0,
   };
   console.log(trayek_id.id);
    dispatch(biayapengurusanFetchPage(data))
  }

  React.useEffect(() => {
    fetchData();
  }, [biayapengurusanDel]);

  return (
    <>
      <PlaceholderLoading loading={getLoading} />
      <View style={styles.searchContainer}>
        <SelectInputWithSubtitle
          options={listTrayek}
          withSearch={true}
          objectKey="kode"
          subtitle1Key="pel_asal"
          subtitle2Key="pel_sampai"
          label="Trayek"
          onChangeValue={(item: any) => setTrayekId(item)}
        />
        <Button
          mode="contained"
          style={styles.btnSearch}
          icon="magnify"
          onPress={handleFilter}
        >
          Cari
        </Button>
      </View>

      {!getLoading && (
        <FlatList
          data={biayapengurusan}
          ListFooterComponent={<Space height={100}/>}
          renderItem={function ({ item }) {
            const {
              kode_trayek,
              mst_bp_biaya,
              pelabuhan_asal,
              pelabuhan_sampai,
              mst_bp_id,
            }: any = item;
            return (
              <Card
                elevation={4}
                style={[styles.card, { marginHorizontal: 10 }]}
                // onPress={function () {
                //   navigation.navigate("UpdateBiayaPengurusan", {
                //     id: mst_bp_id,
                //   });
                // }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    minHeight: 100,
                  }}
                >
                  <View style={{ flex: 8 }}>
                    <Paragraph style={{ fontWeight: "bold", fontSize: 16 }}>
                      Trayek: {kode_trayek}
                    </Paragraph>
                    <Caption>
                      Pelabuhan Asal : <Paragraph>{pelabuhan_asal}</Paragraph>
                    </Caption>
                    <Caption>
                      Pelabuhan Tujuan :{" "}
                      <Paragraph>{pelabuhan_sampai}</Paragraph>
                    </Caption>
                    <Caption>
                      Biaya Pengurusan :{" "}
                      <Paragraph style={styles.paragraph}>Rp {formatRupiah(parseInt(mst_bp_biaya))}</Paragraph>
                    </Caption>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: "space-between",
                      alignItems: "flex-end",
                    }}
                  >
                    <Icon
                      name="square-edit-outline"
                      size={20}
                      onPress={function () {
                        navigation.navigate("UpdateBiayaPengurusan", {
                       item
                        });
                      }}
                    />
                    <Icon
                      name="delete-outline"
                      size={20}
                      color={Colors.danger}
                      onPress={function () {
                        Alert.alert(
                          "Hapus",
                          "Apakah anda yakin akan menghapus biaya pengurusan ini ?",
                          [
                            {
                              text: "Ya",
                              onPress: () => {
                                dispatch(biayapengurusanDelete(mst_bp_id));
                                fetchData();
                              },
                            },
                            {
                              text: "Tdk",
                              onPress: function () {
                                console.log("Cancel");
                              },
                            },
                          ],
                          { cancelable: false }
                        );
                      }}
                    />
                  </View>
                </View>
              </Card>
            );
          }}
          onRefresh={onRefresh}
          refreshing={refresh}
          ListEmptyComponent={EmptyState}
          keyExtractor={function (item, key) {
            return String(key);
          }}
        />
      )}
      <FloatingAdd navigation={navigation} route={route} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  searchContainer: { flexDirection: "row", padding: 5, paddingHorizontal: 10 },
  btnSearch: { justifyContent: "center" },
  paragraph: { color: Colors.danger },
});
