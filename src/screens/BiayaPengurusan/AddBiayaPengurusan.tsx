import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Card, TextInput, Paragraph, Button, Snackbar } from 'react-native-paper'
import {
  biayapengurusanFetchPage,
  biayapengurusanAdd
} from "../../redux/biayapengurusanReducer";
import { routeFetchAll } from "../../redux/routeReducer";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../redux/rootReducer';
import { Space, LoadingComponent, SelectInput, SelectInputWithSubtitle } from '../../components';
import * as yup from 'yup'
import { Formik } from 'formik';


const schema = yup.object().shape({
  biaya_pengurusan: yup.number().min(10000).required("tidak boleh kosong"),
  trayek_id:yup.string().required(),
});
export default function AddBiayaPengurusan(props) {
  const dispatch = useDispatch();
  const { route,navigation } = props
  
   const { id } = useSelector(function (state: RootState | any) {
     return state.auth.authLoginSuccess
       ? state.auth.authLoginSuccess.data.user[0]
       : {
           id: 0,
           usertype: "Not Defined",
         };
   });
   const listRoute = useSelector(function (state: RootState | any) {
     return state.route.routeGetSuccess
       ? state.route.routeGetSuccess.data
       : [{}];
   });
  const AddSuccess = useSelector(function (state: RootState | any) {
      return state.biayapengurusan.biayapengurusanAddSuccess
        ? state.biayapengurusan.biayapengurusanAddSuccess
        : [{}];
  });
    const fetchDataRoute = () => {
      dispatch(routeFetchAll());
  };
   const fetchData = () => {
     const fetchParams = {
       shipper_id: `${id}`,
     };
     dispatch(biayapengurusanFetchPage(fetchParams));
     navigation.goBack();
   };
   const AddLoading = useSelector(function (state: RootState | any) {
     return state.biayapengurusan.biayapengurusanAddLoading;
   });
   const [initData] = useState({
     biaya_pengurusan: '',
     shipper_id: id,
     trayek_id: '',
   })
   useEffect(() => {
     fetchDataRoute();
   }, []);
  
  useEffect(() => {
    if (AddSuccess.success) fetchData();
  }, [AddSuccess.success]);

  return (
    <View style={styles.container}>
      <Card>
        <Card.Content>
          <Formik
            initialValues={initData}
            enableReinitialize={true}
            onSubmit={(values) => {
              let initBP = Number(values.biaya_pengurusan);
              let form = {
                biaya_pengurusan: initBP,
                shipper_id: values.shipper_id,
                trayek_id: values.trayek_id,
              };
              console.log(form)
              dispatch(biayapengurusanAdd(form));
            }}
            validationSchema={schema}
          >
            {(formikProps) => {
              useEffect(() => {
                formikProps.validateForm()
              },[])
              return (
                <>
                  <Paragraph>Kode Trayek</Paragraph>
                  <View style={styles.searchContainer}>
                  <SelectInputWithSubtitle
                    options={listRoute}
                    withSearch={true}
                    objectKey="kode"
                    subtitle1Key="pel_asal"
                    subtitle2Key="pel_sampai"
                    label="Trayek"
                    onChangeValue={(item: any) => formikProps.setFieldValue('trayek_id',item.id)}
                  />

                  </View>
                  <Space height={10} />
                  <Paragraph>Biaya Pengurusan</Paragraph>
                  <TextInput
                    keyboardType="number-pad"
                    onChangeText={formikProps.handleChange("biaya_pengurusan")}
                    dense
                    placeholder='Masukan biaya pengurusan'
                  />
                  <Space height={10} />
                  <Button
                    disabled={formikProps.isValid ? false : true}
                    onPress={formikProps.handleSubmit}
                    mode="contained"
                    icon="send"
                  >
                    Simpan
                  </Button>
                </>
              );
            }}
          </Formik>
        </Card.Content>
      </Card>
      <LoadingComponent visible={AddLoading} />
    
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  searchContainer: { flexDirection: "row", padding: 5, paddingHorizontal: 10 },
});