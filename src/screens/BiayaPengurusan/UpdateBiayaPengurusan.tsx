import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Card, TextInput, Paragraph, Button, Snackbar } from 'react-native-paper'
import {
  biayapengurusanFetchPage,
  biayapengurusanDelete,
  biayapengurusanUpdate,
  biayapengurusanFetchAll,
} from "../../redux/biayapengurusanReducer";
import { routeFetchAll,routeFetchOne } from "../../redux/routeReducer";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../redux/rootReducer';
import { Space, LoadingComponent } from '../../components';
import { formatRupiah } from '../../services/utils';
import * as yup from 'yup'
import { Formik } from 'formik';
import { CommonActions } from '@react-navigation/native';
import Colors from '../../config/Colors';


const schema = yup.object().shape({
  biaya_pengurusan: yup.number().min(10000).required("tidak boleh kosong"),
});
export default function UpdateBiayaPengurusan(props) {
  const dispatch = useDispatch();
  const { route,navigation } = props
  const {
    kode_trayek,
    mst_bp_biaya,
    pelabuhan_asal,
    pelabuhan_sampai,
    mst_bp_id,
    trayek_id,
  } = route.params.item;
   const { id } = useSelector(function (state: RootState | any) {
     return state.auth.authLoginSuccess
       ? state.auth.authLoginSuccess.data.user[0]
       : {
           id: 0,
           usertype: "Not Defined",
         };
   });
  const updateSuccess = useSelector(function (state: RootState | any) {
      return state.biayapengurusan.biayapengurusanAddSuccess
        ? state.biayapengurusan.biayapengurusanAddSuccess
        : [{}];
  });
    const fetchData = () => {
      const fetchParams = {
        shipper_id: `${id}`,
      };
      dispatch(biayapengurusanFetchPage(fetchParams));
      navigation.goBack();
    };
   const updateLoading = useSelector(function (state: RootState | any) {
     return state.biayapengurusan.biayapengurusanAddLoading;
   });
   const [initData, setInitData] = useState({
     biaya_pengurusan: mst_bp_biaya,
     id: mst_bp_id,
     shipper_id: id,
     trayek_id: trayek_id,
    })
  useEffect(() => {
    if (updateSuccess.success) fetchData();
  }, [updateSuccess.success]);
  return (
    <View style={styles.container}>
      <Card>
        <Card.Content>
          <Formik
            initialValues={initData}
            enableReinitialize={true}
            onSubmit={(values) => {
              let initBP = Number(values.biaya_pengurusan);
              let form = {
                biaya_pengurusan: initBP,
                id: values.id,
                shipper_id: values.shipper_id,
                trayek_id: values.trayek_id,
              };
              dispatch(biayapengurusanUpdate(form));
            }}
            validationSchema={schema}
          >
            {(formikProps) => {
              return (
                <>
                  <Paragraph>Kode Trayek</Paragraph>
                  <TextInput
                    dense
                    defaultValue={`${kode_trayek} (${pelabuhan_asal} - ${pelabuhan_sampai})`}
                    disabled
                  />
                  <Space height={10} />
                  <Paragraph>Biaya Pengurusan</Paragraph>
                  <TextInput
                    keyboardType="number-pad"
                    onChangeText={formikProps.handleChange("biaya_pengurusan")}
                    dense
                    defaultValue={mst_bp_biaya}
                  />
                  <Space height={10} />
                  <Button
                    disabled={formikProps.isValid ? false : true}
                    onPress={formikProps.handleSubmit}
                    mode="contained"
                    icon="send"
                  >
                    Simpan
                  </Button>
                </>
              );
            }}
          </Formik>
        </Card.Content>
      </Card>
      <LoadingComponent visible={updateLoading} />
    
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding:10
  },
  
})