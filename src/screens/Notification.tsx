import React from 'react';
import {Root} from '../components';
import {Paragraph, List} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import Colors from './../config/Colors';

export default function Notification() {
  return (
    <Root style={styles.container}>
      <List.Section title="Latest Update">
        <List.Accordion
          title="Pesanan Diterima"
          description="No: 1234"
          left={(props) => <List.Icon {...props} icon="check" />}>
          <List.Item title="First item" />
          <List.Item title="Second item" />
        </List.Accordion>
        <List.Accordion
          title="Pesanan Ditolak"
          description="No: 1235"
          left={(props) => <List.Icon {...props} icon="close" />}>
          <List.Item title="First item" />
          <List.Item title="Second item" />
        </List.Accordion>
        <List.Accordion
          title="Pesanan Selesai"
          description="No: 1236"
          left={(props) => <List.Icon {...props} icon="check-all" />}>
          <List.Item title="First item" />
          <List.Item title="Second item" />
        </List.Accordion>
      </List.Section>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
  },
});
