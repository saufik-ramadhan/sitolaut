import React from 'react';
import {Root} from '../components';
import {Paragraph} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import {Navigation} from 'react-native-navigation';
import {appRoot, authRoot} from '../roots';
import AsyncStorage from '@react-native-community/async-storage';

export default function Splash() {
  /**
   * Does token validation steps
   * @param token
   * @returns boolean
   */
  function isValid(token: string) {
    // Does token validation steps
    if (token) return true;
    else return false;
  }

  /**
   * Navigate to Authentication ROOT
   * @return void
   */
  function goToAuthentication() {
    Navigation.setRoot(authRoot);
  }

  /**
   * Navigate to Application ROOT
   * @return void
   */
  function goToApp() {
    Navigation.setRoot(appRoot);
  }

  /**
   * Clear All AsyncStorage in All Apps
   * @return void
   */
  async function clearAll() {
    try {
      await AsyncStorage.clear();
    } catch (e) {
      // clear error
    }
    console.log('Async Storage Cleared.');
  }

  /**
   * Check if token is available or not
   * if available then goToApp() executed
   * else goToAuthentication() execured
   * @return void
   */
  async function checkToken() {
    try {
      const token = await AsyncStorage.getItem('jwt');
      // console.log('token:', token);
      if (isValid(token)) {
        goToApp();
      } else {
        goToAuthentication();
      }
    } catch (err) {
      console.log('error: ', err);
      goToAuthentication();
    }
  }

  React.useEffect(() => {
    checkToken();
  }, []);

  return (
    <Root style={styles.container}>
      <Paragraph>Loading ...</Paragraph>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
