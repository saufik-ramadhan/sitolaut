import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { Card, Paragraph, Caption, List, Button } from "react-native-paper";
import Colors from "../../config/Colors";
import { useDispatch, useSelector } from "react-redux";
import {
  scheduleFetchWill,
  scheduleFilterClear,
} from "../../redux/scheduleReducer";
import { RootState } from "../../redux/rootReducer";
import { ConsigneeStackProps } from "../Navigator";
import { Space, Icon, FilterButton, EmptyState } from "../../components";
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
} from "rn-placeholder";
import {
  DateFormat,
  TimeFormat,
  formatRupiah,
  DateTimeFormat,
} from "../../services/utils";

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

export default function Jadwal({
  navigation,
  route,
}: ConsigneeStackProps<"Jadwal">) {
  const dispatch = useDispatch();
  const [jadwal, setJadwal] = React.useState([{ containers: [] }]);
  const refresh = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetLoading;
  });
  const jadwalList = useSelector(function (state: RootState) {
    return state.schedule.scheduleData ? state.schedule.scheduleData : [];
  });
  const filter = useSelector(function (state: RootState) {
    return state.schedule.scheduleFilter ? state.schedule.scheduleFilter : {};
  });
  const { currentPage, hasNext } = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetSuccess
      ? state.schedule.scheduleGetSuccess
      : false;
  });

  function fetchData(filter) {
    dispatch(
      scheduleFetchWill({
        length: 10,
        ...filter,
      })
    );
  }

  function handleReachEnd(filter) {
    dispatch(
      scheduleFetchWill({
        length: 10,
        start: currentPage * 10,
        ...filter,
      })
    );
    return jadwalList;
  }

  const onRefresh = () => {
    dispatch(scheduleFilterClear());
  };

  React.useEffect(() => {
    filter === {} ? null : fetchData(filter);
  }, [filter]);

  return (
    <>
      <FlatList
        data={jadwalList}
        onRefresh={onRefresh}
        ListEmptyComponent={EmptyState}
        refreshing={refresh}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        ListFooterComponent={
          <View>
            {hasNext ? (
              <ActivityIndicator size="large" color={Colors.pri} />
            ) : null}
          </View>
        }
        renderItem={function (item) {
          return (
            <List.Accordion
              titleNumberOfLines={2}
              style={{ elevation: 3, backgroundColor: "white" }}
              title={
                <Paragraph style={{ fontWeight: "bold" }}>
                  Operator : {item.item.operator_kapal_name}
                  {"\n"}
                  Trayek: {item.item.kode_trayek} / Voyage {item.item.voyage}
                </Paragraph>
              }
            >
              <List.Item
                style={{
                  borderLeftWidth: 5,
                  borderColor: Colors.def,
                }}
                onPress={function () {
                  null;
                }}
                titleNumberOfLines={10}
                title={
                  <Caption style={{ color: "black" }}>
                    {item.item.pel_asal} ➤ {item.item.pel_sampai}
                    {"\n"}
                    {DateTimeFormat(item.item.tanggal_berangkat) +
                      " ➤ " +
                      DateTimeFormat(item.item.tanggal_tiba)}
                    {"\n"}
                    BUMN :{DateTimeFormat(item.item.tutup_jadwal_shipper_bumn)}
                    {"\n"}
                    SWASTA :
                    {DateTimeFormat(item.item.tutup_jadwal_shipper_swasta)}
                    {item.item.containers.map(function (container, key) {
                      return (
                        <Caption key={key}>
                          {"\n"}
                          {container.type_container_name} Rp.{" "}
                          {formatRupiah(parseInt(container.price))}(
                          {container.sisa_quota}/{container.alokasi_quota})
                        </Caption>
                      );
                    })}
                  </Caption>
                }
              />
            </List.Accordion>
          );
        }}
        keyExtractor={function (item, key) {
          return String(key);
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "JadwalFilterModal",
          });
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({});
