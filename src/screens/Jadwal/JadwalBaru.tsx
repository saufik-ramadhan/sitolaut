import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  Alert,
} from "react-native";
import {
  Card,
  Paragraph,
  Caption,
  Button,
  IconButton,
  Dialog,
  Portal,
  TextInput,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { useDispatch, useSelector } from "react-redux";
import {
  scheduleFetchWill,
  scheduleDelete,
  scheduleFilterClear,
  scheduleFilterAdd,
} from "../../redux/scheduleReducer";
import { RootState } from "../../redux/rootReducer";
import { MonthsID } from "../../config/Months";
import { DateTime, FilterButton, Icon, Space } from "../../components";
import { OperatorStackProps } from "../Navigator";
import { DateFormat, DateFormatStrip, TimeFormat } from "../../services/utils";
import {Formik} from 'formik';

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

export default function JadwalBaru({ navigation }) {
  const dispatch = useDispatch();
  const [showFilter, setShowFilter] = React.useState(false);
  const { id } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : 0;
  });
  const loading = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetLoading;
  });
  const { currentPage, hasNext } = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetSuccess
      ? state.schedule.scheduleGetSuccess.data
      : [];
  });
  const jadwalBaru = useSelector(function (state: RootState) {
    return state.schedule.scheduleData || [];
  });
  const addLoading = useSelector(function (state: RootState) {
    return state.schedule.scheduleAddLoading;
  });
  const delLoading = useSelector(function (state: RootState) {
    return state.schedule.scheduleDeleteLoading;
  });
  const filter = useSelector(function (state: RootState) {
    return state.schedule.scheduleFilter || {};
  });
  const fetchJadwalBaru = (filter) => {
    const params = {
      length: 10,
      start: 0,
      operator_id: `${id}`,
      ...filter,
    };
    dispatch(scheduleFetchWill(params));
  };
  const handleReachEnd = (filter) => {
    const params = {
      length: 10,
      start: currentPage * 10,
      operator_id: `${id}`,
      ...filter,
    };
    dispatch(scheduleFetchWill(params));
  };

  function onRefresh() {
    dispatch(scheduleFilterClear());
  }
  useEffect(() => {
    fetchJadwalBaru(filter);
  }, [filter, addLoading, delLoading]);
  return (
    <View style={{flex: 1}}>
      {loading ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <ActivityIndicator size="large" color={Colors.pri}/>
        </View>
      ) : (
        <FlatList
          data={jadwalBaru}
          keyExtractor={(item, key) => String(key)}
          onRefresh={onRefresh}
          refreshing={loading}
          onEndReachedThreshold={0.5}
          onEndReached={function () {
            hasNext ? handleReachEnd(filter) : null;
          }}
          ListFooterComponent={<Space height={100}/>}
          ListEmptyComponent={
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Caption>Tidak ada jadwal baru</Caption>
            </View>
          }
          renderItem={({ item }) => {
            const depart = new Date(item.tanggal_berangkat);
            const arrive = new Date(item.tanggal_tiba);
            const closebumn = new Date(item.tutup_jadwal_shipper_bumn);
            const closeswasta = new Date(item.tutup_jadwal_shipper_swasta);
            const m = MonthsID;
            return (
              <Card style={styles.card}>
                <View style={{ flexDirection: "row" }}>
                  {/** Rute */}
                  <View style={{ flexDirection: "row", flex: 2 }}>
                    <View style={{ flex: 1 }}>
                      <B>
                        {item.pel_asal}
                        {"\n"}
                        <Caption>
                          {`${DateFormat(depart)}`}
                          {"\n"}
                          {`(${TimeFormat(depart)})`}{" "}
                        </Caption>
                      </B>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
                    </View>
                    <View style={{ flex: 1, alignItems: "flex-end" }}>
                      <B>
                        {item.pel_sampai}
                        {"\n"}
                        <Caption>
                          {`${DateFormat(arrive)}`}
                          {"\n"}
                          {`(${TimeFormat(arrive)})`}{" "}
                        </Caption>
                      </B>
                    </View>
                  </View>

                  {/** Trayek & Voyage */}
                  <View
                    style={{
                      flex: 1,
                      alignItems: "flex-end",
                      justifyContent: "center",
                    }}
                  >
                    <Paragraph>
                      <B>Trayek</B> {item.kode_trayek} {"\n"}
                      <B>Voyage</B> {item.voyage}
                    </Paragraph>
                  </View>
                </View>

                {/** Kapal */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"ferry"}
                    style={{ fontSize: 20, color: Colors.pending }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    {item.nama_kapal}
                  </Paragraph>
                </View>

                {/** Closing BUMN */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"calendar-remove"}
                    style={{ fontSize: 20, color: Colors.def }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    Closing BUMN :{" "}
                    {`${closebumn.getDate()} ${
                      m[closebumn.getMonth()]
                    } ${closebumn.getFullYear()}`}
                  </Paragraph>
                </View>

                {/** Closing SWASTA */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"calendar-remove"}
                    style={{ fontSize: 20, color: Colors.sec }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    Closing SWASTA :{" "}
                    {`${closeswasta.getDate()} ${
                      m[closeswasta.getMonth()]
                    } ${closeswasta.getFullYear()}`}
                  </Paragraph>
                </View>

                {/** Open Detail Modal */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Button
                    style={{ flex: 6 }}
                    color={Colors.gray1}
                    mode="contained"
                    onPress={function () {
                      navigation.navigate("Modals", {
                        screen: "JadwalOperatorModal",
                        params: {
                          id: item.id,
                        },
                      });
                    }}
                  >
                    Detail
                  </Button>
                  <IconButton
                    icon="delete"
                    color={Colors.danger}
                    onPress={function () {
                      Alert.alert(
                        "Anda Yakin",
                        "Anda yakin akan menghapus jadwal ini?",
                        [
                          {
                            text: "OK",
                            onPress: () => {
                              // alert("delete");
                              dispatch(
                                scheduleDelete({
                                  id: item.id,
                                  user_id: String(id),
                                })
                              );
                            },
                          },
                          {
                            text: "Cancel",
                            onPress: function () {
                              console.log("Cancel");
                            },
                          },
                        ],
                        { cancelable: false }
                      );
                    }}
                  />
                </View>
              </Card>
            );
          }}
        />
      )}
      
      <FilterButton
        onPress={function(){setShowFilter(true)}}
      />

      <Portal>
        <Dialog visible={showFilter} onDismiss={function(){setShowFilter(false)}}>
          <Dialog.Content>
            <Formik
              initialValues={{
                eta: filter.eta || "",
                etd: filter.etd || "",
                kode_trayek: "",
              }}
              onSubmit={(val) => {
                dispatch(scheduleFilterAdd(val))
                setShowFilter(false);
              }}
            >
              {
                ({handleSubmit, handleChange, handleBlur, setFieldValue, values}) => (
                  <View>
                    <TextInput
                      dense
                      label="Kode Trayek"
                      onChangeText={handleChange('kode_trayek')}
                      onBlur={handleBlur('kode_trayek')}
                      value={values.kode_trayek}
                    />
                    <Space height={10} />
                    
                    {/** ETD */}
                    <DateTime
                      label="ETD"
                      onChangeValue={(date: string) =>
                        setFieldValue("etd", date.date)
                      }
                      type="date"
                    />
                    <Space height={10} />

                    {/** ETA */}
                    <DateTime
                      label="Arrival"
                      onChangeValue={(date: string) =>
                        setFieldValue("eta", date.date)
                      }
                      type="date"
                    />
                    <Space height={10} />

                    <Button onPress={handleSubmit}>Filter</Button>
                  </View>
                )
              }
            </Formik>
          </Dialog.Content>
        </Dialog>
      </Portal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  modalContainer: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 3,
    elevation: 3,
    margin: 10,
  },
  bold: {
    fontWeight: "bold",
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  ctrlButton: {
    flex: 1,
    borderRadius: 3,
  },
});
