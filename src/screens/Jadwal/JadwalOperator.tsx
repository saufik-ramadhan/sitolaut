import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { JadwalBaru, JadwalSudahBerjalan } from "../../screens/Screen";
import Colors from "./../../config/Colors";
import { IconButton } from "react-native-paper";
import { OperatorStackProps } from "../Navigator";

const Tab = createMaterialTopTabNavigator();

function FloatingAdd({
  navigation,
  route,
}: OperatorStackProps<"JadwalOperator">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateJadwalOperator");
      }}
    />
  );
}

export default function JadwalOperator(
  props: OperatorStackProps<"JadwalOperator">
) {
  return (
    <>
      <Tab.Navigator>
        <Tab.Screen name="Baru" component={JadwalBaru} />
        <Tab.Screen name="Berjalan" component={JadwalSudahBerjalan} />
      </Tab.Navigator>
      <FloatingAdd {...props} />
    </>
  );
}

const styles = StyleSheet.create({});
