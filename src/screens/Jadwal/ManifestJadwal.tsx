import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { Card, Paragraph, Caption, Button } from "react-native-paper";
import Colors from "./../../config/Colors";
import { useDispatch, useSelector } from "react-redux";
import {
  manifestFetchAll,
  manifestFetchOne,
} from "../../redux/manifestReducer";
import { RootState } from "../../redux/rootReducer";
import { MonthsID } from "../../config/Months";
import { Icon } from "../../components";
import { OperatorStackProps } from "../Navigator";

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

export default function ManifestJadwal({ navigation }) {
  const dispatch = useDispatch();
  const { id } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : 0;
  });
  const loading = useSelector(function (state: RootState) {
    return state.manifest.manifestGetLoading;
  });
  const jadwalBaru = useSelector(function (state: RootState) {
    return state.manifest.manifestGetSuccess
      ? state.manifest.manifestGetSuccess.data
      : [{}];
  });
  const fetchManifestJadwal = (data) => {
    const params = {
      length: data.length,
      start: data.start,
      user_id: `${id}`,
    };
    dispatch(manifestFetchAll(params));
  };
  useEffect(() => {
    fetchManifestJadwal({ length: 10, start: 0 });
  }, []);
  return (
    <>
      {loading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={jadwalBaru}
          keyExtractor={(item, key) => String(key)}
          ListEmptyComponent={<Paragraph>Tidak ada jadwal baruy</Paragraph>}
          renderItem={(item) => {
            const depart = new Date(item.item.tanggal_berangkat);
            const arrive = new Date(item.item.tanggal_tiba);
            const closebumn = new Date(item.item.tutup_jadwal_shipper_bumn);
            const closeswasta = new Date(item.item.tutup_jadwal_shipper_swasta);
            const m = MonthsID;
            return (
              <Card style={styles.card}>
                <View style={{ flexDirection: "row" }}>
                  {/** Rute */}
                  <View style={{ flexDirection: "row", flex: 2 }}>
                    <View style={{ flex: 1 }}>
                      <B>
                        {item.item.dari_pelabuhan}
                        {"\n"}
                        <Caption>
                          {`${depart.getDate()} ${m[depart.getMonth()]}`}
                          {"\n"}
                          {`(${depart.getHours()} : ${depart.getMinutes()})`}{" "}
                        </Caption>
                      </B>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
                    </View>
                    <View style={{ flex: 1, alignItems: "flex-end" }}>
                      <B>
                        {item.item.ke_pelabuhan}
                        {"\n"}
                        <Caption>
                          {`${arrive.getDate()} ${m[arrive.getMonth()]}`}
                          {"\n"}
                          {`(${arrive.getHours()} : ${arrive.getMinutes()})`}{" "}
                        </Caption>
                      </B>
                    </View>
                  </View>

                  {/** Trayek & Voyage */}
                  <View
                    style={{
                      flex: 1,
                      alignItems: "flex-end",
                      justifyContent: "center",
                    }}
                  >
                    <Paragraph>
                      <B>Trayek</B> {item.item.kode_trayek} {"\n"}
                      <B>Voyage</B> {item.item.voyage}
                    </Paragraph>
                  </View>
                </View>

                {/** Kapal */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"ferry"}
                    style={{ fontSize: 20, color: Colors.pending }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    {item.item.nama_kapal}
                  </Paragraph>
                </View>

                {/** Closing BUMN */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"calendar-remove"}
                    style={{ fontSize: 20, color: Colors.def }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    Closing BUMN :{" "}
                    {`${closebumn.getDate()} ${
                      m[closebumn.getMonth()]
                    } ${closebumn.getFullYear()}`}
                  </Paragraph>
                </View>

                {/** Closing SWASTA */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"calendar-remove"}
                    style={{ fontSize: 20, color: Colors.sec }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    Closing SWASTA :{" "}
                    {`${closeswasta.getDate()} ${
                      m[closeswasta.getMonth()]
                    } ${closeswasta.getFullYear()}`}
                  </Paragraph>
                </View>

                {/** Print Manifest */}
                <Button
                  icon="file-pdf"
                  color={Colors.danger}
                  mode="contained"
                  onPress={function () {
                    dispatch(manifestFetchOne(item.item.id));
                  }}
                >
                  Manifest
                </Button>
              </Card>
            );
          }}
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  modalContainer: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 3,
    elevation: 3,
    margin: 10,
  },
  bold: {
    fontWeight: "bold",
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  ctrlButton: {
    flex: 1,
    borderRadius: 3,
  },
});
