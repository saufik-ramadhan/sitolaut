import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root, SelectInput, DateTime, Space, Icon, ErrorText } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { vesselFetchAll } from "../../redux/vesselReducer";
import { depotFetchSub } from "../../redux/depotReducer";
import { typecontainerFetchAll } from "../../redux/typecontainerReducer";
import moment from "moment";
import {
  Button,
  Card,
  Caption,
  ActivityIndicator,
  TextInput,
  Paragraph,
} from "react-native-paper";
import { routeFetchClear, routeFetchSub } from "../../redux/routeReducer";
import { scheduleAdd } from "../../redux/scheduleReducer";
import { Formik } from "formik";
import Colors from "./../../config/Colors";
import { DateTimeFormat, DateFormat3, onlyNumber } from "../../services/utils";
import { OperatorStackProps } from "../Navigator";
import * as Yup from 'yup';

const QuotaValidationSchema = Yup.object().shape({
  alokasi_quota: Yup.string().required(),
  container_type_id: Yup.number().required(),
  harga: Yup.string().required(),
});

const RuteValidationSchema = Yup.object().shape({
  depot_id: Yup.number().required().label('Depot'),
  ready_to_book: Yup.boolean().required(),
  tanggal_berangkat: Yup.string().required(),
  tanggal_tiba: Yup.string().required(),
  trayek_id: Yup.number().required(),
  tutup_jadwal_bumn: Yup.string().required(),
  tutup_jadwal_swasta: Yup.string().required(),
  users_id: Yup.string().required(),
  container: Yup.array().of(QuotaValidationSchema).required(),
});


export default function CreateJadwalOperator({
  navigation,
}: OperatorStackProps<"CreateJadwalOperator">) {
  const dispatch = useDispatch();
  const { id } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : 0;
  });
  let jadwal = [];

  /**
   * Selector Data
   */
  const msttrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetSuccess
      ? state.msttrayek.msttrayekGetSuccess.data
      : false;
  });
  const vessel = useSelector(function (state: RootState) {
    return state.vessel.vesselGetSuccess
      ? state.vessel.vesselGetSuccess.data
      : false;
  });
  const depots = useSelector(function (state: RootState) {
    return state.depot.depotGetSuccess ? state.depot.depotGetSuccess.data : false;
  });
  const typecontainer = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerGetSuccess
      ? state.typecontainer.typecontainerGetSuccess.data
      : [];
  });
  const route = useSelector(function (state: RootState) {
    return state.route.routeGetSuccess ? state.route.routeGetSuccess.data : [];
  });
  const routeLoading = useSelector(function (state: RootState) {
    return state.route.routeGetLoading;
  });

  /**
   * Fetch Data
   */
  const fetchMsttrayek = () =>
    dispatch(msttrayekFetchAll({ operator_id: `${id}` }));
  const fetchVessel = () => dispatch(vesselFetchAll({ operator_id: `${id}` }));
  const fetchDepots = () =>
    dispatch(depotFetchSub({ user_operator_id: `${id}` }));
  const fetchTypecontainer = () => dispatch(typecontainerFetchAll());
  const fetchTrayek = (trayek_id) => {
    dispatch(routeFetchSub(`/get_by_master/${trayek_id}`));
    // alert(trayek_id);
  };

  const addDataJadwal = (data) => {
    const params = {
      container: [
        {
          alokasi_quota: "10",
          container_type_id: 1,
          harga: "1200000.00",
        },
        {
          alokasi_quota: "10",
          container_type_id: 2,
          harga: "1560000.00",
        },
      ],
      depot_id: data.depot,
      ready_to_book: true,
      tanggal_berangkat: "2020-07-21 08:57",
      tanggal_tiba: "2020-07-21 08:57",
      trayek_id: data.trayek,
      tutup_jadwal_bumn: "2020-07-21 08:57",
      tutup_jadwal_swasta: "2020-07-21 08:57",
      users_id: `${id}`,
    };
    jadwal.push(params);
  };

  /**
   * Post Data
   */
  const createJadwalOperator = (data) => {
    const params = {
      data: [
        {
          container: [
            {
              alokasi_quota: String(data.alokasi_quota1),
              container_type_id: 1,
              harga: "1200000.00",
            },
            {
              alokasi_quota: String(data.alokasi_quota2),
              container_type_id: 2,
              harga: "1560000.00",
            },
          ],
          depot_id: data.depot,
          ready_to_book: true,
          tanggal_berangkat: data.depart,
          tanggal_tiba: data.arrive,
          trayek_id: data.trayek,
          tutup_jadwal_bumn: data.closeBumn,
          tutup_jadwal_swasta: data.closeSwasta,
          users_id: `${id}`,
        },
      ],
      kapal_id: data.kapal,
      kode_id: data.msttrayek,
      operator_id: `${id}`,
    };
  };

  useEffect(() => {
    fetchMsttrayek();
    fetchVessel();
    fetchDepots();
    fetchTypecontainer();
    dispatch(routeFetchClear());
  }, []);

  /**
   *
   *  {
        depot_id: 176,
        ready_to_book: true,
        tanggal_berangkat: "2020-08-17 12:35",
        tanggal_tiba: "2020-08-18 12:35",
        trayek_id: 698,
        tutup_jadwal_bumn: "2020-08-16 12:35",
        tutup_jadwal_swasta: "2020-08-16 12:35",
        users_id: "6",
      },
   */
  if(!msttrayek && !vessel && !depots) return null;
  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          data: [],
          kapal_id: "",
          kode_id: "",
          operator_id: String(id),
        }}
        onSubmit={(values) => {
          // alert(JSON.stringify(values));
          // console.log(values);
          dispatch(scheduleAdd(values));
          navigation.goBack();
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          registerField,
          values,
          errors,
          touched,
        }) => (
          <View style={{flex: 1}}>
            <SelectInput
              label="Trayek"
              onChangeValue={(val) => {
                fetchTrayek(val.id);
                setFieldValue("kode_id", val.id);
              }}
              options={msttrayek}
              objectKey="label"
            />
            <SelectInput
              label="Kapal"
              onChangeValue={(val) => setFieldValue("kapal_id", val.id)}
              options={vessel}
              objectKey="label"
            />
            {!routeLoading ? (
              route.map((item, key) => (
                <View key={key}>
                  <Formik
                    validationSchema={RuteValidationSchema}
                    initialValues={{
                      depot_id: "",
                      ready_to_book: true,
                      tanggal_berangkat: "",
                      tanggal_tiba: "",
                      trayek_id: item.id,
                      tutup_jadwal_bumn: "",
                      tutup_jadwal_swasta: "",
                      users_id: String(id),
                      container: [],
                    }}
                    onSubmit={function (val) {
                      // alert(JSON.stringify(val));
                      console.log(val);
                      setFieldValue("data", [
                        ...values.data.filter(
                          (item) => item.trayek_id !== val.trayek_id
                        ),
                        val,
                      ]);
                    }}
                  >
                    {(rprops) => (
                      <Card style={styles.card}>
                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center",
                            justifyContent: "center",
                            borderRadius: 3,
                            backgroundColor: Colors.grayL,
                          }}
                        >
                          <Icon name="anchor" style={{ fontSize: 20 }} />
                          <Caption>
                            {" "}
                            {item.pel_asal}
                            {" - "}
                            {item.pel_sampai}
                          </Caption>
                        </View>

                        {/** Depot */}
                        <Space height={5}/>
                        <SelectInput
                          label="Depot"
                          onChangeValue={(val) =>
                            rprops.setFieldValue("depot_id", val.id)
                          }
                          options={depots.filter(
                            (v) => v.port_id === item.port_origin_id
                          )}
                          objectKey="nama_depot"
                        />
                        <ErrorText>{rprops.errors.depot_id}</ErrorText>

                        {item.rates.map((quota, qkey) => (
                          <Formik
                            key={qkey}
                            validationSchema={QuotaValidationSchema}
                            initialValues={{
                              alokasi_quota: "0",
                              container_type_id: quota.container_type_id,
                              harga: quota.price,
                            }}
                            onSubmit={function (val) {
                              if(parseInt(val.alokasi_quota) > 0) {
                                rprops.setFieldValue("container", [
                                  ...rprops.values.container.filter(
                                    (item) =>
                                      item.container_type_id !==
                                      val.container_type_id
                                  ),
                                  val,
                                ]);
                              } else alert("Alokasi kuota harus lebih dari 0 (nol)");
                            }}
                          >
                            {(qprops) => (
                              <View
                                key={qkey}
                                style={{
                                  flexDirection: "row",
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                {/** Quota Container */}
                                <TextInput
                                  dense
                                  style={{ marginBottom: 5, flex: 8 }}
                                  maxFontSizeMultiplier={10}
                                  key={qkey}
                                  mode="outlined"
                                  label={`Quota ${quota.type_container_name}`}
                                  keyboardType="number-pad"
                                  value={qprops.values.alokasi_quota}
                                  onChangeText={function(val) {qprops.setFieldValue('alokasi_quota', onlyNumber(val))}}
                                  onBlur={qprops.handleBlur("alokasi_quota")}
                                />
                                {/** SUBMIT */}
                                {rprops.values.container.find(
                                  (o) =>
                                    o.container_type_id ===
                                    quota.container_type_id
                                ) === undefined ? (
                                  <Icon
                                    name="checkbox-marked"
                                    color="gray"
                                    onPress={qprops.handleSubmit}
                                    style={{ flex: 1, fontSize: 30 }}
                                  />
                                ) : (
                                  <Icon
                                    name="checkbox-marked"
                                    color="green"
                                    onPress={qprops.handleSubmit}
                                    style={{ flex: 1, fontSize: 30 }}
                                  />
                                )}
                              </View>
                            )}
                          </Formik>
                        ))}
                        {/* <Space height={10} /> */}
                        <ErrorText>{rprops.errors.container}</ErrorText>
                        {/** Tanggal Berangkat */}
                        <DateTime
                          label="Departure"
                          onChangeValue={(date: string) =>
                            rprops.setFieldValue("tanggal_berangkat", date.date + " " + date.time)
                          }
                          type="datetime"
                        />
                        <ErrorText>{rprops.errors.tanggal_berangkat}</ErrorText>

                        {/** Tanggal Sampai */}
                        <DateTime
                          label="Arrival"
                          onChangeValue={(date: string) =>
                            rprops.setFieldValue("tanggal_tiba", date.date + " " + date.time)
                          }
                          type="datetime"
                        />
                        <ErrorText>{rprops.errors.tanggal_tiba}</ErrorText>

                        {/** Tanggal Closing BUMN */}
                        <DateTime
                          label="Closing BUMN"
                          onChangeValue={(date: string) =>
                            rprops.setFieldValue("tutup_jadwal_bumn", date.date + " " + date.time)
                          }
                          type="datetime"
                        />
                        <ErrorText>{rprops.errors.tutup_jadwal_bumn}</ErrorText>

                        {/** Tanggal Closing SWASTA */}
                        <DateTime
                          label="Closing Swasta"
                          onChangeValue={(date: string) =>
                            rprops.setFieldValue("tutup_jadwal_swasta", date.date + " " + date.time)
                          }
                          type="datetime"
                        />
                        <ErrorText>{rprops.errors.tutup_jadwal_swasta}</ErrorText>

                        {/** SUBMIT */}
                        {values.data.find((o) => o.trayek_id === item.id) ===
                        undefined ? (
                          <Button
                            mode="contained"
                            onPress={rprops.handleSubmit}
                          >
                            Simpan
                          </Button>
                        ) : (
                          <Button
                            mode="contained"
                            onPress={rprops.handleSubmit}
                            color={Colors.success}
                          >
                            Simpan
                          </Button>
                        )}
                      </Card>
                    )}
                  </Formik>
                </View>
              ))
            ) : (
              <ActivityIndicator />
            )}
            <Space height={50} />
            {route.length === values.data.length && values.kode_id && values.kapal_id ? (
              <Button style={styles.floatingButton} onPress={handleSubmit} mode="contained">Buat Jadwal</Button>
            ) : (
              <Button style={styles.floatingButton} onPress={null} disabled={true} mode="contained">
                Buat Jadwal
              </Button>
            )}
            <Space height={20} />
          </View>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  card: {
    padding: 10,
    elevation: 3,
    marginTop: 10,
    borderRadius: 3,
  },
  floatingButton: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
  }
});
