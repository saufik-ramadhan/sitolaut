import React, { useState } from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  Button,
  List,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput, Space, TextInput } from "../../components";
import { DateFormat } from "../../services/utils";
import {
  unitFilterClear,
  unitFetchPage,
  unitDelete,
  unitFilterAdd,
} from "./../../redux/unitReducer";
import { Formik } from "formik";
import { Item } from 'react-native-paper/lib/typescript/src/components/List/List';

function FloatingAdd({ navigation }: RegulatorStackProps<"Satuan">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateSatuan");
      }}
    />
  );
}

export default function Satuan({ navigation }: RegulatorStackProps<"Satuan">) {
  const dispatch = useDispatch();
  const [nama, setNama] = useState();
  const unit = useSelector(function (state: RootState) {
    return state.unit.unitData ? state.unit.unitData : [];
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.unit.unitGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.unit.unitFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.unit.unitGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.unit.unitAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.unit.unitDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.unit.unitViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      ...filter,
    };
    dispatch(unitFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      ...filter,
    };
    dispatch(unitFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(unitDelete(id));
    dispatch(unitFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(unitFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <View style={{paddingHorizontal: 20, backgroundColor: 'whitesmoke'}}>
        <TextInput
          label="Cari"
          onChangeText={(val) => setNama(val)}
          value={nama}
          style={{ width: '100%' }}
        />
        <Button onPress={() => dispatch(unitFilterAdd(val))}>Cari</Button>
      </View>
      <FlatList
        data={unit}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <List.Item
              title={item.satuan}
              right={() =>
                <>
                  <IconButton
                    icon="square-edit-outline"
                    onPress={() => navigation.navigate("UpdateSatuan", {item: item})}
                  />
                  <IconButton
                    icon="delete"
                    onPress={() => {
                      Alert.alert(
                        "Anda yakin",
                        "Menghapus Satuan ini ?",
                        [
                          { text: "OK", onPress: () => handleDelete(item.id) },
                          { text: "Cancel", onPress: () => null }
                        ],
                        { cancelable: false }
                      );
                    }}
                  />
                </>
              }
            />
          );
        }}
      />
      <FloatingAdd navigation={navigation} />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    marginBottom: 2,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});
