import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import {
  unitAdd,
  unitFetchOne,
  unitUpdate,
  unitFilterAdd,
} from "../../redux/unitReducer";
import { Button, HelperText } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import * as Yup from 'yup';

const Schema = Yup.object({
  id: Yup.number().required().label("ID"),
  satuan: Yup.string().required().label("Satuan")
})

export default function UpdateSatuan({
  navigation,
  route,
}: RegulatorStackProps<"UpdateSatuan">) {
  const dispatch = useDispatch();
  const { id, satuan } = route.params.item;
  function handleUpdate(values) {
    dispatch(unitUpdate(values));
  }
  return (
    <Root style={{padding: 20}}>
      <Formik
        validationSchema={Schema}
        initialValues={{
          id: id,
          satuan: satuan,
        }}
        onSubmit={(values) => {
          handleUpdate(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors }) => (
          <>
            <TextInput
              label="Satuan"
              onChangeText={handleChange("satuan")}
              onBlur={handleBlur("satuan")}
              value={values.satuan}
              error={errors.satuan}
            />
            <HelperText type="error">{errors.satuan}</HelperText>
            <Button onPress={handleSubmit}>Update</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
