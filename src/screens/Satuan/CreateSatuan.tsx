import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Root, TextInput } from "../../components";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { unitAdd, unitFetchOne, unitFilterAdd } from "../../redux/unitReducer";
import { Button, HelperText } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import * as Yup from 'yup';

const Schema = Yup.object({
  satuan: Yup.string().required().label('Satuan')
})

export default function CreateSatuan({
  navigation,
  route,
}: RegulatorStackProps<"CreateSatuan">) {
  const dispatch = useDispatch();
  function handleAdd(values) {
    dispatch(unitAdd(values));
  }
  return (
    <Root style={{padding: 20}}>
      <Formik
        validationSchema={Schema}
        initialValues={{
          satuan: "",
        }}
        onSubmit={(values) => {
          handleAdd(values);
          navigation.goBack();
        }}
      >
        {({ handleSubmit, handleChange, handleBlur, values, errors }) => (
          <>
            <TextInput
              label="Satuan"
              onChangeText={handleChange("satuan")}
              onBlur={handleBlur("satuan")}
              placeholder=""
              value={values.satuan}
              error={errors.satuan}
            />
            <HelperText type="error">{errors.satuan}</HelperText>
            <Button onPress={handleSubmit}>Create</Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({});
