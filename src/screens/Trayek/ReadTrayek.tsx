import React from "react";
import { StyleSheet, Text, View, Alert } from "react-native";
import { Root, ErrorText, Space, SelectInput } from "../../components";
import Colors from "../../config/Colors";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import {
  msttrayekFetchPage,
  msttrayekFetchOne,
} from "../../redux/msttrayekReducer";
import { OperatorStackProps } from "../Navigator";
import {
  Card,
  Caption,
  Paragraph,
  TextInput,
  Button,
  IconButton,
  Dialog,
  List,
  Divider,
} from "react-native-paper";
import { routeAdd, routeFetchSub, routeDelete } from "../../redux/routeReducer";
import * as Yup from "yup";
import { portFetchAll } from "../../redux/portReducer";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { formatRupiah } from "../../services/utils";

/**
 * Validation Schema
 */
const TrayekSchema = Yup.object().shape({
  kode: Yup.string().required("Required"),
  kode_id: Yup.number().required("Required"),
  operator_id: Yup.number().required("Required"),
  port_destination_id: Yup.number()
    .required("Required")
    .moreThan(0, "Required"),
  port_origin_id: Yup.number().required("Required").moreThan(0, "Required"),
  hargaDry: Yup.number().required("Required"),
  hargaRefeer: Yup.number().required("Required"),
});

function EditRoute({
  visible,
  id,
  operator_id,
  port_origin_id,
  port_destination_id,
  port,
  onDismiss,
}) {
  return (
    <Dialog visible={visible} onDismiss={() => onDismiss()}>
      <Card style={{ padding: 10 }}>
        <Formik
          initialValues={{
            id: id,
            operator_id: `${operator_id}`,
            port_destination_id: port_destination_id,
            port_origin_id: port_origin_id,
          }}
          onSubmit={(values) => {
            alert(JSON.stringify(values));
            // routeUpdate(values);
          }}
        >
          {({
            handleChange,
            setFieldValue,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              {/**
               * Port Origin
               */}
              <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  selectedValue={values.port_origin_id}
                  style={{ flex: 10 }}
                  itemStyle={{ fontSize: 8 }}
                  mode="dropdown"
                  onValueChange={function (itemValue: string) {
                    setFieldValue("port_origin_id", itemValue);
                  }}
                >
                  <Picker.Item label="Pelabuhan Origin" value={0} />
                  {port.map((item, key) => {
                    return (
                      <Picker.Item
                        label={item.label}
                        value={item.id}
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </View>
              {errors.port_origin_id && touched.port_origin_id && (
                <ErrorText>{errors.port_origin_id}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Port Destination
               */}
              <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  selectedValue={values.port_destination_id}
                  style={{ flex: 10 }}
                  itemStyle={{ fontSize: 8 }}
                  mode="dropdown"
                  onValueChange={function (itemValue: string) {
                    setFieldValue("port_destination_id", itemValue);
                  }}
                >
                  <Picker.Item label="Pelabuhan Destination" value={0} />
                  {port.map((item, key) => {
                    return (
                      <Picker.Item
                        label={item.label}
                        value={item.id}
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </View>
              {errors.port_destination_id && touched.port_destination_id && (
                <ErrorText>{errors.port_destination_id}</ErrorText>
              )}
              <Space height={10} />
              <Button onPress={handleSubmit}>Simpan</Button>
            </>
          )}
        </Formik>
      </Card>
    </Dialog>
  );
}

export default function ReadTrayek({
  navigation,
  route,
}: OperatorStackProps<"ReadTrayek">) {
  const dispatch = useDispatch();
  const { trayekId, port } = route.params;
  const [refresh, setRefresh] = React.useState(false);
  const [editRoute, setEditRoute] = React.useState(false);
  const [editTrayek, setEditTrayek] = React.useState(false);
  /**
   * Selector
   */
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const trayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekViewSuccess
      ? state.msttrayek.msttrayekViewSuccess.data[0]
      : {};
  });
  const rute = useSelector(function (state: RootState) {
    return state.route.routeGetSuccess
      ? state.route.routeGetSuccess.data
      : [];
  });
  const createLoading = useSelector(function (state: RootState) {
    return state.route.routeAddLoading;
  });
  const deleteLoading = useSelector(function (state: RootState) {
    return state.route.routeDeleteLoading;
  });

  const addRoute = (data) => {
    const params = {
      kode: data.kode,
      kode_id: `${data.kode_id}`,
      operator_id: `${data.operator_id}`,
      port_destination_id: data.port_destination_id,
      port_origin_id: data.port_origin_id,
      rates: [
        {
          harga: data.hargaDry,
          type_container_id: 1,
        },
        {
          harga: data.hargaRefeer,
          type_container_id: 2,
        },
      ],
    };
    // alert(JSON.stringify(params));
    dispatch(routeAdd(params));
  };
  React.useEffect(() => {
    dispatch(msttrayekFetchOne(trayekId));
    dispatch(routeFetchSub(`/get_by_master/${trayekId}`));
  }, [createLoading, deleteLoading]);
  if(!rute && !trayek) return <Caption>Empty</Caption>;
  return (
    <>
      <Root style={styles.container}>
          <List.Subheader>Detail Trayek</List.Subheader>
          <List.Item
            title={trayek.kode}
            description="Kode Trayek"
            left={() => <List.Icon color="#000" icon="barcode" />}
          />
          <List.Item
            title={trayek.target + " Voyage"}
            description="Target"
            left={() => <List.Icon color="#000" icon="counter" />}
          />

          <List.Section>
            <List.Subheader>Rute</List.Subheader>
            {rute.map((item, key) => {
              if(!item.rates) return null;
              return (
                <View
                  key={key}
                >
                  <List.Item
                    title={item.pel_asal}
                    description="Origin"
                    titleStyle={{color: Colors.pri}}
                    left={() => <List.Icon color="#000" icon="anchor" />}
                  />
                  <List.Item
                    title={item.pel_sampai}
                    description="Destination"
                    titleStyle={{color: Colors.pri}}
                    left={() => <List.Icon color="#000" icon="anchor" />}
                  />
                  <List.Subheader>Tarif</List.Subheader>
                  {
                    item.rates.map((price, keyp) => (
                      <List.Item
                        title={"Rp." + formatRupiah(parseInt(price.price))}
                        titleStyle={{color:'orangered'}}
                        description={price.type_container_name}
                        left={() => <List.Icon color="#000" icon="cash" />}
                      />    
                    ))
                  }
                </View>
              );
            })}
          </List.Section>
        <Space height={10} />
      </Root>
    </>
  );
}

const styles = StyleSheet.create({
  container: {},
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
