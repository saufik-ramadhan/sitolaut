import React from "react";
import { StyleSheet, Text, View, Alert } from "react-native";
import { Root, ErrorText, Space, SelectInput } from "../../components";
import Colors from "../../config/Colors";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { OperatorStackProps } from "../Navigator";
import {
  Card,
  Caption,
  Paragraph,
  TextInput,
  Button,
  HelperText,
  List,
} from "react-native-paper";
import { routeAdd, routeFetchSub } from "../../redux/routeReducer";
import * as Yup from "yup";
import { portFetchAll } from "../../redux/portReducer";
import { Formik, isEmptyArray } from "formik";
import { Picker } from "@react-native-community/picker";
import { typecontainerFetchAll } from "../../redux/typecontainerReducer";
import { getData } from "../../redux/masterkodetrayekReducer";
import { masterhargaClear, masterHargaGetActive } from "../../redux/masterhargaReducer";
import { formatRupiah, onlyNumber } from "../../services/utils";
import { isArray, isUndefined } from "lodash";
import { Item } from 'react-native-paper/lib/typescript/src/components/List/List';

/**
 * Validation Schema
 */
const TrayekSchema = Yup.object().shape({
  kode: Yup.string().required().label('Kode Trayek'),
  operator_id: Yup.number().required().label('Operator ID'),
  port_destination_id: Yup.number().required().moreThan(0, "Pilih Pelabuhan").label('Pelabuhan Tujuan'),
  port_origin_id: Yup.number().required("Required").moreThan(0, "Pilih Pelabuhan").label('Pelabuhan Asal'),
  target: Yup.number().required().moreThan(0, "Masukkan Target Voyage").label('Target Voyage'),
  hargaDry: Yup.number(),
  hargaRefeer: Yup.number(),
});

export default function CreateTrayek({
  navigation,
  route,
}: OperatorStackProps<"CreateTrayek">) {
  const dispatch = useDispatch();
  const [tipecargo, setTipecargo] = React.useState(0);
  /**
   * Selector
   */
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const createLoading = useSelector(function (state: RootState) {
    return state.route.routeAddLoading;
  });
  const { masterkodetrayek, masterharga } = useSelector((state: RootState) => state)

  const fetchData = () => {
    let params = {
      statusTrayek: 1,
      idOperator: id
    }
    dispatch(getData(params));
    dispatch(masterhargaClear());
    // alert(JSON.stringify(params));
  };
  React.useEffect(() => {
    fetchData();
  }, [])
  
  return (
    <Root>
      <Formik
        initialValues={{
          kode:"",
          operator_id: String(id),
          target: "",
          port_origin_id: "",
          port_destination_id: "",
        }}
        onSubmit={(vals) => {
          let prices1 = [
            {type_container_id: 1, harga: masterharga.getActiveMasterHarga.data ? masterharga.getActiveMasterHarga.data[0].tarif_dry : 0},
            {type_container_id: 2, harga: masterharga.getActiveMasterHarga.data ? masterharga.getActiveMasterHarga.data[0].tarif_reefer : 0},
          ]
          let prices2 = [
            {type_container_id: 3, harga: masterharga.getActiveMasterHarga.data ? masterharga.getActiveMasterHarga.data[0].tarif_curah : 0},
            {type_container_id: 4, harga: masterharga.getActiveMasterHarga.data ? masterharga.getActiveMasterHarga.data[0].tarif_box : 0}
          ]
          let prices = tipecargo == 1 ? prices1 : tipecargo == 2 ? prices2 : [];
          let detail = {
            ...vals,
            rates: [
              ...prices
            ]
          }
          // alert(JSON.stringify(detail))
          dispatch(routeAdd(detail));
          navigation.goBack();
        }}
        validationSchema={TrayekSchema}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <View style={{padding: 20}}>
            {/**
             * Kode Trayek
             */}
            <SelectInput
              options={masterkodetrayek.getDataSuccess.data}
              objectKey="kode_trayek"
              label="Kode Trayek"
              onChangeValue={(val) => {
                setFieldValue('kode', val.kode_trayek)
                setFieldValue('port_origin_id', val.port_origin_id);
                setFieldValue('port_destination_id', val.port_destination_id);
                setTipecargo(val.id_tipe_cargo);
                dispatch(masterHargaGetActive({kodeTrayek: val.id}));
              }}
            />
            
            {isArray(masterharga.getActiveMasterHarga.data) && !isUndefined(masterharga.getActiveMasterHarga.data) && !isEmptyArray(masterharga.getActiveMasterHarga.data) ? (
              <>
                <List.Item left={() => <List.Icon icon="anchor"/>} title={masterharga.getActiveMasterHarga.data[0].pol} description="Pelabuhan Asal"/>
                <List.Item left={() => <List.Icon icon="anchor"/>} title={masterharga.getActiveMasterHarga.data[0].pod} description="Pelabuhan Tujuan"/>
                {masterharga.getActiveMasterHarga.data[0].tarif_dry !== 0 && 
                  <List.Item left={() => <List.Icon icon="cash"/>} titleStyle={{color: 'orangered'}} title={'Rp.' + formatRupiah(masterharga.getActiveMasterHarga.data[0].tarif_dry)} description="Tarif DRY"/>
                }
                {masterharga.getActiveMasterHarga.data[0].tarif_reefer !== 0 && 
                  <List.Item left={() => <List.Icon icon="cash"/>} titleStyle={{color: 'orangered'}} title={'Rp.' + formatRupiah(masterharga.getActiveMasterHarga.data[0].tarif_reefer)} description="Tarif REEFER"/>
                }
                {masterharga.getActiveMasterHarga.data[0].tarif_curah !== 0 && 
                  <List.Item left={() => <List.Icon icon="cash"/>} titleStyle={{color: 'orangered'}} title={'Rp.' + formatRupiah(masterharga.getActiveMasterHarga.data[0].tarif_curah)} description="Tarif CURAH"/>
                }
                {masterharga.getActiveMasterHarga.data[0].tarif_box !== 0 && 
                  <List.Item left={() => <List.Icon icon="cash"/>} titleStyle={{color: 'orangered'}} title={'Rp.' + formatRupiah(masterharga.getActiveMasterHarga.data[0].tarif_box)} description="Tarif BOX"/>
                }
                {/**
                 * Target Voyage
                 */}
                <TextInput
                  label="Target Voyage"
                  style={[styles.formItem, styles.textInput]}
                  value={values.target}
                  onChangeText={(val) => setFieldValue('target', onlyNumber(val))}
                  onBlur={handleBlur("target")}
                  keyboardType="number-pad"
                  error={errors.target && touched.target}
                />
                {errors.target && touched.target && (
                  <HelperText type="error">{errors.target}</HelperText>
                )}

                <Space height={10}/>
                {/**
                 * Submit
                 */}
                <Button
                  onPress={handleSubmit}
                  color={Colors.sec}
                  mode="contained"
                  loading={createLoading}
                >
                  Simpan
                </Button>
              </>
            ) : <Caption>Harga belum ditentukan</Caption>}

            
          </View>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  formItem: {
    backgroundColor: Colors.white,
    color: "black",
    padding: 5,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 0,
  },
});
