import React from "react";
import { StyleSheet, Text, View, FlatList, TextInput } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, ErrorText, SelectInput } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import { Button } from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import {
  msttrayekFetchPage,
  msttrayekFilterAdd,
} from "./../../redux/msttrayekReducer";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { portFetchAll } from "../../redux/portReducer";
import * as Yup from "yup";

export default function FilterTrayek({
  navigation,
  route,
}: OperatorStackProps<"FilterTrayek">) {
  const dispatch = useDispatch();
  const { port } = route.params;

  return (
    <Root style={styles.container}>
      <Formik
        initialValues={{
          kode_trayek: "",
          pelabuhan_asal_id: "",
          pelabuhan_tujuan_id: "",
          operator_id: "",
        }}
        onSubmit={(values) => {
          dispatch(msttrayekFilterAdd(values));
          navigation.goBack();
        }}
      >
        {({
          handleChange,
          setFieldValue,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <>
            {/**
             * Kode Trayek
             */}
            <TextInput
              style={[styles.formItem, styles.textInput]}
              value={values.kode_trayek}
              onChangeText={handleChange("kode_trayek")}
              onBlur={handleBlur("kode_trayek")}
              placeholder="Kode Trayek"
            />
            {errors.kode_trayek && touched.kode_trayek && (
              <ErrorText>{errors.kode_trayek}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Port Origin
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.pelabuhan_asal_id}
                style={{ flex: 10 }}
                itemStyle={{ fontSize: 8 }}
                mode="dropdown"
                onValueChange={function (itemValue: string) {
                  setFieldValue("pelabuhan_asal_id", itemValue);
                }}
              >
                <Picker.Item label="Pelabuhan Origin" value={0} />
                {port.map((item, key) => {
                  return (
                    <Picker.Item label={item.label} value={item.id} key={key} />
                  );
                })}
              </Picker>
            </View>
            {errors.pelabuhan_asal_id && touched.pelabuhan_asal_id && (
              <ErrorText>{errors.pelabuhan_asal_id}</ErrorText>
            )}
            <Space height={10} />

            {/**
             * Port Destination
             */}
            <View
              style={[
                styles.formItem,
                { flexDirection: "row", justifyContent: "center" },
              ]}
            >
              <Picker
                selectedValue={values.pelabuhan_tujuan_id}
                style={{ flex: 10 }}
                itemStyle={{ fontSize: 8 }}
                mode="dropdown"
                onValueChange={function (itemValue: string) {
                  setFieldValue("pelabuhan_tujuan_id", itemValue);
                }}
              >
                <Picker.Item label="Pelabuhan Destination" value={0} />
                {port.map((item, key) => {
                  return (
                    <Picker.Item label={item.label} value={item.id} key={key} />
                  );
                })}
              </Picker>
            </View>
            {errors.pelabuhan_tujuan_id && touched.pelabuhan_tujuan_id && (
              <ErrorText>{errors.pelabuhan_tujuan_id}</ErrorText>
            )}
            <Space height={10} />

            <Button icon={"filter"} onPress={handleSubmit} mode="contained">
              Filter
            </Button>
          </>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
