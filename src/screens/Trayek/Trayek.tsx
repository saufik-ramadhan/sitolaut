import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, Icon, FilterButton } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import {
  msttrayekFetchPage,
  msttrayekDelete,
  msttrayekFilterClear,
  msttrayekFetchOne,
} from "./../../redux/msttrayekReducer";
import { portFetchAll } from "../../redux/portReducer";
import { typecontainerFetchAll } from "../../redux/typecontainerReducer";

function FloatingAdd({ navigation }: OperatorStackProps<"Trayek">) {
  const port = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  const typecontainer = useSelector(function (state: RootState) {
    return state.typecontainer.typecontainerGetSuccess
      ? state.typecontainer.typecontainerGetSuccess.data
      : [];
  });
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateTrayek", {
          port: port,
          typecontainer: typecontainer,
        });
      }}
    />
  );
}

export default function Trayek({
  navigation,
  route,
}: OperatorStackProps<"Trayek">) {
  const dispatch = useDispatch();
  const refresh = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetLoading;
  });
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetSuccess;
  });
  const msttrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekData
      ? state.msttrayek.msttrayekData
      : [
          {
            data: [],
          },
        ];
  });
  const createSuccess = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekAddLoading;
  });
  const routeAddSuccess = useSelector(function (state: RootState) {
    return state.route.routeAddLoading;
  });
  const routeDelLoading = useSelector(function (state: RootState) {
    return state.route.routeDeleteLoading;
  });
  const deleteSuccess = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekDeleteLoading;
  });
  const filter = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekFilter || {};
  });
  const loading = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetLoading;
  });
  const port = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  const fetchData = (filter) => {
    const params = {
      length: 10,
      start: 0,
      operator_id: `${id}`,
      ...filter,
    };
    dispatch(msttrayekFetchPage(params));
  };
  const handleReactEnd = (filter) => {
    const params = {
      length: 10,
      start: currentPage * 10,
      operator_id: `${id}`,
      ...filter,
    };
    dispatch(msttrayekFetchPage(params));
  };

  const onRefresh = () => {
    dispatch(msttrayekFilterClear());
  };

  React.useEffect(() => {
    dispatch(portFetchAll());
    dispatch(typecontainerFetchAll());
  }, []);
  React.useEffect(() => {
    fetchData(filter);
  }, [filter, createSuccess, deleteSuccess, routeAddSuccess, routeDelLoading]);

  return (
    <>
      <FlatList
        data={msttrayek}
        ListFooterComponent={<Space height={80} />}
        onEndReached={() => {
          hasNext ? handleReactEnd(filter) : null;
        }}
        onEndReachedThreshold={0.5}
        onRefresh={onRefresh}
        refreshing={loading}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              style={styles.card}
              onPress={function () {
                navigation.navigate("ReadTrayek", {
                  trayekId: item.mst_trayek_id,
                  port: port,
                });
              }}
            >
              <View>
                <Caption>
                  Kode Trayek : <Paragraph>{item.kode}</Paragraph>
                </Caption>
                <Caption>
                  Rute :{" "}
                  {item.data &&
                    item.data.map(function (item, key) {
                      return (
                        <Paragraph key={key}>
                          {"\n"}
                          {item.pel_asal} - {item.pel_sampai}
                        </Paragraph>
                      );
                    })}
                </Caption>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  position: "absolute",
                  top: 0,
                  right: 0,
                }}
              >
                <IconButton
                  icon="square-edit-outline"
                  size={20}
                  onPress={function () {
                    dispatch(msttrayekFetchOne(item.mst_trayek_id));
                    navigation.navigate("UpdateTrayek");
                  }}
                />
                <IconButton
                  icon="delete-outline"
                  size={20}
                  color={Colors.danger}
                  onPress={function () {
                    Alert.alert(
                      "Anda Yakin",
                      "Anda yakin akan menghapus trayek ini?",
                      [
                        {
                          text: "OK",
                          onPress: () => {
                            dispatch(msttrayekDelete(item.mst_trayek_id));
                            fetchData(filter);
                          },
                        },
                        {
                          text: "Cancel",
                          onPress: function () {
                            console.log("Cancel");
                          },
                        },
                      ],
                      { cancelable: false }
                    );
                  }}
                />
              </View>
            </Card>
          );
        }}
      />
      <FilterButton
        onPress={function () {
          navigation.navigate("FilterTrayek", {
            port: port,
          });
        }}
      />
      <FloatingAdd navigation={navigation} route={route} />
    </>
  );
}

const styles = StyleSheet.create({
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    padding: 10,
    marginBottom: 3,
  },
});
