import React, { useEffect } from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root, Space, ErrorText } from "../../components";
import Colors from "../../config/Colors";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import {
  Paragraph,
  Card,
  Caption,
  Badge,
  IconButton,
  Button,
  TextInput,
} from "react-native-paper";
import { OperatorStackProps } from "../Navigator";
import {
  msttrayekFetchPage,
  msttrayekUpdate,
  msttrayekFetchOne,
} from "./../../redux/msttrayekReducer";
import { Formik } from "formik";
import { Picker } from "@react-native-community/picker";
import { portFetchAll } from "../../redux/portReducer";
import * as Yup from "yup";

const UpdateTrayekValidation = Yup.object().shape({
  kode: Yup.string().required(),
  target: Yup.string().required().label("Target"),
})

export default function UpdateTrayek({
  navigation,
  route,
}: OperatorStackProps<"UpdateTrayek">) {
  const dispatch = useDispatch();
  const msttrayekViewSuccess = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekViewSuccess;
  });
  const msttrayekAddSuccess = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekAddSuccess;
  });
  const msttrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekViewSuccess
      ? state.msttrayek.msttrayekViewSuccess.data
      : [{ dsad: "" }];
  });
  return (
    <Root style={styles.container}>
      {msttrayekViewSuccess && (
        <Formik
          validationSchema={UpdateTrayekValidation}
          initialValues={{
            id: msttrayek[0].id,
            kode: msttrayek[0].kode,
            target: `${msttrayek[0].target}`,
            users_id: msttrayek[0].users_id,
          }}
          onSubmit={(values) => {
            Alert.alert("Perbarui", "Anda yakin ingin memperbarui trayek ini ?", [
              {
                text:'Ya', onPress:() => {
                  dispatch(msttrayekUpdate(values));
                  navigation.goBack();
                }
              },
              {
                text:'Tdk', onPress:() => null
              }
            ])
          }}
        >
          {({
            handleChange,
            setFieldValue,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              {/**
               * Kode Trayek
               */}
              <TextInput
                style={[styles.formItem, styles.textInput]}
                value={values.kode}
                onChangeText={handleChange("kode")}
                onBlur={handleBlur("kode")}
                label="Kode Trayek"
                disabled
              />
              {errors.kode && touched.kode && (
                <ErrorText>{errors.kode}</ErrorText>
              )}
              <Space height={10} />

              {/**
               * Target
               */}
              <TextInput
                style={[styles.formItem, styles.textInput]}
                value={values.target}
                onChangeText={handleChange("target")}
                onBlur={handleBlur("target")}
                label="Target"
                keyboardType="number-pad"
              />
              {errors.target && touched.target && (
                <ErrorText>{errors.target}</ErrorText>
              )}
              <Space height={10} />

              <Button
                icon={"square-edit-outline"}
                onPress={handleSubmit}
                mode="contained"
              >
                Update
              </Button>
            </>
          )}
        </Formik>
      )}
    </Root>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.white,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
  card: {
    backgroundColor: "white",
    elevation: 3,
    borderLeftColor: Colors.pri,
    borderLeftWidth: 8,
    padding: 10,
    borderRadius: 0,
    marginBottom: 10,
  },
  textInput: {
    paddingHorizontal: 10,
  },
});
