import React, { useState } from "react";
import { View, StyleSheet, Alert, Image } from "react-native";
import StepIndicator from "react-native-step-indicator";
import {
  Card,
  Caption,
  List,
  Badge,
  Paragraph,
  Divider,
  Subheading,
  Button,
  TextInput,
  HelperText,
  Avatar,
  ActivityIndicator,
  Title,
  RadioButton,
} from "react-native-paper";
import Colors from "../../config/Colors";
import { FontAwesome5 as Icon } from "@expo/vector-icons";
import {
  Space,
  Root,
  PlaceholderLoading,
  FileInput,
  DateTime,
} from "../../components";
import {
  DateFormat,
  formatRupiah,
  getUserType,
  DateTimeFormat,
  printReleaseOrder,
  printShippingInstruction,
  printBillOfLading,
  DateFormat3,
  DateFormat5,
} from "../../services/utils";
import {
  bookingFetchOne,
  bookingConfirm,
  bookingPayment,
  bookingGenerateBL,
  bookingAmbilBarang,
  bookingFetchRo,
  bookingFetchSi,
  getListPayment,
  updatePayChannel,
} from "../../redux/bookingReducer";
import { useDispatch, useSelector } from "react-redux";
import {
  containerFetchSub,
  containerConfirm,
  containerUpdate,
} from "../../redux/containerReducer";
import { detailcontainerFetchSub } from "../../redux/detailContainerReducer";
import { trackUpdate } from "../../redux/trackReducer";
import moment from "moment";
import { Form, Formik } from "formik";
import * as yup from "yup";
import {
  BASE_URL,
  BOOKING_URL,
  IP_URL,
  UPLOAD_URL,
} from "../../config/constants";
import { RootState } from "../../redux/rootReducer";
import { operatorFetchUser } from "../../redux/operatorReducer";
import { isNull } from "lodash";
import AsyncStorage from "../../AsyncStorage";
import { bookingFetchBol } from "./../../redux/bookingReducer";
import { ShipperStackProps } from "../Navigator";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import { packagingFetchAll } from "../../redux/packagingReducer";
import { purchaseSetFcl } from "../../redux/purchaseReducer";
const imgDummy = require("../../assets/img/imgDummy.png");

export default function ReadVesselBooking({ route, navigation }: ShipperStackProps<'ReadVesselBooking'>) {
  const dispatch = useDispatch();
  const [, updateState] = React.useState();
  const forceUpdate = React.useCallback(() => updateState({}), []);
  const currentDate = new Date();
  const [isShowConfirm, setIsShowConfirm] = useState(false);
  const [isFormPayment, setIsFormPayment] = useState(false);
  const [isDocPayment, setIsDocPayment] = useState(false);
  const [isConfirmPayment, setIsConfirmPayment] = useState(false);
  const [isFormFinishing, setIsFormFinishing] = useState(false);
  const [isDocBill, setIsDocBill] = useState(false);
  const [file_gambar, setFileGambar]: any = useState("");
  const [isPengambilanBarang, setIsPengambilanBarang] = useState(false);
  const id_booking = route.params.itemId;
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const purchase = useSelector((state: RootState) => state.booking.bookingViewSuccess.data ? state.booking.bookingViewSuccess.data.purchase_order[0] : {});
  const bookingOne = useSelector(function (state: RootState | any) {
    return state.booking.bookingViewSuccess
      ? state.booking.bookingViewSuccess.data
      : {};
  });
  const containers = useSelector(function (state: RootState | any) {
    return state.container.containerGetSuccess
      ? state.container.containerGetSuccess.data
      : [{}];
  });
  const containersGetLoading = useSelector(function (state: RootState | any) {
    return state.container.containerGetLoading;
  });
  const containersAddLoading = useSelector(function (state: RootState | any) {
    return state.container.containerAddLoading;
  });
  const numContainer = useSelector(function (state: RootState | any) {
    return state.container.containerGetSuccess
      ? state.container.containerGetSuccess.numContainer
      : false;
  });
  const isSuccess: any = useSelector(function (state: RootState | any) {
    return state.booking.bookingAddSuccess
      ? state.booking.bookingAddSuccess
      : [{}];
  });

  const containerIsSuccess = useSelector(function (state: RootState | any) {
    return state.container.containerAddSuccess
      ? state.container.containerAddSuccess
      : [{}];
  });

  const containerLoading = useSelector(function (state: RootState) {
    return state.container.containerAddLoading;
  });

  const ViewLoading = useSelector(function (state: RootState | any) {
    return state.booking.bookingViewLoading;
  });

  const { booking } = useSelector((state: RootState) => state);
  // const id_booking = item.id
  const {
    kode_booking,
    kode_trayek,
    nama_pelabuhan_asal,
    nama_pelabuhan_tujuan,
    tanggal_berangkat,
    tanggal_tiba,
    request_kuota,
    type_container_name,
    status,
    tutup_jadwal_shipper_bumn,
    tutup_jadwal_shipper_swasta,
    shipper_profile_nama_perusahaan,
    shipper_profile_npwp,
    shipper_profile_alamat,
    shipper_profile_nama_pic,
    shipper_profile_telp_pic,
    consignee_profile_nama_perusahaan,
    consignee_profile_npwp,
    consignee_profile_alamat,
    consignee_profile_nama_pic,
    consignee_profile_telp_pic,
    deskripsi,
    is_fcl,
    purchase_order,
    total_diterima,
    all_price,
    bukti_pembayaran_file,
    operator_kapal_id,
    jadwal_id,
    operator_kapal_nama_bank,
    operator_kapal_nomor_rekening,
    operator_kapal_atas_nama,
    bukti_pembayaran_time,
    logo_operator,
    bill_lading_number,
    recipient_shipper_name,
    is_received,
    recipient_shipper_date,
    recipient_shipper_ktp,
    operator_kapal_nama_perusahaan,
  }: any = bookingOne;
  // const booking = bookingOne;

  const fetchData = () => {
    dispatch(bookingFetchOne(id_booking));
    dispatch(containerFetchSub("booking/" + id_booking));
    dispatch(detailcontainerFetchSub("get_booking/" + id_booking));
    dispatch(getListPayment());
  };

  const handleAprove = (id: any) => {
    let data = {
      id: id,
      id_jadwal: jadwal_id,
      id_booking: id_booking,
      is_approve: "1",
    };
    dispatch(containerConfirm(data));
    dispatch(containerFetchSub("booking/" + id_booking));
  };

  const handleReject = (id: any) => {
    let data: any = {
      id: id,
      id_jadwal: jadwal_id,
      id_booking: id_booking,
      is_approve: "0",
    };
    dispatch(containerConfirm(data));
    dispatch(containerFetchSub("booking/" + id_booking));
  };

  const onBookingConfirmed = () => {
    let data = {
      id: id_booking,
      status: "2",
      operator_id: operator_kapal_id,
    };

    Alert.alert("Konfirmasi Booking", "Terima booking ini?", [
      {
        text: "Batal",
      },
      {
        text: "Terima",
        onPress: () => {
          setIsShowConfirm(false);
          dispatch(bookingConfirm(data));
        },
      },
    ]);
    // Update Track Log
    let dateTime: any = new Date();
    dateTime = moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
    let trackData = {
      no_po: purchase_order.no_po,
      ts_operator: dateTime,
      ts_port_origin: dateTime,
      operator_id: id,
    };
    dispatch(trackUpdate(trackData));
    forceUpdate();
  };

  const onPaymentConfirmed = () => {
    let data = {
      id: id_booking,
      status: "3",
      operator_id: operator_kapal_id,
    };
    Alert.alert("Konfirmasi Booking", "Terima Pembayaran booking ini?", [
      {
        text: "Batal",
      },
      {
        text: "Terima",
        onPress: () => {
          setIsConfirmPayment(false);
          dispatch(bookingConfirm(data));
          forceUpdate();
        },
      },
    ]);
  };

  const onPaymentUnConfirmed = () => {
    let data = {
      id: id_booking,
      status: "4",
      operator_id: operator_kapal_id,
    };

    Alert.alert("Konfirmasi Booking", "Tolak booking ini?", [
      {
        text: "Batal",
      },
      {
        text: "Tolak",
        onPress: () => {
          setIsShowConfirm(false);
          dispatch(bookingConfirm(data));
          forceUpdate();
        },
      },
    ]);
  };
  const { isOperator, isShipper, isConsignee } = getUserType(usertype);
  const setShowFormPayment = () => {
    if (isShipper) {
      if (status === 2) {
        if (bukti_pembayaran_file === null) {
          if (!isFormPayment) {
            setIsFormPayment(true);
          }
        } else {
          if (isFormPayment) {
            setIsFormPayment(false);
          }
        }
      }
    }
  };
  const setConfirmPayment = () => {
    if (isOperator) {
      if (status == 2) {
        if (bukti_pembayaran_file != null) {
          if (!isConfirmPayment) {
            setIsConfirmPayment(true);
          }
        }
      }
    }
  };
  const setDocPayment = () => {
    if (status >= 3) {
      if (bukti_pembayaran_file != "") {
        if (!isDocPayment) {
          setIsDocPayment(true);
          setIsFormPayment(false);
        }
      }
    }
  };
  const setFormFinishing = () => {
    if (status == 3) {
      if (bukti_pembayaran_file != null) {
        if (bill_lading_number == null) {
          if (!isFormFinishing) {
            setIsFormFinishing(true);
          }
        }
      }
    }
  };

  const setDocBill = () => {
    if (isSuccess) {
      if (status == 5) {
        if (bill_lading_number != null) {
          if (!isDocBill) {
            setIsDocBill(true);
            setIsFormFinishing(false);
          }
          if (isShipper) {
            if (recipient_shipper_name == null) {
              if (!is_received) {
                if (!isPengambilanBarang) {
                  setIsPengambilanBarang(true);
                }
              }
            } else {
              if (isPengambilanBarang) {
                setIsPengambilanBarang(false);
              }
            }
          }
        }
      }
    }
  };
  const handleSubmit = () => {
    if (!file_gambar.name) {
      console.log(file_gambar.name);
      Alert.alert("Perhatian", "Upload bukti pembayaran");
      return;
    }
    setIsFormPayment(false);
    let values = {
      id: id_booking,
      operator_id: operator_kapal_id,
      bukti_pembayaran_file: file_gambar,
    };
    dispatch(bookingPayment(values));
  };

  const handleNumContainer = (values: any) => {
    dispatch(containerUpdate(values));
  };

  const handleBillSubmit = () => {
    let dataPost = {
      id: id_booking,
      operator_id: operator_kapal_id,
    };
    dispatch(bookingGenerateBL(dataPost));

    // Update Track Log
    let dateTime: any = new Date();
    dateTime = moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
    let trackData = {
      no_po: purchase_order.no_po,
      ts_port_dest: dateTime,
    };
    dispatch(trackUpdate(trackData));
  };

  const handlePengambilanBarang = (values: any) => {
    setIsPengambilanBarang(false);
    // values.operator_id = operator_kapal_id;
    // values.recipient_shipper_ktp = file_gambar;
    // delete values.recipient_shipper_ktp_size;

    const ambilBarang = {
      recipient_shipper_name: values.recipient_shipper_name,
      id: values.id,
      recipient_shipper_date: values.recipient_shipper_date,
      recipient_shipper_ktp: values.recipient_shipper_ktp,
      operator_id: operator_kapal_id,
    };
    console.log(ambilBarang);
    dispatch(bookingAmbilBarang(ambilBarang));

    // Update Track Log
    let dateTime: any = new Date();
    dateTime = moment(dateTime).format("YYYY-MM-DD HH:mm:ss");
    let trackData = {
      no_po: purchase_order.no_po,
      ts_receive_shipper: dateTime,
    };
    dispatch(trackUpdate(trackData));
  };

  React.useEffect(() => {
    fetchData();
  }, [containerLoading]);
  React.useEffect(() => {
    if (isSuccess.success) {
      fetchData();
    }
  }, [isSuccess.success]);
  React.useEffect(() => {
    if (containerIsSuccess.success) {
      dispatch(containerFetchSub("booking/" + id_booking));
    }
  }, [containerIsSuccess.success]);

  React.useEffect(() => {
    if (status === 1) setIsShowConfirm(true);
    else setIsShowConfirm(false);
    setShowFormPayment();
    setConfirmPayment();
    setDocPayment();
    setFormFinishing();
    setDocBill();
  }, [status]);
  const setIndicatorPosition = (status: any) => {
    if (status == 1) {
      return 2;
    } else if (status == 2 && bukti_pembayaran_file === null) {
      return 3;
    } else if (status == 2 && bukti_pembayaran_file !== null) {
      return 4;
    } else if (status == 3) {
      return 5;
    } else if (status == 5) {
      return 6;
    } else return 1;
  };
  let logo_vendor = UPLOAD_URL + logo_operator;
  let bukti_pembayaran = "";
  let bill_pembayaran = "";
  if (bukti_pembayaran_file != "") {
    bukti_pembayaran = UPLOAD_URL + bukti_pembayaran_file;
  }

  if(bookingOne == {}) return <View style={{flex: 1, alignItems:'center', justifyContent: 'space-around'}}><ActivityIndicator animating={true} color={Colors.pri} /></View>;

  return (
    <View style={{ flex: 1 }}>
      <Root style={{ backgroundColor: "#eee" }}>
        {!ViewLoading && isSuccess && (
          <View>
            <Card style={{ paddingVertical: 5 }}>
              <StepIndicator
                stepCount={7}
                customStyles={customStyles}
                currentPosition={setIndicatorPosition(status)}
                renderLabel={() => <View></View>}
                renderStepIndicator={renderStepIndicator}
              />
            </Card>
            <Card>
              <List.Item
                title={`Booking | ${kode_booking}`}
                description={() => {
                  return (
                    <View>
                      {status == 1 && (
                        <Badge style={styles.statusSecondary}>
                          Menunggu validasi operator
                        </Badge>
                      )}
                      {status == 2 && bukti_pembayaran_file != null && (
                        <Badge style={styles.statusSecondary}>
                          Menunggu konfirmasi pembayaran
                        </Badge>
                      )}
                      {status == 2 && bukti_pembayaran_file === null && (
                        <Badge style={styles.statusSecondary}>
                          Menunggu pembayaran
                        </Badge>
                      )}
                      {status == 3 && (
                        <Badge style={styles.statusSecondary}>
                          Pembayaran diterima
                        </Badge>
                      )}
                      {status == 4 && (
                        <Badge
                          style={{
                            backgroundColor: Colors.danger,
                            alignSelf: "flex-start",
                          }}
                        >
                          Booking ditolak
                        </Badge>
                      )}
                      {status == 5 && (
                        <Badge style={styles.statusSuccess}>Selesai</Badge>
                      )}
                      {status == 6 && (
                        <Badge style={styles.statusSuccess}>
                          Sudah sampai tujuan
                        </Badge>
                      )}
                      <Paragraph>
                        {" "}
                        Dari {nama_pelabuhan_asal} ke {nama_pelabuhan_tujuan}
                      </Paragraph>
                    </View>
                  );
                }}
              />
              <View>
                {is_fcl == 1 && (
                  <View
                    style={{
                      backgroundColor: Colors.pri,
                      padding: 4,
                    }}
                  >
                    <Paragraph
                      style={{ color: Colors.white, alignSelf: "center" }}
                    >
                      FCL (Full Container Loaded)
                    </Paragraph>
                  </View>
                )}
                {is_fcl == 0 && (
                  <View
                    style={{
                      backgroundColor: Colors.pri,
                      padding: 4,
                    }}
                  >
                    <Paragraph
                      style={{ color: Colors.white, alignSelf: "center" }}
                    >
                      LCL (Less Container Loaded)
                    </Paragraph>
                  </View>
                )}
              </View>
            </Card>
            <List.Item title={<Subheading>Profile Operator</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama Operator</Caption>}
                right={() => (
                  <Paragraph>{operator_kapal_nama_perusahaan}</Paragraph>
                )}
              />
              <Divider />
            </Card>
            <List.Item title={<Subheading>Detail Waktu</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>ETD</Caption>}
                right={() => (
                  <Paragraph>{DateFormat(tanggal_berangkat)}</Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>ETD</Caption>}
                right={() => <Paragraph>{DateFormat(tanggal_tiba)}</Paragraph>}
              />
              <Divider />
              <List.Item
                title={<Caption>Tanggal Closing BUMN</Caption>}
                right={() => (
                  <Paragraph>{DateFormat(tutup_jadwal_shipper_bumn)}</Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>Tanggal Closing Swasta</Caption>}
                right={() => (
                  <Paragraph>
                    {DateFormat(tutup_jadwal_shipper_swasta)}
                  </Paragraph>
                )}
              />
            </Card>
            <List.Item title="Lokasi Pengambilan barang" />
            <Card>
              <List.Item
                title={<Paragraph>Pelabuhan {nama_pelabuhan_tujuan}</Paragraph>}
              />
            </Card>

            <List.Item title={<Subheading>Data Shipper</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama Perusahaan</Caption>}
                right={() => (
                  <Paragraph>{shipper_profile_nama_perusahaan}</Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>NPWP</Caption>}
                right={() => <Paragraph>{shipper_profile_npwp}</Paragraph>}
              />
              <Divider />
              <List.Item
                title={<Caption>Alamat</Caption>}
                right={() => <Paragraph>{shipper_profile_alamat}</Paragraph>}
              />
            </Card>
            <List.Item title={<Subheading>PIC Shipper</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama PIC</Caption>}
                right={() => <Paragraph>{shipper_profile_nama_pic}</Paragraph>}
              />
              <Divider />
              <List.Item
                title={<Caption>No. Telpon</Caption>}
                right={() => <Paragraph>{shipper_profile_telp_pic}</Paragraph>}
              />
              <Divider />
            </Card>

            {is_fcl === 1 && (
              <View>
                <List.Item title={<Subheading>Data Consignee</Subheading>} />
                <Card>
                  <Divider />
                  <List.Item
                    title={<Caption>Nama Perusahaan</Caption>}
                    right={() => (
                      <Paragraph>{consignee_profile_nama_perusahaan}</Paragraph>
                    )}
                  />
                  <Divider />
                  <List.Item
                    title={<Caption>NPWP</Caption>}
                    right={() => (
                      <Paragraph>{consignee_profile_npwp}</Paragraph>
                    )}
                  />
                  <Divider />
                  <List.Item
                    title={<Caption>Alamat</Caption>}
                    right={() => (
                      <Paragraph>{consignee_profile_alamat}</Paragraph>
                    )}
                  />
                </Card>
                <List.Item title={<Subheading>PIC Consignee</Subheading>} />
                <Card>
                  <Divider />
                  <List.Item
                    title={<Caption>Nama PIC</Caption>}
                    right={() => (
                      <Paragraph>{consignee_profile_nama_pic}</Paragraph>
                    )}
                  />
                  <Divider />
                  <List.Item
                    title={<Caption>No. Telpon</Caption>}
                    right={() => (
                      <Paragraph>{consignee_profile_telp_pic}</Paragraph>
                    )}
                  />
                  <Divider />
                </Card>
                <List.Item
                  title={<Subheading>Data Purchase Order</Subheading>}
                />
                {purchase_order.map((item: any, key: any) => {
                  return (
                    <Card key={key}>
                      <List.Item
                        title={<Caption>No. PO</Caption>}
                        right={() => <Paragraph>{item.no_po}</Paragraph>}
                      />
                    </Card>
                  );
                })}
              </View>
            )}
            {is_fcl === 0 && (
              <View>
                <List.Item title={<Subheading>Daftar PO</Subheading>} />
                {purchase_order.map((item: any, key: any) => {
                  const { no_po, nama_perusahaan, npwp, nama_pic } = item;
                  return (
                    <View key={key}>
                      <Card>
                        <List.Item
                          title={no_po}
                          description={() => {
                            return (
                              <>
                                <List.Item
                                  title={<Caption>Consignee</Caption>}
                                  right={() => (
                                    <Paragraph>{nama_perusahaan}</Paragraph>
                                  )}
                                />
                                <Divider />
                                <List.Item
                                  title={<Caption>NPWP</Caption>}
                                  right={() => <Paragraph>{npwp}</Paragraph>}
                                />
                                <Divider />
                                <List.Item
                                  title={<Caption>Nama PIC</Caption>}
                                  right={() => (
                                    <Paragraph>{nama_pic}</Paragraph>
                                  )}
                                />
                                <Divider />
                              </>
                            );
                          }}
                        />
                        <Divider />
                      </Card>
                      <Space height={5} />
                    </View>
                  );
                })}
              </View>
            )}

            <Card>
              <List.Item
                title={<Caption>Type</Caption>}
                right={() => <Paragraph>{type_container_name}</Paragraph>}
              />
              <Divider />
              <List.Item
                title={<Caption>Jumlah Container yang dipesan</Caption>}
                right={() => <Paragraph>{request_kuota}</Paragraph>}
              />
              <Divider />
              <List.Item
                title={<Caption>Jumlah Container yang diterima</Caption>}
                right={() => <Paragraph>{total_diterima}</Paragraph>}
              />
              <Divider />
              <List.Item
                title={<Caption>Jumlah yaang harus dibayar</Caption>}
                right={() => (
                  <Paragraph>Rp. {formatRupiah(all_price)}</Paragraph>
                )}
              />
              <Divider />
            </Card>
            <List.Item title={<Subheading>Daftar Container</Subheading>} right={() => (status == 1 && usertype == "Shipper") ? <Button compact onPress={
              function(){
                navigation.navigate('EditContainer', {
                  booking_id: bookingOne.id,
                })
              }
            }>Edit</Button> : null}/>

            {/* <--Approve and reject Container --> */}
            <View>
              {containers.map((item: any, key: number) => {
                const containerDetail = () => {
                  navigation.navigate("Modals", {
                    screen: "DetailContainerModal",
                    params: {
                      id_booking: id_booking,
                      id: item.id,
                      title: "Detail " + item.nama_container,
                    },
                  });
                };
                return !containersAddLoading && !containersGetLoading ? (
                  <View key={key}>
                    <Card onPress={containerDetail}>
                      <List.Item
                        title=""
                        titleStyle={{ height: 0 }}
                        right={() => (
                          <>
                            {isOperator && status === 1 && (
                              <View>
                                {/* <List.Icon icon="chevron-right" /> */}
                              </View>
                            )}
                          </>
                        )}
                        description={() => (
                          <>
                            <View
                              style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                              }}
                            >
                              <View>
                                <View
                                  style={{
                                    flexDirection: "column",
                                    // alignItems: "center",
                                  }}
                                >
                                  <Subheading>
                                    {item.nama_container}{" "}
                                    {item.nomor_container !== null
                                      ? "(" + item.nomor_container + ")"
                                      : null}
                                  </Subheading>
                                  <Paragraph style={{ fontWeight: "bold" }}>
                                    Harga Container : Rp.{" "}
                                    {formatRupiah(item.harga_percontainer)}
                                  </Paragraph>
                                  {item.is_approve === 1 ? (
                                    <Caption
                                      style={{
                                        backgroundColor: Colors.success,
                                        color: Colors.white,
                                        paddingHorizontal: 5,
                                        marginHorizontal: 5,
                                        borderRadius: 5,
                                        alignSelf: "flex-start",
                                      }}
                                    >
                                      DITERIMA
                                    </Caption>
                                  ) : (
                                    <Caption
                                      style={{
                                        backgroundColor: Colors.danger,
                                        color: Colors.white,
                                        paddingHorizontal: 5,
                                        marginHorizontal: 5,
                                        borderRadius: 5,
                                        alignSelf: "flex-start",
                                      }}
                                    >
                                      DITOLAK
                                    </Caption>
                                  )}
                                </View>
                              </View>
                              <List.Icon icon="chevron-right" />
                            </View>
                            <>
                              {isOperator && status === 1 && (
                                <View>
                                  {item.is_approve === 1 && (
                                    <View>
                                      <Button
                                        mode="contained"
                                        style={{
                                          height: 30,
                                          justifyContent: "center",
                                        }}
                                        labelStyle={{ fontSize: 10 }}
                                        color={Colors.danger}
                                        onPress={() => handleReject(item.id)}
                                        icon="close"
                                      >
                                        Tolak Container
                                      </Button>
                                    </View>
                                  )}
                                  {item.is_approve === 0 && (
                                    <Button
                                      mode="contained"
                                      style={{
                                        height: 30,
                                        justifyContent: "center",
                                      }}
                                      labelStyle={{ fontSize: 10 }}
                                      onPress={() => handleAprove(item.id)}
                                      icon="check"
                                    >
                                      Setujui Container
                                    </Button>
                                  )}
                                </View>
                              )}
                            </>
                            {isOperator &&
                              isConfirmPayment &&
                              item.nomor_container == null &&
                              item.is_approve && (
                                <Formik
                                  enableReinitialize
                                  onSubmit={handleNumContainer}
                                  initialValues={{ id: item.id, nomor: "" }}
                                  validationSchema={yup.object().shape({
                                    nomor: yup.string().required(),
                                  })}
                                >
                                  {(formikProps) => {
                                    return (
                                      <View style={{ flexDirection: "row" }}>
                                        <View style={{ flex: 1 }}>
                                          <TextInput
                                            label="Nomor Container"
                                            onChangeText={formikProps.handleChange(
                                              "nomor"
                                            )}
                                            error={
                                              formikProps.errors.nomor
                                                ? true
                                                : false
                                            }
                                            dense
                                          />
                                        </View>
                                        <Button
                                          mode="contained"
                                          style={{
                                            justifyContent: "center",
                                          }}
                                          labelStyle={{ fontSize: 10 }}
                                          onPress={formikProps.handleSubmit}
                                          icon="check"
                                        >
                                          simpan
                                        </Button>
                                      </View>
                                    );
                                  }}
                                </Formik>
                              )}
                          </>
                        )}
                      />
                    </Card>
                    <Space height={10} />
                  </View>
                ) : (
                  <View
                    style={{
                      flex: 1,
                      padding: 10,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <ActivityIndicator />
                  </View>
                );
              })}
            </View>
            {/* <-- End Approve and reject Container --> */}

            <Card>
              <List.Item title="Permintaan Khusus" description={deskripsi} />
            </Card>
          </View>
        )}
        <Space height={10} />
        {/* <-- Tampilan gambar bukti pembayaran --> */}
        {bukti_pembayaran_time != null && (
          <Card>
            <Card.Content>
              <Subheading>Bukti Pembayaran : </Subheading>
              <Divider />
              {bukti_pembayaran_file && (
                <Card.Cover
                  source={{ uri: UPLOAD_URL + bukti_pembayaran_file }}
                  style={{ resizeMode: "cover" }}
                />
              )}
              <Divider />
              <Paragraph style={{ fontWeight: "bold" }}>
                Tanggal konfirmasi: {DateTimeFormat(bukti_pembayaran_time)}
              </Paragraph>
            </Card.Content>
          </Card>
        )}
        {/* <-- End Tampilan gambar bukti pembayaran --> */}

        <Space height={10} />

        {/* <-- Button Release Order dan Shipping Instuction --> */}

        {(isOperator || isShipper) && isDocPayment && status !== 4 && (
          <Card>
            <Card.Content>
              <Button
                // onPress={onPaymentUnConfirmed}
                mode="contained"
                icon="download"
                onPress={function () {
                  dispatch(bookingFetchRo(id_booking));
                }}
              >
                Release Order
              </Button>
              <Space height={10} />
              <Button
                // onPress={onPaymentUnConfirmed}
                mode="contained"
                icon="download"
                onPress={function () {
                  dispatch(bookingFetchSi(id_booking));
                }}
              >
                Shipping Intruction
              </Button>
            </Card.Content>
          </Card>
        )}
        <Divider />
        {/* <--End Button Release Order dan Shipping Instuction --> */}
        <Space height={10} />

        {/* <-- Button Bill of lading --> */}
        {isOperator && isFormFinishing && (
          <Card>
            <Card.Content>
              <Subheading>Terbitkan Bill of Lading</Subheading>
              <Button
                onPress={handleBillSubmit}
                mode="contained"
                icon="download"
              >
                Terbitkan Bill of Lading
              </Button>
            </Card.Content>
          </Card>
        )}
        {/* <--End Button Bill of Lading --> */}

        <Space height={10} />
        {/* <-- Button Lihat Bill of Lading --> */}

        {(isOperator || isShipper) && isDocBill && (
          <View>
            <Card>
              <Card.Content>
                <Subheading>
                  Nomor Bill of Lading : {bill_lading_number}
                </Subheading>
                <Button
                  // onPress={handleBillSubmit}
                  mode="contained"
                  icon="download"
                  color={Colors.success}
                  onPress={function () {
                    dispatch(bookingFetchBol(id_booking));
                    // printBillOfLading(booking);
                  }}
                >
                  Lihat Bill of Lading
                </Button>
              </Card.Content>
            </Card>
            <Space height={10} />

            {/* <-- View Data pemgambilan barang  --> */}
              {recipient_shipper_date != null && (
                <Card>
                  <List.Subheader>Pengambilan barang oleh Shipper</List.Subheader>
                  <List.Item title={recipient_shipper_name} description="Nama Pengambil"/>
                  <List.Item title={DateTimeFormat(recipient_shipper_date)} description="Tanggal Pengambilan"/>
                  <Image source={{uri: UPLOAD_URL + recipient_shipper_ktp}} style={{width: '100%', height: 100}} resizeMode="contain"/>
                </Card>
              )}
            {/* <--End View Data pengambilan barang --> */}

            {/** Pengambilan barang oleh Consignee */}
            {
              !isNull(purchase.recipient_consignee_ktp) && (
                <Card>
                  <List.Subheader>Pengambilan barang oleh Consignee</List.Subheader>
                  <List.Item title={purchase.recipient_consignee_name} description="Nama Pengambil"/>
                  <List.Item title={DateFormat(purchase.recipient_consignee_date)} description="Tanggal Pengambilan"/>
                  <Image source={{uri: UPLOAD_URL + purchase.recipient_consignee_ktp}} style={{width: '100%', height: 100}} resizeMode="contain"/> 
                </Card>
              )
            }
            {/** -- */}
          </View>
        )}
        <PlaceholderLoading loading={ViewLoading} />
      </Root>

      {/* <-- Button Konfirmasi Booking --> */}
      {isOperator && status === 1 && (
        <Card>
          <Card.Content>
            <Subheading>Konfirmasi Booking : </Subheading>
            <Divider />
            <View style={{ flexDirection: "row", padding: 5 }}>
              <View style={{ flex: 1 }}>
                <Button
                  onPress={onPaymentUnConfirmed}
                  mode="outlined"
                  color={Colors.danger}
                >
                  Tolak
                </Button>
              </View>
              <View style={{ flex: 1 }}>
                <Button
                  onPress={onBookingConfirmed}
                  mode="contained"
                  color={Colors.accepted}
                >
                  Terima
                </Button>
              </View>
            </View>
          </Card.Content>
        </Card>
      )}
      {/* <-- End Button Konfirmasi Booking --> */}

      {/* <-- Form input bukti pembayaran --> */}

      {isShipper && status === 2 && isNull(bukti_pembayaran_file) && (
        <Card elevation={10} style={{backgroundColor: 'whitesmoke'}}>
          <Card.Content>
            <Title>Pembayaran</Title>
            <Formik
              initialValues={{
                institutionCode: "J104408",
                brivaNo: "77777",
                custCode: kode_booking.substring(2),
                nama: operator_kapal_nama_perusahaan,
                amount: String(parseInt(all_price)),
                keterangan: "Bayar Booking "+bookingOne.kode_booking,
                expiredDate: DateFormat5(currentDate.setHours(currentDate.getHours() + 3)),
                id_payment: 0,
                id_booking: bookingOne.id,
              }}
              onSubmit={function(values){
                if(values.id_payment == 2) {navigation.navigate('PembayaranBooking', {
                  bookingOne: bookingOne,
                })}
                else {dispatch(updatePayChannel(values));}
              }}
            >
                {
                  ({handleSubmit, handleBlur, handleChange, setFieldValue, values}) => (
                    <View>
                      {
                        booking.paymentGetSuccess.data ?
                          booking.paymentGetSuccess.data.map((item, key) => (
                            <List.Item 
                              key={key}
                              title={item.payment_channel_name}
                              onPress={() => setFieldValue('id_payment', item.id)}
                              left={() => <RadioButton
                                color={Colors.red}
                                value={item.id}
                                status={ values.id_payment === item.id ? 'checked' : 'unchecked' }
                                onPress={() => setFieldValue('id_payment', item.id)}
                              />}
                            />
                          )) : null
                      }
                      <Button onPress={handleSubmit} icon="check" mode="contained">
                        Pilih Metode Pembayaran
                      </Button>
                    </View>
                  )
                }
            </Formik>
            {/* <Paragraph>Nama Bank : {operator_kapal_nama_bank}</Paragraph>
            <Paragraph>
              No. Rekening : {operator_kapal_nomor_rekening}
            </Paragraph>
            <Paragraph>Atas Nama : {operator_kapal_atas_nama}</Paragraph>
            <FileInput
              title={file_gambar.name ? file_gambar.name : "Bukti pembayaran"}
              getValue={(value: any) => {
                setFileGambar({
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                });
              }}
            />
            <Space height={10} /> */}
          </Card.Content>
        </Card>
      )}
      {/* <-- End Form input bukti pembayaran --> */}

      {/* <-- Form Konfirmasi pembayaran --> */}
      {isOperator && isConfirmPayment && (
        <Card style={{ elevation: 10 }}>
          <Card.Content>
            {containerIsSuccess && numContainer && (
              <View>
                <Subheading>Konfirmasi Pembayaran : </Subheading>
                <Divider />
                <View style={{ flexDirection: "row", padding: 5 }}>
                  <View style={{ flex: 1 }}>
                    <Button
                      onPress={onPaymentUnConfirmed}
                      mode="outlined"
                      color={Colors.danger}
                      icon="close"
                    >
                      Tolak
                    </Button>
                  </View>
                  <View style={{ flex: 1 }}>
                    <Button
                      onPress={onPaymentConfirmed}
                      mode="contained"
                      color={Colors.accepted}
                      icon="check"
                    >
                      Terima
                    </Button>
                  </View>
                </View>
              </View>
            )}
            {containerIsSuccess && !numContainer && (
              <View>
                <Caption
                  style={{
                    padding: 5,
                    borderWidth: 2,
                    textAlign: "center",
                    borderColor: Colors.sec,
                  }}
                >
                  Silahkan input nomor kontainer
                </Caption>
                <Button
                  onPress={onPaymentUnConfirmed}
                  mode="contained"
                  color={Colors.danger}
                  icon="close"
                >
                  Tolak
                </Button>
              </View>
            )}
          </Card.Content>
        </Card>
      )}
      {/* <-- End Form Konfirmasi pembayaran --> */}

      {/* <-- Form Input Pengambilan barang --> */}
      {isShipper && status === 5 && isNull(recipient_shipper_ktp) && isNull(purchase.recipient_consignee_ktp) && (
          <Formik
            enableReinitialize
            initialValues={{
              id: id_booking,
              recipient_shipper_name: "",
              recipient_shipper_date: DateTimeFormat(new Date()),
              recipient_shipper_ktp: "",
              recipient_shipper_ktp_size: "",
            }}
            validationSchema={yup.object().shape({
              recipient_shipper_name: yup.string().required(),
              recipient_shipper_date: yup.string().required(),
              recipient_shipper_ktp: yup.string().required(),
              recipient_shipper_ktp_size: yup.number().max(512000).required(),
            })}
            onSubmit={function (val) {
              // console.log(val);
              handlePengambilanBarang(val);
            }}
          >
            {(formikProps) => {
              return (
                <Card elevation={10}>
                  <Card.Content>
                    <Subheading>Form Pengambilan Barang</Subheading>
                    <TextInput
                      label="Nama Pengambil"
                      onChangeText={formikProps.handleChange(
                        "recipient_shipper_name"
                      )}
                      onBlur={formikProps.handleBlur("recipient_shipper_name")}
                      error={
                        formikProps.errors.recipient_shipper_name &&
                        formikProps.touched
                          ? true
                          : false
                      }
                      value={formikProps.values.recipient_shipper_name}
                    />
                    <Subheading>Tanggal Pengambilan Barang</Subheading>
                    <DateTime
                      label="Tanggal Pengambilan"
                      onChangeValue={(date: any) =>
                        formikProps.setFieldValue(
                          "recipient_shipper_date",
                          DateTimeFormat(date)
                        )
                      }
                    />
                    {formikProps.errors.recipient_shipper_date && (
                      <HelperText>isi tanggal</HelperText>
                    )}
                    <Space height={10} />
                    <FileInput
                      title="KTP"
                      placeholder={
                        formikProps.values.recipient_shipper_ktp
                          ? formikProps.values.recipient_shipper_ktp.name
                          : "Upload File siup"
                      }
                      error={formikProps.errors.recipient_shipper_ktp}
                      getValue={function (value) {
                        formikProps.setFieldValue("recipient_shipper_ktp", {
                          uri: value.uri,
                          type: value.type,
                          name: value.fileName,
                        });
                        formikProps.setFieldValue(
                          "recipient_shipper_ktp_size",
                          value.fileSize
                        );
                      }}
                    />
                    {formikProps.errors.recipient_shipper_ktp_size && (
                      <HelperText style={{ color: "red" }}>
                        file max 512 kb
                      </HelperText>
                    )}

                    <Space height={10} />
                    <Button
                      onPress={formikProps.handleSubmit}
                      mode="contained"
                      color={Colors.accepted}
                      icon="check"
                    >
                      Update Pengambil Barang
                    </Button>
                  </Card.Content>
                </Card>
              );
            }}
          </Formik>
      )}
      {/* <--End Form Input Pengambilan barang --> */}
    </View>
  );
}

const customStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 35,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: Colors.pri,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: Colors.pri,
  stepStrokeUnFinishedColor: "#aaaaaa",
  separatorFinishedColor: Colors.pri,
  separatorUnFinishedColor: "#aaaaaa",
  stepIndicatorFinishedColor: Colors.pri,
  stepIndicatorUnFinishedColor: "#ffffff",
  stepIndicatorCurrentColor: "#ffffff",
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: Colors.pri,
  stepIndicatorLabelFinishedColor: "#ffffff",
  stepIndicatorLabelUnFinishedColor: "#aaaaaa",
  labelColor: "#999999",
  labelSize: 13,
  currentStepLabelColor: Colors.pri,
};

const getStepIndicatorIconConfig = ({
  position,
  stepStatus,
}: {
  position: number;
  stepStatus: string;
}) => {
  const iconConfig = {
    name: "feed",
    color: stepStatus === "finished" ? "#ffffff" : Colors.pri,
    size: 15,
  };
  switch (position) {
    case 0: {
      iconConfig.name = "hand-pointer";
      break;
    }
    case 1: {
      iconConfig.name = "edit";
      break;
    }
    case 2: {
      iconConfig.name = "check-circle";
      break;
    }
    case 3: {
      iconConfig.name = "money-bill";
      break;
    }
    case 4: {
      iconConfig.name = "money-bill";
      break;
    }
    case 5: {
      iconConfig.name = "check";
      break;
    }
    case 6: {
      iconConfig.name = "flag";
      break;
    }
    default: {
      break;
    }
  }
  return iconConfig;
};

const renderStepIndicator = (params: any) => (
  <Icon {...getStepIndicatorIconConfig(params)} />
);

const styles = StyleSheet.create({
  statusSecondary: { backgroundColor: Colors.sec, alignSelf: "flex-start" },
  statusSuccess: { backgroundColor: Colors.success, alignSelf: "flex-start" },
});
