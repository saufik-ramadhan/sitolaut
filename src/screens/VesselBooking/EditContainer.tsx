import React from 'react'
import { Alert, StyleSheet, Text, View, FlatList } from 'react-native'
import { Caption, Card, IconButton, List, Portal, Button, Modal, Title, ActivityIndicator, TextInput } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import Colors from '../../config/Colors';
import { containerFetchSub } from '../../redux/containerReducer';
import { detailcontainerAdd, detailcontainerDelete, detailcontainerFetchSub } from '../../redux/detailContainerReducer';
import { RootState } from '../../redux/rootReducer';
import updateBookingStatus from '../../redux/updatebookingStatus';
import { ShipperStackParams } from '../Navigator'
import {Formik} from 'formik'
import { SelectInput, Space } from '../../components';
import { jenisbarangFetchAll } from '../../redux/jenisbarangReducer';
import { jeniscommodityFetchSub } from '../../redux/jeniscommodityReducer';
import { packagingFetchAll } from '../../redux/packagingReducer';
import { onlyNumber } from '../../services/utils';

function FloatingAdd({onPress}) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={onPress}
    />
  );
}

export default function EditContainer({navigation, route}: ShipperStackParams<'EditContainer'>) {
  //-- State
  const { booking_id } = route.params;
  const dispatch = useDispatch();
  const [modal, setModal] = React.useState({visible: false, data: {}});
  const { container, detailContainer, jenisbarang, jeniscommodity, packaging } = useSelector((state: RootState) => state);

  //-- Method
  const showModal= () => setModal({visible: true, data:{}}) 
  const hideModal= () => setModal({visible: false, data:{}})
  const handleDeleteDetailContainer = (detail_id) => {
    updateBookingStatus(booking_id);
    dispatch(detailcontainerDelete({id: detail_id}));
    // alert(detail_id);
  }
  const fetchData = () => {
    dispatch(containerFetchSub("booking/" + booking_id));
    dispatch(detailcontainerFetchSub("get_booking/" + booking_id));
  }
  
  React.useEffect(() => {
    fetchData();
    // dispatch(jenisbarangFetchAll());
    // dispatch(packagingFetchAll());
  }, [detailContainer.detailContainerAddLoading])
  
  if(
    !container.containerGetSuccess.data ||
    !detailContainer.detailContainerGetSuccess.data ||
    !jenisbarang.jenisbarangGetSuccess.data
  ) return <View style={{flex: 1, alignItems:'center', justifyContent: 'space-around'}}><ActivityIndicator animating={true} color={Colors.pri} /></View>;
  
  return (
    <>
    <FlatList
      data={container.containerGetSuccess.data}
      renderItem={({item}) => (
        <Card>
          <Card.Title title={item.nama_container}/>
          <Card.Content>
            {
              detailContainer.detailContainerGetSuccess.data.map((detail, key) => {
                if(item.id !== detail.id_container) return null;
                return (
                  <List.Item title={detail.deskripsi} description={
                      detail.jenis_barang + " | " +
                      detail.nama_barang + " | " +
                      detail.jumlah_barang + " " +detail.kemasan + " | " +
                      detail.berat_total + "kg"
                    } 
                    descriptionNumberOfLines={3}
                    right={() => <IconButton icon="delete" onPress={
                        function(){
                          Alert.alert('Delete', "Anda yakin ?", [
                            {text: 'Ya', onPress:() => handleDeleteDetailContainer(detail.id)},
                            {text: 'Tidak', onPress:() => console.log('Batal')}
                          ])
                        }
                      }/>
                    }
                  />
                )
              })
            }
          </Card.Content>
        </Card>
      )}
      keyExtractor={(item, key) => String(key)}
    />

    <Portal>
      <Modal visible={modal.visible} onDismiss={hideModal}>
        <View style={{padding: 10, margin: 20, backgroundColor: 'white', borderRadius: 10}}>
          <Title>Tambah Muatan</Title>
          <Formik
            initialValues={{
              berat_total: "",
              deskripsi: "",
              id_container: "",
              jenis_barang_id: "",
              jumlah_barang: "",
              kemasan_id: "",
              nama_barang_id: "",
            }}
            onSubmit={(values) => {
              updateBookingStatus(booking_id);
              dispatch(detailcontainerAdd(values));
              hideModal();
            }}
          >
            {
              ({handleChange, handleSubmit, values, setFieldValue, touched, handleBlur}) => {
                return(
                  <>
                    {/** Pilih Container */}
                    <SelectInput label="Container" options={container.containerGetSuccess.data} objectKey="nama_container" onChangeValue={(val) => setFieldValue('id_container', val.id)}/>

                    {/** Pilih Jenis Barang */}
                    <SelectInput label="Jenis Barang" options={jenisbarang.jenisbarangGetSuccess.data} objectKey="jenis_barang" onChangeValue={(val) => {
                      setFieldValue('jenis_barang_id', val.id)
                      dispatch(jeniscommodityFetchSub('/jenisbarang/'+val.id));
                    }}/>

                    {/** Pilih Barang */}
                    <SelectInput label="Pilih Komoditi" options={jeniscommodity.jeniscommodityGetSuccess.data || []} objectKey="nama_barang" onChangeValue={(val) => setFieldValue('nama_barang_id', val.id)}/>

                    {/** Kemasan */}
                    <SelectInput label="Kemasan" options={packaging.packagingGetSuccess.data || []} objectKey="kemasan" onChangeValue={(val) => setFieldValue('kemasan_id', val.id)}/>

                    {/** Deskripsi */}
                    <TextInput label="Deskripsi" value={values.deskripsi} onChangeText={handleChange('deskripsi')} onBlur={handleBlur('deskripsi')} dense/>
                    
                    {/** Jumlah */}
                    <TextInput label="Jumlah Kemasan" value={values.jumlah_barang} keyboardType="numeric" onChangeText={function(val){
                      setFieldValue('jumlah_barang', onlyNumber(val));
                    }} onBlur={handleBlur('jumlah_barang')} dense/>

                    {/** Tonase */}
                    <TextInput label="Tonase" value={values.berat_total} keyboardType="numeric" onChangeText={function(val){setFieldValue('berat_total', onlyNumber(val));}} onBlur={handleBlur('berat_total')} dense/>
                    <Space height={10}/>
                    <Button icon="plus" mode="contained" onPress={handleSubmit}>
                      ADD
                    </Button>
                  </>
              )}
            }
          </Formik>
        </View>
      </Modal>
    </Portal>

    <FloatingAdd onPress={showModal}/>
    
    </>
  )
}

const styles = StyleSheet.create({})
