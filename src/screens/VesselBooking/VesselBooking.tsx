import React, { Fragment } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import {
  Card,
  Caption,
  Paragraph,
  Badge,
  Divider,
  IconButton,
  Button,
  FAB,
} from "react-native-paper";
import { getUserType, DateFormat2, TimeFormat } from "../../services/utils";
import Colors from "../../config/Colors";
import { Icon, Space, EmptyState, PlaceholderLoading } from "../../components";
import { OperatorStackProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import { bookingFetchPage } from "../../redux/bookingReducer";
import { RootState } from "../../redux/rootReducer";

function FloatingAdd({ navigation }: OperatorStackProps<"JadwalOperator">) {
  return (
    <FAB
      style={{
        position: "absolute",
        bottom: 10,
        right: 10,
        backgroundColor: Colors.pri,
      }}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateVesselBooking");
      }}
    />
  );
}
function FloatingFilter({ navigation }: OperatorStackProps<"JadwalOperator">) {
  return (
    <Button
      style={{
        position: "absolute",
        alignSelf: "center",
        bottom: 10,
        elevation: 10,
      }}
      icon="filter"
      mode="contained"
      color={Colors.white}
      onPress={function () {
        navigation.navigate("Modals", { screen: "SearchBookingModal" });
      }}
    >
      Filter
    </Button>
  );
}
export default function VesselBooking({ navigation }: any) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const bookingList = useSelector(function (state: RootState | any) {
    return state.booking.bookingGetSuccess
      ? state.booking.bookingGetSuccess.data
      : [{}];
  });
  function B({ children }: any) {
    return <Paragraph style={styles.bold}>{children}</Paragraph>;
  }

  const getLoading = useSelector(function (state: RootState | any) {
    return state.booking.bookingGetLoading;
  });
  const { isShipper, isOperator } = getUserType(usertype);
  const getList = () => {
    let params: any = { length: 10 };
    if (isShipper) params.shipper_id = id;
    else if (isOperator) params.operator_id = id;
    else params = { length: 10 };
    dispatch(bookingFetchPage(params));
  };
  // const [refresh, setRefresh] = React.useState(false);
  const bookingLoading = useSelector(function (state: RootState) {
    return state.booking.bookingAddLoading;
  });
  const containerLoading = useSelector(function (state: RootState) {
    return state.container.containerAddLoading;
  });
  const detailContainerLoading = useSelector(function (state: RootState) {
    return state.detailContainer.detailContainerAddLoading;
  });
  const onRefresh = () => {
    getList();
  };
  React.useEffect(() => {
    getList();
  }, [bookingLoading, containerLoading, detailContainerLoading]);
  return (
    <Fragment>
      <PlaceholderLoading loading={getLoading} />
      {!getLoading && (
        <FlatList
          data={bookingList}
          keyExtractor={(item, key) => String(key)}
          refreshing={bookingLoading}
          ListFooterComponent={<Space height={100}/>}
          onRefresh={onRefresh}
          renderItem={({ item }) => {
            const {
              kode_booking,
              kode_trayek,
              nama_pelabuhan_asal,
              nama_pelabuhan_tujuan,
              tanggal_berangkat,
              tanggal_tiba,
              request_kuota,
              type_container_name,
              status,
            }: any = item;
            return (
              <Card
                style={styles.card}
                onPress={() =>
                  navigation.navigate("ReadVesselBooking", { itemId: item.id })
                }
              >
                <B>{kode_booking}</B>
                {item.status == 1 && (
                  <Badge style={styles.statusSecondary}>
                    Menunggu validasi operator
                  </Badge>
                )}
                {item.status == 2 && item.bukti_pembayaran_file == null && (
                  <Badge style={styles.statusSecondary}>
                    Menunggu pembayaran
                  </Badge>
                )}
                {item.status == 2 && item.bukti_pembayaran_file != null && (
                  <Badge style={styles.statusSecondary}>
                    Menunggu konfirmasi pembayaran
                  </Badge>
                )}
                {item.status == 3 && (
                  <Badge style={styles.statusSecondary}>
                    Pembayaran diterima
                  </Badge>
                )}
                {item.status == 4 && (
                  <Badge
                    style={{
                      backgroundColor: Colors.danger,
                      alignSelf: "flex-start",
                    }}
                  >
                    Dibatalkan
                  </Badge>
                )}
                {item.status == 5 && (
                  <Badge style={styles.statusSuccess}>Selesai</Badge>
                )}
                {item.status == 6 && (
                  <Badge style={styles.statusSuccess}>
                    Sudah sampai tujuan
                  </Badge>
                )}
                <Space height={5} />
                <Divider />
                <View style={{ flexDirection: "row" }}>
                  {/** Rute */}
                  <View style={{ flexDirection: "row", flex: 2 }}>
                    <View style={{ flex: 1 }}>
                      <B>
                        {nama_pelabuhan_asal}
                        {"\n"}
                        <Caption>
                          {DateFormat2(tanggal_berangkat)}
                          {"\n"}
                          {TimeFormat(tanggal_berangkat)}
                        </Caption>
                      </B>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
                    </View>
                    <View style={{ flex: 1, alignItems: "flex-end" }}>
                      <B>
                        {nama_pelabuhan_tujuan}
                        {"\n"}
                        <Caption>
                          {DateFormat2(tanggal_tiba)}
                          {"\n"}
                          {TimeFormat(tanggal_tiba)}
                        </Caption>
                      </B>
                    </View>
                  </View>

                  {/** Trayek & Voyage */}
                  <View
                    style={{
                      flex: 1,
                      alignItems: "flex-end",
                      justifyContent: "center",
                    }}
                  >
                    <Paragraph>
                      <B>Trayek</B> {kode_trayek} {"\n"}
                      <B>Voyage</B> 4
                    </Paragraph>
                  </View>
                </View>
                <Divider />
                {/** Kapal */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    name={"buffer"}
                    style={{ fontSize: 20, color: Colors.pending }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    Qty : {request_kuota} container {type_container_name}
                  </Paragraph>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    name={"buffer"}
                    style={{ fontSize: 20, color: Colors.pending }}
                  />

                  {item.is_fcl == 0 && (
                    <Paragraph>LCL (Less Container Loaded)</Paragraph>
                  )}
                  {item.is_fcl == 1 && (
                    <Paragraph>FCL (Full Container Loaded)</Paragraph>
                  )}
                </View>
              </Card>
            );
          }}
          ListEmptyComponent={EmptyState}
        />
      )}
      {usertype == "Shipper" && <FloatingAdd route="" navigation={navigation} />}
      <FloatingFilter route="" navigation={navigation} />
    </Fragment>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  modalContainer: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 3,
    elevation: 3,
    margin: 10,
  },
  bold: {
    fontWeight: "bold",
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  ctrlButton: {
    flex: 1,
    borderRadius: 3,
  },
  statusSecondary: { backgroundColor: Colors.sec, alignSelf: "flex-start" },
  statusSuccess: { backgroundColor: Colors.success, alignSelf: "flex-start" },
});
