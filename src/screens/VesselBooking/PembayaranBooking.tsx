import { Formik } from 'formik';
import React from 'react'
import { FlatList, StyleSheet, Text, View, Image } from 'react-native'
import { Button, Caption, Card, List } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { FileInput } from '../../components';
import { operatorFetchUser } from '../../redux/operatorReducer';
import { RootState } from '../../redux/rootReducer';
import { ShipperStackProps } from '../Navigator'
import * as Yup from 'yup';
import { bookingPayment } from '../../redux/bookingReducer';

const Schema = Yup.object({
  gambar:Yup.string().required().label('Gambar'),
  id:Yup.string().required().label('ID'),
  operator_id:Yup.string().required().label('Operator ID'),
  bukti_pembayaran_file:Yup.string().required().label('Bukti Pembayaran'),
})
export default function PembayaranBooking({navigation, route}: ShipperStackProps<'PembayaranBooking'>) {
  const booking = route.params.bookingOne;
  const dispatch = useDispatch();
  const [file_gambar, setFileGambar] = React.useState("");
  const { operator } = useSelector((state: RootState) => state);
  React.useEffect(() => {
    dispatch(operatorFetchUser(booking.operator_kapal_user_id));
    console.log(booking);
    // alert(booking.operator_kapal_user_id);
  }, [])
  if(!operator.operatorViewSuccess.data) return null;
  const data_operator = operator.operatorViewSuccess.data[0];
  return (
    <>
    <FlatList
      data={data_operator.rekening}
      renderItem={({item}) => (
        <Card elevation={5} style={{margin: 10, borderRadius: 5}}>
          <List.Item title={item.bank} description="Bank"/>
          <List.Item title={item.nomor} description="Nomor Rekening"/>
          <List.Item title={item.atas_nama} description="Atas Nama"/>
        </Card>
      )}
      keyExtractor={(item, key) => String(key)}
    />
    
    <View style={{position: 'absolute', bottom: 30, margin: 10, alignSelf: 'center' }}>
      <Formik
        validationSchema={Schema}
        initialValues={{
          gambar: "",
          id: String(booking.id),
          operator_id: String(booking.operator_kapal_id),
          bukti_pembayaran_file: "",
        }}
        onSubmit={(val) => dispatch(bookingPayment(val))}
      >
        {({handleSubmit, handleChange, setFieldValue, values, errors}) => (
          <>
            <Image source={{uri: values.gambar}} style={{width: 200, height: 200}} resizeMode="contain"/>
            <FileInput
              title={file_gambar.name ? file_gambar.name : "Bukti pembayaran"}
              getValue={(value: any) => {
                setFieldValue('gambar', value.uri);
                setFieldValue('bukti_pembayaran_file', {
                  uri: value.uri,
                  type: value.type,
                  name: value.fileName,
                })
              }}
            />
            <Button onPress={handleSubmit}>Upload Bukti Pembayaran</Button>
          </>
        )}
      </Formik>
    </View>
    </>
  )
}

const styles = StyleSheet.create({})
