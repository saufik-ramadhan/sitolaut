import React from "react";
import { View, Text, FlatList } from "react-native";
import { bookingFetchOne } from "../../redux/bookingReducer";
import { containerFetchSub } from "../../redux/containerReducer";
import {
  detailcontainerFetchSub,
  detailcontainerDelete,
  detailcontainerAdd,
} from "../../redux/detailContainerReducer";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import { jeniscommodityFetchSub } from "../../redux/jeniscommodityReducer";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { packagingFetchAll } from "../../redux/packagingReducer";
import {
  Card,
  Divider,
  Paragraph,
  IconButton,
  ActivityIndicator,
  List,
  Title,
  TextInput,
  Button,
  FAB,
  Portal,
} from "react-native-paper";
import {
  EmptyState,
  PlaceholderLoading,
  Space,
  Icon,
  PickerCostum,
} from "../../components";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as yup from "yup";

export default function FormPackingList(props: any) {
  const dispatch = useDispatch();
  const { route, navigation } = props;
  const { itemId } = route.params;
  // console.log(itemId)

  const containers = useSelector(function (state: RootState | any) {
    return state.container.containerGetSuccess
      ? state.container.containerGetSuccess.data
      : [{}];
  });

  const containerIsSuccess = useSelector(function (state: RootState | any) {
    return state.container.containerGetSuccess.success;
  });

  const detailcontainer = useSelector(function (state: RootState | any) {
    return state.detailContainer.detailContainerGetSuccess
      ? state.detailContainer.detailContainerGetSuccess.data
      : [{}];
  });

  const detailcontainerLoading = useSelector(function (state: RootState) {
    return state.detailContainer.detailContainerGetLoading;
  });

  const detailcontainerAddIsSuccess = useSelector(function (
    state: RootState | any
  ) {
    return state.detailContainer.detailContainerAddSuccess
      ? state.detailContainer.detailContainerAddSuccess
      : [{}];
  });
  const detailcontainerGetIsSuccess = useSelector(function (
    state: RootState | any
  ) {
    return state.detailContainer.detailContainerGetSuccess
      ? state.detailContainer.detailContainerGetSuccess
      : [{}];
  });

  const detailcontainerDeleteIsSuccess = useSelector(function (
    state: RootState | any
  ) {
    return state.detailContainer.detailContainerDeleteSuccess
      ? state.detailContainer.detailContainerDeleteSuccess
      : [{}];
  });

  const fetchInitData = () => {
    dispatch(bookingFetchOne(itemId));
    dispatch(containerFetchSub("booking/" + itemId));
    dispatch(detailcontainerFetchSub("get_booking/" + itemId));
    dispatch(jenisbarangFetchAll());
    dispatch(packagingFetchAll());
  };

  const onJenisBarangChange = (values: any) => {
    let id = values.id;
    if (id != "") {
      dispatch(jeniscommodityFetchSub("/jenisbarang/" + id));
    }
  };

  const handleSubmit = (values: any) => {
    console.log(values);
    dispatch(detailcontainerAdd(values));
  };

  const deleteBarang = (id: any) => {
    dispatch(detailcontainerDelete({ id: id }));
  };

  const gotoAdd = () => {
    navigation.navigate("Modals", {
      screen: "FormAddPackingList",
      params: { itemId },
    });
  };

  React.useEffect(() => {
    fetchInitData();
  }, []);

  React.useEffect(() => {
    if (detailcontainerAddIsSuccess.success) {
      dispatch(detailcontainerFetchSub("get_booking/" + itemId));
    }
    if (detailcontainerDeleteIsSuccess.success) {
      dispatch(detailcontainerFetchSub("get_booking/" + itemId));
    }
  }, [
    detailcontainerAddIsSuccess.success,
    detailcontainerDeleteIsSuccess.success,
  ]);
  const isshowForm = containerIsSuccess && detailcontainerGetIsSuccess;
  return (
    <View style={{ backgroundColor: "#eee", flex: 1 }}>
      {isshowForm ? (
        <FlatList
          data={containers}
          keyExtractor={(item, key) => String(key)}
          renderItem={({ item }) => {
            return (
              <Card>
                <List.Accordion
                  title={
                    <Title>
                      {" "}
                      <Icon name="toolbox-outline" size={25} />{" "}
                      {item.nama_container}
                    </Title>
                  }
                >
                  <Divider />
                  <FlatList
                    data={detailcontainer.filter(
                      (detail) => detail.id_container == item.id
                    )}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({ item }) => {
                      return detailcontainerLoading ? (
                        <View style={{ flex: 1, justifyContent: "center" }}>
                          <ActivityIndicator size="large" />
                        </View>
                      ) : (
                        <Card style={{ padding: 10, margin: 10 }}>
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                            }}
                          >
                            <View>
                              <Paragraph>
                                Jenis Barang : {item.jenis_barang}{" "}
                              </Paragraph>
                              <Paragraph>
                                Nama Barang : {item.nama_barang}
                              </Paragraph>
                              <Paragraph>
                                Deskripsi : {item.deskripsi}
                              </Paragraph>
                              <Paragraph>Kemasan : {item.packaging} </Paragraph>
                              <Paragraph>
                                Jumlah Kemasan : {item.jumlah_barang}
                              </Paragraph>
                              <Paragraph>
                                Berat ({item.packaging}) : {item.berat_total}
                              </Paragraph>
                            </View>
                            <IconButton
                              style={{
                                alignSelf: "flex-end",
                                backgroundColor: Colors.danger,
                                elevation: 5,
                                borderRadius: 5,
                              }}
                              size={30}
                              icon="delete-forever"
                              color={Colors.white}
                              onPress={() => deleteBarang(item.id)}
                            />
                          </View>
                          <Divider />
                        </Card>
                      );
                    }}
                    ListEmptyComponent={EmptyState}
                  />
                </List.Accordion>
              </Card>
            );
          }}
        />
      ) : (
        <PlaceholderLoading loading={true} />
      )}
      <Button
        mode="contained"
        onPress={() =>
          navigation.navigate("ReadVesselBooking", {
            itemId,
          })
        }
      >
        Selesai
      </Button>
      <FAB
        style={{ position: "absolute", bottom: 50, right: 10 }}
        icon="plus"
        onPress={gotoAdd}
      />
    </View>
  );
}
