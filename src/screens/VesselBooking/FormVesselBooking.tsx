import React, { useState } from "react";
import { View, StyleSheet, Alert, ImageBackground, Image } from "react-native";
import StepIndicator from "react-native-step-indicator";
import {
  Card,
  Caption,
  List,
  Badge,
  Paragraph,
  Divider,
  Subheading,
  HelperText,
  Avatar,
  ActivityIndicator,
  TextInput,
  Checkbox,
  Button,
  Chip,
  IconButton,
} from "react-native-paper";
import Colors from "../../config/Colors";
import { FontAwesome5 as Icon } from "@expo/vector-icons";
import {
  Space,
  Root,
  PlaceholderLoading,
  PickerCostum,
} from "../../components";
import {
  DateFormat,
  formatRupiah,
  getUserType,
  DateTimeFormat,
} from "../../services/utils";
import { bookingFetchOne, bookingAdd } from "../../redux/bookingReducer";
import { useDispatch, useSelector } from "react-redux";
import { containerFetchSub } from "../../redux/containerReducer";
import { detailcontainerFetchSub } from "../../redux/detailContainerReducer";
import { IP_URL, UPLOAD_URL } from "../../config/constants";
import { RootState } from "../../redux/rootReducer";
import { scheduleFetchOne } from "../../redux/scheduleReducer";
import { consigneeFetchAll } from "../../redux/consigneeReducer";
import { shipperFetchSub } from "../../redux/shipperReducer";
import { Picker } from "@react-native-community/picker";
import { purchaseFetchBooking } from "../../redux/purchaseReducer";
const imgDummy = require("../../assets/img/imgDummy.png");
import _, { values } from "lodash";
import { Formik, useFormik } from "formik";
import * as yup from "yup";

export default function FormVesselBooking(props: any) {
  const dispatch = useDispatch();
  const [, updateState] = React.useState();
  const [selectedContainer, setSelectedContainer]: any = useState({
    alokasi_quota: 0,
    alokasi_quota_id: 0,
    id: 0,
    price: 0,
    schedule_rates_id: 0,
    sisa_quota: 0,
    type_container_name: "",
  });
  const [selectedType, setSelectedType]: any = useState({
    isConsigneeMultiple: false,
    purchase: [],
    selectedPurchases: [],
    consignee_id: "",
    npwp: "",
    nama_pic: "",
    telp: "",
    alamat: "",
  });
  const [consignee, setConsignee]: any = useState({
    consignee_id: "",
    npwp: "",
    nama_pic: "",
    telp_pic: "",
    alamat: "",
  });
  const [is_fcl, setIs_Fcl] = useState(0);
  const [checked, setChecked] = useState(false);
  const [title, setTitle] = useState("Pilih Purchase Order");
  const { route, navigation } = props;
  const id_jadwal = route.params.id_jadwal;

  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const JadwalView = useSelector(function (state: RootState | any) {
    return state.schedule.scheduleViewSuccess
      ? state.schedule.scheduleViewSuccess.data
      : [{}];
  });

  const consigneeAll = useSelector(function (state: RootState | any) {
    return state.consignee.consigneeGetSuccess
      ? state.consignee.consigneeGetSuccess.data
      : [{}];
  });

  const shipperView = useSelector(function (state: RootState | any) {
    return state.shipper.shipperGetSuccess
      ? state.shipper.shipperGetSuccess.data[0]
      : [{}];
  });

  const Purchasers = useSelector(function (state: RootState | any) {
    return state.purchase.purchaseGetSuccess
      ? state.purchase.purchaseGetSuccess.data
      : [{}];
  });

  const PurchasersSuccess = useSelector(function (state: RootState | any) {
    return state.purchase.purchaseGetSuccess.success;
  });

  const JadwalViewSuccess = useSelector(function (state: RootState | any) {
    return state.schedule.scheduleViewSuccess;
  });
  const consigneeSuccess = useSelector(function (state: RootState | any) {
    return state.consignee.consigneeGetSuccess;
  });

  const shipperSuccess = useSelector(function (state: RootState | any) {
    return state.shipper.shipperGetSuccess.success;
  });
  const ViewLoading = useSelector(function (state: RootState | any) {
    return state.booking.bookingViewLoading;
  });

  const BookingAddSuccess = useSelector(function (state: RootState | any) {
    return state.booking.bookingAddSuccess
      ? state.booking.bookingAddSuccess
      : {};
  });
  const BookingAddLoading = useSelector(function (state: RootState | any) {
    return state.booking.bookingAddLoading;
  });

  const fetchData = () => {
    dispatch(consigneeFetchAll());
    dispatch(shipperFetchSub("user/" + id));
    dispatch(scheduleFetchOne(id_jadwal));
  };
  React.useEffect(() => {
    fetchData();
  }, []);

  let logo_vendor = UPLOAD_URL + JadwalView.operator_logo;

  const fclOption = [
    { value: 0, label: "LCL (Less Container Loaded)" },
    { value: 1, label: "FCL (Full Container Loaded)" },
  ];

  const selectFcl = (opt) => {
    setIs_Fcl(opt.value);
    if (opt.value === 0) {
      setSelectedType({
        isConsigneeMultiple: true,
        purchase: [],
        selectedPurchases: [],
        consignee_id: "",
        npwp: "",
        nama_pic: "",
        telp: "",
        alamat: "",
      });

      const data = {
        shipper_id: id,
      };
      dispatch(purchaseFetchBooking(data));
    } else {
      setSelectedType({
        isConsigneeMultiple: false,
        purchase: [],
        selectedPurchases: [],
      });
    }
  };

  const handleAgree = () => {
    setChecked(!checked);
  };

  const changeConsignee = (item) => {
    const data = {
      shipper_id: id,
      consignee_id: item.id,
    };
    dispatch(purchaseFetchBooking(data));
    setConsignee({
      consignee_id: item.id,
      npwp: item.npwp,
      nama_pic: item.nama_pic,
      telp_pic: item.telp_pic,
      alamat: item.alamat,
    });
  };

  const changePurchase = (item) => {
    if (selectedType.isConsigneeMultiple) {
      try {
        setSelectedType({
          ...selectedType,
          selectedPurchases: _.concat(selectedType.selectedPurchases, item),
        });
      } catch (error) {
        console.log(error);
      }
    } else {
      setSelectedType({
        ...selectedType,
        selectedPurchases: new Array(item),
      });
    }
  };
  const formik = useFormik({
    initialValues: {
      consignee_id: "",
      is_fcl: "",
      request_quota: "",
      deskripsi: "",
      permintaan_khusus: "",
      type_container_id: "",
    },
    onSubmit: (values: any) => {
      if (shipperSuccess) {
        values.shipper_id = id;
      }

      if (JadwalViewSuccess) {
        values.operator_id = JadwalView.operator_id;
      }

      if (values.is_fcl === 1) {
        if (!values.consignee_id) {
          Alert.alert("Perhatian", "Silahkan Pilih consignee");
          return;
        }
      }
      let purchases = selectedType.selectedPurchases;
      if (purchases.length === 0) {
        Alert.alert("Perhatian", "Silahkan Pilih Purchase");
        return;
      }
      let dataPurchases: any = [];
      purchases.forEach((row: any) => {
        let data = {
          purchase_order_id: row.value,
        };
        dataPurchases.push(data);
      });

      let requestQuota = values.request_quota;
      values.packing_list = [];
      for (let index = 0; index < requestQuota; index++) {
        values.packing_list.push([]);
      }

      values.purchase_order = dataPurchases;
      values.jadwal_id = id_jadwal;
      values.quota_id = selectedContainer.alokasi_quota_id;
      console.log(values);
      //  navigation.navigate("FormPackingList", { itemId: 8 });
      dispatch(bookingAdd(values));
    },
    validationSchema: yup.object().shape({
      is_fcl: yup.number().required("*harus diisi"),
      request_quota: yup.number().min(1).required("*harus diisi"),
      deskripsi: yup.string().required("*harus diisi"),
      permintaan_khusus: yup.string().required("*harus diisi"),
      type_container_id: yup.number().required("*harus diisi"),
    }),
  });

  const { isConsigneeMultiple } = selectedType;
  React.useEffect(() => {
    if (PurchasersSuccess) {
      if (Purchasers !== null) {
        const purchase = Purchasers;
        purchase.forEach((row) => {
          let option = {
            value: row.id,
            label: `${row.nama_perusahaan} ${row.no_po} `,
          };
          setSelectedType((prevState) => ({
            ...selectedType,
            purchase: prevState.purchase.concat(option),
          }));
        });
      }
    }
  }, [PurchasersSuccess]);

  React.useEffect(() => {
    if (BookingAddSuccess.success) {
      navigation.navigate("FormPackingList", {
        itemId: BookingAddSuccess.data.id,
      });
      BookingAddSuccess.data.id;
    }
  }, [BookingAddSuccess.success]);

  const renderConsignee = () => {
    if (consigneeSuccess) {
      if (is_fcl) {
        return (
          <>
            <List.Item title={<Subheading>Pilih Consignee </Subheading>} />
            <Card>
              <Card.Content>
                <PickerCostum
                  data={consigneeSuccess ? consigneeAll : []}
                  initLabel="--Pilih Consignee--"
                  objectKey="label"
                  onValueChange={(value) => {
                    changeConsignee(value);
                    formik.setFieldValue("consignee_id", value.id);
                  }}
                />
                {consignee.npwp !== "" && (
                  <>
                    <List.Item
                      title={<Caption>NPWP</Caption>}
                      right={() => <Paragraph>{consignee.npwp}</Paragraph>}
                    />
                    <List.Item
                      title={<Caption>Alamat</Caption>}
                      right={() => <Paragraph>{consignee.alamat}</Paragraph>}
                    />
                    <List.Item title={<Subheading>PIC Consignee</Subheading>} />
                    <Card>
                      <Divider />
                      <List.Item
                        title={<Caption>Nama PIC</Caption>}
                        right={() => (
                          <Paragraph>{consignee.nama_pic}</Paragraph>
                        )}
                      />
                      <Divider />
                      <List.Item
                        title={<Caption>No. Telpon</Caption>}
                        right={() => (
                          <Paragraph>{consignee.telp_pic}</Paragraph>
                        )}
                      />
                      <Divider />
                    </Card>
                  </>
                )}
              </Card.Content>
            </Card>
          </>
        );
      }
    }
  };
  const renderPurchase = () => {
    if (PurchasersSuccess) {
      if (isConsigneeMultiple || consignee.consignee_id !== "") {
        return (
          <>
            <List.Item title={<Subheading>Pilih Purchase Order</Subheading>} />
            <Card>
              <Card.Content>
                <PickerCostum
                  data={_.difference(
                    selectedType.purchase,
                    selectedType.selectedPurchases
                  )}
                  initLabel={title}
                  objectKey="label"
                  onValueChange={(value) => {
                    changePurchase(value);
                  }}
                />
                <HelperText style={{ color: "red" }}>
                  {formik.errors.purchase}
                </HelperText>
              </Card.Content>
            </Card>
            <Divider />
            {selectedType.selectedPurchases.map((item: any, key: any) => (
              <Card key={key}>
                <List.Item
                  title={<Paragraph>{item.label}</Paragraph>}
                  right={() => (
                    <IconButton
                      onPress={function () {
                        const newArr: any = _.filter(
                          selectedType.selectedPurchases,
                          (cond: any) => cond.value != item.value
                        );
                        setSelectedType({
                          ...selectedType,
                          selectedPurchases: newArr,
                        });
                      }}
                      icon="close"
                      color={Colors.danger}
                    />
                  )}
                  left={() => <IconButton icon="chevron-right" />}
                />
                <Divider />
              </Card>
            ))}
            {selectedType.selectedPurchases.length > 1 && (
              <Button
                mode="contained"
                icon="close"
                onPress={function () {
                  setSelectedType({
                    ...selectedType,
                    selectedPurchases: [],
                  });
                }}
              >
                Remove all
              </Button>
            )}
          </>
        );
      }
    }
  };
  const isQuota = formik.values.request_quota > selectedContainer.sisa_quota;
  return (
    <View style={{ flex: 1 }}>
      <Root style={{ backgroundColor: "#eee" }}>
        {!ViewLoading && (
          <View>
            <Card style={{ paddingVertical: 5 }}>
              <StepIndicator
                stepCount={7}
                customStyles={customStyles}
                currentPosition={1}
                renderLabel={() => <View></View>}
                renderStepIndicator={renderStepIndicator}
              />
            </Card>
            <Card>
              <List.Item title={`Trayek | T88`} />
            </Card>
            <List.Item title={<Subheading>Profile Operator</Subheading>} />
            <Card>
              <Divider />
              <ImageBackground
                source={require("../../assets/img/pelabuhan.png")}
                style={{ flex: 1 }}
              >
                <Avatar.Image
                  source={{ uri: logo_vendor }}
                  style={{ alignSelf: "center" }}
                />
              </ImageBackground>
              <List.Item
                title={<Caption>Nama Operator</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView ? JadwalView.operator_nama_perusahaan : null}
                  </Paragraph>
                )}
              />
              <List.Item
                title={<Caption>No. Telepon</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView ? JadwalView.operator_telp : null}
                  </Paragraph>
                )}
              />
              <List.Item
                title={<Caption>Email</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView ? JadwalView.operator_email : null}
                  </Paragraph>
                )}
              />
              <List.Item
                title={<Caption>Alamat</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView ? JadwalView.operator_alamat : null}
                  </Paragraph>
                )}
              />
              <Divider />
            </Card>
            <List.Item title={<Subheading>Detail Waktu</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>ETD</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView
                      ? DateFormat(JadwalView.tanggal_berangkat)
                      : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>ETD</Caption>}
                right={() => (
                  <Paragraph>
                    {" "}
                    {JadwalView ? DateFormat(JadwalView.tanggal_tiba) : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>Tanggal Closing BUMN</Caption>}
                right={() => (
                  <Paragraph>
                    {" "}
                    {JadwalView
                      ? DateFormat(JadwalView.tutup_jadwal_shipper_bumn)
                      : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>Tanggal Closing Swasta</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView
                      ? DateFormat(JadwalView.tutup_jadwal_shipper_swasta)
                      : null}
                  </Paragraph>
                )}
              />
            </Card>
            <List.Item title="Lokasi Depo" />
            <Card>
              <List.Item
                title={<Caption>Nama Depot</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView ? JadwalView.nama_depot : null}
                  </Paragraph>
                )}
              />
              <List.Item
                title={<Caption>Alamat Depot</Caption>}
                right={() => (
                  <Paragraph>
                    {JadwalView ? JadwalView.alamat_depot : null}
                  </Paragraph>
                )}
              />
            </Card>
            <List.Item title={<Subheading>Pilih Tipe : </Subheading>} />
            <Card>
              <Card.Content>
                <PickerCostum
                  data={fclOption}
                  initLabel="--Pilih Tipe--"
                  objectKey="label"
                  onValueChange={(value: any) => {
                    selectFcl(value);
                    formik.setFieldValue("is_fcl", value.value);
                  }}
                />
                <HelperText style={{ color: "red" }}>
                  {formik.errors.is_fcl}
                </HelperText>
              </Card.Content>
            </Card>
            <List.Item title={<Subheading>Tipe Kontainer : </Subheading>} />
            <Card>
              <Card.Content>
                <PickerCostum
                  data={JadwalViewSuccess ? JadwalView.containers : []}
                  initLabel="--Pilih Tipe Kontainer--"
                  objectKey="type_container_name"
                  onValueChange={(value: any) => {
                    setSelectedContainer(value);
                    console.log(value);
                    formik.setFieldValue("type_container_id", value.id);
                  }}
                />
                <HelperText style={{ color: "red" }}>
                  {formik.errors.type_container_id}
                </HelperText>
                <List.Item
                  title={<Caption>Alokasi Kuota</Caption>}
                  right={() => (
                    <Paragraph>{selectedContainer.alokasi_quota}</Paragraph>
                  )}
                />
                <List.Item
                  title={<Caption>Sisa Kuota</Caption>}
                  right={() => (
                    <Paragraph>{selectedContainer.sisa_quota}</Paragraph>
                  )}
                />
                <List.Item
                  title={<Caption>Harga Kontainer</Caption>}
                  right={() => (
                    <Paragraph>
                      Rp. {formatRupiah(selectedContainer.price)}
                    </Paragraph>
                  )}
                />
              </Card.Content>
            </Card>

            {renderConsignee()}
            {renderPurchase()}

            <List.Item title={<Subheading>Data Shipper</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama Perusahaan</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.nama_perusahaan : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>NPWP</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.npwp : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>Alamat</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.alamat : null}
                  </Paragraph>
                )}
              />
            </Card>
            <List.Item title={<Subheading>PIC Shipper</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama PIC</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.nama_pic : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>No. Telpon</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.telp_pic : null}
                  </Paragraph>
                )}
              />
              <Divider />
            </Card>
            <List.Item title={<Subheading>Officer 1</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama Officer</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.nama_officer1 : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>No. Telepon</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.telp_officer1 : null}
                  </Paragraph>
                )}
              />
              <List.Item
                title={
                  <Caption>
                    {shipperSuccess ? shipperView.identify_officer1 : null}
                  </Caption>
                }
                right={() => {
                  return (
                    <Image
                      source={{
                        uri: UPLOAD_URL + shipperView.identify_officerdoc1,
                      }}
                      style={{ height: 100, width: 200, resizeMode: "contain" }}
                    />
                  );
                }}
              />
              <Divider />
            </Card>
            <List.Item title={<Subheading>Officer 2</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama Officer</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.nama_officer2 : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>No. Telepon</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.telp_officer2 : null}
                  </Paragraph>
                )}
              />
              <List.Item
                title={
                  <Caption>
                    {shipperSuccess ? shipperView.identify_officer2 : null}
                  </Caption>
                }
                right={() => {
                  return (
                    <Image
                      source={{
                        uri: UPLOAD_URL + shipperView.identify_officerdoc2,
                      }}
                      style={{ height: 100, width: 200, resizeMode: "contain" }}
                    />
                  );
                }}
              />
              <Divider />
            </Card>
            <List.Item title={<Subheading>Officer 3</Subheading>} />
            <Card>
              <Divider />
              <List.Item
                title={<Caption>Nama Officer</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.nama_officer3 : null}
                  </Paragraph>
                )}
              />
              <Divider />
              <List.Item
                title={<Caption>No. Telepon</Caption>}
                right={() => (
                  <Paragraph>
                    {shipperSuccess ? shipperView.telp_officer3 : null}
                  </Paragraph>
                )}
              />
              <List.Item
                title={
                  <Caption>
                    {shipperSuccess ? shipperView.identify_officer3 : null}
                  </Caption>
                }
                right={() => {
                  return (
                    <Image
                      source={{
                        uri: UPLOAD_URL + shipperView.identify_officerdoc3,
                      }}
                      style={{ height: 100, width: 200, resizeMode: "contain" }}
                    />
                  );
                }}
              />
              <Divider />
            </Card>
            <List.Item title={<Subheading>Detail Pemesanan</Subheading>} />
            {/* <Formik
              initialValues={{
                request_quota: "",
                deskripsi: "",
                permintaan_khusus: "",
              }}
              onSubmit={value => {
                handleSubmit(value)
              }}
            >
              {
                (formikProps) => {
                  return ( */}
            <>
              <Card>
                <Card.Content>
                  <TextInput
                    dense
                    mode="outlined"
                    keyboardType="number-pad"
                    label="Jumlah kuota yang dipesan"
                    onChangeText={formik.handleChange("request_quota")}
                    onBlur={formik.handleBlur("request_quota")}
                    error={
                      formik.errors.request_quota || isQuota ? true : false
                    }
                    value={formik.values.request_quota}
                  />
                  {isQuota && (
                    <HelperText style={{ color: "red" }}>
                      *Jumlah kuota yang tersisa :{" "}
                      {selectedContainer.sisa_quota}
                    </HelperText>
                  )}
                  <Space height={10} />
                  <TextInput
                    dense
                    multiline
                    mode="outlined"
                    label="Deskripsi"
                    numberOfLines={3}
                    onChangeText={formik.handleChange("deskripsi")}
                    onBlur={formik.handleBlur("deskripsi")}
                    error={formik.errors.deskripsi ? true : false}
                    value={formik.values.deskripsi}
                  />
                  <Space height={10} />
                  <TextInput
                    dense
                    multiline
                    mode="outlined"
                    numberOfLines={3}
                    label="Permintaan Khusus"
                    onChangeText={formik.handleChange("permintaan_khusus")}
                    onBlur={formik.handleBlur("permintaan_khusus")}
                    error={formik.errors.permintaan_khusus ? true : false}
                    value={formik.values.permintaan_khusus}
                  />
                </Card.Content>
              </Card>
              <View style={{ flexDirection: "row", maxWidth: "90%" }}>
                <Checkbox
                  status={checked ? "checked" : "unchecked"}
                  color={Colors.pri}
                  onPress={handleAgree}
                />
                <HelperText>
                  Saya menyatakan bahwa barang yang saya cantumkan pada saat
                  packing list sesuai dengan barang yang saya muat di container
                </HelperText>
              </View>
              <Button
                onPress={formik.handleSubmit}
                disabled={!formik.isValid || !checked}
                icon="send"
                mode="contained"
                loading={BookingAddLoading}
              >
                Booking
              </Button>
            </>
          </View>
        )}
        <PlaceholderLoading loading={ViewLoading} />
      </Root>
    </View>
  );
}

const customStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 35,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: Colors.pri,
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: Colors.pri,
  stepStrokeUnFinishedColor: "#aaaaaa",
  separatorFinishedColor: Colors.pri,
  separatorUnFinishedColor: "#aaaaaa",
  stepIndicatorFinishedColor: Colors.pri,
  stepIndicatorUnFinishedColor: "#ffffff",
  stepIndicatorCurrentColor: "#ffffff",
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: Colors.pri,
  stepIndicatorLabelFinishedColor: "#ffffff",
  stepIndicatorLabelUnFinishedColor: "#aaaaaa",
  labelColor: "#999999",
  labelSize: 13,
  currentStepLabelColor: Colors.pri,
};

const getStepIndicatorIconConfig = ({
  position,
  stepStatus,
}: {
  position: number;
  stepStatus: string;
}) => {
  const iconConfig = {
    name: "feed",
    color: stepStatus === "finished" ? "#ffffff" : Colors.pri,
    size: 15,
  };
  switch (position) {
    case 0: {
      iconConfig.name = "hand-pointer";
      break;
    }
    case 1: {
      iconConfig.name = "edit";
      break;
    }
    case 2: {
      iconConfig.name = "check-circle";
      break;
    }
    case 3: {
      iconConfig.name = "money-bill";
      break;
    }
    case 4: {
      iconConfig.name = "money-bill";
      break;
    }
    case 5: {
      iconConfig.name = "check";
      break;
    }
    case 6: {
      iconConfig.name = "flag";
      break;
    }
    default: {
      break;
    }
  }
  return iconConfig;
};

const renderStepIndicator = (params: any) => (
  <Icon {...getStepIndicatorIconConfig(params)} />
);

const styles = StyleSheet.create({
  statusSecondary: { backgroundColor: Colors.sec, alignSelf: "flex-start" },
  statusSuccess: { backgroundColor: Colors.success, alignSelf: "flex-start" },
});
