import {
  createStackNavigator,
  StackNavigationProp,
} from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { RouteProp } from "@react-navigation/native";

/** -------------------------
 * Root
 * Navigator Type Definition
 ------------------------- */
export type RootStackParams = {
  Auth: object | undefined;
  App: object | undefined;
};
export type RootStackProps<T extends keyof RootStackParams> = {
  navigation: StackNavigationProp<RootStackParams, T>;
  route: RouteProp<RootStackParams, T>;
};

/** -------------------------
 * Register Stack
 * Navigator Type Definition
 ------------------------- */
export type RegisterStackParams = {
  Register: object | undefined;
  RegisterConsignee: object | undefined;
  RegisterSupplier: object | undefined;
  RegisterShipper: object | undefined;
  RegisterReseller: object | undefined;
  RegisterOperator: object | undefined;
  RegisterSuccess: object | undefined;
};
export type RegisterStackProps<T extends keyof RegisterStackParams> = {
  navigation: StackNavigationProp<RegisterStackParams, T>;
  route: RouteProp<RegisterStackParams, T>;
};

/** -------------------------
 * Auth Stack
 * Navigator Type Definition
 ------------------------- */

export type AuthStackParams = {
  Login: object | undefined;
  Register: object | undefined;
  ForgotPassword: object | undefined;
};
export type AuthStackProps<T extends keyof AuthStackParams> = {
  navigation: StackNavigationProp<AuthStackParams, T>;
  route: RouteProp<AuthStackParams, T>;
};

/** -------------------------
 * App (User Type)
 * Navigator Type Definition
 ------------------------- */
export type AppStackParams = {
  Consignee: object | undefined;
  Supplier: object | undefined;
  Shipper: object | undefined;
  Reseller: object | undefined;
  Operator: object | undefined;
  Regulator: object | undefined;
  Visitor: object | undefined;
};
export type AppStackProps<T extends keyof AppStackParams> = {
  navigation: StackNavigationProp<AppStackParams, T>;
  route: RouteProp<AppStackParams, T>;
};

/** -------------------------
 * App Bottom Tab
 * Navigator Type Definition
 ------------------------- */
export type AppBottomTabParams = {
  Dashboard: object | undefined;
  Notification: object | undefined;
  Pengaduan: object | undefined;
  Profile: object | undefined;
};
export type AppBottomTabProps<T extends keyof AppBottomTabParams> = {
  navigation: StackNavigationProp<AppBottomTabParams, T>;
  route: RouteProp<AppBottomTabParams, T>;
};

/** -------------------------
 * Consignee
 * Navigator Type Definition
 ------------------------- */
export type ConsigneeStackParams = {
  Dashboard: object | undefined;
  BottomTab: object | undefined;
  Profile: object | undefined;
  UpdateProfile: object | undefined;
  ResetPassword: object | undefined;

  /**
   * Purchase Order
   * CRUD, Search, Filter, Track, Claim, Distribute
   */
  PurchaseOrder: object | undefined;

  /**
   * Create Purchase Order Flow Screens
   */
  CreatePurchaseOrder: object | undefined;
  ChooseSupplier: object | undefined;
  ChooseCommodity: object | undefined;
  CommoditySummary: object | undefined;
  ChooseShipper: object | undefined;
  ChooseTrayek: object | undefined;
  Checkout: object | undefined;
  /** -- */

  ReadPurchaseOrder: object | undefined;
  UpdatePurchaseOrder: object | undefined;
  DeletePurchaseOrder: object | undefined;
  SearchPurchaseOrder: object | undefined;
  FilterPurchaseOrder: object | undefined;
  TrackPurchaseOrder: object | undefined;
  TrackViewPurchaseOrder: object | undefined;
  ClaimPurchaseOrder: object | undefined;
  ReadClaimPurchaseOrder: object | undefined;
  DistributePurchaseOrder: object | undefined;
  ReadDistributePurchaseOrder: object | undefined;

  /**
   * Pengaduan
   * CRU-, Search, Filter
   */
  Pengaduan: object | undefined;
  CreatePengaduan: object | undefined;
  ReadPengaduan: object | undefined;
  UpdatePengaduan: object | undefined;
  SearchPengaduan: object | undefined;
  FilterPengaduan: object | undefined;

  /**
   * Jadwal
   * CRUD, Search, Filter
   */
  Jadwal: object | undefined;
  ReadJadwal: object | undefined;
  FilterJadwal: object | undefined;

  /**
   * Harga Jual Barang
   */
  HargaJualBarang: object | undefined;
  ReadHargaJualBarang: object | undefined;

  /**
   * Help
   */
  DownloadPanduan: object | undefined;

  /**
   * Vessel Geo Location
   */
  VesselGeoLocation: object | undefined;
};
export type ConsigneeStackProps<T extends keyof ConsigneeStackParams> = {
  navigation: StackNavigationProp<ConsigneeStackParams, T>;
  route: RouteProp<ConsigneeStackParams, T>;
};

/** -------------------------
 * Consignee 
 * - Create Purchase Order Stack
 * Navigator Type Definition
 ------------------------- */
export type CreatePurchaseOrderParams = {
  ChooseSupplier: object | undefined;
  ChooseCommodity: object | undefined;
  CommoditySummary: object | undefined;
  ChooseShipper: object | undefined;
  ChooseTrayek: object | undefined;
  Checkout: object | undefined;
};
export type CreatePurchaseOrderProps<
  T extends keyof CreatePurchaseOrderParams
  > = {
    navigation: StackNavigationProp<CreatePurchaseOrderParams, T>;
    route: RouteProp<CreatePurchaseOrderParams, T>;
  };

/** -------------------------
 * Supplier
 * Navigator Type Definition
 ------------------------- */
export type SupplierStackParams = {
  Dashboard: object | undefined;
  BottomTab: object | undefined;

  Profile: object | undefined;
  UpdateProfile: object | undefined;
  ResetPassword: object | undefined;

  /**
   * Purchase Order
   * -RU-, Search, Filter, Track
   */
  PurchaseOrder: object | undefined;
  ReadPurchaseOrder: object | undefined;
  UpdatePurchaseOrder: object | undefined;
  SearchPurchaseOrder: object | undefined;
  FilterPurchaseOrder: object | undefined;
  TrackPurchaseOrder: object | undefined;
  TrackViewPurchaseOrder: object | undefined;

  /**
   * Commodity
   * CRUD, Search, Filter
   */
  Commodity: object | undefined;
  CreateCommodity: object | undefined;
  ReadCommodity: object | undefined;
  UpdateCommodity: object | undefined;
  DeleteCommodity: object | undefined;
  SearchCommodity: object | undefined;
  FilterCommodity: object | undefined;

  /**
   * Pengaduan
   * CRU-, Search, Filter
   */
  Pengaduan: object | undefined;
  CreatePengaduan: object | undefined;
  ReadPengaduan: object | undefined;
  UpdatePengaduan: object | undefined;

  /**
   * Jadwal
   * CRUD, Search, Filter
   */
  Jadwal: object | undefined;
  ReadJadwal: object | undefined;
  FilterJadwal: object | undefined;

  /**
   * Help
   */
  DownloadPanduan: object | undefined;

  /**
   * Vessel Geo Location
   */
  VesselGeoLocation: object | undefined;
};
export type SupplierStackProps<T extends keyof SupplierStackParams> = {
  navigation: StackNavigationProp<SupplierStackParams, T>;
  route: RouteProp<SupplierStackParams, T>;
};

/** -------------------------
 * Shipper
 * Navigator Type Definition
 ------------------------- */
export type ShipperStackParams = {
  Dashboard: object | undefined;
  BottomTab: object | undefined;

  Profile: object | undefined;
  UpdateProfile: object | undefined;
  ResetPassword: object | undefined;

  /**
   * Purchase Order
   * -RU-, Search, Filter, Track
   */
  PurchaseOrder: object | undefined;
  ReadPurchaseOrder: object | undefined;
  UpdatePurchaseOrder: object | undefined;
  SearchPurchaseOrder: object | undefined;
  FilterPurchaseOrder: object | undefined;
  TrackPurchaseOrder: object | undefined;
  TrackViewPurchaseOrder: object | undefined;

  /**
   * Vessel Booking
   * CRUD, Search, Filter
   */
  VesselBooking: object | undefined;
  CreateVesselBooking: object | undefined;
  ReadVesselBooking: object | undefined;
  UpdateVesselBooking: object | undefined;
  DeleteVesselBooking: object | undefined;
  SearchVesselBooking: object | undefined;
  FilterVesselBooking: object | undefined;
  EditContainer: object | undefined;
  PembayaranBooking: object | undefined;

  BiayaPengurusan: object | undefined;
  UpdateBiayaPengurusan: object | undefined;
  AddBiayaPengurusan: object | undefined;
  FormVesselBooking: object | undefined;
  FormPackingList: object | undefined;

  /**
   * Pengaduan
   * C-U-, Search, Filter
   */
  Pengaduan: object | undefined;
  CreatePengaduan: object | undefined;
  UpdatePengaduan: object | undefined;

  /**
   * Help
   */
  DownloadPanduan: object | undefined;

  /**
   * Vessel Geo Location
   */
  VesselGeoLocation: object | undefined;
};
export type ShipperStackProps<T extends keyof ShipperStackParams> = {
  navigation: StackNavigationProp<ShipperStackParams, T>;
  route: RouteProp<ShipperStackParams, T>;
};

/** -------------------------
 * Reseller
 * Navigator Type Definition
------------------------- */
export type ResellerStackParams = {
  Dashboard: object | undefined;
  BottomTab: object | undefined;

  Profile: object | undefined;
  UpdateProfile: object | undefined;
  ResetPassword: object | undefined;

  /**
   * Good
   * -RU-, Search, Filter
   */
  Goods: object | undefined;
  ReadGoods: object | undefined;
  UpdateGoods: object | undefined;
  SearchGoods: object | undefined;
  FilterGoods: object | undefined;

  /**
   * Pengaduan
   * C-U-, Search, Filter
   */
  Pengaduan: object | undefined;
  CreatePengaduan: object | undefined;
  UpdatePengaduan: object | undefined;

  /**
   * Jadwal
   * CRUD, Search, Filter
   */
  Jadwal: object | undefined;
  ReadJadwal: object | undefined;
  FilterJadwal: object | undefined;

  /**
   * Help
   */
  DownloadPanduan: object | undefined;

  /**
   * Vessel Geo Location
   */
  VesselGeoLocation: object | undefined;
};
export type ResellerStackProps<T extends keyof ResellerStackParams> = {
  navigation: StackNavigationProp<ResellerStackParams, T>;
  route: RouteProp<ResellerStackParams, T>;
};
/** -------------------------
 * Operator
 * Navigator Type Definition
 ---------------------------- */
export type OperatorStackParams = {
  Dashboard: object | undefined;
  BottomTab: object | undefined;

  Profile: object | undefined;
  UpdateProfile: object | undefined;
  ResetPassword: object | undefined;

  /**
   * Trayek
   * CRUD, Search, Filter
   */
  Trayek: object | undefined;
  CreateTrayek: object | undefined;
  ReadTrayek: object | undefined;
  UpdateTrayek: object | undefined;
  DeleteTrayek: object | undefined;
  SearchTrayek: object | undefined;
  FilterTrayek: object | undefined;

  /**
   * Pelabuhan
   * CRUD, Search, Filter
   */
  Pelabuhan: object | undefined;
  CreatePelabuhan: object | undefined;
  ReadPelabuhan: object | undefined;
  UpdatePelabuhan: object | undefined;
  DeletePelabuhan: object | undefined;
  SearchPelabuhan: object | undefined;
  FilterPelabuhan: object | undefined;

  /**
   * Depot
   * CRUD, Search, Filter
   */
  Depot: object | undefined;
  CreateDepot: object | undefined;
  ReadDepot: object | undefined;
  UpdateDepot: object | undefined;
  DeleteDepot: object | undefined;
  SearchDepot: object | undefined;
  FilterDepot: object | undefined;

  /**
   * Jadwal
   * CRUD, Search, Filter
   */
  Jadwal: object | undefined;
  JadwalOperator: object | undefined;
  CreateJadwal: object | undefined;
  CreateJadwalOperator: object | undefined;
  ReadJadwal: object | undefined;
  UpdateJadwal: object | undefined;
  DeleteJadwal: object | undefined;
  SearchJadwal: object | undefined;
  FilterJadwal: object | undefined;
  ManifestJadwal: object | undefined;
  Manifest: object | undefined;

  /**
   * Kapal
   * CRUD, Search, Filter, Track
   */
  Kapal: object | undefined;
  // CreateKapal: object | undefined;
  ReadKapal: object | undefined;
  DeleteKapal: object | undefined;
  SearchKapal: object | undefined;
  FilterKapal: object | undefined;
  TrackKapal: object | undefined;
  TrackingData: object | undefined;
  TrackingDataFilter: object | undefined;

  /**
   * Pengaduan
   * C-U-, Search, Filter
   */
  Pengaduan: object | undefined;
  ReadPengaduan: object | undefined;
  CreatePengaduan: object | undefined;
  UpdatePengaduan: object | undefined;
  SearchPengaduan: object | undefined;
  FilterPengaduan: object | undefined;

  /**
   * Vessel Booking
   * CRUD, Search, Filter
   */
  VesselBooking: object | undefined;
  CreateVesselBooking: object | undefined;
  ReadVesselBooking: object | undefined;
  UpdateVesselBooking: object | undefined;
  DeleteVesselBooking: object | undefined;
  SearchVesselBooking: object | undefined;
  FilterVesselBooking: object | undefined;

  /**
   * List Operator
   */
  ListOperator: object | undefined;

  /**
   * Help
   */
  DownloadPanduan: object | undefined;

  /**
   * Vessel Geo Location
   */
  VesselGeoLocation: object | undefined;
};
export type OperatorStackProps<T extends keyof OperatorStackParams> = {
  navigation: StackNavigationProp<OperatorStackParams, T>;
  route: RouteProp<OperatorStackParams, T>;
};
/** -------------------------
 * Regulator
 * Navigator Type Definition
 ---------------------------- */
export type RegulatorStackParams = {
  Dashboard: object | undefined;
  BottomTab: object | undefined;

  /** Profile */
  Profile: object | undefined;
  UpdateProfile: object | undefined;
  ResetPassword: object | undefined;

  /**
   * DashboardRegulator
   */
  OrderMasuk: object | undefined;
  RealisasiMuatan: object | undefined;
  DisparitasHarga: object | undefined;
  DaftarSisaKuotaTrayek: object | undefined;
  TotalMuatanPerWilayah: object | undefined;
  MuatanPerWilayahPerPrioritas: object | undefined;
  RealisasiVoyage: object | undefined;
  WaktuTempuh: object | undefined;
  TotalOrderPerJenisPrioritas: object | undefined;
  LimaMuatanTerbanyak: object | undefined;
  TotalVoyagePerTrayek: object | undefined;
  TotalContainerPerPort: object | undefined;

  /**
   * Purchase Order
   * -R--, Search, Filter, Track
   */
  PurchaseOrder: object | undefined;
  ReadPurchaseOrder: object | undefined;
  SearchPurchaseOrder: object | undefined;
  FilterPurchaseOrder: object | undefined;
  TrackPurchaseOrder: object | undefined;
  TrackViewPurchaseOrder: object | undefined;

  /**
   * Commodity
   * CRUD, Search, Filter
   */
  Commodity: object | undefined;
  CreateCommodity: object | undefined;
  ReadCommodity: object | undefined;
  UpdateCommodity: object | undefined;
  DeleteCommodity: object | undefined;
  SearchCommodity: object | undefined;
  FilterCommodity: object | undefined;

  /**
   * Container
   * CRUD, Search, Filter
   */
  Container: object | undefined;
  CreateContainer: object | undefined;
  ReadContainer: object | undefined;
  UpdateContainer: object | undefined;
  DeleteContainer: object | undefined;

  /**
   * Kapal
   * -R--, Search, Filter, Track
   */
  Kapal: object | undefined;
  ReadKapal: object | undefined;
  CreateKapal: object | undefined;
  UpdateKapal: object | undefined;
  SearchKapal: object | undefined;
  FilterKapal: object | undefined;
  TrackKapal: object | undefined;


  /**
   * Trayek
   * -R--, Search, Filter
   */
  Trayek: object | undefined;
  ReadTrayek: object | undefined;
  SearchTrayek: object | undefined;
  FilterTrayek: object | undefined;

  /**
   * Pelabuhan
   * -R--, Search, Filter
   */
  Pelabuhan: object | undefined;
  ReadPelabuhan: object | undefined;
  CreatePelabuhan: object | undefined;
  UpdatePelabuhan: object | undefined;
  SearchPelabuhan: object | undefined;
  FilterPelabuhan: object | undefined;

  /**
   * Depot
   * -R--, Search, Filter
   */
  Depot: object | undefined;
  ReadDepot: object | undefined;
  SearchDepot: object | undefined;
  FilterDepot: object | undefined;

  /**
   * Jadwal
   * -R--, Search, Filter
   */
  Jadwal: object | undefined;
  ReadJadwal: object | undefined;
  SearchJadwal: object | undefined;
  FilterJadwal: object | undefined;
  ManifestJadwal: object | undefined;

  /**
   * Pengaduan
   * -RU-, Search, Filter
   */
  Pengaduan: object | undefined;
  ReadPengaduan: object | undefined;
  UpdatePengaduan: object | undefined;
  SearchPengaduan: object | undefined;
  FilterPengaduan: object | undefined;

  /**
   * Jenis Barang
   */
  JenisBarang: object | undefined;
  CreateJenisBarang: object | undefined;
  UpdateJenisBarang: object | undefined;

  /**
   * Jenis Commodity
   */
  JenisCommodity: object | undefined;
  JenisCommodityFilter: object | undefined;
  CreateJenisCommodity: object | undefined;
  UpdateJenisCommodity: object | undefined;

  /**
   * Barang Penting
   */
  BarangPenting: object | undefined;
  ReadBarangPenting: object | undefined;
  UpdateBarangPenting: object | undefined;
  CreateBarangPenting: object | undefined;
  FilterBarangpenting: object | undefined;

  /**
   * Kemasan
   */
  Kemasan: object | undefined;
  CreateKemasan: object | undefined;
  UpdateKemasan: object | undefined;

  /**
   * Satuan
   */
  Satuan: object | undefined;
  UpdateSatuan: object | undefined;
  CreateSatuan: object | undefined;

  /**
   * Vessel Geo Location
   */
  VesselGeoLocation: object | undefined;

  /**
   * Master Tarif
   */
  MasterTarif: object | undefined;
  CreateMasterTarif: object | undefined;
  UpdateMasterTarif: object | undefined;
  FilterMasterTarif: object | undefined;

  /**
   * Tipe Container
   */
  TipeContainer: object | undefined;
  CreateTipeContainer: object | undefined;
  UpdateTipeContainer: object | undefined;

  /**
   * Aktivasi User
   */
  AktivasiUser: object | undefined;
  ReadAktivasiUser: object | undefined;

  /**
   * User LCS
   */
  UserLCS: object | undefined;

  DaftarConsignee: object | undefined;
  DaftarSupplier: object | undefined;
  DaftarShipper: object | undefined;
  DaftarReseller: object | undefined;
  DaftarOperator: object | undefined;
  DaftarRegulator: object | undefined;

  ReadConsignee: object | undefined;
  ReadSupplier: object | undefined;
  ReadShipper: object | undefined;
  ReadReseller: object | undefined;
  ReadOperator: object | undefined;
  ReadRegulator: object | undefined;

  FilterDaftarConsignee: object | undefined;
  FilterDaftarSupplier: object | undefined;
  FilterDaftarShipper: object | undefined;
  FilterDaftarReseller: object | undefined;
  FilterDaftarOperator: object | undefined;
  FilterDaftarRegulator: object | undefined;

  /**
   * Manifest
   */
  Manifest: object | undefined;
  ReadManifest: object | undefined;

  /**
   * Report
   */
  Report: object | undefined;

  ReportJumlahUser: object | undefined;
  ReportStatusOrder: object | undefined;
  ReportOrderMasukPerBulan: object | undefined;
  ReportDataUser: object | undefined;
  ReportRealisasiMuatan: object | undefined;
  ReportDisparitasHarga: object | undefined;
  ReportDaftarSisaKuota: object | undefined;
  ReportTotalMuatanPerWilayah: object | undefined;
  ReportMuatanPerWilayahPerPrioritas: object | undefined;
  ReportRealisasiVoyagePerTrayek: object | undefined;
  ReportWaktuTempuh: object | undefined;
  ReportTotalOrderPerJenisPrioritas: object | undefined;
  ReportMuatanTerbanyakPerPrioritas: object | undefined;
  ReportTotalContainerPerPelabuhan: object | undefined;
  ReportMuatanTolLaut: object | undefined;
  DetailTrayekBalik: object | undefined;

  FilterJumlahUser: object | undefined;
  FilterStatusOrder: object | undefined;
  FilterOrderMasukPerBulan: object | undefined;
  FilterDataUser: object | undefined;
  FilterRealisasiMuatan: object | undefined;
  FilterDisparitasHarga: object | undefined;
  FilterDaftarSisaKuota: object | undefined;
  FilterTotalMuatanPerWilayah: object | undefined;
  FilterMuatanPerWilayahPerPrioritas: object | undefined;
  FilterRealisasiVoyagePerTrayek: object | undefined;
  FilterWaktuTempuh: object | undefined;
  FilterTotalOrderPerJenisPrioritas: object | undefined;
  FilterMuatanTerbanyakPerPrioritas: object | undefined;
  FilterTotalContainerPerPelabuhan: object | undefined;

  /**
   * Regulator
   */
  Regulator: object | undefined;
  CreateRegulator: object | undefined;

  /**
   * Log Aktivitas
   */
  LogAktivitas: object | undefined;
  LogAktivitasFilter: object | undefined;

  /**
   * Master
   */
  Master: object | undefined;

  MasterKodeTrayek: object | undefined;
  MasterKodeTrayekModal: object | undefined;

  MasterHarga: object | undefined;
  MasterHargaModal: object | undefined;

  MasterKontrak: object | undefined;
  MasterKontrakModal: object | undefined;
  DetailAddendum: object | undefined;

  /**
   * Guest
   */
  Guest: object | undefined;

  /**
   * Help
   */
  DownloadPanduan: object | undefined;
};
export type RegulatorStackProps<T extends keyof RegulatorStackParams> = {
  navigation: StackNavigationProp<RegulatorStackParams, T>;
  route: RouteProp<RegulatorStackParams, T>;
};

/**
 * Navigators Definition
 * Define all of application navigator
 * to be exported to src/index.tsx
 */
export const RootStack = createStackNavigator<RootStackParams>();
export const RegisterStack = createStackNavigator<RegisterStackParams>();
export const AuthStack = createStackNavigator<AuthStackParams>();
export const AppStack = createStackNavigator<AppStackParams>();
export const AppBottomTab = createBottomTabNavigator<AppBottomTabParams>();
export const AppDrawer = createDrawerNavigator();
export const ConsigneeStack = createStackNavigator<ConsigneeStackParams>();
export const CreatePurchaseOrderStack = createStackNavigator<
  CreatePurchaseOrderParams
>();
export const SupplierStack = createStackNavigator<SupplierStackParams>();
export const ShipperStack = createStackNavigator<ShipperStackParams>();
export const ResellerStack = createStackNavigator<ResellerStackParams>();
export const OperatorStack = createStackNavigator<OperatorStackParams>();
export const RegulatorStack = createStackNavigator<RegulatorStackParams>();
