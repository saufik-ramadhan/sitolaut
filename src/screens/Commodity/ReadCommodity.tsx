import React, { useEffect } from "react";
import { View, StyleSheet, ScrollView, Alert } from "react-native";
import {
  Card,
  List,
  Avatar,
  Paragraph,
  Caption,
  Divider,
  ActivityIndicator,
  Button,
} from "react-native-paper";
import Colors from "../../config/Colors";
import {
  commodityFetchOne,
  commodityDelete,
} from "../../redux/commodityReducer";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";
import { Root } from "../../components";
import { formatRupiah } from "../../services/utils";
import { UPLOAD_URL } from "../../config/constants";

function ReadCommodity({ navigation, route }) {
  const { id } = route.params;
  const commodity = useSelector(function (state: RootState) {
    return state.commodity.commodityViewSuccess
      ? state.commodity.commodityViewSuccess.data[0]
      : false;
  });
  const addSuccess = useSelector(function (state: RootState) {
    return state.commodity.commodityAddLoading;
  });
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(commodityFetchOne(id));
  }, [id, addSuccess]);

  return (
    <Root>
      {!commodity ? (
        <ActivityIndicator />
      ) : (
        <Card>
          <List.Section title=" Detail Commodity" />
          <Divider />
          <Card.Content>
            <List.Item
              title={<Caption>Foto Komoditi</Caption>}
              right={() => (
                <Avatar.Image
                  size={75}
                  style={styles.avatar}
                  source={{ uri: UPLOAD_URL + commodity.gambar }}
                />
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Kode Barang</Caption>}
              right={(props) => (
                <Paragraph style={styles.value}>
                  {commodity.kode_barang}
                </Paragraph>
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Nama Barang</Caption>}
              right={() => (
                <Paragraph style={styles.value}>
                  {commodity.nama_barang}
                </Paragraph>
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Jenis Barang</Caption>}
              right={() => (
                <Paragraph style={styles.value}>
                  {commodity.jenis_barang}
                </Paragraph>
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Nama Supplier</Caption>}
              right={() => (
                <Paragraph style={styles.value}>
                  {commodity.nama_perusahaan}
                </Paragraph>
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Harga Satuan</Caption>}
              right={() => (
                <Paragraph style={styles.value}>
                  Rp. {formatRupiah(parseInt(commodity.harga_satuan))}
                </Paragraph>
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Penyimpanan</Caption>}
              right={() => (
                <Paragraph style={styles.value}>
                  {commodity.penyimpanan}
                </Paragraph>
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Minimum Order</Caption>}
              right={() => (
                <Paragraph style={styles.value}>
                  {commodity.minimum_order}
                </Paragraph>
              )}
            />
            <Divider />
            <List.Item
              title={<Caption>Satuan</Caption>}
              right={() => (
                <Paragraph style={styles.value}>{commodity.satuan}</Paragraph>
              )}
            />
            <Divider />
          </Card.Content>
        </Card>
      )}
      <View style={{ flexDirection: "row" }}>
        <Button
          style={{ flex: 1 }}
          mode="contained"
          onPress={function () {
            navigation.navigate("UpdateCommodity", { id: id });
          }}
        >
          Update
        </Button>
        <Button
          style={{ flex: 1 }}
          mode="contained"
          color={Colors.danger}
          onPress={function () {
            Alert.alert('Hapus', 'Anda yakin akan menghapus komoditas ini?',[
              {text: "Ya", onPress:function(){
                dispatch(commodityDelete(id));
                navigation.goBack();
              }},
              {text: "Tdk", onPress:function(){
                null;
              }}
            ])
          }}
        >
          Delete
        </Button>
      </View>
    </Root>
  );
}
export default ReadCommodity;

const styles = StyleSheet.create({
  avatar: { borderRadius: 0, backgroundColor: "white" },
  value: { marginRight: 10 },
  container: { height: "100%", backgroundColor: Colors.gray1, padding: 10 },
});
