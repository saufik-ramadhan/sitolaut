import React, { useState, useEffect } from "react";
import { View, StyleSheet, Keyboard } from "react-native";
import {
  TextInput,
  Card,
  IconButton,
  Caption,
  Button,
} from "react-native-paper";
import { Picker } from "@react-native-community/picker";
import Colors from "../../config/Colors";
import { Space } from "../../components";
import { useSelector } from "react-redux";

export default function SearchCommodity({ onSearch, jenisBarang }: any) {
  const [visible, setVisible] = useState(false);
  const toggleSearch = () => setVisible(!visible);

  const { id, usertype } = useSelector(function (state?: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const [searchForm, setSearchForm] = useState({
    id_jenis_barang: 0,
    kode_barang: "",
    nama_barang: "",
    start: 0,
    user_id_supplier: id,
  });
  const onJenisBarangChange = (value: any) => {
    setSearchForm({ ...searchForm, id_jenis_barang: value });
  };
  const formHandle = (key: any, value: any) => {
    setSearchForm({ ...searchForm, [key]: value });
  };

  useEffect(() => {
    // console.log(searchForm)
  }, [searchForm]);
  return (
    <View style={{ marginHorizontal: 5 }}>
      {!visible ? (
        <Card onPress={toggleSearch} style={styles.cardSearch}>
          <View style={{ flexDirection: "row" }}>
            <IconButton icon="magnify" />
            <View style={styles.boxSearch}>
              <Caption>Cari Komoditas . . .</Caption>
            </View>
          </View>
        </Card>
      ) : (
        <Card>
          <Card.Content>
            <TextInput
              label="Kode Barang"
              dense
              onChangeText={(text) => formHandle("kode_barang", text)}
            />
            <Space height={10} />
            <TextInput
              label="Nama Barang"
              dense
              onChangeText={(text) => formHandle("nama_barang", text)}
            />
            <Space height={10} />
            <View>
              <Picker
                itemStyle={{ fontSize: 8 }}
                onValueChange={onJenisBarangChange}
                selectedValue={searchForm.id_jenis_barang}
                mode="dropdown"
              >
                <Picker.Item label="Pilih Jenis Barang" value={0} />
                {jenisBarang.map(function (item, key) {
                  return (
                    <Picker.Item
                      key={key}
                      label={item.jenis_barang}
                      value={item.id}
                    />
                  );
                })}
              </Picker>
            </View>
            <Space height={10} />
            <View style={styles.row}>
              <Button
                style={styles.btn}
                mode="outlined"
                icon="close"
                onPress={toggleSearch}
              >
                Tutup
              </Button>
              <Button
                style={styles.btn}
                onPress={function () {
                  onSearch(searchForm);
                  Keyboard.dismiss();
                  toggleSearch();
                }}
                icon="magnify"
                mode="contained"
              >
                Cari
              </Button>
            </View>
          </Card.Content>
        </Card>
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  boxSearch: {
    backgroundColor: Colors.gray1,
    flex: 1,
    marginRight: 10,
    borderRadius: 4,
    padding: 8,
    justifyContent: "center",
  },
  cardSearch: {
    margin: 5,
    padding: 5,
  },
  row: {
    flexDirection: "row",
  },
  btn: { flex: 1, marginHorizontal: 2 },
});
