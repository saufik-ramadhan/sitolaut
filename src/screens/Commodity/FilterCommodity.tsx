import React from "react";
import { StyleSheet, Text, View, Picker } from "react-native";
import { Formik } from "formik";
import { commodityFilterAdd } from "../../redux/commodityReducer";
import { IconButton, TextInput, Button } from "react-native-paper";
import { SelectInput } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux/rootReducer";

export default function CommodityFilter({ navigation }) {
  const dispatch = useDispatch();
  const jenisbarang = useSelector(function (state: RootState) {
    return state.jenisbarang.jenisbarangGetSuccess
      ? state.jenisbarang.jenisbarangGetSuccess.data
      : [];
  });
  return (
    <Formik
      initialValues={{
        jenis_barang_id: "",
        nama_barang: "",
        kode_barang: "",
      }}
      onSubmit={(val) => {
        dispatch(commodityFilterAdd(val));
        navigation.goBack();
      }}
    >
      {({ handleChange, handleBlur, handleSubmit, values, setFieldValue }) => (
        <>
          <TextInput
            label="Kode Barang"
            onChangeText={handleChange("kode_barang")}
            onBlur={handleBlur("kode_barang")}
            value={values.kode_barang}
          />
          <TextInput
            label="Nama Barang"
            onChangeText={handleChange("nama_barang")}
            onBlur={handleBlur("nama_barang")}
            value={values.nama_barang}
          />
          {/**
           * Supplier
           */}
          <SelectInput
            label={"Jenis Barang"}
            onChangeValue={(val) => setFieldValue("jenis_barang_id", val.id)}
            options={jenisbarang}
            objectKey="jenis_barang"
            withSearch
          />

          <Button onPress={handleSubmit}>Filter</Button>
        </>
      )}
    </Formik>
  );
}

const styles = StyleSheet.create({});
