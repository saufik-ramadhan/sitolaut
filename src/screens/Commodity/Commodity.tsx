import React from "react";
import { StyleSheet, Text, View, FlatList, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import {
  IconButton,
  Card,
  Paragraph,
  Caption,
  Badge,
  TextInput,
  Avatar,
} from "react-native-paper";
import Colors from "./../../config/Colors";
import { RegulatorStackProps, SupplierStackProps } from "../Navigator";
import { RootState } from "../../redux/rootReducer";
import { EmptyState, FilterButton, SelectInput, Space } from "../../components";
import { DateFormat, formatRupiah } from "../../services/utils";
import {
  commodityFilterClear,
  commodityFetchPage,
  commodityDelete,
  commodityFilterAdd,
  commodityFetchOne,
} from "./../../redux/commodityReducer";
import { Formik } from "formik";
import { UPLOAD_URL } from "../../config/constants";

function FloatingAdd({ navigation }: SupplierStackProps<"Commodity">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateCommodity");
      }}
    />
  );
}

export default function Commodity({
  navigation,
}: RegulatorStackProps<"Commodity">) {
  const dispatch = useDispatch();
  const commodity = useSelector(function (state: RootState) {
    return state.commodity.commodityData ? state.commodity.commodityData : [];
  });
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const { hasNext, currentPage } = useSelector(function (state: RootState) {
    return state.commodity.commodityGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.commodity.commodityFilter;
  });
  const loading = useSelector(function (state: RootState) {
    return state.commodity.commodityGetLoading;
  });
  const addsuccess = useSelector(function (state: RootState) {
    return state.commodity.commodityAddLoading;
  });
  const delsuccess = useSelector(function (state: RootState) {
    return state.commodity.commodityDeleteLoading;
  });
  const editsuccess = useSelector(function (state: RootState) {
    return state.commodity.commodityViewLoading;
  });
  function fetchData(filter) {
    const params = {
      length: 10,
      start: 0,
      kode_barang: "",
      nama_barang: "",
      user_id_supplier: String(id),
      ...filter,
    };
    dispatch(commodityFetchPage(params));
  }
  function handleReachEnd(filter) {
    const params = {
      length: 10,
      start: currentPage * 10,
      kode_barang: "",
      nama_barang: "",
      user_id_supplier: String(id),
      ...filter,
    };
    dispatch(commodityFetchPage(params));
  }
  function handleDelete(id) {
    dispatch(commodityDelete(id));
    dispatch(commodityFilterAdd({}));
  }
  const onRefresh = () => {
    dispatch(commodityFilterClear());
  };

  React.useEffect(() => {
    fetchData(filter);
  }, [filter, addsuccess, editsuccess, delsuccess]);

  return (
    <>
      <FlatList
        data={commodity}
        onRefresh={onRefresh}
        refreshing={loading}
        ListEmptyComponent={<EmptyState />}
        keyExtractor={function (item, key) {
          return String(key);
        }}
        ListFooterComponent={<Space height={100} />}
        onEndReachedThreshold={1}
        onEndReached={function () {
          hasNext ? handleReachEnd(filter) : null;
        }}
        renderItem={function ({ item }) {
          return (
            <Card
              elevation={4}
              style={styles.card}
              onPress={function () {
                navigation.navigate("ReadCommodity", { id: item.id });
              }}
            >
              <Paragraph style={{ position: "absolute", bottom: 0, right: 0 }}>
                {item.kode_barang}
              </Paragraph>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Avatar.Image
                  size={50}
                  style={{ flex: 2, backgroundColor: "white" }}
                  source={{ uri: UPLOAD_URL + item.gambar }}
                />
                <Caption style={{ flex: 8 }}>
                  <Paragraph>{item.nama_barang}</Paragraph>
                  {"\n"}
                  <Caption>{item.jenis_barang}</Caption>
                  {"\n"}
                  <Caption>
                    Rp. {formatRupiah(parseInt(item.harga_satuan))}
                  </Caption>
                  {" | "}
                  Min. order:{" "}
                  <Caption>
                    {item.minimum_order} {item.satuan}
                  </Caption>
                  {" | "}
                  <Caption>{item.penyimpanan}</Caption>
                </Caption>
                <View style={{ flex: 1 }}></View>
              </View>
            </Card>
          );
        }}
      />
      <FloatingAdd navigation={navigation} />
      <FilterButton
        onPress={function () {
          navigation.navigate("FilterCommodity");
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 5,
    margin: 5,
  },
  formItem: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    borderRadius: 3,
    elevation: 3,
  },
});
