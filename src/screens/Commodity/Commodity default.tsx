import React, { useEffect, useState } from "react";
import { StyleSheet, View, FlatList, Alert } from "react-native";
import {
  Card,
  Paragraph,
  Subheading,
  Caption,
  IconButton,
  Divider,
} from "react-native-paper";
import Colors from "../../config/Colors";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import {
  commodityFetchSub,
  commodityDelete,
  commodityFetchPage,
} from "../../redux/commodityReducer";
import { connect, useSelector } from "react-redux";
import { formatRupiah } from "../../services/utils";
import PlaceholderLoading from "../../components/PlaceholderLoading";
import { useNavigation } from "@react-navigation/native";
import SearchCommodity from "./SearchCommodity";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import { RootState } from "../../redux/rootReducer";
import { ConsigneeStackProps } from "../Navigator";
import { EmptyState } from "../../components";

function FloatingAdd({
  navigation,
}: ConsigneeStackProps<"PurchaseOrder"> | any) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateCommodity");
      }}
    />
  );
}

function Commodity(props: any) {
  const [refresh, setRefresh] = useState(false);
  const {
    commodityFetchSub,
    commodity_list,
    id,
    loading,
    commodityDelete,
    navigation,
    route,
    jenisbarangFetchAll,
    commodityFetchPage,
  } = props;
  const onRefresh = () => {
    setRefresh(true);
    getCommodity();
  };
  const getCommodity = async () => {
    await commodityFetchSub(id);
    await jenisbarangFetchAll();
    setRefresh(false);
  };

  const DeleteCommodity = (id: any) => {
    console.log(id);
    Alert.alert("Perhatian", "Apakah anda yakin akan menghapus data ini?", [
      {
        text: "Batal",
        style: "cancel",
      },
      {
        text: "Hapus",
        onPress: () => {
          commodityDelete(id);
          getCommodity();
        },
      },
    ]);
  };
  const jenisBarang: Array<any> = useSelector(function (
    state: RootState | any
  ) {
    return state.jenisbarang.jenisbarangGetSuccess
      ? state.jenisbarang.jenisbarangGetSuccess.data
      : [{ id: 0, jenis_barang: "Loading..." }];
  });

  useEffect(() => {
    getCommodity();
  }, []);

  return (
    <>
      {/* <SearchCommodity jenisBarang={jenisBarang} onSearch={(form:any)=>commodityFetchPage(form)}/> */}
      {!loading && (
        <FlatList
          contentContainerStyle={styles.flatlist}
          data={commodity_list}
          keyExtractor={(item) => String(item.id)}
          renderItem={({ item }) => (
            <CommodityItem
              item={item}
              onDelete={() => DeleteCommodity(item.id)}
            />
          )}
          refreshing={refresh}
          onRefresh={onRefresh}
          ListFooterComponent={() => <View style={{ height: 100 }}></View>}
          ListEmptyComponent={EmptyState}
        />
      )}
      <FloatingAdd navigation={navigation} route={route} />
      <PlaceholderLoading loading={loading} />
    </>
  );
}

const CommodityItem = ({ item, onDelete }: any) => {
  const navigation = useNavigation();
  const {
    id,
    harga_satuan,
    jenis_barang,
    kode_barang,
    minimum_order,
    nama_barang,
    penyimpanan,
    satuan,
  } = item;
  // console.log(item)
  const gotoDetail = (read: any) => {
    navigation.navigate("ReadCommodity", { id: read.id });
  };
  const goToEdit = (read: any) => {
    navigation.navigate("UpdateCommodity", { id: read.id });
  };
  return (
    <Card style={styles.cardItem}>
      <Card.Content>
        <View style={styles.titleContainer}>
          <Subheading style={styles.title}>Kode : {kode_barang}</Subheading>
          <View style={styles.actionContainer}>
            <IconButton
              onPress={onDelete}
              size={20}
              icon="delete-outline"
              color={Colors.danger}
            />
            <IconButton
              size={20}
              onPress={() => goToEdit(item)}
              icon="square-edit-outline"
              color={Colors.pri}
            />
          </View>
        </View>
        <Divider />
        <TouchableNativeFeedback onPress={() => gotoDetail(item)}>
          <Caption>
            Nama Barang : <Paragraph>{nama_barang}</Paragraph>
          </Caption>
          <Divider />
          <Caption>
            Harga Satuan :{" "}
            <Paragraph style={styles.prices}>
              {" "}
              Rp.{formatRupiah(harga_satuan)}
            </Paragraph>
          </Caption>
          <Divider />
          <Caption>
            Penyimpanan : <Paragraph> {penyimpanan}</Paragraph>
          </Caption>
          <Divider />
          <Caption>
            Jenis Barang : <Paragraph>{jenis_barang}</Paragraph>
          </Caption>
          <Divider />
          <Caption>
            Min. Order :{" "}
            <Paragraph>
              {" "}
              {minimum_order} {satuan}
            </Paragraph>
          </Caption>
        </TouchableNativeFeedback>
      </Card.Content>
    </Card>
  );
};

const mapState = (state: any) => {
  return {
    commodity_list: state.commodity.commodityGetSuccess.data,
    id: state.auth.authLoginSuccess.data.user[0].id,
    loading: state.commodity.commodityGetLoading,
    jenisBarang: state.jenisbarangGetSuccess,
  };
};
const mapDispatch = (dispatch: any) => {
  return {
    commodityFetchSub: (item: any) => dispatch(commodityFetchSub(item)),
    commodityDelete: (item: any) => dispatch(commodityDelete(item)),
    jenisbarangFetchAll: () => dispatch(jenisbarangFetchAll()),
    commodityFetchPage: (item: any) => dispatch(commodityFetchPage(item)),
  };
};
export default connect(mapState, mapDispatch)(Commodity);

const styles = StyleSheet.create({
  container: { backgroundColor: Colors.gray1 },
  cardItem: { marginBottom: 5, borderRadius: 10, margin: 5 },
  title: { color: Colors.pri, fontWeight: "bold" },
  prices: { color: Colors.danger },
  flatlist: {
    margin: 10,
  },
  titleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  actionContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    width: 100,
  },
});
