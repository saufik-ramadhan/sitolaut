import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Button, TextInput } from "react-native-paper";
import {
  Root,
  Space,
  ErrorText,
  FileInput,
  PickerCostum,
} from "../../components";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { commodityAdd, commodityFetchSub } from "../../redux/commodityReducer";
import { RootState } from "../../redux/rootReducer";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import { jeniscommodityFetchSub } from "../../redux/jeniscommodityReducer";
import { unitFetchAll } from "../../redux/unitReducer";
import { SupplierStackProps } from "../Navigator";

/**
 * Validation Schema
 */
const CreateCommoditySchema = Yup.object().shape({
  kode_barang: Yup.string().required("Required"),
  nama_barang: Yup.string().required("Required"),
  id_jenis_barang: Yup.number().moreThan(0, "Required"),
  id_jenis_commodity: Yup.number().moreThan(0, "Required"),
  mst_satuan_id: Yup.number().moreThan(0).required(),
  supplier_id: Yup.string().required("Required"),
  harga_satuan: Yup.number().required("Required"),
  penyimpanan: Yup.string().required("Required"),
  minimum_order: Yup.number().required("Required"),
  deskripsi: Yup.string().required("Required"),
  gambar: Yup.string().required("Required"),
});

/**
 * Main Screen
 * @param navigation
 */
export default function CreateCommodity({
  navigation,
}: SupplierStackProps<"CreateCommodity">) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const isLoading = useSelector(function (state: RootState) {
    return state.commodity.commodityAddLoading;
  });
  const satuan = useSelector(function (state: RootState | any) {
    return state.unit.unitGetSuccess
      ? state.unit.unitGetSuccess.data
      : [{ id: 0, satuan: "Loading..." }];
  });
  const jenisBarang: Array<any> = useSelector(function (
    state: RootState | any
  ) {
    return state.jenisbarang.jenisbarangGetSuccess
      ? state.jenisbarang.jenisbarangGetSuccess.data
      : [{ id: 0, jenis_barang: "Loading..." }];
  });
  const jenisCommodity = useSelector(function (state: RootState | any) {
    return state.jeniscommodity.jeniscommodityGetSuccess
      ? state.jeniscommodity.jeniscommodityGetSuccess.data
      : [{ id: 0, nama_barang: "Loading..." }];
  });
  const commodityAddSuccess = useSelector(function (state: RootState | any) {
    return state.commodity.commodityAddSuccess;
  });

  const _getInitData = async () => {
    await dispatch(jenisbarangFetchAll());
    await dispatch(unitFetchAll());
  };

  const [isSuccess, setSuccsess] = React.useState(false);

  React.useEffect(() => {
    _getInitData();
  }, []);

  React.useEffect(() => {
    if (isSuccess) {
      dispatch(commodityFetchSub(id));
      navigation.navigate("Commodity");
      // Alert.alert(
      //   "Berhasil",
      //   "Komoditi Terbaru Berhasil Dibuat",
      //   [{ text: "OK", onPress: () => navigation.navigate("Commodity") }],
      //   { cancelable: false }
      // );
    }
  }, [isSuccess]);
  return (
    <View style={{ flex: 1, backgroundColor: Colors.white }}>
      <Root style={styles.container}>
        <Formik
          initialValues={{
            kode_barang: "",
            nama_barang: "",
            id_jenis_barang: 0,
            id_jenis_commodity: "",
            mst_satuan_id: "",
            supplier_id: id,
            harga_satuan: "",
            penyimpanan: "",
            minimum_order: "",
            deskripsi: "",
            gambar: "",
            gambar_size: "",
          }}
          onSubmit={(values) => {
            console.log(values);
            dispatch(commodityAdd(values));
            navigation.goBack();
          }}
          validationSchema={CreateCommoditySchema}
        >
          {({
            handleChange,
            setFieldValue,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              {/**
               * Kode Barang
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.kode_barang}
                onChangeText={handleChange("kode_barang")}
                autoCapitalize="none"
                onBlur={handleBlur("kode_barang")}
                label="Kode Barang"
              />
              {errors.kode_barang && touched.kode_barang && (
                <ErrorText>{errors.kode_barang}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Nama Barang
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.nama_barang}
                onChangeText={handleChange("nama_barang")}
                autoCapitalize="none"
                onBlur={handleBlur("nama_barang")}
                label="Nama Barang"
              />
              {errors.nama_barang && touched.nama_barang && (
                <ErrorText>{errors.nama_barang}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Jenis Barang
               */}
              <PickerCostum
                data={jenisBarang}
                objectKey="label"
                initLabel="Pilih Jenis Barang"
                onValueChange={function (itemValue: number | any) {
                  setFieldValue("id_jenis_barang", itemValue.id);
                  dispatch(
                    jeniscommodityFetchSub(`/jenisbarang/${itemValue.id}`)
                  );
                }}
              />
              <Space height={10} />
              {errors.id_jenis_barang && (
                <ErrorText>{errors.id_jenis_barang}</ErrorText>
              )}
              {values.id_jenis_barang ? (
                <PickerCostum
                  data={jenisCommodity}
                  objectKey="label"
                  initLabel="Pilih Jenis Komoditas"
                  onValueChange={function (itemValue: number | any) {
                    setFieldValue("id_jenis_commodity", itemValue.id);
                  }}
                />
              ) : null}

              {errors.id_jenis_commodity && touched.id_jenis_commodity && (
                <ErrorText>{errors.id_jenis_commodity}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Penyimpanan
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.penyimpanan}
                onChangeText={handleChange("penyimpanan")}
                autoCapitalize="none"
                onBlur={handleBlur("penyimpanan")}
                label="Penyimpanan"
              />
              {errors.penyimpanan && touched.penyimpanan && (
                <ErrorText>{errors.penyimpanan}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Harga Satuan
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.harga_satuan}
                onChangeText={handleChange("harga_satuan")}
                onBlur={handleBlur("harga_satuan")}
                label="Harga Satuan"
                keyboardType="number-pad"
              />
              {errors.harga_satuan && touched.harga_satuan && (
                <ErrorText>{errors.harga_satuan}</ErrorText>
              )}
              <Space height={20} />
              {/**
               * Minimum Order
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.minimum_order}
                onChangeText={handleChange("minimum_order")}
                onBlur={handleBlur("minimum_order")}
                label="Minimum Order"
                keyboardType="number-pad"
              />
              {errors.minimum_order && touched.minimum_order && (
                <ErrorText>{errors.minimum_order}</ErrorText>
              )}
              <Space height={20} />
              {/**
               * Satuan (Currency / Ukuran)
               */}
              <PickerCostum
                data={satuan}
                objectKey="label"
                initLabel="Pilih Satuan"
                onValueChange={function (itemValue: number | any) {
                  setFieldValue("mst_satuan_id", itemValue.id);
                  dispatch(
                    jeniscommodityFetchSub(`/jenisbarang/${itemValue.id}`)
                  );
                }}
              />
              {/* <View
                style={[
                  styles.formItem,
                  { flexDirection: "row", justifyContent: "center" },
                ]}
              >
                <Picker
                  selectedValue={values.mst_satuan_id}
                  style={{ flex: 10 }}
                  itemStyle={{ fontSize: 8 }}
                  mode="dropdown"
                  onValueChange={function (itemValue: number) {
                    setFieldValue("mst_satuan_id", itemValue);
                  }}
                >
                  <Picker.Item label="Satuan" value={0} />
                  {satuan.map(function (item, key) {
                    return <Picker.Item label={item.satuan} value={item.id} />;
                  })}
                </Picker>
              </View> */}
              {errors.mst_satuan_id && touched.mst_satuan_id && (
                <ErrorText>{errors.mst_satuan_id}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Gambar Komoditi
               */}
              {values.gambar ? (
                <>
                  <Image
                    source={{ uri: values.gambar.uri }}
                    style={{ height: 100, width: 100 }}
                  />
                  <Space height={10} />
                </>
              ) : null}
              <FileInput
                title="Foto Komoditi"
                placeholder={
                  values.gambar ? values.gambar.name : "Upload File npwp"
                }
                error={errors.gambar_size}
                getValue={function (value) {
                  setFieldValue("gambar", {
                    uri: value.uri,
                    type: value.type,
                    name: value.fileName,
                  });
                  setFieldValue("gambar_size", value.fileSize);
                }}
              />
              <Space height={10} />
              {/**
               * Minimum Order
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.deskripsi}
                onChangeText={handleChange("deskripsi")}
                onBlur={handleBlur("deskripsi")}
                label="Keterangan"
              />
              {errors.deskripsi && touched.deskripsi && (
                <ErrorText>{errors.deskripsi}</ErrorText>
              )}
              <Space height={20} />
              <Button
                mode="contained"
                color={Colors.pri}
                labelStyle={{ color: Colors.white }}
                onPress={handleSubmit}
                loading={isLoading}
                style={{ borderRadius: 3 }}
                contentStyle={{ padding: 5 }}
              >
                Create Commodity
              </Button>
            </>
          )}
        </Formik>
      </Root>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
});
