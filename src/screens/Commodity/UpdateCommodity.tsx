import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { Button, TextInput } from "react-native-paper";
import {
  Root,
  Space,
  ErrorText,
  FileInput,
  PickerCostum,
} from "../../components";
import Colors from "../../config/Colors";
import { Formik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import {
  commodityFetchSub,
  commodityFetchOne,
  commodityUpdate,
} from "../../redux/commodityReducer";
import { RootState } from "../../redux/rootReducer";
import { jenisbarangFetchAll } from "../../redux/jenisbarangReducer";
import { jeniscommodityFetchSub } from "../../redux/jeniscommodityReducer";
import { unitFetchAll } from "../../redux/unitReducer";

/**
 * Validation Schema
 */
const CreateCommoditySchema = Yup.object().shape({
  kode_barang: Yup.string().required("Required"),
  nama_barang: Yup.string().required("Required"),
  id_jenis_barang: Yup.number().moreThan(0, "Required"),
  id_jenis_commodity: Yup.number().moreThan(0, "Required"),
  mst_satuan_id: Yup.number().moreThan(0, "Required"),
  supplier_id: Yup.string().required("Required"),
  harga_satuan: Yup.number().required("Required"),
  penyimpanan: Yup.string().required("Required"),
  minimum_order: Yup.number().required("Required"),
  deskripsi: Yup.string().required("Required"),
  gambar: Yup.string().required("Required"),
});

/**
 * Main Screen
 * @param navigation
 */
export default function CreateCommodity({ navigation, route }: any) {
  const dispatch = useDispatch();
  const { id } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const isLoading = useSelector(function (state: RootState | any) {
    return state.commodity.commodityAddLoading;
  });
  const satuan = useSelector(function (state: RootState | any) {
    return state.unit.unitGetSuccess
      ? state.unit.unitGetSuccess.data
      : [{ id: 0, satuan: "Loading..." }];
  });
  const jenisBarang: Array<any> = useSelector(function (
    state: RootState | any
  ) {
    return state.jenisbarang.jenisbarangGetSuccess
      ? state.jenisbarang.jenisbarangGetSuccess.data
      : [{ id: 0, jenis_barang: "Loading..." }];
  });
  const jenisCommodity = useSelector(function (state: RootState | any) {
    return state.jeniscommodity.jeniscommodityGetSuccess
      ? state.jeniscommodity.jeniscommodityGetSuccess.data
      : [{ id: 0, nama_barang: "Loading..." }];
  });
  const commodityAddSuccess = useSelector(function (state: RootState | any) {
    return state.commodity.commodityAddSuccess;
  });
  const commodity_view = useSelector(function (state: any) {
    return state.commodity.commodityViewSuccess
      ? state.commodity.commodityViewSuccess.data[0]
      : [{}];
  });
  const JenisBarangGetSuccess = useSelector(function (state: any) {
    return state.commodity.commodityViewSuccess.success;
  });
  const fetchData = () => {
    const commodity_id = route.params.id;
    dispatch(commodityFetchOne(commodity_id));
    dispatch(jenisbarangFetchAll());
    dispatch(
      jeniscommodityFetchSub(`/jenisbarang/${commodity_view.id_jenis_barang}`)
    );
    dispatch(unitFetchAll());
  };

  const initValues = {
    id: route.params.id,
    kode_barang: commodity_view.kode_barang,
    nama_barang: commodity_view.nama_barang,
    id_jenis_barang: commodity_view.id_jenis_barang,
    id_jenis_commodity: commodity_view.id_jenis_commodity,
    mst_satuan_id: commodity_view.mst_satuan_id,
    supplier_id: id,
    harga_satuan: commodity_view.harga_satuan,
    penyimpanan: commodity_view.penyimpanan,
    minimum_order: commodity_view.minimum_order,
    deskripsi: commodity_view.deskripsi,
    gambar: commodity_view.gambar,
    gambar_size: "",
  };
  const [file_gambar, setGambar] = React.useState("");

  React.useEffect(() => {
    fetchData();
  }, []);

  const [isSuccess, setSuccess] = React.useState(false);
  const getCommodity = () => {
    dispatch(commodityFetchSub(id));
    navigation.goBack();
  };

  React.useEffect(() => {
    if (isSuccess) {
      getCommodity();
    }
  }, [isSuccess]);
  // React.useEffect(() => {
  //  if(isSuccess) {getCommodity()}
  // }, [isSuccess]);
  return (
    <View style={{ flex: 1, backgroundColor: Colors.white }}>
      <Root style={styles.container}>
        <Formik
          initialValues={initValues}
          onSubmit={(values) => {
            values.gambar = file_gambar;
            dispatch(commodityUpdate(values));
            setSuccess(commodityAddSuccess.success);
            navigation.goBack();
          }}
          validationSchema={CreateCommoditySchema}
        >
          {({
            handleChange,
            setFieldValue,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              {/**
               * Kode Barang
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.kode_barang}
                defaultValue={commodity_view.kode_barang}
                onChangeText={handleChange("kode_barang")}
                autoCapitalize="none"
                onBlur={handleBlur("kode_barang")}
                label="Kode Barang"
              />
              {errors.kode_barang && touched.kode_barang && (
                <ErrorText>{errors.kode_barang}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Nama Barang
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.nama_barang}
                defaultValue={commodity_view.nama_barang}
                onChangeText={handleChange("nama_barang")}
                autoCapitalize="none"
                onBlur={handleBlur("nama_barang")}
                label="Nama Barang"
              />
              {errors.nama_barang && touched.nama_barang && (
                <ErrorText>{errors.nama_barang}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Jenis Barang
               */}
              {JenisBarangGetSuccess && (
                <PickerCostum
                  data={jenisBarang}
                  initLabel={commodity_view.jenis_barang}
                  onValueChange={(value: any) => {
                    setFieldValue("id_jenis_barang", value.id);
                    dispatch(
                      jeniscommodityFetchSub(`/jenisbarang/${value.id}`)
                    );
                  }}
                  objectKey="label"
                />
              )}
              {errors.id_jenis_barang && touched.id_jenis_barang && (
                <ErrorText>{errors.id_jenis_barang}</ErrorText>
              )}

              <Space height={10} />
              <PickerCostum
                data={jenisCommodity}
                initLabel={commodity_view.jenis_commodity}
                onValueChange={(value: any) => {
                  setFieldValue("id_jenis_commodity", value.id);
                  dispatch(jeniscommodityFetchSub(`/jenisbarang/${value.id}`));
                }}
                objectKey="label"
              />
              {errors.id_jenis_commodity && touched.id_jenis_commodity && (
                <ErrorText>{errors.id_jenis_commodity}</ErrorText>
              )}
              <Space height={10} />
              <TextInput
                style={[styles.formItem]}
                value={values.penyimpanan}
                defaultValue={commodity_view.penyimpanan}
                onChangeText={handleChange("penyimpanan")}
                autoCapitalize="none"
                onBlur={handleBlur("penyimpanan")}
                label="Penyimpanan"
              />
              {errors.penyimpanan && touched.penyimpanan && (
                <ErrorText>{errors.penyimpanan}</ErrorText>
              )}
              <Space height={10} />

              <TextInput
                style={[styles.formItem]}
                value={values.harga_satuan}
                defaultValue={commodity_view.harga_satuan}
                onChangeText={handleChange("harga_satuan")}
                onBlur={handleBlur("harga_satuan")}
                label="Harga Satuan"
                keyboardType="number-pad"
              />
              {errors.harga_satuan && touched.harga_satuan && (
                <ErrorText>{errors.harga_satuan}</ErrorText>
              )}
              <Space height={20} />
              {/**
               * Minimum Order
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.minimum_order}
                defaultValue={commodity_view.minimum_order}
                onChangeText={handleChange("minimum_order")}
                onBlur={handleBlur("minimum_order")}
                label="Minimum Order"
                keyboardType="number-pad"
              />
              {errors.minimum_order && touched.minimum_order && (
                <ErrorText>{errors.minimum_order}</ErrorText>
              )}
              <Space height={20} />
              <PickerCostum
                data={satuan}
                initValue={
                  satuan.filter(
                    (item: any) => item.id === commodity_view.mst_satuan_id
                  )[0]
                }
                onValueChange={(value: any) => console.log("form==>", value)}
                objectKey="label"
              />
              {errors.mst_satuan_id && touched.mst_satuan_id && (
                <ErrorText>{errors.mst_satuan_id}</ErrorText>
              )}
              <Space height={10} />
              {/**
               * Gambar Komoditi
               */}
              <Space height={10} />
              <FileInput
                title="Foto Komoditi"
                placeholder={
                  values.gambar ? values.gambar.name : "Upload File npwp"
                }
                error={errors.gambar_size}
                getValue={function (value: any) {
                  setFieldValue("gambar", {
                    uri: value.uri,
                    type: value.type,
                    name: value.fileName,
                  });
                  setGambar({
                    uri: value.uri,
                    type: value.type,
                    name: value.fileName,
                  });
                  setFieldValue("gambar_size", value.fileSize);
                }}
              />
              {values.gambar ? (
                <>
                  <Image
                    source={{ uri: file_gambar.uri || values.gambar.uri }}
                    style={{ height: 100, width: 100 }}
                  />
                </>
              ) : null}
              <Space height={10} />
              {/**
               * Minimum Order
               */}
              <TextInput
                style={[styles.formItem]}
                value={values.deskripsi}
                defaultValue={commodity_view.deskripsi}
                onChangeText={handleChange("deskripsi")}
                onBlur={handleBlur("deskripsi")}
                label="Keterangan"
              />
              {errors.deskripsi && touched.deskripsi && (
                <ErrorText>{errors.deskripsi}</ErrorText>
              )}
              <Space height={20} />
              <Button
                mode="contained"
                color={Colors.pri}
                labelStyle={{ color: Colors.white }}
                onPress={handleSubmit}
                loading={isLoading}
                style={{ borderRadius: 3 }}
                contentStyle={{ padding: 5 }}
                icon="send"
              >
                Simpan
              </Button>
            </>
          )}
        </Formik>
      </Root>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  formItem: {
    backgroundColor: Colors.grayL,
    color: "black",
    width: "100%",
    borderRadius: 3,
    borderColor: Colors.gray5,
    borderWidth: 0,
  },
  inputError: {
    borderWidth: 0.5,
    borderColor: Colors.danger,
  },
});
