import React, { useEffect } from "react";
import { View, Text, Alert, StyleSheet, FlatList } from "react-native";
import {
  Badge,
  Caption,
  Card,
  IconButton,
  List,
  Paragraph,
  Button,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { Center, EmptyState, Root } from "../../components";
import { getData, isActions } from "../../redux/masterkodetrayekReducer";
import { RootState } from "../../redux/rootReducer";
import { RegulatorStackProps } from "../Navigator";
import Colors from "./../../config/Colors";

function FloatingAdd({ navigation }: RegulatorStackProps<"MasterKodeTrayek">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("MasterKodeTrayekModal", {
          method: "ADD",
          data: {},
        });
      }}
    />
  );
}

export default function MasterKodeTrayek(
  props: RegulatorStackProps<"MasterKodeTrayek">
) {
  const dispatch = useDispatch();
  const {getDataSuccess, isActionProcess} = useSelector(
    (state: RootState) => state.masterkodetrayek
  );
  const fetchData = () => {
    let params = {
      page: 1,
      sizePerPage: 5,
      kodeTrayek: "",
      tahunTrayek: "",
      statusTrayek: "",
      id_operator: "",
    };
    dispatch(getData(params));
  };
  useEffect(() => {
    fetchData();
  }, [isActionProcess]);

  // if (!mkt.data) return <Text>Data Master Kode Trayek tidak ditemukan</Text>;
  return (
    <>
      <FlatList
        data={getDataSuccess.data}
        ListEmptyComponent={<Center><EmptyState/></Center>}
        renderItem={({item}) => (
          <List.Accordion title={item.kode_trayek} description={`Operator : ${item.nama_operator}`} style={{backgroundColor: 'white'}}>
            <List.Item title={item.id_tipe_cargo == 1 ? "Container" : "General Cargo"} description="Tipe Kargo" style={styles.item}/>
            <List.Item title={item.tahun_trayek} description="Tahun" style={styles.item}/>
            <List.Item title={item.pelabuhan_origin + " ➤ " + item.pelabuhan_destination} description="Rute" style={styles.item}/>
            <List.Item title={item.status_kode_trayek == "1" ? "Aktif" : "Tidak Aktif"} description="Status" style={styles.item}/>
            <List.Item title={item.is_return_port == "0" ? "Berangkat" : "Balik"} description="Jenis Arus" style={styles.item}/>
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
              <Button onPress={() => {
                props.navigation.navigate("MasterKodeTrayekModal", {
                  method: "EDIT",
                  data: item,
                });
              }}>Edit</Button>
              <Button onPress={() => {
                Alert.alert("Hapus", "Anda yakin akan menghapus trayek ini?", [
                  {text:"Ya", onPress:() => dispatch(isActions("DELETE", {id: item.id}))},
                  {text:"Tidak", onPress:() => null}
                ])
              }}>Delete</Button>
            </View>
          </List.Accordion>
        )}
        keyExtractor={(item, key) => String(key)}
      />

      
      <FloatingAdd {...props} />
    </>
  );
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    margin: 5,
  },
  item: {
    paddingVertical: 0,
    margin: 0
  }
});

{/* <Root>
        {mkt.data.map((item, key) => (
          <Card
            key={key}
            style={styles.card}
            onPress={function () {
              props.navigation.navigate("MasterKodeTrayekModal", {
                method: "EDIT",
                data: item,
              });
            }}
          >
            <Caption>
              Kode Trayek : <Paragraph>{item.kode_trayek}</Paragraph>
            </Caption>
            <Caption>
              Nama Operator : <Paragraph>{item.nama_operator}</Paragraph>
            </Caption>
            <Caption>
              Tipe Cargo :{" "}
              <Paragraph>
                {item.id_tipe_cargo == 1 ? "Container" : "General Cargo"}
              </Paragraph>
            </Caption>
            <Caption>
              Tahun : <Paragraph>{item.tahun_trayek}</Paragraph>
            </Caption>
            <Caption>
              {item.pelabuhan_origin} ➤ {item.pelabuhan_destination}
            </Caption>
            {item.status_kode_trayek == "1" ? (
              <Badge style={{ position: "absolute", backgroundColor: "green" }}>
                Aktif
              </Badge>
            ) : (
              <Badge style={{ position: "absolute", backgroundColor: "red" }}>
                Tidak Aktif
              </Badge>
            )}
            <Caption>
              Jenis Arus :{" "}
              <Paragraph>
                {item.is_return_port == "0" ? "Berangkat" : "Balik"}
              </Paragraph>
            </Caption>
          </Card>
        ))}
      </Root> */}