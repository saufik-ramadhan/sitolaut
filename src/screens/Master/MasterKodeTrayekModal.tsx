import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Yup from "yup";
import { Formik } from "formik";
import { Root, SelectInput2, Space, TextInput } from "../../components";
import { Button, Caption } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { operatorFetchPage } from "../../redux/operatorReducer";
import { RootState } from "../../redux/rootReducer";
import { portFetchAll } from "./../../redux/portReducer";
import { getTipeCargo, isActions } from "../../redux/masterkodetrayekReducer";
import { RegulatorStackProps } from "../Navigator";
import Colors from "./../../config/Colors";

const Schema = Yup.object({
  kode_trayek: Yup.string().required().label('Kode Trayek'),
  tahun_trayek: Yup.string().required().label('Tahun'),
  port_origin_id: Yup.string().required().label('Pelabuhan Asal'),
  port_destination_id: Yup.string().required().label('Pelabuhan Tujuan'),
  is_return_port: Yup.string().required(),
  status_kode_trayek: Yup.string().required().label('Status'),
  id_operator: Yup.string().required().label('Operator'),
  id_tipe_cargo: Yup.string().required().label('Tipe Kargo'),
});


export default function MasterKodeTrayekModal({
  navigation,
  route,
}: RegulatorStackProps<"MasterKodeTrayekModal">) {
  const { method, data } = route.params;
  const dispatch = useDispatch();
  let { operator, port, masterkodetrayek } = useSelector(
    (state: RootState) => state
  );
  const fetchData = () => {
    dispatch(operatorFetchPage());
    dispatch(portFetchAll());
    dispatch(getTipeCargo());
  };
  const tahunTrayek = () => {
    const current = new Date();
    const year = current.getFullYear();
    let trayekYears = [];
    trayekYears.push(year - 1);
    for (let i = year; i < year + 4; i++) {
      trayekYears.push(i);
    }
    return trayekYears.map((item, key) => ({ id: item, label: item }));
  };
  
  useEffect(() => {
    fetchData();
  }, []);

  if(!operator.operatorGetSuccess.data) return null;
  if(!port.portGetSuccess.data) return null;
  if(!masterkodetrayek.dataTipeCargoSuccess) return null;
  return (
    <Root>
      <Formik
        validationSchema={Schema}
        initialValues={
          method == "ADD"
            ? {
                kode_trayek: "",
                tahun_trayek: "",
                port_origin_id: "",
                port_destination_id: "",
                is_return_port: "",
                status_kode_trayek: "",
                id_operator: "",
                id_tipe_cargo: "",
              }
            : {
                id: data.id,
                id_operator: data.id_operator,
                id_tipe_cargo: data.id_tipe_cargo,
                is_return_port: data.is_return_port,
                kode_trayek: data.kode_trayek,
                port_destination_id: data.port_destination_id,
                port_origin_id: data.port_origin_id,
                status_kode_trayek: data.status_kode_trayek,
                tahun_trayek: data.tahun_trayek,
              }
        }
        onSubmit={(vals) => {
          dispatch(isActions(method, vals));
          navigation.goBack();
        }}
      >
        {({
          handleSubmit,
          handleBlur,
          handleChange,
          values,
          errors,
          setFieldValue,
        }) => (
          <View style={{ padding: 10 }}>
            <TextInput
              style={styles.formItem}
              label="Kode Trayek"
              error={errors.kode_trayek}
              value={values.kode_trayek}
              onChangeText={handleChange("kode_trayek")}
            />
            <SelectInput2
              label={"Operator"}
              onValueChange={(itemValue, itemIndex) => setFieldValue("id_operator", itemValue)}
              items={operator.operatorGetSuccess.data || []}
              itemLabel="label"
              itemValue="id"
              selectedValue={values.id_operator}
              error={errors.id_operator}
            />
            <SelectInput2
              label={"Pelabuhan Asal"}
              onValueChange={(itemValue,itemIndex) => setFieldValue("port_origin_id", itemValue)}
              items={port.portGetSuccess.data || []}
              itemLabel="pelabuhan"
              itemValue="id"
              selectedValue={values.port_origin_id}
              error={errors.port_origin_id}
            />
            <SelectInput2
              label={"Pelabuhan Tujuan"}
              onValueChange={(itemValue,itemIndex) =>
                setFieldValue("port_destination_id", itemValue)
              }
              items={port.portGetSuccess.data || []}
              itemLabel="pelabuhan"
              itemValue="id"
              selectedValue={values.port_destination_id}
              error={errors.port_destination_id}
            />
            <SelectInput2
              label={"Jenis Arus"}
              onValueChange={(itemValue,itemIndex) => setFieldValue("is_return_port", itemValue)}
              items={[
                { id: 1, label: "Balik" },
                { id: 0, label: "Berangkat" },
              ]}
              itemLabel="label"
              itemValue="id"
              selectedValue={values.is_return_port}
              error={errors.is_return_port}
            />
            <SelectInput2
              label={"Tahun Trayek"}
              onValueChange={(itemValue,itemIndex) => setFieldValue("tahun_trayek", itemValue)}
              items={tahunTrayek()}
              itemLabel="label"
              itemValue="id"
              selectedValue={values.tahun_trayek}
              error={errors.tahun_trayek}
            />
            <SelectInput2
              label={"Status Kode Trayek"}
              onValueChange={(itemValue,itemIndex) =>
                setFieldValue("status_kode_trayek", itemValue)
              }
              items={[
                { id: 0, label: "Tidak Aktif" },
                { id: 1, label: "Aktif" },
              ]}
              itemLabel="label"
              itemValue="id"
              selectedValue={values.status_kode_trayek}
              error={errors.status_kode_trayek}
            />
            <SelectInput2
              label={"Tipe Cargo"}
              onValueChange={(itemValue,itemIndex) => setFieldValue("id_tipe_cargo", itemValue)}
              items={masterkodetrayek.dataTipeCargoSuccess || []}
              itemLabel="tipe_cargo"
              itemValue="id"
              selectedValue={values.id_tipe_cargo}
              error={errors.id_tipe_cargo}
            />
            <Space height={10}></Space>
            <Button
              mode="contained"
              onPress={handleSubmit}
              color={method == "ADD" ? Colors.def : Colors.sec}
            >
              {method == "ADD" ? "Tambah Trayek" : "Update"}
            </Button>
            <Space height={60}></Space>
            {method == "EDIT" && (
              <Button
                mode="contained"
                color={Colors.danger}
                onPress={function () {
                  dispatch(isActions("DELETE", values));
                  navigation.goBack();
                }}
              >
                Delete
              </Button>
            )}
          </View>
        )}
      </Formik>
    </Root>
  );
}

const styles = StyleSheet.create({
  formItem: {
    marginBottom: 10,
  },
});


// {/**
//              * Operator
//              */}
             
//            {/**
//             * Pelabuhan Asal
//             */}
           
//            {/**
//             * Pelabuhan Tujuan
//             */}
           

//            <Space height={10}></Space>
//            {/**
//             * Jenis Arus
//             */}


//            {/**
//             * Tahun Trayek
//             */}
           

//            {/**
//             * Status Kode Trayek
//             */}
           
//            {/**
//             * Tipe Cargo
//             */}
           