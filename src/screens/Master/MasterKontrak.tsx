import React, { useEffect } from "react";
import { View, Text, Alert, StyleSheet } from "react-native";
import {
  Badge,
  Caption,
  Card,
  IconButton,
  Paragraph,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { Root } from "../../components";
import { masterKontrakFetchAll } from "../../redux/masterkontrakReducer";
import { RootState } from "../../redux/rootReducer";
import { DateFormat, formatRupiah } from "../../services/utils";
import { RegulatorStackProps } from "../Navigator";
import Colors from "./../../config/Colors";

function FloatingAdd({ navigation }: RegulatorStackProps<"MasterKontrak">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("MasterKontrakModal", {
          method: "ADD",
          data: {},
        });
      }}
    />
  );
}

export default function MasterKontrak(
  props: RegulatorStackProps<"MasterKontrak">
) {
  const dispatch = useDispatch();
  const { masterkontrak } = useSelector((state: RootState) => state);
  const mk = useSelector(
    (state: RootState) => state.masterkontrak.kontrakGetSuccess
  );
  const fetchData = () => {
    let params = {
      page: 1,
      sizePerPage: 5,
      tahun: "",
      status: "",
      idTrayek: "",
    };
    dispatch(masterKontrakFetchAll());
  };
  useEffect(() => {
    fetchData();
  }, [masterkontrak.loading]);

  if (!mk.data) return <Text>Data Master Kode Trayek tidak ditemukan</Text>;
  return (
    <>
      <Root>
        {/* <Caption>{JSON.stringify(mk.data)}</Caption> */}
        {mk.data.map((item, key) => (
          <Card
            key={key}
            style={styles.card}
            onPress={function () {
              props.navigation.navigate("MasterKontrakModal", {
                method: "EDIT",
                data: item,
              });
            }}
          >
            <Caption>
              Nomor Kontrak : <Paragraph>{item.nomor_kontrak}</Paragraph>
            </Caption>
            <Caption>
              Tanggal Awal :{" "}
              <Paragraph>{DateFormat(item.tgl_awal_kontrak)}</Paragraph>
            </Caption>
            <Caption>
              Tanggal Kadaluarsa :{" "}
              <Paragraph>{DateFormat(item.tgl_kontrak_expired)}</Paragraph>
            </Caption>
            <Caption>Addendum : {item.addendum == 1 ? "Aktif" : "-"}</Caption>

            {!item.expired ? (
              <Badge style={{ position: "absolute", backgroundColor: "green" }}>
                Aktif
              </Badge>
            ) : (
              <Badge style={{ position: "absolute", backgroundColor: "red" }}>
                Expired
              </Badge>
            )}
            {item.keterangan && <Caption>{item.keterangan}</Caption>}
          </Card>
        ))}
      </Root>
      <FloatingAdd {...props} />
    </>
  );
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    margin: 5,
  },
});
