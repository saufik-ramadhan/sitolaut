import React, { useEffect } from "react";
import { View, Text, Alert, StyleSheet } from "react-native";
import {
  Badge,
  Caption,
  Card,
  IconButton,
  Paragraph,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { Root } from "../../components";
import { getData, masterHargaGetData } from "../../redux/masterhargaReducer";
import { RootState } from "../../redux/rootReducer";
import { formatRupiah } from "../../services/utils";
import { RegulatorStackProps } from "../Navigator";
import Colors from "./../../config/Colors";

function FloatingAdd({ navigation }: RegulatorStackProps<"MasterHarga">) {
  return (
    <IconButton
      style={{
        position: "absolute",
        backgroundColor: Colors.sec,
        bottom: 10,
        right: 10,
      }}
      size={40}
      color={Colors.black}
      icon="plus"
      onPress={function () {
        navigation.navigate("MasterHargaModal", {
          method: "ADD",
          data: {},
        });
      }}
    />
  );
}

export default function MasterHarga(props: RegulatorStackProps<"MasterHarga">) {
  const dispatch = useDispatch();
  const mh = useSelector(
    (state: RootState) => state.masterharga.masterHargaGetSuccess
  );
  const fetchData = () => {
    let params = {
      page: 1,
      sizePerPage: 5,
      tahun: "",
      status: "",
      idTrayek: "",
    };
    dispatch(masterHargaGetData(params));
  };
  useEffect(() => {
    fetchData();
  }, []);

  if (!mh.data) return <Text>Data Master Kode Trayek tidak ditemukan</Text>;
  return (
    <>
      <Root>
        {/* <Caption>{JSON.stringify(mh.data)}</Caption> */}
        {mh.data.map((item, key) => (
          <Card
            key={key}
            style={styles.card}
            onPress={function () {
              props.navigation.navigate("MasterHargaModal", {
                method: "EDIT",
                data: item,
              });
            }}
          >
            <Caption>
              Kode Trayek : <Paragraph>{item.kode_trayek}</Paragraph>
            </Caption>
            <Caption>
              DRY <Paragraph> Rp.{formatRupiah(item.tarif_dry)}</Paragraph>
              {"\n"}
              REEFER{" "}
              <Paragraph> Rp.{formatRupiah(item.tarif_reefer)}</Paragraph>
              {"\n"}
              CURAH <Paragraph> Rp.{formatRupiah(item.tarif_curah)}</Paragraph>
              {"\n"}
              BOX <Paragraph> Rp.{formatRupiah(item.tarif_box)}</Paragraph>
            </Caption>
            <Caption>
              Tahun : <Paragraph>{item.tarif_tahun}</Paragraph>
            </Caption>
            <Caption>
              Referensi : <Paragraph>{item.tarif_referensi}</Paragraph>
            </Caption>
            {item.tarif_status == 1 ? (
              <Badge style={{ position: "absolute", backgroundColor: "green" }}>
                Aktif
              </Badge>
            ) : (
              <Badge style={{ position: "absolute", backgroundColor: "red" }}>
                Tidak Aktif
              </Badge>
            )}
            {item.keterangan ? <Caption>{item.keterangan}</Caption> : null}
          </Card>
        ))}
      </Root>
      <FloatingAdd {...props} />
    </>
  );
}

const styles = StyleSheet.create({
  card: {
    padding: 10,
    margin: 5,
  },
});
