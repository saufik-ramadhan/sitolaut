import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Yup from "yup";
import { Formik } from "formik";
import { Root, SelectInput, Space } from "../../components";
import { Button, Dialog, Portal, TextInput } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { operatorFetchPage } from "../../redux/operatorReducer";
import { RootState } from "../../redux/rootReducer";
import { portFetchAll } from "./../../redux/portReducer";
import { isActions, saveData } from "../../redux/masterhargaReducer";
import { getData } from "../../redux/masterkodetrayekReducer";
import { RegulatorStackProps } from "../Navigator";
import Colors from "./../../config/Colors";
import Confirmation from "../../components/Confirmation";

export default function MasterHargaModal({
  navigation,
  route,
}: RegulatorStackProps<"MasterHargaModal">) {
  const { method, data } = route.params || { method: "", data: {} };
  const [tipeCargo, setTipeCargo] = React.useState(0);
  const [visible, setVisible] = React.useState(false);
  const [keterangan, setKeterangan] = React.useState("");
  const dispatch = useDispatch();
  const { operator, port, masterharga, masterkodetrayek } = useSelector(
    (state: RootState) => state
  );
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const fetchData = () => {
    dispatch(getData({ page: 1 }));
    // dispatch(portFetchAll());
    // dispatch(getTipeCargo());
  };
  const tahunTrayek = () => {
    const current = new Date();
    const year = current.getFullYear();
    let trayekYears = [];
    trayekYears.push(year - 1);
    for (let i = year; i < year + 4; i++) {
      trayekYears.push(i);
    }
    return trayekYears.map((item, key) => ({ id: item, label: item }));
  };
  useEffect(() => {
    fetchData();
    // alert(JSON.stringify(data));
  }, []);
  return (
    <>
      <Confirmation
        visible={visible}
        onYes={function () {
          setVisible(false);
          const params = {
            fId: data.id,
            isDelete: true,
            fKeterangan: keterangan,
          };
          if (keterangan == "") alert("Keterangan harus diisi");
          else {
            dispatch(saveData(params));
            navigation.goBack();
          }
        }}
        onNo={function () {
          setVisible(false);
        }}
        // content="Delete Master Harga"
        component={
          <TextInput
            onChangeText={(val) => setKeterangan(val)}
            value={keterangan}
            dense
            mode="outlined"
          />
        }
        title="Delete"
        onDismiss={function () {
          setVisible(false);
        }}
      />
      <Root>
        <Formik
          initialValues={
            method == "ADD"
              ? {
                  fDry: 0,
                  fReefer: 0,
                  fCurah: 0,
                  fBox: 0,
                  fReferensi: "",
                  fTahun: "",
                  fStatus: "",
                  kode: "",
                  fId: 0,
                }
              : {
                  fDry: data.tarif_dry || 0,
                  fReefer: data.tarif_reefer || 0,
                  fCurah: data.tarif_curah || 0,
                  fBox: data.tarif_box || 0,
                  fReferensi: data.tarif_referensi,
                  fTahun: data.tarif_tahun,
                  fStatus: data.tarif_status,
                  kode: data.id_mst_kode_trayek,
                  fKeterangan: "",
                  fId: data.id,
                }
          }
          onSubmit={(vals) => {
            dispatch(saveData(vals));
            navigation.goBack();
          }}
        >
          {({
            handleSubmit,
            handleBlur,
            handleChange,
            values,
            setFieldValue,
          }) => (
            <View style={{ padding: 10 }}>
              {/**
               * Kode Trayek
               */}
              <SelectInput
                label={"Kode Trayek"}
                onChangeValue={(val) => {
                  setFieldValue("kode", val.id);
                  setTipeCargo(val.id_tipe_cargo);
                  // console.log(val.id_tipe_cargo);
                }}
                options={masterkodetrayek.getDataSuccess.data}
                objectKey="kode_trayek"
              />
              {/**
               * Tahun Tarif
               */}
              <SelectInput
                label={"Tahun Trayek"}
                onChangeValue={(val) => setFieldValue("fTahun", val.id)}
                options={tahunTrayek()}
                objectKey="label"
              />
              {/**
               * Status Harga
               */}
              <SelectInput
                label={"Status"}
                onChangeValue={(val) => setFieldValue("fStatus", val.id)}
                options={[
                  { id: 1, label: "Aktif" },
                  { id: 0, label: "Tidak Aktif" },
                ]}
                objectKey="label"
              />
              <Space height={10}></Space>
              {tipeCargo == 1 ? (
                <>
                  <TextInput
                    label="Tarif DRY"
                    dense
                    value={String(values.fDry)}
                    onChangeText={handleChange("fDry")}
                  />
                  <TextInput
                    label="Tarif REEFER"
                    dense
                    value={String(values.fReefer)}
                    onChangeText={handleChange("fReefer")}
                  />
                </>
              ) : (
                <>
                  <TextInput
                    label="Tarif Curah"
                    dense
                    value={String(values.fCurah)}
                    onChangeText={handleChange("fCurah")}
                  />
                  <TextInput
                    label="Tarif Box"
                    dense
                    value={String(values.fBox)}
                    onChangeText={handleChange("fBox")}
                  />
                </>
              )}
              <Space height={10}></Space>
              <TextInput
                label="Referensi"
                dense
                value={String(values.fReferensi)}
                onChangeText={handleChange("fReferensi")}
              />
              {method == "EDIT" && (
                <TextInput
                  label="Keterangan"
                  dense
                  value={String(values.fKeterangan)}
                  onChangeText={handleChange("fKeterangan")}
                />
              )}
              <Space height={10}></Space>
              <Button
                mode="contained"
                onPress={handleSubmit}
                color={method == "ADD" ? Colors.def : Colors.sec}
              >
                {method == "ADD" ? "Tambah Trayek" : "Update"}
              </Button>
              {method == "EDIT" && (
                <Button
                  mode="contained"
                  color={Colors.danger}
                  onPress={function () {
                    setVisible(true);
                  }}
                >
                  Delete
                </Button>
              )}
            </View>
          )}
        </Formik>
      </Root>
    </>
  );
}

const styles = StyleSheet.create({
  formItem: {
    marginBottom: 10,
  },
});
