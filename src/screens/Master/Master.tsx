import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import {
  MasterKodeTrayek,
  MasterHarga,
  MasterKontrak,
} from "../../screens/Screen";
import Colors from "./../../config/Colors";
import { IconButton, List } from "react-native-paper";
import { RegulatorStackProps } from "../Navigator";

const Tab = createMaterialTopTabNavigator();

export default function Master({navigation}: RegulatorStackProps<"Master">) {
  return (
    <>
      <List.Item title={"Master Kode Trayek"} onPress={() => navigation.navigate('MasterKodeTrayek')} right={() => <List.Icon icon="chevron-right"/>}/>
      <List.Item title={"Master Harga"} onPress={() => navigation.navigate('MasterHarga')} right={() => <List.Icon icon="chevron-right"/>}/>
      <List.Item title={"Master Kontrak"} onPress={() => navigation.navigate('MasterKontrak')} right={() => <List.Icon icon="chevron-right"/>}/>
    </>
  );
}

const styles = StyleSheet.create({});

{/* <Tab.Navigator>
  <Tab.Screen name="Kode Trayek" component={MasterKodeTrayek} />
  <Tab.Screen name="Harga" component={MasterHarga} />
  <Tab.Screen name="Kontrak" component={MasterKontrak} />
</Tab.Navigator> */}