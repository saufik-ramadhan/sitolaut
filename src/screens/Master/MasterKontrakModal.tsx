import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Yup from "yup";
import { Formik } from "formik";
import { DateTime, Icon, Root, SelectInput, Space } from "../../components";
import {
  Button,
  Caption,
  Card,
  Dialog,
  IconButton,
  Paragraph,
  Portal,
  TextInput,
} from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { operatorFetchPage } from "../../redux/operatorReducer";
import { RootState } from "../../redux/rootReducer";
import { portFetchAll } from "./../../redux/portReducer";
import {
  addendumFetchAll,
  addMasterKontrakData,
  DeleteMasterKontrakData,
  EditMasterKontrakData,
} from "../../redux/masterkontrakReducer";
import { getData } from "../../redux/masterkodetrayekReducer";
import { RegulatorStackProps } from "../Navigator";
import Colors from "./../../config/Colors";
import Confirmation from "../../components/Confirmation";
import { DateFormat, DateFormatStrip } from "../../services/utils";

export default function MasterKontrakModal({
  navigation,
  route,
}: RegulatorStackProps<"MasterKontrakModal">) {
  const { method, data } = route.params || { method: "", data: {} };
  // const [tipeCargo, setTipeCargo] = React.useState(0);
  const [visible, setVisible] = React.useState(false);
  const [addendum, setAddendum] = React.useState({
    visible: false,
    method: "ADD",
    data: {},
  });
  const [deleteAddendum, setDeleteAddendum] = React.useState({
    visible: false,
    data: 0,
  });
  // const [keterangan, setKeterangan] = React.useState("");
  const dispatch = useDispatch();
  const { operator, port, masterkontrak, masterkodetrayek } = useSelector(
    (state: RootState) => state
  );
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });
  const fetchData = () => {
    // dispatch(getData({ page: 1 }));
    // dispatch(portFetchAll());
    // dispatch(getTipeCargo());
    if (data.addendum == 1) dispatch(addendumFetchAll(data.id));
  };
  const tahunTrayek = () => {
    const current = new Date();
    const year = current.getFullYear();
    let trayekYears = [];
    trayekYears.push(year - 1);
    for (let i = year; i < year + 4; i++) {
      trayekYears.push(i);
    }
    return trayekYears.map((item, key) => ({ id: item, label: item }));
  };
  useEffect(() => {
    fetchData();
    // alert(JSON.stringify(data));
  }, [masterkontrak.loading]);
  return (
    <>
      <Confirmation
        visible={addendum.visible}
        component={
          <Formik
            initialValues={
              addendum.method == "ADD"
                ? {
                    nomor_kontrak: "",
                    parent_id: data.id,
                    status_kontrak: "1",
                    tahun_kontrak: new Date().getFullYear(),
                    tgl_awal_kontrak: DateFormatStrip(new Date()),
                    tgl_kontrak_expired: DateFormatStrip(new Date()),
                  }
                : {
                    nomor_kontrak: addendum.data.nomor_kontrak,
                    id: addendum.data.id,
                    status_kontrak: "1",
                    tahun_kontrak: new Date().getFullYear(),
                    tgl_awal_kontrak: DateFormatStrip(
                      addendum.data.tgl_awal_kontrak
                    ),
                    tgl_kontrak_expired: DateFormatStrip(
                      addendum.data.tgl_kontrak_expired
                    ),
                  }
            }
            onSubmit={function (val) {
              if (addendum.method == "ADD") dispatch(addMasterKontrakData(val));
              if (addendum.method == "EDIT")
                dispatch(EditMasterKontrakData(val));
              setAddendum({ method: "ADD", visible: false, data: {} });
            }}
          >
            {({
              handleChange,
              handleBlur,
              setFieldValue,
              values,
              handleSubmit,
            }) => (
              <>
                <TextInput
                  label="Nomor Kontrak"
                  onChangeText={handleChange("nomor_kontrak")}
                  onBlur={handleBlur("nomor_kontrak")}
                  value={values.nomor_kontrak}
                  dense
                  mode="outlined"
                />
                {/**
                 * Tanggal Awal Kontrak
                 */}
                <DateTime
                  onChangeValue={function (value: any) {
                    setFieldValue("tgl_awal_kontrak", String(value.date));
                  }}
                  label="Tanggal Awal Kontrak"
                />

                {/**
                 * Tanggal Akhir Kontrak
                 */}
                <DateTime
                  onChangeValue={function (value: any) {
                    setFieldValue("tgl_kontrak_expired", String(value.date));
                  }}
                  label="Tanggal Akhir Kontrak"
                />

                <Button onPress={handleSubmit}>Set Addendum</Button>
              </>
            )}
          </Formik>
        }
        title="Addendum"
        onDismiss={function () {
          setAddendum({ method: "ADD", visible: false, data: {} });
        }}
      />
      <Confirmation
        visible={visible}
        onYes={function () {
          setVisible(false);
          const params = {
            id: data.id,
          };
          dispatch(DeleteMasterKontrakData(params));
          navigation.goBack();
        }}
        onNo={function () {
          setVisible(false);
        }}
        content="Delete Master Kontrak"
        // component={
        //   <TextInput
        //     onChangeText={(val) => setKeterangan(val)}
        //     value={keterangan}
        //     dense
        //     mode="outlined"
        //   />
        // }
        title="Delete"
        onDismiss={function () {
          setVisible(false);
        }}
      />
      <Confirmation
        visible={deleteAddendum.visible}
        onYes={function () {
          setDeleteAddendum({ visible: false, data: 0 });
          const params = {
            id: deleteAddendum.data,
          };
          dispatch(DeleteMasterKontrakData(params));
        }}
        onNo={function () {
          setVisible(false);
        }}
        content="Delete Addendum ?"
        title="Delete"
        onDismiss={function () {
          setDeleteAddendum({ visible: false, data: 0 });
        }}
      />
      <Root>
        <Formik
          initialValues={
            method == "ADD"
              ? {
                  nomor_kontrak: "",
                  tgl_awal_kontrak: DateFormatStrip(new Date()),
                  tgl_kontrak_expired: DateFormatStrip(new Date()),
                  tahun_kontrak: String(new Date().getFullYear()),
                  status_kontrak: 1,
                }
              : {
                  id: data.id,
                  nomor_kontrak: data.nomor_kontrak,
                  tgl_awal_kontrak: data.tgl_awal_kontrak,
                  tgl_kontrak_expired: data.tgl_kontrak_expired,
                  tahun_kontrak: String(new Date().getFullYear()),
                  status_kontrak: data.status_kontrak,
                }
          }
          onSubmit={(vals) => {
            // alert(JSON.stringify(vals));
            if (method == "ADD") dispatch(addMasterKontrakData(vals));
            if (method == "EDIT") dispatch(EditMasterKontrakData(vals));

            navigation.goBack();
          }}
        >
          {({
            handleSubmit,
            handleBlur,
            handleChange,
            values,
            setFieldValue,
          }) => (
            <View style={{ padding: 10 }}>
              <TextInput
                label="Nomor Kontrak"
                dense
                value={String(values.nomor_kontrak)}
                onChangeText={handleChange("nomor_kontrak")}
              />

              {/**
               * Tanggal Awal Kontrak
               */}
              <DateTime
                onChangeValue={function (value: any) {
                  setFieldValue("tgl_awal_kontrak", String(value.date));
                }}
                label="Tanggal Awal Kontrak"
              />

              {/**
               * Tanggal Akhir Kontrak
               */}
              <DateTime
                onChangeValue={function (value: any) {
                  setFieldValue("tgl_kontrak_expired", String(value.date));
                }}
                label="Tanggal Akhir Kontrak"
              />

              {/**
               * Status Kontrak
               */}
              <SelectInput
                label={"Status Kontrak"}
                onChangeValue={(val) => {
                  setFieldValue("status_kontrak", val.id);
                }}
                options={[
                  { id: 1, label: "Aktif" },
                  { id: 0, label: "Tidak Aktif" },
                ]}
                objectKey="label"
              />

              <Space height={10}></Space>

              {data.expired ? (
                <>
                  <Button
                    onPress={function () {
                      setAddendum({ method: "ADD", visible: true, data: {} });
                    }}
                  >
                    Addendum
                  </Button>
                  <Space height={10}></Space>
                  {data.addendum == 1 ? (
                    <>
                      {masterkontrak.adendumGetSuccess.data
                        ? masterkontrak.adendumGetSuccess.data.map(
                            (item, key) => (
                              <Card
                                key={key}
                                style={{ padding: 5, marginBottom: 5 }}
                              >
                                <Caption>
                                  No. Addendum :{" "}
                                  <Paragraph>{item.nomor_kontrak}</Paragraph>
                                </Caption>
                                <Caption>
                                  Status :{" "}
                                  <Paragraph>
                                    {item.status_kontrak == 1
                                      ? "Aktif"
                                      : "Tidak Aktif"}
                                  </Paragraph>
                                </Caption>
                                <Caption>
                                  Tanggal Awal :{" "}
                                  <Paragraph>
                                    {DateFormat(item.tgl_awal_kontrak)}
                                  </Paragraph>
                                </Caption>
                                <Caption>
                                  Tanggal Expired :{" "}
                                  <Paragraph>
                                    {DateFormat(item.tgl_kontrak_expired)}
                                  </Paragraph>
                                </Caption>
                                <Icon
                                  name="file-document-edit"
                                  onPress={function () {
                                    setAddendum({
                                      method: "EDIT",
                                      visible: true,
                                      data: item,
                                    });
                                  }}
                                  style={{
                                    position: "absolute",
                                    right: 0,
                                    fontSize: 25,
                                  }}
                                />
                                <Icon
                                  name="delete"
                                  onPress={function () {
                                    setDeleteAddendum({
                                      visible: true,
                                      data: item.id,
                                    });
                                  }}
                                  style={{
                                    position: "absolute",
                                    right: 0,
                                    bottom: 0,
                                    fontSize: 25,
                                  }}
                                />
                              </Card>
                            )
                          )
                        : null}
                    </>
                  ) : null}
                </>
              ) : (
                <>
                  <Button
                    mode="contained"
                    onPress={handleSubmit}
                    color={method == "ADD" ? Colors.def : Colors.sec}
                  >
                    {method == "ADD" ? "Tambah Trayek" : "Update"}
                  </Button>
                  <Space height={10} />
                  {method == "EDIT" && (
                    <Button
                      mode="contained"
                      color={Colors.danger}
                      onPress={function () {
                        setVisible(true);
                      }}
                    >
                      Delete
                    </Button>
                  )}
                </>
              )}
            </View>
          )}
        </Formik>
      </Root>
    </>
  );
}

const styles = StyleSheet.create({
  formItem: {
    marginBottom: 10,
  },
});
