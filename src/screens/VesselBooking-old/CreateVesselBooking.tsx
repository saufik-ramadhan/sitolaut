import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
} from "react-native";
import { Card, Paragraph, Caption, Button } from "react-native-paper";
import Colors from "./../../config/Colors";
import { useDispatch, useSelector } from "react-redux";
import { scheduleFetchWill } from "../../redux/scheduleReducer";
import { RootState } from "../../redux/rootReducer";
import { MonthsID } from "../../config/Months";
import { Icon } from "../../components";
import { OperatorStackProps } from "../Navigator";
import { DateFormat, TimeFormat } from "../../services/utils";

function B({ children }) {
  return <Paragraph style={styles.bold}>{children}</Paragraph>;
}

export default function CreateVesselBooking({ navigation }) {
  const dispatch = useDispatch();
  const { id } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : 0;
  });
  const loading = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetLoading;
  });
  const jadwalBaru = useSelector(function (state: RootState) {
    return state.schedule.scheduleGetSuccess
      ? state.schedule.scheduleGetSuccess.data
      : [{}];
  });
  const fetchJadwalBaru = (data) => {
    const params = {
      eta: "",
      etd: "",
      length: 10,
      operator: "",
      port_destination_id: "",
      port_origin_id: "",
      start: 0,
      voyage: "",
    };
    dispatch(scheduleFetchWill(params));
  };
  useEffect(() => {
    fetchJadwalBaru({ length: 10, start: 0 });
  }, []);
  return (
    <>
      {loading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={jadwalBaru}
          keyExtractor={(item, key) => String(key)}
          ListEmptyComponent={
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Caption>Tidak ada jadwal baru</Caption>
            </View>
          }
          renderItem={(item) => {
            const depart = new Date(item.item.tanggal_berangkat);
            const arrive = new Date(item.item.tanggal_tiba);
            const closebumn = new Date(item.item.tutup_jadwal_shipper_bumn);
            const closeswasta = new Date(item.item.tutup_jadwal_shipper_swasta);
            const m = MonthsID;
            return (
              <Card style={styles.card}>
                <View style={{ flexDirection: "row" }}>
                  {/** Rute */}
                  <View style={{ flexDirection: "row", flex: 2 }}>
                    <View style={{ flex: 1 }}>
                      <B>
                        {item.item.pel_asal}
                        {"\n"}
                        <Caption>
                          {`${DateFormat(depart)}`}
                          {"\n"}
                          {`(${TimeFormat(depart)})`}{" "}
                        </Caption>
                      </B>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
                    </View>
                    <View style={{ flex: 1, alignItems: "flex-end" }}>
                      <B>
                        {item.item.pel_sampai}
                        {"\n"}
                        <Caption>
                          {`${DateFormat(arrive)}`}
                          {"\n"}
                          {`(${TimeFormat(arrive)})`}{" "}
                        </Caption>
                      </B>
                    </View>
                  </View>

                  {/** Trayek & Voyage */}
                  <View
                    style={{
                      flex: 1,
                      alignItems: "flex-end",
                      justifyContent: "center",
                    }}
                  >
                    <Paragraph>
                      <B>Trayek</B> {item.item.kode_trayek} {"\n"}
                      <B>Voyage</B> {item.item.voyage}
                    </Paragraph>
                  </View>
                </View>

                {/** Kapal */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"ferry"}
                    style={{ fontSize: 20, color: Colors.pending }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    {item.item.nama_kapal}
                  </Paragraph>
                </View>

                {/** Closing BUMN */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"calendar-remove"}
                    style={{ fontSize: 20, color: Colors.def }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    Closing BUMN :{" "}
                    {`${closebumn.getDate()} ${
                      m[closebumn.getMonth()]
                    } ${closebumn.getFullYear()}`}
                  </Paragraph>
                </View>

                {/** Closing SWASTA */}
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <Icon
                    name={"calendar-remove"}
                    style={{ fontSize: 20, color: Colors.sec }}
                  />
                  <Paragraph style={{ alignItems: "center" }}>
                    {" "}
                    Closing SWASTA :{" "}
                    {`${closeswasta.getDate()} ${
                      m[closeswasta.getMonth()]
                    } ${closeswasta.getFullYear()}`}
                  </Paragraph>
                </View>

                {/** Open Detail Modal */}
                <Button
                  color={Colors.success}
                  mode="contained"
                  icon="hand-pointing-up"
                  onPress={function () {
                    navigation.navigate("FormVesselBooking",{id_jadwal:item.item.id});
                  }}
                >
                  Booking
                </Button>
              </Card>
            );
          }}
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  modalContainer: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 3,
    elevation: 3,
    margin: 10,
  },
  bold: {
    fontWeight: "bold",
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  ctrlButton: {
    flex: 1,
    borderRadius: 3,
  },
});
