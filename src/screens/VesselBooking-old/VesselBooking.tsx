import React, { Fragment } from "react";
import { StyleSheet, View, FlatList } from "react-native";
import {
  Card,
  Caption,
  Paragraph,
  Badge,
  Divider,
  IconButton,
  Button,
  FAB,
} from "react-native-paper";
import { getUserType, DateFormat2, TimeFormat } from "../../services/utils";
import Colors from "../../config/Colors";
import {
  Icon,
  Space,
  EmptyState,
  PlaceholderLoading,
  FilterButton,
} from "../../components";
import { OperatorStackProps } from "../Navigator";
import { useDispatch, useSelector } from "react-redux";
import {
  bookingFetchPage,
  bookingFilterClear,
} from "../../redux/bookingReducer";
import { RootState } from "../../redux/rootReducer";
import { shipperFetchAll } from "../../redux/shipperReducer";
import { msttrayekFetchAll } from "../../redux/msttrayekReducer";
import { supplierFetchAll } from "../../redux/supplierReducer";
import { portFetchAll } from "../../redux/portReducer";

function FloatingAdd({ navigation }: OperatorStackProps<"JadwalOperator">) {
  return (
    <FAB
      style={{
        position: "absolute",
        bottom: 10,
        right: 10,
        backgroundColor: Colors.pri,
      }}
      icon="plus"
      onPress={function () {
        navigation.navigate("CreateVesselBooking");
      }}
    />
  );
}
function FloatingFilter({ navigation }: OperatorStackProps<"JadwalOperator">) {
  return (
    <Button
      style={{
        position: "absolute",
        alignSelf: "center",
        bottom: 10,
        elevation: 10,
      }}
      icon="filter"
      mode="contained"
      color={Colors.white}
      onPress={function () {
        navigation.navigate("Modals", { screen: "SearchBookingModal" });
      }}
    >
      Filter
    </Button>
  );
}
export default function VesselBooking({ navigation }: any) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState | any) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          id: 0,
          usertype: "Not Defined",
        };
  });
  const { hasNext, currentPage } = useSelector(function (
    state: RootState | any
  ) {
    return state.booking.bookingGetSuccess;
  });
  const filter = useSelector(function (state: RootState) {
    return state.booking.bookingFilter;
  });
  const listBooking = useSelector(function (state: RootState) {
    return state.booking.bookingData ? state.booking.bookingData : [];
  });
  const listShipper = useSelector(function (state: RootState) {
    return state.shipper.shipperGetSuccess
      ? state.shipper.shipperGetSuccess.data
      : [];
  });
  const listSupplier = useSelector(function (state: RootState) {
    return state.supplier.supplierGetSuccess
      ? state.supplier.supplierGetSuccess.data
      : [];
  });
  const listTrayek = useSelector(function (state: RootState) {
    return state.msttrayek.msttrayekGetSuccess
      ? state.msttrayek.msttrayekGetSuccess.data
      : [];
  });
  const listPort = useSelector(function (state: RootState) {
    return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  });
  function B({ children }: any) {
    return <Paragraph style={styles.bold}>{children}</Paragraph>;
  }

  const getLoading = useSelector(function (state: RootState | any) {
    return state.booking.bookingGetLoading;
  });
  const { isShipper } = getUserType(usertype);
  const fetchData = (filter) => {
    const params = {
      start: 0,
      length: 10,
      shipper_id: usertype == "Shipper" ? id : "",
      operator_id: usertype == "Vessel Operator" ? id : "",
      ...filter,
    };
    dispatch(bookingFetchPage(params));
    dispatch(shipperFetchAll());
    dispatch(msttrayekFetchAll());
    dispatch(supplierFetchAll());
    dispatch(portFetchAll());
  };
  const handleReachEnd = (filter) => {
    const params = {
      length: 10,
      start: currentPage * 10,
      shipper_id: usertype == "Shipper" ? id : "",
      operator_id: usertype == "Vessel Operator" ? id : "",
      ...filter,
    };
    dispatch(bookingFetchPage(params));
  };
  const onRefresh = () => {
    dispatch(bookingFilterClear());
  };
  React.useEffect(() => {
    fetchData(filter);
  }, [filter]);
  return (
    <>
      <FlatList
        data={listBooking}
        keyExtractor={(item, key) => String(key)}
        refreshing={getLoading}
        onRefresh={onRefresh}
        onEndReached={() => {
          hasNext ? handleReachEnd(filter) : null;
        }}
        onEndReachedThreshold={0.5}
        ListFooterComponent={<Space height={80} />}
        renderItem={({ item }) => {
          const {
            kode_booking,
            kode_trayek,
            nama_pelabuhan_asal,
            nama_pelabuhan_tujuan,
            tanggal_berangkat,
            tanggal_tiba,
            request_kuota,
            type_container_name,
            status,
          }: any = item;
          return (
            <Card
              style={styles.card}
              onPress={() => navigation.navigate("ReadVesselBooking", { item })}
            >
              <B>{kode_booking}</B>
              {item.status == 1 && (
                <Badge style={styles.statusSecondary}>
                  Menunggu validasi operator
                </Badge>
              )}
              {item.status == 2 && item.bukti_pembayaran_file == null && (
                <Badge style={styles.statusSecondary}>
                  Menunggu pembayaran
                </Badge>
              )}
              {item.status == 2 && item.bukti_pembayaran_file != null && (
                <Badge style={styles.statusSecondary}>
                  Menunggu konfirmasi pembayaran
                </Badge>
              )}
              {item.status == 3 && (
                <Badge style={styles.statusSecondary}>
                  Pembayaran diterima
                </Badge>
              )}
              {item.status == 4 && (
                <Badge
                  style={{
                    backgroundColor: Colors.danger,
                    alignSelf: "flex-start",
                  }}
                >
                  Dibatalkan
                </Badge>
              )}
              {item.status == 5 && (
                <Badge style={styles.statusSuccess}>Selesai</Badge>
              )}
              {item.status == 6 && (
                <Badge style={styles.statusSuccess}>Sudah sampai tujuan</Badge>
              )}
              <Space height={5} />
              <Divider />
              <View style={{ flexDirection: "row" }}>
                {/** Rute */}
                <View style={{ flexDirection: "row", flex: 2 }}>
                  <View style={{ flex: 1 }}>
                    <B>
                      {nama_pelabuhan_asal}
                      {"\n"}
                      <Caption>
                        {DateFormat2(tanggal_berangkat)}
                        {"\n"}
                        {TimeFormat(tanggal_berangkat)}
                      </Caption>
                    </B>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <Icon name={"arrow-right"} style={{ fontSize: 20 }} />
                  </View>
                  <View style={{ flex: 1, alignItems: "flex-end" }}>
                    <B>
                      {nama_pelabuhan_tujuan}
                      {"\n"}
                      <Caption>
                        {DateFormat2(tanggal_tiba)}
                        {"\n"}
                        {TimeFormat(tanggal_tiba)}
                      </Caption>
                    </B>
                  </View>
                </View>

                {/** Trayek & Voyage */}
                <View
                  style={{
                    flex: 1,
                    alignItems: "flex-end",
                    justifyContent: "center",
                  }}
                >
                  <Paragraph>
                    <B>Trayek</B> {kode_trayek} {"\n"}
                    <B>Voyage</B> 4
                  </Paragraph>
                </View>
              </View>
              <Divider />
              {/** Kapal */}
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Icon
                  name={"buffer"}
                  style={{ fontSize: 20, color: Colors.pending }}
                />
                <Paragraph style={{ alignItems: "center" }}>
                  {" "}
                  Qty : {request_kuota} container {type_container_name}
                </Paragraph>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <Icon
                  name={"buffer"}
                  style={{ fontSize: 20, color: Colors.grayL }}
                />

                {item.is_fcl == 0 && (
                  <Paragraph> LCL (Less Container Loaded)</Paragraph>
                )}
                {item.is_fcl == 1 && (
                  <Paragraph> FCL (Full Container Loaded)</Paragraph>
                )}
              </View>
            </Card>
          );
        }}
        ListEmptyComponent={<EmptyState />}
      />

      {usertype === "Shipper" && (
        <FloatingAdd route="" navigation={navigation} />
      )}
      <FilterButton
        route=""
        navigation={navigation}
        onPress={function () {
          navigation.navigate("Modals", {
            screen: "VesselBookingFilterModal",
            params: {
              shipper: listShipper,
              supplier: listSupplier,
              port: listPort,
              msttrayek: listTrayek,
            },
          });
        }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  modalContainer: {
    flex: 1,
  },
  card: {
    padding: 10,
    borderRadius: 3,
    elevation: 3,
    margin: 10,
  },
  bold: {
    fontWeight: "bold",
  },
  center: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  ctrlButton: {
    flex: 1,
    borderRadius: 3,
  },
  statusSecondary: { backgroundColor: Colors.sec, alignSelf: "flex-start" },
  statusSuccess: { backgroundColor: Colors.success, alignSelf: "flex-start" },
});
