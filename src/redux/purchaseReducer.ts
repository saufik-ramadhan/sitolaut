import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { BASE_URL, PURCHASE_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  purchaseGetError: false,
  purchaseGetLoading: false,
  purchaseGetSuccess: false,
  purchaseViewError: false,
  purchaseViewLoading: false,
  purchaseViewSuccess: false,
  purchaseAddError: false,
  purchaseAddLoading: false,
  purchaseAddSuccess: false,
  purchaseDeleteError: false,
  purchaseDeleteLoading: false,
  purchaseDeleteSuccess: false,
  purchaseData: [],
  purchaseFilter: {},
};

/**
 * ACTION
 */
const setPurchaseGetError = createAction('setPurchaseGetError', (data: any) => data);
const setPurchaseGetLoading = createAction('setPurchaseGetLoading', (data: any) => data);
const setPurchaseGetSuccess = createAction('setPurchaseGetSuccess', (data: any) => data);
const setPurchaseViewError = createAction('setPurchaseViewError', (data: any) => data);
const setPurchaseViewLoading = createAction('setPurchaseViewLoading', (data: any) => data);
const setPurchaseViewSuccess = createAction('setPurchaseViewSuccess', (data: any) => data);
const setPurchaseAddError = createAction('setPurchaseAddError', (data: any) => data);
const setPurchaseAddLoading = createAction('setPurchaseAddLoading', (data: any) => data);
const setPurchaseAddSuccess = createAction('setPurchaseAddSuccess', (data: any) => data);
const setPurchaseDeleteError = createAction('setPurchaseDeleteError', (data: any) => data);
const setPurchaseDeleteLoading = createAction('setPurchaseDeleteLoading', (data: any) => data);
const setPurchaseDeleteSuccess = createAction('setPurchaseDeleteSuccess', (data: any) => data);

const setPurchaseData = createAction('setPurchaseData', (data: any) => data);
const setPurchaseFilterAdd = createAction('setPurchaseFilterAdd', (data: any) => data);
const setPurchaseFilterDel = createAction('setPurchaseFilterDel', (data: any) => data);
const setPurchaseFilterClear = createAction('setPurchaseFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const purchaseReducer = createReducer(
  {
    [setPurchaseGetError]: (state, data) => ({
      ...state,
      purchaseGetError: data,
    }),
    [setPurchaseGetLoading]: (state, data) => ({
      ...state,
      purchaseGetLoading: data,
    }),
    [setPurchaseGetSuccess]: (state, data) => ({
      ...state,
      purchaseGetSuccess: data,
    }),
    [setPurchaseViewError]: (state, data) => ({
      ...state,
      purchaseViewError: data,
    }),
    [setPurchaseViewLoading]: (state, data) => ({
      ...state,
      purchaseViewLoading: data,
    }),
    [setPurchaseViewSuccess]: (state, data) => ({
      ...state,
      purchaseViewSuccess: data,
    }),
    [setPurchaseAddError]: (state, data) => ({
      ...state,
      purchaseAddError: data,
    }),
    [setPurchaseAddLoading]: (state, data) => ({
      ...state,
      purchaseAddLoading: data,
    }),
    [setPurchaseAddSuccess]: (state, data) => ({
      ...state,
      purchaseAddSuccess: data,
    }),
    [setPurchaseDeleteError]: (state, data) => ({
      ...state,
      purchaseDeleteError: data,
    }),
    [setPurchaseDeleteLoading]: (state, data) => ({
      ...state,
      purchaseDeleteLoading: data,
    }),
    [setPurchaseDeleteSuccess]: (state, data) => ({
      ...state,
      purchaseDeleteSuccess: data,
    }),
    [setPurchaseData]: (state, data: Array<"any">) => ({
      ...state,
      purchaseData: data,
    }),
    [setPurchaseFilterAdd]: (state, data: any) => ({
      ...state,
      purchaseFilter: data,
    }),
    [setPurchaseFilterDel]: (state, id: any) => ({
      ...state,
      purchaseFilter: {},
    }),
    [setPurchaseFilterClear]: (state, data: any) => ({
      ...state,
      purchaseFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function purchaseFetchAll() {
  return async function (dispatch) {
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseGetSuccess(false));
    dispatch(setPurchaseGetError(false));
    dispatch(setPurchaseGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPurchaseGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPurchaseGetError(true));
        dispatch(setPurchaseGetLoading(false));
      });
  };
}

export function purchaseFetchSub(path) {
  return async function (dispatch) {
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseGetSuccess(false));
    dispatch(setPurchaseGetError(false));
    dispatch(setPurchaseGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPurchaseGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPurchaseGetError(true));
        dispatch(setPurchaseGetLoading(false));
      });
  };
}

export function purchaseFetchBooking(param) {
  return async function (dispatch) {
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseGetSuccess(false));
    dispatch(setPurchaseGetError(false));
    dispatch(setPurchaseGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(param),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL + "/booking", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPurchaseGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPurchaseGetError(true));
        dispatch(setPurchaseGetLoading(false));
      });
  };
}

export function purchaseFetchPage(param) {
  return async function (dispatch, getState) {
    dispatch(setPurchaseGetSuccess(false));
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseGetError(false));
    dispatch(setPurchaseGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(param),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().purchase.purchaseData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPurchaseData(data.data));
        } else {
          dispatch(setPurchaseData(prev.concat(data.data)));
        }
        dispatch(setPurchaseGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPurchaseGetError(true));
        dispatch(setPurchaseGetLoading(false));
        dispatch(setPurchaseGetLoading(false));
      });
  };
}

export function purchaseAdd(data) {
  return async function (dispatch) {
    dispatch(setPurchaseAddError(false));
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((purchase) => {
        if (purchase.success) {
          dispatch(setPurchaseAddSuccess(purchase));
        } else {
          dispatch(setPurchaseAddError(true));
          dispatch(setPurchaseAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setPurchaseAddError(true));
        dispatch(setPurchaseAddLoading(false));
      });
  };
}

export function purchaseUpdate(data) {
  return async function (dispatch) {
    dispatch(setPurchaseAddError(false));
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((purchase) => {
        if (purchase.success) {
          dispatch(setPurchaseAddSuccess(true));
          dispatch(setPurchaseAddLoading(false));
        } else {
          dispatch(setPurchaseAddError(true));
          dispatch(setPurchaseAddLoading(false));
          dispatch(setPurchaseAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setPurchaseAddError(true));
        dispatch(setPurchaseAddLoading(false));
        dispatch(setPurchaseAddLoading(false));
      });
  };
}
export function purchaseSupplierApproval(data) {
  return async function (dispatch) {
    dispatch(setPurchaseAddError(false));
    dispatch(setPurchaseViewSuccess(false));
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseViewLoading(true));
    dispatch(setPurchaseAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL + "/approve", config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((purchase) => {
        if (purchase.status == 200) {
          dispatch(setPurchaseAddSuccess(purchase));
        } else {
          dispatch(setPurchaseAddError(true));
          dispatch(setPurchaseAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setPurchaseAddError(true));
        dispatch(setPurchaseAddLoading(false));
      });
  };
}
export function purchaseSetFcl(data) {
  return async function (dispatch) {
    dispatch(setPurchaseAddError(false));
    dispatch(setPurchaseViewSuccess(false));
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL + "/set_fcl", config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((purchase) => {
        if (purchase.status == 200) {
          dispatch(setPurchaseAddSuccess(purchase));
        } else {
          dispatch(setPurchaseAddError(true));
          dispatch(setPurchaseAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setPurchaseAddError(true));
        dispatch(setPurchaseAddLoading(false));
      });
  };
}
export function purchaseShipperApproval(data) {
  return async function (dispatch) {
    dispatch(setPurchaseAddError(false));
    dispatch(setPurchaseViewSuccess(false));
    dispatch(setPurchaseAddSuccess(false));
    dispatch(setPurchaseAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL + "/approve_by_shipper", config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((purchase) => {
        if (purchase.status == 200) {
          dispatch(setPurchaseAddSuccess(purchase));
        } else {
          dispatch(setPurchaseAddError(true));
          dispatch(setPurchaseAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setPurchaseAddError(true));
        dispatch(setPurchaseAddLoading(false));
      });
  };
}

export function purchaseFetchOne(id) {
  return async function (dispatch) {
    dispatch(setPurchaseViewSuccess(false));
    dispatch(setPurchaseViewError(false));
    dispatch(setPurchaseViewSuccess(false));
    dispatch(setPurchaseViewLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL + "/view/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPurchaseViewSuccess(data));
        dispatch(setPurchaseViewLoading(false));
      })
      .catch((err) => {
        dispatch(setPurchaseViewError(true));
        dispatch(setPurchaseViewLoading(false));
      });
  };
}

export function purchaseDelete(id) {
  return async function (dispatch) {
    dispatch(setPurchaseViewSuccess(false));
    dispatch(setPurchaseDeleteError(false));
    dispatch(setPurchaseDeleteSuccess(false));
    dispatch(setPurchaseDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PURCHASE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPurchaseDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPurchaseDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setPurchaseDeleteError(true));
        dispatch(setPurchaseDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function purchaseFilterAdd(filter) {
  return function (dispatch) {
    dispatch(setPurchaseFilterAdd(filter));
  };
}

export function purchaseFilterDel(id) {
  return function (dispatch) {
    dispatch(setPurchaseFilterDel(id));
  };
}

export function purchaseFilterClear() {
  return function (dispatch) {
    dispatch(setPurchaseFilterClear());
  };
}

export default purchaseReducer;
