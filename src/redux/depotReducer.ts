import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, DEPOT_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  depotGetError: false,
  depotGetLoading: false,
  depotGetSuccess: false,
  depotViewError: false,
  depotViewLoading: false,
  depotViewSuccess: false,
  depotAddError: false,
  depotAddLoading: false,
  depotAddSuccess: false,
  depotDeleteError: false,
  depotDeleteLoading: false,
  depotDeleteSuccess: false,

  depotData: [],
  depotFilter: {},
};

/**
 * ACTION
 */

const setDepotGetError = createAction('setDepotGetError', (data: any) => data);
const setDepotGetLoading = createAction('setDepotGetLoading', (data: any) => data);
const setDepotGetSuccess = createAction('setDepotGetSuccess', (data: any) => data);
const setDepotViewError = createAction('setDepotViewError', (data: any) => data);
const setDepotViewLoading = createAction('setDepotViewLoading', (data: any) => data);
const setDepotViewSuccess = createAction('setDepotViewSuccess', (data: any) => data);
const setDepotAddError = createAction('setDepotAddError', (data: any) => data);
const setDepotAddLoading = createAction('setDepotAddLoading', (data: any) => data);
const setDepotAddSuccess = createAction('setDepotAddSuccess', (data: any) => data);
const setDepotDeleteError = createAction('setDepotDeleteError', (data: any) => data);
const setDepotDeleteLoading = createAction('setDepotDeleteLoading', (data: any) => data);
const setDepotDeleteSuccess = createAction('setDepotDeleteSuccess', (data: any) => data);

const setDepotData = createAction('setDepotData', (data: any) => data);
const setDepotFilterAdd = createAction('setDepotFilterAdd', (data: any) => data);
const setDepotFilterDel = createAction('setDepotFilterDel', (data: any) => data);
const setDepotFilterClear = createAction('setDepotFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const depotReducer = createReducer(
  {
    [setDepotGetError]: (state, data) => ({ ...state, depotGetError: data }),
    [setDepotGetLoading]: (state, data) => ({
      ...state,
      depotGetLoading: data,
    }),
    [setDepotGetSuccess]: (state, data) => ({
      ...state,
      depotGetSuccess: data,
    }),
    [setDepotViewError]: (state, data) => ({ ...state, depotViewError: data }),
    [setDepotViewLoading]: (state, data) => ({
      ...state,
      depotViewLoading: data,
    }),
    [setDepotViewSuccess]: (state, data) => ({
      ...state,
      depotViewSuccess: data,
    }),
    [setDepotAddError]: (state, data) => ({ ...state, depotAddError: data }),
    [setDepotAddLoading]: (state, data) => ({
      ...state,
      depotAddLoading: data,
    }),
    [setDepotAddSuccess]: (state, data) => ({
      ...state,
      depotAddSuccess: data,
    }),
    [setDepotDeleteError]: (state, data) => ({
      ...state,
      depotDeleteError: data,
    }),
    [setDepotDeleteLoading]: (state, data) => ({
      ...state,
      depotDeleteLoading: data,
    }),
    [setDepotDeleteSuccess]: (state, data) => ({
      ...state,
      depotDeleteSuccess: data,
    }),

    [setDepotData]: (state, data: Array<"any">) => ({
      ...state,
      depotData: data,
    }),
    [setDepotFilterAdd]: (state, data: any) => ({
      ...state,
      depotFilter: data,
    }),
    [setDepotFilterDel]: (state, id: any) => ({
      ...state,
      depotFilter: {},
    }),
    [setDepotFilterClear]: (state, data: any) => ({
      ...state,
      depotFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function depotFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setDepotAddSuccess(false));
    dispatch(setDepotGetSuccess(false));
    dispatch(setDepotGetError(false));
    dispatch(setDepotGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().depot.depotData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setDepotData(data.data));
        } else {
          dispatch(setDepotData(prev.concat(data.data)));
        }
        dispatch(setDepotGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDepotGetError(true));
        dispatch(setDepotGetLoading(false));
      });
  };
}

export function depotFetchSub(data) {
  return async function (dispatch, getState) {
    dispatch(setDepotAddSuccess(false));
    dispatch(setDepotGetSuccess(false));
    dispatch(setDepotGetError(false));
    dispatch(setDepotGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DEPOT_URL + "/sub", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().depot.depotData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setDepotData(data.data));
        } else {
          dispatch(setDepotData(prev.concat(data.data)));
        }
        dispatch(setDepotGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDepotGetError(true));
        dispatch(setDepotGetLoading(false));
      });
  };
}

export function depotFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setDepotAddSuccess(false));
    dispatch(setDepotGetSuccess(false));
    dispatch(setDepotGetError(false));
    dispatch(setDepotGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DEPOT_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().depot.depotData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setDepotData(data.data));
        } else {
          dispatch(setDepotData(prev.concat(data.data)));
        }
        dispatch(setDepotGetSuccess(data));
        dispatch(setDepotGetLoading(false));
      })
      .catch((err) => {
        dispatch(setDepotGetError(true));
        dispatch(setDepotGetLoading(false));
      });
  };
}

export function depotSearch(params) {
  return async function (dispatch, getState) {
    dispatch(setDepotAddSuccess(false));
    dispatch(setDepotGetSuccess(false));
    dispatch(setDepotGetError(false));
    dispatch(setDepotGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DEPOT_URL + "/search", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().depot.depotData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setDepotData(data.data));
        } else {
          dispatch(setDepotData(prev.concat(data.data)));
        }
        dispatch(setDepotGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDepotGetError(true));
        dispatch(setDepotGetLoading(false));
      });
  };
}

export function depotAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setDepotAddError(false));
    dispatch(setDepotAddSuccess(false));
    dispatch(setDepotAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((depot) => {
        dispatch(setDepotAddSuccess(depot));
      })
      .catch((err) => {
        dispatch(setDepotAddError(true));
        dispatch(setDepotAddLoading(false));
      });
  };
}

export function depotFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setDepotAddSuccess(false));
    dispatch(setDepotViewError(false));
    dispatch(setDepotViewSuccess(false));
    dispatch(setDepotViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DEPOT_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDepotViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setDepotViewError(true));
        dispatch(setDepotViewLoading(false));
      });
  };
}

export function depotUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setDepotAddError(false));
    dispatch(setDepotAddSuccess(false));
    dispatch(setDepotAddLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((depot) => {
        if (depot.success) {
          dispatch(setDepotAddSuccess(depot));
        } else {
          dispatch(setDepotAddError(true));
          dispatch(setDepotAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setDepotAddError(true));
        dispatch(setDepotAddLoading(false));
        alert("error");
      });
  };
}

export function depotDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setDepotViewSuccess(false));
    dispatch(setDepotDeleteError(false));
    dispatch(setDepotDeleteSuccess(false));
    dispatch(setDepotDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDepotDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDepotDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setDepotDeleteError(true));
        dispatch(setDepotDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function depotFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setDepotFilterAdd(filter));
  };
}

export function depotFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setDepotFilterDel(id));
  };
}

export function depotFilterClear() {
  return function (dispatch, getState) {
    dispatch(setDepotFilterClear());
  };
}

export default depotReducer;
