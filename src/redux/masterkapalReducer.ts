import { doLogout } from './authReducers'
import { 
  BASE_URL,
  KAPAL_URL,
  LIST_OPERATOR_URL,
  MASTER_KAPAL_OPERATOR_URL,
} from './../config/constants';
import { createAction, createReducer } from 'redux-act'
import AsyncStorage from '../AsyncStorage';

const DefaultState = {
  kapalGetLoading: false || {},
  kapalGetSuccess: false || {},
  kapalGetFailure: false || {},

  kapalReadLoading: false || {},
  kapalReadSuccess: false || {},
  kapalReadFailure: false || {},

  kapalAddLoading: false || {},
  kapalAddSuccess: false || {},
  kapalAddFailure: false || {},

  kapalEditLoading: false || {},
  kapalEditSuccess: false || {},
  kapalEditFailure: false || {},

  kapalDelLoading: false || {},
  kapalDelSuccess: false || {},
  kapalDelFailure: false || {},

  listOperatorGetLoading: false || {},
  listOperatorGetSuccess: false || {},
  listOperatorGetFailure: false || {},
}

// Create Action
const setKapalGetLoading = createAction('setKapalGetLoading', (data: any) => data);
const setKapalGetSuccess = createAction('setKapalGetSuccess', (data: any) => data);
const setKapalGetFailure = createAction('setKapalGetFailure', (data: any) => data);

const setKapalReadLoading = createAction('setKapalReadLoading', (data: any) => data);
const setKapalReadSuccess = createAction('setKapalReadSuccess', (data: any) => data);
const setKapalReadFailure = createAction('setKapalReadFailure', (data: any) => data);

const setKapalAddLoading = createAction('setKapalAddLoading', (data: any) => data);
const setKapalAddSuccess = createAction('setKapalAddSuccess', (data: any) => data);
const setKapalAddFailure = createAction('setKapalAddFailure', (data: any) => data);

const setKapalEditLoading = createAction('setKapalEditLoading', (data: any) => data);
const setKapalEditSuccess = createAction('setKapalEditSuccess', (data: any) => data);
const setKapalEditFailure = createAction('setKapalEditFailure', (data: any) => data);

const setKapalDelLoading = createAction('setKapalDelLoading', (data: any) => data);
const setKapalDelSuccess = createAction('setKapalDelSuccess', (data: any) => data);
const setKapalDelFailure = createAction('setKapalDelFailure', (data: any) => data);

const setListOperatorGetLoading = createAction('setListOperatorGetLoading', (data: any) => data);
const setListOperatorGetSuccess = createAction('setListOperatorGetSuccess', (data: any) => data);
const setListOperatorGetFailure = createAction('setListOperatorGetFailure', (data: any) => data);

// Create Reducer
const masterkapalReducer = createReducer({
  [setKapalGetLoading]: (state, data) => ({...state, kapalGetLoading: data}),
  [setKapalGetSuccess]: (state, data) => ({...state, kapalGetSuccess: data}),
  [setKapalGetFailure]: (state, data) => ({...state, kapalGetFailure: data}),

  [setKapalReadLoading]: (state, data) => ({...state, kapalReadLoading: data}),
  [setKapalReadSuccess]: (state, data) => ({...state, kapalReadSuccess: data}),
  [setKapalReadFailure]: (state, data) => ({...state, kapalReadFailure: data}),

  [setKapalAddLoading]: (state, data) => ({...state, kapalAddLoading: data}),
  [setKapalAddSuccess]: (state, data) => ({...state, kapalAddSuccess: data}),
  [setKapalAddFailure]: (state, data) => ({...state, kapalAddFailure: data}),

  [setKapalEditLoading]: (state, data) => ({...state, kapalEditLoading: data}),
  [setKapalEditSuccess]: (state, data) => ({...state, kapalEditSuccess: data}),
  [setKapalEditFailure]: (state, data) => ({...state, kapalEditFailure: data}),

  [setKapalDelLoading]: (state, data) => ({...state, kapalDelLoading: data}),
  [setKapalDelSuccess]: (state, data) => ({...state, kapalDelSuccess: data}),
  [setKapalDelFailure]: (state, data) => ({...state, kapalDelFailure: data}),

  [setListOperatorGetLoading]: (state, data) => ({...state, listOperatorGetLoading: data}),
  [setListOperatorGetSuccess]: (state, data) => ({...state, listOperatorGetSuccess: data}),
  [setListOperatorGetFailure]: (state, data) => ({...state, listOperatorGetFailure: data}),
}, DefaultState)

/**
 * List Semua Operator
 */
export const listOperatorFetchAll = () => {
  return async (dispatch) => {
    dispatch(setListOperatorGetLoading(true));
    dispatch(setListOperatorGetSuccess(false));
    let token = await AsyncStorage.getItem("token@lcs");
    let config = {}
    if(token) {
      config = {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      }
    } else {
      throw Error("Authentication error")
    }
    fetch(BASE_URL + LIST_OPERATOR_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(setListOperatorGetLoading(false))
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setListOperatorGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setListOperatorGetFailure(err));
        dispatch(setListOperatorGetLoading(false))
        console.error(err, 'Ada yang salah')
      })
  }
}


/**
 * Menambahkan Master Kapal Baru
 * @param data
 */
export const addMasterKapal = data => {
  return async (dispatch) => {
    dispatch(setKapalAddSuccess(false));
    dispatch(setKapalAddLoading(true));
    let token = await AsyncStorage.getItem("token@lcs");
    let config = {}
    if(token) {
      config = {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    } else {
      throw Error("Authentication error")
    }
    fetch(BASE_URL + KAPAL_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(setKapalAddLoading(false))
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setKapalAddSuccess(data));
        if(data.status === 200){
          dispatch(masterKapalFetchAll())
        }
      })
      .catch((err) => {
        dispatch(setKapalAddFailure(err));
        dispatch(setKapalAddLoading(false))
        console.error(err, 'err cuk');
      })
  }
}


/**
 * Hapus Master Kapal
 * @param prop 
 */
export const delMasterKapal = prop => {
  return async (dispatch) => {
    dispatch(setKapalDelLoading(true))
    dispatch(setKapalDelSuccess(false))
    let token = await AsyncStorage.getItem("token@lcs");
    let config = {}
    if(token) {
      config = {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
      }
    } else {
      throw Error("Authentication error")
    }
    fetch(BASE_URL + KAPAL_URL +`/${prop.id}`, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(setKapalDelLoading(false));
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setKapalDelSuccess(data));
        if(data.status === 200){
          dispatch(masterKapalFetchAll())
        }
      })
      .catch((err) => {
        console.log(err, 'err cuk')
        dispatch(setKapalDelLoading(false));
        dispatch(setKapalDelFailure(err));
      })
  }
}


/**
 * Edit Master Kapal
 * @param data 
 */
export const EditMasterKapal = data => {
  return async (dispatch) => {
    dispatch(setKapalEditLoading(true));
    dispatch(setKapalEditSuccess(false));

    let token = await AsyncStorage.getItem("token@lcs");
    let config = {}
    if(token) {
      config = {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    } else {
      throw Error("Authentication error")
    }
    fetch(BASE_URL + KAPAL_URL +`/${data.id}`, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(setKapalEditLoading(false));
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setKapalEditSuccess(data));
        if(data.status === 200){
          dispatch(masterKapalFetchAll())
        }
      })
      .catch((err) => {
        console.log(err, 'err cuk')
        dispatch(setKapalEditLoading(false));
        dispatch(setKapalEditFailure(true));

      })
  }
}


/**
 * List Kapal Berdasarkan Operator yang dipilih
 * @param data 
 */
export const listKapalOperator = id => {
  return async (dispatch) => {
    dispatch(setKapalGetLoading(true));
    dispatch(setKapalGetSuccess(false));
    let token = await AsyncStorage.getItem("token@lcs");
    // let id = localStorage.getItem('tolaut@userId') || null
    let config = {}
    if(token) {
        config = {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
          }
        }
      } else {
        throw Error("Authentication error")
      }
      fetch(BASE_URL + MASTER_KAPAL_OPERATOR_URL+`/${id}`, config)
        .then((response) => {
          if (response.status === 401) {
            dispatch(doLogout())
          }
          if (!response.ok) {
            throw Error(response.statusText)
          }
          dispatch(setKapalGetLoading(false))
          return response
        })
        .then((response) => response.json())
        .then((data) => {
          dispatch(setKapalGetSuccess(data));
        })
        .catch((err) => {
          console.log(err, 'err cuk')
          dispatch(setKapalGetLoading(false))
          dispatch(setKapalGetFailure(err))
        })
  }
}


/**
 * Filter Kapal
 * @param data 
 */
export const masterKaplaFilter = data => {
  return async (dispatch) => {
    dispatch(setKapalGetSuccess(false));
    let token = await AsyncStorage.getItem("token@lcs");
    let config = {}
        if(token) {
            config = {
              method: 'GET',
              headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
              }
            }
          } else {
            throw Error("Authentication error")
          }
          fetch(BASE_URL + KAPAL_URL+`?nama_kapal=${data.nama_kapal}&nomor_kontrak=${data.nomor_kontrak}`, config)
            .then((response) => {
              if (response.status === 401) {
                dispatch(doLogout())
              }
              if (!response.ok) {
                throw Error(response.statusText)
              }
              dispatch(setKapalGetLoading(false))
              return response
            })
            .then((response) => response.json())
            .then((data) => {
              dispatch(setKapalGetSuccess(data))
            })
            .catch((err) => {
              console.log(err, 'err cuk')
              dispatch(setKapalGetSuccess(false))
              dispatch(setKapalGetLoading(false))
            })
  }
}


/**
 * Fetch Semua Master Kapal
 */
export function masterKapalFetchAll() {
    return async (dispatch) => {
        dispatch(setKapalGetSuccess(false));
        let token = await AsyncStorage.getItem("token@lcs");
        let config = {}
        if(token) {
            config = {
              method: 'GET',
              headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
              }
            }
          } else {
            throw Error("Authentication error")
          }
          fetch(BASE_URL + KAPAL_URL, config)
          .then((response) => {
            if (response.status === 401) {
              dispatch(doLogout())
            }
            if (!response.ok) {
              throw Error(response.statusText)
            }
            dispatch(setKapalGetLoading(false))
            return response
          })
          .then((response) => response.json())
          .then((data) => {
            dispatch(setKapalGetSuccess(data))
          })
          .catch((err) => {
            console.error(err, 'err cuk')
            dispatch(setKapalGetFailure(true))
            dispatch(setKapalGetLoading(false))
          })
    }
}

export default masterkapalReducer;