import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import { BASE_URL, SUPPLIER_URL, SHIPPER_URL } from "./../config/constants";
import FetchRetry from "../services/FetchRetry";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  shipperGetError: false,
  shipperGetLoading: false,
  shipperGetSuccess: false,
  shipperViewError: false,
  shipperViewLoading: false,
  shipperViewSuccess: false,
  shipperAddError: false,
  shipperAddLoading: false,
  shipperAddInvalid: false,
  shipperAddSuccess: false,
  shipperDeleteError: false,
  shipperDeleteLoading: false,
  shipperDeleteSuccess: false,

  shipperData: [],
  shipperFilter: {},
};

/**
 * ACTION
 */
const setShipperGetError = createAction('setShipperGetError', (data: any) => data);
const setShipperGetLoading = createAction('setShipperGetLoading', (data: any) => data);
const setShipperGetSuccess = createAction('setShipperGetSuccess', (data: any) => data);
const setShipperViewError = createAction('setShipperViewError', (data: any) => data);
const setShipperViewLoading = createAction('setShipperViewLoading', (data: any) => data);
const setShipperViewSuccess = createAction('setShipperViewSuccess', (data: any) => data);
const setShipperAddError = createAction('setShipperAddError', (data: any) => data);
const setShipperAddLoading = createAction('setShipperAddLoading', (data: any) => data);
const setShipperAddInvalid = createAction('setShipperAddInvalid', (data: any) => data);
const setShipperAddSuccess = createAction('setShipperAddSuccess', (data: any) => data);
const setShipperDeleteError = createAction('setShipperDeleteError', (data: any) => data);
const setShipperDeleteLoading = createAction('setShipperDeleteLoading', (data: any) => data);
const setShipperDeleteSuccess = createAction('setShipperDeleteSuccess', (data: any) => data);

const setShipperData = createAction('setShipperData', (data: any) => data);
const setShipperFilterAdd = createAction('setShipperFilterAdd', (data: any) => data);
const setShipperFilterDel = createAction('setShipperFilterDel', (data: any) => data);
const setShipperFilterClear = createAction('setShipperFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const shipperReducer = createReducer(
  {
    [setShipperGetError]: (state, data) => ({
      ...state,
      shipperGetError: data,
    }),
    [setShipperGetLoading]: (state, data) => ({
      ...state,
      shipperGetLoading: data,
    }),
    [setShipperGetSuccess]: (state, data) => ({
      ...state,
      shipperGetSuccess: data,
    }),
    [setShipperViewError]: (state, data) => ({
      ...state,
      shipperViewError: data,
    }),
    [setShipperViewLoading]: (state, data) => ({
      ...state,
      shipperViewLoading: data,
    }),
    [setShipperViewSuccess]: (state, data) => ({
      ...state,
      shipperViewSuccess: data,
    }),
    [setShipperAddError]: (state, data) => ({
      ...state,
      shipperAddError: data,
    }),
    [setShipperAddLoading]: (state, data) => ({
      ...state,
      shipperAddLoading: data,
    }),
    [setShipperAddInvalid]: (state, data) => ({
      ...state,
      shipperAddInvalid: data,
    }),
    [setShipperAddSuccess]: (state, data) => ({
      ...state,
      shipperAddSuccess: data,
    }),
    [setShipperDeleteError]: (state, data) => ({
      ...state,
      shipperDeleteError: data,
    }),
    [setShipperDeleteLoading]: (state, data) => ({
      ...state,
      shipperDeleteLoading: data,
    }),
    [setShipperDeleteSuccess]: (state, data) => ({
      ...state,
      shipperDeleteSuccess: data,
    }),

    [setShipperData]: (state, data: Array<"any">) => ({
      ...state,
      shipperData: data,
    }),
    [setShipperFilterAdd]: (state, data: any) => ({
      ...state,
      shipperFilter: data,
    }),
    [setShipperFilterDel]: (state, id: any) => ({
      ...state,
      shipperFilter: {},
    }),
    [setShipperFilterClear]: (state, data: any) => ({
      ...state,
      shipperFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function shipperFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setShipperGetSuccess(false));
    dispatch(setShipperGetError(false));
    dispatch(setShipperGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().shipper.shipperData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setShipperData(data.data));
        } else {
          dispatch(setShipperData(prev.concat(data.data)));
        }
        dispatch(setShipperGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperGetError(true));
      });
  };
}

export function shipperFetchSub(shipper_id) {
  return async function (dispatch, getState) {
    dispatch(setShipperGetSuccess(false));
    dispatch(setShipperGetError(false));
    dispatch(setShipperGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL + "/" + shipper_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().shipper.shipperData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setShipperData(data.data));
        } else {
          dispatch(setShipperData(prev.concat(data.data)));
        }
        dispatch(setShipperGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperGetError(true));
      });
  };
}

export function shipperFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setShipperGetSuccess(false));
    dispatch(setShipperGetError(false));
    dispatch(setShipperGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().shipper.shipperData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setShipperData(data.data));
        } else {
          dispatch(setShipperData(prev.concat(data.data)));
        }
        dispatch(setShipperGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperGetError(true));
      });
  };
}

export function shipperClear() {
  return async function (dispatch, getState) {
    dispatch(setShipperGetSuccess(false));
    dispatch(setShipperGetError(false));
    dispatch(setShipperGetLoading(true));
  };
}

export function shipperPriceFetchAll(item) {
  return async function (dispatch, getState) {
    dispatch(setShipperGetSuccess(false));
    dispatch(setShipperGetError(false));
    dispatch(setShipperGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL + "/get_biaya_pengurusan", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().shipper.shipperData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setShipperData(data.data));
        } else {
          dispatch(setShipperData(prev.concat(data.data)));
        }
        dispatch(setShipperGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperGetError(true));
      });
  };
}

export function shipperAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setShipperAddError(false));
    dispatch(setShipperAddSuccess(false));
    dispatch(setShipperAddInvalid(false));
    dispatch(setShipperAddLoading(true));

    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
    config = {
      method: "POST",
      body: formData,
    };
    fetch(BASE_URL + SHIPPER_URL + "/new", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((shipper) => {
        if (shipper.success) {
          dispatch(setShipperAddSuccess(shipper));
        } else {
          dispatch(setShipperAddInvalid(shipper));
        }
      })
      .catch((err) => {
        dispatch(setShipperAddError(true));
        dispatch(setShipperAddLoading(false));
        alert("Error");
      });
  };
}

export function shipperUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setShipperAddError(false));
    dispatch(setShipperAddSuccess(false));
    dispatch(setShipperAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((shipper) => {
        dispatch(setShipperAddSuccess(shipper));
      })
      .catch((err) => {
        dispatch(setShipperAddError(true));
      });
  };
}

export function shipperConfirm(data) {
  return async function (dispatch, getState) {
    dispatch(setShipperAddError(false));
    dispatch(setShipperAddSuccess(false));
    dispatch(setShipperAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL + "/approve", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((shipper) => {
        if (shipper.status === "200 OK") {
          dispatch(setShipperAddSuccess(shipper));
        } else {
          dispatch(setShipperAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setShipperAddError(true));
      });
  };
}

export function shipperFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setShipperViewError(false));
    dispatch(setShipperViewSuccess(false));
    dispatch(setShipperViewLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setShipperViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperViewError(true));
      });
  };
}

export function shipperFetchUser(id) {
  return async function (dispatch, getState) {
    dispatch(setShipperViewError(false));
    dispatch(setShipperViewSuccess(false));
    dispatch(setShipperViewLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL + "/profile", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setShipperViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperViewError(true));
      });
  };
}

export function handleStatus(data) {
  return async function (dispatch, getState) {
    dispatch(setShipperAddError(false));
    dispatch(setShipperAddSuccess(false));
    dispatch(setShipperAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "register/" + data.status, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((shipper) => {
        dispatch(setShipperAddSuccess(shipper));
      })
      .catch((err) => {
        dispatch(setShipperAddError(true));
      });
  };
}

export function shipperDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setShipperViewSuccess(false));
    dispatch(setShipperDeleteError(false));
    dispatch(setShipperDeleteSuccess(false));
    dispatch(setShipperDeleteLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setShipperDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function shipperFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setShipperFilterAdd(filter));
  };
}

export function shipperFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setShipperFilterDel(id));
  };
}

export function shipperFilterClear() {
  return function (dispatch, getState) {
    dispatch(setShipperFilterClear());
  };
}

export default shipperReducer;
export { setShipperAddSuccess };
