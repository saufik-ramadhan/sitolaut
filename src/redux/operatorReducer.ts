import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import { BASE_URL, OPERATOR_URL } from "./../config/constants";
import FetchRetry from "../services/FetchRetry";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  operatorGetError: false,
  operatorGetLoading: false,
  operatorGetSuccess: false,
  operatorViewError: false,
  operatorViewLoading: false,
  operatorViewSuccess: false,
  operatorAddError: false,
  operatorAddLoading: false,
  operatorAddInvalid: false,
  operatorAddSuccess: false,
  operatorDeleteError: false,
  operatorDeleteLoading: false,
  operatorDeleteSuccess: false,

  operatorData: [],
  operatorFilter: {},
};

/**
 * ACTION
 */
const setOperatorGetError = createAction('setOperatorGetError', (data: any) => data);
const setOperatorGetLoading = createAction('setOperatorGetLoading', (data: any) => data);
const setOperatorGetSuccess = createAction('setOperatorGetSuccess', (data: any) => data);
const setOperatorViewError = createAction('setOperatorViewError', (data: any) => data);
const setOperatorViewLoading = createAction('setOperatorViewLoading', (data: any) => data);
const setOperatorViewSuccess = createAction('setOperatorViewSuccess', (data: any) => data);
const setOperatorAddError = createAction('setOperatorAddError', (data: any) => data);
const setOperatorAddLoading = createAction('setOperatorAddLoading', (data: any) => data);
const setOperatorAddInvalid = createAction('setOperatorAddInvalid', (data: any) => data);
const setOperatorAddSuccess = createAction('setOperatorAddSuccess', (data: any) => data);
const setOperatorDeleteError = createAction('setOperatorDeleteError', (data: any) => data);
const setOperatorDeleteLoading = createAction('setOperatorDeleteLoading', (data: any) => data);
const setOperatorDeleteSuccess = createAction('setOperatorDeleteSuccess', (data: any) => data);

const setOperatorData = createAction('setOperatorData', (data: any) => data);
const setOperatorFilterAdd = createAction('setOperatorFilterAdd', (data: any) => data);
const setOperatorFilterDel = createAction('setOperatorFilterDel', (data: any) => data);
const setOperatorFilterClear = createAction('setOperatorFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const operatorReducer = createReducer(
  {
    [setOperatorGetError]: (state, data) => ({
      ...state,
      operatorGetError: data,
    }),
    [setOperatorGetLoading]: (state, data) => ({
      ...state,
      operatorGetLoading: data,
    }),
    [setOperatorGetSuccess]: (state, data) => ({
      ...state,
      operatorGetSuccess: data,
    }),
    [setOperatorViewError]: (state, data) => ({
      ...state,
      operatorViewError: data,
    }),
    [setOperatorViewLoading]: (state, data) => ({
      ...state,
      operatorViewLoading: data,
    }),
    [setOperatorViewSuccess]: (state, data) => ({
      ...state,
      operatorViewSuccess: data,
    }),
    [setOperatorAddError]: (state, data) => ({
      ...state,
      operatorAddError: data,
    }),
    [setOperatorAddLoading]: (state, data) => ({
      ...state,
      operatorAddLoading: data,
    }),
    [setOperatorAddInvalid]: (state, data) => ({
      ...state,
      operatorAddInvalid: data,
    }),
    [setOperatorAddSuccess]: (state, data) => ({
      ...state,
      operatorAddSuccess: data,
    }),
    [setOperatorDeleteError]: (state, data) => ({
      ...state,
      operatorDeleteError: data,
    }),
    [setOperatorDeleteLoading]: (state, data) => ({
      ...state,
      operatorDeleteLoading: data,
    }),
    [setOperatorDeleteSuccess]: (state, data) => ({
      ...state,
      operatorDeleteSuccess: data,
    }),

    [setOperatorData]: (state, data: Array<"any">) => ({
      ...state,
      operatorData: data,
    }),
    [setOperatorFilterAdd]: (state, data: any) => ({
      ...state,
      operatorFilter: data,
    }),
    [setOperatorFilterDel]: (state, id: any) => ({
      ...state,
      operatorFilter: {},
    }),
    [setOperatorFilterClear]: (state, data: any) => ({
      ...state,
      operatorFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function operatorFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setOperatorGetSuccess(false));
    dispatch(setOperatorGetError(false));
    dispatch(setOperatorGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().operator.operatorData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setOperatorData(data.data));
        } else {
          dispatch(setOperatorData(prev.concat(data.data)));
        }
        dispatch(setOperatorGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorGetError(true));
      });
  };
}

export function operatorFetchSub(operator_id) {
  return async function (dispatch, getState) {
    dispatch(setOperatorGetSuccess(false));
    dispatch(setOperatorGetError(false));
    dispatch(setOperatorGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL + "/" + operator_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().operator.operatorData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setOperatorData(data.data));
        } else {
          dispatch(setOperatorData(prev.concat(data.data)));
        }
        dispatch(setOperatorGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorGetError(true));
      });
  };
}

export function operatorFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setOperatorGetSuccess(false));
    dispatch(setOperatorGetError(false));
    dispatch(setOperatorGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().operator.operatorData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setOperatorData(data.data));
        } else {
          dispatch(setOperatorData(prev.concat(data.data)));
        }
        dispatch(setOperatorGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorGetError(true));
      });
  };
}

export function operatorAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setOperatorAddError(false));
    dispatch(setOperatorAddSuccess(false));
    dispatch(setOperatorAddInvalid(false));
    dispatch(setOperatorAddLoading(true));
    alert(JSON.stringify(data));
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      body: formData,
    };
    fetch(BASE_URL + OPERATOR_URL + "/new", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((operator) => {
        if (operator.success) {
          dispatch(setOperatorAddSuccess(operator));
          alert("Success");
        } else {
          dispatch(setOperatorAddInvalid(operator));
        }
      })
      .catch((err) => {
        dispatch(setOperatorAddError(true));
        dispatch(setOperatorAddLoading(false));
        alert("Error");
      });
  };
}

export function operatorUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setOperatorAddError(false));
    dispatch(setOperatorAddSuccess(false));
    dispatch(setOperatorAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((operator) => {
        dispatch(setOperatorAddSuccess(operator));
      })
      .catch((err) => {
        dispatch(setOperatorAddError(true));
      });
  };
}

export function operatorConfirm(data) {
  return async function (dispatch, getState) {
    dispatch(setOperatorAddError(false));
    dispatch(setOperatorAddSuccess(false));
    dispatch(setOperatorAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL + "/approve", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((operator) => {
        if (operator.status === "200 OK") {
          dispatch(setOperatorAddSuccess(operator));
        } else {
          dispatch(setOperatorAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setOperatorAddError(true));
      });
  };
}

export function operatorFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setOperatorViewError(false));
    dispatch(setOperatorViewSuccess(false));
    dispatch(setOperatorViewLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOperatorViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorViewError(true));
      });
  };
}

export function operatorFetchUser(id) {
  return async function (dispatch, getState) {
    dispatch(setOperatorViewError(false));
    dispatch(setOperatorViewSuccess(false));
    dispatch(setOperatorViewLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL + "/profile", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOperatorViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorViewError(true));
      });
  };
}

export function handleStatus(data) {
  return async function (dispatch, getState) {
    dispatch(setOperatorAddError(false));
    dispatch(setOperatorAddSuccess(false));
    dispatch(setOperatorAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "register/" + data.status, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((operator) => {
        dispatch(setOperatorAddSuccess(operator));
      })
      .catch((err) => {
        dispatch(setOperatorAddError(true));
      });
  };
}

export function operatorDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setOperatorViewSuccess(false));
    dispatch(setOperatorDeleteError(false));
    dispatch(setOperatorDeleteSuccess(false));
    dispatch(setOperatorDeleteLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOperatorDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function operatorFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setOperatorFilterAdd(filter));
  };
}

export function operatorFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setOperatorFilterDel(id));
  };
}

export function operatorFilterClear() {
  return function (dispatch, getState) {
    dispatch(setOperatorFilterClear());
  };
}

export default operatorReducer;
export { setOperatorAddSuccess };
