import { createAction, createReducer } from "redux-act";
import { BASE_URL, MSTTRAYEK_URL } from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import { Alert } from "react-native";

/**
 * Default State
 */
const DefaultState = {
  msttrayekGetError: false,
  msttrayekGetLoading: false,
  msttrayekGetSuccess: false,
  msttrayekViewError: false,
  msttrayekViewLoading: false,
  msttrayekViewSuccess: false,
  msttrayekAddError: false,
  msttrayekAddLoading: false,
  msttrayekAddSuccess: false,
  msttrayekDeleteError: false,
  msttrayekDeleteLoading: false,
  msttrayekDeleteSuccess: false,

  msttrayekData: [],
  msttrayekFilter: {},
};

/**
 * ACTION
 */
const setMsttrayekGetError = createAction('setMsttrayekGetError', (data: any) => data);
const setMsttrayekGetLoading = createAction('setMsttrayekGetLoading', (data: any) => data);
const setMsttrayekGetSuccess = createAction('setMsttrayekGetSuccess', (data: any) => data);
const setMsttrayekViewError = createAction('setMsttrayekViewError', (data: any) => data);
const setMsttrayekViewLoading = createAction('setMsttrayekViewLoading', (data: any) => data);
const setMsttrayekViewSuccess = createAction('setMsttrayekViewSuccess', (data: any) => data);
const setMsttrayekAddError = createAction('setMsttrayekAddError', (data: any) => data);
const setMsttrayekAddLoading = createAction('setMsttrayekAddLoading', (data: any) => data);
const setMsttrayekAddSuccess = createAction('setMsttrayekAddSuccess', (data: any) => data);
const setMsttrayekDeleteError = createAction('setMsttrayekDeleteError', (data: any) => data);
const setMsttrayekDeleteLoading = createAction('setMsttrayekDeleteLoading', (data: any) => data);
const setMsttrayekDeleteSuccess = createAction('setMsttrayekDeleteSuccess', (data: any) => data);

const setMsttrayekData = createAction('setMsttrayekData', (data: any) => data);
const setMsttrayekFilterAdd = createAction('setMsttrayekFilterAdd', (data: any) => data);
const setMsttrayekFilterDel = createAction('setMsttrayekFilterDel', (data: any) => data);
const setMsttrayekFilterClear = createAction('setMsttrayekFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const msttrayekReducer = createReducer(
  {
    [setMsttrayekGetError]: (state, data) => ({
      ...state,
      msttrayekGetError: data,
    }),
    [setMsttrayekGetLoading]: (state, data) => ({
      ...state,
      msttrayekGetLoading: data,
    }),
    [setMsttrayekGetSuccess]: (state, data) => ({
      ...state,
      msttrayekGetSuccess: data,
    }),
    [setMsttrayekViewError]: (state, data) => ({
      ...state,
      msttrayekViewError: data,
    }),
    [setMsttrayekViewLoading]: (state, data) => ({
      ...state,
      msttrayekViewLoading: data,
    }),
    [setMsttrayekViewSuccess]: (state, data) => ({
      ...state,
      msttrayekViewSuccess: data,
    }),
    [setMsttrayekAddError]: (state, data) => ({
      ...state,
      msttrayekAddError: data,
    }),
    [setMsttrayekAddLoading]: (state, data) => ({
      ...state,
      msttrayekAddLoading: data,
    }),
    [setMsttrayekAddSuccess]: (state, data) => ({
      ...state,
      msttrayekAddSuccess: data,
    }),
    [setMsttrayekDeleteError]: (state, data) => ({
      ...state,
      msttrayekDeleteError: data,
    }),
    [setMsttrayekDeleteLoading]: (state, data) => ({
      ...state,
      msttrayekDeleteLoading: data,
    }),
    [setMsttrayekDeleteSuccess]: (state, data) => ({
      ...state,
      msttrayekDeleteSuccess: data,
    }),

    [setMsttrayekData]: (state, data: Array<"any">) => ({
      ...state,
      msttrayekData: data,
    }),
    [setMsttrayekFilterAdd]: (state, data: any) => ({
      ...state,
      msttrayekFilter: data,
    }),
    [setMsttrayekFilterDel]: (state, id: any) => ({
      ...state,
      msttrayekFilter: {},
    }),
    [setMsttrayekFilterClear]: (state, data: any) => ({
      ...state,
      msttrayekFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function msttrayekFetchAll(params) {
  return async function (dispatch, getState) {
    dispatch(setMsttrayekAddSuccess(false));
    dispatch(setMsttrayekGetSuccess(false));
    dispatch(setMsttrayekGetError(false));
    dispatch(setMsttrayekGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + MSTTRAYEK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setMsttrayekGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().msttrayek.msttrayekData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setMsttrayekData(data.data));
        } else {
          dispatch(setMsttrayekData(prev.concat(data.data)));
        }
        dispatch(setMsttrayekGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setMsttrayekGetError(true));
        dispatch(setMsttrayekGetLoading(false));
      });
  };
}

export function msttrayekFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setMsttrayekAddSuccess(false));
    dispatch(setMsttrayekGetSuccess(false));
    dispatch(setMsttrayekGetError(false));
    dispatch(setMsttrayekGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + MSTTRAYEK_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setMsttrayekGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().msttrayek.msttrayekData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setMsttrayekData(data.data));
        } else {
          dispatch(setMsttrayekData(prev.concat(data.data)));
        }
        dispatch(setMsttrayekGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setMsttrayekGetError(true));
        dispatch(setMsttrayekGetLoading(false));
      });
  };
}

export function msttrayekFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setMsttrayekAddSuccess(false));
    dispatch(setMsttrayekGetSuccess(false));
    dispatch(setMsttrayekGetError(false));
    dispatch(setMsttrayekGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + MSTTRAYEK_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setMsttrayekGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().msttrayek.msttrayekData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setMsttrayekData(data.data));
        } else {
          dispatch(setMsttrayekData(prev.concat(data.data)));
        }
        dispatch(setMsttrayekGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setMsttrayekGetError(true));
        dispatch(setMsttrayekGetLoading(false));
      });
  };
}

export function msttrayekAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setMsttrayekAddError(false));
    dispatch(setMsttrayekAddSuccess(false));
    dispatch(setMsttrayekAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + MSTTRAYEK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setMsttrayekAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((msttrayek) => {
        if (msttrayek.success) {
          dispatch(setMsttrayekAddSuccess(msttrayek.data));
        } else {
          dispatch(setMsttrayekAddError(true));
          dispatch(setMsttrayekAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setMsttrayekAddError(true));
        dispatch(setMsttrayekAddLoading(false));
      });
  };
}

export function msttrayekFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setMsttrayekAddSuccess(false));
    dispatch(setMsttrayekViewError(false));
    dispatch(setMsttrayekViewSuccess(false));
    dispatch(setMsttrayekViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + MSTTRAYEK_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setMsttrayekViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setMsttrayekViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setMsttrayekViewError(true));
        dispatch(setMsttrayekViewLoading(false));
      });
  };
}

export function msttrayekUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setMsttrayekAddError(false));
    dispatch(setMsttrayekAddSuccess(false));
    dispatch(setMsttrayekAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + MSTTRAYEK_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setMsttrayekAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((msttrayek) => {
        if (msttrayek.success) {
          dispatch(setMsttrayekAddSuccess(msttrayek));
          Alert.alert("Sukses", "Data berhasil diperbarui");
        } else {
          dispatch(setMsttrayekAddError(true));
          dispatch(setMsttrayekAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setMsttrayekAddError(true));
        dispatch(setMsttrayekAddLoading(false));
      });
  };
}

export function msttrayekDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setMsttrayekGetSuccess(false));
    dispatch(setMsttrayekDeleteError(false));
    dispatch(setMsttrayekDeleteSuccess(false));
    dispatch(setMsttrayekDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + MSTTRAYEK_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setMsttrayekDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setMsttrayekDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setMsttrayekDeleteError(true));
        dispatch(setMsttrayekDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function msttrayekFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setMsttrayekFilterAdd(filter));
  };
}

export function msttrayekFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setMsttrayekFilterDel(id));
  };
}

export function msttrayekFilterClear() {
  return function (dispatch, getState) {
    dispatch(setMsttrayekFilterClear());
  };
}

export default msttrayekReducer;
