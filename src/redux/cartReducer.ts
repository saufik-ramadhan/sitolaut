import { createAction, createReducer } from "redux-act";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  items: [],
  loading: false,
};

/**
 * ACTION
 */
const cartAdd = createAction('cartAdd', (data: object | undefined) => data);
const cartDelete = createAction('cartDelete', (id: number) => id);
const cartClear = createAction('cartClear');
const cartIncrease = createAction('cartIncrease', (id: number) => id);
const cartDecrease = createAction('cartDecrease', (id: number) => id);

/**
 * REDUCER
 */
const cartReducer = createReducer(
  {
    [cartAdd]: (state, data) => ({
      ...state,
      items: [...state.items.filter((item) => item.id !== data.id), data],
    }),
    [cartDelete]: (state, id) => ({
      ...state,
      items: state.items.filter((item) => item.id !== id),
    }),
    [cartClear]: (state) => ({
      ...state,
      items: [],
    }),
    [cartIncrease]: (state, id) => ({
      ...state,
      items: state.items.map((item: any) =>
        item.id === id ? { ...item, count: item.count + 1 } : item
      ),
    }),
    [cartDecrease]: (state, id) => ({
      ...state,
      items: state.items.map((item: any) =>
        item.id === id ? { ...item, count: item.count - 1 } : item
      ),
    }),
  },
  DefaultState
);

export function addCartItem(data: any) {
  return async function (dispatch) {
    dispatch(cartAdd(data));
  };
}
export function deleteCartItem(id: number) {
  return async function (dispatch) {
    dispatch(cartDelete(id));
  };
}
export function clearCartItems() {
  return async function (dispatch) {
    dispatch(cartClear());
  };
}
export function increaseCartItem(id: number) {
  return async function (dispatch) {
    dispatch(cartIncrease(id));
  };
}
export function decreaseCartItem(id: number) {
  return async function (dispatch) {
    dispatch(cartDecrease(id));
  };
}

export default cartReducer;
