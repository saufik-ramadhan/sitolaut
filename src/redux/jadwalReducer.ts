import { createAction, createReducer } from "redux-act";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  data: false,
  quota: false,
  loading: false,
};

/**
 * ACTION
 */
const jadwalAdd = createAction('jadwalAdd',(data: any) => data);
const jadwalDelete = createAction('jadwalDelete');
const jadwalClear = createAction('jadwalClear');
const quotaAdd = createAction('quotaAdd',(data: any) => data);

/**
 * REDUCER
 */
const jadwalReducer = createReducer(
  {
    [jadwalAdd]: (state, data) => ({
      ...state,
      data: data,
    }),
    [jadwalDelete]: (state) => ({
      ...state,
      data: false,
      quota: 0,
    }),
    [jadwalClear]: (state) => ({
      ...state,
      data: false,
    }),
    [quotaAdd]: (state, data) => ({
      ...state,
      quota: data,
    }),
  },
  DefaultState
);

export function addJadwal(data: any) {
  return async function (dispatch) {
    dispatch(jadwalAdd(data));
  };
}
export function deleteJadwal() {
  return async function (dispatch) {
    dispatch(jadwalDelete());
  };
}
export function clearJadwals() {
  return async function (dispatch) {
    dispatch(jadwalClear());
  };
}
export function addQuota(data: any) {
  return async function (dispatch) {
    dispatch(quotaAdd(data));
  };
}

export default jadwalReducer;
