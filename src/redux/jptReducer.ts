import { createAction, createReducer } from "redux-act";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  data: false,
  loading: false,
};

/**
 * ACTION
 */
const jptAdd = createAction('jptAdd', (data: any) => data);
const jptDelete = createAction('jptDelete');
const jptClear = createAction('jptClear');

/**
 * REDUCER
 */
const jptReducer = createReducer(
  {
    [jptAdd]: (state, data) => ({
      ...state,
      data: data,
    }),
    [jptDelete]: (state) => ({
      ...state,
      data: false,
    }),
    [jptClear]: (state) => ({
      ...state,
      data: false,
    }),
  },
  DefaultState
);

export function addJpt(data: any) {
  return async function (dispatch) {
    dispatch(jptAdd(data));
  };
}
export function deleteJpt() {
  return async function (dispatch) {
    dispatch(jptDelete());
  };
}
export function clearJpts() {
  return async function (dispatch) {
    dispatch(jptClear());
  };
}

export default jptReducer;
