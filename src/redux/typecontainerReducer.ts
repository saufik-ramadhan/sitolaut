import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import {
  BASE_URL,
  TYPECONTAINER_URL,
  TYPECONTAINER_PRICE_UPDATE_URL,
} from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  typecontainerGetError: false,
  typecontainerGetLoading: false,
  typecontainerGetSuccess: false,
  typecontainerViewError: false,
  typecontainerViewLoading: false,
  typecontainerViewSuccess: false,
  typecontainerAddError: false,
  typecontainerAddLoading: false,
  typecontainerAddSuccess: false,
  typecontainerDeleteError: false,
  typecontainerDeleteLoading: false,
  typecontainerDeleteSuccess: false,
  typecontainerPriceUpdateError: false,
  typecontainerPriceUpdateLoading: false,
  typecontainerPriceUpdateSuccess: false,

  typecontainerData: [],
  typecontainerFilter: {},
};

/**
 * ACTION
 */
const setTypecontainerGetError = createAction('setTypecontainerGetError', (data: any) => data);
const setTypecontainerGetLoading = createAction('setTypecontainerGetLoading', (data: any) => data);
const setTypecontainerGetSuccess = createAction('setTypecontainerGetSuccess', (data: any) => data);
const setTypecontainerViewError = createAction('setTypecontainerViewError', (data: any) => data);
const setTypecontainerViewLoading = createAction('setTypecontainerViewLoading', (data: any) => data);
const setTypecontainerViewSuccess = createAction('setTypecontainerViewSuccess', (data: any) => data);
const setTypecontainerAddError = createAction('setTypecontainerAddError', (data: any) => data);
const setTypecontainerAddLoading = createAction('setTypecontainerAddLoading', (data: any) => data);
const setTypecontainerAddSuccess = createAction('setTypecontainerAddSuccess', (data: any) => data);
const setTypecontainerDeleteError = createAction('setTypecontainerDeleteError', (data: any) => data);
const setTypecontainerDeleteLoading = createAction('setTypecontainerDeleteLoading', (data: any) => data);
const setTypecontainerDeleteSuccess = createAction('setTypecontainerDeleteSuccess', (data: any) => data);
const setTypecontainerPriceUpdateError = createAction('setTypecontainerPriceUpdateError', (data: any) => data);
const setTypecontainerPriceUpdateLoading = createAction('setTypecontainerPriceUpdateLoading', (data: any) => data);
const setTypecontainerPriceUpdateSuccess = createAction('setTypecontainerPriceUpdateSuccess', (data: any) => data);

const setTypecontainerData = createAction('setTypecontainerData', (data: any) => data);
const setTypecontainerFilterAdd = createAction('setTypecontainerFilterAdd', (data: any) => data);
const setTypecontainerFilterDel = createAction('setTypecontainerFilterDel', (data: any) => data);
const setTypecontainerFilterClear = createAction('setTypecontainerFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const typecontainerReducer = createReducer(
  {
    [setTypecontainerGetError]: (state, data) => ({
      ...state,
      typecontainerGetError: data,
    }),
    [setTypecontainerGetLoading]: (state, data) => ({
      ...state,
      typecontainerGetLoading: data,
    }),
    [setTypecontainerGetSuccess]: (state, data) => ({
      ...state,
      typecontainerGetSuccess: data,
    }),
    [setTypecontainerViewError]: (state, data) => ({
      ...state,
      typecontainerViewError: data,
    }),
    [setTypecontainerViewLoading]: (state, data) => ({
      ...state,
      typecontainerViewLoading: data,
    }),
    [setTypecontainerViewSuccess]: (state, data) => ({
      ...state,
      typecontainerViewSuccess: data,
    }),
    [setTypecontainerAddError]: (state, data) => ({
      ...state,
      typecontainerAddError: data,
    }),
    [setTypecontainerAddLoading]: (state, data) => ({
      ...state,
      typecontainerAddLoading: data,
    }),
    [setTypecontainerAddSuccess]: (state, data) => ({
      ...state,
      typecontainerAddSuccess: data,
    }),
    [setTypecontainerDeleteError]: (state, data) => ({
      ...state,
      typecontainerDeleteError: data,
    }),
    [setTypecontainerDeleteLoading]: (state, data) => ({
      ...state,
      typecontainerDeleteLoading: data,
    }),
    [setTypecontainerDeleteSuccess]: (state, data) => ({
      ...state,
      typecontainerDeleteSuccess: data,
    }),
    [setTypecontainerPriceUpdateError]: (state, data) => ({
      ...state,
      typecontainerPriceUpdateError: data,
    }),
    [setTypecontainerPriceUpdateLoading]: (state, data) => ({
      ...state,
      typecontainerPriceUpdateLoading: data,
    }),
    [setTypecontainerPriceUpdateSuccess]: (state, data) => ({
      ...state,
      typecontainerPriceUpdateSuccess: data,
    }),

    [setTypecontainerData]: (state, data: Array<"any">) => ({
      ...state,
      typecontainerData: data,
    }),
    [setTypecontainerFilterAdd]: (state, data: any) => ({
      ...state,
      typecontainerFilter: data,
    }),
    [setTypecontainerFilterDel]: (state, id: any) => ({
      ...state,
      typecontainerFilter: {},
    }),
    [setTypecontainerFilterClear]: (state, data: any) => ({
      ...state,
      typecontainerFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function typecontainerFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerAddSuccess(false));
    dispatch(setTypecontainerGetSuccess(false));
    dispatch(setTypecontainerGetError(false));
    dispatch(setTypecontainerGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().typecontainer.typecontainerData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setTypecontainerData(data.data));
        } else {
          dispatch(setTypecontainerData(prev.concat(data.data)));
        }
        dispatch(setTypecontainerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setTypecontainerGetError(true));
        dispatch(setTypecontainerGetLoading(false));
      });
  };
}

export function typecontainerFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerAddSuccess(false));
    dispatch(setTypecontainerGetSuccess(false));
    dispatch(setTypecontainerGetError(false));
    dispatch(setTypecontainerGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().typecontainer.typecontainerData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setTypecontainerData(data.data));
        } else {
          dispatch(setTypecontainerData(prev.concat(data.data)));
        }
        dispatch(setTypecontainerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setTypecontainerGetError(true));
        dispatch(setTypecontainerGetLoading(false));
      });
  };
}

export function typecontainerFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerAddSuccess(false));
    dispatch(setTypecontainerGetSuccess(false));
    dispatch(setTypecontainerGetError(false));
    dispatch(setTypecontainerGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().typecontainer.typecontainerData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setTypecontainerData(data.data));
        } else {
          dispatch(setTypecontainerData(prev.concat(data.data)));
        }
        dispatch(setTypecontainerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setTypecontainerGetError(true));
        dispatch(setTypecontainerGetLoading(false));
      });
  };
}

export function typecontainerAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerAddError(false));
    dispatch(setTypecontainerAddSuccess(false));
    dispatch(setTypecontainerAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((typecontainer) => {
        dispatch(setTypecontainerAddSuccess(typecontainer));
      })
      .catch((err) => {
        dispatch(setTypecontainerAddError(true));
      });
  };
}

export function typecontainerFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerViewError(false));
    dispatch(setTypecontainerViewSuccess(false));
    dispatch(setTypecontainerViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setTypecontainerViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setTypecontainerViewError(true));
      });
  };
}

export function typecontainerUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerAddError(false));
    dispatch(setTypecontainerAddSuccess(false));
    dispatch(setTypecontainerAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((typecontainer) => {
        if (typecontainer.success) {
          dispatch(setTypecontainerAddSuccess(typecontainer));
        } else {
          dispatch(setTypecontainerAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setTypecontainerAddError(true));
      });
  };
}

export function typecontainerDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerViewSuccess(false));
    dispatch(setTypecontainerDeleteError(false));
    dispatch(setTypecontainerDeleteSuccess(false));
    dispatch(setTypecontainerDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setTypecontainerDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setTypecontainerDeleteError(true));
      });
  };
}

export function typecontainerPriceUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setTypecontainerPriceUpdateError(false));
    dispatch(setTypecontainerPriceUpdateSuccess(false));
    dispatch(setTypecontainerPriceUpdateLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TYPECONTAINER_PRICE_UPDATE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTypecontainerPriceUpdateLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setTypecontainerPriceUpdateSuccess(data));
      })
      .catch((err) => {
        dispatch(setTypecontainerPriceUpdateError(true));
      });
  };
}

/**
 * Filter
 */
export function typecontainerFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setTypecontainerFilterAdd(filter));
  };
}

export function typecontainerFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setTypecontainerFilterDel(id));
  };
}

export function typecontainerFilterClear() {
  return function (dispatch, getState) {
    dispatch(setTypecontainerFilterClear());
  };
}

export default typecontainerReducer;
