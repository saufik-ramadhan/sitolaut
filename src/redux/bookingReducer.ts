import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import {
  BASE_URL,
  BOOKING_URL,
  BOOKING_HAS_DO_VIEW_URL,
  BOOKING_HAS_DO_UPDATE_URL,
  BOOKING_LIST_URL,
} from "./../config/constants";
import AsyncStorage from "../AsyncStorage";
import {
  printBillOfLading,
  printReleaseOrder,
  printShippingInstruction,
} from "../services/utils";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  bookingGetError: false,
  bookingGetLoading: false,
  bookingGetSuccess: false,
  bookingViewError: false,
  bookingViewLoading: false,
  bookingViewSuccess: false,
  bookingAddError: false,
  bookingAddLoading: false,
  bookingAddSuccess: false,
  bookingHasDoViewError: false,
  bookingHasDoViewLoading: false,
  bookingHasDoViewSuccess: false,
  bookingHasDoUpdateError: false,
  bookingHasDoUpdateLoading: false,
  bookingHasDoUpdateInvalid: false,
  bookingHasDoUpdateSuccess: false,

  bookingViewRoLoading: false,
  bookingViewRoSuccess: false,
  bookingViewRoError: false,

  bookingViewSiLoading: false,
  bookingViewSiSuccess: false,
  bookingViewSiError: false,

  bookingViewBolLoading: false,
  bookingViewBolSuccess: false,
  bookingViewBolError: false,

  bookingListError: false,
  bookingListLoading: false,
  bookingListSuccess: false,
  bookingListUrl: false,

  paymentGetLoading: false,
  paymentGetSuccess: false,
  paymentGetError: false,

  bookingData: [],
  bookingFilter: {},
};

/**
 * CREATE ACTION
 */
const setBookingGetError = createAction('setBookingGetError', (data: any) => data);
const setBookingGetLoading = createAction('setBookingGetLoading', (data: any) => data);
const setBookingGetSuccess = createAction('setBookingGetSuccess', (data: any) => data);
const setBookingViewError = createAction('setBookingViewError', (data: any) => data);
const setBookingViewLoading = createAction('setBookingViewLoading', (data: any) => data);
const setBookingViewSuccess = createAction('setBookingViewSuccess', (data: any) => data);
const setBookingAddError = createAction('setBookingAddError', (data: any) => data);
const setBookingAddLoading = createAction('setBookingAddLoading', (data: any) => data);
const setBookingAddSuccess = createAction('setBookingAddSuccess', (data: any) => data);
const setBookingHasDoViewError = createAction('setBookingHasDoViewError', (data: any) => data);
const setBookingHasDoViewLoading = createAction('setBookingHasDoViewLoading', (data: any) => data);
const setBookingHasDoViewSuccess = createAction('setBookingHasDoViewSuccess', (data: any) => data);
const setBookingHasDoUpdateError = createAction('setBookingHasDoUpdateError', (data: any) => data);
const setBookingHasDoUpdateLoading = createAction('setBookingHasDoUpdateLoading', (data: any) => data);
const setBookingHasDoUpdateInvalid = createAction('setBookingHasDoUpdateInvalid', (data: any) => data);
const setBookingHasDoUpdateSuccess = createAction('setBookingHasDoUpdateSuccess', (data: any) => data);

const setBookingViewRoLoading = createAction('setBookingViewRoLoading', (data: any) => data);
const setBookingViewRoSuccess = createAction('setBookingViewRoSuccess', (data: any) => data);
const setBookingViewRoError = createAction('setBookingViewRoError', (data: any) => data);

const setBookingViewSiLoading = createAction('setBookingViewSiLoading', (data: any) => data);
const setBookingViewSiSuccess = createAction('setBookingViewSiSuccess', (data: any) => data);
const setBookingViewSiError = createAction('setBookingViewSiError', (data: any) => data);

const setBookingViewBolLoading = createAction('setBookingViewBolLoading', (data: any) => data);
const setBookingViewBolSuccess = createAction('setBookingViewBolSuccess', (data: any) => data);
const setBookingViewBolError = createAction('setBookingViewBolError', (data: any) => data);

const setBookingListError = createAction('setBookingListError', (data: any) => data);
const setBookingListLoading = createAction('setBookingListLoading', (data: any) => data);
const setBookingListSuccess = createAction('setBookingListSuccess', (data: any) => data);
const setBookingListUrl = createAction('setBookingListUrl', (data: any) => data);

const setPaymentGetLoading = createAction('setPaymentGetLoading', (data: any) => data);
const setPaymentGetSuccess = createAction('setPaymentGetSuccess', (data: any) => data);
const setPaymentGetError = createAction('setPaymentGetError', (data: any) => data);

const setBookingData = createAction('setBookingData', (data: any) => data);
const setBookingFilterAdd = createAction('setBookingFilterAdd', (data: any) => data);
const setBookingFilterDel = createAction('setBookingFilterDel', (data: any) => data);
const setBookingFilterClear = createAction('setBookingFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const bookingReducer = createReducer(
  {
    [setBookingGetError]: (state, data) => ({
      ...state,
      bookingGetError: data,
    }),
    [setBookingGetLoading]: (state, data) => ({
      ...state,
      bookingGetLoading: data,
    }),
    [setBookingGetSuccess]: (state, data) => ({
      ...state,
      bookingGetSuccess: data,
    }),
    [setBookingViewError]: (state, data) => ({
      ...state,
      bookingViewError: data,
    }),
    [setBookingViewLoading]: (state, data) => ({
      ...state,
      bookingViewLoading: data,
    }),
    [setBookingViewSuccess]: (state, data) => ({
      ...state,
      bookingViewSuccess: data,
    }),
    [setBookingAddError]: (state, data) => ({
      ...state,
      bookingAddError: data,
    }),
    [setBookingAddLoading]: (state, data) => ({
      ...state,
      bookingAddLoading: data,
    }),
    [setBookingAddSuccess]: (state, data) => ({
      ...state,
      bookingAddSuccess: data,
    }),
    [setBookingHasDoViewError]: (state, data) => ({
      ...state,
      bookingHasDoViewError: data,
    }),
    [setBookingHasDoViewLoading]: (state, data) => ({
      ...state,
      bookingHasDoViewLoading: data,
    }),
    [setBookingHasDoViewSuccess]: (state, data) => ({
      ...state,
      bookingHasDoViewSuccess: data,
    }),
    [setBookingHasDoUpdateError]: (state, data) => ({
      ...state,
      bookingHasDoUpdateError: data,
    }),
    [setBookingHasDoUpdateLoading]: (state, data) => ({
      ...state,
      bookingHasDoUpdateLoading: data,
    }),
    [setBookingHasDoUpdateInvalid]: (state, data) => ({
      ...state,
      bookingHasDoUpdateInvalid: data,
    }),
    [setBookingHasDoUpdateSuccess]: (state, data) => ({
      ...state,
      bookingHasDoUpdateSuccess: data,
    }),

    [setBookingListError]: (state, data) => ({
      ...state,
      bookingListError: data,
    }),
    [setBookingListLoading]: (state, data) => ({
      ...state,
      bookingListLoading: data,
    }),
    [setBookingListSuccess]: (state, data) => ({
      ...state,
      bookingListSuccess: data,
    }),
    [setBookingListUrl]: (state, data) => ({
      ...state,
      bookingListUrl: data,
    }),

    [setBookingViewRoLoading]: (state, data) => ({
      ...state,
      bookingViewRoLoading: data,
    }),
    [setBookingViewRoSuccess]: (state, data) => ({
      ...state,
      bookingViewRoSuccess: data,
    }),
    [setBookingViewRoError]: (state, data) => ({
      ...state,
      bookingViewRoError: data,
    }),

    [setBookingViewSiLoading]: (state, data) => ({
      ...state,
      bookingViewSiLoading: data,
    }),
    [setBookingViewSiSuccess]: (state, data) => ({
      ...state,
      bookingViewSiSuccess: data,
    }),
    [setBookingViewSiError]: (state, data) => ({
      ...state,
      bookingViewSiError: data,
    }),

    [setBookingViewBolLoading]: (state, data) => ({
      ...state,
      bookingViewBolLoading: data,
    }),
    [setBookingViewBolSuccess]: (state, data) => ({
      ...state,
      bookingViewBolSuccess: data,
    }),
    [setBookingViewBolError]: (state, data) => ({
      ...state,
      bookingViewBolError: data,
    }),


    [setPaymentGetLoading]: (state, data) => ({
      ...state,
      paymentGetLoading: data,
    }),
    [setPaymentGetSuccess]: (state, data) => ({
      ...state,
      paymentGetSuccess: data,
    }),
    [setPaymentGetError]: (state, data) => ({
      ...state,
      paymentGetError: data,
    }),


    [setBookingData]: (state, data: Array<"any">) => ({
      ...state,
      bookingData: data,
    }),
    [setBookingFilterAdd]: (state, data: any) => ({
      ...state,
      bookingFilter: data,
    }),
    [setBookingFilterDel]: (state, id: any) => ({
      ...state,
      bookingFilter: {},
    }),
    [setBookingFilterClear]: (state, data: any) => ({
      ...state,
      bookingFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function bookingFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setBookingGetSuccess(false));
    dispatch(setBookingGetError(false));
    dispatch(setBookingGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().booking.bookingData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setBookingData(data.data));
        } else {
          dispatch(setBookingData(prev.concat(data.data)));
        }
        dispatch(setBookingGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setBookingGetError(true));
        dispatch(setBookingGetLoading(false));
      });
  };
}

export function bookingFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setBookingGetSuccess(false));
    dispatch(setBookingGetError(false));
    dispatch(setBookingGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().booking.bookingData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setBookingData(data.data));
        } else {
          dispatch(setBookingData(prev.concat(data.data)));
        }
        dispatch(setBookingGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setBookingGetError(true));
        dispatch(setBookingGetLoading(false));
      });
  };
}

export function bookingFetchBol(path) {
  return async function (dispatch, getState) {
    dispatch(setBookingViewBolSuccess(false));
    dispatch(setBookingViewBolError(false));
    dispatch(setBookingViewBolLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/bl/" + String(path), config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingViewBolLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        // const currentPage = data.currentPage || 1;
        // const start = data.start || 0;
        // const prev = getState().booking.bookingData || [];
        // if (start == 0 && currentPage == 1) {
        //   dispatch(setBookingData(data.data));
        // } else {
        //   dispatch(setBookingData(prev.concat(data.data)));
        // }
        dispatch(setBookingViewBolSuccess(data));
        printBillOfLading(data.data);
      })
      .catch((err) => {
        dispatch(setBookingViewBolError(true));
        dispatch(setBookingViewBolLoading(false));
      });
  };
}

export function bookingFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setBookingGetSuccess(false));
    dispatch(setBookingGetError(false));
    dispatch(setBookingGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().booking.bookingData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setBookingData(data.data));
        } else {
          dispatch(setBookingData(prev.concat(data.data)));
        }

        dispatch(setBookingGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setBookingGetError(true));
        dispatch(setBookingGetLoading(false));
      });
  };
}

export function bookingFetchConsigneeReceived(param) {
  return async function (dispatch, getState) {
    dispatch(setBookingGetSuccess(false));
    dispatch(setBookingGetError(false));
    dispatch(setBookingGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(param),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/consignee", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().booking.bookingData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setBookingData(data.data));
        } else {
          dispatch(setBookingData(prev.concat(data.data)));
        }

        dispatch(setBookingGetSuccess(data));
      })
      .catch((err) => {
        console.log(err);
        dispatch(setBookingGetError(true));
        dispatch(setBookingGetLoading(false));
      });
  };
}

export function bookingAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingAddError(false));
    dispatch(setBookingAddSuccess(false));
    dispatch(setBookingAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + BOOKING_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((booking) => {
        dispatch(setBookingAddSuccess(booking));
      })
      .catch((err) => {
        dispatch(setBookingAddError(true));
        dispatch(setBookingAddLoading(false));
      });
  };
}

export function bookingFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setBookingViewSuccess(false));
    dispatch(setBookingViewError(false));
    dispatch(setBookingViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let data = {
      id: id,
    };
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/view", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setBookingViewLoading(false));
        dispatch(setBookingViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setBookingViewError(true));
        dispatch(setBookingViewLoading(false));
      });
  };
}

export function bookingFetchSi(id) {
  return async function (dispatch, getState) {
    dispatch(setBookingViewSiSuccess(false));
    dispatch(setBookingViewSiError(false));
    dispatch(setBookingViewSiLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/shipping_intructions", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingViewSiLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setBookingViewSiSuccess(data));
        printShippingInstruction(data.data);
      })
      .catch((err) => {
        dispatch(setBookingViewSiError(true));
        dispatch(setBookingViewSiLoading(false));
      });
  };
}

export function bookingFetchRo(id) {
  return async function (dispatch, getState) {
    dispatch(setBookingViewRoSuccess(false));
    dispatch(setBookingViewRoError(false));
    dispatch(setBookingViewRoLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/release_order", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingViewRoLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setBookingViewRoSuccess(data));
        printReleaseOrder(data.data);
      })
      .catch((err) => {
        dispatch(setBookingViewRoError(true));
        dispatch(setBookingViewRoLoading(false));
      });
  };
}

export function bookingConfirm(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingViewSuccess(false));
    dispatch(setBookingAddSuccess(false));
    dispatch(setBookingAddError(false));
    dispatch(setBookingAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/status", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((booking) => {
        if (booking.status == 200) {
          dispatch(setBookingAddSuccess(booking));
        } else {
          dispatch(setBookingAddError(true));
          dispatch(setBookingAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setBookingAddError(true));
        dispatch(setBookingAddLoading(false));
      });
  };
}

export function bookingPayment(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingAddSuccess(false));
    dispatch(setBookingAddError(false));
    dispatch(setBookingAddLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/payment", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((booking) => {
        if (booking.status == 200) {
          alert("Bukti pembayaran berhasil terkirim");
          dispatch(setBookingAddSuccess(booking));
        } else {
          alert("Bukti pembayaran gagal");
          dispatch(setBookingAddError(true));
          dispatch(setBookingAddLoading(false));
        }
      })
      .catch((err) => {
        alert("Bukti pembayaran gagal terkirim");
        dispatch(setBookingAddError(true));
        dispatch(setBookingAddLoading(false));
      });
  };
}

export function bookingGenerateBL(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingAddSuccess(false));
    dispatch(setBookingAddError(false));
    dispatch(setBookingAddLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/generate_bl", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((booking) => {
        if (booking.success) {
          dispatch(setBookingAddSuccess(booking));
        } else {
          dispatch(setBookingAddError(true));
          dispatch(setBookingAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setBookingAddError(true));
        dispatch(setBookingAddLoading(false));
      });
  };
}

// TODO: Need update
export function bookingAmbilBarang(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingViewSuccess(false));
    dispatch(setBookingAddSuccess(false));
    dispatch(setBookingAddError(false));
    dispatch(setBookingAddLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BOOKING_URL + "/pengambilan_barang", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((booking) => {
        if (booking.status === 200) {
          dispatch(setBookingViewSuccess(false));
          dispatch(setBookingAddSuccess(booking));
        } else {
          dispatch(setBookingAddError(true));
          dispatch(setBookingAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setBookingAddError(true));
        dispatch(setBookingAddLoading(false));
      });
  };
}

export function bookingHasDoView(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingHasDoViewError(false));
    dispatch(setBookingHasDoViewSuccess(false));
    dispatch(setBookingHasDoViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + BOOKING_HAS_DO_VIEW_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingHasDoViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((booking) => {
        dispatch(setBookingHasDoViewSuccess(booking));
      })
      .catch((err) => {
        dispatch(setBookingHasDoViewError(true));
        dispatch(setBookingHasDoViewLoading(false));
      });
  };
}

export function bookingHasDoUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingHasDoUpdateError(false));
    dispatch(setBookingHasDoUpdateSuccess(false));
    dispatch(setBookingHasDoUpdateInvalid(false));
    dispatch(setBookingHasDoUpdateLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + BOOKING_HAS_DO_UPDATE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingHasDoUpdateLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          dispatch(setBookingHasDoUpdateSuccess(true));
        } else {
          dispatch(setBookingHasDoUpdateInvalid(data));
        }
      })
      .catch((err) => {
        dispatch(setBookingHasDoUpdateError(true));
        dispatch(setBookingHasDoUpdateLoading(false));
      });
  };
}

export function getBookingList(data) {
  return async function (dispatch, getState) {
    dispatch(setBookingListError(false));
    dispatch(setBookingListSuccess(false));
    dispatch(setBookingListLoading(true));

    let token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + BOOKING_LIST_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBookingListLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          dispatch(setBookingListSuccess(data));
        }
      })
      .catch((err) => {
        dispatch(setBookingListError(true));
        dispatch(setBookingListLoading(false));
      });
  };
}

/**
 * Pembayaran (Payment)
 */

/**
 * Fetch semua list payment yang ada
 * Manual dan BRIVA
 */
export function getListPayment() {
  return async (dispatch) => {
    dispatch(setPaymentGetLoading(true));
    dispatch(setPaymentGetSuccess(false));

    let token = await AsyncStorage.getItem('token@lcs') || null
    let config = {}

    if (token) {
      config = {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      }
    } else {
      throw Error("Authentication error")
    }
    fetch(BASE_URL + 'payment_channel', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(setPaymentGetLoading(false))
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        if(data.status === 200){
          dispatch(setPaymentGetSuccess(data))
        }else{
          alert(data.message);
          dispatch(setPaymentGetSuccess(false))
        }
        dispatch(setPaymentGetLoading(false))
      })
      .catch((err) => {
        console.log(err)
        dispatch(setPaymentGetError(true))
        dispatch(setPaymentGetLoading(false))
      })
  }
}


/**
 * Meminta Customer Code ke API untuk
 * pembayaran BRIVA
 */
const generateCustomerCode = async datas => {
    let { institutionCode, brivaNo, custCode, nama, amount, keterangan, expiredDate } = datas
    let data = {institutionCode, brivaNo, custCode, nama, amount:amount.toString(), keterangan, expiredDate}
    let token = await AsyncStorage.getItem('token@lcs') || null
    let config = {}
    if (token) {
      config = {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    } else {
      throw Error("Authentication error")
    }
    try{
      const response = await fetch(BASE_URL + `generate_customer_code/${datas.id_booking}`, config)
        if (response.status === 401) {
             return {status:401}
            }
        if (!response.ok) {
          return {status:400, message:response.statusText}
        }
        const dataRes = await response.json()
        return dataRes
    }catch(err){
      console.log(err)
      return {status:400, message:'error connection'}
    }
    
}

/**
 * Perbarui Metode Pembayaran di Database
 */
const updatechanel = datas => {
  return async (dispatch) => {
    let { id_booking, id_payment } = datas
    let data = { id_booking, id_payment }
    let token = await AsyncStorage.getItem('token@lcs') || null
    let config = {}

    if (token) {
      config = {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
    } else {
      throw Error("Authentication error")
    }
    fetch(BASE_URL + 'update_booking_payment', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        if(data.status === 200){
          // dispatch(isLoadPayChanel(false))
          // alert
          // alert("payment channel diperbarui")
        }else{
          // dispatch(isLoadPayChanel(false))
          alert("paymeny channel gagal diperbarui")
        }
      })
      .catch((err) => {
        // dispatch(isLoadPayChanel(false))
        console.log(err)
      })
  }
}

/**
 * Pembayaran BRIVA dan metode lain
 * @param datas 
 */
export const updatePayChannel = datas => {
  return async (dispatch) => {
    // dispatch(isLoadPayChanel(true)) 
    if(datas.id_payment === 1){
      // alert("Belum tersedia");
      const res = await generateCustomerCode(datas)
      if(res.status === 401){
        dispatch(doLogout())
      }
      if(res.status === 200){
        dispatch(updatechanel(datas))
        alert(res.message);
      }else{
        // dispatch(isLoadPayChanel(false))
        alert(res.message);
      }
    }else{
      dispatch(updatechanel(datas))
    }
  }
}

/**
 * Cek status  pembayaran menggunakan
 * @param datas 
 */
export const cekStatusPay = datas => {
  return async (dispatch) => {
    // dispatch(isLoadPayChanel(true))
    let {id, ic, va, cc} = datas
    let token = await AsyncStorage.getItem('token@lcs') || null
    let config = {}

    if (token) {
      config = {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      }
    } else {
      throw Error("Authentication error")
    }
    try{
      const response = await fetch(BASE_URL + `payment_status/${id}/${ic}/${va}/${cc}`, config)
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
      const data = await response.json()
        if(data.status === 200){
          // dispatch(isLoadPayChanel(false))
          alert("sukses cek payment")
        }else{
          // dispatch(isLoadPayChanel(false))
          alert("gagal cek payment")
        }
    }catch(err){
      console.log(err)
    }
  }
}

/**
 * Filter
 */
export function bookingFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setBookingFilterAdd(filter));
  };
}

export function bookingFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setBookingFilterDel(id));
  };
}

export function bookingFilterClear() {
  return function (dispatch, getState) {
    dispatch(setBookingFilterClear());
  };
}

export default bookingReducer;
