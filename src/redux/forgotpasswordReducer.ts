import { createAction, createReducer } from "redux-act";
import {
  BASE_URL,
  FORGOT_PASSWORD_URL,
  RESET_PASSWORD_URL,
} from "./../config/constants";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  forgotPasswordError: false,
  forgotPasswordLoading: false,
  forgotPasswordSuccess: false,
  resetPasswordError: false,
  resetPasswordLoading: false,
  resetPasswordSuccess: false,
};

/**
 * ACTION
 */
const setForgotPasswordError = createAction('setForgotPasswordError', (data: any) => data);
const setForgotPasswordLoading = createAction('setForgotPasswordLoading', (data: any) => data);
const setForgotPasswordSuccess = createAction('setForgotPasswordSuccess', (data: any) => data);
const setResetPasswordError = createAction('setResetPasswordError', (data: any) => data);
const setResetPasswordLoading = createAction('setResetPasswordLoading', (data: any) => data);
const setResetPasswordSuccess = createAction('setResetPasswordSuccess', (data: any) => data);

/**
 * REDUCER
 */
const forgotpasswordReducer = createReducer(
  {
    [setForgotPasswordError]: (state, data) => ({
      ...state,
      forgotPasswordError: data,
    }),
    [setForgotPasswordLoading]: (state, data) => ({
      ...state,
      forgotPasswordLoading: data,
    }),
    [setForgotPasswordSuccess]: (state, data) => ({
      ...state,
      forgotPasswordSuccess: data,
    }),
    [setResetPasswordError]: (state, data) => ({
      ...state,
      resetPasswordError: data,
    }),
    [setResetPasswordLoading]: (state, data) => ({
      ...state,
      resetPasswordLoading: data,
    }),
    [setResetPasswordSuccess]: (state, data) => ({
      ...state,
      resetPasswordSuccess: data,
    }),
  },
  DefaultState
);

/**
 * API
 */
export function forgotPassword(data) {
  return async function (dispatch) {
    dispatch(setForgotPasswordSuccess(false));
    dispatch(setForgotPasswordError(false));
    dispatch(setForgotPasswordLoading(true));

    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };

    fetch(BASE_URL + FORGOT_PASSWORD_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setForgotPasswordLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          alert("Password sent to email");
          return dispatch(setForgotPasswordSuccess(data));
        } else {
          return dispatch(setForgotPasswordError(data));
        }
      })
      .catch((err) => {
        dispatch(setForgotPasswordLoading(false));
        dispatch(setForgotPasswordError(err));
      });
  };
}

export function resetPassword(data) {
  return async function (dispatch) {
    dispatch(setResetPasswordSuccess(false));
    dispatch(setResetPasswordError(false));
    dispatch(setResetPasswordLoading(true));

    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };

    fetch(BASE_URL + RESET_PASSWORD_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResetPasswordLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          alert("success");
          return dispatch(setResetPasswordSuccess(data));
        } else {
          return dispatch(setResetPasswordError(data));
        }
      })
      .catch((err) => {
        alert("failed");
        dispatch(setResetPasswordLoading(false));
        dispatch(setResetPasswordError(err));
      });
  };
}

export default forgotpasswordReducer;
