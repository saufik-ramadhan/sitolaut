import { createAction, createReducer } from "redux-act";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  data: false,
  loading: false,
};

/**
 * ACTION
 */
const choosesupplierAdd = createAction('choosesupplierAdd',(data: any) => data);
const choosesupplierDelete = createAction('choosesupplierDelete');
const choosesupplierClear = createAction('choosesupplierClear');

/**
 * REDUCER
 */
const choosesupplierReducer = createReducer(
  {
    [choosesupplierAdd]: (state, data) => ({
      ...state,
      data: data,
    }),
    [choosesupplierDelete]: (state) => ({
      ...state,
      data: false,
    }),
    [choosesupplierClear]: (state) => ({
      ...state,
      data: false,
    }),
  },
  DefaultState
);

export function addChoosesupplier(data: any) {
  return async function (dispatch) {
    dispatch(choosesupplierAdd(data));
  };
}
export function deleteChoosesupplier() {
  return async function (dispatch) {
    dispatch(choosesupplierDelete());
  };
}
export function clearChoosesuppliers() {
  return async function (dispatch) {
    dispatch(choosesupplierClear());
  };
}

export default choosesupplierReducer;
