import { createAction, createReducer } from "redux-act";
import { BASE_URL, REGULATOR_URL, USER_URL } from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  userGetError: false,
  userGetLoading: false,
  userGetSuccess: false,
  userViewError: false,
  userViewLoading: false,
  userViewSuccess: false,
  userAddError: false,
  userAddLoading: false,
  userAddSuccess: false,
  userDeleteError: false,
  userDeleteLoading: false,
  userDeleteSuccess: false,

  userData: [],
  userFilter: {},
};

/**
 * ACTION
 */
const setUserGetError = createAction('setUserGetError', (data: any) => data);
const setUserGetLoading = createAction('setUserGetLoading', (data: any) => data);
const setUserGetSuccess = createAction('setUserGetSuccess', (data: any) => data);
const setUserViewError = createAction('setUserViewError', (data: any) => data);
const setUserViewLoading = createAction('setUserViewLoading', (data: any) => data);
const setUserViewSuccess = createAction('setUserViewSuccess', (data: any) => data);
const setUserAddError = createAction('setUserAddError', (data: any) => data);
const setUserAddLoading = createAction('setUserAddLoading', (data: any) => data);
const setUserAddSuccess = createAction('setUserAddSuccess', (data: any) => data);
const setUserDeleteError = createAction('setUserDeleteError', (data: any) => data);
const setUserDeleteLoading = createAction('setUserDeleteLoading', (data: any) => data);
const setUserDeleteSuccess = createAction('setUserDeleteSuccess', (data: any) => data);

const setUserData = createAction('setUserData', (data: any) => data);
const setUserFilterAdd = createAction('setUserFilterAdd', (data: any) => data);
const setUserFilterDel = createAction('setUserFilterDel', (data: any) => data);
const setUserFilterClear = createAction('setUserFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const userReducer = createReducer(
  {
    [setUserGetError]: (state, data) => ({ ...state, userGetError: data }),
    [setUserGetLoading]: (state, data) => ({ ...state, userGetLoading: data }),
    [setUserGetSuccess]: (state, data) => ({ ...state, userGetSuccess: data }),
    [setUserViewError]: (state, data) => ({ ...state, userViewError: data }),
    [setUserViewLoading]: (state, data) => ({
      ...state,
      userViewLoading: data,
    }),
    [setUserViewSuccess]: (state, data) => ({
      ...state,
      userViewSuccess: data,
    }),
    [setUserAddError]: (state, data) => ({ ...state, userAddError: data }),
    [setUserAddLoading]: (state, data) => ({ ...state, userAddLoading: data }),
    [setUserAddSuccess]: (state, data) => ({ ...state, userAddSuccess: data }),
    [setUserDeleteError]: (state, data) => ({
      ...state,
      userDeleteError: data,
    }),
    [setUserDeleteLoading]: (state, data) => ({
      ...state,
      userDeleteLoading: data,
    }),
    [setUserDeleteSuccess]: (state, data) => ({
      ...state,
      userDeleteSuccess: data,
    }),

    [setUserData]: (state, data: Array<"any">) => ({
      ...state,
      userData: data,
    }),
    [setUserFilterAdd]: (state, data: any) => ({
      ...state,
      userFilter: data,
    }),
    [setUserFilterDel]: (state, id: any) => ({
      ...state,
      userFilter: {},
    }),
    [setUserFilterClear]: (state, data: any) => ({
      ...state,
      userFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */

export function userFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setUserGetSuccess(false));
    dispatch(setUserGetError(false));
    dispatch(setUserGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + REGULATOR_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().user.userData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setUserData(data.data));
        } else {
          dispatch(setUserData(prev.concat(data.data)));
        }
        dispatch(setUserGetSuccess(data));
      })
      .catch(() => {
        dispatch(setUserGetError(true));
        dispatch(setUserGetLoading(false));
      });
  };
}

export function userFetchSub(company_id) {
  return async function (dispatch, getState) {
    dispatch(setUserGetSuccess(false));
    dispatch(setUserGetError(false));
    dispatch(setUserGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          company: company_id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + USER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().user.userData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setUserData(data.data));
        } else {
          dispatch(setUserData(prev.concat(data.data)));
        }
        dispatch(setUserGetSuccess(data));
      })
      .catch(() => {
        dispatch(setUserGetError(true));
        dispatch(setUserGetLoading(false));
      });
  };
}

export function userAll(data) {
  return async function (dispatch) {
    dispatch(setUserGetSuccess(false));
    dispatch(setUserGetError(false));
    dispatch(setUserGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + "users", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setUserGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setUserGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setUserGetError(true));
        dispatch(setUserGetLoading(false));
      });
  };
}

export function userFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setUserGetSuccess(false));
    dispatch(setUserGetError(false));
    dispatch(setUserGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + USER_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().user.userData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setUserData(data.data));
        } else {
          dispatch(setUserData(prev.concat(data.data)));
        }
        dispatch(setUserGetSuccess(data));
      })
      .catch(() => {
        dispatch(setUserGetError(true));
        dispatch(setUserGetLoading(false));
      });
  };
}

export function userAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setUserAddError(false));
    dispatch(setUserAddSuccess(false));
    dispatch(setUserAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + REGULATOR_URL + "/new", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((user) => {
        dispatch(setUserAddSuccess(user));
        alert('Tambah user berhasil');
      })
      .catch(() => {
        dispatch(setUserAddError(true));
        dispatch(setUserAddLoading(false));
      });
  };
}

export function userChangePassword(data) {
  return async function (dispatch, getState) {
    dispatch(setUserAddError(false));
    dispatch(setUserAddSuccess(false));
    dispatch(setUserAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + USER_URL + "/change_password", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((user) => {
        dispatch(setUserAddSuccess(user));
      })
      .catch(() => {
        dispatch(setUserAddError(true));
        dispatch(setUserAddLoading(false));
      });
  };
}

export function userResetPassword(data) {
  return async function (dispatch, getState) {
    dispatch(setUserAddError(false));
    dispatch(setUserAddSuccess(false));
    dispatch(setUserAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + USER_URL + "/reset_password", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((user) => {
        alert("Sukses Ubah Password");
        dispatch(setUserAddSuccess(user));
      })
      .catch(() => {
        dispatch(setUserAddError(true));
        dispatch(setUserAddLoading(false));
      });
  };
}

export function userFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setUserViewError(false));
    dispatch(setUserViewSuccess(false));
    dispatch(setUserViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + USER_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setUserViewSuccess(data));
      })
      .catch(() => {
        dispatch(setUserViewError(true));
        dispatch(setUserViewLoading(false));
      });
  };
}

export function userFetchUser(id) {
  return async function (dispatch, getState) {
    alert("hello")
    dispatch(setUserViewError(false));
    dispatch(setUserViewSuccess(false));
    dispatch(setUserViewLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: id,
      }),
    };
    fetch(BASE_URL + REGULATOR_URL + "/profile", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setUserViewSuccess(data));
      })
      .catch(() => {
        dispatch(setUserViewError(true));
        dispatch(setUserViewLoading(false));
      });
  };
}

export function userUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setUserAddError(false));
    dispatch(setUserAddSuccess(false));
    dispatch(setUserAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + REGULATOR_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((user) => {
        if (user.success) {
          dispatch(setUserAddSuccess(user));
        } else {
          dispatch(setUserAddError(true));
          dispatch(setUserAddLoading(false));
        }
      })
      .catch(() => {
        dispatch(setUserAddError(true));
        dispatch(setUserAddLoading(false));
      });
  };
}

export function handleStatus(data) {
  return async function (dispatch, getState) {
    dispatch(setUserAddError(false));
    dispatch(setUserAddSuccess(false));
    dispatch(setUserAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: data.id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "user/" + data.status, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((user) => {
        dispatch(setUserAddSuccess(user));
      })
      .catch(() => {
        dispatch(setUserAddError(true));
        dispatch(setUserAddLoading(false));
      });
  };
}

export function userDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setUserViewSuccess(false));
    dispatch(setUserDeleteError(false));
    dispatch(setUserDeleteSuccess(false));
    dispatch(setUserDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + USER_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUserDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setUserDeleteSuccess(data));
      })
      .catch(() => {
        dispatch(setUserDeleteError(true));
        dispatch(setUserDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function userFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setUserFilterAdd(filter));
  };
}

export function userFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setUserFilterDel(id));
  };
}

export function userFilterClear() {
  return function (dispatch, getState) {
    dispatch(setUserFilterClear());
  };
}

export default userReducer;
