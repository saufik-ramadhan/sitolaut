import React from 'react';
import { BASE_URL } from '../config/constants';
import AsyncStorage from '../AsyncStorage';

const updateBookingStatus = async props => {
    console.log(props,'cuk')
    let token = await AsyncStorage.getItem('token@lcs') || null;
    let config = {}
    config = {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    const res = await fetch(BASE_URL + `update_booking_status/${props}`, config)
    if (!res.ok) {
        throw Error(res.statusText)
    }
    const resJson = await res.json()
    console.log(resJson)
    return resJson
}

export default updateBookingStatus