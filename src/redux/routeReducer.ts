import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, ROUTE_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  routeGetError: false,
  routeGetLoading: false,
  routeGetSuccess: false,
  routeViewError: false,
  routeViewLoading: false,
  routeViewSuccess: false,
  routeAddError: false,
  routeAddLoading: false,
  routeAddSuccess: false,
  routeDeleteError: false,
  routeDeleteLoading: false,
  routeDeleteSuccess: false,

  routeData: [],
  routeFilter: {},
};

/**
 * ACTION
 */
const setRouteGetError = createAction('setRouteGetError', (data: any) => data);
const setRouteGetLoading = createAction('setRouteGetLoading', (data: any) => data);
const setRouteGetSuccess = createAction('setRouteGetSuccess', (data: any) => data);
const setRouteViewError = createAction('setRouteViewError', (data: any) => data);
const setRouteViewLoading = createAction('setRouteViewLoading', (data: any) => data);
const setRouteViewSuccess = createAction('setRouteViewSuccess', (data: any) => data);
const setRouteAddError = createAction('setRouteAddError', (data: any) => data);
const setRouteAddLoading = createAction('setRouteAddLoading', (data: any) => data);
const setRouteAddSuccess = createAction('setRouteAddSuccess', (data: any) => data);
const setRouteDeleteError = createAction('setRouteDeleteError', (data: any) => data);
const setRouteDeleteLoading = createAction('setRouteDeleteLoading', (data: any) => data);
const setRouteDeleteSuccess = createAction('setRouteDeleteSuccess', (data: any) => data);

const setRouteData = createAction('setRouteData', (data: any) => data);
const setRouteFilterAdd = createAction('setRouteFilterAdd', (data: any) => data);
const setRouteFilterDel = createAction('setRouteFilterDel', (data: any) => data);
const setRouteFilterClear = createAction('setRouteFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const routeReducer = createReducer(
  {
    [setRouteGetError]: (state, data) => ({ ...state, routeGetError: data }),
    [setRouteGetLoading]: (state, data) => ({
      ...state,
      routeGetLoading: data,
    }),
    [setRouteGetSuccess]: (state, data) => ({
      ...state,
      routeGetSuccess: data,
    }),
    [setRouteViewError]: (state, data) => ({ ...state, routeViewError: data }),
    [setRouteViewLoading]: (state, data) => ({
      ...state,
      routeViewLoading: data,
    }),
    [setRouteViewSuccess]: (state, data) => ({
      ...state,
      routeViewSuccess: data,
    }),
    [setRouteAddError]: (state, data) => ({ ...state, routeAddError: data }),
    [setRouteAddLoading]: (state, data) => ({
      ...state,
      routeAddLoading: data,
    }),
    [setRouteAddSuccess]: (state, data) => ({
      ...state,
      routeAddSuccess: data,
    }),
    [setRouteDeleteError]: (state, data) => ({
      ...state,
      routeDeleteError: data,
    }),
    [setRouteDeleteLoading]: (state, data) => ({
      ...state,
      routeDeleteLoading: data,
    }),
    [setRouteDeleteSuccess]: (state, data) => ({
      ...state,
      routeDeleteSuccess: data,
    }),

    [setRouteData]: (state, data: Array<"any">) => ({
      ...state,
      routeData: data,
    }),
    [setRouteFilterAdd]: (state, data: any) => ({
      ...state,
      routeFilter: data,
    }),
    [setRouteFilterDel]: (state, id: any) => ({
      ...state,
      routeFilter: {},
    }),
    [setRouteFilterClear]: (state, data: any) => ({
      ...state,
      routeFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function routeFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setRouteAddSuccess(false));
    dispatch(setRouteGetSuccess(false));
    dispatch(setRouteGetError(false));
    dispatch(setRouteGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROUTE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRouteGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().route.routeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setRouteData(data.data));
        } else {
          dispatch(setRouteData(prev.concat(data.data)));
        }
        dispatch(setRouteGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setRouteGetError(true));
      });
  };
}

export function routeFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setRouteAddSuccess(false));
    dispatch(setRouteGetSuccess(false));
    dispatch(setRouteGetError(false));
    dispatch(setRouteGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");

    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROUTE_URL + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRouteGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().route.routeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setRouteData(data.data));
        } else {
          dispatch(setRouteData(prev.concat(data.data)));
        }
        dispatch(setRouteGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setRouteGetError(true));
        // alert("gagal");
      });
  };
}

export function routeReset() {
  return async function (dispatch, getState) {
    dispatch(setRouteGetSuccess(false));
  };
}

export function routeFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setRouteAddSuccess(false));
    dispatch(setRouteGetSuccess(false));
    dispatch(setRouteGetError(false));
    dispatch(setRouteGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROUTE_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRouteGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().route.routeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setRouteData(data.data));
        } else {
          dispatch(setRouteData(prev.concat(data.data)));
        }
        dispatch(setRouteGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setRouteGetError(true));
      });
  };
}

export function routeAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setRouteAddLoading(true));
    dispatch(setRouteAddError(false));
    dispatch(setRouteAddSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + ROUTE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRouteAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((route) => {
        if (route.success) {
          alert("Berhasil Menambahkan Trayek");
          dispatch(setRouteAddSuccess(route));
        } else {
          alert(route.message);
          dispatch(setRouteAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setRouteAddError(true));
      });
  };
}

export function routeFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setRouteAddSuccess(false));
    dispatch(setRouteDeleteSuccess(false));
    dispatch(setRouteViewError(false));
    dispatch(setRouteViewSuccess(false));
    dispatch(setRouteViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROUTE_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRouteViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRouteViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setRouteViewError(true));
      });
  };
}

export function routeUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setRouteAddError(false));
    dispatch(setRouteAddSuccess(false));
    dispatch(setRouteAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROUTE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRouteAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((route) => {
        if (route.success) {
          dispatch(setRouteAddSuccess(route));
        } else {
          dispatch(setRouteAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setRouteAddError(true));
      });
  };
}

export function routeDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setRouteViewSuccess(false));
    dispatch(setRouteDeleteError(false));
    dispatch(setRouteDeleteSuccess(false));
    dispatch(setRouteDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROUTE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRouteDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRouteDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setRouteDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function routeFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setRouteFilterAdd(filter));
  };
}

export function routeFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setRouteFilterDel(id));
  };
}

export function routeFilterClear() {
  return function (dispatch, getState) {
    dispatch(setRouteFilterClear());
  };
}

/** Clear */
export function routeFetchClear() {
  return function (dispatch) {
    dispatch(setRouteGetSuccess(false));
  }
}

export default routeReducer;
