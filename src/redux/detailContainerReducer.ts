import AsyncStorage from "../AsyncStorage";
import { BASE_URL,DETAILCONTAINER_URL } from "../config/constants";
import { createReducer, createAction } from "redux-act";
import { doLogout } from "./authReducers";

const DefaultState = {
  detailContainerGetError: false,
  detailContainerGetLoading: false,
  detailContainerGetSuccess: false,
  detailContainerViewError: false,
  detailContainerViewLoading: false,
  detailContainerViewSuccess: false,
  detailContainerAddError: false,
  detailContainerAddLoading: false,
  detailContainerAddInvalid: false,
  detailContainerAddSuccess: false,
  detailContainerDeleteError: false,
  detailContainerDeleteLoading: false,
  detailContainerDeleteSuccess: false,
};

/**
 * ACTION
 */
const setDetailContainerGetError = createAction('setDetailContainerGetError', (data: any) => data);
const setDetailContainerGetLoading = createAction('setDetailContainerGetLoading', (data: any) => data);
const setDetailContainerGetSuccess = createAction('setDetailContainerGetSuccess', (data: any) => data);
const setDetailContainerViewError = createAction('setDetailContainerViewError', (data: any) => data);
const setDetailContainerViewLoading = createAction('setDetailContainerViewLoading', (data: any) => data);
const setDetailContainerViewSuccess = createAction('setDetailContainerViewSuccess', (data: any) => data);
const setDetailContainerAddError = createAction('setDetailContainerAddError', (data: any) => data);
const setDetailContainerAddLoading = createAction('setDetailContainerAddLoading', (data: any) => data);
const setDetailContainerAddInvalid = createAction('setDetailContainerAddInvalid', (data: any) => data);
const setDetailContainerAddSuccess = createAction('setDetailContainerAddSuccess', (data: any) => data);
const setDetailContainerDeleteError = createAction('setDetailContainerDeleteError', (data: any) => data);
const setDetailContainerDeleteLoading = createAction('setDetailContainerDeleteLoading', (data: any) => data);
const setDetailContainerDeleteSuccess = createAction('setDetailContainerDeleteSuccess', (data: any) => data);

/**
 * REDUCER
 */
const detailContainerReducer = createReducer(
  {
    [setDetailContainerGetError]: (state, data) => ({
      ...state,
      detailContainerGetError: data,
    }),
    [setDetailContainerGetLoading]: (state, data) => ({
      ...state,
      detailContainerGetLoading: data,
    }),
    [setDetailContainerGetSuccess]: (state, data) => ({
      ...state,
      detailContainerGetSuccess: data,
    }),
    [setDetailContainerViewError]: (state, data) => ({
      ...state,
      detailContainerViewError: data,
    }),
    [setDetailContainerViewLoading]: (state, data) => ({
      ...state,
      detailContainerViewLoading: data,
    }),
    [setDetailContainerViewSuccess]: (state, data) => ({
      ...state,
      detailContainerViewSuccess: data,
    }),
    [setDetailContainerAddError]: (state, data) => ({
      ...state,
      detailContainerAddError: data,
    }),
    [setDetailContainerAddLoading]: (state, data) => ({
      ...state,
      detailContainerAddLoading: data,
    }),
    [setDetailContainerAddInvalid]: (state, data) => ({
      ...state,
      detailContainerAddInvalid: data,
    }),
    [setDetailContainerAddSuccess]: (state, data) => ({
      ...state,
      detailContainerAddSuccess: data,
    }),
    [setDetailContainerDeleteError]: (state, data) => ({
      ...state,
      detailContainerDeleteError: data,
    }),
    [setDetailContainerDeleteLoading]: (state, data) => ({
      ...state,
      detailContainerDeleteLoading: data,
    }),
    [setDetailContainerDeleteSuccess]: (state, data) => ({
      ...state,
      detailContainerDeleteSuccess: data,
    }),
  },
  DefaultState
);


export function detailcontainerFetchAll() {
  return async (dispatch) => {
    dispatch(setDetailContainerGetSuccess(false));
    dispatch(setDetailContainerGetError(false));
    dispatch(setDetailContainerGetLoading(true));

    let token = await AsyncStorage.getItem("token@lcs") || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILCONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailContainerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDetailContainerGetLoading(false));
        dispatch(setDetailContainerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDetailContainerGetLoading(false));
        dispatch(setDetailContainerGetError(true));
      });
  };
}

export function detailcontainerFetchSub(param) {
  return async (dispatch) => {
    dispatch(setDetailContainerDeleteSuccess(false));
    dispatch(setDetailContainerAddSuccess(false));
    dispatch(setDetailContainerGetSuccess(false));
    dispatch(setDetailContainerGetError(false));
    dispatch(setDetailContainerGetLoading(true));

    let token = await AsyncStorage.getItem("token@lcs") || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILCONTAINER_URL + "/" + param, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailContainerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDetailContainerGetLoading(false));
        dispatch(setDetailContainerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDetailContainerGetLoading(false));
        dispatch(setDetailContainerGetError(true));
      });
  };
}

export function detailcontainerAdd(data) {
  return async (dispatch) => {
    dispatch(setDetailContainerAddError(false));
    dispatch(setDetailContainerAddSuccess(false));
    dispatch(setDetailContainerAddLoading(true));

    let token = await AsyncStorage.getItem("token@lcs") || null;
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + DETAILCONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailContainerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((detailcontainer) => {
        dispatch(setDetailContainerAddLoading(false));
        dispatch(setDetailContainerAddSuccess(detailcontainer));
      })
      .catch((err) => {
        dispatch(setDetailContainerAddLoading(false));
        dispatch(setDetailContainerAddError(true));
      });
  };
}

export function detailcontainerFetchOne(id) {
  return async (dispatch) => {
    dispatch(setDetailContainerViewError(false));
    dispatch(setDetailContainerViewSuccess(false));
    dispatch(setDetailContainerViewLoading(true));

    let token = await AsyncStorage.getItem("token@lcs") || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILCONTAINER_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailContainerViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDetailContainerViewLoading(false));
        dispatch(setDetailContainerViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setDetailContainerViewLoading(false));
        dispatch(setDetailContainerViewError(true));
      });
  };
}

export function detailcontainerDelete(id) {
  return async (dispatch) => {
    dispatch(setDetailContainerDeleteError(false));
    dispatch(setDetailContainerDeleteSuccess(false));
    dispatch(setDetailContainerDeleteLoading(true));

    let token = await AsyncStorage.getItem("token@lcs") || null;
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(id),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILCONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailContainerDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
          dispatch(setDetailContainerDeleteSuccess(data));
        dispatch(setDetailContainerDeleteLoading(false));
          
      })
      .catch((err) => {
          dispatch(setDetailContainerDeleteError(true));
        dispatch(setDetailContainerDeleteLoading(false));
          
      });
  };
}

export default detailContainerReducer;