import { createAction, createReducer } from "redux-act";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, JENISBARANG_URL } from "../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */

const DefaultState = {
  jenisbarangGetError: false,
  jenisbarangGetLoading: false,
  jenisbarangGetSuccess: false,
  jenisbarangViewLoading: false,
  jenisbarangViewError: false,
  jenisbarangViewSuccess: false,
  jenisbarangAddLoading: false,
  jenisbarangAddError: false,
  jenisbarangAddSuccess: false,
  jenisbarangEditLoading: false,
  jenisbarangEditError: false,
  jenisbarangEditSuccess: false,
  jenisbarangDeleteLoading: false,
  jenisbarangDeleteError: false,
  jenisbarangDeleteSuccess: false,

  jenisbarangData: [],
  jenisbarangFilter: {},
};

/**
 * ACTION
 */
const setJenisbarangGetError = createAction('setJenisbarangGetError', (data: any) => data);
const setJenisbarangGetLoading = createAction('setJenisbarangGetLoading', (data: any) => data);
const setJenisbarangGetSuccess = createAction('setJenisbarangGetSuccess', (data: any) => data);
const setJenisbarangViewLoading = createAction('setJenisbarangViewLoading', (data: any) => data);
const setJenisbarangViewError = createAction('setJenisbarangViewError', (data: any) => data);
const setJenisbarangViewSuccess = createAction('setJenisbarangViewSuccess', (data: any) => data);
const setJenisbarangAddLoading = createAction('setJenisbarangAddLoading', (data: any) => data);
const setJenisbarangAddError = createAction('setJenisbarangAddError', (data: any) => data);
const setJenisbarangAddSuccess = createAction('setJenisbarangAddSuccess', (data: any) => data);
const setJenisbarangEditLoading = createAction('setJenisbarangEditLoading', (data: any) => data);
const setJenisbarangEditError = createAction('setJenisbarangEditError', (data: any) => data);
const setJenisbarangEditSuccess = createAction('setJenisbarangEditSuccess', (data: any) => data);
const setJenisbarangDeleteLoading = createAction('setJenisbarangDeleteLoading', (data: any) => data);
const setJenisbarangDeleteError = createAction('setJenisbarangDeleteError', (data: any) => data);
const setJenisbarangDeleteSuccess = createAction('setJenisbarangDeleteSuccess', (data: any) => data);

const setJenisbarangData = createAction('setJenisbarangData', (data: any) => data);
const setJenisbarangFilterAdd = createAction('setJenisbarangFilterAdd', (data: any) => data);
const setJenisbarangFilterDel = createAction('setJenisbarangFilterDel', (data: any) => data);
const setJenisbarangFilterClear = createAction('setJenisbarangFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const jenisbarangReducer = createReducer(
  {
    [setJenisbarangGetError]: (state, data) => ({
      ...state,
      jenisbarangGetError: data,
    }),
    [setJenisbarangGetLoading]: (state, data) => ({
      ...state,
      jenisbarangGetLoading: data,
    }),
    [setJenisbarangGetSuccess]: (state, data) => ({
      ...state,
      jenisbarangGetSuccess: data,
    }),
    [setJenisbarangViewLoading]: (state, data) => ({
      ...state,
      jenisbarangViewLoading: data,
    }),
    [setJenisbarangViewError]: (state, data) => ({
      ...state,
      jenisbarangViewError: data,
    }),
    [setJenisbarangViewSuccess]: (state, data) => ({
      ...state,
      jenisbarangViewSuccess: data,
    }),
    [setJenisbarangAddLoading]: (state, data) => ({
      ...state,
      jenisbarangAddLoading: data,
    }),
    [setJenisbarangAddError]: (state, data) => ({
      ...state,
      jenisbarangAddError: data,
    }),
    [setJenisbarangAddSuccess]: (state, data) => ({
      ...state,
      jenisbarangAddSuccess: data,
    }),
    [setJenisbarangEditLoading]: (state, data) => ({
      ...state,
      jenisbarangEditLoading: data,
    }),
    [setJenisbarangEditError]: (state, data) => ({
      ...state,
      jenisbarangEditError: data,
    }),
    [setJenisbarangEditSuccess]: (state, data) => ({
      ...state,
      jenisbarangEditSuccess: data,
    }),
    [setJenisbarangDeleteLoading]: (state, data) => ({
      ...state,
      jenisbarangDeleteLoading: data,
    }),
    [setJenisbarangDeleteError]: (state, data) => ({
      ...state,
      jenisbarangDeleteError: data,
    }),
    [setJenisbarangDeleteSuccess]: (state, data) => ({
      ...state,
      jenisbarangDeleteSuccess: data,
    }),

    [setJenisbarangData]: (state, data: Array<"any">) => ({
      ...state,
      jenisbarangData: data,
    }),
    [setJenisbarangFilterAdd]: (state, data: any) => ({
      ...state,
      jenisbarangFilter: data,
    }),
    [setJenisbarangFilterDel]: (state, id: any) => ({
      ...state,
      jenisbarangFilter: {},
    }),
    [setJenisbarangFilterClear]: (state, data: any) => ({
      ...state,
      jenisbarangFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function jenisbarangFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setJenisbarangGetSuccess(false));
    dispatch(setJenisbarangGetError(false));
    dispatch(setJenisbarangGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JENISBARANG_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setJenisbarangGetLoading(false));

        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().jenisbarang.jenisbarangData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setJenisbarangData(data.data));
        } else {
          dispatch(setJenisbarangData(prev.concat(data.data)));
        }
        dispatch(setJenisbarangGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setJenisbarangGetLoading(false));
        dispatch(setJenisbarangGetError(true));
      });
  };
}

export function jenisbarangFetchPage(data) {
  return async function (dispatch, getState) {
    dispatch(setJenisbarangGetSuccess(false));
    dispatch(setJenisbarangGetError(false));
    dispatch(setJenisbarangGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JENISBARANG_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setJenisbarangGetLoading(false));

        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().jenisbarang.jenisbarangData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setJenisbarangData(data.data));
        } else {
          dispatch(setJenisbarangData(prev.concat(data.data)));
        }
        dispatch(setJenisbarangGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setJenisbarangGetLoading(false));
        dispatch(setJenisbarangGetError(true));
      });
  };
}

export function jenisbarangFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setJenisbarangViewSuccess(false));
    dispatch(setJenisbarangViewError(false));
    dispatch(setJenisbarangViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JENISBARANG_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setJenisbarangViewLoading(false));

        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setJenisbarangViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setJenisbarangViewLoading(false));
        dispatch(setJenisbarangViewError(true));
      });
  };
}

export function jenisbarangAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setJenisbarangAddSuccess(false));
    dispatch(setJenisbarangAddError(false));
    dispatch(setJenisbarangAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JENISBARANG_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setJenisbarangAddLoading(false));

        return response;
      })
      .then((response) => response.json())
      .then(() => {
        dispatch(setJenisbarangAddSuccess(true));
      })
      .catch((err) => {
        dispatch(setJenisbarangAddLoading(false));
        dispatch(setJenisbarangAddError(true));
      });
  };
}

export function jenisbarangEdit(data) {
  return async function (dispatch, getState) {
    dispatch(setJenisbarangEditSuccess(false));
    dispatch(setJenisbarangEditError(false));
    dispatch(setJenisbarangEditLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JENISBARANG_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setJenisbarangEditLoading(false));

        return response;
      })
      .then((response) => response.json())
      .then(() => {
        dispatch(setJenisbarangEditSuccess(true));
      })
      .catch((err) => {
        dispatch(setJenisbarangEditLoading(false));
        dispatch(setJenisbarangEditError(true));
      });
  };
}

export function jenisbarangDelete(data) {
  return async function (dispatch, getState) {
    dispatch(setJenisbarangDeleteSuccess(false));
    dispatch(setJenisbarangDeleteError(false));
    dispatch(setJenisbarangDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JENISBARANG_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setJenisbarangDeleteLoading(false));

        return response;
      })
      .then((response) => response.json())
      .then(() => {
        dispatch(setJenisbarangDeleteSuccess(true));
      })
      .catch((err) => {
        dispatch(setJenisbarangDeleteLoading(false));
        dispatch(setJenisbarangDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function jenisbarangFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setJenisbarangFilterAdd(filter));
  };
}

export function jenisbarangFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setJenisbarangFilterDel(id));
  };
}

export function jenisbarangFilterClear() {
  return function (dispatch, getState) {
    dispatch(setJenisbarangFilterClear());
  };
}

export default jenisbarangReducer;
