import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, JENISCOMMODITY_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  jeniscommodityGetError: false,
  jeniscommodityGetLoading: false,
  jeniscommodityGetSuccess: false,
  jeniscommodityViewError: false,
  jeniscommodityViewLoading: false,
  jeniscommodityViewSuccess: false,
  jeniscommodityAddError: false,
  jeniscommodityAddLoading: false,
  jeniscommodityAddSuccess: false,
  jeniscommodityDeleteError: false,
  jeniscommodityDeleteLoading: false,
  jeniscommodityDeleteSuccess: false,

  jeniscommodityData: [],
  jeniscommodityFilter: {},
};

/**
 * ACTION
 */

const setJeniscommodityGetError = createAction('setJeniscommodityGetError', (data: any) => data);
const setJeniscommodityGetLoading = createAction('setJeniscommodityGetLoading', (data: any) => data);
const setJeniscommodityGetSuccess = createAction('setJeniscommodityGetSuccess', (data: any) => data);
const setJeniscommodityViewError = createAction('setJeniscommodityViewError', (data: any) => data);
const setJeniscommodityViewLoading = createAction('setJeniscommodityViewLoading', (data: any) => data);
const setJeniscommodityViewSuccess = createAction('setJeniscommodityViewSuccess', (data: any) => data);
const setJeniscommodityAddError = createAction('setJeniscommodityAddError', (data: any) => data);
const setJeniscommodityAddLoading = createAction('setJeniscommodityAddLoading', (data: any) => data);
const setJeniscommodityAddSuccess = createAction('setJeniscommodityAddSuccess', (data: any) => data);
const setJeniscommodityDeleteError = createAction('setJeniscommodityDeleteError', (data: any) => data);
const setJeniscommodityDeleteLoading = createAction('setJeniscommodityDeleteLoading', (data: any) => data);
const setJeniscommodityDeleteSuccess = createAction('setJeniscommodityDeleteSuccess', (data: any) => data);

const setJeniscommodityData = createAction('setJeniscommodityData', (data: any) => data);
const setJeniscommodityFilterAdd = createAction('setJeniscommodityFilterAdd', (data: any) => data);
const setJeniscommodityFilterDel = createAction('setJeniscommodityFilterDel', (data: any) => data);
const setJeniscommodityFilterClear = createAction('setJeniscommodityFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const jeniscommodityReducer = createReducer(
  {
    [setJeniscommodityGetError]: (state, data) => ({
      ...state,
      jeniscommodityGetError: data,
    }),
    [setJeniscommodityGetLoading]: (state, data) => ({
      ...state,
      jeniscommodityGetLoading: data,
    }),
    [setJeniscommodityGetSuccess]: (state, data) => ({
      ...state,
      jeniscommodityGetSuccess: data,
    }),
    [setJeniscommodityViewError]: (state, data) => ({
      ...state,
      jeniscommodityViewError: data,
    }),
    [setJeniscommodityViewLoading]: (state, data) => ({
      ...state,
      jeniscommodityViewLoading: data,
    }),
    [setJeniscommodityViewSuccess]: (state, data) => ({
      ...state,
      jeniscommodityViewSuccess: data,
    }),
    [setJeniscommodityAddError]: (state, data) => ({
      ...state,
      jeniscommodityAddError: data,
    }),
    [setJeniscommodityAddLoading]: (state, data) => ({
      ...state,
      jeniscommodityAddLoading: data,
    }),
    [setJeniscommodityAddSuccess]: (state, data) => ({
      ...state,
      jeniscommodityAddSuccess: data,
    }),
    [setJeniscommodityDeleteError]: (state, data) => ({
      ...state,
      jeniscommodityDeleteError: data,
    }),
    [setJeniscommodityDeleteLoading]: (state, data) => ({
      ...state,
      jeniscommodityDeleteLoading: data,
    }),
    [setJeniscommodityDeleteSuccess]: (state, data) => ({
      ...state,
      jeniscommodityDeleteSuccess: data,
    }),

    [setJeniscommodityData]: (state, data: Array<"any">) => ({
      ...state,
      jeniscommodityData: data,
    }),
    [setJeniscommodityFilterAdd]: (state, data: any) => ({
      ...state,
      jeniscommodityFilter: data,
    }),
    [setJeniscommodityFilterDel]: (state, id: any) => ({
      ...state,
      jeniscommodityFilter: {},
    }),
    [setJeniscommodityFilterClear]: (state, data: any) => ({
      ...state,
      jeniscommodityFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function jeniscommodityFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setJeniscommodityGetSuccess(false));
    dispatch(setJeniscommodityGetError(false));
    dispatch(setJeniscommodityGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + JENISCOMMODITY_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setJeniscommodityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().jeniscommodity.jeniscommodityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setJeniscommodityData(data.data));
        } else {
          dispatch(setJeniscommodityData(prev.concat(data.data)));
        }
        dispatch(setJeniscommodityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setJeniscommodityGetError(true));
      });
  };
}

export function jeniscommodityFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setJeniscommodityGetSuccess(false));
    dispatch(setJeniscommodityGetError(false));
    dispatch(setJeniscommodityGetLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + JENISCOMMODITY_URL + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setJeniscommodityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().jeniscommodity.jeniscommodityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setJeniscommodityData(data.data));
        } else {
          dispatch(setJeniscommodityData(prev.concat(data.data)));
        }
        dispatch(setJeniscommodityGetSuccess(data));
        // alert(JSON.stringify(data.data));
      })
      .catch((err) => {
        dispatch(setJeniscommodityGetError(true));
      });
  };
}

export function jeniscommodityFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setJeniscommodityGetSuccess(false));
    dispatch(setJeniscommodityGetError(false));
    dispatch(setJeniscommodityGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + JENISCOMMODITY_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setJeniscommodityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().jeniscommodity.jeniscommodityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setJeniscommodityData(data.data));
        } else {
          dispatch(setJeniscommodityData(prev.concat(data.data)));
        }
        dispatch(setJeniscommodityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setJeniscommodityGetError(true));
      });
  };
}

export function jeniscommodityFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setJeniscommodityViewLoading(true));
    dispatch(setJeniscommodityViewSuccess(false));
    dispatch(setJeniscommodityViewError(false));
    

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + JENISCOMMODITY_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setJeniscommodityViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setJeniscommodityViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setJeniscommodityViewError(true));
      });
  };
}

export function jeniscommodityAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setJeniscommodityAddError(false));
    dispatch(setJeniscommodityAddSuccess(false));
    dispatch(setJeniscommodityAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JENISCOMMODITY_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setJeniscommodityAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          dispatch(setJeniscommodityAddSuccess(true));
        }
      })
      .catch((err) => {
        dispatch(setJeniscommodityAddError(true));
      });
  };
}

export function jeniscommodityUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setJeniscommodityAddError(false));
    dispatch(setJeniscommodityAddSuccess(false));
    dispatch(setJeniscommodityAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + JENISCOMMODITY_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setJeniscommodityAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          alert("Sukses");
          dispatch(setJeniscommodityAddSuccess(true));
        } else {
          dispatch(setJeniscommodityAddError(true));
        }
      })
      .catch((err) => {
        alert("Gagal");
        dispatch(setJeniscommodityAddError(true));
      });
  };
}

export function jeniscommodityDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setJeniscommodityDeleteError(false));
    dispatch(setJeniscommodityDeleteSuccess(false));
    dispatch(setJeniscommodityDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + JENISCOMMODITY_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setJeniscommodityDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          dispatch(setJeniscommodityDeleteSuccess(true));
        }
      })
      .catch((err) => {
        dispatch(setJeniscommodityDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function jeniscommodityFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setJeniscommodityFilterAdd(filter));
  };
}

export function jeniscommodityFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setJeniscommodityFilterDel(id));
  };
}

export function jeniscommodityFilterClear() {
  return function (dispatch, getState) {
    dispatch(setJeniscommodityFilterClear());
  };
}

export default jeniscommodityReducer;
