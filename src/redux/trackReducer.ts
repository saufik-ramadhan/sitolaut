import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { BASE_URL, TRACK_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";
import { RootState } from "./rootReducer";

/** DEFAULT STATE */

const DefaultState = {
  trackGetError: false,
  trackGetLoading: false,
  trackGetSuccess: false,
  trackViewError: false,
  trackViewLoading: false,
  trackViewSuccess: false,
  trackAddError: false,
  trackAddLoading: false,
  trackAddSuccess: false,
  trackData: [],
  trackFilter: {},
};

/** ACTION */
const setTrackGetError = createAction('setTrackGetError', (data: any) => data);
const setTrackGetLoading = createAction('setTrackGetLoading', (data: any) => data);
const setTrackGetSuccess = createAction('setTrackGetSuccess', (data: any) => data);
const setTrackViewError = createAction('setTrackViewError', (data: any) => data);
const setTrackViewLoading = createAction('setTrackViewLoading', (data: any) => data);
const setTrackViewSuccess = createAction('setTrackViewSuccess', (data: any) => data);
const setTrackAddError = createAction('setTrackAddError', (data: any) => data);
const setTrackAddLoading = createAction('setTrackAddLoading', (data: any) => data);
const setTrackAddSuccess = createAction('setTrackAddSuccess', (data: any) => data);

const setTrackData = createAction('setTrackData', (data: any) => data);
const setTrackFilterAdd = createAction('setTrackFilterAdd', (data: any) => data);
const setTrackFilterDel = createAction('setTrackFilterDel', (data: any) => data);
const setTrackFilterClear = createAction('setTrackFilterClear', (data: any) => data);

/** REDUCER */
const trackReducer = createReducer(
  {
    [setTrackGetError]: (state, data) => ({ ...state, trackGetError: data }),
    [setTrackGetLoading]: (state, data) => ({
      ...state,
      trackGetLoading: data,
    }),
    [setTrackGetSuccess]: (state, data) => ({
      ...state,
      trackGetSuccess: data,
    }),
    [setTrackViewError]: (state, data) => ({ ...state, trackViewError: data }),
    [setTrackViewLoading]: (state, data) => ({
      ...state,
      trackViewLoading: data,
    }),
    [setTrackViewSuccess]: (state, data) => ({
      ...state,
      trackViewSuccess: data,
    }),
    [setTrackAddError]: (state, data) => ({ ...state, trackAddError: data }),
    [setTrackAddLoading]: (state, data) => ({
      ...state,
      trackAddLoading: data,
    }),
    [setTrackAddSuccess]: (state, data) => ({
      ...state,
      trackAddSuccess: data,
    }),
    [setTrackData]: (state, data: Array<"any">) => ({
      ...state,
      trackData: data,
    }),
    [setTrackFilterAdd]: (state, data: any) => ({
      ...state,
      trackFilter: data,
    }),
    [setTrackFilterDel]: (state, id: any) => ({
      ...state,
      trackFilter: {},
    }),
    [setTrackFilterClear]: (state, data: any) => ({
      ...state,
      trackFilter: {},
    }),
  },
  DefaultState
);

/** API */
export function trackFetchAll() {
  return async function (dispatch) {
    dispatch(setTrackGetSuccess(null));
    dispatch(setTrackGetError(false));
    dispatch(setTrackGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRACK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTrackGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setTrackGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setTrackGetError(true));
        dispatch(setTrackGetLoading(false));
      });
  };
}

export function trackFetchSub(path) {
  return async function (dispatch) {
    dispatch(setTrackGetSuccess(null));
    dispatch(setTrackGetError(false));
    dispatch(setTrackGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRACK_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTrackGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setTrackGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setTrackGetError(true));
        dispatch(setTrackGetLoading(false));
      });
  };
}

export function trackFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setTrackGetSuccess(false));
    dispatch(setTrackGetError(false));
    dispatch(setTrackGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRACK_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTrackGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().track.trackData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setTrackData(data.data));
        } else {
          dispatch(setTrackData(prev.concat(data.data)));
        }
        dispatch(setTrackGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setTrackGetError(true));
        dispatch(setTrackGetLoading(false));
      });
  };
}

export function trackAdd(data) {
  return async function (dispatch) {
    dispatch(setTrackAddError(false));
    dispatch(setTrackAddSuccess(null));
    dispatch(setTrackAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRACK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTrackAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((track) => {
        dispatch(setTrackAddSuccess(track));
      })
      .catch((err) => {
        dispatch(setTrackAddError(true));
        dispatch(setTrackAddLoading(false));
      });
  };
}

export function trackUpdate(data) {
  return async function (dispatch) {
    dispatch(setTrackAddError(false));
    dispatch(setTrackAddSuccess(null));
    dispatch(setTrackAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRACK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTrackAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((track) => {
        dispatch(setTrackAddSuccess(track));
      })
      .catch((err) => {
        dispatch(setTrackAddError(true));
        dispatch(setTrackAddLoading(false));
      });
  };
}

export function trackFetchOne(id) {
  return async function (dispatch) {
    dispatch(setTrackViewError(false));
    dispatch(setTrackViewSuccess(null));
    dispatch(setTrackViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRACK_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTrackViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setTrackViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setTrackViewError(true));
        dispatch(setTrackViewLoading(false));
      });
  };
}

/**
 * Filter
 */
export function trackFilterAdd(filter) {
  return function (dispatch) {
    dispatch(setTrackFilterAdd(filter));
  };
}

export function trackFilterDel(id) {
  return function (dispatch) {
    dispatch(setTrackFilterDel(id));
  };
}

export function trackFilterClear() {
  return function (dispatch) {
    dispatch(setTrackFilterClear());
  };
}

export default trackReducer;
