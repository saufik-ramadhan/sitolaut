import { doLogout } from './authReducers'
import { 
  BASE_URL,
  MASTER_KODE_TRAYEK_REPORT_URL,
  TRAYEK_BALIK_URL,
} from '../config/constants'
import { createAction, createReducer } from 'redux-act';
import AsyncStorage from "../AsyncStorage";

const DefaultState = {
  masterKodeTrayekGetSuccess: false,
  masterKodeTrayekGetLoading: false,
  masterKodeTrayekGetFailure: false,
  masterKodeTrayekYearGetSuccess: false,
  masterKodeTrayekYearGetLoading: false,
  masterKodeTrayekYearGetFailure: false,
  dataBarangPerRuasGetSuccess: false,
  dataBarangPerRuasGetLoading: false,
  dataBarangPerRuasGetFailure: false,
  trayekBalikViewSuccess: false,
  trayekBalikViewLoading: false,
  trayekBalikViewFailure: false,
  trayekBalikGetSuccess: false,
  trayekBalikGetLoading: false,
  trayekBalikGetFailure: false,
}

// Create Action
const setMasterKodeTrayekGetSuccess = createAction('setMasterKodeTrayekGetSuccess', (data: any) => data);
const setMasterKodeTrayekGetLoading = createAction('setMasterKodeTrayekGetLoading', (data: any) => data);
const setMasterKodeTrayekGetFailure = createAction('setMasterKodeTrayekGetFailure', (data: any) => data);
const setMasterKodeTrayekYearGetSuccess = createAction('setMasterKodeTrayekYearGetSuccess', (data: any) => data);
const setMasterKodeTrayekYearGetLoading = createAction('setMasterKodeTrayekYearGetLoading', (data: any) => data);
const setMasterKodeTrayekYearGetFailure = createAction('setMasterKodeTrayekYearGetFailure', (data: any) => data);
const setDataBarangPerRuasGetSuccess = createAction('setDataBarangPerRuasGetSuccess', (data: any) => data);
const setDataBarangPerRuasGetLoading = createAction('setDataBarangPerRuasGetLoading', (data: any) => data);
const setDataBarangPerRuasGetFailure = createAction('setDataBarangPerRuasGetFailure', (data: any) => data);
const setTrayekBalikViewSuccess = createAction('setTrayekBalikViewSuccess', (data: any) => data);
const setTrayekBalikViewLoading = createAction('setTrayekBalikViewLoading', (data: any) => data);
const setTrayekBalikViewFailure = createAction('setTrayekBalikViewFailure', (data: any) => data);
const setTrayekBalikGetSuccess = createAction('setTrayekBalikGetSuccess', (data: any) => data);
const setTrayekBalikGetLoading = createAction('setTrayekBalikGetLoading', (data: any) => data);
const setTrayekBalikGetFailure = createAction('setTrayekBalikGetFailure', (data: any) => data);

// Create Reducer
const reporttblReducer = createReducer({
  [setMasterKodeTrayekGetSuccess]: (state, data) => ({...state, masterKodeTrayekGetSuccess: data}),
  [setMasterKodeTrayekGetLoading]: (state, data) => ({...state, masterKodeTrayekGetLoading: data}),
  [setMasterKodeTrayekGetFailure]: (state, data) => ({...state, masterKodeTrayekGetFailure: data}),
  [setMasterKodeTrayekYearGetSuccess]: (state, data) => ({...state, masterKodeTrayekYearGetSuccess: data}),
  [setMasterKodeTrayekYearGetLoading]: (state, data) => ({...state, masterKodeTrayekYearGetLoading: data}),
  [setMasterKodeTrayekYearGetFailure]: (state, data) => ({...state, masterKodeTrayekYearGetFailure: data}),
  [setDataBarangPerRuasGetSuccess]: (state, data) => ({...state, dataBarangPerRuasGetSuccess: data}),
  [setDataBarangPerRuasGetLoading]: (state, data) => ({...state, dataBarangPerRuasGetLoading: data}),
  [setDataBarangPerRuasGetFailure]: (state, data) => ({...state, dataBarangPerRuasGetFailure: data}),
  [setTrayekBalikViewSuccess]: (state, data) => ({...state, trayekBalikViewSuccess: data}),
  [setTrayekBalikViewLoading]: (state, data) => ({...state, trayekBalikViewLoading: data}),
  [setTrayekBalikViewFailure]: (state, data) => ({...state, trayekBalikViewFailure: data}),
  [setTrayekBalikGetSuccess]: (state, data) => ({...state, trayekBalikGetSuccess: data}),
  [setTrayekBalikGetLoading]: (state, data) => ({...state, trayekBalikGetLoading: data}),
  [setTrayekBalikGetFailure]: (state, data) => ({...state, trayekBalikGetFailure: data}),
}, DefaultState)

/**
 * Data Barang Per Ruas
 */
export const dataBarangPerRuas = data => {
  return async (dispatch) => {
    // alert("Hi");
    dispatch(setDataBarangPerRuasGetSuccess(false));
    dispatch(setDataBarangPerRuasGetLoading(true));
    const token = await AsyncStorage.getItem('token@lcs');
    let config = {}
    if(token) {
      config = {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      }
    } else {
      throw Error("Authentication error")
    }
    fetch(BASE_URL + TRAYEK_BALIK_URL + `muatan/${data.id}/${encodeURI(data.kode)}`, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(setDataBarangPerRuasGetLoading(false));
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        if(data.status==200){
          // dispatch(modalBarangPeruas(true))
          dispatch(setDataBarangPerRuasGetSuccess(data.data))
        }else{
          dispatch(setDataBarangPerRuasGetFailure(true))
        }
        
      })
      .catch((err) => {
        console.log(err, 'err cuk')
        // alert('Error');
        dispatch(setDataBarangPerRuasGetFailure(true))
        dispatch(setDataBarangPerRuasGetLoading(false));
      })
  }
}

/**
 * Detail Trayek Balik
 * @param data 
 */
export const dataDetailTrayekBalik = data => {
  return async (dispatch) => {
    // dispatch(showTable(false))
    console.log(data);
    dispatch(setTrayekBalikViewLoading(true));
    dispatch(setTrayekBalikViewSuccess(false));
    const token = await AsyncStorage.getItem('token@lcs');
    let config = {}
    if(token){
      config = {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      }
    } else {
      throw Error("Authentication error")
    }
    // ?kodeTrayek=test&year=2020
    fetch(BASE_URL + TRAYEK_BALIK_URL + `details?kodeTrayek=${data.kode}&year=${data.date}`, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout())
        }
        if (!response.ok) {
          throw Error(response.statusText)
        }
        dispatch(setTrayekBalikViewLoading(false));
        return response
      })
      .then((response) => response.json())
      .then((data) => {
        if(data.status === 200){
          // dispatch(showTable(true))
        }
        dispatch(setTrayekBalikViewSuccess(data))
      })
      .catch((err) => {
        dispatch(setTrayekBalikViewFailure(true))
        dispatch(setTrayekBalikViewLoading(false));
        console.log(err, 'err cuk')
      })
  }
}

/**
 * Data Trayek Balik
 */
export const dataTrayekBalik = data => {
    return async (dispatch) => {
      dispatch(setTrayekBalikGetSuccess(false))
      dispatch(setTrayekBalikGetLoading(true))
      const token = await AsyncStorage.getItem('token@lcs');
      let config = {}
      if(token) {
          config = {
            method: 'GET',
            headers: {
              'Authorization': `Bearer ${token}`,
              'Content-Type': 'application/json'
            }
          }
        } else {
          throw Error("Authentication error")
        }
        fetch(BASE_URL + TRAYEK_BALIK_URL + `header?year=${data.date}`, config)
          .then((response) => {
            if (response.status === 401) {
              dispatch(doLogout())
            }
            if (!response.ok) {
              throw Error(response.statusText)
            }
            dispatch(setTrayekBalikGetLoading(false));
            return response
          })
          .then((response) => response.json())
          .then((data) => {
            dispatch(setTrayekBalikGetSuccess(data));
          })
          .catch((err) => {
            dispatch(setTrayekBalikGetFailure(true));
            dispatch(setTrayekBalikGetLoading(false));
            alert('error')
            console.log(err, 'err cuk')
          })
    }
}

const compare = (a, b) => {
  // Use toUpperCase() to ignore character casing
  const YearA = a.value.toUpperCase();
  const YearB = b.value.toUpperCase();

  let comparison = 0
  if (YearA > YearB) {
    comparison = 1;
  } else if (YearA < YearB) {
    comparison = -1;
  }
  return comparison;
}

/**
 * Fetch Semua Kode Trayek
 */
export const masterKodeTrayekFetchAll = () => {
    return async (dispatch) => {
        dispatch(setMasterKodeTrayekYearGetSuccess(false))
        dispatch(setMasterKodeTrayekGetSuccess(false))
        const token = await AsyncStorage.getItem('token@lcs');
        let config = {}
        if(token) {
            config = {
              method: 'GET',
              headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
              }
            }
          } else {
            throw Error("Authentication error")
          }
          fetch(BASE_URL + MASTER_KODE_TRAYEK_REPORT_URL, config)
          .then((response) => {
            if (response.status === 401) {
              dispatch(doLogout())
            }
            if (!response.ok) {
              throw Error(response.statusText)
            }
            return response
          })
          .then((response) => response.json())
          .then((data) => {
              if(data.status === 200){
                  if(data.data.data.length > 0){
                    const dates = []
                    const kode = []
                    data.data.data.map((val) => {
                        const kt = val.kode_trayek.substring(0,4)
                        if(!kode.some(id => id.value === kt)){
                          let obj = {value: kt, label: kt}
                          kode.push(obj)
                        }

                        if(!dates.some(id => id.value === val.tahun_trayek)){
                            let obj = {value:val.tahun_trayek, label:val.tahun_trayek}
                            dates.push(obj)
                        }
                        return true
                    })
                    dates.sort(compare)
                    dispatch(setMasterKodeTrayekYearGetSuccess(dates))
                    dispatch(setMasterKodeTrayekGetSuccess(kode))
                  }else {
                    const dates = [{value:'', label:'No Data Found'}]
                    console.log(data,'wow')
                    dispatch(setMasterKodeTrayekYearGetSuccess(false))
                  }
              }
          })
          .catch((err) => {
            console.log(err, 'err cuk')
          })
    }
}

export default reporttblReducer;