import { createAction, createReducer } from "redux-act";
import AsyncStorage from "../AsyncStorage";
import { CONTAINER_URL, BASE_URL } from "../config/constants";
import { setShipperAddSuccess } from "./shipperReducer";
import { doLogout } from "./authReducers";

const DefaultState = {
  containerGetError: false,
  containerGetLoading: false,
  containerGetSuccess: false,
  containerViewError: false,
  containerViewLoading: false,
  containerViewSuccess: false,
  containerAddError: false,
  containerAddLoading: false,
  containerAddInvalid: false,
  containerAddSuccess: false,
  containerDeleteError: false,
  containerDeleteLoading: false,
  containerDeleteSuccess: false,

  containerData: [],
  containerFilter: {},
};

/**
 * ACTION
 */
const setContainerGetError = createAction('setContainerGetError', (data: any) => data);
const setContainerGetLoading = createAction('setContainerGetLoading', (data: any) => data);
const setContainerGetSuccess = createAction('setContainerGetSuccess', (data: any) => data);
const setContainerViewError = createAction('setContainerViewError', (data: any) => data);
const setContainerViewLoading = createAction('setContainerViewLoading', (data: any) => data);
const setContainerViewSuccess = createAction('setContainerViewSuccess', (data: any) => data);
const setContainerAddError = createAction('setContainerAddError', (data: any) => data);
const setContainerAddLoading = createAction('setContainerAddLoading', (data: any) => data);
const setContainerAddInvalid = createAction('setContainerAddInvalid', (data: any) => data);
const setContainerAddSuccess = createAction('setContainerAddSuccess', (data: any) => data);
const setContainerDeleteError = createAction('setContainerDeleteError', (data: any) => data);
const setContainerDeleteLoading = createAction('setContainerDeleteLoading', (data: any) => data);
const setContainerDeleteSuccess = createAction('setContainerDeleteSuccess', (data: any) => data);

const setContainerData = createAction('setContainerData', (data: any) => data);
const setContainerFilterAdd = createAction('setContainerFilterAdd', (data: any) => data);
const setContainerFilterDel = createAction('setContainerFilterDel', (data: any) => data);
const setContainerFilterClear = createAction('setContainerFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const ContainerReducer = createReducer(
  {
    [setContainerGetError]: (state, data) => ({
      ...state,
      containerGetError: data,
    }),
    [setContainerGetLoading]: (state, data) => ({
      ...state,
      containerGetLoading: data,
    }),
    [setContainerGetSuccess]: (state, data) => ({
      ...state,
      containerGetSuccess: data,
    }),
    [setContainerViewError]: (state, data) => ({
      ...state,
      containerViewError: data,
    }),
    [setContainerViewLoading]: (state, data) => ({
      ...state,
      containerViewLoading: data,
    }),
    [setContainerViewSuccess]: (state, data) => ({
      ...state,
      containerViewSuccess: data,
    }),
    [setContainerAddError]: (state, data) => ({
      ...state,
      containerAddError: data,
    }),
    [setContainerAddLoading]: (state, data) => ({
      ...state,
      containerAddLoading: data,
    }),
    [setContainerAddInvalid]: (state, data) => ({
      ...state,
      containerAddInvalid: data,
    }),
    [setContainerAddSuccess]: (state, data) => ({
      ...state,
      containerAddSuccess: data,
    }),
    [setContainerDeleteError]: (state, data) => ({
      ...state,
      containerDeleteError: data,
    }),
    [setContainerDeleteLoading]: (state, data) => ({
      ...state,
      containerDeleteLoading: data,
    }),
    [setContainerDeleteSuccess]: (state, data) => ({
      ...state,
      containerDeleteSuccess: data,
    }),

    [setContainerData]: (state, data: Array<"any">) => ({
      ...state,
      containerData: data,
    }),
    [setContainerFilterAdd]: (state, data: any) => ({
      ...state,
      containerFilter: data,
    }),
    [setContainerFilterDel]: (state, id: any) => ({
      ...state,
      containerFilter: {},
    }),
    [setContainerFilterClear]: (state, data: any) => ({
      ...state,
      containerFilter: {},
    }),
  },
  DefaultState
);

export function containerFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setContainerGetSuccess(false));
    dispatch(setContainerGetError(false));
    dispatch(setContainerGetLoading(true));

    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setContainerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().container.containerData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setContainerData(data.data));
        } else {
          dispatch(setContainerData(prev.concat(data.data)));
        }
        dispatch(setContainerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setContainerGetLoading(false));
        dispatch(setContainerGetError(true));
      });
  };
}

export function containerFetchSub(supplier_id) {
  return async function (dispatch, getState) {
    dispatch(setContainerGetSuccess(false));
    dispatch(setContainerGetError(false));
    dispatch(setContainerGetLoading(true));

    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONTAINER_URL + "/" + supplier_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setContainerGetLoading(false));

        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        let container = checkNumContainer(data);
        dispatch(setContainerGetSuccess(container));
        dispatch(setContainerGetLoading(false));
      })
      .catch((err) => {
        dispatch(setContainerGetError(true));
        dispatch(setContainerGetLoading(false));
      });
  };
}

export function containerAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setContainerAddSuccess(false));
    dispatch(setContainerAddError(false));
    dispatch(setContainerAddLoading(true));

    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + CONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setContainerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((container) => {
        dispatch(setContainerAddLoading(false));
        dispatch(setShipperAddSuccess(container));
      })
      .catch((err) => {
        dispatch(setContainerAddError(true));
        dispatch(setContainerAddLoading(false));
      });
  };
}

export function containerFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setContainerViewSuccess(false));
    dispatch(setContainerViewError(false));
    dispatch(setContainerViewLoading(true));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONTAINER_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setContainerViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setContainerViewSuccess(data));
        dispatch(setContainerViewLoading(false));
      })
      .catch((err) => {
        dispatch(setContainerViewError(true));
        dispatch(setContainerViewLoading(false));
      });
  };
}

export function containerConfirm(data) {
  return async function (dispatch, getState) {
    dispatch(setContainerAddSuccess(false));
    dispatch(setContainerAddError(false));
    dispatch(setContainerAddLoading(true));

    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONTAINER_URL + "/approve", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setContainerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((container) => {
        if (container.success) {
          dispatch(setContainerAddSuccess(container));
          dispatch(setContainerAddLoading(false));
        } else {
          dispatch(setContainerAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setContainerAddError(true));
        dispatch(setContainerAddLoading(false));
      });
  };
}

export function containerUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setContainerAddSuccess(false));
    dispatch(setContainerAddError(false));
    dispatch(setContainerAddLoading(true));

    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONTAINER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setContainerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((container) => {
        if (container.status == 200) {
          dispatch(setContainerAddLoading(false));
          dispatch(setContainerAddSuccess(container));
        } else {
          dispatch(setContainerAddLoading(false));
          dispatch(setContainerAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setContainerViewLoading(false));
        dispatch(setContainerViewError(true));
      });
  };
}

export function checkNumContainer(data) {
  let allComplete = true;
  data.data.map((item, index) => {
    if (item.nomor_container == null && item.is_approve) {
      allComplete = false;
    }
  });
  data.numContainer = allComplete;
  // console.log('container==>',data)
  return data;
}

/**
 * Filter
 */
export function containerFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setContainerFilterAdd(filter));
  };
}

export function containerFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setContainerFilterDel(id));
  };
}

export function containerFilterClear() {
  return function (dispatch, getState) {
    dispatch(setContainerFilterClear());
  };
}

export default ContainerReducer;
