import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, MANIFEST_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  manifestGetLoading: false,
  manifestGetError: false,
  manifestGetSuccess: false,
  manifestViewLoading: false,
  manifestViewError: false,
  manifestViewSuccess: false,

  manifestData: [],
  manifestFilter: {},
};

/**
 * ACTION
 */

const setManifestGetLoading = createAction('setManifestGetLoading', (data: any) => data);
const setManifestGetError = createAction('setManifestGetError', (data: any) => data);
const setManifestGetSuccess = createAction('setManifestGetSuccess', (data: any) => data);
const setManifestViewLoading = createAction('setManifestViewLoading', (data: any) => data);
const setManifestViewError = createAction('setManifestViewError', (data: any) => data);
const setManifestViewSuccess = createAction('setManifestViewSuccess', (data: any) => data);

const setManifestData = createAction('setManifestData', (data: any) => data);
const setManifestFilterAdd = createAction('setManifestFilterAdd', (data: any) => data);
const setManifestFilterDel = createAction('setManifestFilterDel', (data: any) => data);
const setManifestFilterClear = createAction('setManifestFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const manifestReducer = createReducer(
  {
    [setManifestGetLoading]: (state, data) => ({
      ...state,
      manifestGetLoading: data,
    }),
    [setManifestGetError]: (state, data) => ({
      ...state,
      manifestGetError: data,
    }),
    [setManifestGetSuccess]: (state, data) => ({
      ...state,
      manifestGetSuccess: data,
    }),
    [setManifestViewLoading]: (state, data) => ({
      ...state,
      manifestViewLoading: data,
    }),
    [setManifestViewError]: (state, data) => ({
      ...state,
      manifestViewError: data,
    }),
    [setManifestViewSuccess]: (state, data) => ({
      ...state,
      manifestViewSuccess: data,
    }),

    [setManifestData]: (state, data: Array<"any">) => ({
      ...state,
      manifestData: data,
    }),
    [setManifestFilterAdd]: (state, data: any) => ({
      ...state,
      manifestFilter: data,
    }),
    [setManifestFilterDel]: (state, id: any) => ({
      ...state,
      manifestFilter: {},
    }),
    [setManifestFilterClear]: (state, data: any) => ({
      ...state,
      manifestFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function manifestFetchAll(data) {
  return async function (dispatch, getState) {
    dispatch(setManifestGetLoading(true));
    dispatch(setManifestGetError(false));
    dispatch(setManifestGetSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + MANIFEST_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setManifestGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().manifest.manifestData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setManifestData(data.data));
        } else {
          dispatch(setManifestData(prev.concat(data.data)));
        }
        dispatch(setManifestGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setManifestGetError(true));
        dispatch(setManifestGetLoading(false));
      });
  };
}

export function manifestFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setManifestViewLoading(true));
    dispatch(setManifestViewError(false));
    dispatch(setManifestViewSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + MANIFEST_URL + "/view", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setManifestViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setManifestViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setManifestViewError(true));
        dispatch(setManifestViewLoading(false));
      });
  };
}

/**
 * Filter
 */
export function manifestFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setManifestFilterAdd(filter));
  };
}

export function manifestFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setManifestFilterDel(id));
  };
}

export function manifestFilterClear() {
  return function (dispatch, getState) {
    dispatch(setManifestFilterClear());
  };
}
export default manifestReducer;
