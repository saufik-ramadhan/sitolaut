import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { BASE_URL, KONTRAK_URL, TRAYEK_URL } from "../config/constants";
import AsyncStorage from "../AsyncStorage";

const DefaultState = {
  kontrakGetModal: false || {},
  kontrakGetSuccess: false || {},
  listTrayekGetSuccess: false || {},
  addKontrakGetSuccess: false || {},
  kontrakEdit: false || {},
  kontrakDelete: false || {},
  adendum: false || {},
  adendumGetSuccess: false || {},
  loading: false,
};

// CREATE ACTION
const setKontrakGetModal = createAction('setKontrakGetModal', (data) => data);
const setKontrakGetSuccess = createAction('setKontrakGetSuccess', (data) => data);
const setListTrayekGetSuccess = createAction('setListTrayekGetSuccess', (data) => data);
const setAddKontrakGetSuccess = createAction('setAddKontrakGetSuccess', (data) => data);
const setKontrakEdit = createAction('setKontrakEdit', (data) => data);
const setKontrakDelete = createAction('setKontrakDelete', (data) => data);
const setAdendum = createAction('setAdendum', (data) => data);
const setAdendumGetSuccess = createAction('setAdendumGetSuccess', (data) => data);
const setLoading = createAction('setLoading', (data) => data);

// CREATE REDUCER
const masterkontrakReducer = createReducer(
  {
    [setKontrakGetModal]: (state, data) => ({
      ...state,
      kontrakGetModal: data,
    }),
    [setKontrakGetSuccess]: (state, data) => ({
      ...state,
      kontrakGetSuccess: data,
    }),
    [setListTrayekGetSuccess]: (state, data) => ({
      ...state,
      listTrayekGetSuccess: data,
    }),
    [setAddKontrakGetSuccess]: (state, data) => ({
      ...state,
      addKontrakGetSuccess: data,
    }),
    [setKontrakEdit]: (state, data) => ({
      ...state,
      kontrakEdit: data,
    }),
    [setKontrakDelete]: (state, data) => ({
      ...state,
      kontrakDelete: data,
    }),
    [setAdendum]: (state, data) => ({
      ...state,
      adendum: data,
    }),
    [setAdendumGetSuccess]: (state, data) => ({
      ...state,
      adendumGetSuccess: data,
    }),
    [setLoading]: (state, data) => ({
      ...state,
      loading: data,
    }),
  },
  DefaultState
);

export function addendumFetchAll(id) {
  return async (dispatch) => {
    dispatch(setAdendumGetSuccess(false));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + `${KONTRAK_URL}/addendum/${id}`, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setAdendumGetSuccess(data));
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setAdendumGetSuccess(false));
      });
  };
}

export function masterKontrakFetchAll() {
  return async (dispatch) => {
    dispatch(setKontrakGetSuccess(false));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + KONTRAK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setKontrakGetSuccess(data));
      })
      .catch((err) => {
        console.log(err, "err cuk");
      });
  };
}

export const DeleteMasterKontrakData = (props) => {
  return async (dispatch) => {
    dispatch(setAddKontrakGetSuccess(false));
    dispatch(setLoading(true));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + KONTRAK_URL + `/${props.id}`, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setAddKontrakGetSuccess(data));
        if (data.status === 200) {
          // dispatch(setKontrakFetchAll())
          // dispatch(setAddendumFetchAll())
          dispatch(setLoading(false));
        }
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setLoading(false));
      });
  };
};

export const EditMasterKontrakData = (props) => {
  return async (dispatch) => {
    dispatch(setAddKontrakGetSuccess(false));
    dispatch(setLoading(true));
    dispatch;
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(props),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + KONTRAK_URL + `/${props.id}`, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setAddKontrakGetSuccess(data));
        if (data.status === 200) {
          // dispatch(setKontrakFetchAll())
          dispatch(setLoading(false));
        }
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setLoading(false));
      });
  };
};

export const addMasterKontrakData = (props) => {
  return async (dispatch) => {
    dispatch(setAddKontrakGetSuccess(false));
    dispatch(setLoading(true));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(props),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + KONTRAK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setAddKontrakGetSuccess(data));
        if (data.status === 200) {
          // dispatch(setKontrakFetchAll())
          // dispatch(setAddendumFetchAll())
          dispatch(setLoading(false));
        }
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setLoading(false));
      });
  };
};

export const kodeTrayekFetchAll = () => {
  return async (dispatch) => {
    dispatch(setListTrayekGetSuccess(false));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRAYEK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setListTrayekGetSuccess(data));
      })
      .catch((err) => {
        console.log(err, "err cuk");
      });
  };
};

export default masterkontrakReducer;
