import { doLogout } from "./authReducers";
import { BASE_URL } from "../config/constants";
import { createAction, createReducer } from "redux-act";
import AsyncStorage from "../AsyncStorage";

const DefaultState = {
  guestGetSuccess: false || {},
  loading: false,
};

// CREATE ACTION
const setGuestGetSuccess = createAction('setGuestGetSuccess', (data: any) => data);
const setLoading = createAction('setLoading', (data: any) => data);

// CREATE REDUCER
const guestReducer = createReducer(
  {
    [setGuestGetSuccess]: (state, data) => ({
      ...state,
      guestGetSuccess: data,
    }),
    [setLoading]: (state, data) => ({
      ...state,
      loading: data,
    }),
  },
  DefaultState
);

export const guestFetchAll = () => {
  return async (dispatch) => {
    dispatch(setGuestGetSuccess(false));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "guest", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setGuestGetSuccess(data));
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setGuestGetSuccess(false));
      });
  };
};

export const changePassword = (data) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "user/reset_password", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setLoading(false));
      });
  };
};

export const addGuest = (data) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "register", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setLoading(false));
      });
  };
};

export const deleteGuest = (data) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "user", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err, "err cuk");
        dispatch(setLoading(false));
      });
  };
};

export default guestReducer;
