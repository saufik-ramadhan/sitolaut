import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import { BASE_URL, RESELLER_URL } from "./../config/constants";
import FetchRetry from "../services/FetchRetry";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  resellerGetError: false,
  resellerGetLoading: false,
  resellerGetSuccess: false,
  resellerViewError: false,
  resellerViewLoading: false,
  resellerViewSuccess: false,
  resellerAddError: false,
  resellerAddLoading: false,
  resellerAddInvalid: false,
  resellerAddSuccess: false,
  resellerDeleteError: false,
  resellerDeleteLoading: false,
  resellerDeleteSuccess: false,

  resellerData: [],
  resellerFilter: {},
};

/**
 * ACTION
 */
const setResellerGetError = createAction('setResellerGetError', (data: any) => data);
const setResellerGetLoading = createAction('setResellerGetLoading', (data: any) => data);
const setResellerGetSuccess = createAction('setResellerGetSuccess', (data: any) => data);
const setResellerViewError = createAction('setResellerViewError', (data: any) => data);
const setResellerViewLoading = createAction('setResellerViewLoading', (data: any) => data);
const setResellerViewSuccess = createAction('setResellerViewSuccess', (data: any) => data);
const setResellerAddError = createAction('setResellerAddError', (data: any) => data);
const setResellerAddLoading = createAction('setResellerAddLoading', (data: any) => data);
const setResellerAddInvalid = createAction('setResellerAddInvalid', (data: any) => data);
const setResellerAddSuccess = createAction('setResellerAddSuccess', (data: any) => data);
const setResellerDeleteError = createAction('setResellerDeleteError', (data: any) => data);
const setResellerDeleteLoading = createAction('setResellerDeleteLoading', (data: any) => data);
const setResellerDeleteSuccess = createAction('setResellerDeleteSuccess', (data: any) => data);

const setResellerData = createAction('setResellerData', (data: any) => data);
const setResellerFilterAdd = createAction('setResellerFilterAdd', (data: any) => data);
const setResellerFilterDel = createAction('setResellerFilterDel', (data: any) => data);
const setResellerFilterClear = createAction('setResellerFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const resellerReducer = createReducer(
  {
    [setResellerGetError]: (state, data) => ({
      ...state,
      resellerGetError: data,
    }),
    [setResellerGetLoading]: (state, data) => ({
      ...state,
      resellerGetLoading: data,
    }),
    [setResellerGetSuccess]: (state, data) => ({
      ...state,
      resellerGetSuccess: data,
    }),
    [setResellerViewError]: (state, data) => ({
      ...state,
      resellerViewError: data,
    }),
    [setResellerViewLoading]: (state, data) => ({
      ...state,
      resellerViewLoading: data,
    }),
    [setResellerViewSuccess]: (state, data) => ({
      ...state,
      resellerViewSuccess: data,
    }),
    [setResellerAddError]: (state, data) => ({
      ...state,
      resellerAddError: data,
    }),
    [setResellerAddLoading]: (state, data) => ({
      ...state,
      resellerAddLoading: data,
    }),
    [setResellerAddInvalid]: (state, data) => ({
      ...state,
      resellerAddInvalid: data,
    }),
    [setResellerAddSuccess]: (state, data) => ({
      ...state,
      resellerAddSuccess: data,
    }),
    [setResellerDeleteError]: (state, data) => ({
      ...state,
      resellerDeleteError: data,
    }),
    [setResellerDeleteLoading]: (state, data) => ({
      ...state,
      resellerDeleteLoading: data,
    }),
    [setResellerDeleteSuccess]: (state, data) => ({
      ...state,
      resellerDeleteSuccess: data,
    }),

    [setResellerData]: (state, data: Array<"any">) => ({
      ...state,
      resellerData: data,
    }),
    [setResellerFilterAdd]: (state, data: any) => ({
      ...state,
      resellerFilter: data,
    }),
    [setResellerFilterDel]: (state, id: any) => ({
      ...state,
      resellerFilter: {},
    }),
    [setResellerFilterClear]: (state, data: any) => ({
      ...state,
      resellerFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function resellerFetchUser(id) {
  return async function (dispatch, getState) {
    dispatch(setResellerViewError(false));
    dispatch(setResellerViewSuccess(false));
    dispatch(setResellerViewLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_URL + "/profile", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setResellerViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setResellerViewError(true));
        dispatch(setResellerViewLoading(false));
      });
  };
}

export function resellerFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setResellerGetSuccess(false));
    dispatch(setResellerGetError(false));
    dispatch(setResellerGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().reseller.resellerData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setResellerData(data.data));
        } else {
          dispatch(setResellerData(prev.concat(data.data)));
        }
        dispatch(setResellerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setResellerGetError(true));
        dispatch(setResellerGetLoading(false));
      });
  };
}

export function resellerFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setResellerGetSuccess(false));
    dispatch(setResellerGetError(false));
    dispatch(setResellerGetLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().reseller.resellerData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setResellerData(data.data));
        } else {
          dispatch(setResellerData(prev.concat(data.data)));
        }
        dispatch(setResellerGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setResellerGetError(true));
        dispatch(setResellerGetLoading(false));
      });
  };
}

export function handleStatus(data) {
  return async function (dispatch, getState) {
    dispatch(setResellerAddError(false));
    dispatch(setResellerAddSuccess(false));
    dispatch(setResellerAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "register/" + data.status, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((reseller) => {
        dispatch(setResellerAddSuccess(reseller));
      })
      .catch((err) => {
        dispatch(setResellerAddError(true));
        dispatch(setResellerAddLoading(false));
      });
  };
}

export function resellerFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setResellerViewError(false));
    dispatch(setResellerViewSuccess(false));
    dispatch(setResellerViewLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setResellerViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setResellerViewError(true));
        dispatch(setResellerViewLoading(false));
      });
  };
}

export function resellerDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setResellerViewSuccess(false));
    dispatch(setResellerDeleteError(false));
    dispatch(setResellerDeleteSuccess(false));
    dispatch(setResellerDeleteLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setResellerDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setResellerDeleteError(true));
        dispatch(setResellerDeleteLoading(false));
      });
  };
}

export function resellerAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setResellerAddError(false));
    dispatch(setResellerAddSuccess(false));
    dispatch(setResellerAddInvalid(false));
    dispatch(setResellerAddLoading(true));

    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      body: formData,
    };
    fetch(BASE_URL + RESELLER_URL + "/new", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((reseller) => {
        if (reseller.success) {
          dispatch(setResellerAddSuccess(reseller));
        } else {
          dispatch(setResellerAddInvalid(reseller));
        }
      })
      .catch((err) => {
        dispatch(setResellerAddError(true));
        dispatch(setResellerAddLoading(false));
        alert("Error");
      });
  };
}

export function resellerUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setResellerAddError(false));
    dispatch(setResellerAddSuccess(false));
    dispatch(setResellerAddLoading(true));

    const token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((reseller) => {
        dispatch(setResellerAddSuccess(reseller));
      })
      .catch((err) => {
        dispatch(setResellerAddError(true));
        dispatch(setResellerAddLoading(false));
      });
  };
}

/**
 * Filter
 */
export function resellerFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setResellerFilterAdd(filter));
  };
}

export function resellerFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setResellerFilterDel(id));
  };
}

export function resellerFilterClear() {
  return function (dispatch, getState) {
    dispatch(setResellerFilterClear());
  };
}

export default resellerReducer;
export { setResellerAddSuccess };
