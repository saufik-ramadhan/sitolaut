import { createAction, createReducer } from "redux-act";
import { BASE_URL, KOTA_URL } from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  kotaGetError: false,
  kotaGetLoading: false,
  kotaGetSuccess: false,
  kotaViewError: false,
  kotaViewLoading: false,
  kotaViewSuccess: false,
  kotaAddError: false,
  kotaAddLoading: false,
  kotaAddSuccess: false,

  kotaData: [],
  kotaFilter: {},
};

/**
 * ACTION
 */
const setKotaGetError = createAction('setKotaGetError', (data: any) => data);
const setKotaGetLoading = createAction('setKotaGetLoading', (data: any) => data);
const setKotaGetSuccess = createAction('setKotaGetSuccess', (data: any) => data);
const setKotaViewError = createAction('setKotaViewError', (data: any) => data);
const setKotaViewLoading = createAction('setKotaViewLoading', (data: any) => data);
const setKotaViewSuccess = createAction('setKotaViewSuccess', (data: any) => data);
const setKotaAddError = createAction('setKotaAddError', (data: any) => data);
const setKotaAddLoading = createAction('setKotaAddLoading', (data: any) => data);
const setKotaAddSuccess = createAction('setKotaAddSuccess', (data: any) => data);

const setKotaData = createAction('setKotaData', (data: any) => data);
const setKotaFilterAdd = createAction('setKotaFilterAdd', (data: any) => data);
const setKotaFilterDel = createAction('setKotaFilterDel', (data: any) => data);
const setKotaFilterClear = createAction('setKotaFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const kotaReducer = createReducer(
  {
    [setKotaGetError]: (state, data) => ({ ...state, kotaGetError: data }),
    [setKotaGetLoading]: (state, data) => ({ ...state, kotaGetLoading: data }),
    [setKotaGetSuccess]: (state, data) => ({ ...state, kotaGetSuccess: data }),
    [setKotaViewError]: (state, data) => ({ ...state, kotaViewError: data }),
    [setKotaViewLoading]: (state, data) => ({
      ...state,
      kotaViewLoading: data,
    }),
    [setKotaViewSuccess]: (state, data) => ({
      ...state,
      kotaViewSuccess: data,
    }),
    [setKotaAddError]: (state, data) => ({ ...state, kotaAddError: data }),
    [setKotaAddLoading]: (state, data) => ({ ...state, kotaAddLoading: data }),
    [setKotaAddSuccess]: (state, data) => ({ ...state, kotaAddSuccess: data }),

    [setKotaData]: (state, data: Array<"any">) => ({
      ...state,
      kotaData: data,
    }),
    [setKotaFilterAdd]: (state, data: any) => ({
      ...state,
      kotaFilter: data,
    }),
    [setKotaFilterDel]: (state, id: any) => ({
      ...state,
      kotaFilter: {},
    }),
    [setKotaFilterClear]: (state, data: any) => ({
      ...state,
      kotaFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function kotaFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setKotaGetSuccess(false));
    dispatch(setKotaGetError(false));
    dispatch(setKotaGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + KOTA_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setKotaGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().kota.kotaData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setKotaData(data.data));
        } else {
          dispatch(setKotaData(prev.concat(data.data)));
        }
        dispatch(setKotaGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setKotaGetError(true));
      });
  };
}

export function kotaFetchSub(dataInput) {
  return async function (dispatch, getState) {
    dispatch(setKotaGetSuccess(false));
    dispatch(setKotaGetError(false));
    dispatch(setKotaGetLoading(true));

    let config = {};
    config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch(BASE_URL + KOTA_URL + "/" + dataInput, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setKotaGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          const currentPage = data.currentPage || 1;
          const start = data.start || 0;
          const prev = getState().kota.kotaData || [];
          if (start == 0 && currentPage == 1) {
            dispatch(setKotaData(data.data));
          } else {
            dispatch(setKotaData(prev.concat(data.data)));
          }
          dispatch(setKotaGetSuccess(data));
        }
      })
      .catch((err) => {
        dispatch(setKotaGetError(true));
      });
  };
}

export function kotaFetchByProvinsi(provinsi_id: number) {
  return async function (dispatch, getState) {
    dispatch(setKotaGetSuccess(false));
    dispatch(setKotaGetError(false));
    dispatch(setKotaGetLoading(true));
    let config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch(`${BASE_URL}kota/get_propinsi/${provinsi_id}`, config)
      .then((response) => {
        dispatch(setKotaGetLoading(false));
        return response.json();
      })
      .then((result) => {
        result.success
          ? dispatch(setKotaGetSuccess(result))
          : console.log("Kota tidak ditemukan");
      })
      .catch(() => {
        dispatch(setKotaGetLoading(false));
        dispatch(setKotaGetSuccess(false));
      });
  };
}

export function kotaAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setKotaAddError(false));
    dispatch(setKotaAddSuccess(false));
    dispatch(setKotaAddLoading(true));
    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + KOTA_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setKotaAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((kota) => {
        dispatch(setKotaAddSuccess(kota));
      })
      .catch((err) => {
        dispatch(setKotaAddError(true));
      });
  };
}

export function kotaFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setKotaViewError(false));
    dispatch(setKotaViewSuccess(false));
    dispatch(setKotaViewLoading(true));
    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: id,
      }),
    };
    fetch(BASE_URL + KOTA_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setKotaViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setKotaViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setKotaViewError(true));
      });
  };
}

/**
 * Filter
 */
export function kotaFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setKotaFilterAdd(filter));
  };
}

export function kotaFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setKotaFilterDel(id));
  };
}

export function kotaFilterClear() {
  return function (dispatch, getState) {
    dispatch(setKotaFilterClear());
  };
}

export default kotaReducer;
