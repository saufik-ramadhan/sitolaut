import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";
import AsyncStorage from "@react-native-community/async-storage";
import { persistStore, persistReducer, PersistConfig } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";

/**
 * Import Root Reducer
 */
import rootReducer from "./rootReducer";

/**
 * Middleware: Redux Persist Config
 * Konfigurasi key, storage dan whiteliset dari redux persist
 */
const persistConfig: PersistConfig<"HSS"> = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["auth", "user", "dashboard", "provinsi", "jeniscommodity"],
};

/**
 * Middleware: Redux Persist Persisted Reducer
 */
const persistedReducer = persistReducer(persistConfig, rootReducer);

/**
 * Redux: Store
 */
const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

/**
 * Middleware: Redux Persist Persistor
 * Wrapper
 */
let persistor = persistStore(store);

export { store, persistor };
