import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, BIAYAPENGURUSAN_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  biayapengurusanGetError: false,
  biayapengurusanGetLoading: false,
  biayapengurusanGetSuccess: false,
  biayapengurusanViewError: false,
  biayapengurusanViewLoading: false,
  biayapengurusanViewSuccess: false,
  biayapengurusanAddError: false,
  biayapengurusanAddLoading: false,
  biayapengurusanAddSuccess: false,
  biayapengurusanDeleteError: false,
  biayapengurusanDeleteLoading: false,
  biayapengurusanDeleteSuccess: false,

  biayapengurusanData: [],
  biayapengurusanFilter: {},
};

/**
 * ACTION
 */
const setBiayapengurusanGetError = createAction('setBiayapengurusanGetError', (data: any) => data);
const setBiayapengurusanGetLoading = createAction('setBiayapengurusanGetLoading', (data: any) => data);
const setBiayapengurusanGetSuccess = createAction('setBiayapengurusanGetSuccess', (data: any) => data);
const setBiayapengurusanViewError = createAction('setBiayapengurusanViewError', (data: any) => data);
const setBiayapengurusanViewLoading = createAction('setBiayapengurusanViewLoading', (data: any) => data);
const setBiayapengurusanViewSuccess = createAction('setBiayapengurusanViewSuccess', (data: any) => data);
const setBiayapengurusanAddError = createAction('setBiayapengurusanAddError', (data: any) => data);
const setBiayapengurusanAddLoading = createAction('setBiayapengurusanAddLoading', (data: any) => data);
const setBiayapengurusanAddSuccess = createAction('setBiayapengurusanAddSuccess', (data: any) => data);
const setBiayapengurusanDeleteError = createAction('setBiayapengurusanDeleteError', (data: any) => data);
const setBiayapengurusanDeleteLoading = createAction('setBiayapengurusanDeleteLoading', (data: any) => data);
const setBiayapengurusanDeleteSuccess = createAction('setBiayapengurusanDeleteSuccess', (data: any) => data);

const setBiayapengurusanData = createAction('setBiayapengurusanData', (data: any) => data);
const setBiayapengurusanFilterAdd = createAction('setBiayapengurusanFilterAdd', (data: any) => data);
const setBiayapengurusanFilterDel = createAction('setBiayapengurusanFilterDel', (data: any) => data);
const setBiayapengurusanFilterClear = createAction('setBiayapengurusanFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const biayapengurusanReducer = createReducer(
  {
    [setBiayapengurusanGetError]: (state, data) => ({
      ...state,
      biayapengurusanGetError: data,
    }),
    [setBiayapengurusanGetLoading]: (state, data) => ({
      ...state,
      biayapengurusanGetLoading: data,
    }),
    [setBiayapengurusanGetSuccess]: (state, data) => ({
      ...state,
      biayapengurusanGetSuccess: data,
    }),
    [setBiayapengurusanViewError]: (state, data) => ({
      ...state,
      biayapengurusanViewError: data,
    }),
    [setBiayapengurusanViewLoading]: (state, data) => ({
      ...state,
      biayapengurusanViewLoading: data,
    }),
    [setBiayapengurusanViewSuccess]: (state, data) => ({
      ...state,
      biayapengurusanViewSuccess: data,
    }),
    [setBiayapengurusanAddError]: (state, data) => ({
      ...state,
      biayapengurusanAddError: data,
    }),
    [setBiayapengurusanAddLoading]: (state, data) => ({
      ...state,
      biayapengurusanAddLoading: data,
    }),
    [setBiayapengurusanAddSuccess]: (state, data) => ({
      ...state,
      biayapengurusanAddSuccess: data,
    }),
    [setBiayapengurusanDeleteError]: (state, data) => ({
      ...state,
      biayapengurusanDeleteError: data,
    }),
    [setBiayapengurusanDeleteLoading]: (state, data) => ({
      ...state,
      biayapengurusanDeleteLoading: data,
    }),
    [setBiayapengurusanDeleteSuccess]: (state, data) => ({
      ...state,
      biayapengurusanDeleteSuccess: data,
    }),

    [setBiayapengurusanData]: (state, data: Array<"any">) => ({
      ...state,
      biayapengurusanData: data,
    }),
    [setBiayapengurusanFilterAdd]: (state, data: any) => ({
      ...state,
      biayapengurusanFilter: data,
    }),
    [setBiayapengurusanFilterDel]: (state, id: any) => ({
      ...state,
      biayapengurusanFilter: {},
    }),
    [setBiayapengurusanFilterClear]: (state, data: any) => ({
      ...state,
      biayapengurusanFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function biayapengurusanFetchAll(tokenAuth) {
  return async function (dispatch, getState) {
    dispatch(setBiayapengurusanAddSuccess(false));
    dispatch(setBiayapengurusanGetSuccess(false));
    dispatch(setBiayapengurusanGetError(false));
    dispatch(setBiayapengurusanGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBiayapengurusanGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().biayapengurusan.biayapengurusanData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setBiayapengurusanData(data.data));
        } else {
          dispatch(setBiayapengurusanData(prev.concat(data.data)));
        }
        dispatch(setBiayapengurusanGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setBiayapengurusanGetError(true));
        dispatch(setBiayapengurusanGetLoading(false));
      });
  };
}

export function biayapengurusanFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setBiayapengurusanAddSuccess(false));
    dispatch(setBiayapengurusanGetSuccess(false));
    dispatch(setBiayapengurusanGetError(false));
    dispatch(setBiayapengurusanGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BIAYAPENGURUSAN_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBiayapengurusanGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().biayapengurusan.biayapengurusanData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setBiayapengurusanData(data.data));
        } else {
          dispatch(setBiayapengurusanData(prev.concat(data.data)));
        }
        dispatch(setBiayapengurusanGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setBiayapengurusanGetError(true));
        dispatch(setBiayapengurusanGetLoading(false));
      });
  };
}

export function biayapengurusanFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setBiayapengurusanAddSuccess(false));
    dispatch(setBiayapengurusanViewSuccess(false));
    dispatch(setBiayapengurusanGetSuccess(false));
    dispatch(setBiayapengurusanGetError(false));
    dispatch(setBiayapengurusanGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BIAYAPENGURUSAN_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBiayapengurusanGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().biayapengurusan.biayapengurusanData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setBiayapengurusanData(data.data));
        } else {
          dispatch(setBiayapengurusanData(prev.concat(data.data)));
        }
        dispatch(setBiayapengurusanGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setBiayapengurusanGetError(true));
        dispatch(setBiayapengurusanGetLoading(false));
      });
  };
}

export function biayapengurusanAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setBiayapengurusanAddError(false));
    dispatch(setBiayapengurusanViewSuccess(false));
    dispatch(setBiayapengurusanAddSuccess(false));
    dispatch(setBiayapengurusanAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBiayapengurusanAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((biayapengurusan) => {
        dispatch(setBiayapengurusanAddSuccess(biayapengurusan));
      })
      .catch((err) => {
        dispatch(setBiayapengurusanAddError(true));
        dispatch(setBiayapengurusanAddLoading(false));
      });
  };
}

export function biayapengurusanFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setBiayapengurusanViewError(false));
    dispatch(setBiayapengurusanViewSuccess(false));
    dispatch(setBiayapengurusanViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BIAYAPENGURUSAN_URL + "/view", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBiayapengurusanViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setBiayapengurusanViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setBiayapengurusanViewError(true));
        dispatch(setBiayapengurusanViewLoading(false));
      });
  };
}

export function biayapengurusanUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setBiayapengurusanViewSuccess(false));
    dispatch(setBiayapengurusanAddError(false));
    dispatch(setBiayapengurusanAddSuccess(false));
    dispatch(setBiayapengurusanAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBiayapengurusanAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((biayapengurusan) => {
        if (biayapengurusan.success) {
          dispatch(setBiayapengurusanAddSuccess(biayapengurusan));
        } else {
          dispatch(setBiayapengurusanAddError(true));
          dispatch(setBiayapengurusanAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setBiayapengurusanAddError(true));
        dispatch(setBiayapengurusanAddLoading(false));
      });
  };
}

export function biayapengurusanDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setBiayapengurusanViewSuccess(false));
    dispatch(setBiayapengurusanDeleteError(false));
    dispatch(setBiayapengurusanDeleteSuccess(false));
    dispatch(setBiayapengurusanDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setBiayapengurusanDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setBiayapengurusanDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setBiayapengurusanDeleteError(true));
        dispatch(setBiayapengurusanDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function biayapengurusanFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setBiayapengurusanFilterAdd(filter));
  };
}

export function biayapengurusanFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setBiayapengurusanFilterDel(id));
  };
}

export function biayapengurusanFilterClear() {
  return function (dispatch, getState) {
    dispatch(setBiayapengurusanFilterClear());
  };
}

export default biayapengurusanReducer;
