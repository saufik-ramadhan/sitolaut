import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, VESSEL_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  vesselGetError: false,
  vesselGetLoading: false,
  vesselGetSuccess: false,
  vesselViewError: false,
  vesselViewLoading: false,
  vesselViewSuccess: false,
  vesselAddError: false,
  vesselAddLoading: false,
  vesselAddInvalid: false,
  vesselAddSuccess: false,
  vesselDeleteError: false,
  vesselDeleteLoading: false,
  vesselDeleteSuccess: false,

  vesselData: [],
  vesselFilter: {},
};

/**
 * ACTION
 */
const setVesselGetError = createAction('setVesselGetError', (data: any) => data);
const setVesselGetLoading = createAction('setVesselGetLoading', (data: any) => data);
const setVesselGetSuccess = createAction('setVesselGetSuccess', (data: any) => data);
const setVesselViewError = createAction('setVesselViewError', (data: any) => data);
const setVesselViewLoading = createAction('setVesselViewLoading', (data: any) => data);
const setVesselViewSuccess = createAction('setVesselViewSuccess', (data: any) => data);
const setVesselAddError = createAction('setVesselAddError', (data: any) => data);
const setVesselAddLoading = createAction('setVesselAddLoading', (data: any) => data);
const setVesselAddInvalid = createAction('setVesselAddInvalid', (data: any) => data);
const setVesselAddSuccess = createAction('setVesselAddSuccess', (data: any) => data);
const setVesselDeleteError = createAction('setVesselDeleteError', (data: any) => data);
const setVesselDeleteLoading = createAction('setVesselDeleteLoading', (data: any) => data);
const setVesselDeleteSuccess = createAction('setVesselDeleteSuccess', (data: any) => data);

const setVesselData = createAction('setVesselData', (data: any) => data);
const setVesselFilterAdd = createAction('setVesselFilterAdd', (data: any) => data);
const setVesselFilterDel = createAction('setVesselFilterDel', (data: any) => data);
const setVesselFilterClear = createAction('setVesselFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const vesselReducer = createReducer(
  {
    [setVesselGetError]: (state, data) => ({ ...state, vesselGetError: data }),
    [setVesselGetLoading]: (state, data) => ({
      ...state,
      vesselGetLoading: data,
    }),
    [setVesselGetSuccess]: (state, data) => ({
      ...state,
      vesselGetSuccess: data,
    }),
    [setVesselViewError]: (state, data) => ({
      ...state,
      vesselViewError: data,
    }),
    [setVesselViewLoading]: (state, data) => ({
      ...state,
      vesselViewLoading: data,
    }),
    [setVesselViewSuccess]: (state, data) => ({
      ...state,
      vesselViewSuccess: data,
    }),
    [setVesselAddError]: (state, data) => ({ ...state, vesselAddError: data }),
    [setVesselAddLoading]: (state, data) => ({
      ...state,
      vesselAddLoading: data,
    }),
    [setVesselAddInvalid]: (state, data) => ({
      ...state,
      vesselAddInvalid: data,
    }),
    [setVesselAddSuccess]: (state, data) => ({
      ...state,
      vesselAddSuccess: data,
    }),
    [setVesselDeleteError]: (state, data) => ({
      ...state,
      vesselDeleteError: data,
    }),
    [setVesselDeleteLoading]: (state, data) => ({
      ...state,
      vesselDeleteLoading: data,
    }),
    [setVesselDeleteSuccess]: (state, data) => ({
      ...state,
      vesselDeleteSuccess: data,
    }),

    [setVesselData]: (state, data: Array<"any">) => ({
      ...state,
      vesselData: data,
    }),
    [setVesselFilterAdd]: (state, data: any) => ({
      ...state,
      vesselFilter: data,
    }),
    [setVesselFilterDel]: (state, id: any) => ({
      ...state,
      vesselFilter: {},
    }),
    [setVesselFilterClear]: (state, data: any) => ({
      ...state,
      vesselFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function vesselFetchAll(item) {
  return async function (dispatch, getState) {
    dispatch(setVesselAddError(false));
    dispatch(setVesselAddSuccess(false));
    dispatch(setVesselGetSuccess(false));
    dispatch(setVesselGetError(false));
    dispatch(setVesselGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().vessel.vesselData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setVesselData(data.data));
        } else {
          dispatch(setVesselData(prev.concat(data.data)));
        }
        dispatch(setVesselGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setVesselGetError(true));
        dispatch(setVesselGetLoading(false));
      });
  };
}

export function vesselFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setVesselAddError(false));
    dispatch(setVesselAddSuccess(false));
    dispatch(setVesselGetSuccess(false));
    dispatch(setVesselGetError(false));
    dispatch(setVesselGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_URL + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().vessel.vesselData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setVesselData(data.data));
        } else {
          dispatch(setVesselData(prev.concat(data.data)));
        }
        dispatch(setVesselGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setVesselGetError(true));
        dispatch(setVesselGetLoading(false));
      });
  };
}

export function vesselFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setVesselAddError(false));
    dispatch(setVesselAddSuccess(false));
    dispatch(setVesselGetSuccess(false));
    dispatch(setVesselGetError(false));
    dispatch(setVesselGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().vessel.vesselData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setVesselData(data.data));
        } else {
          dispatch(setVesselData(prev.concat(data.data)));
        }
        dispatch(setVesselGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setVesselGetError(true));
        dispatch(setVesselGetLoading(false));
      });
  };
}

export function vesselFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setVesselAddError(false));
    dispatch(setVesselAddSuccess(false));
    dispatch(setVesselViewSuccess(false));
    dispatch(setVesselViewError(false));
    dispatch(setVesselViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setVesselViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setVesselViewError(true));
        dispatch(setVesselViewLoading(false));
      });
  };
}

export function vesselAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setVesselAddError(false));
    dispatch(setVesselAddSuccess(false));
    dispatch(setVesselAddLoading(true));
    dispatch(setVesselAddInvalid(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + VESSEL_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          alert(data.message);
          dispatch(setVesselAddSuccess(true));
        } else {
          dispatch(setVesselAddInvalid(data));
        }
      })
      .catch((err) => {
        alert(data.message);
        dispatch(setVesselAddError(true));
        dispatch(setVesselAddLoading(false));
      });
  };
}

export function vesselUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setVesselAddError(false));
    dispatch(setVesselAddSuccess(false));
    dispatch(setVesselAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          dispatch(setVesselAddSuccess(true));
          alert('Berhasil')
        } else {
          dispatch(setVesselAddError(true));
          dispatch(setVesselAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setVesselAddError(true));
        dispatch(setVesselAddLoading(false));
      });
  };
}

export function vesselDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setVesselDeleteError(false));
    dispatch(setVesselDeleteSuccess(false));
    dispatch(setVesselDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          dispatch(setVesselDeleteSuccess(true));
        }
      })
      .catch((err) => {
        dispatch(setVesselDeleteError(true));
        dispatch(setVesselDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function vesselFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setVesselFilterAdd(filter));
  };
}

export function vesselFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setVesselFilterDel(id));
  };
}

export function vesselFilterClear() {
  return function (dispatch, getState) {
    dispatch(setVesselFilterClear());
  };
}

export default vesselReducer;
