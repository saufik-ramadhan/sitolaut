import { createAction, createReducer } from "redux-act";
import { BASE_URL, PROVINSI_URL } from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  provinsiGetError: false,
  provinsiGetLoading: false,
  provinsiGetSuccess: false,
  provinsiViewError: false,
  provinsiViewLoading: false,
  provinsiViewSuccess: false,
  provinsiAddError: false,
  provinsiAddLoading: false,
  provinsiAddSuccess: false,
};

/**
 * ACTION
 */
const setProvinsiGetError = createAction('setProvinsiGetError', (data: any) => data);
const setProvinsiGetLoading = createAction('setProvinsiGetLoading', (data: any) => data);
const setProvinsiGetSuccess = createAction('setProvinsiGetSuccess', (data: any) => data);
const setProvinsiViewError = createAction('setProvinsiViewError', (data: any) => data);
const setProvinsiViewLoading = createAction('setProvinsiViewLoading', (data: any) => data);
const setProvinsiViewSuccess = createAction('setProvinsiViewSuccess', (data: any) => data);
const setProvinsiAddError = createAction('setProvinsiAddError', (data: any) => data);
const setProvinsiAddLoading = createAction('setProvinsiAddLoading', (data: any) => data);
const setProvinsiAddSuccess = createAction('setProvinsiAddSuccess', (data: any) => data);

/**
 * REDUCER
 */
const provinsiReducer = createReducer(
  {
    [setProvinsiGetError]: (state, data) => ({
      ...state,
      provinsiGetError: data,
    }),
    [setProvinsiGetLoading]: (state, data) => ({
      ...state,
      provinsiGetLoading: data,
    }),
    [setProvinsiGetSuccess]: (state, data) => ({
      ...state,
      provinsiGetSuccess: data,
    }),
    [setProvinsiViewError]: (state, data) => ({
      ...state,
      provinsiViewError: data,
    }),
    [setProvinsiViewLoading]: (state, data) => ({
      ...state,
      provinsiViewLoading: data,
    }),
    [setProvinsiViewSuccess]: (state, data) => ({
      ...state,
      provinsiViewSuccess: data,
    }),
    [setProvinsiAddError]: (state, data) => ({
      ...state,
      provinsiAddError: data,
    }),
    [setProvinsiAddLoading]: (state, data) => ({
      ...state,
      provinsiAddLoading: data,
    }),
    [setProvinsiAddSuccess]: (state, data) => ({
      ...state,
      provinsiAddSuccess: data,
    }),
  },
  DefaultState
);

/**
 * API
 */
export function provinsiFetchAll() {
  return async function (dispatch) {
    dispatch(setProvinsiGetSuccess(false));
    dispatch(setProvinsiGetError(false));
    dispatch(setProvinsiGetLoading(true));
    let config = {};
    config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    };
    fetch(BASE_URL + PROVINSI_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setProvinsiGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setProvinsiGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setProvinsiGetError(true));
      });
  };
}

export function provinsiFetchSub(path) {
  return async function (dispatch) {
    dispatch(setProvinsiGetSuccess(false));
    dispatch(setProvinsiGetError(false));
    dispatch(setProvinsiGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PROVINSI_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setProvinsiGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setProvinsiGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setProvinsiGetError(true));
      });
  };
}

export function provinsiAdd(data) {
  return async function (dispatch) {
    dispatch(setProvinsiAddError(false));
    dispatch(setProvinsiAddSuccess(false));
    dispatch(setProvinsiAddLoading(true));
    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + PROVINSI_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setProvinsiAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((provinsi) => {
        dispatch(setProvinsiAddSuccess(provinsi));
      })
      .catch((err) => {
        dispatch(setProvinsiAddError(true));
      });
  };
}

export function provinsiFetchOne(id) {
  return async function (dispatch) {
    dispatch(setProvinsiViewError(false));
    dispatch(setProvinsiViewSuccess(false));
    dispatch(setProvinsiViewLoading(true));
    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: id,
      }),
    };
    fetch(BASE_URL + PROVINSI_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setProvinsiViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setProvinsiViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setProvinsiViewError(true));
      });
  };
}

export default provinsiReducer;
