import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { BASE_URL } from "../config/constants";
import AsyncStorage from "../AsyncStorage";

// INITIAL STATE
const DefaultState = {
  getDataLoading: false,
  getDataSuccess: false,
  getDataError: false,
  isFormOpen: false,
  isFormClose: false,
  isFormError: false,
  isActionProcess: false,
  isActionSuccess: false,
  isActionFailure: false,
  dataTipeCargoProcess: false,
  dataTipeCargoSuccess: false,
  dataTipeCargoFailure: false,

  mktData: [],
  mktFilter: {},
};

// CREATE ACTION
const setGetDataLoading = createAction('setGetDataLoading', (data: any) => data);
const setGetDataSuccess = createAction('setGetDataSuccess', (data: any) => data);
const setGetDataError = createAction('setGetDataError', (data: any) => data);
const setIsFormOpen = createAction('setIsFormOpen', (data: any) => data);
const setIsFormClose = createAction('setIsFormClose', (data: any) => data);
const setIsFormError = createAction('setIsFormError', (data: any) => data);
const setIsActionProcess = createAction('setIsActionProcess', (data: any) => data);
const setIsActionSuccess = createAction('setIsActionSuccess', (data: any) => data);
const setIsActionFailure = createAction('setIsActionFailure', (data: any) => data);
const setDataTipeCargoProcess = createAction('setDataTipeCargoProcess', (data: any) => data);
const setDataTipeCargoSuccess = createAction('setDataTipeCargoSuccess', (data: any) => data);
const setDataTipeCargoFailure = createAction('setDataTipeCargoFailure', (data: any) => data);

const setMktData = createAction('setMktData', (data: any) => data);
const setMktFilterAdd = createAction('setMktFilterAdd', (data: any) => data);
const setMktFilterDel = createAction('setMktFilterDel', (data: any) => data);
const setMktFilterClear = createAction('setMktFilterClear', (data: any) => data);

// CREATE REDUCER
const masterkodetrayekReducer = createReducer(
  {
    [setGetDataLoading]: (state, data) => ({
      ...state,
      getDataLoading: data,
    }),
    [setGetDataSuccess]: (state, data) => ({
      ...state,
      getDataSuccess: data,
    }),
    [setGetDataError]: (state, data) => ({
      ...state,
      getDataError: data,
    }),
    [setIsFormOpen]: (state, data) => ({
      ...state,
      isFormOpen: data,
    }),
    [setIsFormClose]: (state, data) => ({
      ...state,
      isFormClose: data,
    }),
    [setIsFormError]: (state, data) => ({
      ...state,
      isFormError: data,
    }),
    [setIsActionProcess]: (state, data) => ({
      ...state,
      isActionProcess: data,
    }),
    [setIsActionSuccess]: (state, data) => ({
      ...state,
      isActionSuccess: data,
    }),
    [setIsActionFailure]: (state, data) => ({
      ...state,
      isActionFailure: data,
    }),
    [setDataTipeCargoProcess]: (state, data) => ({
      ...state,
      dataTipeCargoProcess: data,
    }),
    [setDataTipeCargoSuccess]: (state, data) => ({
      ...state,
      dataTipeCargoSuccess: data,
    }),
    [setDataTipeCargoFailure]: (state, data) => ({
      ...state,
      dataTipeCargoFailure: data,
    }),

    [setMktData]: (state, data: Array<"any">) => ({
      ...state,
      mktData: data,
    }),
    [setMktFilterAdd]: (state, data: any) => ({
      ...state,
      mktFilter: data,
    }),
    [setMktFilterDel]: (state, id: any) => ({
      ...state,
      mktFilter: {},
    }),
    [setMktFilterClear]: (state, data: any) => ({
      ...state,
      mktFilter: {},
    }),
  },
  DefaultState
);

export function isForm(status) {
  return async (dispatch) => {
    dispatch(setIsFormOpen(false));
    dispatch(setIsFormClose(false));
    dispatch(setIsFormError(false));
    switch (status) {
      case "open":
        dispatch(setIsFormOpen(true));
        break;
      case "close":
        dispatch(setIsFormClose(true));
        break;
      case "error":
        dispatch(setIsFormError(true));
        break;
      default:
        dispatch(setIsFormClose(true));
    }
  };
}

export function getData(params) {
  let parameter = {};

  parameter["page"] = params.page || "";
  parameter["sizePerPage"] = params.sizePerPage ? params.sizePerPage : "";
  parameter["kodeTrayek"] = params.kodeTrayek ? params.kodeTrayek : "";
  parameter["tahunTrayek"] = params.tahunTrayek ? params.tahunTrayek : "";
  parameter["statusTrayek"] = params.statusTrayek ? params.statusTrayek : "";
  parameter["id_operator"] = params.id_operator ? params.id_operator : "";

  return async (dispatch) => {
    dispatch(setGetDataLoading(true));
    dispatch(setGetDataSuccess(false));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const {
      page,
      sizePerPage,
      kodeTrayek,
      tahunTrayek,
      statusTrayek,
      id_operator,
    } = parameter;
    let urlParams = `?`;
    if (page !== "" && sizePerPage !== "") {
      urlParams += `&page=${page}&limit=${sizePerPage}`;
    }

    if (kodeTrayek !== "") {
      urlParams += `&kodeTrayek=${kodeTrayek}`;
    }

    if (tahunTrayek !== "") {
      urlParams += `&tahunTrayek=${tahunTrayek}`;
    }

    if (statusTrayek !== "") {
      urlParams += `&statusTrayek=${statusTrayek}`;
    }

    if (id_operator !== "") {
      urlParams += `&idOperator=${id_operator}`;
    }

    fetch(BASE_URL + "master_kode_trayek/" + urlParams, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 400) {
          dispatch(setGetDataError(true));
        } else {
          dispatch(setGetDataSuccess(data.data));
        }
      })
      .catch((e) => {
        dispatch(setGetDataError(true));
      });
  };
}

export function isActions(method, params) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem("token@lcs");
    let getConfig = {};
    let urlAction = BASE_URL + "master_kode_trayek/";
    // alert(JSON.stringify(params));
    switch (method) {
      case "ADD":
        getConfig["method"] = "POST";
        // getConfig['url'] = urlAction;
        getConfig["body"] = JSON.stringify(params);
        break;
      case "DELETE":
        getConfig["method"] = "DELETE";
        // getConfig['url'] = urlAction + params.id;
        urlAction = urlAction + params.id;
        break;
      case "EDIT":
        getConfig["method"] = "POST";
        // getConfig['url'] = urlAction + params.id;
        urlAction = urlAction + params.id;
        getConfig["body"] = JSON.stringify(params);
        break;
      default:
        getConfig = {};
        break;
    }

    getConfig["headers"] = {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    };

    dispatch(setIsActionProcess(true));

    fetch(urlAction, getConfig)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 400) {
          alert(data.message);
          dispatch(setIsActionFailure(true));
          dispatch(setIsFormError(true));
          dispatch(setIsActionProcess(false));
        } else {
          alert(data.message);
          dispatch(setIsActionSuccess(true));
          dispatch(setIsFormClose(true));
          dispatch(getData({ page: 1, sizePerPage: 10 }));
          dispatch(setIsActionProcess(false));
        }
      })
      .catch((e) => {
        dispatch(setIsActionFailure());
        dispatch(setIsActionProcess(false));
      });
  };
}

export function getTipeCargo() {
  return async (dispatch) => {
    dispatch(setDataTipeCargoProcess());

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };

    fetch(BASE_URL + "master_tipe_cargo", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 400) {
          dispatch(setDataTipeCargoFailure(true));
        } else {
          dispatch(setDataTipeCargoSuccess(data.data));
        }
      })
      .catch((e) => {
        dispatch(setDataTipeCargoFailure(true));
      });
  };
}


/**
 * Filter
 */
export function mktFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setMktFilterAdd(filter));
  };
}

export function mktFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setMktFilterDel(id));
  };
}

export function mktFilterClear() {
  return function (dispatch, getState) {
    dispatch(setMktFilterClear());
  };
}

export default masterkodetrayekReducer;
