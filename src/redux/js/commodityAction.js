import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    COMMODITY_URL,
    COMMODITY_SUPPLIER_URL,

    COMMODITY_GET_ERROR,
    COMMODITY_GET_LOADING,
    COMMODITY_GET_SUCCESS,

    COMMODITY_VIEW_ERROR,
    COMMODITY_VIEW_LOADING,
    COMMODITY_VIEW_SUCCESS,

    COMMODITY_ADD_ERROR,
    COMMODITY_ADD_LOADING,
    COMMODITY_ADD_INVALID,
    COMMODITY_ADD_SUCCESS,

    COMMODITY_DELETE_ERROR,
    COMMODITY_DELETE_LOADING,
    COMMODITY_DELETE_SUCCESS
} from './constant'

const commodityGetError = (bool) => {
    return {
        type: COMMODITY_GET_ERROR,
        commodityGetError: bool
    }
}

const commodityGetLoading = (bool) => {
    return {
        type: COMMODITY_GET_LOADING,
        commodityGetLoading: bool
    }
}

const commodityGetSuccess = (bool, commodities) => {
    return {
        type: COMMODITY_GET_SUCCESS,
        commodityGetSuccess: bool,
        commodities
    }
}

const commodityViewError = (bool) => {
    return {
        type: COMMODITY_VIEW_ERROR,
        commodityViewError: bool
    }
}

const commodityViewLoading = (bool) => {
    return {
        type: COMMODITY_VIEW_LOADING,
        commodityViewLoading: bool
    }
}

const commodityViewSuccess = (bool, commodity) => {
    return {
        type: COMMODITY_VIEW_SUCCESS,
        commodityViewSuccess: bool,
        commodity
    }
}

const commodityAddError = (bool) => {
    return {
        type: COMMODITY_ADD_ERROR,
        commodityAddError: bool
    }
}

const commodityAddLoading = (bool) => {
    return {
        type: COMMODITY_ADD_LOADING,
        commodityAddLoading: bool
    }
}

const commodityAddInvalid = (bool, invalid) => {
    return {
        type: COMMODITY_ADD_INVALID,
        commodityAddInvalid: bool,
        invalid
    }
}

const commodityAddSuccess = (bool, commodity) => {
    return {
        type: COMMODITY_ADD_SUCCESS,
        commodityAddSuccess: bool,
        commodity
    }
}

const commodityDeleteError = (bool) => {
    return {
        type: COMMODITY_DELETE_ERROR,
        commodityDeleteError: bool
    }
}

const commodityDeleteLoading = (bool) => {
    return {
        type: COMMODITY_DELETE_LOADING,
        commodityDeleteLoading: bool
    }
}

const commodityDeleteSuccess = (bool, commodity) => {
    return {
        type: COMMODITY_DELETE_SUCCESS,
        commodityDeleteSuccess: bool,
        commodity
    }
}

export function commodityFetchAll() {
    return (dispatch) => {
        dispatch(commodityGetSuccess(false, null))
        dispatch(commodityAddSuccess(false, null))
        dispatch(commodityGetError(false))
        dispatch(commodityGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMMODITY_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(commodityGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(commodityGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(commodityGetError(true))
            })
    }
}

export function commodityFetchSub(supplier_id) {
    return (dispatch) => {
        dispatch(commodityGetSuccess(false, null))
        dispatch(commodityGetError(false))
        dispatch(commodityGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMMODITY_SUPPLIER_URL + '/' + supplier_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(commodityGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(commodityGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(commodityGetError(true))
            })
    }
}

export function commodityClear() {

    return (dispatch) => {
        dispatch(commodityGetSuccess(false, null))
        dispatch(commodityGetError(false))
        dispatch(commodityGetLoading(true))
    }
}

export function commodityFetchPage(params) {

    return (dispatch) => {
        dispatch(commodityGetSuccess(false, null))
        dispatch(commodityAddSuccess(false, null))
        dispatch(commodityGetError(false))
        dispatch(commodityGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMMODITY_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(commodityGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(commodityGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(commodityGetError(true))
            })
    }
}

export function commodityAdd(data) {
    return (dispatch) => {
        dispatch(commodityAddError(false))
        dispatch(commodityAddSuccess(false, null))
        dispatch(commodityAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            body: formData
        }
        fetch(BASE_URL + COMMODITY_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(commodityAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((commodity) => {
                if (commodity.success === true) {
                    dispatch(commodityAddSuccess(true, commodity))
                } else {
                    dispatch(commodityAddInvalid(true, commodity))
                }
            })
            .catch((err) => {
                dispatch(commodityAddError(true))
            })
    }
}

export function commodityFetchOne(id) {
    return (dispatch) => {
        dispatch(commodityViewError(false))
        dispatch(commodityViewSuccess(false, null))
        dispatch(commodityViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMMODITY_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(commodityViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(commodityViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(commodityViewError(true))
            })
    }
}

export function commodityUpdate(data) {
    return (dispatch) => {
        dispatch(commodityAddError(false))
        dispatch(commodityAddSuccess(false, null))
        dispatch(commodityAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMMODITY_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(commodityAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((commodity) => {

                if (commodity.success) {
                    dispatch(commodityAddSuccess(true, commodity))
                } else {
                    dispatch(commodityAddError(true))
                }
            })
            .catch((err) => {
                dispatch(commodityAddError(true))
            })
    }
}

export function commodityDelete(id) {
    return (dispatch) => {
        dispatch(commodityViewSuccess(false, null))
        dispatch(commodityDeleteError(false))
        dispatch(commodityDeleteSuccess(false, null))
        dispatch(commodityDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMMODITY_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(commodityDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(commodityDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(commodityDeleteError(true))
            })
    }
}