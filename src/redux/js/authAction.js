import {
    BASE_URL,
    AUTH_LOGIN_URL,
    AUTH_REGISTER_URL,

    AUTH_LOGIN_ERROR,
    AUTH_LOGIN_LOADING,
    AUTH_LOGIN_SUCCESS,

    AUTH_LOGOUT_LOADING,
    AUTH_LOGOUT_SUCCESS,

    AUTH_REGISTER_ERROR,
    AUTH_REGISTER_LOADING,
    AUTH_REGISTER_SUCCESS,

    AUTH_REGISTER_CHECK_URL,
    AUTH_REGISTER_CHECK_ERROR,
    AUTH_REGISTER_CHECK_LOADING,
    AUTH_REGISTER_CHECK_SUCCESS
} from './constant'

const authLoginError = (bool) => {
    return {
        type: AUTH_LOGIN_ERROR,
        authLoginError: bool
    }
}

const authLoginLoading = (bool) => {
    return {
        type: AUTH_LOGIN_LOADING,
        authLoginLoading: bool
    }
}

const authLoginSuccess = (bool, account) => {
    return {
        type: AUTH_LOGIN_SUCCESS,
        authLoginSuccess: bool,
        isAuthenticated: bool,
        account
    }
}

export function doLogin(cred) {
    return (dispatch) => {
        dispatch(authLoginSuccess(false, null))
        dispatch(authLoginError(false))
        dispatch(authLoginLoading(true))

        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: cred.email,
                password: cred.password,
                usery_type_id: cred.usery_type_id
            })
        }
        fetch(BASE_URL + AUTH_LOGIN_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(authLoginLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((account) => {
                localStorage.removeItem('tolaut@email')
                localStorage.removeItem('tolaut@typeUser')
                localStorage.removeItem('tolaut@token')
                localStorage.removeItem('tolaut@userId')
                localStorage.removeItem('tolaut@form')


                account.data.all_actions.map((val, idx) => {
                    localStorage.removeItem(`tolaut@${val.action_key}`, val.allowed)
                })

                if (account.success) {
                    localStorage.setItem('tolaut@email', account.data.user[0].email)
                    localStorage.setItem('tolaut@typeUser', cred.usery_type_id)
                    localStorage.setItem('tolaut@token', account.token)
                    localStorage.setItem('tolaut@userId', account.data.user[0].id)
                    localStorage.setItem('tolaut@form', account.data.form)

                    account.data.acl.map((val, idx) => {
                        localStorage.setItem(`tolaut@${val.action_key}`, val.allowed)
                    })

                    dispatch(authLoginSuccess(true, account))
                } else {
                    dispatch(authLoginError(true))
                }
            })
            .catch((err) => {
                dispatch(authLoginError(true))
            })
    }
}

const authRegisterError = (bool) => {
    return {
        type: AUTH_REGISTER_ERROR,
        authRegisterError: bool
    }
}

const authRegisterLoading = (bool) => {
    return {
        type: AUTH_REGISTER_LOADING,
        authRegisterLoading: bool
    }
}

const authRegisterSuccess = (bool, account) => {
    return {
        type: AUTH_REGISTER_SUCCESS,
        authRegisterSuccess: bool,
        isAuthenticated: bool,
        account
    }
}

export function doRegister(cred) {
    return (dispatch) => {
        dispatch(authRegisterSuccess(false, null))
        dispatch(authRegisterError(false))
        dispatch(authRegisterLoading(true))

        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: cred.email,
                password: cred.password,
                usery_type_id: cred.usery_type_id
            })
        }
        fetch(BASE_URL + AUTH_REGISTER_URL, config)
            .then((response) => {
                if (!response.ok) {
                    dispatch(authRegisterError(true))
                }
                dispatch(authRegisterLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((account) => {

                if (account.success) {
                    dispatch(authRegisterSuccess(true, account))
                } else {
                    dispatch(authRegisterError(true))
                }
            })
            .catch((err) => {
                dispatch(authRegisterError(true))
            })
    }
}

const authLogoutLoading = (bool) => {
    return {
        type: AUTH_LOGOUT_LOADING,
        authLogoutLoading: bool
    }
}

const authLogoutSuccess = (bool) => {
    return {
        type: AUTH_LOGOUT_SUCCESS,
        authLogoutSuccess: bool,
        isAuthenticated: !bool
    }
}

export function doLogout() {
    return dispatch => {
        dispatch(authLogoutLoading(true))
        localStorage.removeItem('tolaut@email')
        localStorage.removeItem('tolaut@typeUser')
        localStorage.removeItem('tolaut@userId')
        localStorage.removeItem('tolaut@token')
        dispatch(authLogoutLoading(false))
        dispatch(authLogoutSuccess(true))
    }
}

const authRegisterCheckError = (bool, check_data) => {
    return {
        type: AUTH_REGISTER_CHECK_ERROR,
        authRegisterCheckError: bool,
        check_data
    }
}

const authRegisterCheckLoading = (bool) => {
    return {
        type: AUTH_REGISTER_CHECK_LOADING,
        authRegisterCheckLoading: bool
    }
}

const authRegisterCheckSuccess = (bool, check_data) => {
    return {
        type: AUTH_REGISTER_CHECK_SUCCESS,
        authRegisterCheckSuccess: bool,
        isAuthenticated: bool,
        check_data
    }
}

export function doRegisterCheck(cred) {
    return (dispatch) => {
        dispatch(authRegisterCheckSuccess(false, null))
        dispatch(authRegisterCheckError(false, null))
        dispatch(authRegisterCheckLoading(true))

        let config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: cred.email,
                usery_type_id: cred.usery_type_id,
                password: cred.password,
                confirm_password: cred.confirm_password
            })
        }
        fetch(BASE_URL + AUTH_REGISTER_CHECK_URL, config)
            .then((response) => {
                if (!response.ok) {
                    dispatch(authRegisterCheckError(true))
                }
                dispatch(authRegisterCheckLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((check_data) => {
                if (check_data.success) {
                    dispatch(authRegisterCheckSuccess(true, check_data))
                } else {
                    dispatch(authRegisterCheckError(true, check_data))
                }
            })
            .catch((err) => {
                dispatch(authRegisterCheckError(true, err))
            })
    }
}