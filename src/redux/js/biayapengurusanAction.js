import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    BIAYAPENGURUSAN_URL,

    BIAYAPENGURUSAN_GET_ERROR,
    BIAYAPENGURUSAN_GET_LOADING,
    BIAYAPENGURUSAN_GET_SUCCESS,

    BIAYAPENGURUSAN_VIEW_ERROR,
    BIAYAPENGURUSAN_VIEW_LOADING,
    BIAYAPENGURUSAN_VIEW_SUCCESS,

    BIAYAPENGURUSAN_ADD_ERROR,
    BIAYAPENGURUSAN_ADD_LOADING,
    BIAYAPENGURUSAN_ADD_SUCCESS,

    BIAYAPENGURUSAN_DELETE_ERROR,
    BIAYAPENGURUSAN_DELETE_LOADING,
    BIAYAPENGURUSAN_DELETE_SUCCESS
} from './constant'

const biayapengurusanGetError = (bool) => {
    return {
        type: BIAYAPENGURUSAN_GET_ERROR,
        biayapengurusanGetError: bool
    }
}

const biayapengurusanGetLoading = (bool) => {
    return {
        type: BIAYAPENGURUSAN_GET_LOADING,
        biayapengurusanGetLoading: bool
    }
}

const biayapengurusanGetSuccess = (bool, biayapengurusans) => {
    return {
        type: BIAYAPENGURUSAN_GET_SUCCESS,
        biayapengurusanGetSuccess: bool,
        biayapengurusans
    }
}

const biayapengurusanViewError = (bool) => {
    return {
        type: BIAYAPENGURUSAN_VIEW_ERROR,
        biayapengurusanViewError: bool
    }
}

const biayapengurusanViewLoading = (bool) => {
    return {
        type: BIAYAPENGURUSAN_VIEW_LOADING,
        biayapengurusanViewLoading: bool
    }
}

const biayapengurusanViewSuccess = (bool, biayapengurusan) => {
    return {
        type: BIAYAPENGURUSAN_VIEW_SUCCESS,
        biayapengurusanViewSuccess: bool,
        biayapengurusan
    }
}

const biayapengurusanAddError = (bool) => {
    return {
        type: BIAYAPENGURUSAN_ADD_ERROR,
        biayapengurusanAddError: bool
    }
}

const biayapengurusanAddLoading = (bool) => {
    return {
        type: BIAYAPENGURUSAN_ADD_LOADING,
        biayapengurusanAddLoading: bool
    }
}

const biayapengurusanAddSuccess = (bool, biayapengurusan) => {
    return {
        type: BIAYAPENGURUSAN_ADD_SUCCESS,
        biayapengurusanAddSuccess: bool,
        biayapengurusan
    }
}

const biayapengurusanDeleteError = (bool) => {
    return {
        type: BIAYAPENGURUSAN_DELETE_ERROR,
        biayapengurusanDeleteError: bool
    }
}

const biayapengurusanDeleteLoading = (bool) => {
    return {
        type: BIAYAPENGURUSAN_DELETE_LOADING,
        biayapengurusanDeleteLoading: bool
    }
}

const biayapengurusanDeleteSuccess = (bool, biayapengurusan) => {
    return {
        type: BIAYAPENGURUSAN_DELETE_SUCCESS,
        biayapengurusanDeleteSuccess: bool,
        biayapengurusan
    }
}

export function biayapengurusanFetchAll(tokenAuth) {
    return (dispatch) => {
        dispatch(biayapengurusanAddSuccess(false, null))
        dispatch(biayapengurusanGetSuccess(false, null))
        dispatch(biayapengurusanGetError(false))
        dispatch(biayapengurusanGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(biayapengurusanGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {

                dispatch(biayapengurusanGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(biayapengurusanGetError(true))
            })
    }
}

export function biayapengurusanFetchSub(path) {
    return (dispatch) => {
        dispatch(biayapengurusanAddSuccess(false, null))
        dispatch(biayapengurusanGetSuccess(false, null))
        dispatch(biayapengurusanGetError(false))
        dispatch(biayapengurusanGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BIAYAPENGURUSAN_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(biayapengurusanGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(biayapengurusanGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(biayapengurusanGetError(true))
            })
    }
}

export function biayapengurusanFetchPage(params) {
    return (dispatch) => {
        dispatch(biayapengurusanAddSuccess(false, null))
        dispatch(biayapengurusanViewSuccess(false, null))
        dispatch(biayapengurusanGetSuccess(false, null))
        dispatch(biayapengurusanGetError(false))
        dispatch(biayapengurusanGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BIAYAPENGURUSAN_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(biayapengurusanGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(biayapengurusanGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(biayapengurusanGetError(true))
            })
    }
}

export function biayapengurusanAdd(data) {
    return (dispatch) => {
        dispatch(biayapengurusanAddError(false))
        dispatch(biayapengurusanViewSuccess(false, null))
        dispatch(biayapengurusanAddSuccess(false, null))
        dispatch(biayapengurusanAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(biayapengurusanAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((biayapengurusan) => {
                dispatch(biayapengurusanAddSuccess(true, biayapengurusan))
            })
            .catch((err) => {
                dispatch(biayapengurusanAddError(true))
            })
    }
}

export function biayapengurusanFetchOne(id) {
    return (dispatch) => {
        dispatch(biayapengurusanViewError(false))
        dispatch(biayapengurusanViewSuccess(false, null))
        dispatch(biayapengurusanViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id: id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BIAYAPENGURUSAN_URL + '/view', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(biayapengurusanViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(biayapengurusanViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(biayapengurusanViewError(true))
            })
    }
}

export function biayapengurusanUpdate(data) {
    return (dispatch) => {
        dispatch(biayapengurusanViewSuccess(false, null))
        dispatch(biayapengurusanAddError(false))
        dispatch(biayapengurusanAddSuccess(false, null))
        dispatch(biayapengurusanAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(biayapengurusanAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((biayapengurusan) => {
                if (biayapengurusan.success) {
                    dispatch(biayapengurusanAddSuccess(true, biayapengurusan))
                } else {
                    dispatch(biayapengurusanAddError(true))
                }
            })
            .catch((err) => {
                dispatch(biayapengurusanAddError(true))
            })
    }
}

export function biayapengurusanDelete(id) {
    return (dispatch) => {
        dispatch(biayapengurusanViewSuccess(false, null))
        dispatch(biayapengurusanDeleteError(false))
        dispatch(biayapengurusanDeleteSuccess(false, null))
        dispatch(biayapengurusanDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BIAYAPENGURUSAN_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(biayapengurusanDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(biayapengurusanDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(biayapengurusanDeleteError(true))
            })
    }
}