import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    COMPLAINT_URL,

    COMPLAINT_GET_ERROR,
    COMPLAINT_GET_LOADING,
    COMPLAINT_GET_SUCCESS,

    COMPLAINT_VIEW_ERROR,
    COMPLAINT_VIEW_LOADING,
    COMPLAINT_VIEW_SUCCESS,

    COMPLAINT_ADD_ERROR,
    COMPLAINT_ADD_LOADING,
    COMPLAINT_ADD_SUCCESS,

    COMPLAINT_DELETE_ERROR,
    COMPLAINT_DELETE_LOADING,
    COMPLAINT_DELETE_SUCCESS,
    COMPLAINT_REPLY_ERROR,
    COMPLAINT_REPLY_LOADING,
    COMPLAINT_REPLY_SUCCESS,
    COMPLAINT_CLOSE_ERROR,
    COMPLAINT_CLOSE_SUCCESS,
    COMPLAINT_CLOSE_LOADING
} from './constant'

const complaintGetError = (bool) => {
    return {
        type: COMPLAINT_GET_ERROR,
        complaintGetError: bool
    }
}

const complaintGetLoading = (bool) => {
    return {
        type: COMPLAINT_GET_LOADING,
        complaintGetLoading: bool
    }
}

const complaintGetSuccess = (bool, complaints) => {
    return {
        type: COMPLAINT_GET_SUCCESS,
        complaintGetSuccess: bool,
        complaints
    }
}

const complaintViewError = (bool) => {
    return {
        type: COMPLAINT_VIEW_ERROR,
        complaintViewError: bool
    }
}

const complaintViewLoading = (bool) => {
    return {
        type: COMPLAINT_VIEW_LOADING,
        complaintViewLoading: bool
    }
}

const complaintViewSuccess = (bool, complaint) => {
    return {
        type: COMPLAINT_VIEW_SUCCESS,
        complaintViewSuccess: bool,
        complaint
    }
}

const complaintAddError = (bool) => {
    return {
        type: COMPLAINT_ADD_ERROR,
        complaintAddError: bool
    }
}

const complaintAddLoading = (bool) => {
    return {
        type: COMPLAINT_ADD_LOADING,
        complaintAddLoading: bool
    }
}

const complaintAddSuccess = (bool, complaint) => {
    return {
        type: COMPLAINT_ADD_SUCCESS,
        complaintAddSuccess: bool,
        complaint
    }
}

const complaintDeleteError = (bool) => {
    return {
        type: COMPLAINT_DELETE_ERROR,
        complaintDeleteError: bool
    }
}

const complaintDeleteLoading = (bool) => {
    return {
        type: COMPLAINT_DELETE_LOADING,
        complaintDeleteLoading: bool
    }
}

const complaintDeleteSuccess = (bool, complaint) => {
    return {
        type: COMPLAINT_DELETE_SUCCESS,
        complaintDeleteSuccess: bool,
        complaint
    }
}

const complaintReplyError = (bool) => {
    return {
        type: COMPLAINT_REPLY_ERROR,
        complaintReplyError: bool
    }
}

const complaintReplyLoading = (bool) => {
    return {
        type: COMPLAINT_REPLY_LOADING,
        complaintReplyLoading: bool
    }
}

const complaintReplySuccess = (bool, complaints) => {
    return {
        type: COMPLAINT_REPLY_SUCCESS,
        complaintReplySuccess: bool,
        complaints
    }
}

const complaintCloseError = (bool) => {
    return {
        type: COMPLAINT_CLOSE_ERROR,
        complaintCloseError: bool
    }
}

const complaintCloseLoading = (bool) => {
    return {
        type: COMPLAINT_CLOSE_LOADING,
        complaintCloseLoading: bool
    }
}

const complaintCloseSuccess = (bool, complaints) => {
    return {
        type: COMPLAINT_CLOSE_SUCCESS,
        complaintCloseSuccess: bool,
        complaints
    }
}

export function complaintFetchAll() {
    return (dispatch) => {
        dispatch(complaintGetSuccess(false, null))
        dispatch(complaintGetError(false))
        dispatch(complaintGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMPLAINT_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(complaintGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(complaintGetError(true))
            })
    }
}

export function complaintFetchSub(supplier_id) {
    return (dispatch) => {
        dispatch(complaintGetSuccess(false, null))
        dispatch(complaintGetError(false))
        dispatch(complaintGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMPLAINT_URL + '/' + supplier_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(complaintGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(complaintGetError(true))
            })
    }
}

export function complaintFetchPage(params) {
    return (dispatch) => {
        dispatch(complaintGetSuccess(false, null))
        dispatch(complaintGetError(false))
        dispatch(complaintGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMPLAINT_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(complaintGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(complaintGetError(true))
            })
    }
}

export function complaintAdd(data) {
    return (dispatch) => {
        dispatch(complaintAddError(false))
        dispatch(complaintAddSuccess(false, null))
        dispatch(complaintAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + COMPLAINT_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((complaint) => {
                dispatch(complaintAddSuccess(true, complaint))
            })
            .catch((err) => {
                dispatch(complaintAddError(true))
            })
    }
}

export function complaintFetchOne(id) {
    return (dispatch) => {
        dispatch(complaintViewError(false))
        dispatch(complaintViewSuccess(false, null))
        dispatch(complaintViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = {
            id: id
        }

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMPLAINT_URL + '/view', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(complaintViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(complaintViewError(true))
            })
    }
}

export function complaintUpdate(data) {
    return (dispatch) => {
        dispatch(complaintAddError(false))
        dispatch(complaintAddSuccess(false, null))
        dispatch(complaintAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMPLAINT_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((complaint) => {
                if (complaint.status == 200) {
                    dispatch(complaintAddSuccess(true, complaint))
                } else {
                    dispatch(complaintAddError(true))
                }
            })
            .catch((err) => {
                dispatch(complaintAddError(true))
            })
    }
}

export function complaintDelete(id) {
    return (dispatch) => {
        dispatch(complaintViewSuccess(false, null))
        dispatch(complaintDeleteError(false))
        dispatch(complaintDeleteSuccess(false, null))
        dispatch(complaintDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + COMPLAINT_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(complaintDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(complaintDeleteError(true))
            })
    }
}

export function complaintReply(data) {
    return (dispatch) => {
        dispatch(complaintReplyError(false))
        dispatch(complaintReplySuccess(false, null))
        dispatch(complaintReplyLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + COMPLAINT_URL + '/reply', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintReplyLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((complaint) => {
                dispatch(complaintReplySuccess(true, complaint))
            })
            .catch((err) => {
                dispatch(complaintReplyError(true))
            })
    }
}

export function complaintClose(data) {
    return (dispatch) => {
        dispatch(complaintCloseError(false))
        dispatch(complaintCloseSuccess(false, null))
        dispatch(complaintCloseLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + COMPLAINT_URL + '/close', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(complaintCloseLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((complaint) => {
                dispatch(complaintCloseSuccess(true, complaint))
            })
            .catch((err) => {
                dispatch(complaintCloseError(true))
            })
    }
}