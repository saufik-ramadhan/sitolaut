import {doLogout} from 'actions/authAction';
import {
  BASE_URL,
  DEPOT_URL,
  DEPOT_GET_ERROR,
  DEPOT_GET_LOADING,
  DEPOT_GET_SUCCESS,
  DEPOT_VIEW_ERROR,
  DEPOT_VIEW_LOADING,
  DEPOT_VIEW_SUCCESS,
  DEPOT_ADD_ERROR,
  DEPOT_ADD_LOADING,
  DEPOT_ADD_SUCCESS,
  DEPOT_DELETE_ERROR,
  DEPOT_DELETE_LOADING,
  DEPOT_DELETE_SUCCESS,
} from './constant';

const depotGetError = (bool) => {
  return {
    type: DEPOT_GET_ERROR,
    depotGetError: bool,
  };
};

const depotGetLoading = (bool) => {
  return {
    type: DEPOT_GET_LOADING,
    depotGetLoading: bool,
  };
};

const depotGetSuccess = (bool, depots) => {
  return {
    type: DEPOT_GET_SUCCESS,
    depotGetSuccess: bool,
    depots,
  };
};

const depotViewError = (bool) => {
  return {
    type: DEPOT_VIEW_ERROR,
    depotViewError: bool,
  };
};

const depotViewLoading = (bool) => {
  return {
    type: DEPOT_VIEW_LOADING,
    depotViewLoading: bool,
  };
};

const depotViewSuccess = (bool, depot) => {
  return {
    type: DEPOT_VIEW_SUCCESS,
    depotViewSuccess: bool,
    depot,
  };
};

const depotAddError = (bool) => {
  return {
    type: DEPOT_ADD_ERROR,
    depotAddError: bool,
  };
};

const depotAddLoading = (bool) => {
  return {
    type: DEPOT_ADD_LOADING,
    depotAddLoading: bool,
  };
};

const depotAddSuccess = (bool, depot) => {
  return {
    type: DEPOT_ADD_SUCCESS,
    depotAddSuccess: bool,
    depot,
  };
};

const depotDeleteError = (bool) => {
  return {
    type: DEPOT_DELETE_ERROR,
    depotDeleteError: bool,
  };
};

const depotDeleteLoading = (bool) => {
  return {
    type: DEPOT_DELETE_LOADING,
    depotDeleteLoading: bool,
  };
};

const depotDeleteSuccess = (bool, depot) => {
  return {
    type: DEPOT_DELETE_SUCCESS,
    depotDeleteSuccess: bool,
    depot,
  };
};

export function depotFetchAll() {
  return (dispatch) => {
    dispatch(depotAddSuccess(false, null));
    dispatch(depotGetSuccess(false, null));
    dispatch(depotGetError(false));
    dispatch(depotGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(depotGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(depotGetError(true));
      });
  };
}

export function depotFetchSub(data) {
  return (dispatch) => {
    dispatch(depotAddSuccess(false, null));
    dispatch(depotGetSuccess(false, null));
    dispatch(depotGetError(false));
    dispatch(depotGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DEPOT_URL + '/sub', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(depotGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(depotGetError(true));
      });
  };
}

export function depotFetchPage(params) {
  return (dispatch) => {
    dispatch(depotAddSuccess(false, null));
    dispatch(depotGetSuccess(false, null));
    dispatch(depotGetError(false));
    dispatch(depotGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DEPOT_URL + '/page', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(depotGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(depotGetError(true));
      });
  };
}

export function depotSearch(params) {
  return (dispatch) => {
    dispatch(depotAddSuccess(false, null));
    dispatch(depotGetSuccess(false, null));
    dispatch(depotGetError(false));
    dispatch(depotGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DEPOT_URL + '/search', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(depotGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(depotGetError(true));
      });
  };
}

export function depotAdd(data) {
  return (dispatch) => {
    dispatch(depotAddError(false));
    dispatch(depotAddSuccess(false, null));
    dispatch(depotAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};

    config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((depot) => {
        dispatch(depotAddSuccess(true, depot));
      })
      .catch((err) => {
        dispatch(depotAddError(true));
      });
  };
}

export function depotFetchOne(id) {
  return (dispatch) => {
    dispatch(depotAddSuccess(false, null));
    dispatch(depotViewError(false));
    dispatch(depotViewSuccess(false, null));
    dispatch(depotViewLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DEPOT_URL + '/' + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(depotViewSuccess(true, data));
      })
      .catch((err) => {
        dispatch(depotViewError(true));
      });
  };
}

export function depotUpdate(data) {
  return (dispatch) => {
    dispatch(depotAddError(false));
    dispatch(depotAddSuccess(false, null));
    dispatch(depotAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((depot) => {
        if (depot.success) {
          dispatch(depotAddSuccess(true, depot));
        } else {
          dispatch(depotAddError(true));
        }
      })
      .catch((err) => {
        dispatch(depotAddError(true));
      });
  };
}

export function depotDelete(id) {
  return (dispatch) => {
    dispatch(depotViewSuccess(false, null));
    dispatch(depotDeleteError(false));
    dispatch(depotDeleteSuccess(false, null));
    dispatch(depotDeleteLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({id: id}),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DEPOT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(depotDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(depotDeleteSuccess(true, data));
      })
      .catch((err) => {
        dispatch(depotDeleteError(true));
      });
  };
}
