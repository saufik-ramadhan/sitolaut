import {
  BASE_URL,
  FORGOT_PASSWORD_URL,
  FORGOT_PASSWORD_ERROR,
  FORGOT_PASSWORD_LOADING,
  FORGOT_PASSWORD_SUCCESS,
  RESET_PASSWORD_URL,
  RESET_PASSWORD_ERROR,
  RESET_PASSWORD_LOADING,
  RESET_PASSWORD_SUCCESS,
} from './constant';

const forgotPasswordError = (bool, resultData) => {
  return {
    type: FORGOT_PASSWORD_ERROR,
    forgotPasswordError: bool,
    resultData,
  };
};

const forgotPasswordLoading = (bool) => {
  return {
    type: FORGOT_PASSWORD_LOADING,
    forgotPasswordLoading: bool,
  };
};

const forgotPasswordSuccess = (bool, resultData) => {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    forgotPasswordSuccess: bool,
    resultData,
  };
};

export function forgotPassword(data) {
  return (dispatch) => {
    dispatch(forgotPasswordSuccess(false, null));
    dispatch(forgotPasswordError(false, null));
    dispatch(forgotPasswordLoading(true));

    let config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };

    fetch(BASE_URL + FORGOT_PASSWORD_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(forgotPasswordLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          return dispatch(forgotPasswordSuccess(true, data));
        } else {
          return dispatch(forgotPasswordError(true, data));
        }
      })
      .catch((err) => {
        dispatch(forgotPasswordLoading(false));
        dispatch(forgotPasswordError(true, err));
      });
  };
}

const resetPasswordError = (bool, resultPassword) => {
  return {
    type: RESET_PASSWORD_ERROR,
    resetPasswordError: bool,
    resultPassword,
  };
};

const resetPasswordLoading = (bool) => {
  return {
    type: RESET_PASSWORD_LOADING,
    resetPasswordLoading: bool,
  };
};

const resetPasswordSuccess = (bool, resultPassword) => {
  return {
    type: RESET_PASSWORD_SUCCESS,
    resetPasswordSuccess: bool,
    resultPassword,
  };
};

export function resetPassword(data) {
  return (dispatch) => {
    dispatch(resetPasswordSuccess(false, null));
    dispatch(resetPasswordError(false, null));
    dispatch(resetPasswordLoading(true));

    let config = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };

    fetch(BASE_URL + RESET_PASSWORD_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(resetPasswordLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          return dispatch(resetPasswordSuccess(true, data));
        } else {
          return dispatch(resetPasswordError(true, data));
        }
      })
      .catch((err) => {
        dispatch(resetPasswordLoading(false));
        dispatch(resetPasswordError(true, err));
      });
  };
}
