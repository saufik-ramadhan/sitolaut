import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    CONSIGNEE_URL,

    CONSIGNEE_GET_ERROR,
    CONSIGNEE_GET_LOADING,
    CONSIGNEE_GET_SUCCESS,

    CONSIGNEE_VIEW_ERROR,
    CONSIGNEE_VIEW_LOADING,
    CONSIGNEE_VIEW_SUCCESS,

    CONSIGNEE_ADD_ERROR,
    CONSIGNEE_ADD_LOADING,
    CONSIGNEE_ADD_INVALID,
    CONSIGNEE_ADD_SUCCESS,

    CONSIGNEE_DELETE_ERROR,
    CONSIGNEE_DELETE_LOADING,
    CONSIGNEE_DELETE_SUCCESS
} from './constant'

const consigneeGetError = (bool) => {
    return {
        type: CONSIGNEE_GET_ERROR,
        consigneeGetError: bool
    }
}

const consigneeGetLoading = (bool) => {
    return {
        type: CONSIGNEE_GET_LOADING,
        consigneeGetLoading: bool
    }
}

const consigneeGetSuccess = (bool, consignees) => {
    return {
        type: CONSIGNEE_GET_SUCCESS,
        consigneeGetSuccess: bool,
        consignees
    }
}

const consigneeViewError = (bool) => {
    return {
        type: CONSIGNEE_VIEW_ERROR,
        consigneeViewError: bool
    }
}

const consigneeViewLoading = (bool) => {
    return {
        type: CONSIGNEE_VIEW_LOADING,
        consigneeViewLoading: bool
    }
}

const consigneeViewSuccess = (bool, consignee) => {
    return {
        type: CONSIGNEE_VIEW_SUCCESS,
        consigneeViewSuccess: bool,
        consignee
    }
}

const consigneeAddError = (bool) => {
    return {
        type: CONSIGNEE_ADD_ERROR,
        consigneeAddError: bool
    }
}

const consigneeAddLoading = (bool) => {
    return {
        type: CONSIGNEE_ADD_LOADING,
        consigneeAddLoading: bool
    }
}

const consigneeAddInvalid = (bool, invalid) => {
    return {
        type: CONSIGNEE_ADD_INVALID,
        consigneeAddInvalid: bool,
        invalid
    }
}

const consigneeAddSuccess = (bool, consignee) => {
    return {
        type: CONSIGNEE_ADD_SUCCESS,
        consigneeAddSuccess: bool,
        consignee
    }
}

const consigneeDeleteError = (bool) => {
    return {
        type: CONSIGNEE_DELETE_ERROR,
        consigneeDeleteError: bool
    }
}

const consigneeDeleteLoading = (bool) => {
    return {
        type: CONSIGNEE_DELETE_LOADING,
        consigneeDeleteLoading: bool
    }
}

const consigneeDeleteSuccess = (bool, consignee) => {
    return {
        type: CONSIGNEE_DELETE_SUCCESS,
        consigneeDeleteSuccess: bool,
        consignee
    }
}

export function consigneeFetchAll() {
    return (dispatch) => {
        dispatch(consigneeGetSuccess(false, null))
        dispatch(consigneeGetError(false))
        dispatch(consigneeGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeGetError(true))
            })
    }
}

export function consigneeFetchSub(itemId) {
    return (dispatch) => {
        dispatch(consigneeGetSuccess(false, null))
        dispatch(consigneeGetError(false))
        dispatch(consigneeGetLoading(true))
        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    company: itemId
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeGetError(true))
            })
    }
}

export function consigneeFetchPage(item) {
    return (dispatch) => {
        dispatch(consigneeGetSuccess(false, null))
        dispatch(consigneeGetError(false))
        dispatch(consigneeGetLoading(true))
        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(item)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeGetError(true))
            })
    }
}

export function consigneeAdd(data) {
    return (dispatch) => {
        dispatch(consigneeAddError(false))
        dispatch(consigneeAddInvalid(false, null))
        dispatch(consigneeAddSuccess(false, null))
        dispatch(consigneeAddLoading(true))

        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'POST',
            body: formData
        }

        fetch(BASE_URL + CONSIGNEE_URL + '/new', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((consignee) => {
                if (consignee.success === true) {
                    dispatch(consigneeAddSuccess(true, consignee))
                } else {
                    dispatch(consigneeAddInvalid(true, consignee))
                }
            })
            .catch((err) => {
                dispatch(consigneeAddError(true))
            })
    }
}

export function consigneeUpdate(data) {
    return (dispatch) => {
        dispatch(consigneeAddError(false))
        dispatch(consigneeAddSuccess(false, null))
        dispatch(consigneeAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((consignee) => {
                dispatch(consigneeAddSuccess(true, consignee))
            })
            .catch((err) => {
                dispatch(consigneeAddError(true))
            })
    }
}

export function handleStatus(data) {
    return (dispatch) => {
        dispatch(consigneeAddError(false))
        dispatch(consigneeAddSuccess(false, null))
        dispatch(consigneeAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }

        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + 'register/' + data.status, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((consignee) => {
                dispatch(consigneeAddSuccess(true, consignee))
            })
            .catch((err) => {
                dispatch(consigneeAddError(true))
            })
    }
}

export function consigneeFetchOne(id) {
    return (dispatch) => {
        dispatch(consigneeViewError(false))
        dispatch(consigneeViewSuccess(false, null))
        dispatch(consigneeViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeViewError(true))
            })
    }
}

export function consigneeFetchUser(id) {
    return (dispatch) => {
        dispatch(consigneeViewError(false))
        dispatch(consigneeViewSuccess(false, null))
        dispatch(consigneeViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL + '/profile', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeViewError(true))
            })
    }
}

export function consigneeDelete(id) {
    return (dispatch) => {
        dispatch(consigneeViewSuccess(false, null))
        dispatch(consigneeDeleteError(false))
        dispatch(consigneeDeleteSuccess(false, null))
        dispatch(consigneeDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + CONSIGNEE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(consigneeDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(consigneeDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(consigneeDeleteError(true))
            })
    }
}