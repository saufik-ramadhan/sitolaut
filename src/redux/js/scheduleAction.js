import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    SCHEDULE_URL,

    SCHEDULE_GET_ERROR,
    SCHEDULE_GET_LOADING,
    SCHEDULE_GET_SUCCESS,

    SCHEDULE_VIEW_ERROR,
    SCHEDULE_VIEW_LOADING,
    SCHEDULE_VIEW_SUCCESS,

    SCHEDULE_ADD_ERROR,
    SCHEDULE_ADD_LOADING,
    SCHEDULE_ADD_SUCCESS,

    SCHEDULE_DELETE_ERROR,
    SCHEDULE_DELETE_LOADING,
    SCHEDULE_DELETE_SUCCESS
} from './constant'

const scheduleGetError = (bool) => {
    return {
        type: SCHEDULE_GET_ERROR,
        scheduleGetError: bool
    }
}

const scheduleGetLoading = (bool) => {
    return {
        type: SCHEDULE_GET_LOADING,
        scheduleGetLoading: bool
    }
}

const scheduleGetSuccess = (bool, schedules) => {
    return {
        type: SCHEDULE_GET_SUCCESS,
        scheduleGetSuccess: bool,
        schedules
    }
}

const scheduleViewError = (bool) => {
    return {
        type: SCHEDULE_VIEW_ERROR,
        scheduleViewError: bool
    }
}

const scheduleViewLoading = (bool) => {
    return {
        type: SCHEDULE_VIEW_LOADING,
        scheduleViewLoading: bool
    }
}

const scheduleViewSuccess = (bool, schedule) => {
    return {
        type: SCHEDULE_VIEW_SUCCESS,
        scheduleViewSuccess: bool,
        schedule
    }
}

const scheduleAddError = (bool) => {
    return {
        type: SCHEDULE_ADD_ERROR,
        scheduleAddError: bool
    }
}

const scheduleAddLoading = (bool) => {
    return {
        type: SCHEDULE_ADD_LOADING,
        scheduleAddLoading: bool
    }
}

const scheduleAddSuccess = (bool, schedule) => {
    return {
        type: SCHEDULE_ADD_SUCCESS,
        scheduleAddSuccess: bool,
        schedule
    }
}

const scheduleDeleteError = (bool) => {
    return {
        type: SCHEDULE_DELETE_ERROR,
        scheduleDeleteError: bool
    }
}

const scheduleDeleteLoading = (bool) => {
    return {
        type: SCHEDULE_DELETE_LOADING,
        scheduleDeleteLoading: bool
    }
}

const scheduleDeleteSuccess = (bool, schedule) => {
    return {
        type: SCHEDULE_DELETE_SUCCESS,
        scheduleDeleteSuccess: bool,
        schedule
    }
}

export function scheduleFetchAll() {
    return (dispatch) => {
        dispatch(scheduleAddSuccess(false, null))
        dispatch(scheduleGetSuccess(false, null))
        dispatch(scheduleGetError(false))
        dispatch(scheduleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(scheduleGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(scheduleGetError(true))
            })
    }
}

export function scheduleFetchSub(supplier_id) {
    return (dispatch) => {
        dispatch(scheduleAddSuccess(false, null))
        dispatch(scheduleGetSuccess(false, null))
        dispatch(scheduleGetError(false))
        dispatch(scheduleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL + '/' + supplier_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(scheduleGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(scheduleGetError(true))
            })
    }
}

export function scheduleFetchOld(params) {
    return (dispatch) => {
        dispatch(scheduleAddSuccess(false, null))
        dispatch(scheduleGetSuccess(false, null))
        dispatch(scheduleGetError(false))
        dispatch(scheduleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL + '/old', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(scheduleGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(scheduleGetError(true))
            })
    }
}

export function scheduleReset() {
    return (dispatch) => {
        dispatch(scheduleGetSuccess(false, null))
    }
}

export function scheduleClear() {
    return (dispatch) => {
        dispatch(scheduleGetSuccess(false, null))
        dispatch(scheduleGetError(false))
        dispatch(scheduleGetLoading(true))
    }
}

export function scheduleFetchWill(params) {
    return (dispatch) => {
        dispatch(scheduleGetSuccess(false, null))
        dispatch(scheduleGetError(false))
        dispatch(scheduleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL + '/will', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(scheduleGetSuccess(true, data))
                } else {
                    dispatch(scheduleGetError(true))
                }
            })
            .catch((err) => {
                dispatch(scheduleGetError(true))
            })
    }
}

export function scheduleSearch(params) {
    return (dispatch) => {
        dispatch(scheduleAddSuccess(false, null))
        dispatch(scheduleGetSuccess(false, null))
        dispatch(scheduleGetError(false))
        dispatch(scheduleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL + '/search', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(scheduleGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(scheduleGetError(true))
            })
    }
}

export function scheduleAdd(data) {

    return (dispatch) => {
        dispatch(scheduleAddError(false))
        dispatch(scheduleAddSuccess(false, null))
        dispatch(scheduleAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + SCHEDULE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((schedule) => {
                dispatch(scheduleAddSuccess(true, schedule))
            })
            .catch((err) => {
                dispatch(scheduleAddError(true))
            })
    }
}

export function scheduleFetchOne(id) {
    return (dispatch) => {
        dispatch(scheduleAddSuccess(false, null))
        dispatch(scheduleViewError(false))
        dispatch(scheduleViewSuccess(false, null))
        dispatch(scheduleViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(scheduleViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(scheduleViewError(true))
            })
    }
}

export function scheduleUpdate(data) {
    return (dispatch) => {
        dispatch(scheduleAddError(false))
        dispatch(scheduleAddSuccess(false, null))
        dispatch(scheduleAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((schedule) => {

                if (schedule.success) {
                    dispatch(scheduleAddSuccess(true, schedule))
                } else {
                    dispatch(scheduleAddError(true))
                }
            })
            .catch((err) => {
                dispatch(scheduleAddError(true))
            })
    }
}

export function scheduleDelete(id) {
    return (dispatch) => {
        dispatch(scheduleViewSuccess(false, null))
        dispatch(scheduleDeleteError(false))
        dispatch(scheduleDeleteSuccess(false, null))
        dispatch(scheduleDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + SCHEDULE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(scheduleDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(scheduleDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(scheduleDeleteError(true))
            })
    }
}