import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    BOOKING_URL,

    BOOKING_GET_ERROR,
    BOOKING_GET_LOADING,
    BOOKING_GET_SUCCESS,

    BOOKING_VIEW_ERROR,
    BOOKING_VIEW_LOADING,
    BOOKING_VIEW_SUCCESS,

    BOOKING_ADD_ERROR,
    BOOKING_ADD_LOADING,
    BOOKING_ADD_SUCCESS,

    BOOKING_HAS_DO_VIEW_URL,
    BOOKING_HAS_DO_VIEW_ERROR,
    BOOKING_HAS_DO_VIEW_LOADING,
    BOOKING_HAS_DO_VIEW_SUCCESS,

    BOOKING_HAS_DO_UPDATE_URL,
    BOOKING_HAS_DO_UPDATE_ERROR,
    BOOKING_HAS_DO_UPDATE_LOADING,
    BOOKING_HAS_DO_UPDATE_INVALID,
    BOOKING_HAS_DO_UPDATE_SUCCESS
} from './constant'

const bookingGetError = (bool) => {
    return {
        type: BOOKING_GET_ERROR,
        bookingGetError: bool
    }
}

const bookingGetLoading = (bool) => {
    return {
        type: BOOKING_GET_LOADING,
        bookingGetLoading: bool
    }
}

const bookingGetSuccess = (bool, bookings) => {
    return {
        type: BOOKING_GET_SUCCESS,
        bookingGetSuccess: bool,
        bookings
    }
}

const bookingViewError = (bool) => {
    return {
        type: BOOKING_VIEW_ERROR,
        bookingViewError: bool
    }
}

const bookingViewLoading = (bool) => {
    return {
        type: BOOKING_VIEW_LOADING,
        bookingViewLoading: bool
    }
}

const bookingViewSuccess = (bool, booking) => {
    return {
        type: BOOKING_VIEW_SUCCESS,
        bookingViewSuccess: bool,
        booking
    }
}

const bookingAddError = (bool) => {
    return {
        type: BOOKING_ADD_ERROR,
        bookingAddError: bool
    }
}

const bookingAddLoading = (bool) => {
    return {
        type: BOOKING_ADD_LOADING,
        bookingAddLoading: bool
    }
}

const bookingAddSuccess = (bool, booking) => {
    return {
        type: BOOKING_ADD_SUCCESS,
        bookingAddSuccess: bool,
        booking
    }
}

export function bookingFetchAll() {
    return (dispatch) => {
        dispatch(bookingGetSuccess(false, null))
        dispatch(bookingGetError(false))
        dispatch(bookingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingGetError(true))
            })
    }
}

export function bookingFetchSub(path) {
    return (dispatch) => {
        dispatch(bookingGetSuccess(false, null))
        dispatch(bookingGetError(false))
        dispatch(bookingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingGetError(true))
            })
    }
}

export function bookingFetchPage(params) {
    return (dispatch) => {
        dispatch(bookingGetSuccess(false, null))
        dispatch(bookingGetError(false))
        dispatch(bookingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingGetError(true))
            })
    }
}

export function bookingFetchConsigneeReceived(param) {
    return (dispatch) => {
        dispatch(bookingGetSuccess(false, null))
        dispatch(bookingGetError(false))
        dispatch(bookingGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(param)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/consignee', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingGetError(true))
            })
    }
}

export function bookingAdd(data) {

    return (dispatch) => {
        dispatch(bookingAddError(false))
        dispatch(bookingAddSuccess(false, null))
        dispatch(bookingAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + BOOKING_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((booking) => {
                dispatch(bookingAddSuccess(true, booking))
            })
            .catch((err) => {
                dispatch(bookingAddError(true))
            })
    }
}

export function bookingFetchOne(id) {
    return (dispatch) => {
        dispatch(bookingViewSuccess(false, null))
        dispatch(bookingViewError(false))
        dispatch(bookingViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let data = {
            'id': id
        }
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/view', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingViewError(true))
            })
    }
}

export function bookingFetchSi(id) {
    return (dispatch) => {
        dispatch(bookingViewSuccess(false, null))
        dispatch(bookingViewError(false))
        dispatch(bookingViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "id": id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/shipping_intructions', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingViewError(true))
            })
    }
}

export function bookingFetchRo(id) {
    return (dispatch) => {
        dispatch(bookingViewSuccess(false, null))
        dispatch(bookingViewError(false))
        dispatch(bookingViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "id": id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/release_order', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(bookingViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(bookingViewError(true))
            })
    }
}

export function bookingConfirm(data) {

    return (dispatch) => {
        dispatch(bookingViewSuccess(false, null))
        dispatch(bookingAddSuccess(false, null))
        dispatch(bookingAddError(false))
        dispatch(bookingAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/status', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((booking) => {
                if (booking.status == 200) {
                    dispatch(bookingAddSuccess(true, booking))
                } else {
                    dispatch(bookingAddError(true))
                }
            })
            .catch((err) => {
                dispatch(bookingAddError(true))
            })
    }
}

export function bookingPayment(data) {
    return (dispatch) => {
        dispatch(bookingAddSuccess(false, null))
        dispatch(bookingAddError(false))
        dispatch(bookingAddLoading(true))
        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/payment', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((booking) => {
                if (booking.status == 200) {
                    dispatch(bookingAddSuccess(true, booking))
                } else {
                    dispatch(bookingAddError(true))
                }
            })
            .catch((err) => {
                dispatch(bookingAddError(true))
            })
    }
}

export function bookingGenerateBL(data) {
    return (dispatch) => {
        dispatch(bookingAddSuccess(false, null))
        dispatch(bookingAddError(false))
        dispatch(bookingAddLoading(true))
        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/generate_bl', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((booking) => {
                if (booking.success) {
                    dispatch(bookingAddSuccess(true, booking))
                } else {
                    dispatch(bookingAddError(true))
                }
            })
            .catch((err) => {
                dispatch(bookingAddError(true))
            })
    }
}

// TODO: Need update
export function bookingAmbilBarang(data) {
    return (dispatch) => {
        dispatch(bookingViewSuccess(false, null))
        dispatch(bookingAddSuccess(false, null))
        dispatch(bookingAddError(false))
        dispatch(bookingAddLoading(true))
        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + BOOKING_URL + '/pengambilan_barang', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((booking) => {
                if (booking.status === 200) {
                    dispatch(bookingViewSuccess(false, null))
                    dispatch(bookingAddSuccess(true, booking))
                } else {
                    dispatch(bookingAddError(true))
                }
            })
            .catch((err) => {
                dispatch(bookingAddError(true))
            })
    }
}

// Booking has DO (Goods Receipt)
const bookingHasDOViewError = (bool) => {
    return {
        type: BOOKING_HAS_DO_VIEW_ERROR,
        bookingHasDOViewError: bool
    }
}

const bookingHasDOViewLoading = (bool) => {
    return {
        type: BOOKING_HAS_DO_VIEW_LOADING,
        bookingHasDOViewLoading: bool
    }
}

const bookingHasDOViewSuccess = (bool, bookinghdo) => {
    return {
        type: BOOKING_HAS_DO_VIEW_SUCCESS,
        bookingHasDOViewSuccess: bool,
        bookinghdo
    }
}

const bookingHasDOUpdateError = (bool) => {
    return {
        type: BOOKING_HAS_DO_UPDATE_ERROR,
        bookingHasDOUpdateError: bool
    }
}

const bookingHasDOUpdateLoading = (bool) => {
    return {
        type: BOOKING_HAS_DO_UPDATE_LOADING,
        bookingHasDOUpdateLoading: bool
    }
}

const bookingHasDOUpdateInvalid = (bool, invalid) => {
    return {
        type: BOOKING_HAS_DO_UPDATE_INVALID,
        bookingHasDOUpdateInvalid: bool,
        invalid
    }
}

const bookingHasDOUpdateSuccess = (bool) => {
    return {
        type: BOOKING_HAS_DO_UPDATE_SUCCESS,
        bookingHasDOUpdateSuccess: bool
    }
}

export function bookingHasDOView(data) {
    return (dispatch) => {
        dispatch(bookingHasDOViewError(false))
        dispatch(bookingHasDOViewSuccess(false, null))
        dispatch(bookingHasDOViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + BOOKING_HAS_DO_VIEW_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingHasDOViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((booking) => {
                dispatch(bookingHasDOViewSuccess(true, booking))
            })
            .catch((err) => {
                dispatch(bookingHasDOViewError(true))
            })
    }
}

export function bookingHasDOUpdate(data) {
    return (dispatch) => {
        dispatch(bookingHasDOUpdateError(false))
        dispatch(bookingHasDOUpdateSuccess(false, null))
        dispatch(bookingHasDOUpdateInvalid(false, null))
        dispatch(bookingHasDOUpdateLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData()
        for (var key in data) {
            formData.append(key, data[key])
        }

        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`
                },
                body: formData
            }

        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + BOOKING_HAS_DO_UPDATE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(bookingHasDOUpdateLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    dispatch(bookingHasDOUpdateSuccess(true))
                } else {
                    dispatch(bookingHasDOUpdateInvalid(true, data))
                }
            })
            .catch((err) => {
                dispatch(bookingHasDOUpdateError(true))
            })
    }
}