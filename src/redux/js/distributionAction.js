import {doLogout} from 'actions/authAction';
import {
  BASE_URL,
  DISTRIBUTION_URL,
  DISTRIBUTION_GET_ERROR,
  DISTRIBUTION_GET_LOADING,
  DISTRIBUTION_GET_SUCCESS,
  DISTRIBUTION_VIEW_ERROR,
  DISTRIBUTION_VIEW_LOADING,
  DISTRIBUTION_VIEW_SUCCESS,
  DISTRIBUTION_ADD_ERROR,
  DISTRIBUTION_ADD_LOADING,
  DISTRIBUTION_ADD_SUCCESS,
} from './constant';

const distributionGetError = (bool) => {
  return {
    type: DISTRIBUTION_GET_ERROR,
    distributionGetError: bool,
  };
};

const distributionGetLoading = (bool) => {
  return {
    type: DISTRIBUTION_GET_LOADING,
    distributionGetLoading: bool,
  };
};

const distributionGetSuccess = (bool, distributions) => {
  return {
    type: DISTRIBUTION_GET_SUCCESS,
    distributionGetSuccess: bool,
    distributions,
  };
};

const distributionViewError = (bool) => {
  return {
    type: DISTRIBUTION_VIEW_ERROR,
    distributionViewError: bool,
  };
};

const distributionViewLoading = (bool) => {
  return {
    type: DISTRIBUTION_VIEW_LOADING,
    distributionViewLoading: bool,
  };
};

const distributionViewSuccess = (bool, distribution) => {
  return {
    type: DISTRIBUTION_VIEW_SUCCESS,
    distributionViewSuccess: bool,
    distribution,
  };
};

const distributionAddError = (bool) => {
  return {
    type: DISTRIBUTION_ADD_ERROR,
    distributionAddError: bool,
  };
};

const distributionAddLoading = (bool) => {
  return {
    type: DISTRIBUTION_ADD_LOADING,
    distributionAddLoading: bool,
  };
};

const distributionAddSuccess = (bool, distribution) => {
  return {
    type: DISTRIBUTION_ADD_SUCCESS,
    distributionAddSuccess: bool,
    distribution,
  };
};

export function distributionFetchAll() {
  return (dispatch) => {
    dispatch(distributionAddSuccess(false, null));
    dispatch(distributionGetSuccess(false, null));
    dispatch(distributionGetError(false));
    dispatch(distributionGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DISTRIBUTION_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(distributionGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(distributionGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(distributionGetError(true));
      });
  };
}

export function distributionFetchSub(item) {
  return (dispatch) => {
    dispatch(distributionAddSuccess(false, null));
    dispatch(distributionGetSuccess(false, null));
    dispatch(distributionGetError(false));
    dispatch(distributionGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DISTRIBUTION_URL + '/get_all', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(distributionGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(distributionGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(distributionGetError(true));
      });
  };
}

export function distributionFetchReseller(data) {
  return (dispatch) => {
    dispatch(distributionGetSuccess(false, null));
    dispatch(distributionGetError(false));
    dispatch(distributionGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DISTRIBUTION_URL + '/reseller', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(distributionGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(distributionGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(distributionGetError(true));
      });
  };
}

export function distributionAdd(data) {
  return (dispatch) => {
    dispatch(distributionAddError(false));
    dispatch(distributionAddSuccess(false, null));
    dispatch(distributionAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};

    config = {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + DISTRIBUTION_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(distributionAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((distribution) => {
        dispatch(distributionAddSuccess(true, distribution));
      })
      .catch((err) => {
        dispatch(distributionAddError(true));
      });
  };
}

export function distributionUpdate(data) {
  return (dispatch) => {
    dispatch(distributionAddError(false));
    dispatch(distributionAddSuccess(false, null));
    dispatch(distributionAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};

    config = {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + DISTRIBUTION_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(distributionAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((distribution) => {
        dispatch(distributionAddSuccess(true, distribution));
      })
      .catch((err) => {
        dispatch(distributionAddError(true));
      });
  };
}

export function distributionFetchOne(id) {
  return (dispatch) => {
    dispatch(distributionAddSuccess(false, null));
    dispatch(distributionViewError(false));
    dispatch(distributionViewSuccess(false, null));
    dispatch(distributionViewLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DISTRIBUTION_URL + '/' + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(distributionViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(distributionViewSuccess(true, data));
      })
      .catch((err) => {
        dispatch(distributionViewError(true));
      });
  };
}
