import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    PORT_URL,

    PORT_GET_ERROR,
    PORT_GET_LOADING,
    PORT_GET_SUCCESS,

    PORT_VIEW_ERROR,
    PORT_VIEW_LOADING,
    PORT_VIEW_SUCCESS,

    PORT_ADD_ERROR,
    PORT_ADD_LOADING,
    PORT_ADD_SUCCESS,

    PORT_DELETE_ERROR,
    PORT_DELETE_LOADING,
    PORT_DELETE_SUCCESS
} from './constant'

const portGetError = (bool) => {
    return {
        type: PORT_GET_ERROR,
        portGetError: bool
    }
}

const portGetLoading = (bool) => {
    return {
        type: PORT_GET_LOADING,
        portGetLoading: bool
    }
}

const portGetSuccess = (bool, ports) => {
    return {
        type: PORT_GET_SUCCESS,
        portGetSuccess: bool,
        ports
    }
}

const portViewError = (bool) => {
    return {
        type: PORT_VIEW_ERROR,
        portViewError: bool
    }
}

const portViewLoading = (bool) => {
    return {
        type: PORT_VIEW_LOADING,
        portViewLoading: bool
    }
}

const portViewSuccess = (bool, port) => {
    return {
        type: PORT_VIEW_SUCCESS,
        portViewSuccess: bool,
        port
    }
}

const portAddError = (bool) => {
    return {
        type: PORT_ADD_ERROR,
        portAddError: bool
    }
}

const portAddLoading = (bool) => {
    return {
        type: PORT_ADD_LOADING,
        portAddLoading: bool
    }
}

const portAddSuccess = (bool, port) => {
    return {
        type: PORT_ADD_SUCCESS,
        portAddSuccess: bool,
        port
    }
}

const portDeleteError = (bool) => {
    return {
        type: PORT_DELETE_ERROR,
        portDeleteError: bool
    }
}

const portDeleteLoading = (bool) => {
    return {
        type: PORT_DELETE_LOADING,
        portDeleteLoading: bool
    }
}

const portDeleteSuccess = (bool, port) => {
    return {
        type: PORT_DELETE_SUCCESS,
        portDeleteSuccess: bool,
        port
    }
}

export function portFetchAll() {
    return (dispatch) => {
        dispatch(portGetSuccess(false, null))
        dispatch(portGetError(false))
        dispatch(portGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PORT_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(portGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(portGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(portGetError(true))
            })
    }
}

export function portFetchSub(supplier_id) {
    return (dispatch) => {
        dispatch(portGetSuccess(false, null))
        dispatch(portGetError(false))
        dispatch(portGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PORT_URL + '/' + supplier_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(portGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(portGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(portGetError(true))
            })
    }
}

export function portFetchPage(params) {
    return (dispatch) => {
        dispatch(portGetSuccess(false, null))
        dispatch(portGetError(false))
        dispatch(portGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PORT_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(portGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(portGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(portGetError(true))
            })
    }
}

export function portAdd(data) {
    return (dispatch) => {
        dispatch(portAddError(false))
        dispatch(portAddSuccess(false, null))
        dispatch(portAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
            },
            body: formData
        }
        fetch(BASE_URL + PORT_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(portAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((port) => {
                dispatch(portAddSuccess(true, port))
            })
            .catch((err) => {
                dispatch(portAddError(true))
            })
    }
}

export function portFetchOne(id) {
    return (dispatch) => {
        dispatch(portViewError(false))
        dispatch(portViewSuccess(false, null))
        dispatch(portViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PORT_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(portViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(portViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(portViewError(true))
            })
    }
}

export function portUpdate(data) {
    return (dispatch) => {
        dispatch(portAddError(false))
        dispatch(portAddSuccess(false, null))
        dispatch(portAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PORT_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(portAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((port) => {
                if (port.status == 200) {
                    dispatch(portAddSuccess(true, port))
                } else {
                    dispatch(portAddError(true))
                }
            })
            .catch((err) => {
                dispatch(portAddError(true))
            })
    }
}

export function portDelete(id) {
    return (dispatch) => {
        dispatch(portViewSuccess(false, null))
        dispatch(portDeleteError(false))
        dispatch(portDeleteSuccess(false, null))
        dispatch(portDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PORT_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(portDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(portDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(portDeleteError(true))
            })
    }
}