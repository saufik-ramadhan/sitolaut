import {doLogout} from 'actions/authAction';
import {
  BASE_URL,
  MANIFEST_URL,
  MANIFEST_GET_LOADING,
  MANIFEST_GET_ERROR,
  MANIFEST_GET_SUCCESS,
  MANIFEST_VIEW_LOADING,
  MANIFEST_VIEW_ERROR,
  MANIFEST_VIEW_SUCCESS,
} from './constant';

const manifestGetLoading = (bool) => {
  return {
    type: MANIFEST_GET_LOADING,
    manifestGetLoading: bool,
  };
};

const manifestGetError = (bool) => {
  return {
    type: MANIFEST_GET_ERROR,
    manifestGetError: bool,
  };
};

const manifestGetSuccess = (bool, manifests) => {
  return {
    type: MANIFEST_GET_SUCCESS,
    manifestGetSuccess: bool,
    manifests,
  };
};

const manifestViewLoading = (bool) => {
  return {
    type: MANIFEST_VIEW_LOADING,
    manifestViewLoading: bool,
  };
};

const manifestViewError = (bool) => {
  return {
    type: MANIFEST_VIEW_ERROR,
    manifestViewError: bool,
  };
};

const manifestViewSuccess = (bool, manifest) => {
  return {
    type: MANIFEST_VIEW_SUCCESS,
    manifestViewSuccess: bool,
    manifest,
  };
};

export function manifestFetchAll(data) {
  return (dispatch) => {
    dispatch(manifestGetLoading(true));
    dispatch(manifestGetError(false));
    dispatch(manifestGetSuccess(false, null));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }

    fetch(BASE_URL + MANIFEST_URL + '/page', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(manifestGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(manifestGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(manifestGetError(true));
      });
  };
}

export function manifestFetchOne(id) {
  return (dispatch) => {
    dispatch(manifestViewLoading(true));
    dispatch(manifestViewError(false));
    dispatch(manifestViewSuccess(false, null));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error('Authentication error');
    }

    fetch(BASE_URL + MANIFEST_URL + '/view', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(manifestViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(manifestViewSuccess(true, data));
      })
      .catch((err) => {
        dispatch(manifestViewError(true));
      });
  };
}
