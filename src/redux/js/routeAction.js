import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    ROUTE_URL,

    ROUTE_GET_ERROR,
    ROUTE_GET_LOADING,
    ROUTE_GET_SUCCESS,

    ROUTE_VIEW_ERROR,
    ROUTE_VIEW_LOADING,
    ROUTE_VIEW_SUCCESS,

    ROUTE_ADD_ERROR,
    ROUTE_ADD_LOADING,
    ROUTE_ADD_SUCCESS,

    ROUTE_DELETE_ERROR,
    ROUTE_DELETE_LOADING,
    ROUTE_DELETE_SUCCESS
} from './constant'

const routeGetError = (bool) => {
    return {
        type: ROUTE_GET_ERROR,
        routeGetError: bool
    }
}

const routeGetLoading = (bool) => {
    return {
        type: ROUTE_GET_LOADING,
        routeGetLoading: bool
    }
}

const routeGetSuccess = (bool, routes) => {
    return {
        type: ROUTE_GET_SUCCESS,
        routeGetSuccess: bool,
        routes
    }
}

const routeViewError = (bool) => {
    return {
        type: ROUTE_VIEW_ERROR,
        routeViewError: bool
    }
}

const routeViewLoading = (bool) => {
    return {
        type: ROUTE_VIEW_LOADING,
        routeViewLoading: bool
    }
}

const routeViewSuccess = (bool, route) => {
    return {
        type: ROUTE_VIEW_SUCCESS,
        routeViewSuccess: bool,
        route
    }
}

const routeAddError = (bool) => {
    return {
        type: ROUTE_ADD_ERROR,
        routeAddError: bool
    }
}

const routeAddLoading = (bool) => {
    return {
        type: ROUTE_ADD_LOADING,
        routeAddLoading: bool
    }
}

const routeAddSuccess = (bool, route) => {
    return {
        type: ROUTE_ADD_SUCCESS,
        routeAddSuccess: bool,
        route
    }
}

const routeDeleteError = (bool) => {
    return {
        type: ROUTE_DELETE_ERROR,
        routeDeleteError: bool
    }
}

const routeDeleteLoading = (bool) => {
    return {
        type: ROUTE_DELETE_LOADING,
        routeDeleteLoading: bool
    }
}

const routeDeleteSuccess = (bool, route) => {
    return {
        type: ROUTE_DELETE_SUCCESS,
        routeDeleteSuccess: bool,
        route
    }
}

export function routeFetchAll() {
    return (dispatch) => {
        dispatch(routeAddSuccess(false, null))
        dispatch(routeGetSuccess(false, null))
        dispatch(routeGetError(false))
        dispatch(routeGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROUTE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(routeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(routeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(routeGetError(true))
            })
    }
}

export function routeFetchSub(path) {
    return (dispatch) => {
        dispatch(routeAddSuccess(false, null))
        dispatch(routeGetSuccess(false, null))
        dispatch(routeGetError(false))
        dispatch(routeGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROUTE_URL + '/' + path, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(routeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(routeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(routeGetError(true))
            })
    }
}

export function routeReset() {
    return (dispatch) => {
        dispatch(routeGetSuccess(false, null))
    }
}

export function routeFetchPage(params) {
    return (dispatch) => {
        dispatch(routeAddSuccess(false, null))
        dispatch(routeGetSuccess(false, null))
        dispatch(routeGetError(false))
        dispatch(routeGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROUTE_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(routeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(routeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(routeGetError(true))
            })
    }
}

export function routeAdd(data) {

    return (dispatch) => {
        dispatch(routeAddLoading(true))
        dispatch(routeAddError(false))
        dispatch(routeAddSuccess(false, null))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + ROUTE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(routeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((route) => {
                if (route.success) {
                    dispatch(routeAddSuccess(true, route))
                } else {
                    dispatch(routeAddError(true))
                }

            })
            .catch((err) => {
                dispatch(routeAddError(true))
            })
    }
}

export function routeFetchOne(id) {
    return (dispatch) => {
        dispatch(routeAddSuccess(false, null))
        dispatch(routeViewError(false))
        dispatch(routeViewSuccess(false, null))
        dispatch(routeViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROUTE_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(routeViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(routeViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(routeViewError(true))
            })
    }
}

export function routeUpdate(data) {
    return (dispatch) => {
        dispatch(routeAddError(false))
        dispatch(routeAddSuccess(false, null))
        dispatch(routeAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROUTE_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(routeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((route) => {

                if (route.success) {
                    dispatch(routeAddSuccess(true, route))
                } else {
                    dispatch(routeAddError(true))
                }
            })
            .catch((err) => {
                dispatch(routeAddError(true))
            })
    }
}

export function routeDelete(id) {
    return (dispatch) => {
        dispatch(routeViewSuccess(false, null))
        dispatch(routeDeleteError(false))
        dispatch(routeDeleteSuccess(false, null))
        dispatch(routeDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROUTE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(routeDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(routeDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(routeDeleteError(true))
            })
    }
}