import {doLogout} from 'actions/authAction';
import {
  BASE_URL,
  PRICE_URL,
  PRICE_GET_ERROR,
  PRICE_GET_LOADING,
  PRICE_GET_SUCCESS,
  PRICE_VIEW_ERROR,
  PRICE_VIEW_LOADING,
  PRICE_VIEW_SUCCESS,
  PRICE_ADD_ERROR,
  PRICE_ADD_LOADING,
  PRICE_ADD_SUCCESS,
  PRICE_DELETE_ERROR,
  PRICE_DELETE_LOADING,
  PRICE_DELETE_SUCCESS,
} from './constant';

const priceGetError = (bool) => {
  return {
    type: PRICE_GET_ERROR,
    priceGetError: bool,
  };
};

const priceGetLoading = (bool) => {
  return {
    type: PRICE_GET_LOADING,
    priceGetLoading: bool,
  };
};

const priceGetSuccess = (bool, prices) => {
  return {
    type: PRICE_GET_SUCCESS,
    priceGetSuccess: bool,
    prices,
  };
};

const priceViewError = (bool) => {
  return {
    type: PRICE_VIEW_ERROR,
    priceViewError: bool,
  };
};

const priceViewLoading = (bool) => {
  return {
    type: PRICE_VIEW_LOADING,
    priceViewLoading: bool,
  };
};

const priceViewSuccess = (bool, price) => {
  return {
    type: PRICE_VIEW_SUCCESS,
    priceViewSuccess: bool,
    price,
  };
};

const priceAddError = (bool) => {
  return {
    type: PRICE_ADD_ERROR,
    priceAddError: bool,
  };
};

const priceAddLoading = (bool) => {
  return {
    type: PRICE_ADD_LOADING,
    priceAddLoading: bool,
  };
};

const priceAddSuccess = (bool, price) => {
  return {
    type: PRICE_ADD_SUCCESS,
    priceAddSuccess: bool,
    price,
  };
};

const priceDeleteError = (bool) => {
  return {
    type: PRICE_DELETE_ERROR,
    priceDeleteError: bool,
  };
};

const priceDeleteLoading = (bool) => {
  return {
    type: PRICE_DELETE_LOADING,
    priceDeleteLoading: bool,
  };
};

const priceDeleteSuccess = (bool, price) => {
  return {
    type: PRICE_DELETE_SUCCESS,
    priceDeleteSuccess: bool,
    price,
  };
};

export function priceFetchAll(tokenAuth) {
  return (dispatch) => {
    dispatch(priceAddSuccess(false, null));
    dispatch(priceGetSuccess(false, null));
    dispatch(priceGetError(false));
    dispatch(priceGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(priceGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(priceGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(priceGetError(true));
      });
  };
}

export function priceFetchSub(path) {
  return (dispatch) => {
    dispatch(priceAddSuccess(false, null));
    dispatch(priceGetSuccess(false, null));
    dispatch(priceGetError(false));
    dispatch(priceGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + PRICE_URL + '/' + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(priceGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(priceGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(priceGetError(true));
      });
  };
}

export function priceFetchPage(params) {
  return (dispatch) => {
    dispatch(priceAddSuccess(false, null));
    dispatch(priceViewSuccess(false, null));
    dispatch(priceGetSuccess(false, null));
    dispatch(priceGetError(false));
    dispatch(priceGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + PRICE_URL + '/page', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(priceGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(priceGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(priceGetError(true));
      });
  };
}

export function priceAdd(data) {
  return (dispatch) => {
    dispatch(priceAddError(false));
    dispatch(priceViewSuccess(false, null));
    dispatch(priceAddSuccess(false, null));
    dispatch(priceAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(priceAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((price) => {
        dispatch(priceAddSuccess(true, price));
      })
      .catch((err) => {
        dispatch(priceAddError(true));
      });
  };
}

export function priceFetchOne(id) {
  return (dispatch) => {
    dispatch(priceViewError(false));
    dispatch(priceViewSuccess(false, null));
    dispatch(priceViewLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({id: id}),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + PRICE_URL + '/view', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(priceViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(priceViewSuccess(true, data));
      })
      .catch((err) => {
        dispatch(priceViewError(true));
      });
  };
}

export function priceUpdate(data) {
  return (dispatch) => {
    dispatch(priceViewSuccess(false, null));
    dispatch(priceAddError(false));
    dispatch(priceAddSuccess(false, null));
    dispatch(priceAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(priceAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((price) => {
        if (price.success) {
          dispatch(priceAddSuccess(true, price));
        } else {
          dispatch(priceAddError(true));
        }
      })
      .catch((err) => {
        dispatch(priceAddError(true));
      });
  };
}

export function priceDelete(id) {
  return (dispatch) => {
    dispatch(priceViewSuccess(false, null));
    dispatch(priceDeleteError(false));
    dispatch(priceDeleteSuccess(false, null));
    dispatch(priceDeleteLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({id: id}),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(priceDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(priceDeleteSuccess(true, data));
      })
      .catch((err) => {
        dispatch(priceDeleteError(true));
      });
  };
}
