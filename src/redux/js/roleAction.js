import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    ROLE_URL,

    ROLE_GET_ERROR,
    ROLE_GET_LOADING,
    ROLE_GET_SUCCESS,

    ROLE_VIEW_ERROR,
    ROLE_VIEW_LOADING,
    ROLE_VIEW_SUCCESS,

    ROLE_ADD_ERROR,
    ROLE_ADD_LOADING,
    ROLE_ADD_SUCCESS,

    ROLE_DELETE_ERROR,
    ROLE_DELETE_LOADING,
    ROLE_DELETE_SUCCESS,
    ROLE_EDIT_ERROR,
    ROLE_EDIT_LOADING,
    ROLE_EDIT_SUCCESS
} from './constant'

const roleGetError = (bool) => {
    return {
        type: ROLE_GET_ERROR,
        roleGetError: bool
    }
}

const roleGetLoading = (bool) => {
    return {
        type: ROLE_GET_LOADING,
        roleGetLoading: bool
    }
}

const roleGetSuccess = (bool, roles) => {
    return {
        type: ROLE_GET_SUCCESS,
        roleGetSuccess: bool,
        roles
    }
}

const roleViewError = (bool) => {
    return {
        type: ROLE_VIEW_ERROR,
        roleViewError: bool
    }
}

const roleViewLoading = (bool) => {
    return {
        type: ROLE_VIEW_LOADING,
        roleViewLoading: bool
    }
}

const roleViewSuccess = (bool, role) => {
    return {
        type: ROLE_VIEW_SUCCESS,
        roleViewSuccess: bool,
        role
    }
}

const roleAddError = (bool) => {
    return {
        type: ROLE_ADD_ERROR,
        roleAddError: bool
    }
}

const roleAddLoading = (bool) => {
    return {
        type: ROLE_ADD_LOADING,
        roleAddLoading: bool
    }
}

const roleAddSuccess = (bool, role) => {
    return {
        type: ROLE_ADD_SUCCESS,
        roleAddSuccess: bool,
        role
    }
}

const roleDeleteError = (bool) => {
    return {
        type: ROLE_DELETE_ERROR,
        roleDeleteError: bool
    }
}

const roleDeleteLoading = (bool) => {
    return {
        type: ROLE_DELETE_LOADING,
        roleDeleteLoading: bool
    }
}

const roleDeleteSuccess = (bool, role) => {
    return {
        type: ROLE_DELETE_SUCCESS,
        roleDeleteSuccess: bool,
        role
    }
}

const roleEditError = (bool) => {
    return {
        type: ROLE_EDIT_ERROR,
        roleEditError: bool
    }
}

const roleEditLoading = (bool) => {
    return {
        type: ROLE_EDIT_LOADING,
        roleEditLoading: bool
    }
}

const roleEditSuccess = (bool, role) => {
    return {
        type: ROLE_EDIT_SUCCESS,
        roleEditSuccess: bool,
        role
    }
}

export function roleFetchAll(data) {
    return (dispatch) => {
        dispatch(roleGetSuccess(false, null))
        dispatch(roleGetError(false))
        dispatch(roleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        // if (token) {
        config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        // } else {
        //   throw Error("Authentication error")
        // }
        fetch(BASE_URL + ROLE_URL + '/all', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(roleGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(roleGetError(true))
            })
    }
}

export function roleFetchSub(supplier_id) {
    return (dispatch) => {
        dispatch(roleGetSuccess(false, null))
        dispatch(roleGetError(false))
        dispatch(roleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROLE_URL + '/' + supplier_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(roleGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(roleGetError(true))
            })
    }
}

export function roleFetchPage(params) {
    return (dispatch) => {
        dispatch(roleGetSuccess(false, null))
        dispatch(roleGetError(false))
        dispatch(roleGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROLE_URL + '/comprehensive', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(roleGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(roleGetError(true))
            })
    }
}

export function roleAdd(data) {
    return (dispatch) => {
        dispatch(roleAddError(false))
        dispatch(roleAddSuccess(false, null))
        dispatch(roleAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + ROLE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((role) => {
                dispatch(roleAddSuccess(true, role))
            })
            .catch((err) => {
                dispatch(roleAddError(true))
            })
    }
}

export function roleEdit(data) {
    return (dispatch) => {
        dispatch(roleEditError(false))
        dispatch(roleEditSuccess(false, null))
        dispatch(roleEditLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        let formData = new FormData();
        for (var key in data) {
            formData.append(key, data[key])
        }

        config = {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + ROLE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleEditLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((role) => {
                dispatch(roleEditSuccess(true, role))
            })
            .catch((err) => {
                dispatch(roleEditError(true))
            })
    }
}

export function roleFetchOne(data) {
    return (dispatch) => {
        dispatch(roleViewError(false))
        dispatch(roleViewSuccess(false, null))
        dispatch(roleViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROLE_URL + '/view', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(roleViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(roleViewError(true))
            })
    }
}

export function roleUpdate(data) {
    return (dispatch) => {
        dispatch(roleAddError(false))
        dispatch(roleAddSuccess(false, null))
        dispatch(roleAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROLE_URL, config)
            .then((response) => {

                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((role) => {
                if (role.status == 200) {
                    dispatch(roleAddSuccess(true, role))
                } else {
                    dispatch(roleAddError(true))
                }
            })
            .catch((err) => {
                dispatch(roleAddError(true))
            })
    }
}

export function roleDelete(id) {
    return (dispatch) => {
        dispatch(roleViewSuccess(false, null))
        dispatch(roleDeleteError(false))
        dispatch(roleDeleteSuccess(false, null))
        dispatch(roleDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    'id': id
                })
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + ROLE_URL, config)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(roleDeleteLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(roleDeleteSuccess(true, data))
            })
            .catch((err) => {
                dispatch(roleDeleteError(true))
            })
    }
}