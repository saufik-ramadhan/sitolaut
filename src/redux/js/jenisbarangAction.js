import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    JENISBARANG_URL,

    JENISBARANG_GET_ERROR,
    JENISBARANG_GET_LOADING,
    JENISBARANG_GET_SUCCESS,

    JENISBARANG_VIEW_LOADING,
    JENISBARANG_VIEW_ERROR,
    JENISBARANG_VIEW_SUCCESS,

    JENISBARANG_ADD_LOADING,
    JENISBARANG_ADD_ERROR,
    JENISBARANG_ADD_SUCCESS,

    JENISBARANG_EDIT_LOADING,
    JENISBARANG_EDIT_ERROR,
    JENISBARANG_EDIT_SUCCESS,

    JENISBARANG_DELETE_LOADING,
    JENISBARANG_DELETE_ERROR,
    JENISBARANG_DELETE_SUCCESS,
} from './constant'

const jenisbarangGetError = (bool) => {
    return {
        type: JENISBARANG_GET_ERROR,
        jenisbarangGetError: bool
    }
}

const jenisbarangGetLoading = (bool) => {
    return {
        type: JENISBARANG_GET_LOADING,
        jenisbarangGetLoading: bool
    }
}

const jenisbarangGetSuccess = (bool, jenisbarangs) => {
    return {
        type: JENISBARANG_GET_SUCCESS,
        jenisbarangGetSuccess: bool,
        jenisbarangs
    }
}

export function jenisbarangFetchAll() {
    return (dispatch) => {
        dispatch(jenisbarangGetSuccess(false, null))
        dispatch(jenisbarangGetError(false))
        dispatch(jenisbarangGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JENISBARANG_URL, config).then((response) => {
            if (response.status === 401) {
                dispatch(doLogout())
            }

            if (!response.ok) {
                throw Error(response.statusText)
            }

            dispatch(jenisbarangGetLoading(false))

            return response
        }).then((response) => response.json()).then((data) => {
            dispatch(jenisbarangGetSuccess(true, data))
        }).catch((err) => {
            dispatch(jenisbarangGetLoading(false))
            dispatch(jenisbarangGetError(true))
        })
    }
}

export function jenisbarangFetchPage(data) {
    return (dispatch) => {
        dispatch(jenisbarangGetSuccess(false, null))
        dispatch(jenisbarangGetError(false))
        dispatch(jenisbarangGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JENISBARANG_URL + '/page', config).then((response) => {
            if (response.status === 401) {
                dispatch(doLogout())
            }

            if (!response.ok) {
                throw Error(response.statusText)
            }

            dispatch(jenisbarangGetLoading(false))

            return response
        }).then((response) => response.json()).then((data) => {
            dispatch(jenisbarangGetSuccess(true, data))
        }).catch((err) => {
            dispatch(jenisbarangGetLoading(false))
            dispatch(jenisbarangGetError(true))
        })
    }
}

const jenisbarangViewError = (bool) => {
    return {
        type: JENISBARANG_VIEW_ERROR,
        jenisbarangViewError: bool
    }
}

const jenisbarangViewLoading = (bool) => {
    return {
        type: JENISBARANG_VIEW_LOADING,
        jenisbarangViewLoading: bool
    }
}

export const jenisbarangViewSuccess = (bool, jenisbarang) => {
    return {
        type: JENISBARANG_VIEW_SUCCESS,
        jenisbarangViewSuccess: bool,
        jenisbarang
    }
}

export function jenisbarangFetchOne(id) {
    return (dispatch) => {
        dispatch(jenisbarangViewSuccess(false, null))
        dispatch(jenisbarangViewError(false))
        dispatch(jenisbarangViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JENISBARANG_URL + '/' + id, config).then((response) => {
            if (response.status === 401) {
                dispatch(doLogout())
            }

            if (!response.ok) {
                throw Error(response.statusText)
            }

            dispatch(jenisbarangViewLoading(false))

            return response
        }).then((response) => response.json()).then((data) => {
            dispatch(jenisbarangViewSuccess(true, data))
        }).catch((err) => {
            dispatch(jenisbarangViewLoading(false))
            dispatch(jenisbarangViewError(true))
        })
    }
}

const jenisbarangAddError = (bool) => {
    return {
        type: JENISBARANG_ADD_ERROR,
        jenisbarangAddError: bool
    }
}

const jenisbarangAddLoading = (bool) => {
    return {
        type: JENISBARANG_ADD_LOADING,
        jenisbarangAddLoading: bool
    }
}

export const jenisbarangAddSuccess = (bool) => {
    return {
        type: JENISBARANG_ADD_SUCCESS,
        jenisbarangAddSuccess: bool
    }
}

export function jenisbarangAdd(data) {
    return (dispatch) => {
        dispatch(jenisbarangAddSuccess(false))
        dispatch(jenisbarangAddError(false))
        dispatch(jenisbarangAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JENISBARANG_URL, config).then((response) => {
            if (response.status === 401) {
                dispatch(doLogout())
            }

            if (!response.ok) {
                throw Error(response.statusText)
            }

            dispatch(jenisbarangAddLoading(false))

            return response
        }).then((response) => response.json()).then(() => {
            dispatch(jenisbarangAddSuccess(true))
        }).catch((err) => {
            dispatch(jenisbarangAddLoading(false))
            dispatch(jenisbarangAddError(true))
        })
    }
}

const jenisbarangEditError = (bool) => {
    return {
        type: JENISBARANG_EDIT_ERROR,
        jenisbarangEditError: bool
    }
}

const jenisbarangEditLoading = (bool) => {
    return {
        type: JENISBARANG_EDIT_LOADING,
        jenisbarangEditLoading: bool
    }
}

export const jenisbarangEditSuccess = (bool) => {
    return {
        type: JENISBARANG_EDIT_SUCCESS,
        jenisbarangEditSuccess: bool
    }
}

export function jenisbarangEdit(data) {
    return (dispatch) => {
        dispatch(jenisbarangEditSuccess(false))
        dispatch(jenisbarangEditError(false))
        dispatch(jenisbarangEditLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'PUT',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JENISBARANG_URL, config).then((response) => {
            if (response.status === 401) {
                dispatch(doLogout())
            }

            if (!response.ok) {
                throw Error(response.statusText)
            }

            dispatch(jenisbarangEditLoading(false))

            return response
        }).then((response) => response.json()).then(() => {
            dispatch(jenisbarangEditSuccess(true))
        }).catch((err) => {
            dispatch(jenisbarangEditLoading(false))
            dispatch(jenisbarangEditError(true))
        })
    }
}

const jenisbarangDeleteError = (bool) => {
    return {
        type: JENISBARANG_DELETE_ERROR,
        jenisbarangDeleteError: bool
    }
}

const jenisbarangDeleteLoading = (bool) => {
    return {
        type: JENISBARANG_DELETE_LOADING,
        jenisbarangDeleteLoading: bool
    }
}

export const jenisbarangDeleteSuccess = (bool) => {
    return {
        type: JENISBARANG_DELETE_SUCCESS,
        jenisbarangDeleteSuccess: bool
    }
}

export function jenisbarangDelete(data) {
    return (dispatch) => {
        dispatch(jenisbarangDeleteSuccess(false))
        dispatch(jenisbarangDeleteError(false))
        dispatch(jenisbarangDeleteLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            }
        } else {
            throw Error("Authentication error")
        }

        fetch(BASE_URL + JENISBARANG_URL, config).then((response) => {
            if (response.status === 401) {
                dispatch(doLogout())
            }

            if (!response.ok) {
                throw Error(response.statusText)
            }

            dispatch(jenisbarangDeleteLoading(false))

            return response
        }).then((response) => response.json()).then(() => {
            dispatch(jenisbarangDeleteSuccess(true))
        }).catch((err) => {
            dispatch(jenisbarangDeleteLoading(false))
            dispatch(jenisbarangDeleteError(true))
        })
    }
}