import {doLogout} from 'actions/authAction';
import {
  BASE_URL,
  DETAILPO_URL,
  DETAILPO_GET_ERROR,
  DETAILPO_GET_LOADING,
  DETAILPO_GET_SUCCESS,
  DETAILPO_VIEW_ERROR,
  DETAILPO_VIEW_LOADING,
  DETAILPO_VIEW_SUCCESS,
  DETAILPO_ADD_ERROR,
  DETAILPO_ADD_LOADING,
  DETAILPO_ADD_SUCCESS,
  DETAILPO_DELETE_ERROR,
  DETAILPO_DELETE_LOADING,
  DETAILPO_DELETE_SUCCESS,
} from './constant';

const detailpoGetError = (bool) => {
  return {
    type: DETAILPO_GET_ERROR,
    detailpoGetError: bool,
  };
};

const detailpoGetLoading = (bool) => {
  return {
    type: DETAILPO_GET_LOADING,
    detailpoGetLoading: bool,
  };
};

const detailpoGetSuccess = (bool, detailpos) => {
  return {
    type: DETAILPO_GET_SUCCESS,
    detailpoGetSuccess: bool,
    detailpos,
  };
};

const detailpoViewError = (bool) => {
  return {
    type: DETAILPO_VIEW_ERROR,
    detailpoViewError: bool,
  };
};

const detailpoViewLoading = (bool) => {
  return {
    type: DETAILPO_VIEW_LOADING,
    detailpoViewLoading: bool,
  };
};

const detailpoViewSuccess = (bool, detailpo) => {
  return {
    type: DETAILPO_VIEW_SUCCESS,
    detailpoViewSuccess: bool,
    detailpo,
  };
};

const detailpoAddError = (bool) => {
  return {
    type: DETAILPO_ADD_ERROR,
    detailpoAddError: bool,
  };
};

const detailpoAddLoading = (bool) => {
  return {
    type: DETAILPO_ADD_LOADING,
    detailpoAddLoading: bool,
  };
};

const detailpoAddSuccess = (bool, detailpo) => {
  return {
    type: DETAILPO_ADD_SUCCESS,
    detailpoAddSuccess: bool,
    detailpo,
  };
};

const detailpoDeleteError = (bool) => {
  return {
    type: DETAILPO_DELETE_ERROR,
    detailpoDeleteError: bool,
  };
};

const detailpoDeleteLoading = (bool) => {
  return {
    type: DETAILPO_DELETE_LOADING,
    detailpoDeleteLoading: bool,
  };
};

const detailpoDeleteSuccess = (bool, detailpo) => {
  return {
    type: DETAILPO_DELETE_SUCCESS,
    detailpoDeleteSuccess: bool,
    detailpo,
  };
};

export function detailpoFetchAll() {
  return (dispatch) => {
    dispatch(detailpoGetSuccess(false, null));
    dispatch(detailpoGetError(false));
    dispatch(detailpoGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(detailpoGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(detailpoGetError(true));
      });
  };
}

export function detailpoFetchSub(path) {
  return (dispatch) => {
    dispatch(detailpoGetSuccess(false, null));
    dispatch(detailpoGetError(false));
    dispatch(detailpoGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL + '/' + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          let detailpo = buildGrandTotal(data);
          dispatch(detailpoGetSuccess(true, detailpo));
        } else {
          dispatch(detailpoGetError(true));
        }
      })
      .catch((err) => {
        dispatch(detailpoGetError(true));
      });
  };
}

export function detailpoFetchDistribution(item) {
  return (dispatch) => {
    dispatch(detailpoGetSuccess(false, null));
    dispatch(detailpoGetError(false));
    dispatch(detailpoGetLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL + '/distribution', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          let detailpo = buildGrandTotal(data);
          dispatch(detailpoGetSuccess(true, detailpo));
        } else {
          dispatch(detailpoGetError(true));
        }
      })
      .catch((err) => {
        dispatch(detailpoGetError(true));
      });
  };
}

export function buildGrandTotal(data) {
  let total = data.data;
  if (total.length > 0) {
    function grandTotal(item) {
      return Number(item.harga_total);
    }

    function sum(prev, next) {
      return prev + next;
    }

    let totalSum = total.map(grandTotal).reduce(sum);
    data.grandTotal = Number(totalSum);
  } else {
    data.grandTotal = 0;
  }

  return data;
}

export function detailpoAdd(data) {
  return (dispatch) => {
    dispatch(detailpoAddError(false));
    dispatch(detailpoAddSuccess(false, null));
    dispatch(detailpoAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((detailpo) => {
        dispatch(detailpoAddSuccess(true, detailpo));
      })
      .catch((err) => {
        dispatch(detailpoAddError(true));
      });
  };
}

export function detailpoUpdate(data) {
  return (dispatch) => {
    dispatch(detailpoAddError(false));
    dispatch(detailpoAddSuccess(false, null));
    dispatch(detailpoAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'PUT',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((detailpo) => {
        dispatch(detailpoAddSuccess(true, detailpo));
      })
      .catch((err) => {
        dispatch(detailpoAddError(true));
      });
  };
}

export function detailpoConfirm(data) {
  return (dispatch) => {
    dispatch(detailpoAddError(false));
    dispatch(detailpoAddSuccess(false, null));
    dispatch(detailpoAddLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL + '/approve', config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((detailpo) => {
        if (detailpo.status === '200 OK') {
          dispatch(detailpoAddSuccess(true, detailpo));
        } else {
          dispatch(detailpoAddError(true));
        }
      })
      .catch((err) => {
        dispatch(detailpoAddError(true));
      });
  };
}

export function detailpoFetchOne(id) {
  return (dispatch) => {
    dispatch(detailpoViewError(false));
    dispatch(detailpoViewSuccess(false, null));
    dispatch(detailpoViewLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL + '/' + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(detailpoViewSuccess(true, data));
      })
      .catch((err) => {
        dispatch(detailpoViewError(true));
      });
  };
}

export function detailpoDelete(id) {
  return (dispatch) => {
    dispatch(detailpoDeleteError(false));
    dispatch(detailpoDeleteSuccess(false, null));
    dispatch(detailpoGetSuccess(false, null));
    dispatch(detailpoDeleteLoading(true));

    let token = localStorage.getItem('tolaut@token') || null;
    let config = {};
    if (token) {
      config = {
        method: 'DELETE',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(id),
      };
    } else {
      throw Error('Authentication error');
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(detailpoDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(detailpoDeleteSuccess(true, data));
      })
      .catch((err) => {
        dispatch(detailpoDeleteError(true));
      });
  };
}
