import {
    doLogout
} from 'actions/authAction'
import {
    BASE_URL,
    PRICECONSIGNEE_URL,

    PRICECONSIGNEE_GET_ERROR,
    PRICECONSIGNEE_GET_LOADING,
    PRICECONSIGNEE_GET_SUCCESS,

    PRICECONSIGNEE_VIEW_ERROR,
    PRICECONSIGNEE_VIEW_LOADING,
    PRICECONSIGNEE_VIEW_SUCCESS,

    PRICECONSIGNEE_ADD_ERROR,
    PRICECONSIGNEE_ADD_LOADING,
    PRICECONSIGNEE_ADD_SUCCESS
} from './constant'

const priceconsigneeGetError = (bool) => {
    return {
        type: PRICECONSIGNEE_GET_ERROR,
        priceconsigneeGetError: bool
    }
}

const priceconsigneeGetLoading = (bool) => {
    return {
        type: PRICECONSIGNEE_GET_LOADING,
        priceconsigneeGetLoading: bool
    }
}

const priceconsigneeGetSuccess = (bool, priceconsignees) => {
    return {
        type: PRICECONSIGNEE_GET_SUCCESS,
        priceconsigneeGetSuccess: bool,
        priceconsignees
    }
}

const priceconsigneeViewError = (bool) => {
    return {
        type: PRICECONSIGNEE_VIEW_ERROR,
        priceconsigneeViewError: bool
    }
}

const priceconsigneeViewLoading = (bool) => {
    return {
        type: PRICECONSIGNEE_VIEW_LOADING,
        priceconsigneeViewLoading: bool
    }
}

const priceconsigneeViewSuccess = (bool, priceconsignee) => {
    return {
        type: PRICECONSIGNEE_VIEW_SUCCESS,
        priceconsigneeViewSuccess: bool,
        priceconsignee
    }
}

const priceconsigneeAddError = (bool) => {
    return {
        type: PRICECONSIGNEE_ADD_ERROR,
        priceconsigneeAddError: bool
    }
}

const priceconsigneeAddLoading = (bool) => {
    return {
        type: PRICECONSIGNEE_ADD_LOADING,
        priceconsigneeAddLoading: bool
    }
}

const priceconsigneeAddSuccess = (bool, priceconsigneeAdded) => {
    return {
        type: PRICECONSIGNEE_ADD_SUCCESS,
        priceconsigneeAddSuccess: bool,
        priceconsigneeAdded
    }
}

export function priceconsigneeFetchAll() {
    return (dispatch) => {
        dispatch(priceconsigneeGetSuccess(false, null))
        dispatch(priceconsigneeGetError(false))
        dispatch(priceconsigneeGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PRICECONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(priceconsigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(priceconsigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(priceconsigneeGetError(true))
            })
    }
}

export function priceconsigneeFetchSub(supplier_id) {
    return (dispatch) => {
        dispatch(priceconsigneeGetSuccess(false, null))
        dispatch(priceconsigneeGetError(false))
        dispatch(priceconsigneeGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PRICECONSIGNEE_URL + '/' + supplier_id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(priceconsigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(priceconsigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(priceconsigneeGetError(true))
            })
    }
}

export function priceconsigneeFetchPage(params) {
    return (dispatch) => {
        dispatch(priceconsigneeGetSuccess(false, null))
        dispatch(priceconsigneeGetError(false))
        dispatch(priceconsigneeGetLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PRICECONSIGNEE_URL + '/page', config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(priceconsigneeGetLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(priceconsigneeGetSuccess(true, data))
            })
            .catch((err) => {
                dispatch(priceconsigneeGetError(true))
            })
    }
}

export function priceconsigneeUpdate(data) {

    return (dispatch) => {
        dispatch(priceconsigneeAddError(false))
        dispatch(priceconsigneeAddSuccess(false, null))
        dispatch(priceconsigneeAddLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}

        config = {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        fetch(BASE_URL + PRICECONSIGNEE_URL, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(priceconsigneeAddLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((priceconsigneeAdded) => {
                dispatch(priceconsigneeAddSuccess(true, []))
            })
            .catch((err) => {
                dispatch(priceconsigneeAddError(true))
            })
    }
}

export function priceconsigneeFetchOne(id) {
    return (dispatch) => {
        dispatch(priceconsigneeViewError(false))
        dispatch(priceconsigneeViewSuccess(false, null))
        dispatch(priceconsigneeViewLoading(true))

        let token = localStorage.getItem('tolaut@token') || null
        let config = {}
        if (token) {
            config = {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }
        } else {
            throw Error("Authentication error")
        }
        fetch(BASE_URL + PRICECONSIGNEE_URL + '/' + id, config)
            .then((response) => {
                if (response.status === 401) {
                    dispatch(doLogout())
                }
                if (!response.ok) {
                    throw Error(response.statusText)
                }
                dispatch(priceconsigneeViewLoading(false))
                return response
            })
            .then((response) => response.json())
            .then((data) => {
                dispatch(priceconsigneeViewSuccess(true, data))
            })
            .catch((err) => {
                dispatch(priceconsigneeViewError(true))
            })
    }
}