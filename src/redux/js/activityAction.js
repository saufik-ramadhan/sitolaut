import { doLogout } from "actions/authAction";
import {
  BASE_URL,
  ACTIVITY_URL,
  ACTIVITY_GET_ERROR,
  ACTIVITY_GET_LOADING,
  ACTIVITY_GET_SUCCESS,
  ACTIVITY_VIEW_ERROR,
  ACTIVITY_VIEW_LOADING,
  ACTIVITY_VIEW_SUCCESS,
} from "./constant";

const activityGetError = (bool) => {
  return {
    type: ACTIVITY_GET_ERROR,
    activityGetError: bool,
  };
};

const activityGetLoading = (bool) => {
  return {
    type: ACTIVITY_GET_LOADING,
    activityGetLoading: bool,
  };
};

const activityGetSuccess = (bool, activities) => {
  return {
    type: ACTIVITY_GET_SUCCESS,
    activityGetSuccess: bool,
    activities,
  };
};

const activityViewError = (bool) => {
  return {
    type: ACTIVITY_VIEW_ERROR,
    activityViewError: bool,
  };
};

const activityViewLoading = (bool) => {
  return {
    type: ACTIVITY_VIEW_LOADING,
    activityViewLoading: bool,
  };
};

const activityViewSuccess = (bool, activity) => {
  return {
    type: ACTIVITY_VIEW_SUCCESS,
    activityViewSuccess: bool,
    activity,
  };
};

export function activityFetchAll(item) {
  return (dispatch) => {
    dispatch(activityGetSuccess(false, null));
    dispatch(activityGetError(false));
    dispatch(activityGetLoading(true));

    let token = localStorage.getItem("tolaut@token") || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(activityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(activityGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(activityGetError(true));
      });
  };
}

export function activityFetchSub(path) {
  return (dispatch) => {
    dispatch(activityGetSuccess(false, null));
    dispatch(activityGetError(false));
    dispatch(activityGetLoading(true));

    let token = localStorage.getItem("tolaut@token") || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(activityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(activityGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(activityGetError(true));
      });
  };
}

export function activityFetchPage(item) {
  return (dispatch) => {
    dispatch(activityGetSuccess(false, null));
    dispatch(activityGetError(false));
    dispatch(activityGetLoading(true));

    let token = localStorage.getItem("tolaut@token") || null;
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(activityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(activityGetSuccess(true, data));
      })
      .catch((err) => {
        dispatch(activityGetError(true));
      });
  };
}

export function activityFetchOne(id) {
  return (dispatch) => {
    dispatch(activityViewSuccess(false, null));
    dispatch(activityViewError(false));
    dispatch(activityViewLoading(true));

    let token = localStorage.getItem("tolaut@token") || null;
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(activityViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(activityViewSuccess(true, data));
      })
      .catch((err) => {
        dispatch(activityViewError(true));
      });
  };
}
