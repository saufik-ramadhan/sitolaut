import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import { BASE_URL, SUPPLIER_URL } from "./../config/constants";
import FetchRetry from "../services/FetchRetry";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  supplierGetError: false,
  supplierGetLoading: false,
  supplierGetSuccess: false,
  supplierViewError: false,
  supplierViewLoading: false,
  supplierViewSuccess: false,
  supplierAddError: false,
  supplierAddLoading: false,
  supplierAddInvalid: false,
  supplierAddSuccess: false,
  supplierDeleteError: false,
  supplierDeleteLoading: false,
  supplierDeleteSuccess: false,

  supplierData: [],
  supplierFilter: {},
};

/**
 * ACTION
 */
const setSupplierGetError = createAction('setSupplierGetError', (data: any) => data);
const setSupplierGetLoading = createAction('setSupplierGetLoading', (data: any) => data);
const setSupplierGetSuccess = createAction('setSupplierGetSuccess', (data: any) => data);
const setSupplierViewError = createAction('setSupplierViewError', (data: any) => data);
const setSupplierViewLoading = createAction('setSupplierViewLoading', (data: any) => data);
const setSupplierViewSuccess = createAction('setSupplierViewSuccess', (data: any) => data);
const setSupplierAddError = createAction('setSupplierAddError', (data: any) => data);
const setSupplierAddLoading = createAction('setSupplierAddLoading', (data: any) => data);
const setSupplierAddInvalid = createAction('setSupplierAddInvalid', (data: any) => data);
const setSupplierAddSuccess = createAction('setSupplierAddSuccess', (data: any) => data);
const setSupplierDeleteError = createAction('setSupplierDeleteError', (data: any) => data);
const setSupplierDeleteLoading = createAction('setSupplierDeleteLoading', (data: any) => data);
const setSupplierDeleteSuccess = createAction('setSupplierDeleteSuccess', (data: any) => data);

const setSupplierData = createAction('setSupplierData', (data: any) => data);
const setSupplierFilterAdd = createAction('setSupplierFilterAdd', (data: any) => data);
const setSupplierFilterDel = createAction('setSupplierFilterDel', (data: any) => data);
const setSupplierFilterClear = createAction('setSupplierFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const supplierReducer = createReducer(
  {
    [setSupplierGetError]: (state, data) => ({
      ...state,
      supplierGetError: data,
    }),
    [setSupplierGetLoading]: (state, data) => ({
      ...state,
      supplierGetLoading: data,
    }),
    [setSupplierGetSuccess]: (state, data) => ({
      ...state,
      supplierGetSuccess: data,
    }),
    [setSupplierViewError]: (state, data) => ({
      ...state,
      supplierViewError: data,
    }),
    [setSupplierViewLoading]: (state, data) => ({
      ...state,
      supplierViewLoading: data,
    }),
    [setSupplierViewSuccess]: (state, data) => ({
      ...state,
      supplierViewSuccess: data,
    }),
    [setSupplierAddError]: (state, data) => ({
      ...state,
      supplierAddError: data,
    }),
    [setSupplierAddLoading]: (state, data) => ({
      ...state,
      supplierAddLoading: data,
    }),
    [setSupplierAddInvalid]: (state, data) => ({
      ...state,
      supplierAddInvalid: data,
    }),
    [setSupplierAddSuccess]: (state, data) => ({
      ...state,
      supplierAddSuccess: data,
    }),
    [setSupplierDeleteError]: (state, data) => ({
      ...state,
      supplierDeleteError: data,
    }),
    [setSupplierDeleteLoading]: (state, data) => ({
      ...state,
      supplierDeleteLoading: data,
    }),
    [setSupplierDeleteSuccess]: (state, data) => ({
      ...state,
      supplierDeleteSuccess: data,
    }),

    [setSupplierData]: (state, data: Array<"any">) => ({
      ...state,
      supplierData: data,
    }),
    [setSupplierFilterAdd]: (state, data: any) => ({
      ...state,
      supplierFilter: data,
    }),
    [setSupplierFilterDel]: (state, id: any) => ({
      ...state,
      supplierFilter: {},
    }),
    [setSupplierFilterClear]: (state, data: any) => ({
      ...state,
      supplierFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function supplierClear() {
  return async function (dispatch, getState) {
    dispatch(setSupplierGetSuccess(false));
    dispatch(setSupplierGetError(false));
    dispatch(setSupplierGetLoading(true));
  };
}

export function supplierFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setSupplierGetSuccess(false));
    dispatch(setSupplierGetError(false));
    dispatch(setSupplierGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().supplier.supplierData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setSupplierData(data.data));
        } else {
          dispatch(setSupplierData(prev.concat(data.data)));
        }
        dispatch(setSupplierGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierGetError(true));
        dispatch(setSupplierGetLoading(false));
      });
  };
}

export function supplierFetchSub(company_id) {
  return async function (dispatch, getState) {
    dispatch(setSupplierGetSuccess(false));
    dispatch(setSupplierGetError(false));
    dispatch(setSupplierGetLoading(true));
    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        company: company_id,
      }),
    };
    fetch(BASE_URL + SUPPLIER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().supplier.supplierData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setSupplierData(data.data));
        } else {
          dispatch(setSupplierData(prev.concat(data.data)));
        }
        dispatch(setSupplierGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierGetError(true));
        dispatch(setSupplierGetLoading(false));
      });
  };
}

export function supplierFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setSupplierGetSuccess(false));
    dispatch(setSupplierGetError(false));
    dispatch(setSupplierGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().supplier.supplierData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setSupplierData(data.data));
        } else {
          dispatch(setSupplierData(prev.concat(data.data)));
        }
        dispatch(setSupplierGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierGetError(true));
        dispatch(setSupplierGetLoading(false));
      });
  };
}

export function supplierAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setSupplierAddError(false));
    dispatch(setSupplierAddInvalid(false));
    dispatch(setSupplierAddSuccess(false));
    dispatch(setSupplierAddLoading(true));

    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      body: formData,
    };
    fetch(BASE_URL + SUPPLIER_URL + "/new", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((supplier) => {
        if (supplier.success) {
          dispatch(setSupplierAddSuccess(supplier));
        } else {
          dispatch(setSupplierAddInvalid(supplier));
        }
      })
      .catch((err) => {
        dispatch(setSupplierAddError(true));
        dispatch(setSupplierAddLoading(false));
        alert("Error");
      });
  };
}

export function supplierUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setSupplierAddError(false));
    dispatch(setSupplierAddSuccess(false));
    dispatch(setSupplierAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((supplier) => {
        dispatch(setSupplierAddSuccess(supplier));
      })
      .catch((err) => {
        dispatch(setSupplierAddError(true));
        dispatch(setSupplierAddLoading(false));
      });
  };
}

export function supplierFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setSupplierViewError(false));
    dispatch(setSupplierViewSuccess(false));
    dispatch(setSupplierViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setSupplierViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierViewError(true));
        dispatch(setSupplierViewLoading(false));
      });
  };
}

export function supplierFetchUser(id) {
  return async function (dispatch, getState) {
    dispatch(setSupplierViewError(false));
    dispatch(setSupplierViewSuccess(false));
    dispatch(setSupplierViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_URL + "/profile", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setSupplierViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierViewError(true));
        dispatch(setSupplierViewLoading(false));
      });
  };
}

export function handleStatus(data) {
  return async function (dispatch, getState) {
    dispatch(setSupplierAddError(false));
    dispatch(setSupplierAddSuccess(false));
    dispatch(setSupplierAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "register/" + data.status, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((supplier) => {
        dispatch(setSupplierAddSuccess(supplier));
      })
      .catch((err) => {
        dispatch(setSupplierAddError(true));
        dispatch(setSupplierAddLoading(false));
      });
  };
}

export function supplierDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setSupplierViewSuccess(false));
    dispatch(setSupplierDeleteError(false));
    dispatch(setSupplierDeleteSuccess(false));
    dispatch(setSupplierDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setSupplierDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierDeleteError(true));
        dispatch(setSupplierDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function supplierFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setSupplierFilterAdd(filter));
  };
}

export function supplierFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setSupplierFilterDel(id));
  };
}

export function supplierFilterClear() {
  return function (dispatch, getState) {
    dispatch(setSupplierFilterClear());
  };
}

export default supplierReducer;
export { setSupplierAddSuccess };
