import { createAction, createReducer } from "redux-act";
import { BASE_URL, PORT_URL } from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import FetchRetry from "../services/FetchRetry";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  portGetError: false,
  portGetLoading: false,
  portGetSuccess: false,
  portViewError: false,
  portViewLoading: false,
  portViewSuccess: false,
  portAddError: false,
  portAddLoading: false,
  portAddSuccess: false,
  portDeleteError: false,
  portDeleteLoading: false,
  portDeleteSuccess: false,

  portData: [],
  portFilter: {},
};

/**
 * ACTION
 */
const setPortGetError = createAction('setPortGetError', (data: any) => data);
const setPortGetLoading = createAction('setPortGetLoading', (data: any) => data);
const setPortGetSuccess = createAction('setPortGetSuccess', (data: any) => data);
const setPortViewError = createAction('setPortViewError', (data: any) => data);
const setPortViewLoading = createAction('setPortViewLoading', (data: any) => data);
const setPortViewSuccess = createAction('setPortViewSuccess', (data: any) => data);
const setPortAddError = createAction('setPortAddError', (data: any) => data);
const setPortAddLoading = createAction('setPortAddLoading', (data: any) => data);
const setPortAddSuccess = createAction('setPortAddSuccess', (data: any) => data);
const setPortDeleteError = createAction('setPortDeleteError', (data: any) => data);
const setPortDeleteLoading = createAction('setPortDeleteLoading', (data: any) => data);
const setPortDeleteSuccess = createAction('setPortDeleteSuccess', (data: any) => data);

const setPortData = createAction('setPortData', (data: any) => data);
const setPortFilterAdd = createAction('setPortFilterAdd', (data: any) => data);
const setPortFilterDel = createAction('setPortFilterDel', (data: any) => data);
const setPortFilterClear = createAction('setPortFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const portReducer = createReducer(
  {
    [setPortGetError]: (state, data) => ({ ...state, portGetError: data }),
    [setPortGetLoading]: (state, data) => ({ ...state, portGetLoading: data }),
    [setPortGetSuccess]: (state, data) => ({ ...state, portGetSuccess: data }),
    [setPortViewError]: (state, data) => ({ ...state, portViewError: data }),
    [setPortViewLoading]: (state, data) => ({
      ...state,
      portViewLoading: data,
    }),
    [setPortViewSuccess]: (state, data) => ({
      ...state,
      portViewSuccess: data,
    }),
    [setPortAddError]: (state, data) => ({ ...state, portAddError: data }),
    [setPortAddLoading]: (state, data) => ({ ...state, portAddLoading: data }),
    [setPortAddSuccess]: (state, data) => ({ ...state, portAddSuccess: data }),
    [setPortDeleteError]: (state, data) => ({
      ...state,
      portDeleteError: data,
    }),
    [setPortDeleteLoading]: (state, data) => ({
      ...state,
      portDeleteLoading: data,
    }),
    [setPortDeleteSuccess]: (state, data) => ({
      ...state,
      portDeleteSuccess: data,
    }),

    [setPortData]: (state, data: Array<"any">) => ({
      ...state,
      portData: data,
    }),
    [setPortFilterAdd]: (state, data: any) => ({
      ...state,
      portFilter: data,
    }),
    [setPortFilterDel]: (state, id: any) => ({
      ...state,
      portFilter: {},
    }),
    [setPortFilterClear]: (state, data: any) => ({
      ...state,
      portFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function portFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setPortGetSuccess(false));
    dispatch(setPortGetError(false));
    dispatch(setPortGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PORT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPortGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().port.portData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPortData(data.data));
        } else {
          dispatch(setPortData(prev.concat(data.data)));
        }
        dispatch(setPortGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPortGetError(true));
      });
  };
}

export function portFetchSub(supplier_id) {
  return async function (dispatch, getState) {
    dispatch(setPortGetSuccess(false));
    dispatch(setPortGetError(false));
    dispatch(setPortGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PORT_URL + "/" + supplier_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPortGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().port.portData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPortData(data.data));
        } else {
          dispatch(setPortData(prev.concat(data.data)));
        }
        dispatch(setPortGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPortGetError(true));
      });
  };
}

export function portFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setPortGetSuccess(false));
    dispatch(setPortGetError(false));
    dispatch(setPortGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PORT_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPortGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().port.portData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPortData(data.data));
        } else {
          dispatch(setPortData(prev.concat(data.data)));
        }
        dispatch(setPortGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPortGetError(true));
      });
  };
}

export function portAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setPortAddError(false));
    dispatch(setPortAddSuccess(false));
    dispatch(setPortAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    };
    fetch(BASE_URL + PORT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPortAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((port) => {
        dispatch(setPortAddSuccess(port));
      })
      .catch((err) => {
        dispatch(setPortAddError(true));
      });
  };
}

export function portFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setPortViewError(false));
    dispatch(setPortViewSuccess(false));
    dispatch(setPortViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PORT_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPortViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPortViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setPortViewError(true));
      });
  };
}

export function portUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setPortAddError(false));
    dispatch(setPortAddSuccess(false));
    dispatch(setPortAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PORT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPortAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((port) => {
        if (port.status == 200) {
          dispatch(setPortAddSuccess(port));
          alert('sukses')
        } else {
          dispatch(setPortAddError(true));
          alert('gagal')
        }
      })
      .catch((err) => {
        dispatch(setPortAddError(true));
        alert('gagal')
      });
  };
}

export function portDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setPortViewSuccess(false));
    dispatch(setPortDeleteError(false));
    dispatch(setPortDeleteSuccess(false));
    dispatch(setPortDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PORT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPortDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPortDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setPortDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function portFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setPortFilterAdd(filter));
  };
}

export function portFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setPortFilterDel(id));
  };
}

export function portFilterClear() {
  return function (dispatch, getState) {
    dispatch(setPortFilterClear());
  };
}

export default portReducer;
