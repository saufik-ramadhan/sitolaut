import { createAction, createReducer } from "redux-act";
import {
  BASE_URL,
  AUTH_LOGIN_URL,
  AUTH_REGISTER_URL,
  AUTH_REGISTER_CHECK_URL,
} from "../config/constants";
import AsyncStorage from "./../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  authLoginError: false,
  authLoginLoading: false,
  authLoginSuccess: false,
  authLogoutLoading: false,
  authLogoutSuccess: false,
  authRegisterError: false,
  authRegisterLoading: false,
  authRegisterSuccess: false,
  authRegisterCheckError: false,
  authRegisterCheckLoading: false,
  authRegisterCheckSuccess: false,
  authForgotPasswordError: false,
  authForgotPasswordLoading: false,
  authForgotPasswordSuccess: false,
  token: false,
};

/**
 * ACTION
 */
const setAuthLoginError = createAction('setAuthLoginError', (data: any) => data);
const setAuthLoginLoading = createAction('setAuthLoginLoading', (data: any) => data);
const setAuthLoginSuccess = createAction('setAuthLoginSuccess', (data: any) => data);
const setAuthLogoutLoading = createAction('setAuthLogoutLoading', (data: any) => data);
const setAuthLogoutSuccess = createAction('setAuthLogoutSuccess', (data: any) => data);
const setAuthRegisterError = createAction('setAuthRegisterError', (data: any) => data);
const setAuthRegisterLoading = createAction('setAuthRegisterLoading', (data: any) => data);
const setAuthRegisterSuccess = createAction('setAuthRegisterSuccess', (data: any) => data);
const setAuthRegisterCheckError = createAction('setAuthRegisterCheckError', (data: any) => data);
const setAuthRegisterCheckLoading = createAction('setAuthRegisterCheckLoading', (data: any) => data);
const setAuthRegisterCheckSuccess = createAction('setAuthRegisterCheckSuccess', (data: any) => data);
const setAuthForgotPasswordError = createAction('setAuthForgotPasswordError', (data: any) => data);
const setAuthForgotPasswordLoading = createAction('setAuthForgotPasswordLoading', (data: any) => data);
const setAuthForgotPasswordSuccess = createAction('setAuthForgotPasswordSuccess', (data: any) => data);
const setToken = createAction('setToken', (data: any) => data);

/**
 * REDUCER
 */
const authReducer = createReducer(
  {
    [setAuthLoginError]: (state, data) => ({ ...state, authLoginError: data }),
    [setAuthLoginLoading]: (state, data) => ({
      ...state,
      authLoginLoading: data,
    }),
    [setAuthLoginSuccess]: (state, data) => ({
      ...state,
      authLoginSuccess: data,
    }),
    [setAuthLogoutLoading]: (state, data) => ({
      ...state,
      authLogoutLoading: data,
    }),
    [setAuthLogoutSuccess]: (state, data) => ({
      ...state,
      authLogoutSuccess: data,
    }),
    [setAuthRegisterError]: (state, data) => ({
      ...state,
      authRegisterError: data,
    }),
    [setAuthRegisterLoading]: (state, data) => ({
      ...state,
      authRegisterLoading: data,
    }),
    [setAuthRegisterSuccess]: (state, data) => ({
      ...state,
      authRegisterSuccess: data,
    }),
    [setAuthRegisterCheckError]: (state, data) => ({
      ...state,
      authRegisterCheckError: data,
    }),
    [setAuthRegisterCheckLoading]: (state, data) => ({
      ...state,
      authRegisterCheckLoading: data,
    }),
    [setAuthRegisterCheckSuccess]: (state, data) => ({
      ...state,
      authRegisterCheckSuccess: data,
    }),
    [setAuthForgotPasswordError]: (state, data) => ({
      ...state,
      authForgotPasswordError: data,
    }),
    [setAuthForgotPasswordLoading]: (state, data) => ({
      ...state,
      authForgotPasswordLoading: data,
    }),
    [setAuthForgotPasswordSuccess]: (state, data) => ({
      ...state,
      authForgotPasswordSuccess: data,
    }),
    [setToken]: (state, data) => ({
      ...state,
      token: data,
    }),
  },
  DefaultState
);

/**
 * withTimeout
 * Function for adding time out for each request
 * @param msecs
 * @param promise
 */
function withTimeout(msecs: number, promise: any) {
  const timeout = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error("timeout"));
    }, msecs);
  });
  return Promise.race([timeout, promise]);
}

export function doLogin(cred) {
  return async function (dispatch) {
    dispatch(setAuthLoginSuccess(false));
    dispatch(setAuthLoginError(false));
    dispatch(setAuthLoginLoading(true));
    dispatch(setToken(false));
    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: cred.email,
        password: cred.password,
        usery_type_id: cred.usery_type_id,
      }),
    };
    fetch(BASE_URL + AUTH_LOGIN_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setAuthLoginLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((account) => {
        if (account.token) {
          AsyncStorage.setItem("token@lcs", account.token);
          AsyncStorage.setItem("token@userId", String(account.data.user[0].id));
          dispatch(setToken(account.token));
          dispatch(setAuthLoginSuccess(account));
        } else {
          alert("Email atau password yang anda masukkan salah");
          dispatch(setAuthLoginError(true));
        }
      })
      .catch((err) => {
        dispatch(setAuthLoginError(true));
        dispatch(setAuthLoginLoading(false));
        alert("Timeout");
      });
  };
}

export function doRegister(cred) {
  return async function (dispatch) {
    dispatch(setAuthRegisterSuccess(false));
    dispatch(setAuthRegisterError(false));
    dispatch(setAuthRegisterLoading(true));

    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: cred.email,
        password: cred.password,
        usery_type_id: cred.usery_type_id,
      }),
    };
    fetch(BASE_URL + AUTH_REGISTER_URL, config)
      .then((response) => {
        if (!response.ok) {
          dispatch(setAuthRegisterError(true));
        }
        dispatch(setAuthRegisterLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((account) => {
        if (account.success) {
          dispatch(setAuthRegisterSuccess(account));
        } else {
          dispatch(setAuthRegisterError(true));
        }
      })
      .catch((err) => {
        dispatch(setAuthRegisterError(true));
      });
  };
}

/**
 * doLogout
 * logout from app
 */
export function doLogout() {
  return async function (dispatch) {
    dispatch(setToken(false));
    // dispatch(setAuthLoginSuccess(false));
  };
}

export function doRegisterCheck(cred) {
  return async function (dispatch) {
    dispatch(setAuthRegisterCheckSuccess(false));
    dispatch(setAuthRegisterCheckError(false));
    dispatch(setAuthRegisterCheckLoading(true));

    let config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: cred.email,
        usery_type_id: cred.usery_type_id,
        password: cred.password,
        confirm_password: cred.confirm_password,
      }),
    };
    fetch(BASE_URL + AUTH_REGISTER_CHECK_URL, config)
      .then((response) => {
        if (!response.ok) {
          dispatch(setAuthRegisterCheckError(true));
        }
        dispatch(setAuthRegisterCheckLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((check_data) => {
        if (check_data.success) {
          dispatch(setAuthRegisterCheckSuccess(check_data));
        } else {
          dispatch(setAuthRegisterCheckError(check_data));
        }
      })
      .catch((err) => {
        dispatch(setAuthRegisterCheckError(true));
        dispatch(setAuthRegisterCheckLoading(false));
        console.log("Timeout");
      });
  };
}

export function registerClearData() {
  return async function (dispatch) {
    dispatch(setAuthRegisterCheckSuccess(false));
    dispatch(setAuthRegisterCheckError(false));
  };
}

export function forgotPassword(cred) {
  return async function (dispatch) {
    dispatch(setAuthForgotPasswordLoading(true));
    const config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: cred.email,
        usery_type_id: cred.usery_type_id,
      }),
    };
    fetch("https://lcsv2.artikulpi.com/api/v1/forgot-password", config)
      .then((response) => response.json())
      .then((result) => {
        if (result.success) {
          dispatch(setAuthForgotPasswordSuccess(result));
        } else {
          dispatch(setAuthForgotPasswordError(result));
        }
        dispatch(setAuthForgotPasswordLoading(false));
      })
      .catch((err) => {
        dispatch(setAuthForgotPasswordSuccess(false));
        dispatch(setAuthForgotPasswordLoading(false));
        alert("Error");
      });
  };
}

export default authReducer;
