import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { BASE_URL, COMPLAINT_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

const DefaultState = {
  complaintGetError: false,
  complaintGetLoading: false,
  complaintGetSuccess: false,
  complaintViewError: false,
  complaintViewLoading: false,
  complaintViewSuccess: false,
  complaintAddError: false,
  complaintAddLoading: false,
  complaintAddSuccess: false,
  complaintDeleteError: false,
  complaintDeleteLoading: false,
  complaintDeleteSuccess: false,
  complaintReplyError: false,
  complaintReplyLoading: false,
  complaintReplySuccess: false,
  complaintCloseError: false,
  complaintCloseSuccess: false,
  complaintCloseLoading: false,

  complaintData: [],
  complaintFilter: {},
};

/**
 * ACTION
 */

const setComplaintGetError = createAction('setComplaintGetError', (data: any) => data);
const setComplaintGetLoading = createAction('setComplaintGetLoading', (data: any) => data);
const setComplaintGetSuccess = createAction('setComplaintGetSuccess', (data: any) => data);
const setComplaintViewError = createAction('setComplaintViewError', (data: any) => data);
const setComplaintViewLoading = createAction('setComplaintViewLoading', (data: any) => data);
const setComplaintViewSuccess = createAction('setComplaintViewSuccess', (data: any) => data);
const setComplaintAddError = createAction('setComplaintAddError', (data: any) => data);
const setComplaintAddLoading = createAction('setComplaintAddLoading', (data: any) => data);
const setComplaintAddSuccess = createAction('setComplaintAddSuccess', (data: any) => data);
const setComplaintDeleteError = createAction('setComplaintDeleteError', (data: any) => data);
const setComplaintDeleteLoading = createAction('setComplaintDeleteLoading', (data: any) => data);
const setComplaintDeleteSuccess = createAction('setComplaintDeleteSuccess', (data: any) => data);
const setComplaintReplyError = createAction('setComplaintReplyError', (data: any) => data);
const setComplaintReplyLoading = createAction('setComplaintReplyLoading', (data: any) => data);
const setComplaintReplySuccess = createAction('setComplaintReplySuccess', (data: any) => data);
const setComplaintCloseError = createAction('setComplaintCloseError', (data: any) => data);
const setComplaintCloseSuccess = createAction('setComplaintCloseSuccess', (data: any) => data);
const setComplaintCloseLoading = createAction('setComplaintCloseLoading', (data: any) => data);

const setComplaintData = createAction('setComplaintData', (data: any) => data);
const setComplaintFilterAdd = createAction('setComplaintFilterAdd', (data: any) => data);
const setComplaintFilterDel = createAction('setComplaintFilterDel', (data: any) => data);
const setComplaintFilterClear = createAction('setComplaintFilterClear', (data: any) => data);

/**
 * REDUCER
 */

const complaintReducer = createReducer(
  {
    [setComplaintGetError]: (state, data) => ({
      ...state,
      complaintGetError: data,
    }),
    [setComplaintGetLoading]: (state, data) => ({
      ...state,
      complaintGetLoading: data,
    }),
    [setComplaintGetSuccess]: (state, data) => ({
      ...state,
      complaintGetSuccess: data,
    }),
    [setComplaintViewError]: (state, data) => ({
      ...state,
      complaintViewError: data,
    }),
    [setComplaintViewLoading]: (state, data) => ({
      ...state,
      complaintViewLoading: data,
    }),
    [setComplaintViewSuccess]: (state, data) => ({
      ...state,
      complaintViewSuccess: data,
    }),
    [setComplaintAddError]: (state, data) => ({
      ...state,
      complaintAddError: data,
    }),
    [setComplaintAddLoading]: (state, data) => ({
      ...state,
      complaintAddLoading: data,
    }),
    [setComplaintAddSuccess]: (state, data) => ({
      ...state,
      complaintAddSuccess: data,
    }),
    [setComplaintDeleteError]: (state, data) => ({
      ...state,
      complaintDeleteError: data,
    }),
    [setComplaintDeleteLoading]: (state, data) => ({
      ...state,
      complaintDeleteLoading: data,
    }),
    [setComplaintDeleteSuccess]: (state, data) => ({
      ...state,
      complaintDeleteSuccess: data,
    }),
    [setComplaintReplyError]: (state, data) => ({
      ...state,
      complaintReplyError: data,
    }),
    [setComplaintReplyLoading]: (state, data) => ({
      ...state,
      complaintReplyLoading: data,
    }),
    [setComplaintReplySuccess]: (state, data) => ({
      ...state,
      complaintReplySuccess: data,
    }),
    [setComplaintCloseError]: (state, data) => ({
      ...state,
      complaintCloseError: data,
    }),
    [setComplaintCloseSuccess]: (state, data) => ({
      ...state,
      complaintCloseSuccess: data,
    }),
    [setComplaintCloseLoading]: (state, data) => ({
      ...state,
      complaintCloseLoading: data,
    }),

    [setComplaintData]: (state, data: Array<"any">) => ({
      ...state,
      complaintData: data,
    }),
    [setComplaintFilterAdd]: (state, data: any) => ({
      ...state,
      complaintFilter: data,
    }),
    [setComplaintFilterDel]: (state, id: any) => ({
      ...state,
      complaintFilter: {},
    }),
    [setComplaintFilterClear]: (state, data: any) => ({
      ...state,
      complaintFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */

export function complaintFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setComplaintGetSuccess(false));
    dispatch(setComplaintGetError(false));
    dispatch(setComplaintGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMPLAINT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().complaint.complaintData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setComplaintData(data.data));
        } else {
          dispatch(setComplaintData(prev.concat(data.data)));
        }
        dispatch(setComplaintGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setComplaintGetError(true));
        dispatch(setComplaintGetLoading(false));
      });
  };
}

export function complaintFetchSub(supplier_id) {
  return async function (dispatch, getState) {
    dispatch(setComplaintGetSuccess(false));
    dispatch(setComplaintGetError(false));
    dispatch(setComplaintGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMPLAINT_URL + "/" + supplier_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().complaint.complaintData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setComplaintData(data.data));
        } else {
          dispatch(setComplaintData(prev.concat(data.data)));
        }
        dispatch(setComplaintGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setComplaintGetError(true));
        dispatch(setComplaintGetLoading(false));
      });
  };
}

export function complaintFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setComplaintGetSuccess(false));
    dispatch(setComplaintGetError(false));
    dispatch(setComplaintGetLoading(true));
    dispatch(setComplaintAddSuccess(false));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMPLAINT_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().complaint.complaintData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setComplaintData(data.data));
        } else {
          dispatch(setComplaintData(prev.concat(data.data)));
        }
        dispatch(setComplaintGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setComplaintGetError(true));
        dispatch(setComplaintGetLoading(false));
      });
  };
}

export function complaintAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setComplaintAddError(false));
    dispatch(setComplaintAddSuccess(false));
    dispatch(setComplaintAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + COMPLAINT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((complaint) => {
        dispatch(setComplaintAddSuccess(complaint));
      })
      .catch((err) => {
        dispatch(setComplaintAddError(true));
        dispatch(setComplaintAddLoading(false));
      });
  };
}

export function complaintFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setComplaintViewError(false));
    dispatch(setComplaintViewSuccess(false));
    dispatch(setComplaintViewLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = {
      id: id,
    };

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMPLAINT_URL + "/view", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setComplaintViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setComplaintViewError(true));
        dispatch(setComplaintViewLoading(false));
      });
  };
}

export function complaintUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setComplaintAddError(false));
    dispatch(setComplaintAddSuccess(false));
    dispatch(setComplaintAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMPLAINT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((complaint) => {
        if (complaint.status == 200) {
          dispatch(setComplaintAddSuccess(complaint));
        } else {
          dispatch(setComplaintAddError(true));
          dispatch(setComplaintAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setComplaintAddError(true));
        dispatch(setComplaintAddLoading(false));
      });
  };
}

export function complaintDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setComplaintViewSuccess(false));
    dispatch(setComplaintDeleteError(false));
    dispatch(setComplaintDeleteSuccess(false));
    dispatch(setComplaintDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMPLAINT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setComplaintDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setComplaintDeleteError(true));
        dispatch(setComplaintDeleteLoading(false));
      });
  };
}

export function complaintReply(data) {
  return async function (dispatch, getState) {
    dispatch(setComplaintReplyError(false));
    dispatch(setComplaintReplySuccess(false));
    dispatch(setComplaintReplyLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + COMPLAINT_URL + "/reply", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintReplyLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((complaint) => {
        dispatch(setComplaintReplySuccess(complaint));
      })
      .catch((err) => {
        dispatch(setComplaintReplyError(true));
        dispatch(setComplaintReplyLoading(false));
      });
  };
}

export function complaintClose(data) {
  return async function (dispatch, getState) {
    dispatch(setComplaintCloseError(false));
    dispatch(setComplaintCloseSuccess(false));
    dispatch(setComplaintCloseLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + COMPLAINT_URL + "/close", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setComplaintCloseLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((complaint) => {
        dispatch(setComplaintCloseSuccess(complaint));
      })
      .catch((err) => {
        dispatch(setComplaintCloseError(true));
        dispatch(setComplaintCloseLoading(false));
      });
  };
}

/**
 * Filter
 */
export function complaintFilterAdd(filter) {
  return function (dispatch) {
    dispatch(setComplaintFilterAdd(filter));
  };
}

export function complaintFilterDel(id) {
  return function (dispatch) {
    dispatch(setComplaintFilterDel(id));
  };
}

export function complaintFilterClear() {
  return function (dispatch) {
    dispatch(setComplaintFilterClear());
  };
}

export default complaintReducer;
