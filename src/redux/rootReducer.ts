import { combineReducers } from "redux";
import authReducer from "./authReducers";
import consigneeReducer from "./consigneeReducer";
import kotaReducer from "./kotaReducer";
import supplierReducer from "./supplierReducer";
import shipperReducer from "./shipperReducer";
import resellerReducer from "./resellerReducer";
import operatorReducer from "./operatorReducer";
import provinsiReducer from "./provinsiReducer";
import userReducer from "./userReducer";
import commodityReducer from "./commodityReducer";
import msttrayekReducer from "./msttrayekReducer";
import portReducer from "./portReducer";
import scheduleReducer from "./scheduleReducer";
import dashboardReducer from "./dashboardReducer";
import bookingReducer from "./bookingReducer";
import complaintReducer from "./complaintReducer";
import jenisbarangReducer from "./jenisbarangReducer";
import priceconsigneeReducer from "./priceconsigneeReducer";
import purchaseReducer from "./purchaseReducer";
import trackReducer from "./trackReducer";
import roleReducer from "./roleReducer";
import biayapengurusanReducer from "./biayapengurusanReducer";
import routeReducer from "./routeReducer";
import detailpoReducer from "./detailpoReducer";
import distributionReducer from "./distributionReducer";
import forgotpasswordReducer from "./forgotpasswordReducer";
import jeniscommodityReducer from "./jeniscommodityReducer";
import priceReducer from "./priceReducer";
import depotReducer from "./depotReducer";
import manifestReducer from "./manifestReducer";
import unitReducer from "./unitReducer";
import vesselReducer from "./vesselReducer";
import typecontainerReducer from "./typecontainerReducer";
import cartReducer from "./cartReducer";
import jptReducer from "./jptReducer";
import jadwalReducer from "./jadwalReducer";
import choosesupplierReducer from "./choosesupplierReducer";
import containerReducer from "./containerReducer";
import detailContainerReducer from "./detailContainerReducer";
import packagingReducer from "./packagingReducer";
import trackingdataReducer from "./trackingdataReducer";
import activityReducer from "./activityReducer";
import masterkodetrayekReducer from "./masterkodetrayekReducer";
import masterhargaReducer from "./masterhargaReducer";
import masterkontrakReducer from "./masterkontrakReducer";
import guestReducer from "./guestReducer";
import masterkapalReducer from "./masterkapalReducer";
import reporttblReducer from "./reporttblReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  biayapengurusan: biayapengurusanReducer,
  booking: bookingReducer,
  cart: cartReducer,
  choosesupplier: choosesupplierReducer,
  commodity: commodityReducer,
  complaint: complaintReducer,
  consignee: consigneeReducer,
  dashboard: dashboardReducer,
  depot: depotReducer,
  detailpo: detailpoReducer,
  distribution: distributionReducer,
  forgotpassword: forgotpasswordReducer,
  jenisbarang: jenisbarangReducer,
  jeniscommodity: jeniscommodityReducer,
  jpt: jptReducer,
  jadwal: jadwalReducer,
  kota: kotaReducer,
  manifest: manifestReducer,
  msttrayek: msttrayekReducer,
  operator: operatorReducer,
  packaging: packagingReducer,
  port: portReducer,
  price: priceReducer,
  priceconsignee: priceconsigneeReducer,
  provinsi: provinsiReducer,
  purchase: purchaseReducer,
  reseller: resellerReducer,
  role: roleReducer,
  route: routeReducer,
  schedule: scheduleReducer,
  shipper: shipperReducer,
  supplier: supplierReducer,
  track: trackReducer,
  typecontainer: typecontainerReducer,
  unit: unitReducer,
  user: userReducer,
  vessel: vesselReducer,
  container: containerReducer,
  detailContainer: detailContainerReducer,
  trackingdata: trackingdataReducer,
  activity: activityReducer,
  masterkodetrayek: masterkodetrayekReducer,
  masterharga: masterhargaReducer,
  masterkontrak: masterkontrakReducer,
  masterkapal: masterkapalReducer,
  reporttbl: reporttblReducer,
  guest: guestReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
