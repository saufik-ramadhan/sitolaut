import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { RootState } from "./rootReducer";
import { useSelector } from "react-redux";
import { BASE_URL, PRICECONSIGNEE_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  priceconsigneeGetError: false,
  priceconsigneeGetLoading: false,
  priceconsigneeGetSuccess: false,
  priceconsigneeViewError: false,
  priceconsigneeViewLoading: false,
  priceconsigneeViewSuccess: false,
  priceconsigneeAddError: false,
  priceconsigneeAddLoading: false,
  priceconsigneeAddSuccess: false,

  priceconsigneeData: [],
  priceconsigneeFilter: {},
};

/**
 * ACTION
 */
const setPriceconsigneeGetError = createAction('setPriceconsigneeGetError', (data: any) => data);
const setPriceconsigneeGetLoading = createAction('setPriceconsigneeGetLoading', (data: any) => data);
const setPriceconsigneeGetSuccess = createAction('setPriceconsigneeGetSuccess', (data: any) => data);
const setPriceconsigneeViewError = createAction('setPriceconsigneeViewError', (data: any) => data);
const setPriceconsigneeViewLoading = createAction('setPriceconsigneeViewLoading', (data: any) => data);
const setPriceconsigneeViewSuccess = createAction('setPriceconsigneeViewSuccess', (data: any) => data);
const setPriceconsigneeAddError = createAction('setPriceconsigneeAddError', (data: any) => data);
const setPriceconsigneeAddLoading = createAction('setPriceconsigneeAddLoading', (data: any) => data);
const setPriceconsigneeAddSuccess = createAction('setPriceconsigneeAddSuccess', (data: any) => data);

const setPriceconsigneeData = createAction('setPriceconsigneeData', (data: any) => data);
const setPriceconsigneeFilterAdd = createAction('setPriceconsigneeFilterAdd', (data: any) => data);
const setPriceconsigneeFilterDel = createAction('setPriceconsigneeFilterDel', (data: any) => data);
const setPriceconsigneeFilterClear = createAction('setPriceconsigneeFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const priceconsigneeReducer = createReducer(
  {
    [setPriceconsigneeGetError]: (state, data) => ({
      ...state,
      priceconsigneeGetError: data,
    }),
    [setPriceconsigneeGetLoading]: (state, data) => ({
      ...state,
      priceconsigneeGetLoading: data,
    }),
    [setPriceconsigneeGetSuccess]: (state, data) => ({
      ...state,
      priceconsigneeGetSuccess: data,
    }),
    [setPriceconsigneeViewError]: (state, data) => ({
      ...state,
      priceconsigneeViewError: data,
    }),
    [setPriceconsigneeViewLoading]: (state, data) => ({
      ...state,
      priceconsigneeViewLoading: data,
    }),
    [setPriceconsigneeViewSuccess]: (state, data) => ({
      ...state,
      priceconsigneeViewSuccess: data,
    }),
    [setPriceconsigneeAddError]: (state, data) => ({
      ...state,
      priceconsigneeAddError: data,
    }),
    [setPriceconsigneeAddLoading]: (state, data) => ({
      ...state,
      priceconsigneeAddLoading: data,
    }),
    [setPriceconsigneeAddSuccess]: (state, data) => ({
      ...state,
      priceconsigneeAddSuccess: data,
    }),

    [setPriceconsigneeData]: (state, data: Array<"any">) => ({
      ...state,
      priceconsigneeData: data,
    }),
    [setPriceconsigneeFilterAdd]: (state, data: any) => ({
      ...state,
      priceconsigneeFilter: data,
    }),
    [setPriceconsigneeFilterDel]: (state, id: any) => ({
      ...state,
      priceconsigneeFilter: {},
    }),
    [setPriceconsigneeFilterClear]: (state, data: any) => ({
      ...state,
      priceconsigneeFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function priceconsigneeFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setPriceconsigneeGetSuccess(false));
    dispatch(setPriceconsigneeGetError(false));
    dispatch(setPriceconsigneeGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICECONSIGNEE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceconsigneeGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPriceconsigneeGetSuccess(data));
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().priceconsignee.priceconsigneeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPriceconsigneeData(data.data));
        } else {
          dispatch(setPriceconsigneeData(prev.concat(data.data)));
        }
      })
      .catch((err) => {
        dispatch(setPriceconsigneeGetError(true));
        dispatch(setPriceconsigneeGetLoading(false));
      });
  };
}

export function priceconsigneeFetchSub(supplier_id) {
  return async function (dispatch, getState) {
    dispatch(setPriceconsigneeGetSuccess(false));
    dispatch(setPriceconsigneeGetError(false));
    dispatch(setPriceconsigneeGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICECONSIGNEE_URL + "/" + supplier_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceconsigneeGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPriceconsigneeGetSuccess(data));
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().priceconsignee.priceconsigneeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPriceconsigneeData(data.data));
        } else {
          dispatch(setPriceconsigneeData(prev.concat(data.data)));
        }
      })
      .catch((err) => {
        dispatch(setPriceconsigneeGetError(true));
        dispatch(setPriceconsigneeGetLoading(false));
      });
  };
}

export function priceconsigneeFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setPriceconsigneeGetSuccess(false));
    dispatch(setPriceconsigneeGetError(false));
    dispatch(setPriceconsigneeGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICECONSIGNEE_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceconsigneeGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().priceconsignee.priceconsigneeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPriceconsigneeData(data.data));
        } else {
          dispatch(setPriceconsigneeData(prev.concat(data.data)));
        }
        dispatch(setPriceconsigneeGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPriceconsigneeGetError(true));
        dispatch(setPriceconsigneeGetLoading(false));
      });
  };
}

export function priceconsigneeUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setPriceconsigneeAddError(false));
    dispatch(setPriceconsigneeAddSuccess(false));
    dispatch(setPriceconsigneeAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + PRICECONSIGNEE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceconsigneeAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((priceconsigneeAdded) => {
        dispatch(setPriceconsigneeAddSuccess([]));
      })
      .catch((err) => {
        dispatch(setPriceconsigneeAddError(true));
        dispatch(setPriceconsigneeAddLoading(false));
      });
  };
}

export function priceconsigneeFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setPriceconsigneeViewError(false));
    dispatch(setPriceconsigneeViewSuccess(false));
    dispatch(setPriceconsigneeViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICECONSIGNEE_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceconsigneeViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPriceconsigneeViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setPriceconsigneeViewError(true));
        dispatch(setPriceconsigneeViewLoading(false));
      });
  };
}

/**
 * Filter
 */
export function priceconsigneeFilterAdd(filter) {
  return function (dispatch) {
    dispatch(setPriceconsigneeFilterAdd(filter));
  };
}

export function priceconsigneeFilterDel(id) {
  return function (dispatch) {
    dispatch(setPriceconsigneeFilterDel(id));
  };
}

export function priceconsigneeFilterClear() {
  return function (dispatch) {
    dispatch(setPriceconsigneeFilterClear());
  };
}

export default priceconsigneeReducer;
