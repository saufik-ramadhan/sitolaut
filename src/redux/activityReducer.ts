import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import { BASE_URL, ACTIVITY_URL } from "../config/constants";

const DefaultState = {
  activityGetError: false,
  activityGetLoading: false,
  activityGetSuccess: false,
  activityViewError: false,
  activityViewLoading: false,
  activityViewSuccess: false,

  activityData: [],
  activityFilter: {},
};

const setActivityGetError = createAction('setActivityGetError', (data: any) => data);
const setActivityGetLoading = createAction('setActivityGetLoading', (data: any) => data);
const setActivityGetSuccess = createAction('setActivityGetSuccess', (data: any) => data);
const setActivityViewError = createAction('setActivityViewError', (data: any) => data);
const setActivityViewLoading = createAction('setActivityViewLoading', (data: any) => data);
const setActivityViewSuccess = createAction('setActivityViewSuccess', (data: any) => data);

const setActivityData = createAction('setActivityData', (data: any) => data);
const setActivityFilterAdd = createAction('setActivityFilterAdd', (data: any) => data);
const setActivityFilterDel = createAction('setActivityFilterDel', (data: any) => data);
const setActivityFilterClear = createAction('setActivityFilterClear', (data: any) => data);

const activityReducer = createReducer(
  {
    [setActivityGetError]: (state, data) => ({
      ...state,
      activityGetError: data,
    }),
    [setActivityGetLoading]: (state, data) => ({
      ...state,
      activityGetLoading: data,
    }),
    [setActivityGetSuccess]: (state, data) => ({
      ...state,
      activityGetSuccess: data,
    }),
    [setActivityViewError]: (state, data) => ({
      ...state,
      activityViewError: data,
    }),
    [setActivityViewLoading]: (state, data) => ({
      ...state,
      activityViewLoading: data,
    }),
    [setActivityViewSuccess]: (state, data) => ({
      ...state,
      activityViewSuccess: data,
    }),

    [setActivityData]: (state, data: Array<"any">) => ({
      ...state,
      activityData: data,
    }),
    [setActivityFilterAdd]: (state, data: any) => ({
      ...state,
      activityFilter: data,
    }),
    [setActivityFilterDel]: (state, id: any) => ({
      ...state,
      activityFilter: {},
    }),
    [setActivityFilterClear]: (state, data: any) => ({
      ...state,
      activityFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function activityFetchAll(item) {
  return async function (dispatch, getState) {
    dispatch(setActivityGetSuccess(false));
    dispatch(setActivityGetError(false));
    dispatch(setActivityGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setActivityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().activity.activityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setActivityData(data.data));
        } else {
          dispatch(setActivityData(prev.concat(data.data)));
        }
        dispatch(setActivityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setActivityGetError(true));
      });
  };
}

export function activityFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setActivityGetSuccess(false));
    dispatch(setActivityGetError(false));
    dispatch(setActivityGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setActivityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().activity.activityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setActivityData(data.data));
        } else {
          dispatch(setActivityData(prev.concat(data.data)));
        }
        dispatch(setActivityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setActivityGetError(true));
      });
  };
}

export function activityFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setActivityGetSuccess(false));
    dispatch(setActivityGetError(false));
    dispatch(setActivityGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setActivityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().activity.activityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setActivityData(data.data));
        } else {
          dispatch(setActivityData(prev.concat(data.data)));
        }
        dispatch(setActivityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setActivityGetError(true));
      });
  };
}

export function activityFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setActivityViewSuccess(false));
    dispatch(setActivityViewError(false));
    dispatch(setActivityViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ACTIVITY_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setActivityViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setActivityViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setActivityViewError(true));
      });
  };
}

/**
 * Filter
 */
export function activityFilterAdd(filter) {
  return function (dispatch) {
    dispatch(setActivityFilterAdd(filter));
  };
}

export function activityFilterDel(id) {
  return function (dispatch) {
    dispatch(setActivityFilterDel(id));
  };
}

export function activityFilterClear() {
  return function (dispatch) {
    dispatch(setActivityFilterClear());
  };
}

export default activityReducer;
