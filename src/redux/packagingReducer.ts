import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import { BASE_URL, PACKAGING_URL } from "../config/constants";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  packagingGetError: false,
  packagingGetLoading: false,
  packagingGetSuccess: false,
  packagingViewError: false,
  packagingViewLoading: false,
  packagingViewSuccess: false,
  packagingAddError: false,
  packagingAddLoading: false,
  packagingAddSuccess: false,
  packagingDeleteError: false,
  packagingDeleteLoading: false,
  packagingDeleteSuccess: false,

  packagingData: [],
  packagingFilter: {},
};

/**
 * ACTION
 */
const setPackagingGetError = createAction('setPackagingGetError', (data: any) => data);
const setPackagingGetLoading = createAction('setPackagingGetLoading', (data: any) => data);
const setPackagingGetSuccess = createAction('setPackagingGetSuccess', (data: any) => data);
const setPackagingViewError = createAction('setPackagingViewError', (data: any) => data);
const setPackagingViewLoading = createAction('setPackagingViewLoading', (data: any) => data);
const setPackagingViewSuccess = createAction('setPackagingViewSuccess', (data: any) => data);
const setPackagingAddError = createAction('setPackagingAddError', (data: any) => data);
const setPackagingAddLoading = createAction('setPackagingAddLoading', (data: any) => data);
const setPackagingAddSuccess = createAction('setPackagingAddSuccess', (data: any) => data);
const setPackagingDeleteError = createAction('setPackagingDeleteError', (data: any) => data);
const setPackagingDeleteLoading = createAction('setPackagingDeleteLoading', (data: any) => data);
const setPackagingDeleteSuccess = createAction('setPackagingDeleteSuccess', (data: any) => data);

const setPackagingData = createAction('setPackagingData', (data: any) => data);
const setPackagingFilterAdd = createAction('setPackagingFilterAdd', (data: any) => data);
const setPackagingFilterDel = createAction('setPackagingFilterDel', (data: any) => data);
const setPackagingFilterClear = createAction('setPackagingFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const packagingReducer = createReducer(
  {
    [setPackagingGetError]: (state, data) => ({
      ...state,
      packagingGetError: data,
    }),
    [setPackagingGetLoading]: (state, data) => ({
      ...state,
      packagingGetLoading: data,
    }),
    [setPackagingGetSuccess]: (state, data) => ({
      ...state,
      packagingGetSuccess: data,
    }),
    [setPackagingViewError]: (state, data) => ({
      ...state,
      packagingViewError: data,
    }),
    [setPackagingViewLoading]: (state, data) => ({
      ...state,
      packagingViewLoading: data,
    }),
    [setPackagingViewSuccess]: (state, data) => ({
      ...state,
      packagingViewSuccess: data,
    }),
    [setPackagingAddError]: (state, data) => ({
      ...state,
      packagingAddError: data,
    }),
    [setPackagingAddLoading]: (state, data) => ({
      ...state,
      packagingAddLoading: data,
    }),
    [setPackagingAddSuccess]: (state, data) => ({
      ...state,
      packagingAddSuccess: data,
    }),
    [setPackagingDeleteError]: (state, data) => ({
      ...state,
      packagingDeleteError: data,
    }),
    [setPackagingDeleteLoading]: (state, data) => ({
      ...state,
      packagingDeleteLoading: data,
    }),
    [setPackagingDeleteSuccess]: (state, data) => ({
      ...state,
      packagingDeleteSuccess: data,
    }),

    [setPackagingData]: (state, data: Array<"any">) => ({
      ...state,
      packagingData: data,
    }),
    [setPackagingFilterAdd]: (state, data: any) => ({
      ...state,
      packagingFilter: data,
    }),
    [setPackagingFilterDel]: (state, id: any) => ({
      ...state,
      packagingFilter: {},
    }),
    [setPackagingFilterClear]: (state, data: any) => ({
      ...state,
      packagingFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function packagingFetchAll(tokenAuth) {
  return async function (dispatch, getState) {
    dispatch(setPackagingAddSuccess(false));
    dispatch(setPackagingGetSuccess(false));
    dispatch(setPackagingGetError(false));
    dispatch(setPackagingGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PACKAGING_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPackagingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().packaging.packagingData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPackagingData(data.data));
        } else {
          dispatch(setPackagingData(prev.concat(data.data)));
        }
        dispatch(setPackagingGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPackagingGetError(true));
        dispatch(setPackagingGetLoading(false));
      });
  };
}

export function packagingFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setPackagingAddSuccess(false));
    dispatch(setPackagingGetSuccess(false));
    dispatch(setPackagingGetError(false));
    dispatch(setPackagingGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PACKAGING_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPackagingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().packaging.packagingData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPackagingData(data.data));
        } else {
          dispatch(setPackagingData(prev.concat(data.data)));
        }
        dispatch(setPackagingGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPackagingGetError(true));
        dispatch(setPackagingGetLoading(false));
      });
  };
}

export function packagingFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setPackagingAddSuccess(false));
    dispatch(setPackagingGetSuccess(false));
    dispatch(setPackagingGetError(false));
    dispatch(setPackagingGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PACKAGING_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPackagingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().packaging.packagingData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPackagingData(data.data));
        } else {
          dispatch(setPackagingData(prev.concat(data.data)));
        }
        dispatch(setPackagingGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPackagingGetError(true));
        dispatch(setPackagingGetLoading(false));
      });
  };
}

export function packagingAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setPackagingAddError(false));
    dispatch(setPackagingAddSuccess(false));
    dispatch(setPackagingAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PACKAGING_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPackagingAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((packaging) => {
        dispatch(setPackagingAddSuccess(packaging));
      })
      .catch((err) => {
        dispatch(setPackagingAddError(true));
        dispatch(setPackagingAddLoading(false));
      });
  };
}

export function packagingFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setPackagingViewError(false));
    dispatch(setPackagingViewSuccess(false));
    dispatch(setPackagingViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PACKAGING_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPackagingViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPackagingViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setPackagingViewError(true));
        dispatch(setPackagingViewLoading(false));
      });
  };
}

export function packagingUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setPackagingAddError(false));
    dispatch(setPackagingAddSuccess(false));
    dispatch(setPackagingAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PACKAGING_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPackagingAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((packaging) => {
        if (packaging.success) {
          dispatch(setPackagingAddSuccess(packaging));
        } else {
          dispatch(setPackagingAddError(true));
          dispatch(setPackagingAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setPackagingAddError(true));
        dispatch(setPackagingAddLoading(false));
      });
  };
}

export function packagingDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setPackagingViewSuccess(false));
    dispatch(setPackagingDeleteError(false));
    dispatch(setPackagingDeleteSuccess(false));
    dispatch(setPackagingDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PACKAGING_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPackagingDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPackagingDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setPackagingDeleteError(true));
        dispatch(setPackagingDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function packagingFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setPackagingFilterAdd(filter));
  };
}

export function packagingFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setPackagingFilterDel(id));
  };
}

export function packagingFilterClear() {
  return function (dispatch, getState) {
    dispatch(setPackagingFilterClear());
  };
}

export default packagingReducer;
