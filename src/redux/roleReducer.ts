import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, ROLE_URL } from "./../config/constants";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  roleGetError: false,
  roleGetLoading: false,
  roleGetSuccess: false,
  roleViewError: false,
  roleViewLoading: false,
  roleViewSuccess: false,
  roleAddError: false,
  roleAddLoading: false,
  roleAddSuccess: false,
  roleDeleteError: false,
  roleDeleteLoading: false,
  roleDeleteSuccess: false,
  roleEditError: false,
  roleEditLoading: false,
  roleEditSuccess: false,
};

/**
 * ACTION
 */
const setRoleGetError = createAction('setRoleGetError', (data: any) => data);
const setRoleGetLoading = createAction('setRoleGetLoading', (data: any) => data);
const setRoleGetSuccess = createAction('setRoleGetSuccess', (data: any) => data);
const setRoleViewError = createAction('setRoleViewError', (data: any) => data);
const setRoleViewLoading = createAction('setRoleViewLoading', (data: any) => data);
const setRoleViewSuccess = createAction('setRoleViewSuccess', (data: any) => data);
const setRoleAddError = createAction('setRoleAddError', (data: any) => data);
const setRoleAddLoading = createAction('setRoleAddLoading', (data: any) => data);
const setRoleAddSuccess = createAction('setRoleAddSuccess', (data: any) => data);
const setRoleDeleteError = createAction('setRoleDeleteError', (data: any) => data);
const setRoleDeleteLoading = createAction('setRoleDeleteLoading', (data: any) => data);
const setRoleDeleteSuccess = createAction('setRoleDeleteSuccess', (data: any) => data);
const setRoleEditError = createAction('setRoleEditError', (data: any) => data);
const setRoleEditLoading = createAction('setRoleEditLoading', (data: any) => data);
const setRoleEditSuccess = createAction('setRoleEditSuccess', (data: any) => data);

/**
 * REDUCER
 */
const roleReducer = createReducer(
  {
    [setRoleGetError]: (state, data) => ({ ...state, roleGetError: data }),
    [setRoleGetLoading]: (state, data) => ({ ...state, roleGetLoading: data }),
    [setRoleGetSuccess]: (state, data) => ({ ...state, roleGetSuccess: data }),
    [setRoleViewError]: (state, data) => ({ ...state, roleViewError: data }),
    [setRoleViewLoading]: (state, data) => ({
      ...state,
      roleViewLoading: data,
    }),
    [setRoleViewSuccess]: (state, data) => ({
      ...state,
      roleViewSuccess: data,
    }),
    [setRoleAddError]: (state, data) => ({ ...state, roleAddError: data }),
    [setRoleAddLoading]: (state, data) => ({ ...state, roleAddLoading: data }),
    [setRoleAddSuccess]: (state, data) => ({ ...state, roleAddSuccess: data }),
    [setRoleDeleteError]: (state, data) => ({
      ...state,
      roleDeleteError: data,
    }),
    [setRoleDeleteLoading]: (state, data) => ({
      ...state,
      roleDeleteLoading: data,
    }),
    [setRoleDeleteSuccess]: (state, data) => ({
      ...state,
      roleDeleteSuccess: data,
    }),
    [setRoleEditError]: (state, data) => ({ ...state, roleEditError: data }),
    [setRoleEditLoading]: (state, data) => ({
      ...state,
      roleEditLoading: data,
    }),
    [setRoleEditSuccess]: (state, data) => ({
      ...state,
      roleEditSuccess: data,
    }),
  },
  DefaultState
);

/**
 * API
 */
export function roleFetchAll(data) {
  return async function (dispatch) {
    dispatch(setRoleGetSuccess(false));
    dispatch(setRoleGetError(false));
    dispatch(setRoleGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    // if (token) {
    config = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    // } else {
    //   throw Error("Authentication error")
    // }
    fetch(BASE_URL + ROLE_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRoleGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setRoleGetError(true));
      });
  };
}

export function roleFetchSub(supplier_id) {
  return async function (dispatch) {
    dispatch(setRoleGetSuccess(false));
    dispatch(setRoleGetError(false));
    dispatch(setRoleGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROLE_URL + "/" + supplier_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRoleGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setRoleGetError(true));
      });
  };
}

export function roleFetchPage(params) {
  return async function (dispatch) {
    dispatch(setRoleGetSuccess(false));
    dispatch(setRoleGetError(false));
    dispatch(setRoleGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROLE_URL + "/comprehensive", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRoleGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setRoleGetError(true));
      });
  };
}

export function roleAdd(data) {
  return async function (dispatch) {
    dispatch(setRoleAddError(false));
    dispatch(setRoleAddSuccess(false));
    dispatch(setRoleAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + ROLE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((role) => {
        dispatch(setRoleAddSuccess(role));
      })
      .catch((err) => {
        dispatch(setRoleAddError(true));
      });
  };
}

export function roleEdit(data) {
  return async function (dispatch) {
    dispatch(setRoleEditError(false));
    dispatch(setRoleEditSuccess(false));
    dispatch(setRoleEditLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + ROLE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleEditLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((role) => {
        dispatch(setRoleEditSuccess(role));
      })
      .catch((err) => {
        dispatch(setRoleEditError(true));
      });
  };
}

export function roleFetchOne(data) {
  return async function (dispatch) {
    dispatch(setRoleViewError(false));
    dispatch(setRoleViewSuccess(false));
    dispatch(setRoleViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROLE_URL + "/view", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRoleViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setRoleViewError(true));
      });
  };
}

export function roleUpdate(data) {
  return async function (dispatch) {
    dispatch(setRoleAddError(false));
    dispatch(setRoleAddSuccess(false));
    dispatch(setRoleAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROLE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((role) => {
        if (role.status == 200) {
          dispatch(setRoleAddSuccess(role));
        } else {
          dispatch(setRoleAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setRoleAddError(true));
      });
  };
}

export function roleDelete(id) {
  return async function (dispatch) {
    dispatch(setRoleViewSuccess(false));
    dispatch(setRoleDeleteError(false));
    dispatch(setRoleDeleteSuccess(false));
    dispatch(setRoleDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + ROLE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRoleDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRoleDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setRoleDeleteError(true));
      });
  };
}

export default roleReducer;
