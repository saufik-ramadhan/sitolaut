import { createAction, createReducer } from "redux-act";
import { BASE_URL, CONSIGNEE_URL, SCHEDULE_URL } from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import FetchRetry from "../services/FetchRetry";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  scheduleGetError: false,
  scheduleGetLoading: false,
  scheduleGetSuccess: false,
  scheduleGetOldError: false,
  scheduleGetOldLoading: false,
  scheduleGetOldSuccess: false,
  scheduleViewError: false,
  scheduleViewLoading: false,
  scheduleViewSuccess: false,
  scheduleAddError: false,
  scheduleAddLoading: false,
  scheduleAddSuccess: false,
  scheduleDeleteError: false,
  scheduleDeleteLoading: false,
  scheduleDeleteSuccess: false,

  scheduleData: [],
  scheduleFilter: {},

  scheduleDataOld: [],
  scheduleFilterOld: {},
};

/**
 * ACTION
 */
const setScheduleGetError = createAction('setScheduleGetError', (data: any) => data);
const setScheduleGetLoading = createAction('setScheduleGetLoading', (data: any) => data);
const setScheduleGetSuccess = createAction('setScheduleGetSuccess', (data: any) => data);
const setScheduleGetOldError = createAction('setScheduleGetOldError', (data: any) => data);
const setScheduleGetOldLoading = createAction('setScheduleGetOldLoading', (data: any) => data);
const setScheduleGetOldSuccess = createAction('setScheduleGetOldSuccess', (data: any) => data);
const setScheduleViewError = createAction('setScheduleViewError', (data: any) => data);
const setScheduleViewLoading = createAction('setScheduleViewLoading', (data: any) => data);
const setScheduleViewSuccess = createAction('setScheduleViewSuccess', (data: any) => data);
const setScheduleAddError = createAction('setScheduleAddError', (data: any) => data);
const setScheduleAddLoading = createAction('setScheduleAddLoading', (data: any) => data);
const setScheduleAddSuccess = createAction('setScheduleAddSuccess', (data: any) => data);
const setScheduleDeleteError = createAction('setScheduleDeleteError', (data: any) => data);
const setScheduleDeleteLoading = createAction('setScheduleDeleteLoading', (data: any) => data);
const setScheduleDeleteSuccess = createAction('setScheduleDeleteSuccess', (data: any) => data);

const setScheduleData = createAction('setScheduleData', (data: any) => data);
const setScheduleFilterAdd = createAction('setScheduleFilterAdd', (data: any) => data);
const setScheduleFilterDel = createAction('setScheduleFilterDel', (data: any) => data);
const setScheduleFilterClear = createAction('setScheduleFilterClear', (data: any) => data);

const setScheduleDataOld = createAction('setScheduleDataOld', (data: any) => data);
const setScheduleFilterAddOld = createAction('setScheduleFilterAddOld', (data: any) => data);
const setScheduleFilterDelOld = createAction('setScheduleFilterDelOld', (data: any) => data);
const setScheduleFilterClearOld = createAction('setScheduleFilterClearOld', (data: any) => data);

/**
 * REDUCER
 */
const scheduleReducer = createReducer(
  {
    [setScheduleGetError]: (state, data) => ({
      ...state,
      scheduleGetError: data,
    }),
    [setScheduleGetLoading]: (state, data) => ({
      ...state,
      scheduleGetLoading: data,
    }),
    [setScheduleGetSuccess]: (state, data) => ({
      ...state,
      scheduleGetSuccess: data,
    }),
    [setScheduleGetOldError]: (state, data) => ({
      ...state,
      scheduleGetOldError: data,
    }),
    [setScheduleGetOldLoading]: (state, data) => ({
      ...state,
      scheduleGetOldLoading: data,
    }),
    [setScheduleGetOldSuccess]: (state, data) => ({
      ...state,
      scheduleGetOldSuccess: data,
    }),
    [setScheduleViewError]: (state, data) => ({
      ...state,
      scheduleViewError: data,
    }),
    [setScheduleViewLoading]: (state, data) => ({
      ...state,
      scheduleViewLoading: data,
    }),
    [setScheduleViewSuccess]: (state, data) => ({
      ...state,
      scheduleViewSuccess: data,
    }),
    [setScheduleAddError]: (state, data) => ({
      ...state,
      scheduleAddError: data,
    }),
    [setScheduleAddLoading]: (state, data) => ({
      ...state,
      scheduleAddLoading: data,
    }),
    [setScheduleAddSuccess]: (state, data) => ({
      ...state,
      scheduleAddSuccess: data,
    }),
    [setScheduleDeleteError]: (state, data) => ({
      ...state,
      scheduleDeleteError: data,
    }),
    [setScheduleDeleteLoading]: (state, data) => ({
      ...state,
      scheduleDeleteLoading: data,
    }),
    [setScheduleDeleteSuccess]: (state, data) => ({
      ...state,
      scheduleDeleteSuccess: data,
    }),

    [setScheduleData]: (state, data: Array<"any">) => ({
      ...state,
      scheduleData: data,
    }),
    [setScheduleFilterAdd]: (state, data: any) => ({
      ...state,
      scheduleFilter: data,
    }),
    [setScheduleFilterDel]: (state, id: any) => ({
      ...state,
      scheduleFilter: {},
    }),
    [setScheduleFilterClear]: (state, data: any) => ({
      ...state,
      scheduleFilter: {},
    }),

    [setScheduleDataOld]: (state, data: Array<"any">) => ({
      ...state,
      scheduleDataOld: data,
    }),
    [setScheduleFilterAddOld]: (state, data: any) => ({
      ...state,
      scheduleFilterOld: data,
    }),
    [setScheduleFilterDelOld]: (state, id: any) => ({
      ...state,
      scheduleFilterOld: {},
    }),
    [setScheduleFilterClearOld]: (state, data: any) => ({
      ...state,
      scheduleFilterOld: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function scheduleFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setScheduleAddSuccess(false));
    dispatch(setScheduleGetSuccess(false));
    dispatch(setScheduleGetError(false));
    dispatch(setScheduleGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setScheduleGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setScheduleGetError(true));
        dispatch(setScheduleGetLoading(false));
      });
  };
}

export function scheduleFetchSub(supplier_id) {
  return async function (dispatch, getState) {
    dispatch(setScheduleAddSuccess(false));
    dispatch(setScheduleGetSuccess(false));
    dispatch(setScheduleGetError(false));
    dispatch(setScheduleGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL + "/" + supplier_id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setScheduleGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setScheduleGetError(true));
        dispatch(setScheduleGetLoading(false));
      });
  };
}

export function scheduleFetchOld(params) {
  return async function (dispatch, getState) {
    dispatch(setScheduleAddSuccess(false));
    dispatch(setScheduleGetOldSuccess(false));
    dispatch(setScheduleGetOldError(false));
    dispatch(setScheduleGetOldLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL + "/old", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleGetOldLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().schedule.scheduleDataOld || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setScheduleDataOld(data.data));
        } else {
          dispatch(setScheduleDataOld(prev.concat(data.data)));
        }
        dispatch(setScheduleGetOldSuccess(data));
      })
      .catch((err) => {
        dispatch(setScheduleGetOldError(true));
        dispatch(setScheduleGetOldLoading(false));
      });
  };
}

export function scheduleReset() {
  return async function (dispatch, getState) {
    dispatch(setScheduleGetSuccess(false));
  };
}

export function scheduleClear() {
  return async function (dispatch, getState) {
    dispatch(setScheduleGetSuccess(false));
    dispatch(setScheduleGetError(false));
    dispatch(setScheduleGetLoading(false));
  };
}

export function scheduleFetchWill(params) {
  return async function (dispatch, getState) {
    dispatch(setScheduleGetSuccess(false));
    dispatch(setScheduleGetError(false));
    dispatch(setScheduleGetLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL + "/will", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          const currentPage = data.currentPage || 1;
          const start = data.start || 0;
          const prev = getState().schedule.scheduleData || [];
          if (start == 0 && currentPage == 1) {
            dispatch(setScheduleData(data.data));
          } else {
            dispatch(setScheduleData(prev.concat(data.data)));
          }
          dispatch(setScheduleGetSuccess(data));
        } else {
          dispatch(setScheduleGetError(true));
          dispatch(setScheduleGetLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setScheduleGetError(true));
        dispatch(setScheduleGetLoading(false));
      });
  };
}

export function scheduleSearch(params) {
  return async function (dispatch, getState) {
    dispatch(setScheduleAddSuccess(false));
    dispatch(setScheduleGetSuccess(false));
    dispatch(setScheduleGetError(false));
    dispatch(setScheduleGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL + "/search", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setScheduleGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setScheduleGetError(true));
        dispatch(setScheduleGetLoading(false));
      });
  };
}

export function scheduleAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setScheduleAddError(false));
    dispatch(setScheduleAddSuccess(false));
    dispatch(setScheduleAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + SCHEDULE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((schedule) => {
        dispatch(setScheduleAddSuccess(schedule));
      })
      .catch((err) => {
        dispatch(setScheduleAddError(true));
        dispatch(setScheduleAddLoading(false));
      });
  };
}

export function scheduleFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setScheduleAddSuccess(false));
    dispatch(setScheduleViewError(false));
    dispatch(setScheduleViewSuccess(false));
    dispatch(setScheduleViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setScheduleViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setScheduleViewError(true));
        dispatch(setScheduleViewLoading(false));
      });
  };
}

export function scheduleUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setScheduleAddError(false));
    dispatch(setScheduleAddSuccess(false));
    dispatch(setScheduleAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((schedule) => {
        if (schedule.success) {
          dispatch(setScheduleAddSuccess(schedule));
        } else {
          dispatch(setScheduleAddError(true));
          dispatch(setScheduleAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setScheduleAddError(true));
        dispatch(setScheduleAddLoading(false));
      });
  };
}

export function scheduleDelete(dataPost) {
  return async (dispatch) => {
    dispatch(setScheduleViewSuccess(false));
    dispatch(setScheduleDeleteError(false));
    dispatch(setScheduleDeleteSuccess(false));
    dispatch(setScheduleDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(dataPost),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SCHEDULE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setScheduleDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setScheduleDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setScheduleDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function scheduleFilterAdd(filter) {
  return function (dispatch) {
    dispatch(setScheduleFilterAdd(filter));
  };
}

export function scheduleFilterDel(id) {
  return function (dispatch) {
    dispatch(setScheduleFilterDel(id));
  };
}

export function scheduleFilterClear() {
  return function (dispatch) {
    dispatch(setScheduleFilterClear());
  };
}

/**
 * Filter
 */
export function scheduleFilterAddOld(filter) {
  return function (dispatch) {
    dispatch(setScheduleFilterAddOld(filter));
  };
}

export function scheduleFilterDelOld(id) {
  return function (dispatch) {
    dispatch(setScheduleFilterDelOld(id));
  };
}

export function scheduleFilterClearOld() {
  return function (dispatch) {
    dispatch(setScheduleFilterClearOld());
  };
}

export default scheduleReducer;
