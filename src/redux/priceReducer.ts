import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, PRICE_URL } from "../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  priceGetError: false,
  priceGetLoading: false,
  priceGetSuccess: false,
  priceViewError: false,
  priceViewLoading: false,
  priceViewSuccess: false,
  priceAddError: false,
  priceAddLoading: false,
  priceAddSuccess: false,
  priceDeleteError: false,
  priceDeleteLoading: false,
  priceDeleteSuccess: false,

  priceData: [],
  priceFilter: {},
};

/**
 * ACTION
 */
const setPriceGetError = createAction('setPriceGetError', (data: any) => data);
const setPriceGetLoading = createAction('setPriceGetLoading', (data: any) => data);
const setPriceGetSuccess = createAction('setPriceGetSuccess', (data: any) => data);
const setPriceViewError = createAction('setPriceViewError', (data: any) => data);
const setPriceViewLoading = createAction('setPriceViewLoading', (data: any) => data);
const setPriceViewSuccess = createAction('setPriceViewSuccess', (data: any) => data);
const setPriceAddError = createAction('setPriceAddError', (data: any) => data);
const setPriceAddLoading = createAction('setPriceAddLoading', (data: any) => data);
const setPriceAddSuccess = createAction('setPriceAddSuccess', (data: any) => data);
const setPriceDeleteError = createAction('setPriceDeleteError', (data: any) => data);
const setPriceDeleteLoading = createAction('setPriceDeleteLoading', (data: any) => data);
const setPriceDeleteSuccess = createAction('setPriceDeleteSuccess', (data: any) => data);

const setPriceData = createAction('setPriceData', (data: any) => data);
const setPriceFilterAdd = createAction('setPriceFilterAdd', (data: any) => data);
const setPriceFilterDel = createAction('setPriceFilterDel', (data: any) => data);
const setPriceFilterClear = createAction('setPriceFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const priceReducer = createReducer(
  {
    [setPriceGetError]: (state, data) => ({ ...state, priceGetError: data }),
    [setPriceGetLoading]: (state, data) => ({
      ...state,
      priceGetLoading: data,
    }),
    [setPriceGetSuccess]: (state, data) => ({
      ...state,
      priceGetSuccess: data,
    }),
    [setPriceViewError]: (state, data) => ({ ...state, priceViewError: data }),
    [setPriceViewLoading]: (state, data) => ({
      ...state,
      priceViewLoading: data,
    }),
    [setPriceViewSuccess]: (state, data) => ({
      ...state,
      priceViewSuccess: data,
    }),
    [setPriceAddError]: (state, data) => ({ ...state, priceAddError: data }),
    [setPriceAddLoading]: (state, data) => ({
      ...state,
      priceAddLoading: data,
    }),
    [setPriceAddSuccess]: (state, data) => ({
      ...state,
      priceAddSuccess: data,
    }),
    [setPriceDeleteError]: (state, data) => ({
      ...state,
      priceDeleteError: data,
    }),
    [setPriceDeleteLoading]: (state, data) => ({
      ...state,
      priceDeleteLoading: data,
    }),
    [setPriceDeleteSuccess]: (state, data) => ({
      ...state,
      priceDeleteSuccess: data,
    }),

    [setPriceData]: (state, data: Array<"any">) => ({
      ...state,
      priceData: data,
    }),
    [setPriceFilterAdd]: (state, data: any) => ({
      ...state,
      priceFilter: data,
    }),
    [setPriceFilterDel]: (state, id: any) => ({
      ...state,
      priceFilter: {},
    }),
    [setPriceFilterClear]: (state, data: any) => ({
      ...state,
      priceFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function priceFetchAll(tokenAuth) {
  return async function (dispatch, getState) {
    dispatch(setPriceAddSuccess(false));
    dispatch(setPriceGetSuccess(false));
    dispatch(setPriceGetError(false));
    dispatch(setPriceGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().price.priceData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPriceData(data.data));
        } else {
          dispatch(setPriceData(prev.concat(data.data)));
        }
        dispatch(setPriceGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPriceGetError(true));
        dispatch(setPriceGetLoading(false));
      });
  };
}

export function priceFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setPriceAddSuccess(false));
    dispatch(setPriceGetSuccess(false));
    dispatch(setPriceGetError(false));
    dispatch(setPriceGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICE_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().price.priceData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPriceData(data.data));
        } else {
          dispatch(setPriceData(prev.concat(data.data)));
        }
        dispatch(setPriceGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPriceGetError(true));
        dispatch(setPriceGetLoading(false));
      });
  };
}

export function priceFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setPriceAddSuccess(false));
    dispatch(setPriceViewSuccess(false));
    dispatch(setPriceGetSuccess(false));
    dispatch(setPriceGetError(false));
    dispatch(setPriceGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICE_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().price.priceData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setPriceData(data.data));
        } else {
          dispatch(setPriceData(prev.concat(data.data)));
        }
        dispatch(setPriceGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setPriceGetError(true));
        dispatch(setPriceGetLoading(false));
      });
  };
}

export function priceAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setPriceAddError(false));
    dispatch(setPriceViewSuccess(false));
    dispatch(setPriceAddSuccess(false));
    dispatch(setPriceAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((price) => {
        dispatch(setPriceAddSuccess(price));
      })
      .catch((err) => {
        dispatch(setPriceAddError(true));
        dispatch(setPriceAddLoading(false));
      });
  };
}

export function priceFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setPriceViewError(false));
    dispatch(setPriceViewSuccess(false));
    dispatch(setPriceViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICE_URL + "/view", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPriceViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setPriceViewError(true));
        dispatch(setPriceViewLoading(false));
      });
  };
}

export function priceUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setPriceViewSuccess(false));
    dispatch(setPriceAddError(false));
    dispatch(setPriceAddSuccess(false));
    dispatch(setPriceAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((price) => {
        if (price.success) {
          dispatch(setPriceAddSuccess(price));
        } else {
          dispatch(setPriceAddError(true));
          dispatch(setPriceAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setPriceAddError(true));
        dispatch(setPriceAddLoading(false));
      });
  };
}

export function priceDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setPriceViewSuccess(false));
    dispatch(setPriceDeleteError(false));
    dispatch(setPriceDeleteSuccess(false));
    dispatch(setPriceDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + PRICE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setPriceDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPriceDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setPriceDeleteError(true));
        dispatch(setPriceDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function priceFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setPriceFilterAdd(filter));
  };
}

export function priceFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setPriceFilterDel(id));
  };
}

export function priceFilterClear() {
  return function (dispatch, getState) {
    dispatch(setPriceFilterClear());
  };
}

export default priceReducer;
