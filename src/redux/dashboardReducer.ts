import { createAction, createReducer } from "redux-act";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import {
  BASE_URL,
  CONSIGNEE_STATISTIC_URL,
  SUPPLIER_STATISTIC_URL,
  SHIPPER_STATISTIC_URL,
  RESELLER_STATISTIC_URL,
  OPERATOR_STATISTIC_URL,
  REGULATOR_STATISTIC_URL,
  VESSEL_TRACKING_URL,
  ORDER_PER_JENIS_PRIORITAS_URL,
  MUATAN_TERBANYAK_URL,
  CONTAINER_PER_TAHUN_URL,
  MUATAN_PER_WILAYAH_URL,
  ORDER_PAID_URL,
  ORDER_UNPAID_URL,
  ORDER_DENIED_URL,
  ALL_ORDER_URL,
  ORDER_PER_BULAN_URL,
  MUATAN_PER_OPERATOR_URL,
  PO_URL,
  DASHBOARD_COMMODITY_URL,
  DO_URL,
  SISA_QUOTA_TRAYEK_URL,
  VOYAGE_PER_TRAYEK_URL,
  DISPARITAS_HARGA_URL,
  REALISASI_URL,
  WAKTU_TEMPUH_URL,
  JADWAL_KAPAL_URL,
  BOOKING_VESSEL_URL,
  DISTRIBUSI_BARANG_URL,
  MUATAN_PER_WILAYAH_PER_PRIORITAS_URL,
} from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";

/** DEFAULT STATE */
const DefaultState = {
  consigneeSttcAllError: false,
  consigneeSttcAllLoading: false,
  consigneeSttcAllSuccess: false,
  consigneeSttcActiveError: false,
  consigneeSttcActiveLoading: false,
  consigneeSttcActiveSuccess: false,
  supplierSttcAllError: false,
  supplierSttcAllLoading: false,
  supplierSttcAllSuccess: false,
  supplierSttcActiveError: false,
  supplierSttcActiveLoading: false,
  supplierSttcActiveSuccess: false,
  shipperSttcAllError: false,
  shipperSttcAllLoading: false,
  shipperSttcAllSuccess: false,
  shipperSttcActiveError: false,
  shipperSttcActiveLoading: false,
  shipperSttcActiveSuccess: false,
  resellerSttcAllError: false,
  resellerSttcAllLoading: false,
  resellerSttcAllSuccess: false,
  resellerSttcActiveError: false,
  resellerSttcActiveLoading: false,
  resellerSttcActiveSuccess: false,
  operatorSttcAllError: false,
  operatorSttcAllLoading: false,
  operatorSttcAllSuccess: false,
  operatorSttcActiveError: false,
  operatorSttcActiveLoading: false,
  operatorSttcActiveSuccess: false,
  operatorNameError: false,
  operatorNameLoading: false,
  operatorNameSuccess: false,
  regulatorSttcAllError: false,
  regulatorSttcAllLoading: false,
  regulatorSttcAllSuccess: false,
  regulatorSttcActiveError: false,
  regulatorSttcActiveLoading: false,
  regulatorSttcActiveSuccess: false,
  vesselTrackingGetError: false,
  vesselTrackingGetLoading: false,
  vesselTrackingGetSuccess: false,
  vesselTrackingViewError: false,
  vesselTrackingViewLoading: false,
  vesselTrackingViewSuccess: false,
  orderPerJenisPrioritasLoading: false,
  orderPerJenisPrioritasError: false,
  orderPerJenisPrioritasSuccess: false,
  muatanTerbanyakLoading: false,
  muatanTerbanyakError: false,
  muatanTerbanyakSuccess: false,
  containerPerTahunLoading: false,
  containerPerTahunError: false,
  containerPerTahunSuccess: false,
  muatanPerWilayahLoading: false,
  muatanPerWilayahError: false,
  muatanPerWilayahSuccess: false,

  muatanPerWilayahPerPrioritasLoading: false,
  muatanPerWilayahPerPrioritasError: false,
  muatanPerWilayahPerPrioritasSuccess: false,

  orderPaidLoading: false,
  orderPaidError: false,
  orderPaidSuccess: false,
  orderUnpaidLoading: false,
  orderUnpaidError: false,
  orderUnpaidSuccess: false,
  orderDeniedLoading: false,
  orderDeniedError: false,
  orderDeniedSuccess: false,
  allOrderLoading: false,
  allOrderError: false,
  allOrderSuccess: false,
  orderPerBulanLoading: false,
  orderPerBulanError: false,
  orderPerBulanSuccess: false,
  muatanPerOperatorLoading: false,
  muatanPerOperatorError: false,
  muatanPerOperatorSuccess: false,
  purchaseOrderLoading: false,
  purchaseOrderError: false,
  purchaseOrderSuccess: false,
  dashboardCommodityLoading: false,
  dashboardCommodityError: false,
  dashboardCommoditySuccess: false,
  deliveryOrderLoading: false,
  deliveryOrderError: false,
  deliveryOrderSuccess: false,
  sisaQuotaTrayekLoading: false,
  sisaQuotaTrayekError: false,
  sisaQuotaTrayekSuccess: false,
  voyagePerTrayekLoading: false,
  voyagePerTrayekError: false,
  voyagePerTrayekSuccess: false,
  disparitasHargaLoading: false,
  disparitasHargaError: false,
  disparitasHargaSuccess: false,
  realisasiLoading: false,
  realisasiError: false,
  realisasiSuccess: false,
  waktuTempuhLoading: false,
  waktuTempuhError: false,
  waktuTempuhSuccess: false,
  jadwalKapalLoading: false,
  jadwalKapalError: false,
  jadwalKapalSuccess: false,
  bookingVesselLoading: false,
  bookingVesselError: false,
  bookingVesselSuccess: false,
  distribusiBarangLoading: false,
  distribusiBarangError: false,
  distribusiBarangSuccess: false,
};

const setConsigneeSttcAllError = createAction('setConsigneeSttcAllError', (data: any) => data);
const setConsigneeSttcAllLoading = createAction('setConsigneeSttcAllLoading', (data: any) => data);
const setConsigneeSttcAllSuccess = createAction('setConsigneeSttcAllSuccess', (data: any) => data);
const setConsigneeSttcActiveError = createAction('setConsigneeSttcActiveError', (data: any) => data);
const setConsigneeSttcActiveLoading = createAction('setConsigneeSttcActiveLoading', (data: any) => data);
const setConsigneeSttcActiveSuccess = createAction('setConsigneeSttcActiveSuccess', (data: any) => data);
const setSupplierSttcAllError = createAction('setSupplierSttcAllError', (data: any) => data);
const setSupplierSttcAllLoading = createAction('setSupplierSttcAllLoading', (data: any) => data);
const setSupplierSttcAllSuccess = createAction('setSupplierSttcAllSuccess', (data: any) => data);
const setSupplierSttcActiveError = createAction('setSupplierSttcActiveError', (data: any) => data);
const setSupplierSttcActiveLoading = createAction('setSupplierSttcActiveLoading', (data: any) => data);
const setSupplierSttcActiveSuccess = createAction('setSupplierSttcActiveSuccess', (data: any) => data);
const setShipperSttcAllError = createAction('setShipperSttcAllError', (data: any) => data);
const setShipperSttcAllLoading = createAction('setShipperSttcAllLoading', (data: any) => data);
const setShipperSttcAllSuccess = createAction('setShipperSttcAllSuccess', (data: any) => data);
const setShipperSttcActiveError = createAction('setShipperSttcActiveError', (data: any) => data);
const setShipperSttcActiveLoading = createAction('setShipperSttcActiveLoading', (data: any) => data);
const setShipperSttcActiveSuccess = createAction('setShipperSttcActiveSuccess', (data: any) => data);
const setResellerSttcAllError = createAction('setResellerSttcAllError', (data: any) => data);
const setResellerSttcAllLoading = createAction('setResellerSttcAllLoading', (data: any) => data);
const setResellerSttcAllSuccess = createAction('setResellerSttcAllSuccess', (data: any) => data);
const setResellerSttcActiveError = createAction('setResellerSttcActiveError', (data: any) => data);
const setResellerSttcActiveLoading = createAction('setResellerSttcActiveLoading', (data: any) => data);
const setResellerSttcActiveSuccess = createAction('setResellerSttcActiveSuccess', (data: any) => data);
const setOperatorSttcAllError = createAction('setOperatorSttcAllError', (data: any) => data);
const setOperatorSttcAllLoading = createAction('setOperatorSttcAllLoading', (data: any) => data);
const setOperatorSttcAllSuccess = createAction('setOperatorSttcAllSuccess', (data: any) => data);
const setOperatorSttcActiveError = createAction('setOperatorSttcActiveError', (data: any) => data);
const setOperatorSttcActiveLoading = createAction('setOperatorSttcActiveLoading', (data: any) => data);
const setOperatorSttcActiveSuccess = createAction('setOperatorSttcActiveSuccess', (data: any) => data);
const setOperatorNameError = createAction('setOperatorNameError', (data: any) => data);
const setOperatorNameLoading = createAction('setOperatorNameLoading', (data: any) => data);
const setOperatorNameSuccess = createAction('setOperatorNameSuccess', (data: any) => data);
const setRegulatorSttcAllError = createAction('setRegulatorSttcAllError', (data: any) => data);
const setRegulatorSttcAllLoading = createAction('setRegulatorSttcAllLoading', (data: any) => data);
const setRegulatorSttcAllSuccess = createAction('setRegulatorSttcAllSuccess', (data: any) => data);
const setRegulatorSttcActiveError = createAction('setRegulatorSttcActiveError', (data: any) => data);
const setRegulatorSttcActiveLoading = createAction('setRegulatorSttcActiveLoading', (data: any) => data);
const setRegulatorSttcActiveSuccess = createAction('setRegulatorSttcActiveSuccess', (data: any) => data);
const setVesselTrackingGetError = createAction('setVesselTrackingGetError', (data: any) => data);
const setVesselTrackingGetLoading = createAction('setVesselTrackingGetLoading', (data: any) => data);
const setVesselTrackingGetSuccess = createAction('setVesselTrackingGetSuccess', (data: any) => data);
const setVesselTrackingViewError = createAction('setVesselTrackingViewError', (data: any) => data);
const setVesselTrackingViewLoading = createAction('setVesselTrackingViewLoading', (data: any) => data);
const setVesselTrackingViewSuccess = createAction('setVesselTrackingViewSuccess', (data: any) => data);
const setOrderPerJenisPrioritasLoading = createAction('setOrderPerJenisPrioritasLoading', (data: any) => data);
const setOrderPerJenisPrioritasError = createAction('setOrderPerJenisPrioritasError', (data: any) => data);
const setOrderPerJenisPrioritasSuccess = createAction('setOrderPerJenisPrioritasSuccess', (data: any) => data);
const setMuatanTerbanyakLoading = createAction('setMuatanTerbanyakLoading', (data: any) => data);
const setMuatanTerbanyakError = createAction('setMuatanTerbanyakError', (data: any) => data);
const setMuatanTerbanyakSuccess = createAction('setMuatanTerbanyakSuccess', (data: any) => data);
const setContainerPerTahunLoading = createAction('setContainerPerTahunLoading', (data: any) => data);
const setContainerPerTahunError = createAction('setContainerPerTahunError', (data: any) => data);
const setContainerPerTahunSuccess = createAction('setContainerPerTahunSuccess', (data: any) => data);
const setMuatanPerWilayahLoading = createAction('setMuatanPerWilayahLoading', (data: any) => data);
const setMuatanPerWilayahError = createAction('setMuatanPerWilayahError', (data: any) => data);
const setMuatanPerWilayahSuccess = createAction('setMuatanPerWilayahSuccess', (data: any) => data);

const setMuatanPerWilayahPerPrioritasLoading = createAction('setMuatanPerWilayahPerPrioritasLoading', 
  (data: any) => data
);
const setMuatanPerWilayahPerPrioritasError = createAction('setMuatanPerWilayahPerPrioritasError', (data: any) => data);
const setMuatanPerWilayahPerPrioritasSuccess = createAction('setMuatanPerWilayahPerPrioritasSuccess', 
  (data: any) => data
);

const setOrderPaidLoading = createAction('setOrderPaidLoading', (data: any) => data);
const setOrderPaidError = createAction('setOrderPaidError', (data: any) => data);
const setOrderPaidSuccess = createAction('setOrderPaidSuccess', (data: any) => data);
const setOrderUnpaidLoading = createAction('setOrderUnpaidLoading', (data: any) => data);
const setOrderUnpaidError = createAction('setOrderUnpaidError', (data: any) => data);
const setOrderUnpaidSuccess = createAction('setOrderUnpaidSuccess', (data: any) => data);
const setOrderDeniedLoading = createAction('setOrderDeniedLoading', (data: any) => data);
const setOrderDeniedError = createAction('setOrderDeniedError', (data: any) => data);
const setOrderDeniedSuccess = createAction('setOrderDeniedSuccess', (data: any) => data);
const setAllOrderLoading = createAction('setAllOrderLoading', (data: any) => data);
const setAllOrderError = createAction('setAllOrderError', (data: any) => data);
const setAllOrderSuccess = createAction('setAllOrderSuccess', (data: any) => data);
const setOrderPerBulanLoading = createAction('setOrderPerBulanLoading', (data: any) => data);
const setOrderPerBulanError = createAction('setOrderPerBulanError', (data: any) => data);
const setOrderPerBulanSuccess = createAction('setOrderPerBulanSuccess', (data: any) => data);
const setMuatanPerOperatorLoading = createAction('setMuatanPerOperatorLoading', (data: any) => data);
const setMuatanPerOperatorError = createAction('setMuatanPerOperatorError', (data: any) => data);
const setMuatanPerOperatorSuccess = createAction('setMuatanPerOperatorSuccess', (data: any) => data);
const setPurchaseOrderLoading = createAction('setPurchaseOrderLoading', (data: any) => data);
const setPurchaseOrderError = createAction('setPurchaseOrderError', (data: any) => data);
const setPurchaseOrderSuccess = createAction('setPurchaseOrderSuccess', (data: any) => data);
const setDashboardCommodityLoading = createAction('setDashboardCommodityLoading', (data: any) => data);
const setDashboardCommodityError = createAction('setDashboardCommodityError', (data: any) => data);
const setDashboardCommoditySuccess = createAction('setDashboardCommoditySuccess', (data: any) => data);
const setDeliveryOrderLoading = createAction('setDeliveryOrderLoading', (data: any) => data);
const setDeliveryOrderError = createAction('setDeliveryOrderError', (data: any) => data);
const setDeliveryOrderSuccess = createAction('setDeliveryOrderSuccess', (data: any) => data);
const setSisaQuotaTrayekLoading = createAction('setSisaQuotaTrayekLoading', (data: any) => data);
const setSisaQuotaTrayekError = createAction('setSisaQuotaTrayekError', (data: any) => data);
const setSisaQuotaTrayekSuccess = createAction('setSisaQuotaTrayekSuccess', (data: any) => data);
const setVoyagePerTrayekLoading = createAction('setVoyagePerTrayekLoading', (data: any) => data);
const setVoyagePerTrayekError = createAction('setVoyagePerTrayekError', (data: any) => data);
const setVoyagePerTrayekSuccess = createAction('setVoyagePerTrayekSuccess', (data: any) => data);
const setDisparitasHargaLoading = createAction('setDisparitasHargaLoading', (data: any) => data);
const setDisparitasHargaError = createAction('setDisparitasHargaError', (data: any) => data);
const setDisparitasHargaSuccess = createAction('setDisparitasHargaSuccess', (data: any) => data);
const setRealisasiLoading = createAction('setRealisasiLoading', (data: any) => data);
const setRealisasiError = createAction('setRealisasiError', (data: any) => data);
const setRealisasiSuccess = createAction('setRealisasiSuccess', (data: any) => data);
const setWaktuTempuhLoading = createAction('setWaktuTempuhLoading', (data: any) => data);
const setWaktuTempuhError = createAction('setWaktuTempuhError', (data: any) => data);
const setWaktuTempuhSuccess = createAction('setWaktuTempuhSuccess', (data: any) => data);
const setJadwalKapalLoading = createAction('setJadwalKapalLoading', (data: any) => data);
const setJadwalKapalError = createAction('setJadwalKapalError', (data: any) => data);
const setJadwalKapalSuccess = createAction('setJadwalKapalSuccess', (data: any) => data);
const setBookingVesselLoading = createAction('setBookingVesselLoading', (data: any) => data);
const setBookingVesselError = createAction('setBookingVesselError', (data: any) => data);
const setBookingVesselSuccess = createAction('setBookingVesselSuccess', (data: any) => data);
const setDistribusiBarangLoading = createAction('setDistribusiBarangLoading', (data: any) => data);
const setDistribusiBarangError = createAction('setDistribusiBarangError', (data: any) => data);
const setDistribusiBarangSuccess = createAction('setDistribusiBarangSuccess', (data: any) => data);

/**
 * REDUCER INITIALIZATION
 */
const dashboardReducer = createReducer(
  {
    [setConsigneeSttcAllError]: (state, data) => ({
      ...state,
      consigneeSttcAllError: data,
    }),
    [setConsigneeSttcAllLoading]: (state, data) => ({
      ...state,
      consigneeSttcAllLoading: data,
    }),
    [setConsigneeSttcAllSuccess]: (state, data) => ({
      ...state,
      consigneeSttcAllSuccess: data,
    }),
    [setConsigneeSttcActiveError]: (state, data) => ({
      ...state,
      consigneeSttcActiveError: data,
    }),
    [setConsigneeSttcActiveLoading]: (state, data) => ({
      ...state,
      consigneeSttcActiveLoading: data,
    }),
    [setConsigneeSttcActiveSuccess]: (state, data) => ({
      ...state,
      consigneeSttcActiveSuccess: data,
    }),
    [setSupplierSttcAllError]: (state, data) => ({
      ...state,
      supplierSttcAllError: data,
    }),
    [setSupplierSttcAllLoading]: (state, data) => ({
      ...state,
      supplierSttcAllLoading: data,
    }),
    [setSupplierSttcAllSuccess]: (state, data) => ({
      ...state,
      supplierSttcAllSuccess: data,
    }),
    [setSupplierSttcActiveError]: (state, data) => ({
      ...state,
      supplierSttcActiveError: data,
    }),
    [setSupplierSttcActiveLoading]: (state, data) => ({
      ...state,
      supplierSttcActiveLoading: data,
    }),
    [setSupplierSttcActiveSuccess]: (state, data) => ({
      ...state,
      supplierSttcActiveSuccess: data,
    }),
    [setShipperSttcAllError]: (state, data) => ({
      ...state,
      shipperSttcAllError: data,
    }),
    [setShipperSttcAllLoading]: (state, data) => ({
      ...state,
      shipperSttcAllLoading: data,
    }),
    [setShipperSttcAllSuccess]: (state, data) => ({
      ...state,
      shipperSttcAllSuccess: data,
    }),
    [setShipperSttcActiveError]: (state, data) => ({
      ...state,
      shipperSttcActiveError: data,
    }),
    [setShipperSttcActiveLoading]: (state, data) => ({
      ...state,
      shipperSttcActiveLoading: data,
    }),
    [setShipperSttcActiveSuccess]: (state, data) => ({
      ...state,
      shipperSttcActiveSuccess: data,
    }),
    [setResellerSttcAllError]: (state, data) => ({
      ...state,
      resellerSttcAllError: data,
    }),
    [setResellerSttcAllLoading]: (state, data) => ({
      ...state,
      resellerSttcAllLoading: data,
    }),
    [setResellerSttcAllSuccess]: (state, data) => ({
      ...state,
      resellerSttcAllSuccess: data,
    }),
    [setResellerSttcActiveError]: (state, data) => ({
      ...state,
      resellerSttcActiveError: data,
    }),
    [setResellerSttcActiveLoading]: (state, data) => ({
      ...state,
      resellerSttcActiveLoading: data,
    }),
    [setResellerSttcActiveSuccess]: (state, data) => ({
      ...state,
      resellerSttcActiveSuccess: data,
    }),
    [setOperatorSttcAllError]: (state, data) => ({
      ...state,
      operatorSttcAllError: data,
    }),
    [setOperatorSttcAllLoading]: (state, data) => ({
      ...state,
      operatorSttcAllLoading: data,
    }),
    [setOperatorSttcAllSuccess]: (state, data) => ({
      ...state,
      operatorSttcAllSuccess: data,
    }),
    [setOperatorSttcActiveError]: (state, data) => ({
      ...state,
      operatorSttcActiveError: data,
    }),
    [setOperatorSttcActiveLoading]: (state, data) => ({
      ...state,
      operatorSttcActiveLoading: data,
    }),
    [setOperatorSttcActiveSuccess]: (state, data) => ({
      ...state,
      operatorSttcActiveSuccess: data,
    }),
    [setOperatorNameError]: (state, data) => ({
      ...state,
      operatorNameError: data,
    }),
    [setOperatorNameLoading]: (state, data) => ({
      ...state,
      operatorNameLoading: data,
    }),
    [setOperatorNameSuccess]: (state, data) => ({
      ...state,
      operatorNameSuccess: data,
    }),
    [setRegulatorSttcAllError]: (state, data) => ({
      ...state,
      regulatorSttcAllError: data,
    }),
    [setRegulatorSttcAllLoading]: (state, data) => ({
      ...state,
      regulatorSttcAllLoading: data,
    }),
    [setRegulatorSttcAllSuccess]: (state, data) => ({
      ...state,
      regulatorSttcAllSuccess: data,
    }),
    [setRegulatorSttcActiveError]: (state, data) => ({
      ...state,
      regulatorSttcActiveError: data,
    }),
    [setRegulatorSttcActiveLoading]: (state, data) => ({
      ...state,
      regulatorSttcActiveLoading: data,
    }),
    [setRegulatorSttcActiveSuccess]: (state, data) => ({
      ...state,
      regulatorSttcActiveSuccess: data,
    }),
    [setVesselTrackingGetError]: (state, data) => ({
      ...state,
      vesselTrackingGetError: data,
    }),
    [setVesselTrackingGetLoading]: (state, data) => ({
      ...state,
      vesselTrackingGetLoading: data,
    }),
    [setVesselTrackingGetSuccess]: (state, data) => ({
      ...state,
      vesselTrackingGetSuccess: data,
    }),
    [setVesselTrackingViewError]: (state, data) => ({
      ...state,
      vesselTrackingViewError: data,
    }),
    [setVesselTrackingViewLoading]: (state, data) => ({
      ...state,
      vesselTrackingViewLoading: data,
    }),
    [setVesselTrackingViewSuccess]: (state, data) => ({
      ...state,
      vesselTrackingViewSuccess: data,
    }),
    [setOrderPerJenisPrioritasLoading]: (state, data) => ({
      ...state,
      orderPerJenisPrioritasLoading: data,
    }),
    [setOrderPerJenisPrioritasError]: (state, data) => ({
      ...state,
      orderPerJenisPrioritasError: data,
    }),
    [setOrderPerJenisPrioritasSuccess]: (state, data) => ({
      ...state,
      orderPerJenisPrioritasSuccess: data,
    }),
    [setMuatanTerbanyakLoading]: (state, data) => ({
      ...state,
      muatanTerbanyakLoading: data,
    }),
    [setMuatanTerbanyakError]: (state, data) => ({
      ...state,
      muatanTerbanyakError: data,
    }),
    [setMuatanTerbanyakSuccess]: (state, data) => ({
      ...state,
      muatanTerbanyakSuccess: data,
    }),
    [setContainerPerTahunLoading]: (state, data) => ({
      ...state,
      containerPerTahunLoading: data,
    }),
    [setContainerPerTahunError]: (state, data) => ({
      ...state,
      containerPerTahunError: data,
    }),
    [setContainerPerTahunSuccess]: (state, data) => ({
      ...state,
      containerPerTahunSuccess: data,
    }),
    [setMuatanPerWilayahLoading]: (state, data) => ({
      ...state,
      muatanPerWilayahLoading: data,
    }),
    [setMuatanPerWilayahError]: (state, data) => ({
      ...state,
      muatanPerWilayahError: data,
    }),
    [setMuatanPerWilayahSuccess]: (state, data) => ({
      ...state,
      muatanPerWilayahSuccess: data,
    }),

    [setMuatanPerWilayahPerPrioritasLoading]: (state, data) => ({
      ...state,
      muatanPerWilayahPerPrioritasLoading: data,
    }),
    [setMuatanPerWilayahPerPrioritasError]: (state, data) => ({
      ...state,
      muatanPerWilayahPerPrioritasError: data,
    }),
    [setMuatanPerWilayahPerPrioritasSuccess]: (state, data) => ({
      ...state,
      muatanPerWilayahPerPrioritasSuccess: data,
    }),

    [setOrderPaidLoading]: (state, data) => ({
      ...state,
      orderPaidLoading: data,
    }),
    [setOrderPaidError]: (state, data) => ({ ...state, orderPaidError: data }),
    [setOrderPaidSuccess]: (state, data) => ({
      ...state,
      orderPaidSuccess: data,
    }),
    [setOrderUnpaidLoading]: (state, data) => ({
      ...state,
      orderUnpaidLoading: data,
    }),
    [setOrderUnpaidError]: (state, data) => ({
      ...state,
      orderUnpaidError: data,
    }),
    [setOrderUnpaidSuccess]: (state, data) => ({
      ...state,
      orderUnpaidSuccess: data,
    }),
    [setOrderDeniedLoading]: (state, data) => ({
      ...state,
      orderDeniedLoading: data,
    }),
    [setOrderDeniedError]: (state, data) => ({
      ...state,
      orderDeniedError: data,
    }),
    [setOrderDeniedSuccess]: (state, data) => ({
      ...state,
      orderDeniedSuccess: data,
    }),
    [setAllOrderLoading]: (state, data) => ({
      ...state,
      allOrderLoading: data,
    }),
    [setAllOrderError]: (state, data) => ({ ...state, allOrderError: data }),
    [setAllOrderSuccess]: (state, data) => ({
      ...state,
      allOrderSuccess: data,
    }),
    [setOrderPerBulanLoading]: (state, data) => ({
      ...state,
      orderPerBulanLoading: data,
    }),
    [setOrderPerBulanError]: (state, data) => ({
      ...state,
      orderPerBulanError: data,
    }),
    [setOrderPerBulanSuccess]: (state, data) => ({
      ...state,
      orderPerBulanSuccess: data,
    }),
    [setMuatanPerOperatorLoading]: (state, data) => ({
      ...state,
      muatanPerOperatorLoading: data,
    }),
    [setMuatanPerOperatorError]: (state, data) => ({
      ...state,
      muatanPerOperatorError: data,
    }),
    [setMuatanPerOperatorSuccess]: (state, data) => ({
      ...state,
      muatanPerOperatorSuccess: data,
    }),
    [setPurchaseOrderLoading]: (state, data) => ({
      ...state,
      purchaseOrderLoading: data,
    }),
    [setPurchaseOrderError]: (state, data) => ({
      ...state,
      purchaseOrderError: data,
    }),
    [setPurchaseOrderSuccess]: (state, data) => ({
      ...state,
      purchaseOrderSuccess: data,
    }),
    [setDashboardCommodityLoading]: (state, data) => ({
      ...state,
      dashboardCommodityLoading: data,
    }),
    [setDashboardCommodityError]: (state, data) => ({
      ...state,
      dashboardCommodityError: data,
    }),
    [setDashboardCommoditySuccess]: (state, data) => ({
      ...state,
      dashboardCommoditySuccess: data,
    }),
    [setDeliveryOrderLoading]: (state, data) => ({
      ...state,
      deliveryOrderLoading: data,
    }),
    [setDeliveryOrderError]: (state, data) => ({
      ...state,
      deliveryOrderError: data,
    }),
    [setDeliveryOrderSuccess]: (state, data) => ({
      ...state,
      deliveryOrderSuccess: data,
    }),
    [setSisaQuotaTrayekLoading]: (state, data) => ({
      ...state,
      sisaQuotaTrayekLoading: data,
    }),
    [setSisaQuotaTrayekError]: (state, data) => ({
      ...state,
      sisaQuotaTrayekError: data,
    }),
    [setSisaQuotaTrayekSuccess]: (state, data) => ({
      ...state,
      sisaQuotaTrayekSuccess: data,
    }),
    [setVoyagePerTrayekLoading]: (state, data) => ({
      ...state,
      voyagePerTrayekLoading: data,
    }),
    [setVoyagePerTrayekError]: (state, data) => ({
      ...state,
      voyagePerTrayekError: data,
    }),
    [setVoyagePerTrayekSuccess]: (state, data) => ({
      ...state,
      voyagePerTrayekSuccess: data,
    }),
    [setDisparitasHargaLoading]: (state, data) => ({
      ...state,
      disparitasHargaLoading: data,
    }),
    [setDisparitasHargaError]: (state, data) => ({
      ...state,
      disparitasHargaError: data,
    }),
    [setDisparitasHargaSuccess]: (state, data) => ({
      ...state,
      disparitasHargaSuccess: data,
    }),
    [setRealisasiLoading]: (state, data) => ({
      ...state,
      realisasiLoading: data,
    }),
    [setRealisasiError]: (state, data) => ({ ...state, realisasiError: data }),
    [setRealisasiSuccess]: (state, data) => ({
      ...state,
      realisasiSuccess: data,
    }),
    [setWaktuTempuhLoading]: (state, data) => ({
      ...state,
      waktuTempuhLoading: data,
    }),
    [setWaktuTempuhError]: (state, data) => ({
      ...state,
      waktuTempuhError: data,
    }),
    [setWaktuTempuhSuccess]: (state, data) => ({
      ...state,
      waktuTempuhSuccess: data,
    }),
    [setJadwalKapalLoading]: (state, data) => ({
      ...state,
      jadwalKapalLoading: data,
    }),
    [setJadwalKapalError]: (state, data) => ({
      ...state,
      jadwalKapalError: data,
    }),
    [setJadwalKapalSuccess]: (state, data) => ({
      ...state,
      jadwalKapalSuccess: data,
    }),
    [setBookingVesselLoading]: (state, data) => ({
      ...state,
      bookingVesselLoading: data,
    }),
    [setBookingVesselError]: (state, data) => ({
      ...state,
      bookingVesselError: data,
    }),
    [setBookingVesselSuccess]: (state, data) => ({
      ...state,
      bookingVesselSuccess: data,
    }),
    [setDistribusiBarangLoading]: (state, data) => ({
      ...state,
      distribusiBarangLoading: data,
    }),
    [setDistribusiBarangError]: (state, data) => ({
      ...state,
      distribusiBarangError: data,
    }),
    [setDistribusiBarangSuccess]: (state, data) => ({
      ...state,
      distribusiBarangSuccess: data,
    }),
  },
  DefaultState
);

/**
 * API
 */

export function consigneeStatisticAll() {
  return async function (dispatch) {
    dispatch(setConsigneeSttcAllSuccess(false));
    dispatch(setConsigneeSttcAllError(false));
    dispatch(setConsigneeSttcAllLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_STATISTIC_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeSttcAllLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setConsigneeSttcAllSuccess(data));
      })
      .catch((err) => {
        dispatch(setConsigneeSttcAllError(true));
        dispatch(setConsigneeSttcAllLoading(false));
      });
  };
}

export function consigneeStatisticActive() {
  return async function (dispatch) {
    dispatch(setConsigneeSttcActiveSuccess(false));
    dispatch(setConsigneeSttcActiveError(false));
    dispatch(setConsigneeSttcActiveLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_STATISTIC_URL + "/active", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeSttcActiveLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setConsigneeSttcActiveSuccess(data));
      })
      .catch((err) => {
        dispatch(setConsigneeSttcActiveError(true));
        dispatch(setConsigneeSttcActiveLoading(false));
      });
  };
}

export function supplierStatisticAll() {
  return async function (dispatch) {
    dispatch(setSupplierSttcAllSuccess(false));
    dispatch(setSupplierSttcAllError(false));
    dispatch(setSupplierSttcAllLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_STATISTIC_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierSttcAllLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setSupplierSttcAllSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierSttcAllError(true));
        dispatch(setSupplierSttcAllLoading(false));
      });
  };
}

export function supplierStatisticActive() {
  return async function (dispatch) {
    dispatch(setSupplierSttcActiveSuccess(false));
    dispatch(setSupplierSttcActiveError(false));
    dispatch(setSupplierSttcActiveLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SUPPLIER_STATISTIC_URL + "/active", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setSupplierSttcActiveLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setSupplierSttcActiveSuccess(data));
      })
      .catch((err) => {
        dispatch(setSupplierSttcActiveError(true));
        dispatch(setSupplierSttcActiveLoading(false));
      });
  };
}

export function shipperStatisticAll() {
  return async function (dispatch) {
    dispatch(setShipperSttcAllSuccess(false));
    dispatch(setShipperSttcAllError(false));
    dispatch(setShipperSttcAllLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_STATISTIC_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperSttcAllLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setShipperSttcAllSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperSttcAllError(true));
        dispatch(setShipperSttcAllLoading(false));
      });
  };
}

export function shipperStatisticActive() {
  return async function (dispatch) {
    dispatch(setShipperSttcActiveSuccess(false));
    dispatch(setShipperSttcActiveError(false));
    dispatch(setShipperSttcActiveLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + SHIPPER_STATISTIC_URL + "/active", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setShipperSttcActiveLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setShipperSttcActiveSuccess(data));
      })
      .catch((err) => {
        dispatch(setShipperSttcActiveError(true));
        dispatch(setShipperSttcActiveLoading(false));
      });
  };
}

export function resellerStatisticAll() {
  return async function (dispatch) {
    dispatch(setResellerSttcAllSuccess(false));
    dispatch(setResellerSttcAllError(false));
    dispatch(setResellerSttcAllLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_STATISTIC_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerSttcAllLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setResellerSttcAllSuccess(data));
      })
      .catch((err) => {
        dispatch(setResellerSttcAllError(true));
        dispatch(setResellerSttcAllLoading(false));
      });
  };
}

export function resellerStatisticActive() {
  return async function (dispatch) {
    dispatch(setResellerSttcActiveSuccess(false));
    dispatch(setResellerSttcActiveError(false));
    dispatch(setResellerSttcActiveLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + RESELLER_STATISTIC_URL + "/active", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setResellerSttcActiveLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setResellerSttcActiveSuccess(data));
      })
      .catch((err) => {
        dispatch(setResellerSttcActiveError(true));
        dispatch(setResellerSttcActiveLoading(false));
      });
  };
}

export function operatorStatisticAll() {
  return async function (dispatch) {
    dispatch(setOperatorSttcAllSuccess(false));
    dispatch(setOperatorSttcAllError(false));
    dispatch(setOperatorSttcAllLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_STATISTIC_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorSttcAllLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOperatorSttcAllSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorSttcAllError(true));
        dispatch(setOperatorSttcAllLoading(false));
      });
  };
}

export function operatorStatisticActive() {
  return async function (dispatch) {
    dispatch(setOperatorSttcActiveSuccess(false));
    dispatch(setOperatorSttcActiveError(false));
    dispatch(setOperatorSttcActiveLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_STATISTIC_URL + "/active", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorSttcActiveLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOperatorSttcActiveSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorSttcActiveError(true));
        dispatch(setOperatorSttcActiveLoading(false));
      });
  };
}

export function getOperatorName(data) {
  return async function (dispatch) {
    dispatch(setOperatorNameSuccess(false));
    dispatch(setOperatorNameError(false));
    dispatch(setOperatorNameLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + OPERATOR_STATISTIC_URL + "/name", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setOperatorNameLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOperatorNameSuccess(data));
      })
      .catch((err) => {
        dispatch(setOperatorNameError(true));
        dispatch(setOperatorNameLoading(false));
      });
  };
}

export function regulatorStatisticAll() {
  return async function (dispatch) {
    dispatch(setRegulatorSttcAllSuccess(false));
    dispatch(setRegulatorSttcAllError(false));
    dispatch(setRegulatorSttcAllLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + REGULATOR_STATISTIC_URL + "/all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRegulatorSttcAllLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRegulatorSttcAllSuccess(data));
      })
      .catch((err) => {
        dispatch(setRegulatorSttcAllError(true));
        dispatch(setRegulatorSttcAllLoading(false));
      });
  };
}

export function regulatorStatisticActive() {
  return async function (dispatch) {
    dispatch(setRegulatorSttcActiveSuccess(false));
    dispatch(setRegulatorSttcActiveError(false));
    dispatch(setRegulatorSttcActiveLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + REGULATOR_STATISTIC_URL + "/active", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setRegulatorSttcActiveLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRegulatorSttcActiveSuccess(data));
      })
      .catch((err) => {
        dispatch(setRegulatorSttcActiveError(true));
        dispatch(setRegulatorSttcActiveLoading(false));
      });
  };
}

export function vesselTrackingGet(data) {
  return async (dispatch, getState) => {
    dispatch(setVesselTrackingGetSuccess(false));
    dispatch(setVesselTrackingGetError(false));
    dispatch(setVesselTrackingGetLoading(true));

    let token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_TRACKING_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselTrackingGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setVesselTrackingGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setVesselTrackingGetError(true));
      });
  };
}

export function vesselTrackingView(data) {
  return async function (dispatch) {
    dispatch(setVesselTrackingViewSuccess(false));
    dispatch(setVesselTrackingViewError(false));
    dispatch(setVesselTrackingViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + VESSEL_TRACKING_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setVesselTrackingViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setVesselTrackingViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setVesselTrackingViewError(true));
        dispatch(setVesselTrackingViewLoading(false));
      });
  };
}

// order per jenis prioritas
export function orderPerJenisPrioritas(data) {
  return async function (dispatch) {
    dispatch(setOrderPerJenisPrioritasLoading(true));
    dispatch(setOrderPerJenisPrioritasError(false));
    dispatch(setOrderPerJenisPrioritasSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + ORDER_PER_JENIS_PRIORITAS_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setOrderPerJenisPrioritasLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOrderPerJenisPrioritasSuccess(data));
      })
      .catch((err) => {
        dispatch(setOrderPerJenisPrioritasError(true));
        dispatch(setOrderPerJenisPrioritasLoading(false));
      });
  };
}

// lima muatan terbanyak
export function muatanTerbanyak(data) {
  return async function (dispatch) {
    dispatch(setMuatanTerbanyakLoading(true));
    dispatch(setMuatanTerbanyakError(false));
    dispatch(setMuatanTerbanyakSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + MUATAN_TERBANYAK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setMuatanTerbanyakLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setMuatanTerbanyakSuccess(data));
      })
      .catch((err) => {
        dispatch(setMuatanTerbanyakError(true));
        dispatch(setMuatanTerbanyakLoading(false));
      });
  };
}

// container pertahun
export function containerPerTahun(data) {
  return async function (dispatch) {
    dispatch(setContainerPerTahunLoading(true));
    dispatch(setContainerPerTahunError(false));
    dispatch(setContainerPerTahunSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + CONTAINER_PER_TAHUN_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setContainerPerTahunLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setContainerPerTahunSuccess(data));
      })
      .catch((err) => {
        dispatch(setContainerPerTahunError(true));
        dispatch(setContainerPerTahunLoading(false));
      });
  };
}

// muatan perwilayah
export function muatanPerWilayah(data) {
  return async function (dispatch) {
    dispatch(setMuatanPerWilayahLoading(true));
    dispatch(setMuatanPerWilayahError(false));
    dispatch(setMuatanPerWilayahSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + MUATAN_PER_WILAYAH_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setMuatanPerWilayahLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setMuatanPerWilayahSuccess(data));
      })
      .catch((err) => {
        dispatch(setMuatanPerWilayahError(true));
        dispatch(setMuatanPerWilayahLoading(false));
      });
  };
}

// muatan per wilayah per prioritas
export function muatanPerWilayahPerPrioritas(data) {
  return async (dispatch) => {
    dispatch(setMuatanPerWilayahPerPrioritasLoading(true));
    dispatch(setMuatanPerWilayahPerPrioritasError(false));
    dispatch(setMuatanPerWilayahPerPrioritasSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + MUATAN_PER_WILAYAH_PER_PRIORITAS_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setMuatanPerWilayahPerPrioritasLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setMuatanPerWilayahPerPrioritasSuccess(data));
      })
      .catch((err) => {
        dispatch(setMuatanPerWilayahPerPrioritasError(true));
      });
  };
}

// order paid
export function orderPaid() {
  return async function (dispatch) {
    dispatch(setOrderPaidLoading(true));
    dispatch(setOrderPaidError(false));
    dispatch(setOrderPaidSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + ORDER_PAID_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setOrderPaidLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOrderPaidSuccess(data));
      })
      .catch((err) => {
        dispatch(setOrderPaidError(true));
        dispatch(setOrderPaidLoading(false));
      });
  };
}

// order unpaid
export function orderUnpaid() {
  return async function (dispatch) {
    dispatch(setOrderUnpaidLoading(true));
    dispatch(setOrderUnpaidError(false));
    dispatch(setOrderUnpaidSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + ORDER_UNPAID_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setOrderUnpaidLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOrderUnpaidSuccess(data));
      })
      .catch((err) => {
        dispatch(setOrderUnpaidError(true));
        dispatch(setOrderUnpaidLoading(false));
      });
  };
}

// order paid
export function orderDenied() {
  return async function (dispatch) {
    dispatch(setOrderDeniedLoading(true));
    dispatch(setOrderDeniedError(false));
    dispatch(setOrderDeniedSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + ORDER_DENIED_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setOrderDeniedLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOrderDeniedSuccess(data));
      })
      .catch((err) => {
        dispatch(setOrderDeniedError(true));
        dispatch(setOrderDeniedLoading(false));
      });
  };
}

// all orders
export function allOrder() {
  return async function (dispatch) {
    dispatch(setAllOrderLoading(true));
    dispatch(setAllOrderError(false));
    dispatch(setAllOrderSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + ALL_ORDER_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setAllOrderLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setAllOrderSuccess(data));
      })
      .catch((err) => {
        dispatch(setAllOrderError(true));
        dispatch(setAllOrderLoading(false));
      });
  };
}

// order per bulan
export function orderPerBulan(data) {
  return async function (dispatch) {
    dispatch(setOrderPerBulanLoading(true));
    dispatch(setOrderPerBulanError(false));
    dispatch(setOrderPerBulanSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + ORDER_PER_BULAN_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setOrderPerBulanLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setOrderPerBulanSuccess(data));
      })
      .catch((err) => {
        dispatch(setOrderPerBulanError(true));
        dispatch(setOrderPerBulanLoading(false));
      });
  };
}

// muatan per operator
export function muatanPerOperator(data) {
  return async function (dispatch) {
    dispatch(setMuatanPerOperatorLoading(true));
    dispatch(setMuatanPerOperatorError(false));
    dispatch(setMuatanPerOperatorSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + MUATAN_PER_OPERATOR_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setMuatanPerOperatorLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setMuatanPerOperatorSuccess(data));
      })
      .catch((err) => {
        dispatch(setMuatanPerOperatorError(true));
        dispatch(setMuatanPerOperatorLoading(false));
      });
  };
}

// PO
export function purchaseOrder(data) {
  return async function (dispatch) {
    dispatch(setPurchaseOrderLoading(true));
    dispatch(setPurchaseOrderError(false));
    dispatch(setPurchaseOrderSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + PO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setPurchaseOrderLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setPurchaseOrderSuccess(data));
      })
      .catch((err) => {
        dispatch(setPurchaseOrderError(true));
        dispatch(setPurchaseOrderLoading(false));
      });
  };
}

// PO
export function dashboardCommodity(data) {
  return async function (dispatch) {
    dispatch(setDashboardCommodityLoading(true));
    dispatch(setDashboardCommodityError(false));
    dispatch(setDashboardCommoditySuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + DASHBOARD_COMMODITY_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setDashboardCommodityLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDashboardCommoditySuccess(data));
      })
      .catch((err) => {
        dispatch(setDashboardCommodityError(true));
        dispatch(setDashboardCommodityLoading(false));
      });
  };
}

// DO
export function deliveryOrder(data) {
  return async function (dispatch) {
    dispatch(setDeliveryOrderLoading(true));
    dispatch(setDeliveryOrderError(false));
    dispatch(setDeliveryOrderSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + DO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setDeliveryOrderLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDeliveryOrderSuccess(data));
      })
      .catch((err) => {
        dispatch(setDeliveryOrderError(true));
        dispatch(setDeliveryOrderLoading(false));
      });
  };
}

// sisa quota trayek
export function sisaQuotaTrayek(data) {
  return async function (dispatch) {
    dispatch(setSisaQuotaTrayekLoading(true));
    dispatch(setSisaQuotaTrayekError(false));
    dispatch(setSisaQuotaTrayekSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + SISA_QUOTA_TRAYEK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setSisaQuotaTrayekLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setSisaQuotaTrayekSuccess(data));
      })
      .catch((err) => {
        dispatch(setSisaQuotaTrayekError(true));
        dispatch(setSisaQuotaTrayekLoading(false));
      });
  };
}

// voyage per trayek
export function voyagePerTrayek(data) {
  return async function (dispatch) {
    dispatch(setVoyagePerTrayekLoading(true));
    dispatch(setVoyagePerTrayekError(false));
    dispatch(setVoyagePerTrayekSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + VOYAGE_PER_TRAYEK_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setVoyagePerTrayekLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setVoyagePerTrayekSuccess(data));
      })
      .catch((err) => {
        dispatch(setVoyagePerTrayekError(true));
        dispatch(setVoyagePerTrayekLoading(false));
      });
  };
}

// disparitas harga
export function disparitasHarga(data) {
  return async function (dispatch) {
    dispatch(setDisparitasHargaLoading(true));
    dispatch(setDisparitasHargaError(false));
    dispatch(setDisparitasHargaSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + DISPARITAS_HARGA_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setDisparitasHargaLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDisparitasHargaSuccess(data));
      })
      .catch((err) => {
        dispatch(setDisparitasHargaError(true));
        dispatch(setDisparitasHargaLoading(false));
      });
  };
}

// realisasi
export function realisasi(data) {
  return async function (dispatch) {
    dispatch(setRealisasiLoading(true));
    dispatch(setRealisasiError(false));
    dispatch(setRealisasiSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + REALISASI_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setRealisasiLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setRealisasiSuccess(data));
      })
      .catch((err) => {
        dispatch(setRealisasiError(true));
        dispatch(setRealisasiLoading(false));
      });
  };
}

// waktu tempuh
export function waktuTempuh(data) {
  return async function (dispatch) {
    dispatch(setWaktuTempuhLoading(true));
    dispatch(setWaktuTempuhError(false));
    dispatch(setWaktuTempuhSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + WAKTU_TEMPUH_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setWaktuTempuhLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setWaktuTempuhSuccess(data));
      })
      .catch((err) => {
        dispatch(setWaktuTempuhError(true));
        dispatch(setWaktuTempuhLoading(false));
      });
  };
}

// jadwal kapal
export function jadwalKapal(data) {
  return async function (dispatch) {
    dispatch(setJadwalKapalLoading(true));
    dispatch(setJadwalKapalError(false));
    dispatch(setJadwalKapalSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + JADWAL_KAPAL_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setJadwalKapalLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setJadwalKapalSuccess(data));
      })
      .catch((err) => {
        dispatch(setJadwalKapalError(true));
        dispatch(setJadwalKapalLoading(false));
      });
  };
}

// booking vessel
export function bookingVessel(data) {
  return async function (dispatch) {
    dispatch(setBookingVesselLoading(true));
    dispatch(setBookingVesselError(false));
    dispatch(setBookingVesselSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + BOOKING_VESSEL_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setBookingVesselLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setBookingVesselSuccess(data));
      })
      .catch((err) => {
        dispatch(setBookingVesselError(true));
        dispatch(setBookingVesselLoading(false));
      });
  };
}

// distribusi barang
export function distribusiBarang(data) {
  return async function (dispatch) {
    dispatch(setDistribusiBarangLoading(true));
    dispatch(setDistribusiBarangError(false));
    dispatch(setDistribusiBarangSuccess(false));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }

    fetch(BASE_URL + DISTRIBUSI_BARANG_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        dispatch(setDistribusiBarangLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDistribusiBarangSuccess(data));
      })
      .catch((err) => {
        dispatch(setDistribusiBarangError(true));
        dispatch(setDistribusiBarangLoading(false));
      });
  };
}

export default dashboardReducer;
