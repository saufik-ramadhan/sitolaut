import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import {
  BASE_URL,
  COMMODITY_URL,
  COMMODITY_SUPPLIER_URL,
} from "./../config/constants";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  commodityGetError: false,
  commodityGetLoading: false,
  commodityGetSuccess: false,
  commodityViewError: false,
  commodityViewLoading: false,
  commodityViewSuccess: false,
  commodityAddError: false,
  commodityAddLoading: false,
  commodityAddInvalid: false,
  commodityAddSuccess: false,
  commodityDeleteError: false,
  commodityDeleteLoading: false,
  commodityDeleteSuccess: false,

  commodityData: [],
  commodityFilter: {},
};

/**
 * ACTION
 */
const setCommodityGetError = createAction('setCommodityGetError', (data: any) => data);
const setCommodityGetLoading = createAction('setCommodityGetLoading', (data: any) => data);
const setCommodityGetSuccess = createAction('setCommodityGetSuccess', (data: any) => data);
const setCommodityViewError = createAction('setCommodityViewError', (data: any) => data);
const setCommodityViewLoading = createAction('setCommodityViewLoading', (data: any) => data);
const setCommodityViewSuccess = createAction('setCommodityViewSuccess', (data: any) => data);
const setCommodityAddError = createAction('setCommodityAddError', (data: any) => data);
const setCommodityAddLoading = createAction('setCommodityAddLoading', (data: any) => data);
const setCommodityAddInvalid = createAction('setCommodityAddInvalid', (data: any) => data);
const setCommodityAddSuccess = createAction('setCommodityAddSuccess', (data: any) => data);
const setCommodityDeleteError = createAction('setCommodityDeleteError', (data: any) => data);
const setCommodityDeleteLoading = createAction('setCommodityDeleteLoading', (data: any) => data);
const setCommodityDeleteSuccess = createAction('setCommodityDeleteSuccess', (data: any) => data);

const setCommodityData = createAction('setCommodityData', (data: any) => data);
const setCommodityFilterAdd = createAction('setCommodityFilterAdd', (data: any) => data);
const setCommodityFilterDel = createAction('setCommodityFilterDel', (data: any) => data);
const setCommodityFilterClear = createAction('setCommodityFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const commodityReducer = createReducer(
  {
    [setCommodityGetError]: (state, data) => ({
      ...state,
      commodityGetError: data,
    }),
    [setCommodityGetLoading]: (state, data) => ({
      ...state,
      commodityGetLoading: data,
    }),
    [setCommodityGetSuccess]: (state, data) => ({
      ...state,
      commodityGetSuccess: data,
    }),
    [setCommodityViewError]: (state, data) => ({
      ...state,
      commodityViewError: data,
    }),
    [setCommodityViewLoading]: (state, data) => ({
      ...state,
      commodityViewLoading: data,
    }),
    [setCommodityViewSuccess]: (state, data) => ({
      ...state,
      commodityViewSuccess: data,
    }),
    [setCommodityAddError]: (state, data) => ({
      ...state,
      commodityAddError: data,
    }),
    [setCommodityAddLoading]: (state, data) => ({
      ...state,
      commodityAddLoading: data,
    }),
    [setCommodityAddInvalid]: (state, data) => ({
      ...state,
      commodityAddInvalid: data,
    }),
    [setCommodityAddSuccess]: (state, data) => ({
      ...state,
      commodityAddSuccess: data,
    }),
    [setCommodityDeleteError]: (state, data) => ({
      ...state,
      commodityDeleteError: data,
    }),
    [setCommodityDeleteLoading]: (state, data) => ({
      ...state,
      commodityDeleteLoading: data,
    }),
    [setCommodityDeleteSuccess]: (state, data) => ({
      ...state,
      commodityDeleteSuccess: data,
    }),

    [setCommodityData]: (state, data: Array<"any">) => ({
      ...state,
      commodityData: data,
    }),
    [setCommodityFilterAdd]: (state, data: any) => ({
      ...state,
      commodityFilter: data,
    }),
    [setCommodityFilterDel]: (state, id: any) => ({
      ...state,
      commodityFilter: {},
    }),
    [setCommodityFilterClear]: (state, data: any) => ({
      ...state,
      commodityFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function commodityFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setCommodityGetSuccess(false));
    dispatch(setCommodityAddSuccess(false));
    dispatch(setCommodityGetError(false));
    dispatch(setCommodityGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMMODITY_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setCommodityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().commodity.commodityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setCommodityData(data.data));
        } else {
          dispatch(setCommodityData(prev.concat(data.data)));
        }
        dispatch(setCommodityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setCommodityGetError(true));
        dispatch(setCommodityGetLoading(false));
      });
  };
}

export function commodityFetchSub(supplier_id) {
  return async function (dispatch, getState) {
    dispatch(setCommodityGetSuccess(false));
    dispatch(setCommodityGetError(false));
    dispatch(setCommodityGetLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMMODITY_SUPPLIER_URL + "/" + String(supplier_id), config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setCommodityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().commodity.commodityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setCommodityData(data.data));
        } else {
          dispatch(setCommodityData(prev.concat(data.data)));
        }
        dispatch(setCommodityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setCommodityGetError(true));
        dispatch(setCommodityGetLoading(false));
      });
  };
}

export function commodityClear() {
  return async function (dispatch, getState) {
    dispatch(setCommodityGetSuccess(false));
    dispatch(setCommodityGetError(false));
    dispatch(setCommodityGetLoading(false));
  };
}

export function commodityFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setCommodityGetSuccess(false));
    dispatch(setCommodityAddSuccess(false));
    dispatch(setCommodityGetError(false));
    dispatch(setCommodityGetLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMMODITY_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setCommodityGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().commodity.commodityData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setCommodityData(data.data));
        } else {
          dispatch(setCommodityData(prev.concat(data.data)));
        }
        dispatch(setCommodityGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setCommodityGetError(true));
        dispatch(setCommodityGetLoading(false));
      });
  };
}

export function commodityAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setCommodityAddError(false));
    dispatch(setCommodityAddSuccess(false));
    dispatch(setCommodityAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    };
    fetch(BASE_URL + COMMODITY_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setCommodityAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((commodity) => {
        if (commodity.success === true) {
          dispatch(setCommodityAddSuccess(commodity));
        } else {
          dispatch(setCommodityAddInvalid(commodity));
        }
      })
      .catch((err) => {
        alert(JSON.stringify(err));
        dispatch(setCommodityAddError(true));
        dispatch(setCommodityAddLoading(false));
      });
  };
}

export function commodityFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setCommodityViewError(false));
    const token = await AsyncStorage.getItem("token@lcs");
    // dispatch(setCommodityViewSuccess(false));
    dispatch(setCommodityViewLoading(true));

    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMMODITY_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setCommodityViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data !== undefined) {
          dispatch(setCommodityViewLoading(false));
          dispatch(setCommodityViewSuccess(data));
        } else dispatch(setCommodityViewSuccess([{}]));
      })
      .catch((err) => {
        dispatch(setCommodityViewError(true));
        dispatch(setCommodityViewLoading(false));
      });
  };
}

export function commodityUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setCommodityAddError(false));
    dispatch(setCommodityAddSuccess(false));
    dispatch(setCommodityAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMMODITY_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setCommodityAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((commodity) => {
        if (commodity.success) {
          dispatch(setCommodityAddSuccess(commodity));
        } else {
          dispatch(setCommodityAddError(true));
          dispatch(setCommodityAddLoading(false));
        }
      })
      .catch((err) => {
        dispatch(setCommodityAddError(err));
        dispatch(setCommodityAddLoading(false));
      });
  };
}

export function commodityDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setCommodityViewSuccess(false));
    dispatch(setCommodityDeleteError(false));
    dispatch(setCommodityDeleteSuccess(false));
    dispatch(setCommodityDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + COMMODITY_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setCommodityDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setCommodityDeleteSuccess(data));
        console.log(data);
      })
      .catch((err) => {
        dispatch(setCommodityDeleteError(true));
        dispatch(setCommodityDeleteLoading(false));
        console.log(err);
      });
  };
}

/**
 * Filter
 */
export function commodityFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setCommodityFilterAdd(filter));
  };
}

export function commodityFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setCommodityFilterDel(id));
  };
}

export function commodityFilterClear() {
  return function (dispatch, getState) {
    dispatch(setCommodityFilterClear());
  };
}

export default commodityReducer;
