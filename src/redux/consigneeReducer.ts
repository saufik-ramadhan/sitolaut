import { createAction, createReducer } from "redux-act";
import { BASE_URL, CONSIGNEE_URL } from "./../config/constants";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import FetchRetry from "../services/FetchRetry";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  consigneeGetError: false,
  consigneeGetLoading: false,
  consigneeGetSuccess: false,
  consigneeViewError: false,
  consigneeViewLoading: false,
  consigneeViewSuccess: false,
  consigneeAddError: false,
  consigneeAddLoading: false,
  consigneeAddInvalid: false,
  consigneeAddSuccess: false,
  consigneeDeleteError: false,
  consigneeDeleteLoading: false,
  consigneeDeleteSuccess: false,

  consigneeData: [],
  consigneeFilter: {},
};

/**
 * ACTION
 */
const setConsigneeGetError = createAction('setConsigneeGetError', (data: any) => data);
const setConsigneeGetLoading = createAction('setConsigneeGetLoading', (data: any) => data);
const setConsigneeGetSuccess = createAction('setConsigneeGetSuccess', (data: any) => data);
const setConsigneeViewError = createAction('setConsigneeViewError', (data: any) => data);
const setConsigneeViewLoading = createAction('setConsigneeViewLoading', (data: any) => data);
const setConsigneeViewSuccess = createAction('setConsigneeViewSuccess', (data: any) => data);
const setConsigneeAddError = createAction('setConsigneeAddError', (data: any) => data);
const setConsigneeAddLoading = createAction('setConsigneeAddLoading', (data: any) => data);
const setConsigneeAddInvalid = createAction('setConsigneeAddInvalid', (data: any) => data);
const setConsigneeAddSuccess = createAction('setConsigneeAddSuccess', (data: any) => data);
const setConsigneeDeleteError = createAction('setConsigneeDeleteError', (data: any) => data);
const setConsigneeDeleteLoading = createAction('setConsigneeDeleteLoading', (data: any) => data);
const setConsigneeDeleteSuccess = createAction('setConsigneeDeleteSuccess', (data: any) => data);

const setConsigneeData = createAction('setConsigneeData', (data: any) => data);
const setConsigneeFilterAdd = createAction('setConsigneeFilterAdd', (data: any) => data);
const setConsigneeFilterDel = createAction('setConsigneeFilterDel', (data: any) => data);
const setConsigneeFilterClear = createAction('setConsigneeFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const consigneeReducer = createReducer(
  {
    [setConsigneeGetError]: (state, data) => ({
      ...state,
      consigneeGetError: data,
    }),
    [setConsigneeGetLoading]: (state, data) => ({
      ...state,
      consigneeGetLoading: data,
    }),
    [setConsigneeGetSuccess]: (state, data) => ({
      ...state,
      consigneeGetSuccess: data,
    }),
    [setConsigneeViewError]: (state, data) => ({
      ...state,
      consigneeViewError: data,
    }),
    [setConsigneeViewLoading]: (state, data) => ({
      ...state,
      consigneeViewLoading: data,
    }),
    [setConsigneeViewSuccess]: (state, data) => ({
      ...state,
      consigneeViewSuccess: data,
    }),
    [setConsigneeAddError]: (state, data) => ({
      ...state,
      consigneeAddError: data,
    }),
    [setConsigneeAddLoading]: (state, data) => ({
      ...state,
      consigneeAddLoading: data,
    }),
    [setConsigneeAddInvalid]: (state, data) => ({
      ...state,
      consigneeAddInvalid: data,
    }),
    [setConsigneeAddSuccess]: (state, data) => ({
      ...state,
      consigneeAddSuccess: data,
    }),
    [setConsigneeDeleteError]: (state, data) => ({
      ...state,
      consigneeDeleteError: data,
    }),
    [setConsigneeDeleteLoading]: (state, data) => ({
      ...state,
      consigneeDeleteLoading: data,
    }),
    [setConsigneeDeleteSuccess]: (state, data) => ({
      ...state,
      consigneeDeleteSuccess: data,
    }),

    [setConsigneeData]: (state, data: Array<"any">) => ({
      ...state,
      consigneeData: data,
    }),
    [setConsigneeFilterAdd]: (state, data: any) => ({
      ...state,
      consigneeFilter: data,
    }),
    [setConsigneeFilterDel]: (state, id: any) => ({
      ...state,
      consigneeFilter: {},
    }),
    [setConsigneeFilterClear]: (state, data: any) => ({
      ...state,
      consigneeFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function consigneeFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setConsigneeGetSuccess(false));
    dispatch(setConsigneeGetError(false));
    dispatch(setConsigneeGetLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().consignee.consigneeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setConsigneeData(data.data));
        } else {
          dispatch(setConsigneeData(prev.concat(data.data)));
        }
        dispatch(setConsigneeGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setConsigneeGetError(true));
        dispatch(setConsigneeGetLoading(false));
      });
  };
}

export function consigneeFetchSub(itemId) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeGetSuccess(false));
    dispatch(setConsigneeGetError(false));
    dispatch(setConsigneeGetLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          company: itemId,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().consignee.consigneeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setConsigneeData(data.data));
        } else {
          dispatch(setConsigneeData(prev.concat(data.data)));
        }
        dispatch(setConsigneeGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setConsigneeGetError(true));
        dispatch(setConsigneeGetLoading(false));
      });
  };
}

export function consigneeFetchPage(item) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeGetSuccess(false));
    dispatch(setConsigneeGetError(false));
    dispatch(setConsigneeGetLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().consignee.consigneeData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setConsigneeData(data.data));
        } else {
          dispatch(setConsigneeData(prev.concat(data.data)));
        }
        dispatch(setConsigneeGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setConsigneeGetError(true));
        dispatch(setConsigneeGetLoading(false));
      });
  };
}

export function consigneeAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeAddError(false));
    dispatch(setConsigneeAddInvalid(false));
    dispatch(setConsigneeAddSuccess(false));
    dispatch(setConsigneeAddLoading(true));

    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    config = {
      method: "POST",
      body: formData,
    };

    fetch(BASE_URL + CONSIGNEE_URL + "/new", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((consignee) => {
        if (consignee.success === true) {
          dispatch(setConsigneeAddSuccess(consignee));
          alert("Success");
        } else {
          dispatch(setConsigneeAddInvalid(consignee));
          alert("Invalid");
        }
      })
      .catch((err) => {
        dispatch(setConsigneeAddError(true));
        dispatch(setConsigneeAddLoading(false));
        alert("Error");
      });
  };
}

export function consigneeUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeAddError(false));
    dispatch(setConsigneeAddSuccess(false));
    dispatch(setConsigneeAddLoading(true));
    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    let formData = new FormData();
    for (var key in data) {
      formData.append(key, data[key]);
    }

    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((consignee) => {
        dispatch(setConsigneeAddSuccess(consignee));
      })
      .catch((err) => {
        dispatch(setConsigneeAddError(true));
        dispatch(setConsigneeAddLoading(false));
        alert("Fail");
      });
  };
}

export function handleStatus(data) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeAddError(false));
    dispatch(setConsigneeAddSuccess(false));
    dispatch(setConsigneeAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + "register/" + data.status, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((consignee) => {
        dispatch(setConsigneeAddSuccess(consignee));
      })
      .catch((err) => {
        dispatch(setConsigneeAddError(true));
        dispatch(setConsigneeAddLoading(false));
      });
  };
}

export function consigneeFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeViewError(false));
    dispatch(setConsigneeViewSuccess(false));
    dispatch(setConsigneeViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setConsigneeViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setConsigneeViewError(true));
        dispatch(setConsigneeViewLoading(false));
      });
  };
}

export function consigneeFetchUser(id) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeViewError(false));
    dispatch(setConsigneeViewSuccess(false));
    dispatch(setConsigneeViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    FetchRetry(BASE_URL + CONSIGNEE_URL + "/profile", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setConsigneeViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setConsigneeViewError(true));
        dispatch(setConsigneeViewLoading(false));
        dispatch(setConsigneeViewLoading(false));
      });
  };
}

export function consigneeDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setConsigneeViewSuccess(false));
    dispatch(setConsigneeDeleteError(false));
    dispatch(setConsigneeDeleteSuccess(false));
    dispatch(setConsigneeDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: id,
        }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + CONSIGNEE_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setConsigneeDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        alert("Sukses menghapus akun consignee");
        dispatch(setConsigneeDeleteSuccess(data));
      })
      .catch((err) => {
        alert("Error");
        dispatch(setConsigneeDeleteError(true));
        dispatch(setConsigneeDeleteLoading(false));
      });
  };
}

/**
 * Filter
 */
export function consigneeFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setConsigneeFilterAdd(filter));
  };
}

export function consigneeFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setConsigneeFilterDel(id));
  };
}

export function consigneeFilterClear() {
  return function (dispatch, getState) {
    dispatch(setConsigneeFilterClear());
  };
}

export default consigneeReducer;
export { setConsigneeAddSuccess };
