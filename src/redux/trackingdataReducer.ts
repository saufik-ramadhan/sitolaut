import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import AsyncStorage from "../AsyncStorage";
import {
  BASE_URL,
  PACKAGING_URL,
  TRACKING_DATA_URL,
} from "../config/constants";

/**
 * Default State
 */
const DefaultState = {
  trackingdataGetError: false,
  trackingdataGetLoading: false,
  trackingdataGetSuccess: false,

  trackingdataData: [],
  trackingdataFilter: {},
};

/**
 * ACTION
 */
const setTrackingdataGetError = createAction('setTrackingdataGetError', (data: any) => data);
const setTrackingdataGetLoading = createAction('setTrackingdataGetLoading', (data: any) => data);
const setTrackingdataGetSuccess = createAction('setTrackingdataGetSuccess', (data: any) => data);

const setTrackingdataData = createAction('setTrackingdataData', (data: any) => data);
const setTrackingdataFilterAdd = createAction('setTrackingdataFilterAdd', (data: any) => data);
const setTrackingdataFilterDel = createAction('setTrackingdataFilterDel', (data: any) => data);
const setTrackingdataFilterClear = createAction('setTrackingdataFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const trackingdataReducer = createReducer(
  {
    [setTrackingdataGetError]: (state, data) => ({
      ...state,
      trackingdataGetError: data,
    }),
    [setTrackingdataGetLoading]: (state, data) => ({
      ...state,
      trackingdataGetLoading: data,
    }),
    [setTrackingdataGetSuccess]: (state, data) => ({
      ...state,
      trackingdataGetSuccess: data,
    }),

    [setTrackingdataData]: (state, data: Array<"any">) => ({
      ...state,
      trackingdataData: data,
    }),
    [setTrackingdataFilterAdd]: (state, data: any) => ({
      ...state,
      trackingdataFilter: data,
    }),
    [setTrackingdataFilterDel]: (state, id: any) => ({
      ...state,
      trackingdataFilter: {},
    }),
    [setTrackingdataFilterClear]: (state, data: any) => ({
      ...state,
      trackingdataFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function trackingDataGet(data) {
  return async function (dispatch, getState) {
    dispatch(setTrackingdataGetSuccess(false));
    dispatch(setTrackingdataGetError(false));
    dispatch(setTrackingdataGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + TRACKING_DATA_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setTrackingdataGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().trackingdata.trackingdataData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setTrackingdataData(data.data));
        } else {
          dispatch(setTrackingdataData(prev.concat(data.data)));
        }

        dispatch(setTrackingdataGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setTrackingdataGetError(true));
      });
  };
}

/**
 * Filter
 */
export function trackingdataFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setTrackingdataFilterAdd(filter));
  };
}

export function trackingdataFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setTrackingdataFilterDel(id));
  };
}

export function trackingdataFilterClear() {
  return function (dispatch, getState) {
    dispatch(setTrackingdataFilterClear());
  };
}

export default trackingdataReducer;
