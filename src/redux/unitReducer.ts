import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { BASE_URL, UNIT_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  unitGetError: false,
  unitGetLoading: false,
  unitGetSuccess: false,
  unitViewError: false,
  unitViewLoading: false,
  unitViewSuccess: false,
  unitAddError: false,
  unitAddLoading: false,
  unitAddSuccess: false,
  unitDeleteError: false,
  unitDeleteLoading: false,
  unitDeleteSuccess: false,

  unitData: [],
  unitFilter: {},
};

/**
 * ACTION
 */
const setUnitGetError = createAction('setUnitGetError', (data: any) => data);
const setUnitGetLoading = createAction('setUnitGetLoading', (data: any) => data);
const setUnitGetSuccess = createAction('setUnitGetSuccess', (data: any) => data);
const setUnitViewError = createAction('setUnitViewError', (data: any) => data);
const setUnitViewLoading = createAction('setUnitViewLoading', (data: any) => data);
const setUnitViewSuccess = createAction('setUnitViewSuccess', (data: any) => data);
const setUnitAddError = createAction('setUnitAddError', (data: any) => data);
const setUnitAddLoading = createAction('setUnitAddLoading', (data: any) => data);
const setUnitAddSuccess = createAction('setUnitAddSuccess', (data: any) => data);
const setUnitDeleteError = createAction('setUnitDeleteError', (data: any) => data);
const setUnitDeleteLoading = createAction('setUnitDeleteLoading', (data: any) => data);
const setUnitDeleteSuccess = createAction('setUnitDeleteSuccess', (data: any) => data);

const setUnitData = createAction('setUnitData', (data: any) => data);
const setUnitFilterAdd = createAction('setUnitFilterAdd', (data: any) => data);
const setUnitFilterDel = createAction('setUnitFilterDel', (data: any) => data);
const setUnitFilterClear = createAction('setUnitFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const unitReducer = createReducer(
  {
    [setUnitGetError]: (state, data) => ({ ...state, unitGetError: data }),
    [setUnitGetLoading]: (state, data) => ({ ...state, unitGetLoading: data }),
    [setUnitGetSuccess]: (state, data) => ({ ...state, unitGetSuccess: data }),
    [setUnitViewError]: (state, data) => ({ ...state, unitViewError: data }),
    [setUnitViewLoading]: (state, data) => ({
      ...state,
      unitViewLoading: data,
    }),
    [setUnitViewSuccess]: (state, data) => ({
      ...state,
      unitViewSuccess: data,
    }),
    [setUnitAddError]: (state, data) => ({ ...state, unitAddError: data }),
    [setUnitAddLoading]: (state, data) => ({ ...state, unitAddLoading: data }),
    [setUnitAddSuccess]: (state, data) => ({ ...state, unitAddSuccess: data }),
    [setUnitDeleteError]: (state, data) => ({
      ...state,
      unitDeleteError: data,
    }),
    [setUnitDeleteLoading]: (state, data) => ({
      ...state,
      unitDeleteLoading: data,
    }),
    [setUnitDeleteSuccess]: (state, data) => ({
      ...state,
      unitDeleteSuccess: data,
    }),

    [setUnitData]: (state, data: Array<"any">) => ({
      ...state,
      unitData: data,
    }),
    [setUnitFilterAdd]: (state, data: any) => ({
      ...state,
      unitFilter: data,
    }),
    [setUnitFilterDel]: (state, id: any) => ({
      ...state,
      unitFilter: {},
    }),
    [setUnitFilterClear]: (state, data: any) => ({
      ...state,
      unitFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function unitFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setUnitGetSuccess(false));
    dispatch(setUnitGetError(false));
    dispatch(setUnitGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + UNIT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUnitGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().unit.unitData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setUnitData(data.data));
        } else {
          dispatch(setUnitData(prev.concat(data.data)));
        }
        dispatch(setUnitGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setUnitGetError(true));
      });
  };
}

export function unitFetchSub(path) {
  return async function (dispatch, getState) {
    dispatch(setUnitAddSuccess(false));
    dispatch(setUnitGetSuccess(false));
    dispatch(setUnitGetError(false));
    dispatch(setUnitGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + UNIT_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUnitGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().unit.unitData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setUnitData(data.data));
        } else {
          dispatch(setUnitData(prev.concat(data.data)));
        }
        dispatch(setUnitGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setUnitGetError(true));
      });
  };
}

export function unitFetchPage(params) {
  return async function (dispatch, getState) {
    dispatch(setUnitAddSuccess(false));
    dispatch(setUnitGetSuccess(false));
    dispatch(setUnitGetError(false));
    dispatch(setUnitGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + UNIT_URL + "/page", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUnitGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().unit.unitData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setUnitData(data.data));
        } else {
          dispatch(setUnitData(prev.concat(data.data)));
        }
        dispatch(setUnitGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setUnitGetError(true));
      });
  };
}

export function unitAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setUnitAddError(false));
    dispatch(setUnitAddSuccess(false));
    dispatch(setUnitAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + UNIT_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUnitAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((unit) => {
        dispatch(setUnitAddSuccess(unit));
      })
      .catch((err) => {
        dispatch(setUnitAddError(true));
      });
  };
}

export function unitFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setUnitViewError(false));
    dispatch(setUnitViewSuccess(false));
    dispatch(setUnitViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + UNIT_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUnitViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setUnitViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setUnitViewError(true));
      });
  };
}

export function unitUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setUnitAddError(false));
    dispatch(setUnitAddSuccess(false));
    dispatch(setUnitAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + UNIT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUnitAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((unit) => {
        if (unit.success) {
          dispatch(setUnitAddSuccess(unit));
        } else {
          dispatch(setUnitAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setUnitAddError(true));
      });
  };
}

export function unitDelete(id) {
  return async function (dispatch, getState) {
    dispatch(setUnitViewSuccess(false));
    dispatch(setUnitDeleteError(false));
    dispatch(setUnitDeleteSuccess(false));
    dispatch(setUnitDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ id: id }),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + UNIT_URL, config)
      .then((response) => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setUnitDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setUnitDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setUnitDeleteError(true));
      });
  };
}

/**
 * Filter
 */
export function unitFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setUnitFilterAdd(filter));
  };
}

export function unitFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setUnitFilterDel(id));
  };
}

export function unitFilterClear() {
  return function (dispatch, getState) {
    dispatch(setUnitFilterClear());
  };
}

export default unitReducer;
