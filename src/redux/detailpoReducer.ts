import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, DETAILPO_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  detailpoGetError: false,
  detailpoGetLoading: false,
  detailpoGetSuccess: false,
  detailpoViewError: false,
  detailpoViewLoading: false,
  detailpoViewSuccess: false,
  detailpoAddError: false,
  detailpoAddLoading: false,
  detailpoAddSuccess: false,
  detailpoDeleteError: false,
  detailpoDeleteLoading: false,
  detailpoDeleteSuccess: false,
};

/**
 * ACTION
 */
const setDetailpoGetError = createAction('setDetailpoGetError', (data: any) => data);
const setDetailpoGetLoading = createAction('setDetailpoGetLoading', (data: any) => data);
const setDetailpoGetSuccess = createAction('setDetailpoGetSuccess', (data: any) => data);
const setDetailpoViewError = createAction('setDetailpoViewError', (data: any) => data);
const setDetailpoViewLoading = createAction('setDetailpoViewLoading', (data: any) => data);
const setDetailpoViewSuccess = createAction('setDetailpoViewSuccess', (data: any) => data);
const setDetailpoAddError = createAction('setDetailpoAddError', (data: any) => data);
const setDetailpoAddLoading = createAction('setDetailpoAddLoading', (data: any) => data);
const setDetailpoAddSuccess = createAction('setDetailpoAddSuccess', (data: any) => data);
const setDetailpoDeleteError = createAction('setDetailpoDeleteError', (data: any) => data);
const setDetailpoDeleteLoading = createAction('setDetailpoDeleteLoading', (data: any) => data);
const setDetailpoDeleteSuccess = createAction('setDetailpoDeleteSuccess', (data: any) => data);

/**
 * REDUCER
 */
const detailpoReducer = createReducer(
  {
    [setDetailpoGetError]: (state, data) => ({
      ...state,
      detailpoGetError: data,
    }),
    [setDetailpoGetLoading]: (state, data) => ({
      ...state,
      detailpoGetLoading: data,
    }),
    [setDetailpoGetSuccess]: (state, data) => ({
      ...state,
      detailpoGetSuccess: data,
    }),
    [setDetailpoViewError]: (state, data) => ({
      ...state,
      detailpoViewError: data,
    }),
    [setDetailpoViewLoading]: (state, data) => ({
      ...state,
      detailpoViewLoading: data,
    }),
    [setDetailpoViewSuccess]: (state, data) => ({
      ...state,
      detailpoViewSuccess: data,
    }),
    [setDetailpoAddError]: (state, data) => ({
      ...state,
      detailpoAddError: data,
    }),
    [setDetailpoAddLoading]: (state, data) => ({
      ...state,
      detailpoAddLoading: data,
    }),
    [setDetailpoAddSuccess]: (state, data) => ({
      ...state,
      detailpoAddSuccess: data,
    }),
    [setDetailpoDeleteError]: (state, data) => ({
      ...state,
      detailpoDeleteError: data,
    }),
    [setDetailpoDeleteLoading]: (state, data) => ({
      ...state,
      detailpoDeleteLoading: data,
    }),
    [setDetailpoDeleteSuccess]: (state, data) => ({
      ...state,
      detailpoDeleteSuccess: data,
    }),
  },
  DefaultState
);

/**
 * API
 */
export function detailpoFetchAll() {
  return async function (dispatch) {
    dispatch(setDetailpoGetSuccess(false));
    dispatch(setDetailpoGetError(false));
    dispatch(setDetailpoGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDetailpoGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDetailpoGetError(true));
      });
  };
}

export function detailpoFetchSub(path) {
  return async function (dispatch) {
    dispatch(setDetailpoGetSuccess(false));
    dispatch(setDetailpoGetError(false));
    dispatch(setDetailpoGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL + "/" + path, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          let detailpo = buildGrandTotal(data);
          dispatch(setDetailpoGetSuccess(detailpo));
        } else {
          dispatch(setDetailpoGetError(true));
        }
      })
      .catch((err) => {
        dispatch(setDetailpoGetError(true));
      });
  };
}

export function detailpoFetchDistribution(item) {
  return async function (dispatch) {
    dispatch(setDetailpoGetSuccess(false));
    dispatch(setDetailpoGetError(false));
    dispatch(setDetailpoGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL + "/distribution", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          let detailpo = buildGrandTotal(data);
          dispatch(setDetailpoGetSuccess(detailpo));
        } else {
          dispatch(setDetailpoGetError(true));
        }
      })
      .catch((err) => {
        dispatch(setDetailpoGetError(true));
      });
  };
}

export function buildGrandTotal(data) {
  let total = data.data;
  if (total.length > 0) {
    function grandTotal(item) {
      return Number(item.harga_total);
    }

    function sum(prev, next) {
      return prev + next;
    }

    let totalSum = total.map(grandTotal).reduce(sum);
    data.grandTotal = Number(totalSum);
  } else {
    data.grandTotal = 0;
  }

  return data;
}

export function detailpoAdd(data) {
  return async function (dispatch) {
    dispatch(setDetailpoAddError(false));
    dispatch(setDetailpoAddSuccess(false));
    dispatch(setDetailpoAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((detailpo) => {
        dispatch(setDetailpoAddSuccess(detailpo));
      })
      .catch((err) => {
        dispatch(setDetailpoAddError(true));
      });
  };
}

export function detailpoUpdate(data) {
  return async function (dispatch) {
    dispatch(setDetailpoAddError(false));
    dispatch(setDetailpoAddSuccess(false));
    dispatch(setDetailpoAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((detailpo) => {
        dispatch(setDetailpoAddSuccess(detailpo));
      })
      .catch((err) => {
        dispatch(setDetailpoAddError(true));
      });
  };
}

export function detailpoConfirm(data) {
  return async function (dispatch) {
    dispatch(setDetailpoAddError(false));
    dispatch(setDetailpoAddSuccess(false));
    dispatch(setDetailpoAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL + "/approve", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((detailpo) => {
        if (detailpo.status === "200 OK") {
          dispatch(setDetailpoAddSuccess(detailpo));
        } else {
          dispatch(setDetailpoAddError(true));
        }
      })
      .catch((err) => {
        dispatch(setDetailpoAddError(true));
      });
  };
}

export function detailpoFetchOne(id) {
  return async function (dispatch) {
    dispatch(setDetailpoViewError(false));
    dispatch(setDetailpoViewSuccess(false));
    dispatch(setDetailpoViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDetailpoViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setDetailpoViewError(true));
      });
  };
}

export function detailpoDelete(id) {
  return async function (dispatch) {
    dispatch(setDetailpoDeleteError(false));
    dispatch(setDetailpoDeleteSuccess(false));
    dispatch(setDetailpoGetSuccess(false));
    dispatch(setDetailpoDeleteLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(id),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DETAILPO_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDetailpoDeleteLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDetailpoDeleteSuccess(data));
      })
      .catch((err) => {
        dispatch(setDetailpoDeleteError(true));
      });
  };
}

export default detailpoReducer;
