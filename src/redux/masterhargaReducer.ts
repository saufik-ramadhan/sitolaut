import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { BASE_URL } from "../config/constants";
import AsyncStorage from "../AsyncStorage";

const DefaultState = {
  masterHargaGetLoading: false,
  masterHargaGetError: false,
  masterHargaGetSuccess: false,
  isActionProcess: false,
  isActionSuccess: false,
  isActionFailure: false,
  masterHargaForm: false,
  getActiveMasterHarga: false,
};

const setMasterHargaGetLoading = createAction('setMasterHargaGetLoading', (data: any) => data);
const setMasterHargaGetError = createAction('setMasterHargaGetError', (data: any) => data);
const setMasterHargaGetSuccess = createAction('setMasterHargaGetSuccess', (data: any) => data);
const setMasterHargaForm = createAction('setMasterHargaForm', (data: any) => data);
const setGetActiveMasterHarga = createAction('setGetActiveMasterHarga', (data: any) => data);
const setIsActionProcess = createAction('setIsActionProcess', (data: any) => data);
const setIsActionSuccess = createAction('setIsActionSuccess', (data: any) => data);
const setIsActionFailure = createAction('setIsActionFailure', (data: any) => data);

const masterhargaReducer = createReducer(
  {
    [setMasterHargaGetLoading]: (state, data) => ({
      ...state,
      masterHargaGetLoading: data,
    }),
    [setMasterHargaGetError]: (state, data) => ({
      ...state,
      masterHargaGetError: data,
    }),
    [setMasterHargaGetSuccess]: (state, data) => ({
      ...state,
      masterHargaGetSuccess: data,
    }),
    [setIsActionProcess]: (state, data) => ({
      ...state,
      isActionProcess: data,
    }),
    [setIsActionSuccess]: (state, data) => ({
      ...state,
      isActionSuccess: data,
    }),
    [setIsActionFailure]: (state, data) => ({
      ...state,
      isActionFailure: data,
    }),
    [setMasterHargaForm]: (state, data) => ({
      ...state,
      masterHargaForm: {
        code: data.code,
        open: data.bool,
        status: data.status,
        data: data.data,
        message: data.message,
      },
    }),
    [setGetActiveMasterHarga]: (state, data) => ({
      ...state,
      getActiveMasterHarga: data,
    }),
  },
  DefaultState
);

export function masterHargaActionForm(open, code, status, data, message) {
  return async (dispatch) => {
    dispatch(setMasterHargaForm(open, code, status, data, message));
  };
}

export function masterHargaGetData(params) {
  params ? (params = params) : (params = { page: 1, sizePerPage: 5 });
  return async (dispatch) => {
    dispatch(setMasterHargaGetLoading(true));
    dispatch(setMasterHargaGetError(false));
    dispatch(setMasterHargaGetSuccess(false));

    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const { page, sizePerPage } = params;
    let urlParams = `?page=${page}&limit=${sizePerPage}`;
    if (params.tahun) {
      urlParams = urlParams + `&tahun=${params.tahun}`;
    }
    if (params.status) {
      urlParams = urlParams + `&status=${params.status}`;
    }
    if (params.kodeTrayek) {
      urlParams = urlParams + `&idTrayek=${params.kodeTrayek}`;
    }
    fetch(BASE_URL + "master_harga/" + urlParams, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        dispatch(setMasterHargaGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 400) {
          dispatch(setMasterHargaGetError(true));
        } else {
          // dispatch(setMasterHargaGetActive());
          dispatch(setMasterHargaGetSuccess(data.data));
        }
      })
      .catch((err) => {
        dispatch(setMasterHargaGetError(true));
      });
  };
}

export function isActions(method, params) {
  return async (dispatch) => {
    const token = await AsyncStorage.getItem("token@lcs");
    alert(JSON.stringify(params));
    let getConfig = {};
    let urlAction = BASE_URL + "master_harga/";
    // alert(JSON.stringify(params));
    switch (method) {
      case "ADD":
        getConfig["method"] = "POST";
        // getConfig['url'] = urlAction;
        getConfig["body"] = JSON.stringify(params);
        break;
      case "DELETE":
        getConfig["method"] = "DELETE";
        // getConfig['url'] = urlAction + params.id;
        urlAction = urlAction + params.id;
        break;
      case "EDIT":
        getConfig["method"] = "POST";
        // getConfig['url'] = urlAction + params.id;
        urlAction = urlAction + params.id;
        getConfig["body"] = JSON.stringify(params);
        break;
      default:
        getConfig = {};
        break;
    }

    getConfig["headers"] = {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    };

    dispatch(setIsActionProcess(true));

    fetch(urlAction, getConfig)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 400) {
          // alert(data.message);
          dispatch(setIsActionFailure(true));
        } else {
          dispatch(setIsActionSuccess(true));
        }
      })
      .catch((e) => {
        dispatch(setIsActionFailure(true));
      });
  };
}

export function saveData(params) {
  return async (dispatch) => {
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {};
    let urlRequest = BASE_URL + "master_harga/";
    let status;

    if (params.isDelete) {
      urlRequest = urlRequest + params.fId;
      config = {
        method: "DELETE",
        body: JSON.stringify({
          id: params.fId,
          keterangan: params.fKeterangan,
        }),
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
      status = "hapus";
    } else {
      if (params.fId !== 0) {
        urlRequest = urlRequest + params.fId;
        console.log(urlRequest);
        config = {
          method: "POST",
          body: JSON.stringify({
            tarif_dry: parseInt(params.fDry),
            tarif_reefer: parseInt(params.fReefer),
            tarif_curah: parseInt(params.fCurah),
            tarif_box: parseInt(params.fBox),
            tarif_referensi: params.fReferensi,
            tarif_tahun: parseInt(params.fTahun),
            tarif_status: params.fStatus,
            id_mst_kode_trayek: parseInt(params.kode),
            keterangan: params.fKeterangan,
            // status: 0
          }),
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        };
        status = "edit";
        // console.log(params);
      } else {
        config = {
          method: "POST",
          body: JSON.stringify({
            tarif_dry: parseInt(params.fDry),
            tarif_reefer: parseInt(params.fReefer),
            tarif_curah: parseInt(params.fCurah),
            tarif_box: parseInt(params.fBox),
            tarif_referensi: params.fReferensi,
            tarif_tahun: parseInt(params.fTahun),
            tarif_status: params.fStatus,
            created_by: "1",
            id_mst_kode_trayek: parseInt(params.kode),
            keterangan: params.fKeterangan,
          }),
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        };
        status = "add";
      }
    }

    fetch(urlRequest, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }

        // dispatch(setMasterHargaForm(true, 1, '', []))
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 400) {
          alert(data.message);
          dispatch(setMasterHargaForm(true, 0, status, params, data.message));
        } else {
          dispatch(setMasterHargaForm(false, 1, "", [], data.message));
          dispatch(masterHargaGetData());
        }
      })
      .catch((err) => {
        dispatch(setMasterHargaForm(true, 0, status, params));
      });
  };
}

export function masterHargaGetActive(params) {
  return async (dispatch) => {
    // dispatch(setGetActiveMasterHarga(false));
    let token = (await AsyncStorage.getItem("token@lcs")) || null;
    let config = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };

    let urlParams = `?page=1&limit=1`;

    if (params.tahun) {
      urlParams = urlParams + `&tahun=${params.tahun}`;
    }
    if (params.status) {
      urlParams = urlParams + `&status=${params.status}`;
    }
    if (params.kodeTrayek) {
      urlParams = urlParams + `&idTrayek=${params.kodeTrayek}`;
    }

    fetch(BASE_URL + "master_harga" + urlParams, config)
      .then((response) => {
        dispatch(setGetActiveMasterHarga(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === 400) {
          dispatch(setGetActiveMasterHarga(false));
        } else {
          dispatch(setGetActiveMasterHarga(data.data));
        }
      })
      .catch((err) => {
        dispatch(setGetActiveMasterHarga(false));
      });
  };
}

export function masterhargaClear() {
  return function(dispatch) {
    dispatch(setGetActiveMasterHarga(false));
  }
}

export default masterhargaReducer;
