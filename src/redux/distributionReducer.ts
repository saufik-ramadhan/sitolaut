import { createAction, createReducer } from "redux-act";
import { doLogout } from "./authReducers";
import { useSelector } from "react-redux";
import { RootState } from "./rootReducer";
import { BASE_URL, DISTRIBUTION_URL } from "./../config/constants";
import AsyncStorage from "../AsyncStorage";

/**
 * DEFAULT STATE
 */
const DefaultState = {
  distributionGetError: false,
  distributionGetLoading: false,
  distributionGetSuccess: false,
  distributionViewError: false,
  distributionViewLoading: false,
  distributionViewSuccess: false,
  distributionAddError: false,
  distributionAddLoading: false,
  distributionAddSuccess: false,

  distributionData: [],
  distributionFilter: {},
};

/**
 * ACTION
 */
const setDistributionGetError = createAction('setDistributionGetError', (data: any) => data);
const setDistributionGetLoading = createAction('setDistributionGetLoading', (data: any) => data);
const setDistributionGetSuccess = createAction('setDistributionGetSuccess', (data: any) => data);
const setDistributionViewError = createAction('setDistributionViewError', (data: any) => data);
const setDistributionViewLoading = createAction('setDistributionViewLoading', (data: any) => data);
const setDistributionViewSuccess = createAction('setDistributionViewSuccess', (data: any) => data);
const setDistributionAddError = createAction('setDistributionAddError', (data: any) => data);
const setDistributionAddLoading = createAction('setDistributionAddLoading', (data: any) => data);
const setDistributionAddSuccess = createAction('setDistributionAddSuccess', (data: any) => data);

const setDistributionData = createAction('setDistributionData', (data: any) => data);
const setDistributionFilterAdd = createAction('setDistributionFilterAdd', (data: any) => data);
const setDistributionFilterDel = createAction('setDistributionFilterDel', (data: any) => data);
const setDistributionFilterClear = createAction('setDistributionFilterClear', (data: any) => data);

/**
 * REDUCER
 */
const distributionReducer = createReducer(
  {
    [setDistributionGetError]: (state, data) => ({
      ...state,
      distributionGetError: data,
    }),
    [setDistributionGetLoading]: (state, data) => ({
      ...state,
      distributionGetLoading: data,
    }),
    [setDistributionGetSuccess]: (state, data) => ({
      ...state,
      distributionGetSuccess: data,
    }),
    [setDistributionViewError]: (state, data) => ({
      ...state,
      distributionViewError: data,
    }),
    [setDistributionViewLoading]: (state, data) => ({
      ...state,
      distributionViewLoading: data,
    }),
    [setDistributionViewSuccess]: (state, data) => ({
      ...state,
      distributionViewSuccess: data,
    }),
    [setDistributionAddError]: (state, data) => ({
      ...state,
      distributionAddError: data,
    }),
    [setDistributionAddLoading]: (state, data) => ({
      ...state,
      distributionAddLoading: data,
    }),
    [setDistributionAddSuccess]: (state, data) => ({
      ...state,
      distributionAddSuccess: data,
    }),

    [setDistributionData]: (state, data: Array<"any">) => ({
      ...state,
      distributionData: data,
    }),
    [setDistributionFilterAdd]: (state, data: any) => ({
      ...state,
      distributionFilter: data,
    }),
    [setDistributionFilterDel]: (state, id: any) => ({
      ...state,
      distributionFilter: {},
    }),
    [setDistributionFilterClear]: (state, data: any) => ({
      ...state,
      distributionFilter: {},
    }),
  },
  DefaultState
);

/**
 * API
 */
export function distributionFetchAll() {
  return async function (dispatch, getState) {
    dispatch(setDistributionAddSuccess(false));
    dispatch(setDistributionGetSuccess(false));
    dispatch(setDistributionGetError(false));
    dispatch(setDistributionGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DISTRIBUTION_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDistributionGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().distribution.distributionData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setDistributionData(data.data));
        } else {
          dispatch(setDistributionData(prev.concat(data.data)));
        }
        dispatch(setDistributionGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDistributionGetError(true));
        dispatch(setDistributionGetLoading(false));
      });
  };
}

export function distributionFetchSub(item) {
  return async function (dispatch, getState) {
    dispatch(setDistributionAddSuccess(false));
    dispatch(setDistributionGetSuccess(false));
    dispatch(setDistributionGetError(false));
    dispatch(setDistributionGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(item),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DISTRIBUTION_URL + "/get_all", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDistributionGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().distribution.distributionData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setDistributionData(data.data));
        } else {
          dispatch(setDistributionData(prev.concat(data.data)));
        }
        dispatch(setDistributionGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDistributionGetError(true));
        dispatch(setDistributionGetLoading(false));
      });
  };
}

export function distributionFetchReseller(data) {
  return async function (dispatch, getState) {
    dispatch(setDistributionGetSuccess(false));
    dispatch(setDistributionGetError(false));
    dispatch(setDistributionGetLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DISTRIBUTION_URL + "/reseller", config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDistributionGetLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        const currentPage = data.currentPage || 1;
        const start = data.start || 0;
        const prev = getState().distribution.distributionData || [];
        if (start == 0 && currentPage == 1) {
          dispatch(setDistributionData(data.data));
        } else {
          dispatch(setDistributionData(prev.concat(data.data)));
        }
        dispatch(setDistributionGetSuccess(data));
      })
      .catch((err) => {
        dispatch(setDistributionGetError(true));
        dispatch(setDistributionGetLoading(false));
      });
  };
}

export function distributionAdd(data) {
  return async function (dispatch, getState) {
    dispatch(setDistributionAddError(false));
    dispatch(setDistributionAddSuccess(false));
    dispatch(setDistributionAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + DISTRIBUTION_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDistributionAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((distribution) => {
        dispatch(setDistributionAddSuccess(distribution));
      })
      .catch((err) => {
        dispatch(setDistributionAddError(true));
        dispatch(setDistributionAddLoading(false));
      });
  };
}

export function distributionUpdate(data) {
  return async function (dispatch, getState) {
    dispatch(setDistributionAddError(false));
    dispatch(setDistributionAddSuccess(false));
    dispatch(setDistributionAddLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};

    config = {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    };
    fetch(BASE_URL + DISTRIBUTION_URL, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDistributionAddLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((distribution) => {
        dispatch(setDistributionAddSuccess(distribution));
      })
      .catch((err) => {
        dispatch(setDistributionAddError(true));
        dispatch(setDistributionAddLoading(false));
      });
  };
}

export function distributionFetchOne(id) {
  return async function (dispatch, getState) {
    dispatch(setDistributionAddSuccess(false));
    dispatch(setDistributionViewError(false));
    dispatch(setDistributionViewSuccess(false));
    dispatch(setDistributionViewLoading(true));

    const token = await AsyncStorage.getItem("token@lcs");
    let config = {};
    if (token) {
      config = {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      };
    } else {
      throw Error("Authentication error");
    }
    fetch(BASE_URL + DISTRIBUTION_URL + "/" + id, config)
      .then((response) => {
        if (response.status === 401) {
          dispatch(doLogout());
        }
        if (!response.ok) {
          throw Error(response.statusText);
        }
        dispatch(setDistributionViewLoading(false));
        return response;
      })
      .then((response) => response.json())
      .then((data) => {
        dispatch(setDistributionViewSuccess(data));
      })
      .catch((err) => {
        dispatch(setDistributionViewError(true));
        dispatch(setDistributionViewLoading(false));
      });
  };
}

/**
 * Filter
 */
export function distributionFilterAdd(filter) {
  return function (dispatch, getState) {
    dispatch(setDistributionFilterAdd(filter));
  };
}

export function distributionFilterDel(id) {
  return function (dispatch, getState) {
    dispatch(setDistributionFilterDel(id));
  };
}

export function distributionFilterClear() {
  return function (dispatch, getState) {
    dispatch(setDistributionFilterClear());
  };
}

export default distributionReducer;
