import {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../redux/rootReducer';

interface useFetch {
  priReducer: string;
  secReducer?: Array<string>;
}

export default function useFetch({
  priReducer,
  secReducer,
}: useFetch) {
  const dispatch = useDispatch();
  const { id, usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
          usertype: "Not Defined",
        };
  });

  
  const primary = useSelector(function (state: RootState) {
    return state.[`${priReducer}`];
  });

  return primary;
  // const { hasNext, currentPage } = useSelector(function (state: RootState) {
  //   return state.booking.bookingGetSuccess;
  // });
  // const filter = useSelector(function (state: RootState) {
  //   return state.booking.bookingFilter;
  // });
  // const listSupplier = useSelector(function (state: RootState) {
  //   return state.supplier.supplierGetSuccess
  //     ? state.supplier.supplierGetSuccess.data
  //     : [];
  // });
  // const listShipper = useSelector(function (state: RootState) {
  //   return state.shipper.shipperGetSuccess
  //     ? state.shipper.shipperGetSuccess.data
  //     : [];
  // });
  // const listPort = useSelector(function (state: RootState) {
  //   return state.port.portGetSuccess ? state.port.portGetSuccess.data : [];
  // });
  // const loading = useSelector(function (state: RootState) {
  //   return state.booking.bookingGetLoading;
  // });

  // function fetchData(filter) {
  //   const params = {
  //     length: 10,
  //     id: `${id}`,
  //     ...filter,
  //   };
  //   dispatch(bookingFetchConsigneeReceived(params));
  //   dispatch(shipperFetchAll());
  //   dispatch(supplierFetchAll());
  //   dispatch(portFetchAll());
  // }

  // function handleReachEnd(filter) {
  //   const params = {
  //     length: 10,
  //     id: `${id}`,
  //     start: currentPage * 10,
  //     ...filter,
  //   };
  //   dispatch(bookingFetchConsigneeReceived(params));
  // }

  // const onRefresh = () => {
  //   dispatch(bookingFilterClear());
  // };
}
