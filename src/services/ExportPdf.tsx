import React, { useEffect, useState } from "react";
import RNFetchBlob from "rn-fetch-blob";
import Share from "react-native-share";
import RNFS from "react-native-fs";
import { Alert, Platform, TouchableOpacity, View } from "react-native";
import { IconButton, Caption } from "react-native-paper";
import AsyncStorage from "../AsyncStorage";
import * as Notifications from "expo-notifications";
import moment from "moment";
import { Icon } from "../components";

type ExportPdf = {
  url: string;
  data?: any;
  filename?: string;
};
export default function ExportPdf({ url, data, filename, onPress }: ExportPdf) {
  const [downloaded, setDownloaded] = useState("");
  const download = async (url?: string, data?: object, filename?: string) => {
    let dirs = RNFetchBlob.fs.dirs;
    const token = await AsyncStorage.getItem("token@lcs");
    try {
      if (Platform.OS === "android") {
        const configOptions = { fileCache: true };
        RNFetchBlob.config(configOptions)
          .fetch(
            "POST",
            url,
            {
              Authorization: `Bearer ${token}`, //yourTokenIfHave
              "Content-Type": "application/json", // 'application/octet-stream'
            },
            JSON.stringify(data)
          )
          .then((resp) => {
            const destPath =
              RNFS.DownloadDirectoryPath +
              `/${filename}_${moment().format("DDMMYYYY")}.pdf`;
            const filePath = resp.path();
            // await Share.open(options);
            RNFS.moveFile(filePath, destPath);
            // RNFS.downloadFile({
            //   fromUrl: `file://${filePath}`,
            //   toFile: destPath,
            // });
            // RNFS.unlink(filePath);
            // RNFS.

            // const { id, promise } = RNFS.downloadFile({
            //   fromUrl: resp.path(),
            //   toFile: destPath,
            //   background: false,
            //   cacheable: false,
            //   connectionTimeout: 60 * 1000,
            //   readTimeout: 120 * 1000,
            // });
            // return resp.readFile("base64");
            return destPath;
            // return filePath;
          })
          .then((path) => {
            // First, set the handler that will cause the notification
            // to show the alert
            // FileViewer.open(path);
            // RNFS.unlink(path);
            // setDownloaded(path);
            Notifications.setNotificationHandler({
              handleNotification: async () => ({
                shouldShowAlert: true,
                shouldPlaySound: true,
                shouldSetBadge: true,
              }),
            });
            // Second, call the method
            Notifications.scheduleNotificationAsync({
              content: {
                title: "File Berhasil di Download",
                body: path,
              },
              trigger: null,
            });
            // FileViewer.open(path);
          });

        // .then(async (base64Data) => {
        //   base64Data = `data:application/pdf;base64,` + base64Data;
        //   // await Share.open({ url: base64Data });
        //   // remove the image or pdf from device's storage
        //   // await RNFS.unlink(filePath);
        //   // console.log(base64Data);
        //   // await RNFS.writeFile(destPath, base64Data);
        // });
      } else {
        RNFetchBlob.config({
          fileCache: true,
          path: dirs.DocumentDir + `/${itemPDF.fileName}`,
        })
          .fetch("POST", url, {
            Authorization: `Bearer ${token}`, //yourTokenIfHave
            "Content-Type": "application/json", // 'application/octet-stream'
            body: JSON.stringify(data),
          })
          .then(async (res) => {
            // the temp file path
            if (res && res.path()) {
              const filePath = res.path();
              let options = {
                type: "application/pdf",
                url: filePath,
              };
              await Share.open(options);
              await RNFS.unlink(filePath);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    } catch (error) {
      console.log("download: ", error);
    }
  };
  return (
    <TouchableOpacity
      onPress={function () {
        download(url, data, filename);
      }}
    >
      <View style={{ flexDirection: "row", padding: 10, alignItems: "center" }}>
        <Icon name="file-pdf" style={{ fontSize: 20, color: "maroon" }} />
      </View>
    </TouchableOpacity>
  );
}
