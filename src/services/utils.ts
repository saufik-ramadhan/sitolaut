import moment from "moment";
import Colors from "../config/Colors";
import id from "moment/locale/id";
import * as Print from "expo-print";

import { CONSIGNEE_DELETE_SUCCESS, UPLOAD_URL } from "./../config/constants";
import { useDispatch, useSelector } from "react-redux";
import { bookingFetchRo } from "../redux/bookingReducer";
import { RootState } from "../redux/rootReducer";
export const bulanId = [
  { id: 1, label: "Januari" },
  { id: 2, label: "Februari" },
  { id: 3, label: "Maret" },
  { id: 4, label: "April" },
  { id: 5, label: "Mei" },
  { id: 6, label: "Juni" },
  { id: 7, label: "Juli" },
  { id: 8, label: "Agustus" },
  { id: 9, label: "September" },
  { id: 10, label: "Oktober" },
  { id: 11, label: "November" },
  { id: 12, label: "Desember" },
];
export const jenisUser = [
  { id: 1, label: "Consignee" },
  { id: 2, label: "Supplier" },
  { id: 3, label: "Shipper" },
  { id: 4, label: "Reseller" },
  { id: 5, label: "Vessel Operator" },
  { id: 6, label: "Regulator" },
];
export const statusUser = [
  { id: 0, label: "Tidak Aktif" },
  { id: 1, label: "Aktif" },
  { id: 2, label: "Ditolak" },
];
export const monthId = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];
export const formatRupiah = (angka: any) => {
  try {
    var reverse = angka.toString().split("").reverse().join(""),
      ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join(".").split("").reverse().join("");
    return ribuan;
  } catch (error) {
    return "";
  }
};
export const onlyNumber = (text: string) => {
  return text.replace(/[^0-9]/g, '');
}
export const getBulan = (date: Date) => monthId[date.getMonth()];
export const npwpFormat = (nStr: string) => {
  return nStr.replace(
    /(\d{2})(\d{3})(\d{3})(\d{1})(\d{3})(\d{3})/,
    "$1.$2.$3.$4-$5.$6"
  );
};
export const DateFormat = (date: any) => moment(date).format("DD MMMM yyyy");
export const DateFormat2 = (date: any) => moment(date).format("DD MMM");
export const DateFormat3 = (date: any) => {
  const dates = moment(date).format("yyyy-MM-DD");
  const time = moment(date).format("HH:mm");
  return `${dates} ${time}`;
};
export const DateFormat4 = (date: any) => moment(date).format("DD/MM/YY");
export const DateFormat5 = (date: any) => {
  const dates = moment(date).format("yyyy-MM-DD");
  const time = moment(date).format("HH:mm:ss");
  return `${dates} ${time}`;
};

export const DateFormatStrip = (date: any) => moment(date).format("yyyy-MM-DD");
export const TimeFormat = (time: any) => moment(time).format("HH:mm");
export const DateTimeFormat = (date: any) => {
  const dates = moment(date).format("DD MMMM yyyy ");
  const time = moment(date).format("HH:mm");
  return `${dates} - ${time}`;
};
export const FormatTanggal = (date: Date) => {
  const tanggal = new Date(date);
  const hari = tanggal.getDate();
  const bulan = monthId[tanggal.getMonth()];
  const tahun = tanggal.getFullYear();
  return hari + " " + bulan + " " + tahun;
};

export const getUserType = (usertype: string | any) => {
  const isConsignee = usertype == "Consignee";
  const isSupplier = usertype == "Supplier";
  const isShipper = usertype == "Shipper";
  const isReseller = usertype == "Reseller";
  const isOperator = usertype == "Vessel Operator";
  const isRegulator = usertype == "Regulator";

  return {
    isConsignee,
    isSupplier,
    isShipper,
    isReseller,
    isOperator,
    isRegulator,
  };
};

/**
 * 
 * @param is_approve 
 * @param is_approve_by_shipper 
 * @default
 * status1 = is_approve == 0;
 * status2 = is_approve == 1;
 * status3 = is_approve == 2;
 * status4 = is_approve_by_shipper == 0;
   status5 = is_approve_by_shipper == 1;
   status6 = is_approve_by_shipper == 2;
 */
export const getConfirmationStatus = (
  is_approve: number,
  is_approve_by_shipper: number
) => {
  //supplier
  const status1 = is_approve == 0;
  const status2 = is_approve == 1;
  const status3 = is_approve == 2;
  //consignee
  const status4 = is_approve_by_shipper == 0;
  const status5 = is_approve_by_shipper == 1;
  const status6 = is_approve_by_shipper == 2;
  const supplierPending = status1 && status4;
  const shipperPending = status2 && status4; //
  const supplierAccepted = status2; //
  const shipperAccepted = status2 && status5;
  const supplierRejected = status3 && status4;
  const shipperRejected = status2 && status6;
  return {
    status1,
    status2,
    status3,
    status4,
    status5,
    status6,
    supplierPending,
    shipperPending,
    supplierAccepted,
    shipperAccepted,
    supplierRejected,
    shipperRejected,
  };
};

export const seStatusProps = (
  is_approve: number,
  is_approve_by_shipper: number,
  usertype: any
) => {
  const {
    status1,
    status2,
    status3,
    status4,
    status5,
    status6,
    supplierPending,
    shipperPending,
    supplierAccepted,
    shipperAccepted,
    shipperRejected,
    supplierRejected,
  } = getConfirmationStatus(is_approve, is_approve_by_shipper);

  let label, color;
  if (getUserType(usertype).isSupplier) {
    if (supplierPending) {
      label = "Menunggu Konfirmasi Anda";
    } else if (shipperRejected) {
      label = "Ditolak oleh Shipper ";
    } else if (supplierAccepted && shipperPending) {
      label = "Menunggu Konfirmasi Shipper";
    } else if (supplierRejected) {
      label = "Ditolak";
    } else if (shipperAccepted) {
      label = "Diterima oleh Shipper";
    }
  }
  if (getUserType(usertype).isShipper) {
    if (supplierAccepted && shipperPending) {
      label = "Menunggu Konfirmasi Anda";
    } else if (supplierRejected) {
      label = "Ditolak oleh Supplier";
    } else if (shipperAccepted) {
      label = "Diterima";
    } else {
      label = "Ditolak";
    }
  } else if (
    getUserType(usertype).isConsignee ||
    getUserType(usertype).isRegulator
  ) {
    if (supplierPending) {
      label = "Menunggu Konfirmasi Supplier";
    } else if (supplierRejected) {
      label = "Ditolak oleh Supplier";
    } else if (status5) {
      label = "Diterima oleh Shipper";
    } else if (shipperRejected) {
      label = "Ditolak oleh Shipper";
    } else if (supplierAccepted || shipperPending) {
      label = "Menunggu Konfirmasi Shipper";
    }
  }

  if (supplierPending || shipperPending) {
    color = Colors.gray1;
  } else if (supplierAccepted && shipperAccepted) {
    color = Colors.accepted;
  } else if (supplierRejected || shipperRejected) {
    color = Colors.danger;
  } else color = Colors.sec;

  return { label, color };
};

export const checkMin = (val, min) => {
  let result = 0;
  if (val < min) {
    let minimum = Number(val);
    result = formatRupiah(minimum);
  } else {
    result = formatRupiah(val);
  }

  return result;
};

export const printReleaseOrder = async (data) => {
  const options = {
    width: 612,
    height: 1008,
    html: `
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html>
    <head>
      <meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
      <title></title>
      <meta name="generator" content="LibreOffice 6.4.3.2 (Windows)"/>
      <meta name="created" content="2020-09-02T09:57:21.090000000"/>
      <meta name="changed" content="2020-09-02T11:05:30.964000000"/>
      <style type="text/css">
        @page { size: 595.3pt 841.9pt; margin: 56.7pt }
        td p { background: transparent }
        a:link { color: #000080; so-language: zxx; text-decoration: underline }
        a:visited { color: #800000; so-language: zxx; text-decoration: underline }
        p {
          margin: 0;
          padding: 0;
          border: 0;
          font-size: 100%;
          font: inherit;
          vertical-align: baseline;
        }
      </style>
    </head>
    <body lang="en-US" link="#000080" vlink="#800000" dir="ltr">
    <table width="643" cellpadding="0" cellspacing="0">
      <col width="160"/>
    
      <col width="320"/>
    
      <col width="163"/>
    
      <tr>
        <td width="160" valign="top" style="border: none; padding: 0pt"><p align="left">
          <img src="${
            UPLOAD_URL + data.logo_perusahaan
          }" name="Image1" align="left" width="61" height="61" border="0"/>
    <br/>
    
          </p>
        </td>
        <td width="320" style="border: none; padding: 0pt"><p align="center">
          RELEASE ORDER</p>
          <p align="center">Nomor : ${data.kode_booking}</p>
        </td>
        <td width="163" valign="top" style="border: none; padding: 0pt"><p align="left">
          <br/>
    
          </p>
        </td>
      </tr>
    </table>
    <p align="left" style="margin-bottom: 0pt; "><br/>
    
    </p>
    <table width="643" cellpadding="0" cellspacing="0">
      <col width="128"/>
    
      <col width="515"/>
    
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">POL</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.pel_asal}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">POD</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.pel_sampai}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">ETD</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${DateFormat(data.tanggal_berangkat)}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">ETA</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${DateFormat(data.tanggal_tiba)}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">Shipper</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.shipper_name}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">Consignee</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.consignee_name}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">Quantity</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.qty} Container</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">Container
          Type</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.type_container_name}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">DOG</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.dog}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">Nama
          Depot</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.nama_depot}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">Alamat
          Depot</p>
        </td>
        <td width="515" style="border: none; padding: 0pt"><p align="left">:
          ${data.alamat_depot}</p>
        </td>
      </tr>
      <tr valign="top">
        <td width="128" style="border: none; padding: 0pt"><p align="left">Nama
          Pengurus :</p>
        </td>
        <td width="515" style="border: none; padding: 0pt">
          <p align="left">   1.${data.nama_pengurus1}<br/>2.${
      data.nama_perngurus2
    }<br/>3.${data.nama_pengurus3}</p>
        </td>
      </tr>
    </table>
    <p align="left" style="margin-bottom: 0pt; ">Syarat
    pengambilan container :</p>
    <ol>
      <li><p align="left" style="margin-bottom: 0pt; ">Pengambilan
      container dilakukan oleh pengurus terdaftar dan tidak dapat
      diwakilkan</p>
      <li><p align="left" style="margin-bottom: 0pt; ">Pengambilan
      container harus menyebutkan / memberikan :</p>
    </ol>
    <ol type="a">
        <ol type="a">
          <li><p align="left" style="margin-bottom: 0pt; ; background: transparent; page-break-before: auto">
          Nomor RO ini</p>
          <li><p align="left" style="margin-bottom: 0pt; ; background: transparent">
          Identitas asli pengurus</p>
          <li><p align="left" style="margin-bottom: 0pt; ; background: transparent">
          Fotocopy identitas (poin b), sebanyak 1 lembar</p>
        </ol>
    </ol>
    <p align="left" style="margin-bottom: 0pt; ">Layanan
    pengambilan container akan dilayani setelah semua syarat diatas
    lengkap</p>
    <p align="left" style="margin-bottom: 0pt; ">Hormat
    kami</p>
    <p align="left" style="margin-bottom: 0pt; "><img src="${
      UPLOAD_URL + data.foto_ttd
    }" name="Image2" align="left" width="115" height="79">
      <br clear="left"/>
    </img>
    
    </p>
    <p align="left" style="margin-bottom: 0pt; ">${data.nama_ttd}</p>
    </body>
    </html>
    `,
  };
  await Print.printAsync(options);
};

export const printShippingInstruction = async (data) => {
  const options = {
    width: 612,
    height: 1008,
    html: `
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
    <html>
      <head>
        <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
        <title></title>
        <meta name="generator" content="LibreOffice 6.4.3.2 (Windows)" />
        <meta name="created" content="2020-09-02T11:14:56.204000000" />
        <meta name="changed" content="2020-09-02T11:29:17.381000000" />
        <style type="text/css">
          @page {
            size: 595.3pt 841.9pt;
            margin: 56.7pt;
          }
          td p {
            background: transparent;
          }
          a:link {
            color: #000080;
            so-language: zxx;
            text-decoration: underline;
          }
          a:visited {
            color: #800000;
            so-language: zxx;
            text-decoration: underline;
          }
          p {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
          }
        </style>
      </head>
      <body lang="en-US" link="#000080" vlink="#800000" dir="ltr">
        <p align="center" style="margin-bottom: 0pt; line-height: 100%">
          <b>SHIPPING INSTRUCTION</b>
        </p>
        <p align="center" style="margin-bottom: 0pt; line-height: 100%">
          Nomor: ${data.no_si}
        </p>
        <p style="margin-bottom: 0pt; line-height: 100%"><br /></p>
        <table width="643" cellpadding="4" cellspacing="0">
          <col width="23" />
    
          <col width="200" />
    
          <col width="136" />
    
          <col width="120" />
    
          <col width="122" />
    
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: 1px solid #000000;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 2.8pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">1</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: 1px solid #000000;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 2.8pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt"
                  >Nama Pengirim / Shipper</font
                >
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="border: 1px solid #000000; padding: 2.8pt"
            >
              <p>
                ${data.pengirim_name}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">2</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt"
                  >Nama Penerima / Consignee</font
                >
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.penerima_name}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">3</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Nama Kapal / Voyage</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.nama_kapal}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">4</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Kondisi Pengiriman</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                Birth to Birth
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">5</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Alamat Asal</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.pel_asal}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">6</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Alamat Tujuan</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.pel_sampai}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">7</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt"
                  >Jumlah &amp; Tipe Container</font
                >
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.jml} Container ${data.type_container_name}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">8</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Nomor Container *</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.no_container}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">9</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt"
                  >Berat Kotor / Gross Weight</font
                >
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.berat_kotor}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">10</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Pelabuhan Muat</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.pel_asal}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">11</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Pelabuhan Bongkar</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${data.pel_sampai}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">12</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt"
                  >Jadwal Keberangkatan Kapal</font
                >
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                ${DateFormat(data.tanggal_berangkat)}
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">13</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Data Barang</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                <br />
              </p>
            </td>
          </tr>
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <br />
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Nama Barang</font>
              </p>
            </td>
            <td
              width="136"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Kemasan</font>
              </p>
            </td>
            <td
              width="120"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Jumlah</font>
              </p>
            </td>
            <td
              width="122"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Berat</font>
              </p>
            </td>
          </tr>
          ${data.detail.map(
            (item, key) =>
              `
              <tr valign="top">
                <td
                  width="23"
                  style="
                    border-top: none;
                    border-bottom: 1px solid #000000;
                    border-left: 1px solid #000000;
                    border-right: none;
                    padding-top: 0pt;
                    padding-bottom: 2.8pt;
                    padding-left: 2.8pt;
                    padding-right: 0pt;
                  "
                >
                  <p>
                    <br />
                  </p>
                </td>
                <td
                  width="200"
                  style="
                    border-top: none;
                    border-bottom: 1px solid #000000;
                    border-left: 1px solid #000000;
                    border-right: none;
                    padding-top: 0pt;
                    padding-bottom: 2.8pt;
                    padding-left: 2.8pt;
                    padding-right: 0pt;
                  "
                >
                  <p>
                    <font size="2" style="font-size: 11pt">${item.nama_barang}</font>
                  </p>
                </td>
                <td
                  width="136"
                  style="
                    border-top: none;
                    border-bottom: 1px solid #000000;
                    border-left: 1px solid #000000;
                    border-right: none;
                    padding-top: 0pt;
                    padding-bottom: 2.8pt;
                    padding-left: 2.8pt;
                    padding-right: 0pt;
                  "
                >
                  <p>
                    ${item.kemasan}
                  </p>
                </td>
                <td
                  width="120"
                  style="
                    border-top: none;
                    border-bottom: 1px solid #000000;
                    border-left: 1px solid #000000;
                    border-right: none;
                    padding-top: 0pt;
                    padding-bottom: 2.8pt;
                    padding-left: 2.8pt;
                    padding-right: 0pt;
                  "
                >
                  <p>
                    ${item.jumlah_barang}
                  </p>
                </td>
                <td
                  width="122"
                  style="
                    border-top: none;
                    border-bottom: 1px solid #000000;
                    border-left: 1px solid #000000;
                    border-right: 1px solid #000000;
                    padding-top: 0pt;
                    padding-bottom: 2.8pt;
                    padding-left: 2.8pt;
                    padding-right: 2.8pt;
                  "
                >
                  <p>
                    ${item.berat_total}
                  </p>
                </td>
              </tr>
              `
          )}
          
          
          <tr valign="top">
            <td
              width="23"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">14</font>
              </p>
            </td>
            <td
              width="200"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: none;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 0pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt">Keterangan lain lain</font>
              </p>
            </td>
            <td
              colspan="3"
              width="394"
              style="
                border-top: none;
                border-bottom: 1px solid #000000;
                border-left: 1px solid #000000;
                border-right: 1px solid #000000;
                padding-top: 0pt;
                padding-bottom: 2.8pt;
                padding-left: 2.8pt;
                padding-right: 2.8pt;
              "
            >
              <p>
                <font size="2" style="font-size: 11pt"
                  >${data.deskripsi}</font
                >
              </p>
            </td>
          </tr>
        </table>
        <p style="margin-bottom: 0pt; line-height: 100%">
          Pemilik / pengirim barang telah menyatakan bahwa :
        </p>
        <ol>
          <li style="margin: 0pt">
            Barang yang dimuat adalah benar dan sesuai dengan Shipping instruction /
            packing list terlampir
          </li>
          <li>
            <p
              style="margin-bottom: 0pt; line-height: 100%; background: transparent"
            >
              <font size="2" style="font-size: 11pt"
                >Barang yang dimuat tidak melebihi batas berat/ukuran/jumlah yang
                diperkenankan oleh operator kapal tol laut.</font
              >
            </p>
          </li>
    
          <li>
            <p
              style="margin-bottom: 0pt; line-height: 100%; background: transparent"
            >
              <font size="2" style="font-size: 11pt"
                >Barang yang dimuat bukan barang yang dilarang oleh hukum RI</font
              >
            </p>
          </li>
    
          <li>
            <p
              style="margin-bottom: 0pt; line-height: 100%; background: transparent"
            >
              <font size="2" style="font-size: 11pt"
                >Menyetujui penggunaan satuan tarif, serta ketentuan pengiriman
                sesuai prosedur yang berlaku</font
              >
            </p>
          </li>
    
          <li>
            <p
              style="margin-bottom: 0pt; line-height: 100%; background: transparent"
            >
              <font size="2" style="font-size: 11pt"
                >Sanggup mengontrol harga barang yang dikirim melalui subsidi
                ini</font
              >
            </p>
          </li>
    
          <li>
            <p
              style="margin-bottom: 0pt; line-height: 100%; background: transparent"
            >
              <font size="2" style="font-size: 11pt"
                >Apabila terjadi kerusakan barang dalam proses pelayanan mengacu
                pada aturan yang berlaku</font
              >
            </p>
          </li>
    
          <li>
            <p
              style="margin-bottom: 0pt; line-height: 100%; background: transparent"
            >
              <font size="2" style="font-size: 11pt"
                >Apabila pada kemudian hari ditemukan penyimpanan terhadal hal
                tersebut diatas, maka saya yang bertanda tangan di bawah ini
                bertanggungjawab secara penuh terhadap konsekuensi yang
                terjadi</font
              >
            </p>
          </li>
        </ol>
    
        <table width="643" cellpadding="0" cellspacing="0">
          <col width="214" />
    
          <col width="138" />
    
          <col width="291" />
    
          <tr valign="top">
            <td width="214" style="border: none; padding: 0pt">
              <p><br /></p>
              <p><font size="2" style="font-size: 11pt">Pemohon</font></p>
            </td>
            <td width="138" style="border: none; padding: 0pt">
              <p><br /></p>
            </td>
            <td width="291" style="border: none; padding: 0pt">
              <p>
                <font size="2" style="font-size: 11pt"
                  >Kota Jakarta Selatan, 02 September 2020</font
                >
              </p>
              <p><br /></p>
            </td>
          </tr>
          <tr valign="top">
            <td width="214" height="59" style="border: none; padding: 0pt">
              <p><img src="${
                UPLOAD_URL + data.shipper_foto_ttd
              }" name="Image2" align="left" width="115" height="79"></p>
            </td>
            <td width="138" style="border: none; padding: 0pt">
              <p><br /></p>
            </td>
            <td width="291" style="border: none; padding: 0pt">
              <p><img src="${
                UPLOAD_URL + data.operator_foto_ttd
              }" name="Image2" align="left" width="115" height="79"></p>
            </td>
          </tr>
          <tr valign="top">
            <td width="214" style="border: none; padding: 0pt">
              <p><font size="2" style="font-size: 11pt">Toni Sucipto</font></p>
            </td>
            <td width="138" style="border: none; padding: 0pt">
              <p><br /></p>
            </td>
            <td width="291" style="border: none; padding: 0pt">
              <p><font size="2" style="font-size: 11pt">Asep</font></p>
            </td>
          </tr>
        </table>
        <p style="margin-bottom: 0pt; line-height: 100%">
          <font size="2" style="font-size: 11pt">Tembusan</font>
        </p>
        <ol>
          <li value="1">
            <p style="margin-bottom: 0pt; line-height: 100%">
              <font size="2" style="font-size: 11pt">Kementrian Perhubungan</font>
            </p>
          </li>
    
          <li>
            <p style="margin-bottom: 0pt; line-height: 100%">
              <font size="2" style="font-size: 11pt">Kementrian Perdagangan</font>
            </p>
          </li>
        </ol>
      </body>
    </html>
    
    `,
  };
  await Print.printAsync(options);
};

export const printBillOfLading = async (data) => {
  const options = {
    width: 612,
    height: 1008,
    html: `
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

    <html>
    <head>
      
      <meta http-equiv="content-type" content="text/html; charset=windows-1252"/>
      <title></title>
      <meta name="generator" content="LibreOffice 6.4.3.2 (Windows)"/>
      <meta name="created" content="2020-09-02T11:06:39.760000000"/>
      <meta name="changed" content="2020-09-02T12:05:01.375000000"/>
      
      <style type="text/css">
        body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Liberation Sans"; font-size:x-small }
        a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  } 
        a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  } 
        comment { display:none;  } 
      </style>
      
    </head>
    
    <body>
    <table cellspacing="0" border="0">
      <colgroup width="32"></colgroup>
      <colgroup width="144"></colgroup>
      <colgroup width="146"></colgroup>
      <colgroup span="3" width="85"></colgroup>
      <colgroup width="161"></colgroup>
      <tr>
        <td colspan=2 height="29" align="left" valign=middle><b>Shipper / Pengirim</b></td>
        <td align="left">${data.shipper_nama_perusahaan} ${
      data.pelabuhan_asal
    }</td>
        <td align="left"><br></td>
        <td colspan=2 align="center" valign=middle>NOMOR : ${
          data.bill_lading_number
        }</td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td colspan=2 height="29" align="left" valign=middle><b>Consignee / Penerima</b></td>
        <td align="left">${data.consignee_nama_perusahaan}</td>
        <td align="left"><br></td>
        <td colspan=2 rowspan=3 align="center" valign=middle><br><img src="${
          UPLOAD_URL + data.logo_perusahaan
        }" width=75 height=72 hspace=48 vspace=8>
        </td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td colspan=2 height="29" align="left" valign=middle><b>Notify Party</b></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td colspan=2 height="29" align="left" valign=middle><b>Kapal</b></td>
        <td align="left">Kendhaga Utama</td>
        <td align="left"><br></td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td colspan=2 height="29" align="left" valign=middle><b>Tgl. ETD</b></td>
        <td align="left">02 September 2020</td>
        <td align="left"><br></td>
        <td colspan=2 align="center" valign=middle>${
          data.operator_nama_perusahaan
        }</td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td colspan=2 height="29" align="left" valign=middle><b>Tujuan</b></td>
        <td align="left">${data.pelabuhan_tujuan}</td>
        <td align="left"><br></td>
        <td colspan=2 align="center" valign=middle><b>BILL OF LADING</b></td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td height="18" align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="36" align="center" valign=middle><b>No.</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Keterangan Merk</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Keterangan Isian</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Koli/Unit</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Berat (ton)</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Volume (m3)</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Unit/Ton/m3/Menurut Tarif</b></td>
      </tr>

      ${data.containers.map(
        (item, key) =>
          `
          <tr>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="19" align="right" sdval="1" sdnum="9216;">${String(
              key
            )}</td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left">${
              item.no_container
            }</td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left">${
              item.nama_barang
            }</td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><br></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" sdval="2" sdnum="9216;">${
              item.berat_total
            }</td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><br></td>
            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><br></td>
          </tr>
          `
      )}
      
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=3 height="19" align="left" valign=middle><b>TOTAL</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><br></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="right" sdval="2" sdnum="9216;">2</td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><br></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><br></td>
      </tr>
      <tr>
        <td height="18" align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
      </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 height="53" align="center" valign=middle><b>Unit/Ton/m3/Menurut Tarif</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Uang Tambang Satuan</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Jumlah Uang Tambang</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="center" valign=middle><b>Rincian Biaya</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b>Harus dibayar sebelum penyerahan barang di pelabuhan</b></td>
      </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 rowspan=4 height="239" align="center" valign=middle><br></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=4 align="center" valign=middle><br></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=4 align="center" valign=middle><br></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=middle>Jumlah Uang Tambang (UT)<br>Biaya tambahan    % dari UT karena pembayaran kemudian</td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=4 align="center" valign=middle><br></td>
      </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=middle>Uang dokumen, Materai<br>Biaya Lain &ndash; lain<br>Reduksi (-)</td>
        </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=middle>Ongkos Pelabuhan Muat<br>Ongkos Pelabuhan Tujuan<br>Sewa Container</td>
        </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=2 align="left" valign=middle>Total</td>
        </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="18" align="left" valign=middle><b>Jumlah yang harus dibayar di pelabuhan muat</b></td>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left"><b>Uang Tambang Satuan</b></td>
      </tr>
      <tr>
        <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=7 height="18" align="left" valign=middle><b>Terbilang :</b></td>
      </tr>
      <tr>
      <td height="18" align="left"><br></td>
      <td align="left"><br></td>
      <td align="left"><br></td>
      <td align="left"><br></td>
      <td align="left" colspan=3>${data.kota}, ${DateFormat(Date())}</td>
      </tr>
      <tr>
        <td height="18" align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td colspan=3 rowspan=2 align="left">
          <img src="${UPLOAD_URL + data.foto_ttd}" width=100 height=100>
        </td>
      </tr>
      <tr>
        <td colspan=3 align="left" valign=middle>Yang berlaku bagi surat muatan ini adalah hukum Indonesia, sesuai dengan peraturan-peraturan dalam Undang-Undang Dagang (KUHD)</td>
        <td align="left"><br></td>
        </tr>
      <tr>
        <td height="18" align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"></td>
        <td align="left"></td>
        <td align="left">PT Operator</td>
        </tr>
      <tr>
        <td height="18" align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        </tr>
      <tr>
        <td height="18" align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        <td align="left"><br></td>
        </tr>
    </table>
    <!-- ************************************************************************** -->
    </body>
    
    </html>    
    `,
  };
  await Print.printAsync(options);
};
