/**
 * withTimeout
 * Function for adding time out for each request
 * @param msecs
 * @param promise
 */
export default function withTimeout(msecs: number, promise: any) {
  const timeout = new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error('timeout'));
    }, msecs);
  });
  return Promise.race([timeout, promise]);
}
