/**
 *
 * @param url Requested URL
 * @param options Header
 * @param n Times to retry upon failure
 */

function FetchRetry(url: string, options: object, n: number = 3) {
  return fetch(url, options).catch(function (error) {
    if (n === 1) throw error;
    return fetch_retry(url, options, n - 1);
  });
}

export default FetchRetry;
