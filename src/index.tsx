import React, { useEffect } from "react";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import {
  Provider as PaperProvider,
  Paragraph,
  DefaultTheme,
  IconButton,
} from "react-native-paper";
import {
  Provider as ReduxProvider,
  useSelector,
  useDispatch,
} from "react-redux";
import {
  RootStack,
  AppStack,
  AppBottomTab,
  RegisterStack,
  AuthStack,
  ConsigneeStack,
  SupplierStack,
  ShipperStack,
  ResellerStack,
  OperatorStack,
  RegulatorStack,
  AppDrawer,
  CreatePurchaseOrderStack,
} from "./screens/Navigator";
import * as Screen from "./screens/Screen";
import {
  CardStyleInterpolators,
  createStackNavigator,
} from "@react-navigation/stack";
import Colors from "./config/Colors";
import { store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";
import { RootState } from "./redux/rootReducer";
import { Icon } from "./components";
import { consigneeFetchUser, consigneeAdd } from "./redux/consigneeReducer";
import { supplierFetchUser } from "./redux/supplierReducer";
import { shipperFetchUser } from "./redux/shipperReducer";
import { resellerFetchUser } from "./redux/resellerReducer";
import { operatorFetchUser } from "./redux/operatorReducer";
import { View, Image, StyleSheet } from "react-native";

/**
 * Register Components
 * for Consignee, Supplier, Shipper, Reseller, Operator
 */
function Register() {
  const registerSuccess = useSelector(function (state: RootState) {
    return (
      state.consignee.consigneeAddSuccess ||
      state.supplier.supplierAddSuccess ||
      state.shipper.shipperAddSuccess ||
      state.reseller.resellerAddSuccess ||
      state.operator.operatorAddSuccess
    );
  });
  return (
    <RegisterStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      {registerSuccess ? (
        <RegisterStack.Screen
          name="RegisterSuccess"
          component={Screen.RegisterSuccess}
          options={{ headerShown: false }}
        />
      ) : (
          <>
            <RegisterStack.Screen
              name="Register"
              component={Screen.Register}
              options={{
                headerShown: false,
                headerTitle: "Pendaftaran"
              }}
            />
            <RegisterStack.Screen
              name="RegisterConsignee"
              options={{
                headerTitle: "Pendaftaran Consignee",
              }}
              component={Screen.RegisterConsignee}
            />
            <RegisterStack.Screen
              name="RegisterSupplier"
              options={{
                headerTitle: "Pendaftaran Supplier",
              }}
              component={Screen.RegisterSupplier}
            />
            <RegisterStack.Screen
              name="RegisterShipper"
              options={{
                headerTitle: "Pendaftaran Shipper",
              }}
              component={Screen.RegisterShipper}
            />
            <RegisterStack.Screen
              name="RegisterReseller"
              options={{
                headerTitle: "Pendaftaran Reseller",
              }}
              component={Screen.RegisterReseller}
            />
            <RegisterStack.Screen
              name="RegisterOperator"
              options={{
                headerTitle: "Pendaftaran Operator",
              }}
              component={Screen.RegisterOperator}
            />
          </>
        )}
    </RegisterStack.Navigator>
  );
}

/**
 * Auth Component Navigator
 * Navigator for Login, Register and Forgot Password
 * Screen
 */
function AuthComponent() {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      <AuthStack.Screen name="Login" component={Screen.Login} />
      <AuthStack.Screen name="Register" component={Register} />
      <AuthStack.Screen
        name="ForgotPassword"
        component={Screen.ForgotPassword}
      />
    </AuthStack.Navigator>
  );
}

/**
 * App Bottom Tab Component
 * Navigator for Tab Component in All User Type
 * --
 */
function AppTabComponent() {
  return (
    <AppBottomTab.Navigator
      tabBarOptions={{
        activeTintColor: Colors.white,
        activeBackgroundColor: Colors.def,
        showIcon: true,
      }}
    >
      <AppBottomTab.Screen
        name="Dashboard"
        component={Screen.Dashboard}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="home"
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 20,
              }}
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Paragraph
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 12,
              }}
            >
              Beranda
            </Paragraph>
          ),
        }}
      />
      <AppBottomTab.Screen
        name="Pengaduan"
        component={Screen.Pengaduan}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="chat-alert"
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 20,
              }}
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Paragraph
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 12,
              }}
            >
              Pengaduan
            </Paragraph>
          ),
        }}
      />
      {/* <AppBottomTab.Screen
        name="Notification"
        component={Screen.Notification}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="bell"
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 20,
              }}
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Paragraph
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 12,
              }}
            >
              Notifications
            </Paragraph>
          ),
        }}
      /> */}
      <AppBottomTab.Screen
        name="Profile"
        component={Screen.Profile}
        options={{
          tabBarIcon: ({ focused }) => (
            <Icon
              name="shield-account"
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 20,
              }}
            />
          ),
          tabBarLabel: ({ focused }) => (
            <Paragraph
              style={{
                color: focused ? Colors.white : Colors.gray7,
                fontSize: 12,
              }}
            >
              Akun
            </Paragraph>
          ),
        }}
      />
    </AppBottomTab.Navigator>
  );
}

/**
 * Consignee
 * Navigator for All of Consignee Screens
 */
function Consignee() {
  return (
    <ConsigneeStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <ConsigneeStack.Screen
        name="Dashboard"
        component={AppTabComponent}
        options={{ headerShown: false }}
      />
      <ConsigneeStack.Screen
        name="UpdateProfile"
        options={{title: "Perbarui Profil"}}
        component={Screen.UpdateProfile}
      />

      <ConsigneeStack.Screen
        name="ResetPassword"
        component={Screen.ResetPassword}
        options={{title: "Reset Password"}}
      />

      {/**
       * PURCHASE ORDER SECTION
       */}
      <ConsigneeStack.Screen
        name="PurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.PurchaseOrder}
      />
      <ConsigneeStack.Screen
        name="ReadPurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.ReadPurchaseOrder}
      />
      <ConsigneeStack.Screen
        name="UpdatePurchaseOrder"
        options={{title: "Ubah Order Barang"}}
        component={Screen.UpdatePurchaseOrder}
      />
      <ConsigneeStack.Screen
        name="DeletePurchaseOrder"
        component={Screen.DeletePurchaseOrder}
      />
      <ConsigneeStack.Screen
        name="SearchPurchaseOrder"
        options={{title: "Cari Order Barang"}}
        component={Screen.SearchPurchaseOrder}
      />
      <ConsigneeStack.Screen
        name="TrackPurchaseOrder"
        component={Screen.TrackPurchaseOrder}
        options={{title:"Lacak Order Barang"}}
      />
      <ConsigneeStack.Screen
        name="TrackViewPurchaseOrder"
        component={Screen.TrackViewPurchaseOrder}
        options={{title: "Lacak Order Barang"}}
      />
      <ConsigneeStack.Screen
        name="ClaimPurchaseOrder"
        component={Screen.ClaimPurchaseOrder}
        options={{title:"Pengambilan Barang"}}
      />
      <ConsigneeStack.Screen
        name="ReadClaimPurchaseOrder"
        component={Screen.ReadClaimPurchaseOrder}
        options={{title:"Pengambilan Barang"}}
      />
      <ConsigneeStack.Screen
        name="DistributePurchaseOrder"
        component={Screen.DistributePurchaseOrder}
        options={{title: "Distribusi Barang"}}
      />
      <ConsigneeStack.Screen
        name="ReadDistributePurchaseOrder"
        component={Screen.ReadDistributePurchaseOrder}
        options={{title: "Distribusi Barang"}}
      />

      {/**
       * CREATE PURCHASE ORDER
       */}
      <ConsigneeStack.Screen
        name="ChooseSupplier"
        component={Screen.ChooseSupplier}
        options={{
          title: "Pilih Supplier",
        }}
      />
      <ConsigneeStack.Screen
        name="ChooseCommodity"
        component={Screen.ChooseCommodity}
        options={{
          title: "Pilih Komoditas",
        }}
      />
      <ConsigneeStack.Screen
        name="CommoditySummary"
        component={Screen.CommoditySummary}
        options={{
          title: "Keranjang Belanja",
        }}
      />
      <ConsigneeStack.Screen
        name="ChooseShipper"
        component={Screen.ChooseShipper}
        options={{
          title: "Pilih Shipper",
        }}
      />
      <ConsigneeStack.Screen
        name="ChooseTrayek"
        component={Screen.ChooseTrayek}
      />
      <ConsigneeStack.Screen name="Checkout" component={Screen.Checkout} />

      {/**
       * PENGADUAN
       */}
      <ConsigneeStack.Screen name="Pengaduan" component={Screen.Pengaduan} />
      <ConsigneeStack.Screen
        name="CreatePengaduan"
        component={Screen.CreatePengaduan}
        options={{title:"Buat Pengaduan"}}
      />
      <ConsigneeStack.Screen
        name="ReadPengaduan"
        component={Screen.ReadPengaduan}
        options={{title: "Pengaduan"}}
      />
      <ConsigneeStack.Screen
        name="UpdatePengaduan"
        component={Screen.UpdatePengaduan}
        options={{title: "Ubah Pengaduan"}}
      />
      <ConsigneeStack.Screen
        name="SearchPengaduan"
        component={Screen.SearchPengaduan}
        options={{title: "Cari Pengaduan"}}
      />
      <ConsigneeStack.Screen
        name="FilterPengaduan"
        component={Screen.FilterPengaduan}
        options={{title: "Filter Pengaduan"}}
      />
      <ConsigneeStack.Screen
        name="DownloadPanduan"
        component={Screen.DownloadPanduan}
        options={{title: "Unduh Panduan"}}
      />

      {/**
       * JADWAL
       */}
      <ConsigneeStack.Screen name="Jadwal" component={Screen.Jadwal} />
      <ConsigneeStack.Screen name="ReadJadwal" component={Screen.ReadJadwal} options={{title: 'Jadwal'}} />
      <ConsigneeStack.Screen
        name="FilterJadwal"
        component={Screen.FilterJadwal}
        options={{title: "Filter Jadwal"}}
      />

      {/**
       * Harga Jual Barang
       */}
      <ConsigneeStack.Screen
        name="HargaJualBarang"
        component={Screen.HargaJualBarang}
        options={{title:'Harga Jual Barang'}}
      />
      <ConsigneeStack.Screen
        name="ReadHargaJualBarang"
        component={Screen.ReadHargaJualBarang}
        options={{title:'Harga Jual Barang'}}
      />

      {/**
       * VESSEL GEO LOCATION
       */}
      <ConsigneeStack.Screen
        name="VesselGeoLocation"
        options={{title: "Peta Kapal Tol Laut"}}
        component={Screen.VesselGeoLocation}
      />
    </ConsigneeStack.Navigator>
  );
}

/**
 * Supplier
 * Navigator for All of Supplier Screens
 * --
 */
function Supplier() {
  return (
    <SupplierStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <SupplierStack.Screen
        name="Dashboard"
        component={AppTabComponent}
        options={{ headerShown: false }}
      />
      <SupplierStack.Screen
        name="UpdateProfile"
        options={{title: "Perbarui Profil"}}
        component={Screen.UpdateProfile}
      />

      <SupplierStack.Screen
        name="ResetPassword"
        component={Screen.ResetPassword}
        options={{title: "Reset Password"}}
      />

      {/**
       * Purchase Order
       */}
      <SupplierStack.Screen
        name="PurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.PurchaseOrder}
      />
      <SupplierStack.Screen
        name="ReadPurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.ReadPurchaseOrder}
      />
      <SupplierStack.Screen
        name="UpdatePurchaseOrder"
        options={{title: "Ubah Order Barang"}}
        component={Screen.UpdatePurchaseOrder}
      />
      <SupplierStack.Screen
        name="SearchPurchaseOrder"
        options={{title: "Cari Order Barang"}}
        component={Screen.SearchPurchaseOrder}
      />
      <SupplierStack.Screen
        name="FilterPurchaseOrder"
        component={Screen.FilterPurchaseOrder}
        options={{headerTitle:"Filter Daftar Pembelian"}}
      />
      <SupplierStack.Screen
        name="TrackPurchaseOrder"
        component={Screen.TrackPurchaseOrder}
        options={{title:"Lacak Order Barang"}}
      />
      <SupplierStack.Screen
        name="TrackViewPurchaseOrder"
        component={Screen.TrackViewPurchaseOrder}
        options={{title: "Lacak Order Barang"}}
      />

      {/**
       * Commodity
       */}
      <SupplierStack.Screen name="Commodity" component={Screen.Commodity} options={{title:"Komoditas"}} />
      <SupplierStack.Screen
        name="CreateCommodity"
        component={Screen.CreateCommodity}
        options={{title: "Komoditas Baru"}}
      />
      <SupplierStack.Screen
        name="ReadCommodity"
        component={Screen.ReadCommodity}
        options={{title: "Komoditas"}}
      />
      <SupplierStack.Screen
        name="UpdateCommodity"
        component={Screen.UpdateCommodity}
        options={{title: "Ubah Komoditas"}}
      />
      <SupplierStack.Screen
        name="DeleteCommodity"
        component={Screen.DeleteCommodity}
        options={{title: "Hapus Komoditas"}}
      />
      <SupplierStack.Screen
        name="SearchCommodity"
        component={Screen.SearchCommodity}
        options={{title: "Cari Komoditas"}}
      />
      <SupplierStack.Screen
        name="FilterCommodity"
        component={Screen.FilterCommodity}
        options={{title: "Filter Komoditas"}}
      />

      {/**
       * Pengaduan
       */}
      <SupplierStack.Screen name="Pengaduan" component={Screen.Pengaduan} />
      <SupplierStack.Screen
        name="CreatePengaduan"
        component={Screen.CreatePengaduan}
        options={{title:"Buat Pengaduan"}}
      />
      <SupplierStack.Screen
        name="ReadPengaduan"
        component={Screen.ReadPengaduan}
        options={{title: "Pengaduan"}}
      />
      <SupplierStack.Screen
        name="UpdatePengaduan"
        component={Screen.UpdatePengaduan}
        options={{title: "Ubah Pengaduan"}}
      />
      <SupplierStack.Screen
        name="DownloadPanduan"
        component={Screen.DownloadPanduan}
        options={{title: "Unduh Panduan"}}
      />

      {/**
       * JADWAL
       */}
      <SupplierStack.Screen name="Jadwal" component={Screen.Jadwal} />
      <SupplierStack.Screen name="ReadJadwal" component={Screen.ReadJadwal} options={{title: 'Jadwal'}} />
      <SupplierStack.Screen
        name="FilterJadwal"
        component={Screen.FilterJadwal}
        options={{title: "Filter Jadwal"}}
      />

      {/**
       * VESSEL GEO LOCATION
       */}
      <SupplierStack.Screen
        name="VesselGeoLocation"
        options={{title: "Peta Kapal Tol Laut"}}
        component={Screen.VesselGeoLocation}
      />
    </SupplierStack.Navigator>
  );
}

/**
 * Shipper
 * Navigator for All of Shipper Screens
 * --
 */
function Shipper() {
  return (
    <ShipperStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <ShipperStack.Screen
        name="Dashboard"
        component={AppTabComponent}
        options={{
          headerShown: false,
        }}
      />
      <ShipperStack.Screen
        name="UpdateProfile"
        options={{title: "Perbarui Profil"}}
        component={Screen.UpdateProfile}
      />

      <ShipperStack.Screen
        name="ResetPassword"
        component={Screen.ResetPassword}
        options={{title: "Reset Password"}}
      />

      {/**
       * Purchase Order
       */}
      <ShipperStack.Screen
        name="PurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.PurchaseOrder}
      />
      <ShipperStack.Screen
        name="ReadPurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.ReadPurchaseOrder}
      />
      <ShipperStack.Screen
        name="UpdatePurchaseOrder"
        options={{title: "Ubah Order Barang"}}
        component={Screen.UpdatePurchaseOrder}
      />
      <ShipperStack.Screen
        name="SearchPurchaseOrder"
        options={{title: "Cari Order Barang"}}
        component={Screen.SearchPurchaseOrder}
      />
      <ShipperStack.Screen
        name="FilterPurchaseOrder"
        component={Screen.FilterPurchaseOrder}
      />
      <ShipperStack.Screen
        name="TrackPurchaseOrder"
        component={Screen.TrackPurchaseOrder}
        options={{title:"Lacak Order Barang"}}
      />
      <ShipperStack.Screen
        name="TrackViewPurchaseOrder"
        component={Screen.TrackViewPurchaseOrder}
        options={{title: "Lacak Order Barang"}}
      />

      {/**
       * Vessel Booking
       */}
      <ShipperStack.Screen
        name="VesselBooking"
        component={Screen.VesselBooking}
        options={{ title: "Booking Kapal" }}
      />
      <ShipperStack.Screen
        name="CreateVesselBooking"
        component={Screen.CreateVesselBooking}
        options={{ title: "Buat Booking Kapal"}}
      />
      <ShipperStack.Screen
        name="ReadVesselBooking"
        component={Screen.ReadVesselBooking}
        options={{ title: "Detil Booking" }}
      />
      <ShipperStack.Screen
        name="UpdateVesselBooking"
        component={Screen.UpdateVesselBooking}
        options={{ title: "Update Booking" }}
      />
      <ShipperStack.Screen
        name="DeleteVesselBooking"
        component={Screen.DeleteVesselBooking}
        options={{ title: "Hapus Booking" }}
      />
      <ShipperStack.Screen
        name="SearchVesselBooking"
        component={Screen.SearchVesselBooking}
        options={{ title: "Cari Booking" }}
      />
      <ShipperStack.Screen
        name="FilterVesselBooking"
        options={{ title: "Filter Booking" }}
        component={Screen.FilterVesselBooking}
      />
      <ShipperStack.Screen
        name="EditContainer"
        options={{ title: "Edit Container" }}
        component={Screen.EditContainer}
      />
      <ShipperStack.Screen
        name="PembayaranBooking"
        options={{ title: "Pembayaran" }}
        component={Screen.PembayaranBooking}
      />

      {/**
       * Pengaduan
       */}
      <ShipperStack.Screen name="Pengaduan" component={Screen.Pengaduan} />
      <ShipperStack.Screen
        name="CreatePengaduan"
        component={Screen.CreatePengaduan}
        options={{title:"Buat Pengaduan"}}
      />
      <ShipperStack.Screen
        name="UpdatePengaduan"
        component={Screen.UpdatePengaduan}
        options={{title: "Ubah Pengaduan"}}
      />
      <ShipperStack.Screen
        name="DownloadPanduan"
        component={Screen.DownloadPanduan}
        options={{title: "Unduh Panduan"}}
      />

      {/**
       * Biaya Pengurusan
       */}
      <ShipperStack.Screen
        name="BiayaPengurusan"
        component={Screen.BiayaPengurusan}
        options={{ title: "Biaya Pengurusan" }}
      />
      <ShipperStack.Screen
        name="UpdateBiayaPengurusan"
        component={Screen.UpdateBiayaPengurusan}
        options={{ title: "Update Biaya Pengurusan" }}
      />
      <ShipperStack.Screen
        name="AddBiayaPengurusan"
        component={Screen.AddBiayaPengurusan}
        options={{ title: "Tambah Biaya Pengurusan" }}
      />
      <ShipperStack.Screen
        name="FormVesselBooking"
        component={Screen.FormVesselBooking}
        options={{ title: "Booking Route" }}
      />
      <ShipperStack.Screen
        name="FormPackingList"
        component={Screen.FormPackingList}
        options={{ title: "Booking Route" }}
      />

      {/**
       * VESSEL GEO LOCATION
       */}
      <ShipperStack.Screen
        name="VesselGeoLocation"
        options={{title: "Peta Kapal Tol Laut"}}
        component={Screen.VesselGeoLocation}
      />
    </ShipperStack.Navigator>
  );
}

/**
 * Reseller
 * Navigator for all of Reseller screens
 * --
 */
function Reseller() {
  return (
    <ResellerStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <ResellerStack.Screen
        name="Dashboard"
        component={AppTabComponent}
        options={{
          headerShown: false,
        }}
      />
      <ResellerStack.Screen
        name="UpdateProfile"
        options={{title: "Perbarui Profil"}}
        component={Screen.UpdateProfile}
      />

      <ResellerStack.Screen
        name="ResetPassword"
        component={Screen.ResetPassword}
        options={{title: "Reset Password"}}
      />

      {/**
       * Barang
       */}
      <ResellerStack.Screen name="Goods" component={Screen.Goods} options={{title: "Daftar Barang"}} />
      <ResellerStack.Screen name="ReadGoods" component={Screen.ReadGoods} options={{title:"Barang"}}/>
      <ResellerStack.Screen name="UpdateGoods" component={Screen.UpdateGoods} options={{title: "Perbarui Barang"}}/>
      <ResellerStack.Screen name="SearchGoods" component={Screen.SearchGoods} options={{title: "Cari Barang"}}/>
      <ResellerStack.Screen name="FilterGoods" component={Screen.FilterGoods} options={{title: "Filter Barang"}}/>

      {/**
       * JADWAL
       */}
      <ResellerStack.Screen name="Jadwal" component={Screen.Jadwal} />
      <ResellerStack.Screen name="ReadJadwal" component={Screen.ReadJadwal} options={{title: 'Jadwal'}} />
      <ResellerStack.Screen
        name="FilterJadwal"
        component={Screen.FilterJadwal}
        options={{title: "Filter Jadwal"}}
      />

      {/**
       * Pengaduan
       */}
      <ResellerStack.Screen name="Pengaduan" component={Screen.Pengaduan} />
      <ResellerStack.Screen
        name="CreatePengaduan"
        component={Screen.CreatePengaduan}
        options={{title:"Buat Pengaduan"}}
      />
      <ResellerStack.Screen
        name="UpdatePengaduan"
        component={Screen.UpdatePengaduan}
        options={{title: "Ubah Pengaduan"}}
      />
      <ResellerStack.Screen
        name="DownloadPanduan"
        component={Screen.DownloadPanduan}
        options={{title: "Unduh Panduan"}}
      />

      {/**
       * VESSEL GEO LOCATION
       */}
      <ResellerStack.Screen
        name="VesselGeoLocation"
        options={{title: "Peta Kapal Tol Laut"}}
        component={Screen.VesselGeoLocation}
      />
    </ResellerStack.Navigator>
  );
}

/**
 * Operator
 * Navigator for all of Operator screens
 * --
 */
function Operator() {
  return (
    <OperatorStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <OperatorStack.Screen
        name="Dashboard"
        component={AppTabComponent}
        options={{ headerShown: false }}
      />
      <OperatorStack.Screen
        name="UpdateProfile"
        options={{title: "Perbarui Profil"}}
        component={Screen.UpdateProfile}
      />

      <OperatorStack.Screen
        name="ResetPassword"
        component={Screen.ResetPassword}
        options={{title: "Reset Password"}}
      />

      {/**
       * Trayek
       */}
      <OperatorStack.Screen name="Trayek" component={Screen.Trayek} />
      <OperatorStack.Screen
        name="CreateTrayek"
        component={Screen.CreateTrayek}
        options={{title: "Buat Trayek"}}
      />
      <OperatorStack.Screen name="ReadTrayek" component={Screen.ReadTrayek} options={{title:"Trayek"}} />
      <OperatorStack.Screen
        name="UpdateTrayek"
        component={Screen.UpdateTrayek}
        options={{title: "Ubah Trayek"}}
      />
      <OperatorStack.Screen
        name="DeleteTrayek"
        component={Screen.DeleteTrayek}
        options={{title: "Hapus Trayek"}}
      />
      <OperatorStack.Screen
        name="SearchTrayek"
        component={Screen.SearchTrayek}
        options={{title: "Cari Trayek"}}
      />
      <OperatorStack.Screen
        name="FilterTrayek"
        component={Screen.FilterTrayek}
        options={{title: "Filter Trayek"}}
      />

      {/**
       * Pelabuhan
       */}
      <OperatorStack.Screen name="Pelabuhan" component={Screen.Pelabuhan} options={{title:'Pelabuhan'}}/>
      <OperatorStack.Screen
        name="CreatePelabuhan"
        component={Screen.CreatePelabuhan}
        options={{title: "Pelabuhan Baru"}}
      />
      <OperatorStack.Screen
        name="ReadPelabuhan"
        component={Screen.ReadPelabuhan}
        options={{title:'Pelabuhan'}}
      />
      <OperatorStack.Screen
        name="UpdatePelabuhan"
        component={Screen.UpdatePelabuhan}
        options={{title:'Ubah Pelabuhan'}}
      />
      <OperatorStack.Screen
        name="DeletePelabuhan"
        component={Screen.DeletePelabuhan}
        options={{title: "Hapus Pelabuhan"}}
      />
      <OperatorStack.Screen
        name="SearchPelabuhan"
        component={Screen.SearchPelabuhan}
        options={{title: "Cari Pelabuhan"}}
      />
      <OperatorStack.Screen
        name="FilterPelabuhan"
        component={Screen.FilterPelabuhan}
        options={{title:'Filter Pelabuhan'}}
      />

      {/**
       * Depot
       */}
      <OperatorStack.Screen name="Depot" component={Screen.Depot} options={{title: "Depot"}} />
      <OperatorStack.Screen name="CreateDepot" component={Screen.CreateDepot} options={{title: "Buat Depots"}} />
      <OperatorStack.Screen name="ReadDepot" component={Screen.ReadDepot} options={{title:"Depot"}} />
      <OperatorStack.Screen name="UpdateDepot" component={Screen.UpdateDepot} options={{title: "Ubah Depot"}} />
      <OperatorStack.Screen name="DeleteDepot" component={Screen.DeleteDepot} options={{title: "Hapus Depot"}} />
      <OperatorStack.Screen name="SearchDepot" component={Screen.SearchDepot} options={{title: "Cari Depot"}} />
      <OperatorStack.Screen name="FilterDepot" component={Screen.FilterDepot} options={{title: "Filter Depot"}} />

      {/**
       * Jadwal
       */}
      <OperatorStack.Screen name="Jadwal" component={Screen.Jadwal} />
      <OperatorStack.Screen
        name="JadwalOperator"
        options={{
          title: "Jadwal Operator",
        }}
        component={Screen.JadwalOperator}
      />
      <OperatorStack.Screen
        name="CreateJadwal"
        component={Screen.CreateJadwal}
        options={{title: "Buat Jadwal"}}
      />
      <OperatorStack.Screen
        name="CreateJadwalOperator"
        component={Screen.CreateJadwalOperator}
        options={{title:'Buat Jadwal Kapal'}}
      />
      <OperatorStack.Screen name="ReadJadwal" component={Screen.ReadJadwal} options={{title: 'Jadwal'}} />
      <OperatorStack.Screen
        name="UpdateJadwal"
        component={Screen.UpdateJadwal}
        options={{title: "Ubah Jadwal"}}
      />
      <OperatorStack.Screen
        name="DeleteJadwal"
        component={Screen.DeleteJadwal}
        options={{title: "Hapus Jadwal"}}
      />
      <OperatorStack.Screen
        name="SearchJadwal"
        component={Screen.SearchJadwal}
        options={{title: "Cari Jadwal"}}
      />
      <OperatorStack.Screen
        name="FilterJadwal"
        component={Screen.FilterJadwal}
        options={{title: "Filter Jadwal"}}
      />
      <OperatorStack.Screen
        name="ManifestJadwal"
        component={Screen.ManifestJadwal}
        options={{title: "Manifest Jadwal"}}
      />
      <OperatorStack.Screen
        name="Manifest"
        component={Screen.Manifest}
        options={{title: "Manifest"}}
      />

      {/**
       * Kapal
       */}
      <OperatorStack.Screen
        name="Kapal"
        component={Screen.Kapal}
      />
      <OperatorStack.Screen name="ReadKapal" component={Screen.ReadKapal} options={{title: "Kapal"}} />
      <OperatorStack.Screen name="DeleteKapal" component={Screen.DeleteKapal} options={{title: "Hapus Kapal"}} />
      <OperatorStack.Screen name="SearchKapal" component={Screen.SearchKapal} options={{title: "Cari Kapal"}} />
      <OperatorStack.Screen name="FilterKapal" component={Screen.FilterKapal} options={{title: "Filter Kapal"}} />
      <OperatorStack.Screen name="TrackKapal" component={Screen.TrackKapal} options={{title: "Lacak Kapal"}} />
      <OperatorStack.Screen
        name="TrackingData"
        component={Screen.TrackingData}
        options={{title: "Data Tracking"}}
      />
      <OperatorStack.Screen
        name="TrackingDataFilter"
        component={Screen.TrackingDataFilter}
        options={{title: "Filter Data Tracking"}}
      />

      {/**
       * Vessel Booking
       */}
      <OperatorStack.Screen
        name="VesselBooking"
        component={Screen.VesselBooking}
        options={{title: "Booking Kapal"}}
      />
      <OperatorStack.Screen
        name="CreateVesselBooking"
        component={Screen.CreateVesselBooking}
        options={{ title: "Buat Booking Kapal"}}
      />
      <OperatorStack.Screen
        name="ReadVesselBooking"
        component={Screen.ReadVesselBooking}
        options={{title: "Detil Booking"}}
      />
      <OperatorStack.Screen
        name="UpdateVesselBooking"
        component={Screen.UpdateVesselBooking}
        options={{title: "Update Booking"}}
      />
      <OperatorStack.Screen
        name="DeleteVesselBooking"
        component={Screen.DeleteVesselBooking}
        options={{title: "Hapus Booking"}}
      />
      <OperatorStack.Screen
        name="SearchVesselBooking"
        component={Screen.SearchVesselBooking}
        options={{title: "Cari Booking"}}
      />
      <OperatorStack.Screen
        name="FilterVesselBooking"
        component={Screen.FilterVesselBooking}
        options={{title: "Filter Booking"}}
      />

      {/**
       * Pengaduan Section
       */}
      <OperatorStack.Screen name="Pengaduan" component={Screen.Pengaduan} />
      <OperatorStack.Screen
        name="ReadPengaduan"
        component={Screen.ReadPengaduan}
        options={{title: "Pengaduan"}}
      />
      <OperatorStack.Screen
        name="CreatePengaduan"
        component={Screen.CreatePengaduan}
        options={{title:"Buat Pengaduan"}}
      />
      <OperatorStack.Screen
        name="UpdatePengaduan"
        component={Screen.UpdatePengaduan}
        options={{title: "Ubah Pengaduan"}}
      />
      <OperatorStack.Screen
        name="SearchPengaduan"
        component={Screen.SearchPengaduan}
        options={{title: "Cari Pengaduan"}}
      />
      <OperatorStack.Screen
        name="FilterPengaduan"
        component={Screen.FilterPengaduan}
        options={{title: "Filter Pengaduan"}}
      />

      {/**
       * ListOperator
       */}
      <OperatorStack.Screen
        name="ListOperator"
        component={Screen.ListOperator}
      />

      <OperatorStack.Screen
        name="DownloadPanduan"
        component={Screen.DownloadPanduan}
        options={{title: "Unduh Panduan"}}
      />

      {/**
       * VESSEL GEO LOCATION
       */}
      <OperatorStack.Screen
        name="VesselGeoLocation"
        options={{title: "Peta Kapal Tol Laut"}}
        component={Screen.VesselGeoLocation}
      />
    </OperatorStack.Navigator>
  );
}

/**
 * Regulator
 * Navigator for all of Regulator screens
 * --
 */
function Regulator() {
  return (
    <RegulatorStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <RegulatorStack.Screen
        name="Dashboard"
        component={AppTabComponent}
        options={{
          headerShown: false,
        }}
      />
      <RegulatorStack.Screen
        name="UpdateProfile"
        options={{title: "Perbarui Profil"}}
        component={Screen.UpdateProfile}
      />

      <RegulatorStack.Screen
        name="ResetPassword"
        component={Screen.ResetPassword}
        options={{title: "Reset Password"}}
      />

      {/**
       * Purchase Order
       */}
      <RegulatorStack.Screen
        name="PurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.PurchaseOrder}
      />
      <RegulatorStack.Screen
        name="ReadPurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.ReadPurchaseOrder}
      />
      <RegulatorStack.Screen
        name="SearchPurchaseOrder"
        options={{title: "Cari Order Barang"}}
        component={Screen.SearchPurchaseOrder}
      />
      <RegulatorStack.Screen
        name="FilterPurchaseOrder"
        component={Screen.FilterPurchaseOrder}
      />
      <RegulatorStack.Screen
        name="TrackPurchaseOrder"
        component={Screen.TrackPurchaseOrder}
        options={{title:"Lacak Order Barang"}}
      />
      <RegulatorStack.Screen
        name="TrackViewPurchaseOrder"
        component={Screen.TrackViewPurchaseOrder}
        options={{title: "Lacak Order Barang"}}
      />

      {/**
       * Vessel Booking
       */}
      <RegulatorStack.Screen
        name="VesselBooking"
        component={Screen.VesselBooking}
        options={{ title: "Booking Kapal" }}
      />
      <RegulatorStack.Screen
        name="CreateVesselBooking"
        component={Screen.CreateVesselBooking}
        options={{ title: "Buat Booking Kapal"}}
      />
      <RegulatorStack.Screen
        name="ReadVesselBooking"
        component={Screen.ReadVesselBooking}
        options={{ title: "Detil Booking" }}
      />
      <RegulatorStack.Screen
        name="UpdateVesselBooking"
        component={Screen.UpdateVesselBooking}
        options={{ title: "Update Booking" }}
      />
      <RegulatorStack.Screen
        name="DeleteVesselBooking"
        component={Screen.DeleteVesselBooking}
        options={{ title: "Hapus Booking" }}
      />
      <RegulatorStack.Screen
        name="SearchVesselBooking"
        component={Screen.SearchVesselBooking}
        options={{ title: "Cari Booking" }}
      />
      <RegulatorStack.Screen
        name="FilterVesselBooking"
        options={{ title: "Filter Booking" }}
        component={Screen.FilterVesselBooking}
      />

      {/**
       * Commodity
       */}
      <RegulatorStack.Screen name="Commodity" component={Screen.Commodity} options={{title: "Komoditas"}} />
      <RegulatorStack.Screen
        name="CreateCommodity"
        component={Screen.CreateCommodity}
        options={{title: "Komoditas Baru"}}
      />
      <RegulatorStack.Screen
        name="ReadCommodity"
        component={Screen.ReadCommodity}
        options={{title: "Komoditas"}}
      />
      <RegulatorStack.Screen
        name="UpdateCommodity"
        component={Screen.UpdateCommodity}
        options={{title: "Ubah Komoditas"}}
      />
      <RegulatorStack.Screen
        name="DeleteCommodity"
        component={Screen.DeleteCommodity}
        options={{title: "Hapus Komoditas"}}
      />
      <RegulatorStack.Screen
        name="SearchCommodity"
        component={Screen.SearchCommodity}
        options={{title: "Cari Komoditas"}}
      />
      <RegulatorStack.Screen
        name="FilterCommodity"
        component={Screen.FilterCommodity}
        options={{title: "Filter Komoditas"}}
      />

      {/**
       * Container
       */}
      <RegulatorStack.Screen name="Container" component={Screen.Container} />
      <RegulatorStack.Screen
        name="CreateContainer"
        component={Screen.CreateContainer}
        options={{title: "Buat Container"}}
      />
      <RegulatorStack.Screen
        name="ReadContainer"
        component={Screen.ReadContainer}
        options={{title: "Container"}}
      />
      <RegulatorStack.Screen
        name="UpdateContainer"
        component={Screen.UpdateContainer}
        options={{title: "Ubah Container"}}
      />
      <RegulatorStack.Screen
        name="DeleteContainer"
        component={Screen.DeleteContainer}
        options={{title: "Hapus Container"}}
      />

      {/**
       * Kapal
       */}
      <RegulatorStack.Screen name="Kapal" component={Screen.Kapal} />
      <RegulatorStack.Screen name="ReadKapal" component={Screen.ReadKapal} options={{title: "Kapal"}} />
      <RegulatorStack.Screen name="CreateKapal" component={Screen.CreateKapal} options={{title: "Buat Kapal"}} />
      <RegulatorStack.Screen name="UpdateKapal" component={Screen.UpdateKapal} options={{title: "Ubah Kapal"}} />
      <RegulatorStack.Screen
        name="SearchKapal"
        component={Screen.SearchKapal} options={{title: "Cari Kapal"}}
      />
      <RegulatorStack.Screen
        name="FilterKapal"
        component={Screen.FilterKapal} options={{title: "Filter Kapal"}}
      />
      <RegulatorStack.Screen name="TrackKapal" component={Screen.TrackKapal} options={{title: "Lacak Kapal"}} />

      {/**
       * Trayek
       */}
      <RegulatorStack.Screen name="Trayek" component={Screen.Trayek} />
      <RegulatorStack.Screen name="ReadTrayek" component={Screen.ReadTrayek} options={{title:"Trayek"}} />
      <RegulatorStack.Screen
        name="SearchTrayek"
        component={Screen.SearchTrayek}
      />
      <RegulatorStack.Screen
        name="FilterTrayek"
        component={Screen.FilterTrayek}
      />

      {/**
       * Pelabuhan
       */}
      <RegulatorStack.Screen name="Pelabuhan" component={Screen.Pelabuhan} />
      <RegulatorStack.Screen
        name="CreatePelabuhan"
        component={Screen.CreatePelabuhan}
        options={{title:'Buat Pelabuhan'}}
      />
      <RegulatorStack.Screen
        name="ReadPelabuhan"
        component={Screen.ReadPelabuhan}
        options={{title:'Pelabuhan'}}
      />
      <RegulatorStack.Screen
        name="UpdatePelabuhan"
        component={Screen.UpdatePelabuhan}
        options={{title:'Perbarui Pelabuhan'}}
      />
      <RegulatorStack.Screen
        name="SearchPelabuhan"
        component={Screen.SearchPelabuhan}
        options={{title: "Cari Pelabuhan"}}
      />
      <RegulatorStack.Screen
        name="FilterPelabuhan"
        component={Screen.FilterPelabuhan}
        options={{title:'Filter Pelabuhan'}}
      />

      {/**
       * Depot
       */}
      <RegulatorStack.Screen name="Depot" component={Screen.Depot} options={{title: "Depot"}} />
      <RegulatorStack.Screen name="ReadDepot" component={Screen.ReadDepot} options={{title:"Depot"}} />
      <RegulatorStack.Screen
        name="SearchDepot"
        component={Screen.SearchDepot} options={{title: "Cari Depot"}}
      />
      <RegulatorStack.Screen
        name="FilterDepot"
        component={Screen.FilterDepot} options={{title: "Filter Depot"}}
      />

      {/**
       * Jadwal
       */}
      <RegulatorStack.Screen name="Jadwal" component={Screen.Jadwal} />
      <RegulatorStack.Screen name="ReadJadwal" component={Screen.ReadJadwal} options={{title: 'Jadwal'}} />
      <RegulatorStack.Screen
        name="SearchJadwal"
        component={Screen.SearchJadwal}
        options={{title: "Cari Jadwal"}}
      />
      <RegulatorStack.Screen
        name="FilterJadwal"
        component={Screen.FilterJadwal}
        options={{title: "Filter Jadwal"}}
      />
      <RegulatorStack.Screen
        name="ManifestJadwal"
        component={Screen.ManifestJadwal}
        options={{title: "Manifest Jadwal"}}
      />

      {/**
       * Pengaduan
       */}
      <RegulatorStack.Screen name="Pengaduan" component={Screen.Pengaduan} />
      <RegulatorStack.Screen
        name="ReadPengaduan"
        component={Screen.ReadPengaduan}
        options={{title: "Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="UpdatePengaduan"
        component={Screen.UpdatePengaduan}
        options={{title: "Ubah Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="SearchPengaduan"
        component={Screen.SearchPengaduan}
        options={{title: "Cari Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="FilterPengaduan"
        component={Screen.FilterPengaduan}
        options={{title: "Filter Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="DownloadPanduan"
        component={Screen.DownloadPanduan}
        options={{title: "Unduh Panduan"}}
      />

      {/**
       * JENIS BARANG
       */}
      <RegulatorStack.Screen
        name="JenisBarang"
        component={Screen.JenisBarang}
      />
      <RegulatorStack.Screen
        name="CreateJenisBarang"
        component={Screen.CreateJenisBarang}
      />
      <RegulatorStack.Screen
        name="UpdateJenisBarang"
        component={Screen.UpdateJenisBarang}
      />

      {/**
       * JENIS COMMODITY
       */}
      <RegulatorStack.Screen
        name="JenisCommodity"
        component={Screen.JenisCommodity}
      />
      <RegulatorStack.Screen
        name="JenisCommodityFilter"
        component={Screen.JenisCommodityFilter}
      />
      <RegulatorStack.Screen
        name="CreateJenisCommodity"
        component={Screen.CreateJenisCommodity}
      />
      <RegulatorStack.Screen
        name="UpdateJenisCommodity"
        component={Screen.UpdateJenisCommodity}
      />

      {/**
       * BARANG PENTING
       */}
      <RegulatorStack.Screen
        name="BarangPenting"
        component={Screen.BarangPenting}
      />
      <RegulatorStack.Screen
        name="UpdateBarangPenting"
        component={Screen.UpdateBarangPenting}
      />
      <RegulatorStack.Screen
        name="CreateBarangPenting"
        component={Screen.CreateBarangPenting}
      />


      {/**
       * KEMASAN
       */}
      <RegulatorStack.Screen name="Kemasan" component={Screen.Kemasan} />
      <RegulatorStack.Screen
        name="CreateKemasan"
        component={Screen.CreateKemasan}
      />
      <RegulatorStack.Screen
        name="UpdateKemasan"
        component={Screen.UpdateKemasan}
      />

      {/**
       * SATUAN
       */}
      <RegulatorStack.Screen name="Satuan" component={Screen.Satuan} />
      <RegulatorStack.Screen
        name="CreateSatuan"
        component={Screen.CreateSatuan}
      />
      <RegulatorStack.Screen
        name="UpdateSatuan"
        component={Screen.UpdateSatuan}
      />

      {/**
       * VESSEL GEO LOCATION
       */}
      <RegulatorStack.Screen
        name="VesselGeoLocation"
        options={{title: "Peta Kapal Tol Laut"}}
        component={Screen.VesselGeoLocation}
      />

      {/**
       * MASTER TARIF
       */}
      <RegulatorStack.Screen
        name="MasterTarif"
        component={Screen.MasterTarif}
      />
      <RegulatorStack.Screen
        name="CreateMasterTarif"
        component={Screen.CreateMasterTarif}
      />
      <RegulatorStack.Screen
        name="UpdateMasterTarif"
        component={Screen.UpdateMasterTarif}
      />
      <RegulatorStack.Screen
        name="FilterMasterTarif"
        component={Screen.FilterMasterTarif}
      />

      {/**
       * TIPE CONTAINER
       */}
      <RegulatorStack.Screen
        name="TipeContainer"
        component={Screen.TipeContainer}
      />
      <RegulatorStack.Screen
        name="CreateTipeContainer"
        component={Screen.CreateTipeContainer}
      />
      <RegulatorStack.Screen
        name="UpdateTipeContainer"
        component={Screen.UpdateTipeContainer}
      />

      {/**
       * AKTIVASI USER
       */}
      <RegulatorStack.Screen
        name="AktivasiUser"
        component={Screen.AktivasiUser}
      />
      <RegulatorStack.Screen
        name="ReadAktivasiUser"
        component={Screen.ReadAktivasiUser}
      />
      {/**
       * USER LCS
       */}
      <RegulatorStack.Screen name="UserLCS" component={Screen.UserLCS} />

      <RegulatorStack.Screen
        name="DaftarConsignee"
        component={Screen.DaftarConsignee}
      />
      <RegulatorStack.Screen
        name="DaftarSupplier"
        component={Screen.DaftarSupplier}
      />
      <RegulatorStack.Screen
        name="DaftarShipper"
        component={Screen.DaftarShipper}
      />
      <RegulatorStack.Screen
        name="DaftarReseller"
        component={Screen.DaftarReseller}
      />
      <RegulatorStack.Screen
        name="DaftarOperator"
        component={Screen.DaftarOperator}
      />
      <RegulatorStack.Screen
        name="DaftarRegulator"
        component={Screen.DaftarRegulator}
      />

      <RegulatorStack.Screen
        name="ReadConsignee"
        component={Screen.ReadConsignee}
      />
      <RegulatorStack.Screen
        name="ReadSupplier"
        component={Screen.ReadSupplier}
      />
      <RegulatorStack.Screen
        name="ReadShipper"
        component={Screen.ReadShipper}
      />
      <RegulatorStack.Screen
        name="ReadReseller"
        component={Screen.ReadReseller}
      />
      <RegulatorStack.Screen
        name="ReadOperator"
        component={Screen.ReadOperator}
      />
      <RegulatorStack.Screen
        name="ReadRegulator"
        component={Screen.ReadRegulator}
      />

      <RegulatorStack.Screen
        name="FilterDaftarConsignee"
        component={Screen.FilterDaftarConsignee}
      />
      <RegulatorStack.Screen
        name="FilterDaftarSupplier"
        component={Screen.FilterDaftarSupplier}
      />
      <RegulatorStack.Screen
        name="FilterDaftarShipper"
        component={Screen.FilterDaftarShipper}
      />
      <RegulatorStack.Screen
        name="FilterDaftarReseller"
        component={Screen.FilterDaftarReseller}
      />
      <RegulatorStack.Screen
        name="FilterDaftarOperator"
        component={Screen.FilterDaftarOperator}
      />
      <RegulatorStack.Screen
        name="FilterDaftarRegulator"
        component={Screen.FilterDaftarRegulator}
      />

      {/**
       * Manifest
       */}
      <RegulatorStack.Screen name="Manifest" component={Screen.Manifest} />
      <RegulatorStack.Screen
        name="ReadManifest"
        component={Screen.ReadManifest}
      />

      {/**
       * Report
       */}
      <RegulatorStack.Screen name="Report" component={Screen.Report} />
      <RegulatorStack.Screen
        name="ReportJumlahUser"
        component={Screen.ReportJumlahUser}
      />
      <RegulatorStack.Screen
        name="ReportStatusOrder"
        component={Screen.ReportStatusOrder}
      />
      <RegulatorStack.Screen
        name="ReportOrderMasukPerBulan"
        component={Screen.ReportOrderMasukPerBulan}
      />
      <RegulatorStack.Screen
        name="ReportDataUser"
        component={Screen.ReportDataUser}
      />
      <RegulatorStack.Screen
        name="ReportRealisasiMuatan"
        component={Screen.ReportRealisasiMuatan}
      />
      <RegulatorStack.Screen
        name="ReportDisparitasHarga"
        component={Screen.ReportDisparitasHarga}
      />
      <RegulatorStack.Screen
        name="ReportDaftarSisaKuota"
        component={Screen.ReportDaftarSisaKuota}
      />
      <RegulatorStack.Screen
        name="ReportTotalMuatanPerWilayah"
        component={Screen.ReportTotalMuatanPerWilayah}
      />
      <RegulatorStack.Screen
        name="ReportMuatanPerWilayahPerPrioritas"
        component={Screen.ReportMuatanPerWilayahPerPrioritas}
      />
      <RegulatorStack.Screen
        name="ReportRealisasiVoyagePerTrayek"
        component={Screen.ReportRealisasiVoyagePerTrayek}
      />
      <RegulatorStack.Screen
        name="ReportWaktuTempuh"
        component={Screen.ReportWaktuTempuh}
      />
      <RegulatorStack.Screen
        name="ReportTotalOrderPerJenisPrioritas"
        component={Screen.ReportTotalOrderPerJenisPrioritas}
      />
      <RegulatorStack.Screen
        name="ReportMuatanTerbanyakPerPrioritas"
        component={Screen.ReportMuatanTerbanyakPerPrioritas}
      />
      <RegulatorStack.Screen
        name="ReportTotalContainerPerPelabuhan"
        component={Screen.ReportTotalContainerPerPelabuhan}
      />
      <RegulatorStack.Screen
        name="ReportMuatanTolLaut"
        component={Screen.ReportMuatanTolLaut}
      />
      <RegulatorStack.Screen
        name="DetailTrayekBalik"
        component={Screen.DetailTrayekBalik}
      />

      <RegulatorStack.Screen
        name="FilterJumlahUser"
        component={Screen.FilterJumlahUser}
      />
      <RegulatorStack.Screen
        name="FilterStatusOrder"
        component={Screen.FilterStatusOrder}
      />
      <RegulatorStack.Screen
        name="FilterOrderMasukPerBulan"
        component={Screen.FilterOrderMasukPerBulan}
      />
      <RegulatorStack.Screen
        name="FilterDataUser"
        component={Screen.FilterDataUser}
      />
      <RegulatorStack.Screen
        name="FilterRealisasiMuatan"
        component={Screen.FilterRealisasiMuatan}
      />
      <RegulatorStack.Screen
        name="FilterDisparitasHarga"
        component={Screen.FilterDisparitasHarga}
      />
      <RegulatorStack.Screen
        name="FilterDaftarSisaKuota"
        component={Screen.FilterDaftarSisaKuota}
      />
      <RegulatorStack.Screen
        name="FilterTotalMuatanPerWilayah"
        component={Screen.FilterTotalMuatanPerWilayah}
      />
      <RegulatorStack.Screen
        name="FilterMuatanPerWilayahPerPrioritas"
        component={Screen.FilterMuatanPerWilayahPerPrioritas}
      />
      <RegulatorStack.Screen
        name="FilterRealisasiVoyagePerTrayek"
        component={Screen.FilterRealisasiVoyagePerTrayek}
      />
      <RegulatorStack.Screen
        name="FilterWaktuTempuh"
        component={Screen.FilterWaktuTempuh}
      />
      <RegulatorStack.Screen
        name="FilterTotalOrderPerJenisPrioritas"
        component={Screen.FilterTotalOrderPerJenisPrioritas}
      />
      <RegulatorStack.Screen
        name="FilterMuatanTerbanyakPerPrioritas"
        component={Screen.FilterMuatanTerbanyakPerPrioritas}
      />
      <RegulatorStack.Screen
        name="FilterTotalContainerPerPelabuhan"
        component={Screen.FilterTotalContainerPerPelabuhan}
      />

      {/**
       * REGULATOR
       */}
      <RegulatorStack.Screen name="Regulator" component={Screen.Regulator} />
      <RegulatorStack.Screen name="CreateRegulator" component={Screen.CreateRegulator} />


      {/**
       * Log Activity
       */}
      <RegulatorStack.Screen
        name="LogAktivitas"
        component={Screen.LogAktivitas}
      />
      <RegulatorStack.Screen
        name="LogAktivitasFilter"
        component={Screen.LogAktivitasFilter}
      />

      {/**
       * Master
       */}
      <RegulatorStack.Screen name="Master" component={Screen.Master} />
      <RegulatorStack.Screen
        name="MasterKodeTrayek"
        component={Screen.MasterKodeTrayek}
      />
      <RegulatorStack.Screen
        name="MasterHarga"
        component={Screen.MasterHarga}
      />
      <RegulatorStack.Screen
        name="MasterKontrak"
        component={Screen.MasterKontrak}
      />

      <RegulatorStack.Screen
        name="MasterKodeTrayekModal"
        component={Screen.MasterKodeTrayekModal}
      />
      <RegulatorStack.Screen
        name="MasterHargaModal"
        component={Screen.MasterHargaModal}
      />
      <RegulatorStack.Screen
        name="MasterKontrakModal"
        component={Screen.MasterKontrakModal}
      />

      {/** Guest */}
      <RegulatorStack.Screen name="Guest" component={Screen.Guest} />

      {/**
       * Dashboard
       */}
      <RegulatorStack.Screen name="OrderMasuk" component={Screen.OrderMasuk} />
      <RegulatorStack.Screen
        name="RealisasiMuatan"
        component={Screen.RealisasiMuatan}
      />
      <RegulatorStack.Screen
        name="DisparitasHarga"
        component={Screen.DisparitasHarga}
      />
      <RegulatorStack.Screen
        name="DaftarSisaKuotaTrayek"
        component={Screen.DaftarSisaKuotaTrayek}
      />
      <RegulatorStack.Screen
        name="TotalMuatanPerWilayah"
        component={Screen.TotalMuatanPerWilayah}
      />
      <RegulatorStack.Screen
        name="MuatanPerWilayahPerPrioritas"
        component={Screen.MuatanPerWilayahPerPrioritas}
      />
      <RegulatorStack.Screen
        name="RealisasiVoyage"
        component={Screen.RealisasiVoyage}
      />
      <RegulatorStack.Screen
        name="WaktuTempuh"
        component={Screen.WaktuTempuh}
      />
      <RegulatorStack.Screen
        name="TotalOrderPerJenisPrioritas"
        component={Screen.TotalOrderPerJenisPrioritas}
      />
      <RegulatorStack.Screen
        name="LimaMuatanTerbanyak"
        component={Screen.LimaMuatanTerbanyak}
      />
      <RegulatorStack.Screen
        name="TotalVoyagePerTrayek"
        component={Screen.TotalVoyagePerTrayek}
      />
      <RegulatorStack.Screen
        name="TotalContainerPerPort"
        component={Screen.TotalContainerPerPort}
      />
    </RegulatorStack.Navigator>
  );
}

/**
 * Visitor
 * Navigator for all of Visitor screens
 * --
 */
function Visitor() {
  return (
    <RegulatorStack.Navigator
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <RegulatorStack.Screen
        name="Dashboard"
        component={AppTabComponent}
        options={{
          headerShown: false,
        }}
      />
      <RegulatorStack.Screen
        name="UpdateProfile"
        options={{title: "Perbarui Profil"}}
        component={Screen.UpdateProfile}
      />

      <RegulatorStack.Screen
        name="ResetPassword"
        component={Screen.ResetPassword}
        options={{title: "Reset Password"}}
      />

      {/**
       * Purchase Order
       */}
      <RegulatorStack.Screen
        name="PurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.PurchaseOrder}
      />
      <RegulatorStack.Screen
        name="ReadPurchaseOrder"
        options={{title: "Order Barang"}}
        component={Screen.ReadPurchaseOrder}
      />
      <RegulatorStack.Screen
        name="SearchPurchaseOrder"
        options={{title: "Cari Order Barang"}}
        component={Screen.SearchPurchaseOrder}
      />
      <RegulatorStack.Screen
        name="FilterPurchaseOrder"
        component={Screen.FilterPurchaseOrder}
      />
      <RegulatorStack.Screen
        name="TrackPurchaseOrder"
        component={Screen.TrackPurchaseOrder}
        options={{title:"Lacak Order Barang"}}
      />
      <RegulatorStack.Screen
        name="TrackViewPurchaseOrder"
        component={Screen.TrackViewPurchaseOrder}
        options={{title: "Lacak Order Barang"}}
      />

      {/**
       * Vessel Booking
       */}
      <RegulatorStack.Screen
        name="VesselBooking"
        component={Screen.VesselBooking}
        options={{ title: "Booking Kapal" }}
      />
      <RegulatorStack.Screen
        name="CreateVesselBooking"
        component={Screen.CreateVesselBooking}
        options={{ title: "Buat Booking Kapal"}}
      />
      <RegulatorStack.Screen
        name="ReadVesselBooking"
        component={Screen.ReadVesselBooking}
        options={{ title: "Detil Booking" }}
      />
      <RegulatorStack.Screen
        name="UpdateVesselBooking"
        component={Screen.UpdateVesselBooking}
        options={{ title: "Update Booking" }}
      />
      <RegulatorStack.Screen
        name="DeleteVesselBooking"
        component={Screen.DeleteVesselBooking}
        options={{ title: "Hapus Booking" }}
      />
      <RegulatorStack.Screen
        name="SearchVesselBooking"
        component={Screen.SearchVesselBooking}
        options={{ title: "Cari Booking" }}
      />
      <RegulatorStack.Screen
        name="FilterVesselBooking"
        options={{ title: "Filter Booking" }}
        component={Screen.FilterVesselBooking}
      />

      {/**
       * Commodity
       */}
      <RegulatorStack.Screen name="Commodity" component={Screen.Commodity} options={{title: "Komoditas"}} />
      <RegulatorStack.Screen
        name="CreateCommodity"
        component={Screen.CreateCommodity}
        options={{title: "Komoditas Baru"}}
      />
      <RegulatorStack.Screen
        name="ReadCommodity"
        component={Screen.ReadCommodity}
        options={{title: "Komoditas"}}
      />
      <RegulatorStack.Screen
        name="UpdateCommodity"
        component={Screen.UpdateCommodity}
        options={{title: "Ubah Komoditas"}}
      />
      <RegulatorStack.Screen
        name="DeleteCommodity"
        component={Screen.DeleteCommodity}
        options={{title: "Hapus Komoditas"}}
      />
      <RegulatorStack.Screen
        name="SearchCommodity"
        component={Screen.SearchCommodity}
        options={{title: "Cari Komoditas"}}
      />
      <RegulatorStack.Screen
        name="FilterCommodity"
        component={Screen.FilterCommodity}
        options={{title: "Filter Komoditas"}}
      />

      {/**
       * Container
       */}
      <RegulatorStack.Screen name="Container" component={Screen.Container} />
      <RegulatorStack.Screen
        name="CreateContainer"
        component={Screen.CreateContainer}
        options={{title: "Buat Container"}}
      />
      <RegulatorStack.Screen
        name="ReadContainer"
        component={Screen.ReadContainer}
        options={{title: "Container"}}
      />
      <RegulatorStack.Screen
        name="UpdateContainer"
        component={Screen.UpdateContainer}
        options={{title: "Ubah Container"}}
      />
      <RegulatorStack.Screen
        name="DeleteContainer"
        component={Screen.DeleteContainer}
        options={{title: "Hapus Container"}}
      />

      {/**
       * Kapal
       */}
      <RegulatorStack.Screen name="Kapal" component={Screen.Kapal} />
      <RegulatorStack.Screen name="ReadKapal" component={Screen.ReadKapal} options={{title: "Kapal"}} />
      <RegulatorStack.Screen
        name="SearchKapal"
        component={Screen.SearchKapal} options={{title: "Cari Kapal"}}
      />
      <RegulatorStack.Screen
        name="FilterKapal"
        component={Screen.FilterKapal} options={{title: "Filter Kapal"}}
      />
      <RegulatorStack.Screen name="TrackKapal" component={Screen.TrackKapal} options={{title: "Lacak Kapal"}} />

      {/**
       * Trayek
       */}
      <RegulatorStack.Screen name="Trayek" component={Screen.Trayek} />
      <RegulatorStack.Screen name="ReadTrayek" component={Screen.ReadTrayek} options={{title:"Trayek"}} />
      <RegulatorStack.Screen
        name="SearchTrayek"
        component={Screen.SearchTrayek}
      />
      <RegulatorStack.Screen
        name="FilterTrayek"
        component={Screen.FilterTrayek}
      />

      {/**
       * Pelabuhan
       */}
      <RegulatorStack.Screen name="Pelabuhan" component={Screen.Pelabuhan} />
      <RegulatorStack.Screen
        name="ReadPelabuhan"
        component={Screen.ReadPelabuhan}
        options={{title:'Pelabuhan'}}
      />
      <RegulatorStack.Screen
        name="SearchPelabuhan"
        component={Screen.SearchPelabuhan}
        options={{title: "Cari Pelabuhan"}}
      />
      <RegulatorStack.Screen
        name="FilterPelabuhan"
        component={Screen.FilterPelabuhan}
        options={{title:'Filter Pelabuhan'}}
      />

      {/**
       * Depot
       */}
      <RegulatorStack.Screen name="Depot" component={Screen.Depot} options={{title: "Depot"}} />
      <RegulatorStack.Screen name="ReadDepot" component={Screen.ReadDepot} options={{title:"Depot"}} />
      <RegulatorStack.Screen
        name="SearchDepot"
        component={Screen.SearchDepot} options={{title: "Cari Depot"}}
      />
      <RegulatorStack.Screen
        name="FilterDepot"
        component={Screen.FilterDepot} options={{title: "Filter Depot"}}
      />

      {/**
       * Jadwal
       */}
      <RegulatorStack.Screen name="Jadwal" component={Screen.Jadwal} />
      <RegulatorStack.Screen name="ReadJadwal" component={Screen.ReadJadwal} options={{title: 'Jadwal'}} />
      <RegulatorStack.Screen
        name="SearchJadwal"
        component={Screen.SearchJadwal}
        options={{title: "Cari Jadwal"}}
      />
      <RegulatorStack.Screen
        name="FilterJadwal"
        component={Screen.FilterJadwal}
        options={{title: "Filter Jadwal"}}
      />
      <RegulatorStack.Screen
        name="ManifestJadwal"
        component={Screen.ManifestJadwal}
        options={{title: "Manifest Jadwal"}}
      />

      {/**
       * Pengaduan
       */}
      <RegulatorStack.Screen name="Pengaduan" component={Screen.Pengaduan} />
      <RegulatorStack.Screen
        name="ReadPengaduan"
        component={Screen.ReadPengaduan}
        options={{title: "Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="UpdatePengaduan"
        component={Screen.UpdatePengaduan}
        options={{title: "Ubah Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="SearchPengaduan"
        component={Screen.SearchPengaduan}
        options={{title: "Cari Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="FilterPengaduan"
        component={Screen.FilterPengaduan}
        options={{title: "Filter Pengaduan"}}
      />
      <RegulatorStack.Screen
        name="DownloadPanduan"
        component={Screen.DownloadPanduan}
        options={{title: "Unduh Panduan"}}
      />

      {/**
       * JENIS BARANG
       */}
      <RegulatorStack.Screen
        name="JenisBarang"
        component={Screen.JenisBarang}
      />
      <RegulatorStack.Screen
        name="CreateJenisBarang"
        component={Screen.CreateJenisBarang}
      />
      <RegulatorStack.Screen
        name="UpdateJenisBarang"
        component={Screen.UpdateJenisBarang}
      />

      {/**
       * JENIS COMMODITY
       */}
      <RegulatorStack.Screen
        name="JenisCommodity"
        component={Screen.JenisCommodity}
      />
      <RegulatorStack.Screen
        name="JenisCommodityFilter"
        component={Screen.JenisCommodityFilter}
      />
      <RegulatorStack.Screen
        name="CreateJenisCommodity"
        component={Screen.CreateJenisCommodity}
      />
      <RegulatorStack.Screen
        name="UpdateJenisCommodity"
        component={Screen.UpdateJenisCommodity}
      />

      {/**
       * BARANG PENTING
       */}
      <RegulatorStack.Screen
        name="BarangPenting"
        component={Screen.BarangPenting}
      />

      {/**
       * KEMASAN
       */}
      <RegulatorStack.Screen name="Kemasan" component={Screen.Kemasan} />
      <RegulatorStack.Screen
        name="CreateKemasan"
        component={Screen.CreateKemasan}
      />
      <RegulatorStack.Screen
        name="UpdateKemasan"
        component={Screen.UpdateKemasan}
      />

      {/**
       * SATUAN
       */}
      <RegulatorStack.Screen name="Satuan" component={Screen.Satuan} />
      <RegulatorStack.Screen
        name="CreateSatuan"
        component={Screen.CreateSatuan}
      />
      <RegulatorStack.Screen
        name="UpdateSatuan"
        component={Screen.UpdateSatuan}
      />

      {/**
       * VESSEL GEO LOCATION
       */}
      <RegulatorStack.Screen
        name="VesselGeoLocation"
        options={{title: "Peta Kapal Tol Laut"}}
        component={Screen.VesselGeoLocation}
      />

      {/**
       * MASTER TARIF
       */}
      <RegulatorStack.Screen
        name="MasterTarif"
        component={Screen.MasterTarif}
      />
      <RegulatorStack.Screen
        name="CreateMasterTarif"
        component={Screen.CreateMasterTarif}
      />
      <RegulatorStack.Screen
        name="UpdateMasterTarif"
        component={Screen.UpdateMasterTarif}
      />
      <RegulatorStack.Screen
        name="FilterMasterTarif"
        component={Screen.FilterMasterTarif}
      />

      {/**
       * TIPE CONTAINER
       */}
      <RegulatorStack.Screen
        name="TipeContainer"
        component={Screen.TipeContainer}
      />
      <RegulatorStack.Screen
        name="CreateTipeContainer"
        component={Screen.CreateTipeContainer}
      />
      <RegulatorStack.Screen
        name="UpdateTipeContainer"
        component={Screen.UpdateTipeContainer}
      />

      {/**
       * AKTIVASI USER
       */}
      <RegulatorStack.Screen
        name="AktivasiUser"
        component={Screen.AktivasiUser}
      />
      <RegulatorStack.Screen
        name="ReadAktivasiUser"
        component={Screen.ReadAktivasiUser}
      />
      {/**
       * USER LCS
       */}
      <RegulatorStack.Screen name="UserLCS" component={Screen.UserLCS} />

      <RegulatorStack.Screen
        name="DaftarConsignee"
        component={Screen.DaftarConsignee}
      />
      <RegulatorStack.Screen
        name="DaftarSupplier"
        component={Screen.DaftarSupplier}
      />
      <RegulatorStack.Screen
        name="DaftarShipper"
        component={Screen.DaftarShipper}
      />
      <RegulatorStack.Screen
        name="DaftarReseller"
        component={Screen.DaftarReseller}
      />
      <RegulatorStack.Screen
        name="DaftarOperator"
        component={Screen.DaftarOperator}
      />
      <RegulatorStack.Screen
        name="DaftarRegulator"
        component={Screen.DaftarRegulator}
      />

      <RegulatorStack.Screen
        name="ReadConsignee"
        component={Screen.ReadConsignee}
      />
      <RegulatorStack.Screen
        name="ReadSupplier"
        component={Screen.ReadSupplier}
      />
      <RegulatorStack.Screen
        name="ReadShipper"
        component={Screen.ReadShipper}
      />
      <RegulatorStack.Screen
        name="ReadReseller"
        component={Screen.ReadReseller}
      />
      <RegulatorStack.Screen
        name="ReadOperator"
        component={Screen.ReadOperator}
      />
      <RegulatorStack.Screen
        name="ReadRegulator"
        component={Screen.ReadRegulator}
      />

      <RegulatorStack.Screen
        name="FilterDaftarConsignee"
        component={Screen.FilterDaftarConsignee}
      />
      <RegulatorStack.Screen
        name="FilterDaftarSupplier"
        component={Screen.FilterDaftarSupplier}
      />
      <RegulatorStack.Screen
        name="FilterDaftarShipper"
        component={Screen.FilterDaftarShipper}
      />
      <RegulatorStack.Screen
        name="FilterDaftarReseller"
        component={Screen.FilterDaftarReseller}
      />
      <RegulatorStack.Screen
        name="FilterDaftarOperator"
        component={Screen.FilterDaftarOperator}
      />
      <RegulatorStack.Screen
        name="FilterDaftarRegulator"
        component={Screen.FilterDaftarRegulator}
      />

      {/**
       * Manifest
       */}
      <RegulatorStack.Screen name="Manifest" component={Screen.Manifest} />
      <RegulatorStack.Screen
        name="ReadManifest"
        component={Screen.ReadManifest}
      />

      {/**
       * Report
       */}
      <RegulatorStack.Screen name="Report" component={Screen.Report} />
      <RegulatorStack.Screen
        name="ReportJumlahUser"
        component={Screen.ReportJumlahUser}
      />
      <RegulatorStack.Screen
        name="ReportStatusOrder"
        component={Screen.ReportStatusOrder}
      />
      <RegulatorStack.Screen
        name="ReportOrderMasukPerBulan"
        component={Screen.ReportOrderMasukPerBulan}
      />
      <RegulatorStack.Screen
        name="ReportDataUser"
        component={Screen.ReportDataUser}
      />
      <RegulatorStack.Screen
        name="ReportRealisasiMuatan"
        component={Screen.ReportRealisasiMuatan}
      />
      <RegulatorStack.Screen
        name="ReportDisparitasHarga"
        component={Screen.ReportDisparitasHarga}
      />
      <RegulatorStack.Screen
        name="ReportDaftarSisaKuota"
        component={Screen.ReportDaftarSisaKuota}
      />
      <RegulatorStack.Screen
        name="ReportTotalMuatanPerWilayah"
        component={Screen.ReportTotalMuatanPerWilayah}
      />
      <RegulatorStack.Screen
        name="ReportMuatanPerWilayahPerPrioritas"
        component={Screen.ReportMuatanPerWilayahPerPrioritas}
      />
      <RegulatorStack.Screen
        name="ReportRealisasiVoyagePerTrayek"
        component={Screen.ReportRealisasiVoyagePerTrayek}
      />
      <RegulatorStack.Screen
        name="ReportWaktuTempuh"
        component={Screen.ReportWaktuTempuh}
      />
      <RegulatorStack.Screen
        name="ReportTotalOrderPerJenisPrioritas"
        component={Screen.ReportTotalOrderPerJenisPrioritas}
      />
      <RegulatorStack.Screen
        name="ReportMuatanTerbanyakPerPrioritas"
        component={Screen.ReportMuatanTerbanyakPerPrioritas}
      />
      <RegulatorStack.Screen
        name="ReportTotalContainerPerPelabuhan"
        component={Screen.ReportTotalContainerPerPelabuhan}
      />

      <RegulatorStack.Screen
        name="FilterJumlahUser"
        component={Screen.FilterJumlahUser}
      />
      <RegulatorStack.Screen
        name="FilterStatusOrder"
        component={Screen.FilterStatusOrder}
      />
      <RegulatorStack.Screen
        name="FilterOrderMasukPerBulan"
        component={Screen.FilterOrderMasukPerBulan}
      />
      <RegulatorStack.Screen
        name="FilterDataUser"
        component={Screen.FilterDataUser}
      />
      <RegulatorStack.Screen
        name="FilterRealisasiMuatan"
        component={Screen.FilterRealisasiMuatan}
      />
      <RegulatorStack.Screen
        name="FilterDisparitasHarga"
        component={Screen.FilterDisparitasHarga}
      />
      <RegulatorStack.Screen
        name="FilterDaftarSisaKuota"
        component={Screen.FilterDaftarSisaKuota}
      />
      <RegulatorStack.Screen
        name="FilterTotalMuatanPerWilayah"
        component={Screen.FilterTotalMuatanPerWilayah}
      />
      <RegulatorStack.Screen
        name="FilterMuatanPerWilayahPerPrioritas"
        component={Screen.FilterMuatanPerWilayahPerPrioritas}
      />
      <RegulatorStack.Screen
        name="FilterRealisasiVoyagePerTrayek"
        component={Screen.FilterRealisasiVoyagePerTrayek}
      />
      <RegulatorStack.Screen
        name="FilterWaktuTempuh"
        component={Screen.FilterWaktuTempuh}
      />
      <RegulatorStack.Screen
        name="FilterTotalOrderPerJenisPrioritas"
        component={Screen.FilterTotalOrderPerJenisPrioritas}
      />
      <RegulatorStack.Screen
        name="FilterMuatanTerbanyakPerPrioritas"
        component={Screen.FilterMuatanTerbanyakPerPrioritas}
      />
      <RegulatorStack.Screen
        name="FilterTotalContainerPerPelabuhan"
        component={Screen.FilterTotalContainerPerPelabuhan}
      />

      {/**
       * REGULATOR
       */}
      <RegulatorStack.Screen name="Regulator" component={Screen.Regulator} />
      <RegulatorStack.Screen name="CreateRegulator" component={Screen.CreateRegulator} />


      {/**
       * Log Activity
       */}
      <RegulatorStack.Screen
        name="LogAktivitas"
        component={Screen.LogAktivitas}
      />
      <RegulatorStack.Screen
        name="LogAktivitasFilter"
        component={Screen.LogAktivitasFilter}
      />

      {/**
       * Master
       */}
      <RegulatorStack.Screen name="Master" component={Screen.Master} />
      <RegulatorStack.Screen
        name="MasterKodeTrayek"
        component={Screen.MasterKodeTrayek}
      />
      <RegulatorStack.Screen
        name="MasterHarga"
        component={Screen.MasterHarga}
      />
      <RegulatorStack.Screen
        name="MasterKontrak"
        component={Screen.MasterKontrak}
      />

      <RegulatorStack.Screen
        name="MasterKodeTrayekModal"
        component={Screen.MasterKodeTrayekModal}
      />
      <RegulatorStack.Screen
        name="MasterHargaModal"
        component={Screen.MasterHargaModal}
      />
      <RegulatorStack.Screen
        name="MasterKontrakModal"
        component={Screen.MasterKontrakModal}
      />

      {/** Guest */}
      <RegulatorStack.Screen name="Guest" component={Screen.Guest} />

      {/**
       * Dashboard
       */}
      <RegulatorStack.Screen name="OrderMasuk" component={Screen.OrderMasuk} />
      <RegulatorStack.Screen
        name="RealisasiMuatan"
        component={Screen.RealisasiMuatan}
      />
      <RegulatorStack.Screen
        name="DisparitasHarga"
        component={Screen.DisparitasHarga}
      />
      <RegulatorStack.Screen
        name="DaftarSisaKuotaTrayek"
        component={Screen.DaftarSisaKuotaTrayek}
      />
      <RegulatorStack.Screen
        name="TotalMuatanPerWilayah"
        component={Screen.TotalMuatanPerWilayah}
      />
      <RegulatorStack.Screen
        name="MuatanPerWilayahPerPrioritas"
        component={Screen.MuatanPerWilayahPerPrioritas}
      />
      <RegulatorStack.Screen
        name="RealisasiVoyage"
        component={Screen.RealisasiVoyage}
      />
      <RegulatorStack.Screen
        name="WaktuTempuh"
        component={Screen.WaktuTempuh}
      />
      <RegulatorStack.Screen
        name="TotalOrderPerJenisPrioritas"
        component={Screen.TotalOrderPerJenisPrioritas}
      />
      <RegulatorStack.Screen
        name="LimaMuatanTerbanyak"
        component={Screen.LimaMuatanTerbanyak}
      />
      <RegulatorStack.Screen
        name="TotalVoyagePerTrayek"
        component={Screen.TotalVoyagePerTrayek}
      />
      <RegulatorStack.Screen
        name="TotalContainerPerPort"
        component={Screen.TotalContainerPerPort}
      />
    </RegulatorStack.Navigator>
  );
}

/**
 * App Component
 * Navigator for all of user type screens
 * --
 */
function AppComponent() {
  const { usertype } = useSelector(function (state: RootState) {
    return state.auth.authLoginSuccess
      ? state.auth.authLoginSuccess.data.user[0]
      : {
        id: 0,
        usertype: false,
      };
  });
  return (
    <AppStack.Navigator screenOptions={{ headerShown: false }} mode="modal">
      {usertype == "Consignee" ? (
        <AppStack.Screen name="Consignee" component={Consignee} />
      ) : usertype == "Supplier" ? (
        <AppStack.Screen name="Supplier" component={Supplier} />
      ) : usertype == "Shipper" ? (
        <AppStack.Screen name="Shipper" component={Shipper} />
      ) : usertype == "Reseller" ? (
        <AppStack.Screen name="Reseller" component={Reseller} />
      ) : usertype == "Vessel Operator" ? (
        <AppStack.Screen name="Operator" component={Operator} />
      ) : usertype == "Regulator" ? (
        <AppStack.Screen name="Regulator" component={Regulator} />
      ) : (
                    <AppStack.Screen name="Visitor" component={Visitor} />
                  )}
      <AppStack.Screen name="Modals" component={ModalComponent} />
    </AppStack.Navigator>
  );
}

/**
 * Modals Stack
 * contains all of modals used inside app
 */
const Modal = createStackNavigator();
function ModalComponent() {
  return (
    <Modal.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.pri,
        },
        headerTintColor: Colors.white,
        cardStyle: {
          backgroundColor: Colors.white,
        },
      }}
    >
      <Modal.Screen name="AnimatedSandbox" component={Screen.AnimatedSandbox} />
      <Modal.Screen name="ImageModal" component={Screen.ImageModal} 
        options={{
          headerShown: false,
        }}
      />
      <Modal.Screen
        name="PelabuhanFilterModal"
        component={Screen.PelabuhanFilterModal}
        options={({ navigation, route }) => ({
          headerTitle: 'Filter Pelabuhan',
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="VesselBookingFilterModal"
        component={Screen.VesselBookingFilterModal}
        options={({ navigation, route }) => ({
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="HargaJualBarangFilterModal"
        component={Screen.HargaJualBarangFilterModal}
        options={({ navigation, route }) => ({
          headerTitle:'Harga Jual Barang',
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="DistributePurchaseOrderFilterModal"
        component={Screen.DistributePurchaseOrderFilterModal}
        options={({ navigation, route }) => ({
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="ClaimPurchaseOrderFilterModal"
        component={Screen.ClaimPurchaseOrderFilterModal}
        options={({ navigation, route }) => ({
          headerTitle: "Pengambilan Barang",
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="FilterPurchaseOrder"
        component={Screen.FilterPurchaseOrder}
        options={({ navigation, route }) => ({
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
          title:"Filter Daftar Pembelian"
        })}
      />
      <Modal.Screen
        name="TrackPurchaseOrderFilterModal"
        component={Screen.TrackPurchaseOrderFilterModal}
        options={({ navigation, route }) => ({
          headerTitle: 'Filter Lacak Pembelian',
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="JadwalFilterModal"
        component={Screen.JadwalFilterModal}
        options={({ navigation, route }) => ({
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="JadwalOperatorModal"
        component={Screen.JadwalOperatorModal}
        options={({ navigation, route }) => ({
          headerTitle:'Jadwal Operator',
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="SearchBookingModal"
        component={Screen.SearchBookingModal}
        options={({ navigation, route }) => ({
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
        })}
      />
      <Modal.Screen
        name="DetailContainerModal"
        component={Screen.DetailContainerModal}
        options={({ navigation, route }) => ({
          headerRight: (props) => (
            <IconButton
              icon={"close"}
              onPress={function () {
                navigation.goBack();
              }}
              color={props.tintColor}
            />
          ),
          title: "Detail Container",
        })}
      />
    </Modal.Navigator>
  );
}

/**
 * RootStack
 * Root Navigator contains
 * Auth and App Navigator
 */
function Main() {
  const isAuthenticated = useSelector((state: RootState) => {
    return state.auth.token;
  });
  const theme = {
    ...DefaultTheme,
    roundness: 0,
    colors: {
      ...DefaultTheme.colors,
      primary: "#064E89",
      accent: "#f1c40f",
    },
  };
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer>
        <RootStack.Navigator
          screenOptions={{
            headerShown: false,
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          }}
        >
          {isAuthenticated ? (
            <RootStack.Screen name="App" component={AppComponent} />
          ) : (
              <RootStack.Screen name="Auth" component={AuthComponent} />
            )}
        </RootStack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
}

export default function ReduxWrapper() {
  return (
    <ReduxProvider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Main />
      </PersistGate>
    </ReduxProvider>
  );
}
