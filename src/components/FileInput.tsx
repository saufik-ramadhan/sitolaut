import React, { memo } from "react";
import { View, Text, TouchableWithoutFeedback, StyleSheet } from "react-native";
import { Icon } from "./index";
import { Caption as Subtitle } from "react-native-paper";
import ImagePicker, { ImagePickerOptions } from "react-native-image-picker";
import { default as colors } from "./../config/Colors";
import ImageManipulator from "expo-image-manipulator";

type FileInput = {
  error?: any;
  placeholder?: any;
  title?: any;
  getValue?: any;
  onFocus?: any;
};

const FileInput = ({
  error,
  placeholder,
  title,
  getValue,
  onFocus,
}: FileInput) => {
  const options: ImagePickerOptions = {
    title: "Pilih File",
    storageOptions: {
      skipBackup: true,
      path: "images",
    },
    quality: 0.5,
    allowsEditing: true,
  };

  async function getFile() {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else {
        getValue(response);
      }
    });
  }
  const errorColor = error ? colors.danger : colors.primary;
  return (
    <View>
      <TouchableWithoutFeedback onFocus={onFocus} onPress={() => getFile()}>
        <View
          style={{
            borderRadius: 3,
            flexDirection: "row",
            alignItems: "center",
            backgroundColor: colors.grayL,
            padding: 10,
            paddingVertical: 15,
          }}
        >
          <Text
            style={{
              color: colors.gray4,
              fontSize: 14,
            }}
          >
            {title + " (Max. 512kb)"}
          </Text>

          <Icon
            name={"image"}
            style={{
              color: colors.gray3,
              fontSize: 20,
              position: "absolute",
              padding: 10,
              right: 0,
            }}
          />
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  errors: {
    color: "darkred",
  },
});

export default memo(FileInput);
