import React, { memo } from "react";
import { Portal, Dialog, Paragraph } from "react-native-paper";
import { Button } from "react-native-paper";

type ConfirmationDialog = {
  onYes?: any;
  onNo?: any;
  visible: boolean;
  onDismiss: any;
  content?: string;
  title: string;
  component?: React.ReactNode;
};

function Confirmation({
  onYes,
  onNo,
  visible,
  onDismiss,
  content,
  title,
  component,
}: ConfirmationDialog) {
  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Title>{title}</Dialog.Title>
        <Dialog.Content>
          {content && <Paragraph>{content}</Paragraph>}
          {component && component}
        </Dialog.Content>
        {onYes && (
          <Dialog.Actions>
            <Button onPress={onYes}>Ya</Button>
            <Button onPress={onNo}>Tidak</Button>
          </Dialog.Actions>
        )}
      </Dialog>
    </Portal>
  );
}

export default memo(Confirmation);
