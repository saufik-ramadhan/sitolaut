import React, { memo } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";

type HorizontalList = {
  children: any;
  snapToInterval: number;
};
function HorizontalList({ children, snapToInterval }: HorizontalList) {
  return (
    <ScrollView
      horizontal={true}
      pagingEnabled={true}
      snapToInterval={snapToInterval}
      decelerationRate={"fast"}
      showsHorizontalScrollIndicator={false}
    >
      {children}
    </ScrollView>
  );
}

export default memo(HorizontalList);

const styles = StyleSheet.create({});
