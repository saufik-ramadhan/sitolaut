import React, { useState, useEffect, memo } from "react";
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StatusBar,
  FlatList,
  TextInput,
} from "react-native";
import { Icon } from "./index";
import { default as colors } from "../config/Colors";
import { List, IconButton, Card, Divider, Caption } from "react-native-paper";
import { bool, boolean } from "yup";

function Selector({
  onPressCancel,
  onSelected,
  options,
  withSearch,
  label,
  objectKey,
  visible,
  subtitle1Key,
  subtitle2Key,
  ...props
}: any) {
  const [data, setData] = useState([]);
  const handleFilter = function (text: string) {
    const regex = new RegExp(`${text.trim()}`, "i");
    let data = options.filter((item: any) => {
      if (item[objectKey || "value"])
        return item[objectKey || "value"].search(regex) >= 0;
    });
    setData(data);
  };

  useEffect(() => setData(options), [visible]);
  return (
    <Modal
      {...props}
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={onPressCancel}
    >
      <View
        style={{
          backgroundColor: "rgba(0,0,0,0.5)",
          flex: 1,
        }}
      >
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={{ flex: 1 }} />
        </TouchableWithoutFeedback>
        <View
          style={{
            flex: 2,
            backgroundColor: "white",
            borderTopColor: colors.pri,
            borderTopWidth: 8,
          }}
        >
          {withSearch && (
            <View
              style={{
                paddingHorizontal: 20,
                backgroundColor: "#eee",
                flexDirection: "row",
                marginHorizontal: 5,
                elevation: 5,
              }}
            >
              <List.Icon style={{ marginHorizontal: 0 }} icon="magnify" />
              <TextInput
                placeholder={`Cari ${label} . . .`}
                onChangeText={function (e) {
                  handleFilter(e);
                }}
                // autoFocus
              />
            </View>
          )}
          <View style={{ marginHorizontal: 20 }}>
            <FlatList
              data={data}
              showsVerticalScrollIndicator={false}
              ListFooterComponent={<View style={{ height: 100 }} />}
              ListEmptyComponent={
                <Text
                  style={{
                    color: "#666",
                    textAlign: "center",
                    marginTop: 10,
                    fontSize: 16,
                  }}
                >
                  Data tidak ditemukan
                </Text>
              }
              renderItem={function ({ item }) {
                return (
                  <Card
                    onPress={function () {
                      onSelected(item);
                      onPressCancel();
                    }}
                  >
                    <List.Item
                      title={item[objectKey || "value"]}
                      description={`${item[subtitle1Key || "value"]} - ${
                        item[subtitle2Key || "value"]
                      }`}
                    />

                    <Divider />
                  </Card>
                );
              }}
              keyExtractor={function (item, key) {
                return String(key);
              }}
            />
            <View style={{ height: 10 }} />
          </View>
        </View>
      </View>
    </Modal>
  );
}

type SelectInput = {
  onChangeValue: any;
  options: any;
  label: any;
  withSearch: boolean;
  objectKey: any;
  error: any;
  subtitle1Key: any;
  subtitle2Key: any;
};
/**
 * Custom Picker Component
 * JavaScript, with search for
 * entries feature
 */
function SelectInput({
  label,
  options,
  onChangeValue,
  withSearch,
  error,
  objectKey,
  subtitle1Key,
  subtitle2Key,
}: SelectInput) {
  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState(false);
  return (
    <View style={{ backgroundColor: colors.white, borderRadius: 3, flex: 1 }}>
      <Selector
        objectKey={objectKey}
        visible={visible}
        onPressCancel={function () {
          setVisible(!visible);
        }}
        onSelected={function (val) {
          setSelected(val);
          onChangeValue ? onChangeValue(val) : null;
        }}
        options={options}
        label={label}
        withSearch={withSearch ? true : false}
        subtitle1Key={subtitle1Key}
        subtitle2Key={subtitle2Key}
      />

      <TouchableOpacity
        onPress={function () {
          setVisible(!visible);
        }}
      >
        <View
          style={{
            flexDirection: "row",
            borderColor: error ? colors.danger : "#ccc",
            paddingVertical: 15,
            borderBottomWidth: 1,
          }}
        >
          <View style={{ flex: 8, marginLeft: 8 }}>
            <Text
              style={{
                fontSize: 14,
                color: "#888",
              }}
            >
              {Object.keys(selected).length > 0
                ? selected[objectKey || "value"]
                : label}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            <Icon
              name={"chevron-down"}
              type="Feather"
              style={{
                color: error ? colors.danger : "#ccc",
                fontSize: 20,
              }}
            />
          </View>
        </View>
      </TouchableOpacity>
      {error && (
        <Subtitle style={{ color: colors.danger, marginLeft: 10 }}>
          {error}
        </Subtitle>
      )}
    </View>
  );
}

export default memo(SelectInput);
