import React, { memo, useState } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  TouchableNativeFeedback,
  FlatList,
  TouchableOpacity,
} from "react-native";
import Colors from "../config/Colors";
import {
  List,
  Divider,
  Button,
  Card,
  IconButton,
  Paragraph,
} from "react-native-paper";
var no = 1;

interface Props {
  initValue?: any;
  initLabel?: string;
  data?: any;
  onValueChange: any;
  objectKey: string;
}

/**
 *
 * @param objectKey
 * property key dari item untuk object yang ingin ditampilkan
 * contoh: {id:1, label:'label 1'}
 * jika objectKey='label' yang ditampilkan pada list item = label 1
 */

function PickerCostum(props: Props) {
  const { data, initLabel, initValue, onValueChange, objectKey } = props;
  const [visible, setVisible] = useState(false);
  const [value, setValue] = useState(initValue);
  const toggleModal = () => setVisible(!visible);
  const onChange = (item: any) => {
    setValue(item);
    toggleModal();
    onValueChange(item);
  };
  const getLabel = value ? value[objectKey] : initLabel;
  React.useEffect(() => {
    // console.log(initValue)
  }, []);
  return (
    <View>
      <Modal visible={visible} transparent={true}>
        <TouchableNativeFeedback onPress={toggleModal}>
          <View style={styles.blankSection}></View>
        </TouchableNativeFeedback>
        <View style={styles.pickerContainer}>
          <FlatList
            data={data}
            keyExtractor={(item, key) => String(key)}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity onPress={() => onChange(item)}>
                  <List.Item
                    title={item[objectKey] || initLabel}
                    right={() => <List.Icon icon="chevron-right" />}
                  />
                  <Divider />
                </TouchableOpacity>
              );
            }}
            ListEmptyComponent={EmptyComponent}
          />
          <Button onPress={toggleModal} style={styles.btn}>
            Cancel
          </Button>
        </View>
      </Modal>
      <Card style={styles.card} onPress={toggleModal}>
        <List.Item
          title={getLabel}
          right={() => <IconButton style={styles.icon} icon="chevron-down" />}
        />
      </Card>
    </View>
  );
}
const EmptyComponent = () => {
  return (
    <View style={styles.emptyContainer}>
      <Paragraph>Tidak ada data</Paragraph>
    </View>
  );
};

PickerCostum.defaultProps = {
  onValueChange: () => console.log("value has change"),
  initLabel: "Pilih Sesuatu",
  data: [
    {
      id: 1,
      label: "label 1",
    },
    {
      id: 1,
      label: "label 2",
    },
  ],
};
const styles = StyleSheet.create({
  blankSection: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.2)",
  },
  pickerContainer: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  btn: { marginVertical: 5 },
  icon: { height: 20 },
  card: { backgroundColor: Colors.gray1 },
  emptyContainer: { flex: 1, alignItems: "center", padding: 10 },
});

export default memo(PickerCostum);
