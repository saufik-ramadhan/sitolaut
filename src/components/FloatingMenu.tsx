import React, { memo } from "react";
import { View, Text, TouchableWithoutFeedback, ScrollView } from "react-native";
import Icon from "./Icon";
import { useNavigation } from "@react-navigation/native";
import Colors, { default as colors } from "./../config/Colors";
import { FlatList } from "react-native-gesture-handler";
import { Caption, IconButton, Paragraph } from "react-native-paper";

interface FloatingMenu {
  data?: Array<any> | undefined;
  style?: object | undefined;
}

function FloatingMenu({ data, style, navigation }: FloatingMenu) {
  return (
    <FlatList
      data={data}
      renderItem={({item}) => (
        <View style={{justifyContent: 'flex-start', alignItems: 'center', width: 60}}>
          <IconButton icon={item.icon} onPress={() => null} size={30} color={Colors.pri} style={{margin: 0, padding: 0}} onPress={() => navigation.navigate(item.route)}/>
          <Paragraph style={{fontSize: 10, lineHeight:12, textAlign: 'center', textAlignVertical:"center"}}>{item.name}</Paragraph>
        </View>
      )}
      keyExtractor={(item, key) => String(key)}
      contentContainerStyle={{paddingHorizontal: 10}}
      horizontal
      showsHorizontalScrollIndicator={false}
    />
  );
}
FloatingMenu.defaultProps = {
  data: [
    {
      name: "P.Order",
      icon: "file-invoice",
      iconType: "FontAwesome5",
      route: "PurchaseOrder",
    },
    {
      name: "Pengaduan",
      icon: "notification",
      iconType: "AntDesign",
      route: "Pengaduan",
    },
    {
      name: "Tracking",
      icon: "route",
      iconType: "FontAwesome5",
      route: "Tracking",
    },
    {
      name: "Penerimaan",
      icon: "handshake",
      iconType: "FontAwesome5",
      route: "Penerimaan",
    },
    {
      name: "Distribusi",
      icon: "truck",
      iconType: "FontAwesome5",
      route: "Distribusi",
    },
  ],
};
{/* <View
      style={{
        backgroundColor: colors.white,
        elevation: 0,
        padding: 5,
      }}
    >
      <View
        style={{
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "flex-start",
          justifyContent: "flex-start",
        }}
      >
        {data.map(function (item, key) {
          return (
            <TouchableWithoutFeedback
              key={key}
              onPress={function () {
                navigation.navigate(item.route);
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  marginHorizontal: 0,
                  marginVertical: 5,
                  width: "20%",
                }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    width: 50,
                    borderRadius: 3,
                  }}
                >
                  <Icon
                    name={item.icon}
                    type={item.iconType}
                    style={{ fontSize: 30, color: Colors.pri }}
                  />
                </View>
                <Text
                  style={{
                    color: colors.gray8,
                    textAlign: "center",
                    fontSize: 10,
                  }}
                >
                  {item.name}
                </Text>
              </View>
            </TouchableWithoutFeedback>
          );
        })}
      </View>
    </View> */}
export default memo(FloatingMenu);
