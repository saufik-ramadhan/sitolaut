import React from 'react'
import { StyleSheet, View } from 'react-native'

interface Center {
  children: React.ReactNode;
}

export default function Center({children}: Center) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      {children}
    </View>
  )
}


