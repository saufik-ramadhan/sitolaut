import React, { memo, ReactNode } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-paper";
import Colors from "../config/Colors";

type FilterButton = {
  onPress: any;
  left?: ReactNode;
  right?: ReactNode;
};
function FilterButton({ onPress, left, right }: FilterButton) {
  return (
    <View
      style={{
        flexDirection: "row",
        position: "absolute",
        bottom: 30,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      {left}
      <Button
        style={{
          borderRadius: 40,
        }}
        mode="contained"
        color="white"
        labelStyle={{
          color: Colors.def,
        }}
        icon="filter"
        onPress={onPress}
      >
        Filter
      </Button>
      {right}
    </View>
  );
}

const styles = StyleSheet.create({});
export default memo(FilterButton);
