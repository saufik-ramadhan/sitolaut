import React, { memo } from "react";
import { View } from "react-native";
import {
  Placeholder,
  PlaceholderLine,
  PlaceholderMedia,
  Fade,
} from "rn-placeholder";

function PlaceholderLoading({ loading }: any) {
  return loading ? (
    <>
      <View style={{ padding: 10 }}>
        <Placeholder Animation={Fade} Left={PlaceholderMedia}>
          <PlaceholderLine width={90} />
          <View style={{ flexDirection: "row" }}>
            <PlaceholderLine width={30} />
            <View style={{ width: 10 }} />
            <PlaceholderLine width={30} />
          </View>
          <PlaceholderLine width={30} />
        </Placeholder>
      </View>

      <View style={{ padding: 10 }}>
        <Placeholder Animation={Fade} Left={PlaceholderMedia}>
          <PlaceholderLine width={90} />
          <View style={{ flexDirection: "row" }}>
            <PlaceholderLine width={30} />
            <View style={{ width: 10 }} />
            <PlaceholderLine width={30} />
          </View>
          <PlaceholderLine width={30} />
        </Placeholder>
      </View>

      <View style={{ padding: 10 }}>
        <Placeholder Animation={Fade} Left={PlaceholderMedia}>
          <PlaceholderLine width={90} />
          <View style={{ flexDirection: "row" }}>
            <PlaceholderLine width={30} />
            <View style={{ width: 10 }} />
            <PlaceholderLine width={30} />
          </View>
          <PlaceholderLine width={30} />
        </Placeholder>
      </View>

      <View style={{ padding: 10 }}>
        <Placeholder Animation={Fade} Left={PlaceholderMedia}>
          <PlaceholderLine width={90} />
          <View style={{ flexDirection: "row" }}>
            <PlaceholderLine width={30} />
            <View style={{ width: 10 }} />
            <PlaceholderLine width={30} />
          </View>
          <PlaceholderLine width={30} />
        </Placeholder>
      </View>

      <View style={{ padding: 10 }}>
        <Placeholder Animation={Fade} Left={PlaceholderMedia}>
          <PlaceholderLine width={90} />
          <View style={{ flexDirection: "row" }}>
            <PlaceholderLine width={30} />
            <View style={{ width: 10 }} />
            <PlaceholderLine width={30} />
          </View>
          <PlaceholderLine width={30} />
        </Placeholder>
      </View>

      <View style={{ padding: 10 }}>
        <Placeholder Animation={Fade} Left={PlaceholderMedia}>
          <PlaceholderLine width={90} />
          <View style={{ flexDirection: "row" }}>
            <PlaceholderLine width={30} />
            <View style={{ width: 10 }} />
            <PlaceholderLine width={30} />
          </View>
          <PlaceholderLine width={30} />
        </Placeholder>
      </View>
    </>
  ) : null;
}

export default memo(PlaceholderLoading);
