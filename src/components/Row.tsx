import React, { memo } from "react";
import { View } from "react-native";

type Row = {
  justifyContent?: string;
  alignItems?: string;
  children?: React.ReactNode;
};

function Row({ children, justifyContent, alignItems }: Row) {
  return (
    <View style={{ flexDirection: "row", justifyContent, alignItems }}>
      {children}
    </View>
  );
}

export default memo(Row);
