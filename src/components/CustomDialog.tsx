import React, { memo } from "react";
import { ReactNode } from "react";
import { Portal, Dialog } from "react-native-paper";

interface DialogType {
  visible: boolean;
  onDismiss: any;
  title: string;
  children: ReactNode;
}

function CustomDialog({ visible, onDismiss, children, title }: DialogType) {
  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Title>{title}</Dialog.Title>
        <Dialog.Content>{children}</Dialog.Content>
      </Dialog>
    </Portal>
  );
}

export default memo(CustomDialog);
