import React, { memo, useState } from "react";
import { View, Platform, Text, StyleSheet } from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import { Card, Button, Caption } from "react-native-paper";
import Colors from "./../config/Colors";
import { Icon } from "./index";
import moment from "moment";
import {
  DateFormat,
  TimeFormat,
  DateFormat3,
  DateFormatStrip,
} from "../services/utils";
type DateTime = {
  type?: "date" | "time" | "datetime";
  onChangeValue?: any;
  onChangeTime?: any;
  initialValue?: Date;
  label?: string;
};

function DateTime({
  type = "date",
  label = "Unlabeled",
  onChangeValue,
  initialValue = new Date(),
}: DateTime) {
  const [date, setDate] = useState(initialValue);
  const [time, setTime] = useState(initialValue);
  const [datetime, setDateTime] = useState("");
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const onChange = (event, currentDate) => {
    setShow(Platform.OS === "ios");
    if (currentDate) {
      setDate(currentDate);
      let concat = {
        date: DateFormatStrip(currentDate),
        time: TimeFormat(time),
      };
      // DateFormatStrip(currentDate) + " " + TimeFormat(time);
      onChangeValue(concat);
    }
  };

  const onTimeChange = (e, currentTime) => {
    setShow(Platform.OS === "ios");
    if (currentTime) {
      setTime(currentTime);
      let concat = {
        date: DateFormatStrip(date),
        time: TimeFormat(currentTime),
      };
      // let concat = DateFormatStrip(date) + " " + TimeFormat(currentTime);
      onChangeValue(concat);
    }
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  const showTimepicker = () => {
    showMode("time");
  };

  return (
    <View>
      {type == "date" ? (
        <Card onPress={showDatepicker} style={styles.container}>
          <Text style={{ color: Colors.gray4 }}>{label}</Text>
          <Text style={styles.text}>{DateFormat(date)}</Text>
          <Icon
            name={"calendar"}
            style={{
              color: Colors.gray3,
              fontSize: 20,
              paddingVertical: 10,
              position: "absolute",
              right: 0,
            }}
          />
        </Card>
      ) : type == "time" ? (
        <Card onPress={showTimepicker} style={styles.container}>
          <Text style={{ color: Colors.gray4 }}>{label}</Text>
          <Text style={styles.text}>{`${date.getTime()}`}</Text>
        </Card>
      ) : (
        <View style={{ flexDirection: "row" }}>
          <Card
            onPress={showDatepicker}
            style={[styles.container, { flex: 3 }]}
          >
            <Text style={{ color: Colors.gray4 }}>{label} Date</Text>
            <Text style={styles.text}>{DateFormat(date)}</Text>
            <Icon
              name={"calendar"}
              style={{
                color: Colors.gray3,
                fontSize: 20,
                paddingVertical: 10,
                position: "absolute",
                right: 0,
              }}
            />
          </Card>
          <Card
            onPress={showTimepicker}
            style={[styles.container, { flex: 1, marginLeft: 5 }]}
          >
            <Text style={styles.text}>{`\n${TimeFormat(time)}`}</Text>
            <Icon
              name={"clock"}
              style={{
                color: Colors.gray3,
                fontSize: 20,
                paddingVertical: 10,
                position: "absolute",
                right: 0,
              }}
            />
          </Card>
        </View>
      )}
      {show && (
        <>
          <DateTimePicker
            testID="dateTimePicker"
            value={mode == "date" ? date : time}
            mode={mode}
            display="spinner"
            onChange={mode == "date" ? onChange : onTimeChange}
            minuteInterval={1}
          />
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: Colors.grayL,
    paddingVertical: 5,
    borderRadius: 3,
  },
  text: {
    color: Colors.gray4,
  },
});

export default memo(DateTime);
