import React, { useState, useEffect, memo } from "react";
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StatusBar,
  FlatList,
  TextInput,
} from "react-native";
import { Icon } from "./index";
import Colors, { default as colors } from "./../config/Colors";
import {
  Caption as Subtitle,
  List,
  IconButton,
  Card,
  Divider,
} from "react-native-paper";
import { bool, boolean } from "yup";

function Selector({
  onPressCancel,
  onSelected,
  options,
  withSearch,
  label,
  objectKey,
  subtitle,
  subtitle2,
  visible,
  ...props
}: any) {
  const [data, setData] = useState([]);
  const handleFilter = function (text: string) {
    // const newData = options.filter(function (item) {
    //   const itemData = `${item[objectKey||'value'].toUpperCase()}`;
    //   const textData = text.toUpperCase();
    //   return itemData.indexOf(textData) > -1;
    // });

    const regex = new RegExp(`${text.trim()}`, "i");
    let data = options.filter((item: any) => {
      if (item[objectKey || "value"])
        return item[objectKey || "value"].search(regex) >= 0;
    });
    setData(data);
  };

  useEffect(() => setData(options), [visible]);
  return (
    <Modal
      {...props}
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={onPressCancel}
    >
      <View
        style={{
          backgroundColor: "rgba(0,0,0,0.5)",
          flex: 1,
        }}
      >
        <TouchableWithoutFeedback onPress={onPressCancel}>
          <View style={{ flex: 1 }} />
        </TouchableWithoutFeedback>
        <View
          style={{
            flex: 2,
            backgroundColor: "white",
            borderTopColor: colors.pri,
            borderTopWidth: 8,
          }}
        >
          {withSearch && (
            <View
              style={{
                paddingHorizontal: 20,
                backgroundColor: "#eee",
                flexDirection: "row",
                marginHorizontal: 5,
                elevation: 5,
              }}
            >
              <List.Icon style={{ marginHorizontal: 0 }} icon="magnify" />
              <TextInput
                placeholder={`Cari ${label} . . .`}
                onChangeText={function (e) {
                  handleFilter(e);
                }}
                // autoFocus
              />
            </View>
          )}
          <View style={{ marginHorizontal: 20 }}>
            <FlatList
              data={data}
              showsVerticalScrollIndicator={false}
              ListFooterComponent={<View style={{ height: 100 }} />}
              ListEmptyComponent={
                <Text
                  style={{
                    color: "#666",
                    textAlign: "center",
                    marginTop: 10,
                    fontSize: 16,
                  }}
                >
                  Data tidak ditemukan
                </Text>
              }
              renderItem={function ({ item }) {
                return (
                  <Card
                    onPress={function () {
                      onSelected(item);
                      onPressCancel();
                    }}
                  >
                    <List.Item title={`${item[objectKey || "value"]}`} />
                    <Divider />
                  </Card>
                );
              }}
              keyExtractor={function (item, key) {
                return String(key);
              }}
            />
            <View style={{ height: 10 }} />
          </View>
        </View>
      </View>
    </Modal>
  );
}

type SelectInput = {
  initialValue: any;
  onChangeValue: any;
  options: any;
  label: any;
  withSearch?: boolean;
  objectKey: any;
  error?: any;
  subtitle?: string;
  subtitle2?: string;
};
/**
 * Custom Picker Component
 * JavaScript, with search for
 * entries feature
 */
function SelectInput({
  label,
  options,
  initialValue,
  onChangeValue,
  withSearch,
  error,
  objectKey,
  subtitle,
  subtitle2,
}: SelectInput) {
  // const compare = () => {
  //   let initial = options.filter((item) => String(item.id) == initialValue);
  //   return initial;
  // }
  const [visible, setVisible] = useState(false);
  const [selected, setSelected] = useState(false);
  return (
    <View style={{ backgroundColor: colors.grayL, borderRadius: 3 }}>
      <Text
        style={{
          fontSize: 12,
          color: "#666",
          position: "absolute",
          left: 12,
          top: 8,
        }}
      >
        {label}
      </Text>
      <Selector
        objectKey={objectKey}
        subtitle={subtitle}
        subtitle2={subtitle2}
        visible={visible}
        onPressCancel={function () {
          setVisible(!visible);
        }}
        onSelected={function (val) {
          setSelected(val);
          onChangeValue ? onChangeValue(val) : null;
        }}
        options={options}
        label={label}
        withSearch={withSearch ? true : false}
        // subtitle1={subtitle1}
        // subtitle2={subtitle2}
      />

      <TouchableOpacity
        onPress={function () {
          setVisible(!visible);
        }}
      >
        <View
          style={{
            flexDirection: "row",
            borderColor: error ? colors.danger : "#ccc",
            borderBottomWidth: 1,
          }}
        >
          <View style={{ flex: 8, marginLeft: 8 }}>
            <Text
              style={{
                fontSize: 14,
                paddingTop: 25,
                paddingHorizontal: 5,
                paddingBottom: 5,
                color:
                  Object.keys(selected).length > 0 ? Colors.def : Colors.gray2,
              }}
            >
              {Object.keys(selected).length > 0
                ? selected[objectKey || "value"]
                : label}{" "}
              {subtitle &&
                (Object.keys(selected).length > 0
                  ? selected[subtitle || "value"]
                  : label)}{" "}
              {subtitle2 &&
                (Object.keys(selected).length > 0
                  ? selected[subtitle2 || "value"]
                  : label)}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "flex-end",
            }}
          >
            <Icon
              name={"chevron-down"}
              type="Feather"
              style={{
                color: error ? colors.danger : "#ccc",
                fontSize: 20,
              }}
            />
          </View>
        </View>
      </TouchableOpacity>
      {error && (
        <Subtitle style={{ color: colors.danger, marginLeft: 10 }}>
          {error}
        </Subtitle>
      )}
    </View>
  );
}

export default memo(SelectInput);
