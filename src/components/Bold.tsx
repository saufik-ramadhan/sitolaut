import React, { memo } from "react";
import { StyleSheet, Text } from "react-native";

interface Bold {
  children?: React.ReactNode;
  style?: any;
  props?: any;
}

function Bold({ children, style, ...props }: Bold) {
  return (
    <Text {...props} style={[styles.bold, style]}>
      {children}
    </Text>
  );
}

const styles = StyleSheet.create({
  bold: {
    fontWeight: "bold",
  },
});

export default memo(Bold);
