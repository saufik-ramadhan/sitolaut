import React from "react";
import { Text } from "react-native";

interface Heading {
  children?: React.ReactNode;
  style?: any;
  props?: any;
}

export function Heading1({ children, style, ...props }: Heading) {
  return (
    <Text {...props} style={[{ fontSize: 40 }, style]}>
      {children}
    </Text>
  );
}

export function Heading2({ children, style, ...props }: Heading) {
  return (
    <Text {...props} style={[{ fontSize: 30 }, style]}>
      {children}
    </Text>
  );
}

export function Heading3({ children, style, ...props }: Heading) {
  return (
    <Text {...props} style={[{ fontSize: 25 }, style]}>
      {children}
    </Text>
  );
}

export function Heading4({ children, style, ...props }: Heading) {
  return (
    <Text {...props} style={[{ fontSize: 20 }, style]}>
      {children}
    </Text>
  );
}

export function Heading5({ children, style, ...props }: Heading) {
  return (
    <Text {...props} style={[{ fontSize: 18 }, style]}>
      {children}
    </Text>
  );
}
