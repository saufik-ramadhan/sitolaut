import React, { memo, useState } from "react";
import { View, Text, StyleSheet, FlatList, Modal } from "react-native";
import {
  Modal as PaperModal,
  Portal,
  Card,
  Searchbar,
  List,
  Appbar,
  Title,
} from "react-native-paper";
import { Header } from "react-native/Libraries/NewAppScreen";

function SearchModal({ data }) {
  const [selected, setSelected] = useState({ id: "", name: "" });
  const [pick, setPicker] = useState(true);
  const { id, name }: any = selected;
  const onChange = (text: any) => {
    setSelected({ ...selected, name: text });
    setPicker(true);
  };

  const findData = (query: any) => {
    if (name === "") {
      return [];
    }
    const regex = new RegExp(`${query.trim()}`, "i");
    return data.filter((item) => item.name.search(regex) >= 0);
  };
  const newData = name !== "" ? findData(name) : data;
  const comp = (a: any, b: any) =>
    a.toLowerCase().trim() === b.toLowerCase().trim();
  const openPicker = () => {
    setPicker(!pick);
  };
  return (
    <Portal>
      <Modal visible={true} contentContainerStyle={styles.container}>
        <Appbar.Header>
          <Appbar.BackAction onPress={function () {}} />
          <Appbar.Content title="Pilih Supplier" />
        </Appbar.Header>
        <Card style={styles.card}>
          <Searchbar placeholder="cari . . .  " />
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <Card>
                <List.Item title={item.nama_perusahaan} />
              </Card>
            )}
            keyExtractor={(item) => String(item.id)}
          />
        </Card>
      </Modal>
    </Portal>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    justifyContent: "flex-start",
  },
  card: { padding: 10, borderRadius: 5 },
});

export default memo(SearchModal);
