import React, { memo } from "react";
import { View, StyleSheet } from "react-native";
import { Icon } from "../components";
import Colors from "../config/Colors";
import { Caption } from "react-native-paper";

const EmptyState = () => {
  return (
    <View style={styles.emptyWrapper}>
      <Icon name="file-search-outline" color={Colors.gray10} size={50} />
      <Caption style={{ textAlign: "center" }}>
        Data tidak ditermukan{"\n"}silahkan lakukan refresh dengan scroll ke
        atas{"\n"}
        atau ganti filter data
      </Caption>
    </View>
  );
};
const styles = StyleSheet.create({
  emptyWrapper: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default memo(EmptyState);
