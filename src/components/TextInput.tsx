import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput as RNTextInput, TextInputProps } from 'react-native'
import { Caption } from 'react-native-paper'
import Colors from './../config/Colors';

interface TextInput extends TextInputProps {
  label: string;
  error: string | boolean |undefined;
}

export default function TextInput({error, label, ...props}: TextInput) {
  const [focused, setFocused] = useState(false);
  return (
    <View>
      <Caption>{label}</Caption>
      <RNTextInput
        {...props}
        style={[styles.textInput, {
          borderColor: focused ? Colors.lightblue : error ? Colors.danger : Colors.gray,
        }]}
        onFocus={function(){setFocused(true)}}
        onBlur={function(){setFocused(false)}}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  textInput: {
    borderWidth: 1.5,
    borderColor: 'gray',
    borderRadius: 5,
    paddingHorizontal: 8,
    fontSize: 15,
    marginBottom: 6,
  },
  error: {
    color: 'red',
    fontSize: 10
  }
})
