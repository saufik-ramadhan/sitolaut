import React, { useRef, useEffect, useState, ReactNode, memo } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  ScrollView,
  NativeEventEmitter,
  Dimensions,
} from "react-native";
import Icon from "./Icon";
import { useNavigation } from "@react-navigation/native";
import { default as colors } from "./../config/Colors";

import {
  PanGestureHandler,
  PinchGestureHandler,
  RotationGestureHandler,
  State,
  NativeViewGestureHandler,
} from "react-native-gesture-handler";
import Animated from "react-native-reanimated";
import { Menu } from "react-native-paper";
import { clamp } from "lodash";
import { timing } from "react-native-redash";

const WINDOW_HEIGHT = Dimensions.get("window").height;
const WINDOW_WIDTH = Dimensions.get("window").width;

interface GojekMenu {
  data?: Array<any> | undefined;
  style?: object | undefined;
  children?: React.ReactNode;
}

const {
  set,
  cond,
  block,
  eq,
  add,
  sub,
  Value,
  event,
  concat,
  multiply,
  min,
  useCode,
  abs,
  call,
  interpolate,
  Extrapolate,
  diffClamp,
  Clock,
  clockRunning,
  not,
} = Animated;

const { height } = Dimensions.get("window");
const MINIMIZED_PLAYER_HEIGHT = 50;
const SNAP_TOP = 0;
const SNAP_BOTTOM = height - MINIMIZED_PLAYER_HEIGHT;

const MAX_MENU_HEIGHT = 500;

function GojekMenu({ data, style, navigation, children }: GojekMenu) {
  const [MenuHeight] = useState(new Value(50));
  const MenuHeightClamped = new Value(50);
  const offsetY = new Value(50);
  const [favVisible, setFavVisible] = useState(false);

  const translationY = new Value(0);
  const velocityY = new Value(0);
  const state = new Value(State.UNDETERMINED);
  const offset = new Value(SNAP_BOTTOM);
  const goUp: Animated.Value<0 | 1> = new Value(0);
  const goDown: Animated.Value<0 | 1> = new Value(0);
  const clock = new Clock();
  const handleDrag = event([
    {
      nativeEvent: ({ translationY: y, state }) =>
        block([
          set(MenuHeight, sub(offsetY, y)),
          cond(goUp, [
            set(
              offsetY,
              timing({
                clock,
                from: MenuHeight,
                to: MAX_MENU_HEIGHT,
              })
            ),
            cond(not(clockRunning(clock)), [set(goUp, 0)]),
          ]),
          cond(goDown, [
            set(
              offsetY,
              timing({
                clock,
                from: MenuHeight,
                to: 50,
              })
            ),
            cond(not(clockRunning(clock)), [set(goDown, 0)]),
          ]),
          call([MenuHeight], hideFav),
        ]),
    },
  ]);

  function hideFav([MenuHeight]) {
    if (MenuHeight > 70) setFavVisible(false);
    else setFavVisible(true);
  }

  const MenuHeightInterpolate = interpolate(MenuHeight, {
    inputRange: [50, MAX_MENU_HEIGHT],
    outputRange: [50, MAX_MENU_HEIGHT],
    extrapolate: Extrapolate.CLAMP,
  });
  const favOpacity = interpolate(MenuHeight, {
    inputRange: [60, 70],
    outputRange: [1, 0],
  });
  const menuOpacity = interpolate(MenuHeight, {
    inputRange: [60, 100],
    outputRange: [0, 1],
  });
  const MenuMargin = interpolate(MenuHeight, {
    inputRange: [70, 100],
    outputRange: [20, 0],
    extrapolate: Extrapolate.CLAMP,
  });
  const MenuPosition = interpolate(MenuHeight, {
    inputRange: [70, 100],
    outputRange: [20, 0],
    extrapolate: Extrapolate.CLAMP,
  });

  return (
    <Animated.View
      style={{
        backgroundColor: colors.white,
        elevation: 5,
        padding: 15,
        paddingTop: 20,
        borderRadius: 40,
        position: "absolute",
        marginHorizontal: MenuMargin,
        bottom: MenuPosition,
      }}
    >
      <PanGestureHandler
        onGestureEvent={handleDrag}
        onHandlerStateChange={handleDrag}
      >
        <Animated.View
          style={[
            {
              height: MenuHeightInterpolate,
            },
          ]}
        >
          <View
            style={{
              backgroundColor: colors.gray1,
              borderRadius: 50,
              width: "15%",
              paddingVertical: 2,
              position: "absolute",
              top: -10,
              alignSelf: "center",
            }}
          ></View>
          {favVisible && (
            <Animated.View
              style={{
                opacity: favOpacity,
                flexDirection: "row",
                flexWrap: "wrap",
                alignItems: "flex-start",
                justifyContent: "flex-start",
              }}
            >
              {data.map(function (item, key) {
                return (
                  <TouchableWithoutFeedback
                    key={key}
                    onPress={function () {
                      navigation.navigate(item.route);
                    }}
                  >
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        marginHorizontal: 0,
                        marginVertical: 5,
                        width: "25%",
                      }}
                    >
                      <View
                        style={{
                          justifyContent: "center",
                          alignItems: "center",
                          width: 50,
                          borderRadius: 3,
                        }}
                      >
                        <Icon
                          name={item.icon}
                          type={item.iconType}
                          style={{ color: colors.pri, fontSize: 40 }}
                        />
                      </View>
                      <Text
                        style={{
                          color: colors.gray8,
                          textAlign: "center",
                          fontSize: 12,
                        }}
                      >
                        {item.name}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                );
              })}
            </Animated.View>
          )}

          <Animated.View
            style={{
              opacity: menuOpacity,
            }}
          >
            {children}
          </Animated.View>
        </Animated.View>
      </PanGestureHandler>
    </Animated.View>
  );
}
GojekMenu.defaultProps = {
  data: [
    {
      name: "Purchase",
      icon: "clipboard-text",
      route: "PurchaseOrder",
    },
    {
      name: "Jadwal",
      icon: "calendar",
      route: "Jadwal",
    },
    {
      name: "Vessel Map",
      icon: "map",
      route: "VesselGeoLocation",
    },
    {
      name: "Report",
      icon: "file-table",
      route: "Report",
    },
  ],
};

export default memo(GojekMenu);
