import React, { memo } from "react";
import { Text } from "react-native";
import Colors from "./../config/Colors";

type ErrorText = {
  children?: string;
  color?: string;
};

function ErrorText({ children, color }: ErrorText) {
  return (
    <Text style={{ color: color || Colors.danger, fontSize: 12 }}>
      {children}
    </Text>
  );
}

export default memo(ErrorText);
