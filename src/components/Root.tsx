import React, { memo } from "react";
import { SafeAreaView, StyleSheet, ScrollView } from "react-native";
import { KeyboardAwareInsetsView } from "react-native-keyboard-tracking-view";

interface RootProps {
  children?: React.ReactNode;
  style?: object;
  refreshControl?: any;
}

/**
 * Component Wrapper Screen
 * Basisnya ScrollView
 * Sudah Keyboard Aware
 * @param children
 * @param style
 * @param refreshControl
 */

function Root(props: RootProps) {
  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        contentContainerStyle={[styles.scrollView, {padding: 20, ...props.style}]}
        refreshControl={props.refreshControl}
      >
        {props.children}
      </ScrollView>
      <KeyboardAwareInsetsView />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  scrollView: {
    flexGrow: 1,
  },
});

export default memo(Root);
