import React from "react";
import { Dimensions, SafeAreaView, StyleSheet, Text, View } from "react-native";
import Animated from "react-native-reanimated";
import {
  PanGestureHandler,
  State,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler";
import { clamp, onGestureEvent, timing, withSpring } from "react-native-redash";
import { getBottomSpace } from "react-native-iphone-x-helper";
import { Icon } from "../components";
import Colors from "./../config/Colors";
import { IconButton } from "react-native-paper";

const { height } = Dimensions.get("window");
const TABBAR_HEIGHT = getBottomSpace() + 50;
const MINIMIZED_PLAYER_HEIGHT = 80;
const SNAP_TOP = 0;
const SNAP_BOTTOM = height - TABBAR_HEIGHT - MINIMIZED_PLAYER_HEIGHT;
const config = {
  damping: 30,
  mass: 1,
  stiffness: 200,
  overshootClamping: false,
  restSpeedThreshold: 0.1,
  restDisplacementThreshold: 0.1,
};
const {
  Clock,
  Value,
  cond,
  useCode,
  set,
  block,
  not,
  clockRunning,
  interpolate,
  diffClamp,
  Extrapolate,
} = Animated;

const styles = StyleSheet.create({
  playerSheet: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "white",
  },
  container: {
    backgroundColor: "#272829",
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: TABBAR_HEIGHT,
    flexDirection: "row",
    borderTopColor: "black",
    borderWidth: 1,
  },
});

interface GojekMenu {
  data?: Array<any> | undefined;
}

const data = [
  {
    name: "Purchase",
    icon: "clipboard-text",
    route: "PurchaseOrder",
  },
  {
    name: "Jadwal",
    icon: "calendar",
    route: "Jadwal",
  },
  {
    name: "Vessel Map",
    icon: "map",
    route: "VesselGeoLocation",
  },
  {
    name: "Report",
    icon: "file-table",
    route: "Report",
  },
];

const BottomTab = ({ children, navigation }) => {
  const translationY = new Value(0);
  const velocityY = new Value(0);
  const state = new Value(State.UNDETERMINED);
  const offset = new Value(SNAP_BOTTOM);
  const goUp: Animated.Value<0 | 1> = new Value(0);
  const goDown: Animated.Value<0 | 1> = new Value(0);
  const gestureHandler = onGestureEvent({
    state,
    translationY,
    velocityY,
  });
  const translateY = withSpring({
    value: clamp(translationY, SNAP_TOP, SNAP_BOTTOM),
    velocity: velocityY,
    offset,
    state,
    snapPoints: [SNAP_TOP, SNAP_BOTTOM],
    config,
  });
  const translateBottomTab = interpolate(translateY, {
    inputRange: [SNAP_TOP, SNAP_BOTTOM],
    outputRange: [TABBAR_HEIGHT, 0],
    extrapolate: Extrapolate.CLAMP,
  });
  const opacity = interpolate(translateY, {
    inputRange: [SNAP_BOTTOM - MINIMIZED_PLAYER_HEIGHT, SNAP_BOTTOM],
    outputRange: [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });
  const opacity2 = interpolate(translateY, {
    inputRange: [
      SNAP_BOTTOM - MINIMIZED_PLAYER_HEIGHT * 2,
      SNAP_BOTTOM - MINIMIZED_PLAYER_HEIGHT,
    ],
    outputRange: [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });
  const opacity3 = interpolate(translateY, {
    inputRange: [SNAP_BOTTOM - MINIMIZED_PLAYER_HEIGHT, SNAP_BOTTOM],
    outputRange: [1, 0],
    extrapolate: Extrapolate.CLAMP,
  });
  const clock = new Clock();
  useCode(
    block([
      cond(goUp, [
        set(
          offset,
          timing({
            clock,
            from: offset,
            to: SNAP_TOP,
          })
        ),
        cond(not(clockRunning(clock)), [set(goUp, 0)]),
      ]),
      cond(goDown, [
        set(
          offset,
          timing({
            clock,
            from: offset,
            to: SNAP_BOTTOM,
          })
        ),
        cond(not(clockRunning(clock)), [set(goDown, 0)]),
      ]),
    ]),
    []
  );
  return (
    <>
      <PanGestureHandler {...gestureHandler}>
        <Animated.View
          style={[
            styles.playerSheet,
            {
              transform: [{ translateY }],
              backgroundColor: "transparent",
            },
          ]}
        >
          {/* <Player onPress={() => goDown.setValue(1)} /> */}
          {/* <Text onPress={() => goDown.setValue(1)}>Player</Text> */}
          <Animated.View
            style={{ opacity: opacity3, flex: 1, backgroundColor: "white" }}
          >
            {children}
          </Animated.View>
          <Animated.View
            pointerEvents="none"
            style={{
              opacity: opacity2,
              backgroundColor: "transparent",
              ...StyleSheet.absoluteFillObject,
            }}
          />
          <Animated.View
            style={{
              opacity,
              position: "absolute",
              top: -50,
              left: 0,
              right: 0,
              margin: 20,
              borderRadius: 40,
              elevation: 2,
              height: MINIMIZED_PLAYER_HEIGHT,
              backgroundColor: "white",
              justifyContent: "center",
              alignItems: "center",
              padding: 5,
              zIndex:10,
            }}
          >
            {/* <View
              style={{
                padding: 2,
                width: 50,
                borderRadius: 5,
                position: "absolute",
                top: 8,
                backgroundColor: Colors.grayL,
              }}
            ></View> */}
            <View style={{ flexDirection: "row"}}>
              {data.map((item, key) => (
                <View
                  style={{
                    width: "25%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <Icon
                    name={item.icon}
                    size={30}
                    onPress={function () {
                      navigation.navigate(`${item.route}`);
                    }}
                  />
                  <Text style={{ fontSize: 10 }}>{item.name}</Text>
                </View>
              ))}
            </View>
          </Animated.View>
        </Animated.View>
      </PanGestureHandler>
      {/* <Animated.View
        style={{ transform: [{ translateY: translateBottomTab }] }}
      >
        <SafeAreaView style={styles.container}>
          <Icon name="home" label="Home" />
          <Icon name="search" label="Cari" />
          <Icon
            name="chevron-up"
            size={40}
            label="Player"
            onPress={() => goUp.setValue(1)}
          />
        </SafeAreaView>
      </Animated.View> */}
    </>
  );
};

export default BottomTab;
