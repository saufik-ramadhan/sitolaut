import React, { memo } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Portal, Modal, Card, ActivityIndicator } from "react-native-paper";

function LoadingComponent({ visible }: any) {
  return (
    <Portal>
      <Modal visible={visible} contentContainerStyle={styles.container}>
        <Card style={styles.card}>
          <Card.Content>
            <ActivityIndicator />
          </Card.Content>
        </Card>
      </Modal>
    </Portal>
  );
}

export default memo(LoadingComponent);

const styles = StyleSheet.create({
  container: { alignItems: "center" },
  card: {
    width: 100,
    borderRadius: 10,
    elevation: 0,
  },
});
