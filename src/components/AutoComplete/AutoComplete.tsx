import React, { useState } from "react";
import { View, FlatList, StyleSheet } from "react-native";
import { TextInput, IconButton } from "react-native-paper";

export default function AutoComplete({
  onValueChange,
  value,
  data,
  renderItem,
  onChangeText,
}: any) {
  const [selected, setSelected] = useState({ id: "", name: "" });
  const [pick, setPicker] = useState(true);
  const { id, name }: any = selected;
  const onChange = (text: any) => {
    setSelected({ ...selected, name: text });
    setPicker(true);
  };

  const findData = (query: any) => {
    if (name === "") {
      return [];
    }
    const regex = new RegExp(`${query.trim()}`, "i");
    return data.filter((item) => item.name.search(regex) >= 0);
  };
  const newData = name !== "" ? findData(name) : data;
  const comp = (a: any, b: any) =>
    a.toLowerCase().trim() === b.toLowerCase().trim();
  const openPicker = () => {
    setPicker(!pick);
  };
  return (
    <View style={{ position: "relative", maxHeight: 200 }}>
      <View>
        <TextInput
          dense
          value={value}
          onChangeText={onChangeText}
          placeholder="Cari Consignee"
          // onBlur={openPicker}
        />
        <IconButton
          style={styles.icon}
          icon="chevron-down"
          onPress={openPicker}
        />
      </View>
      <FlatList data={newData} renderItem={renderItem} />
      {/* {newData.map((item, key) => {
        return (
           pick && <Card
            style={styles.flatlistContainer}
            onPress={() => {
              setSelected(item);
              onValueChange(item)
              openPicker();
            }}
          >
            <List.Item title={item.name} />
            <Divider />
          </Card>
        );
        })} */}
    </View>
  );
}

const styles = StyleSheet.create({
  flatlistContainer: {
    zIndex: 10,
    // positison: 'absolute',
    // flex:1
  },
  icon: { position: "absolute", right: 0, justifyContent: "center" },
});
AutoComplete.defaultProps = {
  onValueChange: () => console.log("valuse change"),
  data: [
    { id: 1, name: "nama perusahaan 1" },
    { id: 2, name: "nama consignee 2" },
    { id: 3, name: "nama perusahaan 3" },
    { id: 4, name: "nama supplier 4" },
    { id: 5, name: "nama perusahaan 5" },
  ],
};
