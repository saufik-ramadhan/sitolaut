import React, { memo } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableOpacityProps,
  TextInput,
} from "react-native";
import Colors from "./../config/Colors";

function Prev(props: TouchableOpacityProps) {
  return (
    <TouchableOpacity {...props}>
      <Text style={styles.text}>Prev</Text>
    </TouchableOpacity>
  );
}
function Next(props: TouchableOpacityProps) {
  return (
    <TouchableOpacity {...props}>
      <Text style={styles.text}>Next</Text>
    </TouchableOpacity>
  );
}
// type Page = {
//   count: Number;
//   current: Number;
//   onChangeNumber: any;
// };
// function Page({ count, current, onChangeNumber }: Page) {
//   return (
//     <View style={{ padding: 5, flexDirection: "row" }}>
//       <Text>{current}</Text>
//       <Text> of {count}</Text>
//     </View>
//   );
// }
function Pagination({ current = 1, count = 1, onPrev, onNext }) {
  return (
    <View
      style={{
        position: "absolute",
        bottom: 0,
        flexDirection: "row",
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Prev onPress={onPrev} style={styles.button} />
      <Text>
        {current} dari {count}
      </Text>
      <Next onPress={onNext} style={styles.button} />
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    color: "white",
  },
  button: {
    marginHorizontal: 50,
    backgroundColor: Colors.def,
    padding: 5,
    borderRadius: 20,
  },
});

export default memo(Pagination);
