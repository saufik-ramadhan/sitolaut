import React, { memo } from "react";
import { View, Text, Button } from "react-native";
import SlidingUpPanel from "rn-sliding-up-panel";

const styles = {
  container: {
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    margin: 10,
    elevation: 10,
    borderRadius: 40,
    height: 50,
  },
};

class SlideUpMenu extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Button title="Show panel" onPress={() => this._panel.show()} />
        <SlidingUpPanel ref={(c) => (this._panel = c)}>
          <View style={styles.container}>
            <Text>Here is the content inside panel</Text>
            <Button title="Hide" onPress={() => this._panel.hide()} />
          </View>
        </SlidingUpPanel>
      </View>
    );
  }
}

export default memo(SlideUpMenu);
