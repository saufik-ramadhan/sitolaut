import React, { memo } from "react";
import { MaterialCommunityIcons as Icon } from "@expo/vector-icons";

const IconComp = function ({ name, ...props }) {
  return <Icon {...props} name={name} />;
};

export default memo(IconComp);
