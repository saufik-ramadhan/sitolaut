import React, { memo } from "react";
import { View } from "react-native";

interface Space {
  height?: number;
}

/**
 * Add Spacing
 * @param height
 */
function Space({ height }: Space) {
  return <View style={{ height: height }} />;
}

export default memo(Space);
