import { Picker } from '@react-native-community/picker'
import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'
import { Caption } from 'react-native-paper'
import Colors from './../config/Colors';

type ItemValue  = number | string
interface SelectInput2 {
  label: string;
  onValueChange: (itemValue: ItemValue, itemIndex: number) => void;
  selectedValue: number | string;
  mode: 'dialog' | 'dropdown';
  items: ObjectArray;
  itemLabel: string;
  itemValue: string;
  error: string | number | undefined;
}

/**
 * SelectInput2 -- berbasis @react-native-community/picker
 * @param param0 
 */
export default function SelectInput2({error, label, onValueChange, selectedValue, mode, items = [], itemLabel, itemValue}: SelectInput2) {
  const [focused, setFocused] = useState(false);
  return (
    <View>
    <Caption>{label}</Caption>
    <View style={[styles.picker, {
      borderColor: focused ? Colors.lightblue : error ? Colors.danger : Colors.gray,
    }]}>
    <Picker
      mode={mode}
      selectedValue={String(selectedValue)}
      onValueChange={onValueChange}
      onTouchStart={function(){setFocused(true)}}
      onTouchEnd={function(){setFocused(false)}}
    >
      <Picker.Item label="Pilih..." value="" />
      {items.map((item: any, key: any) => (
        <Picker.Item key={key} label={String(item[itemLabel])} value={String(item[itemValue])} />
      ))}
    </Picker>
    </View>
    </View>
  )
}

const styles = StyleSheet.create({
  picker: {
    borderWidth: 1.5,
    borderColor: 'gray',
    borderRadius: 5,
    width: '100%',
    alignSelf: 'center',
    marginBottom: 10
  }
})
